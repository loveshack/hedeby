/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.i18n;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;

/**
 *  This ant task check the I18N bundles of the Hedeby projects.
 *  It can detect for following errors:
 *
 *  <ul>
 *    <li>Unused message bundles (properties files that are not message
 *        bundes should be excluded from the propertyFiles FileSet.</li>
 *    <li>Obsolete message key in message bundle.</li>
 *  </ul>
 *
 *  <p>The ant task assumes that the name of the message bundle is defined in a string
 *  literal of all classes that uses the message bundles. The developer must ensure
 *  this fact, otherwise this task marks used message as obsolete.</p>
 *
 *  <p>There is a special handling for cli message bundles implemented. If a message
 *  bundle name contains the string .cli. all messages ending with
 *  .name, .shortcut, .argument, .description, .long_description or .privileges_description
 *  are treated as messages that are used for cli commands. The suffix will be
 *  stripped and the resulting string will be assumed a string literal for the message.</p>
 *
 */
public class CheckBundleTask extends Task {

    private FileSet propertyFiles;
    private FileSet javaFiles;
    private String failedProperty;
    private boolean failOnError = true;

    private static final String[] CLI_SUFFIXES = {
        ".name", ".shortcut", ".argument", ".description", ".long_description", ".privileges_description"
    };


    /**
     * Execute this task
     * 
     * @throws org.apache.tools.ant.BuildException if the failOnError property is set to true and
     *                      the message bundles contains error
     */
    @Override
    public void execute() throws BuildException {

        if (propertyFiles == null) {
            throw new BuildException("missing propertyFiles");
        }

        if (javaFiles == null) {
            throw new BuildException("missing javaFiles");
        }

        DirectoryScanner scanner = propertyFiles.getDirectoryScanner(getProject());

        log("Building bundle map", Project.MSG_VERBOSE);

        boolean failed = false;
        List<Bundle> bundles = new LinkedList<Bundle>();
        for (String filename : scanner.getIncludedFiles()) {
            if (filename.endsWith(".properties")) {
                String bundleName = filename.substring(0, filename.length() - ".properties".length()).replace(File.separatorChar, '.');
                Bundle bundle = new Bundle(bundleName, new File(scanner.getBasedir(), filename));
                bundles.add(bundle);
                log("Found property file " + bundleName + " (" + filename + ")", Project.MSG_VERBOSE);
            } else {
                log("Skip non property file " + filename, Project.MSG_WARN);
            }
        }

        scanner = javaFiles.getDirectoryScanner(getProject());
        for (String filename : scanner.getIncludedFiles()) {
            log("Grep for bundle string in " + filename, Project.MSG_DEBUG);
            File file = new File(scanner.getBasedir(), filename);
            String content = readFile(file);
            for (Bundle bundle : bundles) {
                if (usesBundle(content, bundle)) {
                    log(bundle.filename + ": Bundle is used in " + filename, Project.MSG_VERBOSE);
                    bundle.addJavaFile(file);
                }
            }
        }

        log("Searching for obsolete message keys", Project.MSG_VERBOSE);

        for (Bundle bundle : bundles) {
            if (bundle.javaFiles.isEmpty()) {
                log(String.format("%s: Is not used as resource bundle", bundle.filename));
                continue;
            }
            log("Searching for obsolete keys of bundle " + bundle.name, Project.MSG_VERBOSE);


            try {
                bundle.load();
            } catch (IOException ex) {
                throw new BuildException(ex.getLocalizedMessage(), ex);
            }

            for (File f : bundle.javaFiles) {
                String content = readFile(f);
                bundle.grepAndReduce(content);
                if (bundle.getKeys().isEmpty()) {
                    break;
                }
            }
            if (bundle.getKeys().isEmpty()) {
                log("Bundle " + bundle.name + "is clean", Project.MSG_VERBOSE);
                continue;
            }

            ArrayList<Key> obsKeys = new ArrayList<Key>(bundle.getKeys().values());
            Collections.sort(obsKeys, KEY_BY_LINE_COMPARATOR);
            for (Key key : obsKeys) {
                log(String.format("%s:%d: [warning] [obsolete key] %s", bundle.filename, key.getLine(), key.getName()));
            }

            log(String.format("%s: [error] Bundle %s has %d obsolete keys", bundle.filename, bundle.name, bundle.getKeys().size()));
            failed = true;
        }

        if (failed) {
            if (failedProperty != null) {
                getProject().setProperty(failedProperty, "true");
            }
            if (failOnError) {
                throw new BuildException("Found errors in message bundles");
            }
        }
    }

    private boolean usesBundle(String fileContent, Bundle bundle) throws BuildException {
        try {
            BufferedReader rd = new BufferedReader(new StringReader(fileContent));
            try {
                String line;
                final String pattern = '"' + bundle.name + '"';
                while ((line = rd.readLine()) != null) {
                    //System.out.print("matches '" + line + "' against '" + pattern.pattern() + "'? ");
                    if (line.contains(pattern)) {
                        //System.out.println("true");
                        return true;
                    }
                //System.out.println("false");
                }
            } finally {
                rd.close();
            }
            return false;
        } catch (IOException ex) {
            throw new BuildException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * @return the failedProperty
     */
    public String getFailedProperty() {
        return failedProperty;
    }

    /**
     * @param failedProperty the failedProperty to set
     */
    public void setFailedProperty(String failedProperty) {
        this.failedProperty = failedProperty;
    }

    /**
     * @return the failOnError
     */
    public boolean isFailOnError() {
        return failOnError;
    }

    /**
     * @param failOnError the failOnError to set
     */
    public void setFailOnError(boolean failOnError) {
        this.failOnError = failOnError;
    }

    private class Bundle {

        final File filename;
        final String name;
        List<File> javaFiles;
        final boolean cliBundle;
        private final Map<String, Key> keys = new HashMap<String, Key>();

        public Bundle(String name, File filename) {
            this.name = name;
            this.filename = filename;
            this.javaFiles = new LinkedList<File>();
            cliBundle = name.contains(".cli.");
        }

        public void addJavaFile(File file) {
            javaFiles.add(file);
        }

        public boolean isCliBundle() {
            return cliBundle;
        }

        public Map<String, Key> getKeys() {
            return keys;
        }

        private String parseKey(String key) {

            key = key.trim();
            StringBuilder ret = new StringBuilder();
            boolean lastWasBackslash = false;
            for (int i = 0; i < key.length(); i++) {
                char c = key.charAt(i);
                if (c == '\\' && !lastWasBackslash) {
                    lastWasBackslash = true;
                    continue;
                }
                ret.append(c);
                lastWasBackslash = false;
            }
            return ret.toString();
        }

        public void load() throws IOException {

            BufferedReader br = new BufferedReader(new FileReader(filename));
            int lineNr = 1;
            try {
                String line;
                boolean processNextLine = true;

                while ((line = br.readLine()) != null) {
                    if (line.startsWith("#") || line.length() == 0) {
                        processNextLine = true;
                    } else {
                        if (processNextLine) {
                            String tokens[] = line.split("(?<!\\\\)=", 2);
                            if (tokens != null && tokens.length == 2) {
                                Key key = new Key(lineNr, parseKey(tokens[0]));
                                keys.put(key.getName(), key);
                            }
                        }
                        processNextLine = !line.endsWith("\\");
                    }
                    lineNr++;
                }
            } finally {
                br.close();
            }

            // Make a cross check that our own properties parsing code produces
            // the same result as the class Properties
            Properties props = new Properties();
            FileInputStream fin = new FileInputStream(filename);
            try {
                props.load(fin);
            } finally {
                fin.close();
            }
            for (Key key : keys.values()) {
                if (!props.containsKey(key.getName())) {
                    log(filename + ":" + key.getLine() + ": [error] found ghost key " + key.getName());
                }
            }
            for (String key : props.stringPropertyNames()) {
                if (!keys.containsKey(key)) {
                    log(filename + ": [error] missing key " + key);
                }
            }


        }

        private void grepAndReduce(String fileContent) throws BuildException {
            try {
                BufferedReader rd = new BufferedReader(new StringReader(fileContent));
                try {
                    String line;

                    while ((line = rd.readLine()) != null) {
                        for (String key : new ArrayList<String>(keys.keySet())) {
                            if (line.contains(key)) {
                                keys.remove(key);
                                continue;
                            }
                            if (isCliBundle()) {
                                for (String suffix : CLI_SUFFIXES) {
                                    if (key.endsWith(suffix)) {
                                        String stripped = key.substring(0, key.length() - suffix.length());
                                        if (line.contains('"' + stripped + '"')) {
                                            keys.remove(key);
                                            break;
//                                        } else {
//                                            if(key.equals("AddCloudAdapterCliCommand.name")) {
//                                                log(String.format("%s: Does not match against '%s' (line='%s')", filename, stripped, line));
//                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (keys.isEmpty()) {
                            break;
                        }
                    }
                } finally {
                    rd.close();
                }
            } catch (IOException ex) {
                throw new BuildException(ex.getLocalizedMessage(), ex);
            }
        }
    }
    static Comparator<Key> KEY_BY_LINE_COMPARATOR = new Comparator<Key>() {

        public int compare(Key o1, Key o2) {
            return o1.line - o2.line;
        }
    };

    static class Key {

        private final int line;
        private final String name;

        public Key(int line, String name) {
            this.line = line;
            this.name = name;
        }

        /**
         * @return the line
         */
        public int getLine() {
            return line;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }
    }

    public FileSet createJavaFiles() {
        if (javaFiles == null) {
            javaFiles = new FileSet();
        }
        return javaFiles;
    }

    public FileSet createPropertyFiles() {
        if (propertyFiles == null) {
            propertyFiles = new FileSet();
        }
        return propertyFiles;
    }

    private static String readFile(File file) throws BuildException {
        StringBuilder strBuf = new StringBuilder();
        try {
            FileReader fr = new FileReader(file);
            char [] buf = new char[4096];
            int len = 0;
            while ( (len = fr.read(buf)) > 0) {
                strBuf.append(buf, 0, len);
            }
            return strBuf.toString();
        } catch(IOException ex) {
            throw new BuildException(ex.getLocalizedMessage(), ex);
        }
    }
}
