/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.ant.monaco;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.swing.table.TableModel;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;

/**
 * AntTask for running monaco queries
 * 
 * <p>The MonacoTask executes queries on monaco web server and prints the result
 *    to stdout</p>
 * 
 * <H2>Parameters</H2>
 * 
 * <table border='1'>
 *    <tr>
 *       <th>Attribute</th><th>Description</th><th>Required</th>
 *    </tr>
 *    <tr>
 *       <td>host</td>
 *       <td>Hostname where Monaco is running</td>
 *       <td>true</td>
 *    </tr>
 *    <tr>
 *       <td>product</td>
 *       <td>The queries will retrieve only CRs for this product</td>
 *       <td>false, default is gridengine</td>
 *    </tr>
 *    <tr>
 *       <td>category</td>
 *       <td>The queries will retrieve only CRs for this category</td>
 *       <td>false, default is sdm</td>
 *    </tr>
 *    <tr>
 *       <td>failIfQueriesHasResults</td>
 *       <td>if set to true the build process will fail of any query has a result</td>
 *       <td>true</td>
 *    </tr>
 *    <tr>
 *       <td>hasResultsProperty</td>
 *       <td>The value of the property will be set to true of any query has a result</td>
 *       <td>empty</td>
 *    </tr>
 * </table>
 * 
 * <H2>Parameters specified as nested elements</H2>
 * 
 * <H3>query</H3>
 * 
 * <p>The query element defines a query</p>
 * 
 * <H4>Parameters for query elements</H4>
 * 
 * <table border='1'>
 *    <tr>
 *       <th>Attribute</th><th>Description</th><th>Required</th>
 *    </tr>
 *    <tr>
 *       <td>title</td>
 *       <td>Title of the query, will be printed to stdout of the query has a result</td>
 *       <td>true</td>
 *    </tr>
 * </table>
 * 
 * <H5>Param element for query elements</H5>
 * 
 * <p>The param element defines the filter parameters for the query. Each param has
 *    the attributes key and value. The key defines the name of the filter parameter,
 *    the value defines the value of the filter parameters.</p>
 * 
 * <H3>message elements</H3>
 * 
 * The content of message elements are printed to stdout. The monaco task knows the
 * place where the message has been defined. The messages will be printed in the
 * correct order.
 * 
 * <H2>Example</H2>
 * 
 * <pre>
 * &lt;monaco host="foo.bar"<&gt>
 *   <message>Test monaco queries</message>
 *   <query title="Test">
 *      <param key="hook2" value="null"/>
 *      <message>query Test has results</message>
 *   </query>
 *   <message>Finished</message>
 * <&lt;/monaco</gt;>
 * 
 * </pre>
 */
public class MonacoTask extends Task {

    private final static Map<String, QueryParamHandler> QUERY_PARAM_HANDLER_MAP = new HashMap<String, QueryParamHandler>();
    private final static QueryParamHandler DEFAULT_PARAM_HANDLER = new DefaultParamHandler();

    /**
     * This static initializer defines all special parameter handler
     */
    static {
        QUERY_PARAM_HANDLER_MAP.put("status", new StatusParamHandler());
    }

    
    private String product = "gridengine";
    private String category = "sdm";
    private boolean failIfQueriesHasResults = true;
    private String hasResultsProperty = null;
    private String host = null;
    private final List<Query> queries = new LinkedList<Query>();
    private final Map<Integer, Message> messageMap = new HashMap<Integer, Message>();

    /**
     * execute the ant task.
     * 
     * @throws org.apache.tools.ant.BuildException if the ant task failed
     */
    @Override
    public void execute() throws BuildException {

        if (host == null || host.length() == 0) {
            throw new BuildException("attribute host is mandatory");
        }

        Monaco monaco = new Monaco(getProject(), host);

        boolean hasResults = false;
        for (Query query : queries) {
            hasResults |= query.runQuery(monaco);
        }

        if (hasResults) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);

            int messageIndex = 0;

            for (Query query : queries) {
                Message message = messageMap.get(messageIndex);
                if (message != null) {
                    pw.println(message.getMessage());
                }
                messageIndex++;
                if (query.hasResult()) {
                    query.printResult(pw);
                }
            }

            Message message = messageMap.get(messageIndex);
            if (message != null) {
                pw.println(message.getMessage());
            }

            pw.flush();

            getProject().log(sw.toString(), Project.MSG_WARN);
            if (hasResultsProperty != null && hasResults) {
                getProject().setProperty(hasResultsProperty, "true");
            }

            if (isFailIfQueriesHasResults()) {
                throw new BuildException("Not all bugster entries are correct");
            }

        }
    }

    /**
     * Is the failIfQueriesHasResults attribute set.
     * 
     * <p>The method name must follow the java bean conventions for getters otherwise
     * ant will not recognize that the failIfQueriesHasResults member is an attribute.</p>
     * 
     * @return <code>true</code> if the ailIfQueriesHasResults attribute is set
     */
    public boolean isFailIfQueriesHasResults() {
        return failIfQueriesHasResults;
    }

    /**
     * Set the failIfQueriesHasResults attribute.
     * 
     * @param failIfQueriesHasResults the value of the failIfQueriesHasResults property
     */
    public void setFailIfQueriesHasResults(boolean failIfQueriesHasResults) {
        this.failIfQueriesHasResults = failIfQueriesHasResults;
    }

    /**
     * Get the value of hasResultsProperty attribute.
     * @return the value of the hasResultsProperty attribute
     */
    public String getHasResultsProperty() {
        return hasResultsProperty;
    }

    /**
     * Set the value of the hasResultsProperty attribute
     * 
     * @param hasResultsProperty the value of the hasResultsProperty attribute
     */
    public void setHasResultsProperty(String hasResultsProperty) {
        this.hasResultsProperty = hasResultsProperty;
    }

    /**
     * Get the value of host attribute.
     * @return the value of the host attribute
     */
    public String getHost() {
        return host;
    }

    /**
     * Set the value of the host attribute
     * 
     * @param host the value of the host attribute
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Create a new nested Message element. 
     * 
     * <p>Ant will fill the content of the returned Message object.</p>
     * 
     * <p>The new message is stored internally in the correct order,
     *    the message will be printed after the query output. If the query
     *    does not procude output the message is also printed.</p>
     * 
     * @return the nexted Message element
     */
    public Message createMessage() {
        Message ret = new Message();
        messageMap.put(queries.size(), ret);
        return ret;
    }

    /**
     * Create a new nested Query element.
     * 
     * <p>Ant will fill the content of the returned Query object.</p>
     * 
     * @return the new nested Query element
     */
    public Query createQuery() {
        Query ret = new Query();
        queries.add(ret);
        return ret;
    }

    /**
     * The message class holds 
     */
    public class Message {

        private String message;

        /**
         * Sets the content of the message element. 
         * 
         * <p>The name of the method must follow the naming conventions of ant.</p>
         * 
         * @param message
         */
        public void addText(String message) {
            this.message = getProject().replaceProperties(message);
            this.message = formatMessage(getProject().replaceProperties(message));
        }

        private String formatMessage(String message) {

            String lines[] = message.split(Pattern.quote("\n"));

            StringBuilder sb = new StringBuilder(message.length());
            for (String line : lines) {
                sb.append(String.format("%s\n", line.trim()));
            }
            return sb.toString().trim();
        }

        public String getMessage() {
            return message;
        }
    }

    /**
     * Helper class which stored the parameters for a query
     */
    public class Query {

        private String title = "No title";
        private final List<QueryParameter> params = new LinkedList<QueryParameter>();
        private TableModel result;
        private Message message;

        /**
         * Create a query parameter
         * 
         * <p>Ant will fill the content of the returned QueryParameter object.</p>
         * 
         * <p>Each QueryParameter defines a filter criteria for the Monaco query.</p>
         * 
         * @return the nexted QueryParameter element
         */
        public QueryParameter createParam() {
            QueryParameter ret = new QueryParameter();
            params.add(ret);
            return ret;
        }

        /**
         * Create a new nested Message element. 
         * 
         * <p>Ant will fill the content of the returned Message object.</p>
         * 
         * <p>The message will only be printed if the query has results.</p>
         * 
         * @return the nexted Message element
         */
        public Message createMessage() {
            this.message = new Message();
            return message;
        }

        /**
         * Get the title of the query (will be printed on the top if the query output).
         * @return the title of the query
         */
        public String getTitle() {
            return title;
        }

        /**
         * Set the title of the query
         * @param title the title of the query
         */
        public void setTitle(String title) {
            this.title = title;
        }

        private boolean runQuery(Monaco monaco) {

            log("Running query ' " + title + "'", Project.MSG_VERBOSE);

            Field[] fields = {
                new Field(Field.CR).sort('a'),
                new Field(Field.STATE),
                new Field(Field.RESP_ENG).width(40),
                new Field(Field.SUBMITED_BY).width(40),
                new Field(Field.SYNOPSIS).width(80)
            };

            Map<Integer, Integer> widthMap = new HashMap<Integer, Integer>();

            widthMap.put(57, 10);
            widthMap.put(7, 10);
            widthMap.put(51, 40);
            widthMap.put(87, -1);

            Map<String, String> filter = new HashMap<String, String>(params.size() + 2);
            filter.put("product", product);
            filter.put("categoryt", category);

            for (QueryParameter p : params) {
                QueryParamHandler ph = getParamHandler(p.getKey());
                String value = ph.valueToQueryString(p.getValue());
                filter.put(p.getKey(), value);
            }

            log("Filter params are " + filter.toString(), Project.MSG_VERBOSE);

            try {
                result = monaco.query(filter, fields);
                if (result.getRowCount() == 0) {
                    log("query did not return any rows", Project.MSG_VERBOSE);
                    return false;
                } else {
                    log("query returned  " + result.getRowCount() + " entries", Project.MSG_VERBOSE);
                    return true;
                }
            } catch (IOException ex) {
                throw new BuildException("Monaco query failed: " + ex.getLocalizedMessage(), ex);
            }
        }

        /**
         * Did the query produce any result
         * @return <code>true</code> if the runQuery has not been called or the query produced no result
         */
        public boolean hasResult() {
            return result != null && result.getRowCount() > 0;
        }

        public void printResult(PrintWriter pw) {
            pw.println();
            pw.println(title);
            for (int i = 0; i < title.length(); i++) {
                pw.print('-');
            }
            pw.println();
            if (message != null) {
                pw.println(message.getMessage());
                pw.println();
            }
            if (result.getRowCount() > 0) {
                printTextTable(result, pw);
            } else {
                pw.println("No fixed CRs found");
            }
        }
    }
    
    

    private QueryParamHandler getParamHandler(String key) {
        QueryParamHandler ret = QUERY_PARAM_HANDLER_MAP.get(key);
        if (ret == null) {
            ret = DEFAULT_PARAM_HANDLER;
        }
        return ret;
    }
    
    /** The QueryParamHandler maps the string from the ant build script into
     *  the String which is necessary for the Monaco Query. 
     */
    public interface QueryParamHandler {
        /**
         * convert parameter value from the ant script into the string unterstood
         * by Monaco.
         * 
         * @param value  the parameters from the ant script
         * @return  the string understood by Monaco
         * 
         * @throws org.apache.tools.ant.BuildException
         */
        public String valueToQueryString(String value) throws BuildException;
    }

    /**
     * The default handler makes not conversion. The parameter value from the
     * ant script is directly used for Monaco
     */
    private static class DefaultParamHandler implements QueryParamHandler {

        /**
         * Returns value with conversion.
         * 
         * @param value the value
         * @return the value parameter
         */
        public String valueToQueryString(String value) {
            return value;
        }
    }

    /**
     * This ParamHandler converts the values of the status parameters.
     * 
     * Monaco expects for the state the numerical id of the state. The mapping from
     * the state string into the numerical id is is defined in the CRState enum.
     * 
     * The status parameter in the ant script allows the following expressions
     * 
     * &lt;STATE&gt;(,&lt;STATE&gt;)* or &lt;STATE&gt; - &lt;STATE&gt;
     * 
     * &lt;STATE&gt; must be a string which can be parsed by <code>CRState.valueOf</code>
     * 
     * The returned string will have the form &lt;NUM&gt;(,&lt;NUM&gt;)* or &lt;NUM&gt;-&lt;NUM&gt;
     * where &lt;NUM&gt; the value of <code>CRState.num</code> attribute of the corresponding
     * &lt;STATE&gt;.
     * 
     */
    private static class StatusParamHandler implements QueryParamHandler {

        private CRState parse(String s) {
            try {
                return CRState.valueOf(s.trim().toUpperCase());
            } catch (IllegalArgumentException ex) {
                throw new BuildException("'" + s + "' is not avalid CR state");
            }
        }

        /**
         * Perform the conversion of the status parameter value
         * @param value the status parameter value from ant script
         * @return the status parameter understood by Monaco
         * @throws org.apache.tools.ant.BuildException if <code>value</code> does not contain a valid status expression
         */
        public String valueToQueryString(String value) throws BuildException {
            List<CRState> states = new LinkedList<CRState>();

            String[] values = value.split(Pattern.quote(","));
            for (String token : values) {

                String[] subToken = token.split(Pattern.quote("-"));

                CRState startState = CRState.DISPATCHED;
                CRState endState = CRState.FIX_CLOSED;

                switch (subToken.length) {
                    case 1:
                        startState = parse(subToken[0]);
                        endState = startState;
                        break;
                    case 2:
                        startState = parse(subToken[0]);
                        endState = parse(subToken[1]);
                        break;
                    default:
                        throw new BuildException("'" + value + "' is not valid parameter for status");
                }

                if (startState.num() == endState.num()) {
                    states.add(startState);
                } else if (startState.num() > endState.num()) {
                    CRState tmp = startState;
                    startState = endState;
                    endState = tmp;
                }

                for (CRState state : CRState.values()) {
                    if (state.equals(CRState.ALL)) {
                        continue;
                    }
                    if (state.num() >= startState.num() && state.num() <= endState.num()) {
                        states.add(state);
                    }
                }
            }
            return CRState.getQueryString(states);
        }
    }

    /**
     * Class which holds the parameters for a Monaco query
     */
    public static class QueryParameter {

        private String key;
        private String value;

        /**
         * get the key of the parameter
         * @return the key of the parameter
         */
        public String getKey() {
            return key;
        }

        /**
         * Set the key of the parameter
         * @param key the key of the parameter
         */
        public void setKey(String key) {
            this.key = key;
        }

        /**
         * Get the value of the parameter
         * @return the value of the parameter
         */
        public String getValue() {
            return value;
        }

        /**
         * Set the value of the parameter.
         * 
         * @param value  the value of the parameter
         */
        public void setValue(String value) {
            this.value = value;
        }
    }

    private static void printTextTable(TableModel tm, PrintWriter pw) {


        int colWidth[] = new int[tm.getColumnCount()];

        for (int col = 0; col < tm.getColumnCount(); col++) {
            colWidth[col] = Math.max(tm.getColumnName(col).length(), colWidth[col]);
        }
        for (int row = 0; row < tm.getRowCount(); row++) {
            for (int col = 0; col < tm.getColumnCount(); col++) {
                colWidth[col] = Math.max(tm.getValueAt(row, col).toString().length(), colWidth[col]);
            }
        }

        String format[] = new String[tm.getColumnCount()];
        for (int col = 0; col < tm.getColumnCount(); col++) {
            format[col] = "%1$-" + colWidth[col] + "s";
        }

        String del = " | ";

        for (int col = 0; col < tm.getColumnCount(); col++) {
            if (col > 0) {
                pw.print(del);
            }
            pw.printf(format[col], tm.getColumnName(col));
        }
        pw.println();
        for (int col = 0; col < tm.getColumnCount(); col++) {
            if (col > 0) {
                for (int i = 0; i < del.length(); i++) {
                    pw.print('-');
                }
            }
            for (int i = 0; i < colWidth[col]; i++) {
                pw.print('-');
            }
        }
        pw.println();
        for (int row = 0; row < tm.getRowCount(); row++) {
            for (int col = 0; col < tm.getColumnCount(); col++) {
                if (col > 0) {
                    pw.print(del);
                }
                pw.printf(format[col], tm.getValueAt(row, col));
            }
            pw.println();
        }
    }

    /**
     * Get the product for the Monaco query
     * @return the procuct
     */
    public String getProduct() {
        return product;
    }

    /**
     * Set the produce for the Monaco query
     *  
     * @param product the product
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * Get the category for the Monaco query
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Set the category for the Monaco query
     * @param category the category
     */
    public void setCategory(String category) {
        this.category = category;
    }
}
