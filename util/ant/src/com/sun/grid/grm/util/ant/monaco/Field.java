/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.ant.monaco;

/**
 * Represent a field for the result of a monaco query
 */
class Field {
    
    /** index of the resposible eng. field */
    public final static int RESP_ENG = 51;
    
    /** index of the CR field */
    public final static int CR = 7;
    
    /** index of the SUBMITED_BY field */
    public final static int SUBMITED_BY = 87;
    
    /** index of the SYNOPSIS field */
    public final static int SYNOPSIS = 90;
    
    /** index of the STATE field */
    public final static int STATE = 76;
    

    private int index;
    private Integer width;
    private char aline;
    private char sort;

    /**
     * Create a new field
     * @param index index of the column
     */
    public Field(int index) {
        this.index = index;
        aline = '-';
        sort = '-';
        width = null;
    }

    /**
     * Set the width of a field
     * @param width the width of the field
     * @return the field
     */
    Field width(int width) {
        this.width = new Integer(width);
        return this;
    }

    /**
     * the the alignment of the field ('l' = left, 'r' = right , '-' no alignment).
     * @param aline the alignment of the field
     * @return the alignment of the field
     */
    Field aline(char aline) {
        this.aline = aline;
        return this;
    }

    /**
     * Set the sort order of the field ('a' = acending, 'd' = descending, '-' do not sort)
     * @param sort  the sort order
     * @return
     */
    Field sort(char sort) {
        this.sort = sort;
        return this;
    }

    /**
     * Get the format of the field for the request url
     * @return
     */
    String getFormat() {
        String ws = width == null ? "" : Integer.toString(width);
        return String.format("%d.%c.%c.%s", getIndex(), getSort(), getAline(), ws);
    }

    /**
     * Get the index of the field
     * @return the index of the field
     */
    public int getIndex() {
        return index;
    }

    /**
     * Get the width of the field 
     * @return the width of the field
     */
    public int getWidth() {
        return width;
    }

    /**
     * Get the aligmnent of the field
     * @return the aligmnent of the field
     */
    public char getAline() {
        return aline;
    }

    /**
     * Get the sort order of the field
     * @return the sort order
     */
    public char getSort() {
        return sort;
    }
}
