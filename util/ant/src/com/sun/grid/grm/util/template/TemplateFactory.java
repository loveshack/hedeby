/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util.template;

import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The factory create a template object from a file template file. The template file
 * contains a jsp like syntax:
 *
 * Example File
 *
 * <pre>
 * &lt;%@page import="com.sun.mirror.apt.*"%&gt;
 * &lt;%@page import="com.sun.mirror.declaration.*"%&gt;
 * &lt;%@page import="java.util.*" %&gt;
 * &lt;%
 *  AnnotationProcessorEnvironment env = (AnnotationProcessorEnvironment)params.get("env");
 *  TypeDeclaration typeDecl = (TypeDeclaration)params.get("typeDecl");
 * %&gt;
 *
 * package com.sun.grid.foo;
 *
 * class Sample<%=typeDecl.getSimpleName()%> implements <%=typeDecl.getSimpleName()%>  {
 *
 * &lt;%
 *     for(MethodDeclaration md: typeDecl.getMethods()) {
 *        // do something for the method
 *     }
 * %&gt;
 *
 * }
 * </pre>
 *
 * The <code>TemplateFactory</code> do not implement the complete JSP syntax.
 * The template generation does only work of the javac compiler is in the classpath
 * of the jvm.
 *
 */
public class TemplateFactory {
    
    private File tmpDir;
    private File tmpSrcDir;
    private File tmpClassesDir;
    private String classpath;
    private ClassLoader classLoader;
    private final static Logger logger = Logger.getLogger(TemplateFactory.class.getName());
    
    /**
     * Create a new TemplateFactory
     * @param tmpDir      temp directory
     * @param classpath   classpath for the template compilation
     */
    public TemplateFactory(File tmpDir, String classpath, ClassLoader classLoader) {
        
        this.tmpDir = tmpDir;
        tmpSrcDir = new File(tmpDir, "src");
        tmpClassesDir = new File(tmpDir, "classes");
        this.classpath = classpath;
        this.classLoader  = classLoader;
        if( !tmpSrcDir.exists() ) {
            if( !tmpSrcDir.mkdir() ) {
                throw new IllegalArgumentException("Can not create src dir " + tmpSrcDir );
            }
        }
        if( !tmpClassesDir.exists() ) {
            if( !tmpClassesDir.mkdir() ) {
                throw new IllegalArgumentException("Can not create classes dir " + tmpSrcDir );
            }
        }
        
    }
    
    private String getClassName(File f) {
        
        StringBuffer buf = new StringBuffer(f.getName());
        for(int i = 0; i < buf.length(); i++ ) {
            switch(buf.charAt(i)) {
                case '.':
                case '/':
                    buf.setCharAt(i, '_');
            }
        }
        return buf.toString();
    }
    
    
    /**
     *  Create a template from a file which contains a jsp like syntax
     * @param f path
     * @throws IOException
     * @return the template file
     */
    public Template createTemplate(File f ) throws IOException {
        
        String  className = getClassName(f);
        
        
        File srcFile = new File( tmpSrcDir, className + ".java" );
        
        File classFile = new File(tmpClassesDir, className + ".class");
        
        if( !classFile.exists() || f.lastModified() > classFile.lastModified() ) {
            
            logger.fine("create template class " + className + " from file " + f);
            
            // Template class doest not exists or is older the the template file
            // Recompile it
            
            Printer p = new Printer(srcFile);
            
            List importList = collectImports(f);
            
            writeHeader(p, null, className, importList );
            writeTemplate(p, f);
            writeFooter(p, className );
            
            p.flush();
            p.close();
            
            String args [] = {
                "-d", tmpClassesDir.getAbsolutePath(),
                "-classpath", classpath,
                srcFile.getAbsolutePath()
            };
            
            try {
                Class<?> c = Class.forName("com.sun.tools.javac.Main");
                Object compiler = c.newInstance();
                Method compile = c.getMethod("compile",
                        new Class [] {(new String [] {}).getClass()});
                
                int result = ((Integer) compile.invoke
                        (compiler, new Object[] {args}))
                        .intValue();
                if( result != 0 ) {
                    throw new IOException("Compile returned " + result );
                }
            } catch (Exception ex) {
                if (ex instanceof IOException) {
                    throw (IOException) ex;
                } else {
                    
                    IOException e = new IOException("Error starting compiler");
                    e.initCause(ex);
                    throw e;
                }
            }
        }
        
        logger.fine("load template class " + className  );
        
        ClassLoader cl = new URLClassLoader( new URL [] { tmpClassesDir.toURI().toURL() }, classLoader );
        Class cls;
        try {
            cls = cl.loadClass(className);
        } catch (ClassNotFoundException ex) {
            IOException e = new IOException(ex.getMessage());
            e.initCause(ex);
            throw e;
        }
        
        Object obj;
        try {
            obj = cls.newInstance();
        } catch (InstantiationException ex) {
            IOException e = new IOException(ex.getMessage());
            e.initCause(ex);
            throw e;
        } catch (IllegalAccessException ex) {
            IOException e = new IOException(ex.getMessage());
            e.initCause(ex);
            throw e;
        }
        
        Template ret = (Template)obj;
        
        return ret;
    }
    
    
    private void writeLine(Printer p, StringBuffer line, boolean newLine ) {
        
        if(newLine) {
            p.print("p.println(\"");
        } else {
            p.print("p.print(\"");
        }
        for(int i = 0; i < line.length(); i++ ) {
            char c = line.charAt(i);
            switch(c) {
                case '"':  p.print("\\\""); break;
                case '\\': p.print("\\\\"); break;
                default: p.print(c);
            }
        }
        p.println("\");");
        line.setLength(0);
    }
    
    private char read(Reader rd) throws IOException {
        int c = rd.read();
        if( c < 0 ) {
            throw new EOFException("eof");
        }
        if( logger.isLoggable(Level.FINE)) {
            logger.finest("got char " + (char)c);
        }
        return (char)c;
    }
    
    private List<String> collectImports(File f) throws IOException {
        
        logger.fine("collectImports: " +  f );
        Reader br = new FileReader(f);
        char c = 0;
        
        char [] suffix = "<%@page".toCharArray();
        ArrayList<String> ret = new ArrayList<String>();
        
        try {
            
            while( true ) {
                logger.fine("search for <%@page");
                int index = 0;
                while(index < suffix.length) {
                    c = read(br);
                    if(Character.toLowerCase(c) != suffix[index]) {
                        break;
                    }
                    index++;
                }
                if(index == suffix.length) {
                    logger.fine("found <%@page");
                    StringBuffer line = new StringBuffer();
                    
                    while(true) {
                        c = read(br);
                        if( c == '%' ) {
                            c = read(br);
                            if( c == '>') {
                                
                                logger.fine("page statement finished");
                                
                                String [] parts = line.toString().split(" ");
                                
                                for(int i = 0; i < parts.length; i++ ) {
                                    
                                    parts[i] = parts[i].trim();
                                    
                                    int equalIndex = parts[i].indexOf('=');
                                    
                                    if(equalIndex > 0) {
                                        String option = parts[i].substring(0, equalIndex);
                                        
                                        if("import".equalsIgnoreCase(option)) {
                                            
                                            String value = parts[i].substring(equalIndex+1);
                                            int len = value.length();
                                            if(len > 0) {
                                                int startIndex = 0;
                                                int endIndex = len;
                                                if(value.charAt(0) == '"' ||
                                                        value.charAt(0) == '\'') {
                                                    startIndex=1;
                                                }
                                                if(value.charAt(len-1) == '"' ||
                                                        value.charAt(len-1) == '\'') {
                                                    endIndex = len - 1;
                                                }
                                                value = value.substring(startIndex, endIndex);
                                                
                                                logger.fine("imports: " + value);
                                                String [] imports = value.toString().split(",");
                                                
                                                for(int ii = 0; ii < imports.length; ii++) {
                                                    logger.fine("found import " + imports[ii]);
                                                    ret.add(imports[ii].trim());
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                                
                                line.setLength(0);
                            } else {
                                line.append(c);
                                line.append('%');
                            }
                        } else {
                            line.append(c);
                        }
                    }
                }
            }
        } catch( EOFException eof ) {
            logger.finer("EOF");
        }
        return ret;
    }
    
    
    private void writeTemplate(Printer p, File f) throws IOException {
        
        logger.fine("writeTemplate: " +  f );
        Reader br = new FileReader(f);
        File dir = f.getParentFile();
        char c = 0;
        StringBuffer line = new StringBuffer();
        
        try {
            while( true ) {
                c = read(br);
                if( c == '<' ) {
                    c = read(br);
                    if( c == '%') {
                        writeLine(p,line, false);
                        writeCode(p, br, dir);
                    } else {
                        line.append("<");
                        line.append(c);
                    }
                } else {
                    if( c == '\n' || c == '\r' ) {
                        writeLine(p,line, true);
                    } else {
                        line.append((char)c);
                    }
                }
            }
        } catch( EOFException eof ) {
            logger.finer("EOF");
        }
        if( line.length() > 0 ) {
            writeLine(p,line, true);
        }
    }
    
    private void writeCode(Printer p, Reader rd, File dir) throws IOException {
        
        char c = read(rd);
        
        boolean first = true;
        boolean inline = false;
        if( c == '@' ) {
            writeStaticCode(p,rd, dir);
            return;
        } else if( c == '=' ) {
            inline = true;
            p.print("p.print(");
        } else {
            p.print(c);
        }
        
        while( true ) {
            c = read(rd);
            if( c == '%' ) {
                c = read(rd);
                if( c == '>' ) {
                    if( inline ) {
                        p.print(");");
                    }
                    break;
                } else {
                    p.print("%");
                }
            } else {
                if( first ) {
                    if( c == '=' ) {
                        inline = true;
                        p.print("p.print(");
                    } else {
                        p.print(c);
                    }
                    first = false;
                } else {
                    p.print(c);
                }
            }
        }
    }
    
    
    private void writeStaticCode(Printer p, Reader rd, File dir) throws IOException {
        
        StringBuffer buf = new StringBuffer();
        char c = 0;
        
        while(true) {
            c = read(rd);
            if( c == '%' ) {
                c = read(rd);
                if( c == '>' ) {
                    break;
                } else {
                    buf.append('%');
                }
            } else {
                buf.append(c);
            }
        }
        
        String str = buf.toString().trim();
        StringTokenizer st = new StringTokenizer(str," ");
        if( st.hasMoreTokens() ) {
            String token = st.nextToken();
            
            if( "include".equals(token) ) {
                if( st.hasMoreTokens() ) {
                    token = st.nextToken();
                    
                    if( token.startsWith("file=") ) {
                        
                        String filename = token.substring(6, token.length() - 1);
                        
                        File newFile = new File(dir, filename );
                        
                        p.println();
                        p.print("// BEGIN ");
                        p.print( newFile.getCanonicalPath() );
                        p.println(" --------------------------");
                        writeTemplate(p, newFile);
                        p.println();
                        p.print("// END ");
                        p.print( newFile.getCanonicalPath() );
                        p.println(" --------------------------");
                        
                        
                    } else {
                        throw new IOException("invalid include '" + str + "'" );
                    }
                    
                    
                } else {
                    throw new IOException("invalid include '" + str + "'" );
                }
            } else if ( "page".equals(token)) {
                // skip page static block
            } else {
                throw new IOException("invalid static code '" + str + "'" );
            }
        } else {
            throw new IOException("invalid static code '" + str + "'" );
        }
    }
    
    private void writeHeader(Printer p, String packageName, String className, List importList ) {
        
        if( packageName != null ) {
            p.print("package ");
            p.print( packageName );
            p.println(";");
            
        }
        
        Iterator iter = importList.iterator();
        
        while(iter.hasNext()) {
            p.print("import ");
            p.print(iter.next());
            p.println(";");
        }
        
        p.print("public class ");
        p.print( className );
        p.print(" implements ");
        p.print(Template.class.getName());
        p.println(" {");
        p.indent();
        
        p.print("public void print(final ");
        p.print( Printer.class.getName() );
        p.println(" p, final java.util.Map params) {");
        p.indent();
        
    }
    
    private void writeFooter(Printer p, String className ) {
        
        p.deindent();
        p.println("} // end of print");
        p.deindent();
        p.print("} // end of class ");
        p.println( className );
    }
    
    
    
}
