/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.ant;

import com.sun.grid.grm.util.template.Printer;
import com.sun.grid.grm.util.template.Template;
import com.sun.grid.grm.util.template.TemplateFactory;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.Mapper;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.PatternSet;
import org.apache.tools.ant.util.FileNameMapper;

/**
 *  Ant Task for JSP like source code generation
 */
public class GeneratorTask extends Task {
    
    private Path    classpath; 
    private PatternSet classes;
    private Mapper mapperElement;
    private File    template;
    private File    tmp;
    private File    dest;
    private String  suffix;
    
    private List<Parameter> params = new LinkedList<Parameter>();
    
    /**
     * Execute this task
     * @throws org.apache.tools.ant.BuildException 
     */
    @Override
    public void execute() throws BuildException {
        
        if(template == null ) {
            throw new BuildException("template is mandatory");
        }
        
        if(classpath == null) {
            throw new BuildException("classpath is mandatory");
        }
        
        if(classes == null) {
            throw new BuildException("classes are mandatory");
        }
        
        if(mapperElement == null) {
            throw new BuildException("mapper is mandatory");
        }
        
        FileNameMapper mapper = mapperElement.getImplementation();

        String [] pathes = classpath.list();
        URL [] url = new URL[pathes.length];

        
        for(int i = 0; i < url.length; i++) {
            try {
                File file = new File(pathes[i]);
                url[i] = file.toURI().toURL();
                log("Add " + url[i] + " to classpath", Project.MSG_VERBOSE);
            } catch (MalformedURLException ex) {
                throw new BuildException(ex.getMessage(), ex);
            }
        }
        ClassLoader classLoader = new URLClassLoader(url, getClass().getClassLoader());
        
        
        log("create template factory", Project.MSG_DEBUG);
        TemplateFactory factory = new TemplateFactory(tmp, classpath.toString(), classLoader);
        
        log("create template", Project.MSG_DEBUG);
        Template template;
        try {
            template = factory.createTemplate(GeneratorTask.this.template);
        } catch (IOException ex) {
            throw new BuildException("Can not create template: " + ex.getLocalizedMessage(), ex);
        }
        
        Map<String, Object> allParams = new HashMap<String, Object>();
        String [] classnames = classes.getIncludePatterns(getProject());
        for(String classname: classnames) {
            
            log("Project class " + classname, Project.MSG_VERBOSE);
            Class clazz;
            try {
                clazz = classLoader.loadClass(classname);
            } catch (ClassNotFoundException ex) {
                log("class " + classname + " not found", Project.MSG_WARN);
                continue;
            }
            
            String [] filenames = mapper.mapFileName(clazz.getName());
            
            if(filenames == null || filenames.length == 0) {
                throw new BuildException("No mapping for " + clazz.getName() + " defined");
            } else if (filenames.length > 1) {
                throw new BuildException("Mapping for " + clazz.getName() + " is not distinct");
            }
            
            File file = new File(dest, filenames[0]);
            Printer p;
            try {
                p = new Printer(file);
            } catch (IOException ex) {
                throw new BuildException("Can not create printer: " + ex.getMessage(), ex);
            }
            
            for(Parameter param: params) {
                allParams.put(param.getKey(), param.getValue());
            }
            
            allParams.put("classname", classname);

            template.print(p, allParams);
            p.flush();
            log("Created " + file, Project.MSG_VERBOSE);
        }
    }

    public Parameter createParam() {
        Parameter ret = new Parameter();
        params.add(ret);
        return ret;
    }
    
    public Path createClasspath() {
        if(classpath == null) {
            classpath = new Path(getProject());
        }
        return classpath;
    }
    
    public PatternSet createClasses() {
        if(classes == null) {
            classes = new PatternSet();
        }
        return classes;
    }

    public Mapper createMapper() {
        if (mapperElement != null) {
            throw new BuildException("Cannot define more than one mapper",
                                     getLocation());
        }
        mapperElement = new Mapper(getProject());
        return mapperElement;
    }
    public File getTemplate() {
        return template;
    }

    public void setTemplate(File template) {
        this.template = template;
    }

    public File getTmp() {
        return tmp;
    }

    public void setTmp(File tmp) {
        this.tmp = tmp;
    }

    public File getDest() {
        return dest;
    }

    public void setDest(File dest) {
        this.dest = dest;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
    
    public static class Parameter {
        private String key;
        private String value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
    
    
}
