/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.ant;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.Execute;
import org.apache.tools.ant.types.Commandline;
import org.apache.tools.ant.types.FileSet;

/**
 * Helper task for executing jhindexer.
 *
 * Creates a configuration file for the jhindexer and executes
 * the jhindex executable (located in ${javahelpHome}/bin/jhindexer)
 *
 * <h3>Parameters</h3>
 *
 * <table border="1" cellpadding="2" cellspacing="0">
 *   <tr><th>Parameter</th><th>Description</th><th>Required</th></tr>
 *   <tr>
 *      <td valign="top">javahelphome</td>
 *      <td valign="top">Path to java help installation. The jhindexer executable
 *          is searched in ${javahelphome}/bin/jhindexer
 *      </td>
 *      <td valign="top">true</td>
 *   </tr>
 *   <tr>
 *      <td valign="top">dbdir</td>
 *      <td valign="top">Path the java help database directory (-db option
 *         of jhindexer
 *      </td>
 *      <td valign="top">true</td>
 *   </tr>
 * </table>
 *
 * <H3>Nested Elements</H3>
 *
 * <H4>files</h4>
 *
 * Defines a fileset of file which are included into the javahelp search
 * database. The basedir of the fileset is removed from the document pathes
 *
 * <H3>Example</H3>
 *
 * <pre>
 *
 *  &lt;jhindexer javahelphome="/tools/javahelp"
 *             dbdir="${build.dir}/help/JavaHelpSearch"&gt;
 *     &lt;files dir="${build.dir}/help"&gt;
 *         &lt;include name="*.html"/&gt;
 *         &lt;include name="*.xhtml"/&gt;
 *     &lt;/files&gt;
 *  &lt;/jhindexer&gt;
 *
 * </pre>
 * 
 */
public class JavaHelpIndexer extends Task {
    
    private File javahelpHome;
    
    private FileSet files;
    private File    dbDir;
    
    private Commandline cmd = new Commandline();
    
    /** Creates a new instance of JavaHelpIndexer */
    public JavaHelpIndexer() {
    }
    
    private File createConfigFile() throws BuildException {
        
        try {
            File file = File.createTempFile("JavaHelpIndexer", ".config");

            PrintWriter pw = new PrintWriter(file);

            DirectoryScanner scanner = files.getDirectoryScanner(getProject());
            
            pw.print("IndexRemove ");
            pw.println(scanner.getBasedir().getAbsolutePath() + File.separatorChar);
            
            for(String filename: scanner.getIncludedFiles()) {
                pw.print("File ");
                File incFile = new File(scanner.getBasedir(), filename);
                pw.println(incFile.getAbsolutePath().replace('\\', '/'));
            }

            pw.flush();
            pw.close();

            return file;
        } catch(IOException ioe) {
            throw new BuildException("Can not create jhindexer config file: " + ioe.getLocalizedMessage(), ioe);
        }
    }
    
    /**
     * Execute the java help indexer
     *
     * @throws org.apache.tools.ant.BuildException 
     */
    public void execute() throws BuildException {
        
        if(files == null) {
            log("No files for jhindexer");
            return;
        }
        
        if(getDbDir() == null) {
            throw new BuildException("dbDir is mandatory");
        }
        
        if(!getDbDir().isDirectory()) {
            throw new BuildException("dbDir must point to a directory");
        }
        

        if(getJavahelpHome() == null) {
            throw new BuildException("javahelpHome is mandatory");
        }
        
        if(!getJavahelpHome().isDirectory()) {
            throw new BuildException("javahelpHome must point to a directory");
        }
        
        File binDir = new File(getJavahelpHome(), "bin");
        
        File jhIndexerFile = new File(binDir, "jhindexer");
        
        if(!jhIndexerFile.exists()) {
            throw new BuildException(jhIndexerFile + " not found");
        }
        
        
        Commandline toExecute = (Commandline) cmd.clone();

        toExecute.setExecutable(jhIndexerFile.getAbsolutePath());        
        toExecute.createArgument().setValue("-db");
        toExecute.createArgument().setFile(getDbDir());
        
        File configFile = createConfigFile();
        try {
            toExecute.createArgument().setValue("-c");
            toExecute.createArgument().setFile(configFile);        
            Execute exe = new Execute();
            exe.setAntRun(getProject());

            /*
             * No reason to change the working directory as all filenames and
             * path components have been resolved already.
             *
             * Avoid problems with command line length in some environments.
             */
            exe.setWorkingDirectory(null);

            try {
                exe.setCommandline(toExecute.getCommandline());
                int ret = exe.execute();
                if (ret != 0) {
                    throw new BuildException("jhindexer returned " + ret, getLocation());
                }
            } catch (IOException e) {
                throw new BuildException("jhindexer failed: " + e, e, getLocation());
            }
        } finally {
            configFile.delete();
        }
    }

    public File getJavahelpHome() {
        return javahelpHome;
    }

    public void setJavahelpHome(File javahelpHome) {
        this.javahelpHome = javahelpHome;
    }

    public File getDbDir() {
        return dbDir;
    }

    public void setDbDir(File dbDir) {
        this.dbDir = dbDir;
    }
    
    public FileSet createFiles() {
        if(files == null) {
            files = new FileSet();
        }
        return files;
    }
}
