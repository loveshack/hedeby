/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.ant.monaco;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Pattern;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.tools.ant.Project;

/**
 *  Helper class for running monaco queries
 * 
 *  <p>Monaco is a dynamic web page. With http GET request query on the bug database
 *  can be executed.</p>
 * 
 *  <p>The GET request has the following form:</p>
 * 
 *  <pre>
 *     http://&lt;hostname&gt;/list.jsp?(&lt;filter_param&gt;)*(&fieldids=&lt;fields_ids&gt;)(&fields==&lt;field_formats&gt;)&text=pipe
 *  </pre>
 * 
 *  <p>The <code>&lt;hostname&gt;</code> parameter is set via the constructor.</p>
 * 
 *  <p>The following query parameters are supported:</p>
 * 
 *  <table border="1">
 *   <tr>
 *       <th>Parameters</th><th>Description</th>
 *   </tr>
 *   <tr>
 *       <td><code>filter_param</code></td>
 *       <td>The filter parameters are define with the parameter <code>filter</code> of the
 *           method <code>runQuery</code>. This parameter is a <code>Map</code> containing
 *           key value pairs for the filters.<br>
 *           The create filter parameter for the query has the form <code>&lt;key&gt;=&lt;value&gt;</code>.
 *       </td>
 *   </tr>
 *   <tr>
 *       <td><code>field_ids</code></td>
 *       <td>
 *       The field ids is a comma separated list if id for the column which should be included
 *       in the output. The <code>field_ids</code> are defined with the <code>fields</code> parameter
 *       of the <code>runQuery</code> method. <code>Field#getIndex</code> returns the field id.
 *       </td>
 *   </tr>
 *   <tr>
 *       <td><code>field_formats</code></td>
 *       <td>the field_formats defines the width and the alignment of the columns included in the result.
 *            This parameter is a comma seperated list of field format specifiers. A field format specifier
 *            has the format <code>&lt;field_id&gt;.&lt;sort order&gt;.&lt;alignment&gt;.&lt;width&gt;  where
 *            <ul>
 *               <li><code>&lt;sort order&gt;</code> can be <code>a<code> for ascending, <code>d</code> descending and <code>-</code> for unsorted</li>
 *               <li><code>&lt;alignment&gt;</code> can be <code>l</code> for left aligned, or <code>r</code> right aligned or <code>- for default alignment.</li>
 *               <li><code>&lt;width&gt;</code> is a number defining the width of the column in character</li>
 *            </ul>
 *    </td>
 *   </tr>
 * 
 *  </table>
 * 
 *   <p>For each query the <code>text=pipe</code> parameter is hard code that that the result is formated in a plain text
 *      with <code>|<code> as delimiter, e.g.:
 * 
 *   <pre>
 *        ID  |STA|RESP ENG                                |SUBMITTED_BY      |SYNOPSIS           |
 *      8015  |dis|-                                       |mr.foo@bar.com    |sample             |
 *      8016  |dis|-                                       |mr.foo@bar.com    |sample1            |
 *
 *    </pre>
 * 
 */
class Monaco {
    
    
    private final String host;
    private final Project project;
    
    /**
     * Create a new instanceof Monaco
     * @param project  the ant project (used for logging)
     * @param host     the host where the monaco web service is running
     */
    public Monaco(Project project, String host) {
        this.project = project;
        this.host = host;
    }
    
    private String runQuery(Map<String, String> filter, Field[] fields) throws IOException {
        StringBuilder urlStr = new StringBuilder(1024);
        urlStr.append("http://");
        urlStr.append(host);
        urlStr.append("/list.jsp?");
        for (Map.Entry<String, String> entry : filter.entrySet()) {
            urlStr.append(entry.getKey());
            urlStr.append("=");
            // TODO encode the value
            urlStr.append(entry.getValue());
            urlStr.append("&");
        }
        urlStr.append("text=pipe");
        urlStr.append('&');
        urlStr.append("fieldids=");
        boolean first = true;
        for (Field field : fields) {
            if (first) {
                first = false;
            } else {
                urlStr.append(',');
            }
            urlStr.append(field.getIndex());
        }
        urlStr.append('&');
        urlStr.append("fields=");

        boolean isFirst = true;
        for (Field field : fields) {
            if (isFirst) {
                isFirst = false;
            } else {
                urlStr.append(',');
            }
            urlStr.append(field.getFormat());
        }

        project.log("Running query '" + urlStr.toString() + "'", Project.MSG_DEBUG);
        
        HttpURLConnection con = (HttpURLConnection) new URL(urlStr.toString()).openConnection();

        con.setDoInput(true);
        con.connect();

        StringBuilder sb = new StringBuilder();

        InputStream in = con.getInputStream();
        try {
            InputStreamReader rd = new InputStreamReader(in);
            char buf[] = new char[1024];
            int len = 0;
            while ((len = rd.read(buf)) > 0) {
                sb.append(buf, 0, len);
            }
        } finally {
            in.close();
        }
        
        String ret = sb.toString();
        project.log(String.format("Result of query -------------------------\n%s\n------------------------", ret),
                    Project.MSG_DEBUG);
        return ret;
    }

    /**
     * Run a monaco query
     * @param filter   the filter
     * @param fields   the fields included in the output
     * @return  the returned URL
     * @throws IOException on any IO error
     */
    public TableModel query(Map<String, String> filter, Field[] fields) throws IOException {

        String str = runQuery(filter, fields);

        String[] lines = str.split(Pattern.quote("\n"));

        int lineIndex = 0;

        String[] columnNames = null;

        while (lineIndex < lines.length) {
            String line = lines[lineIndex].trim();
            if (line.length() > 0 && line.charAt(line.length() - 1) == '|') {
                // we found the first line
                columnNames = line.substring(0, line.length() - 1).split(Pattern.quote("|"));
                for (int col = 0; col < columnNames.length; col++) {
                    columnNames[col] = columnNames[col].trim();
                }
                project.log(String.format("Line %d contains header: %s", lineIndex, Arrays.toString(columnNames)), Project.MSG_DEBUG);
                lineIndex++;
                break;
            } else {
                project.log(String.format("Skipping line %d", lineIndex), Project.MSG_DEBUG);
            }
            lineIndex++;
        }

        if (columnNames == null) {
            throw new IOException("Column line not found in output");
        }

        DefaultTableModel ret = new DefaultTableModel();

        ret.setColumnIdentifiers(columnNames);
        while (lineIndex < lines.length) {
            String line = lines[lineIndex].trim();
            if (line.length() > 0 && line.charAt(line.length() - 1) == '|') {
                String[] values = line.substring(0, line.length() - 1).split(Pattern.quote("|"));
                for (int col = 0; col < columnNames.length; col++) {
                    values[col] = values[col].trim();
                }
                project.log(String.format("Line %d: %s", lineIndex, Arrays.toString(values)), Project.MSG_DEBUG);
                ret.addRow(values);
            } else {
                project.log(String.format("Skipping line %d", lineIndex), Project.MSG_DEBUG);
            }
            lineIndex++;
        }
        return ret;
    }

}
