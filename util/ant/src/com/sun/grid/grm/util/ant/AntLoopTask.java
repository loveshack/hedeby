/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.ant;

import java.io.File;
import java.util.StringTokenizer;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.SubAnt;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.util.FileUtils;

/**
 *  This task tokenizer a string into a list of modules
 *  for each of this module the basedir is specified
 *  in a property with name "${module}.dir".
 *  for each of this module the target in it's build file is called.
 *
 *  <H2>Example</H2>
 *
 *  The follwing sniplet of a ant script call the target
 *  compile in the antscripts ../module1 and ../module2.
 *
 *  <pre>
 *
 *  ...
 *  &lt;property name="modules" value="module1, module2"/&gt;
 *  &lt;property name="module1.dir" location="../module1"/&gt;
 *  &lt;property name="module1.dir" location="../module2"/&gt;
 *
 *  &lt;antloop target="compile" modules="module1, module2
 *           delimiter="," 
 *           inheritall="true", 
 *           inheritrefs="true"/&gt;
 *  ...
 *  </pre>
 *  
 */
public class AntLoopTask extends Task {
    private String modules;
    private String delimiter = ",";
    private boolean trim = true;
    private boolean inheritAll = true;
    private boolean inheritRefs = false;
    private String target;
    
    /**
     * Execute the antloop
     * @throws org.apache.tools.ant.BuildException if the antloop failed
     */
    public void execute() throws BuildException {
        if(modules == null) {
            throw new BuildException("modules is mandatory");
        }
        if(target == null) {
            throw new BuildException("target is mandatory");
        }
        
        if(delimiter == null) {
            throw new BuildException("target is mandatory");
        }
        
        StringTokenizer st = new StringTokenizer(modules, delimiter);
        while(st.hasMoreTokens()) {
            String module = st.nextToken();
            
            if(trim) {
                module = module.trim();
            }

            String moduleDirStr = getProject().getProperty(module + ".dir");
            if(moduleDirStr == null) {
                throw new BuildException("Property " + module + ".dir is not defined");
            }
            File moduleDir = null;
            if(FileUtils.isAbsolutePath(moduleDirStr)) {
                moduleDir = new File(moduleDirStr);
            } else {
                moduleDir = new File(getProject().getBaseDir(), moduleDirStr);
            }
            
            if(!moduleDir.isDirectory()) {
                throw new BuildException("directory '" + moduleDir + "' does not exist");
            }
            log("run ant " + getTarget() + " in directory " + moduleDir, Project.MSG_VERBOSE);
            SubAnt ant = (SubAnt)getProject().createTask("subant");
            ant.setOwningTarget(getOwningTarget());
            ant.init();
            ant.setInheritall(inheritAll);
            ant.setInheritrefs(inheritRefs);
            ant.setTarget(getTarget());
//            ant.setAntfile(moduleDir+"/build.xml");
            Path p = new Path(getProject());
            p.setPath(moduleDirStr);
            ant.setBuildpath(p);
            ant.execute();                                    
        }
    }
    
    /**
     * Get the modules
     * @return the list of modules as string
     */
    public String getModules() {
        return modules;
    }
    
    /**
     * set the modules
     * @param modules the modules
     */
    public void setModules(String modules) {
        this.modules = modules;
    }
    
    /**
     * Get the call target
     * @return the called target
     */
    public String getTarget() {
        return target;
    }
    
    /**
     * set the called target
     * @param target the target
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * get the delimiter for the module list
     * @return the delimiter for the module list
     */
    public String getDelimiter() {
        return delimiter;
    }

    /**
     * set the delimiter for the module list
     * @param delimiter delimiter for the modules list
     */
    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    /**
     * Determine if the token of the module list should be trimmed
     * @return <CODE>true</CODE> if the token should be trimmed
     */
    public boolean isTrim() {
        return trim;
    }

    /**
     * set if the tokens of the module string should be trimmed
     * @param trim the trim flag
     */
    public void setTrim(boolean trim) {
        this.trim = trim;
    }

    /**
     * Determin of the called ant targets should inherit all
     * @return <CODE>true</CODE> if the called targets should inherit all
     */
    public boolean isInheritAll() {
        return inheritAll;
    }

    /**
     * Set the inheritall flag
     * @param inheritAll new value for the inherit all flag
     */
    public void setInheritAll(boolean inheritAll) {
        this.inheritAll = inheritAll;
    }

    /**
     * Determine if the called target should inherit all references
     * @return <CODE>true</CODE> if the called targets should inherit all references
     */
    public boolean isInheritRefs() {
        return inheritRefs;
    }

    /**
     * set the inheritrefs flag
     * @param inheritRefs the new value for the inheritrefs flag
     */
    public void setInheritRefs(boolean inheritRefs) {
        this.inheritRefs = inheritRefs;
    }
    
    
}
