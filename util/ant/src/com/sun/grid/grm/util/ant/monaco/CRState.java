/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.ant.monaco;

import java.util.List;

/**
 * The enum describes all possible states of CR.
 */
public enum CRState {

    DISPATCHED     (1), 
    INCOMPLETE     (2), 
    ACCEPTED       (3), 
    DEFER          (4), 
    CAUSE_KNOWN    (5),
    FIX_UNDERSTOOD (6), 
    FIX_INPROGRESS (7), 
    FIX_AVAILABLE  (8), 
    FIX_FAILED     (9),
    FIX_DELIVERED  (10), 
    FIX_CLOSED     (11),
    ALL            (12);
    
    /** contains the state number as known in the bug database*/
    private final int num;

    CRState(int num) {
        this.num = num;
    }

    /**
     * Get the number of the CRState
     * @return the number of the CRState
     */
    public int num() {
        return num;
    }

    /**
     * Get the string representation of the CRState which is usable for a monaco query
     * @return the string representation
     */
    public String toQueryString() {
        switch (this) {
            case ALL:
                return "All";
            default:
                return Integer.toHexString(num).toUpperCase();
        }
    }

    /**
     * Get the monaco query string for list of CRState
     * @param states the list of CRStates
     * @return the monaco query string
     */
    public static String getQueryString(List<CRState> states) {
        if (states.contains(CRState.ALL)) {
            return CRState.ALL.toQueryString();
        } else {
            StringBuilder ret = new StringBuilder();

            boolean first = true;
            for (CRState state : states) {
                if (first) {
                    first = false;
                } else {
                    ret.append(',');
                }
                ret.append(state.toQueryString());
            }
            return ret.toString();
        }
    }
}
