/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.ant;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;

/**
 *  <p>Ant task for getting the CVS tag.</p>
 *
 *  <p>The CVSTag ant task looks into the CVS subdirectory of the directory
 *  which is specified in the <code>dir</code> property. If the Tag file
 *  exists, the content is taken as CVS tag. If not the HEAD is assumed.</p>
 *
 *  <p>The name of the HEAD task can be spedified in with <code>head</code> property
 *  (default is <code>maintrunk</code>).</p>
 *
 *  <p>The found CVS tag is stored in a ant project property. This name of the
 *  project property must been defined in the property property</p>
 *
 *  <p>if the CVS directory in the specified directory does not exit the project property
 *  is set to the value of property <code>unknown</code> (default "unknown").</p>
 * Fixme: setting the properties doesn't currently work.
 *
 *  <H2>Example</H2>
 *
 *  <pre>
 *  ...
 *     &lt;target name="cvstag"&gt;
 *          &lt;cvstag dir="${basedir}" property="project.cvs.tag" head="maintrunc" unknown="unknown"/&gt;
 *          &lt;echo&gt;CVS Tag is ${project.cvs.tag}&lt;/echo&gt;
 *     &lt;/target&gt;
 *  </pre>
 *  
 */
public class CVSTag extends Task {
    
    private File dir;
    private String property;
    private String head = "maintrunk";
    private String unknown = "1.0u5";
    
    private String getTagFromFile(File tagFile) throws BuildException {
        String ret = "1.0u5";
        log("Reading tag file " + tagFile, Project.MSG_VERBOSE);
        try {
            FileReader fr = new FileReader(tagFile);
            BufferedReader rd = new BufferedReader(fr);
            
            String buf = null;
            while( (buf = rd.readLine()) != null) {
                if(buf.startsWith("T") || buf.startsWith("N") || buf.startsWith("D") ) {
                    ret = buf.substring(1);
                    break;
                }
            }            
            rd.close();
        } catch(IOException ioe) {
            throw new BuildException("I/O error while reading tag file " + tagFile + ": " + ioe.getLocalizedMessage(), ioe);
        }
        return ret;
    }
    
    
    /**
     * Execute the CVSTag task
     *
     * @throws org.apache.tools.ant.BuildException 
     */
    @Override
    public void execute() throws BuildException {

        if( dir == null) {
            throw new BuildException("dir is mandatory");
        }
        
        if (!dir.isDirectory()) {
            throw new BuildException("dir must point to a directory");
        }
        
        if (property == null) {
            throw new BuildException("property is mandatory");
        }

        String tag = unknown;
        
        File cvsDir = new File(dir, "CVS");
        
        if(cvsDir.isDirectory()) {
            File tagFile = new File(cvsDir, "Tag");

            if(tagFile.exists()) {
                tag = getTagFromFile(tagFile);
            } else {
                tag = head;
            }
        }
        getProject().setProperty(property, tag);
    }

    public File getDir() {
        return dir;
    }

    public void setDir(File dir) {
        this.dir = dir;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getUnknown() {
        return unknown;
    }

    public void setUnknown(String unknown) {
        this.unknown = unknown;
    }
}
