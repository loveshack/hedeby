/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.test.resource;

import com.sun.grid.grm.resource.AbstractResourceType;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.ResourcePropertyType;
import java.util.Map;

public class TestResourceType extends AbstractResourceType {

    private final static long serialVersionUID = -2009080301L;
    
    public static final ResourcePropertyType FAKE_HOSTNAME = ResourcePropertyType.newMandatory("resourceHostname", String.class);
    private final static TestResourceType instance = new TestResourceType();

    private TestResourceType() {
        addProperty(FAKE_HOSTNAME);
    }

    public static TestResourceType getInstance() {
        return instance;
    }

    public String getName() {
        return "test";
    }

    public String getResourceName(Map<String, Object> properties) throws InvalidResourcePropertiesException {
        return (String)FAKE_HOSTNAME.getValue(properties);
    }

    public boolean isResourceBound(Map<String, Object> properties) {
        return properties.containsKey(FAKE_HOSTNAME.getName());
    }

    @Override
    protected void setPropertiesForResourceName(String name, Map<String, Object> props) {
        props.put(FAKE_HOSTNAME.getName(), name);
        super.setPropertiesForResourceName(name, props);
    }

}
