/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.test.cli;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.util.Hostname;

/**
 * Test Cli Command for executing scripts on the executor
 */
@CliCommandDescriptor(
    name = "ShowConnectionEventsCliCommand",
    hasShortcut = true,
    category = CliCategory.TEST,
    bundle="com.sun.grid.grm.test.cli.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ShowConnectionEventsCliCommand extends AbstractCliCommand {

    @CliOptionDescriptor(name = "ShowConnectionEventsCliCommand.c", numberOfOperands = 1, required = true)
        private String component = null;
    @CliOptionDescriptor(name = "ShowConnectionEventsCliCommand.h", numberOfOperands = 1, required = true)
        private String host = null;
    
    public void execute(AbstractCli cli) throws GrmException {
        try {

            Hostname hostname = Hostname.getInstance(host);
            ExecutionEnv env = cli.getExecutionEnv();

            GrmComponent comp = ComponentService.<GrmComponent>getComponentByNameTypeAndHost(env, component, GrmComponent.class, hostname);
            if (comp == null) {
                throw new GrmException("Component " + component  + " not found at host " + hostname);
            }
            
            comp.addComponentEventListener(new MyComponentListener());

            Thread.sleep(Integer.MAX_VALUE);
        } catch (InterruptedException ex) {
            // Ignore
        }
    }
    
    
    
    class MyComponentListener implements ComponentEventListener {

        public void connectionClosed() {
            System.out.println("connectionClosed");
        }

        public void connectionFailed() {
            System.out.println("connectionFailed");
        }

        public void eventsLost() {
            System.out.println("eventsLost");
        }

        public void componentUnknown(ComponentStateChangedEvent event) {
            System.out.println(event);
        }

        public void componentStarting(ComponentStateChangedEvent event) {
            System.out.println(event);
        }

        public void componentStarted(ComponentStateChangedEvent event) {
            System.out.println(event);
        }

        public void componentStopping(ComponentStateChangedEvent event) {
            System.out.println(event);
        }

        public void componentStopped(ComponentStateChangedEvent event) {
            System.out.println(event);
        }

        public void componentReloading(ComponentStateChangedEvent event) {
            System.out.println(event);
        }
        
    }
    

}
