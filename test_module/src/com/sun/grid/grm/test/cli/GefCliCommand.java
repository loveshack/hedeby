/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.test.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.gef.ActionConfig;
import com.sun.grid.grm.config.gef.DefaultGefConfig;
import com.sun.grid.grm.gef.GefAction;
import com.sun.grid.grm.gef.ActionExecutor;
import com.sun.grid.grm.gef.ActionFactory;
import com.sun.grid.grm.gef.GefStepException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.util.GrmFormatter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;

/**
 * Test Cli Command for executing scripts on the executor
 */
@CliCommandDescriptor(
    name = "GefCliCommand",
    hasShortcut = true,
    category = CliCategory.TEST,
    bundle="com.sun.grid.grm.test.cli.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class GefCliCommand extends AbstractCliCommand {

    @CliOptionDescriptor(name = "GefCliCommand.r", numberOfOperands = 1, required = true)
        private String resourceIdOrName;
    @CliOptionDescriptor(name = "GefCliCommand.f", numberOfOperands = 1, required = true)
        private File gefFile;
    @CliOptionDescriptor(name = "GefCliCommand.g", numberOfOperands = 1, required= false)
        private File globalParamsFile;


    public void execute(AbstractCli cli) throws GrmException {

        DefaultGefConfig gefConfig = (DefaultGefConfig) XMLUtil.load(gefFile);

        Map<String,Object> globalParams = new HashMap<String,Object>();
        if (globalParamsFile != null) {
            try {
                Properties props = new Properties();
                props.load(new FileReader(globalParamsFile));
                for(String key: props.stringPropertyNames()) {
                    globalParams.put(key, props.get(key));
                }
            } catch (IOException ex) {
                throw new GrmException(ex.getLocalizedMessage(), ex);
            }
        }
        ResourceProvider rp = ComponentService.getComponentByType(cli.getExecutionEnv(), ResourceProvider.class);

        Resource resource = rp.getResourceByIdOrName(resourceIdOrName);

        GefAction action = ActionFactory.newAction(cli.getExecutionEnv(), "action", gefConfig.getAction(), gefConfig);

        Handler handler = new ConsoleHandler();
        handler.setLevel(Level.ALL);

        GrmFormatter fmt = new GrmFormatter("gef", false,
            new GrmFormatter.Column[] {  GrmFormatter.Column.TIME, GrmFormatter.Column.LEVEL, GrmFormatter.Column.MESSAGE } );

        handler.setFormatter(fmt);
        
        ActionExecutor exe = action.newActionExecutor(cli.getExecutionEnv(), resource, handler, globalParams);

        List<Collection<ResourceChanged>> changes = new LinkedList<Collection<ResourceChanged>>();
        try {
            while(exe.hasNextStep()) {
                changes.add(exe.executeNextStep());
            }
        } catch(GefStepException ex) {
            cli.err().printlnDirectly(ex.getLocalizedMessage());
        }
        for(Collection<ResourceChanged> stepChanges: changes) {
            cli.out().printlnDirectly(stepChanges.toString());
        }

        cli.out().printlnDirectly("Resource: " + resource.getProperties().toString());
    }



}
