/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.test.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommand;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 *
 */
@CliCommandDescriptor(name = "PrintPrivilegeTableCliCommand", hasShortcut = false, category = CliCategory.TEST, bundle = "com.sun.grid.grm.test.cli.messages", requiredPrivileges = {UserPrivilege.READ_BOOTSTRAP_CONFIG})
public class PrintPrivilegeTableCliCommand extends AbstractSortedTableCliCommand {

    public void execute(AbstractCli cli) throws GrmException {

        cli.loadAllExtensions();
        Collection<Class<? extends CliCommand>> cmds = new HashSet<Class<? extends CliCommand>>(cli.getCommands());

        PrivilegeTableModel tm = new PrivilegeTableModel(cmds);
        Table table = createTable(tm, cli);
        table.print(cli);

    }

    private class PrivilegeTableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = -2008081201L;
        private final List<Class<? extends CliCommand>> commands;

        public PrivilegeTableModel(Collection<Class<? extends CliCommand>> cmds) {
            commands = new ArrayList<Class<? extends CliCommand>>(cmds);
        }

        public int getRowCount() {
            return commands.size();
        }

        public int getColumnCount() {
            return UserPrivilege.ALL_PRIVILEGES.length + 1;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {

            Class<? extends CliCommand> cmd = commands.get(rowIndex);
            CliCommandDescriptor descr = cmd.getAnnotation(CliCommandDescriptor.class);
            switch (columnIndex) {
                case 0:
                    return AbstractCli.getName(descr, cmd.getClassLoader());
                default:
                    String reqPriv = UserPrivilege.ALL_PRIVILEGES[columnIndex - 1];
                    for (String priv : descr.requiredPrivileges()) {
                        if (priv.equals(reqPriv)) {
                            return "m";
                        }
                    }
                    for (String priv : descr.optionalPrivileges()) {
                        if (priv.equals(reqPriv)) {
                            return "o";
                        }
                    }
                    return "";
            }

        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return "command";
                default:
                    return UserPrivilege.getName(UserPrivilege.ALL_PRIVILEGES[columnIndex - 1]);
            }
        }
    }
}
