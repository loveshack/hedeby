/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.test.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.executor.RemoteFile;
import com.sun.grid.grm.executor.Result;
import com.sun.grid.grm.executor.impl.ShellCommand;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.util.List;

/**
 * Test Cli Command for executing commands and scripts on the executor
 */
@CliCommandDescriptor(
    name = "ExecuteCliCommand",
    hasShortcut = true,
    category = CliCategory.TEST,
    bundle="com.sun.grid.grm.test.cli.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ExecuteCliCommand extends AbstractCliCommand {

    @CliOptionDescriptor(name = "ExecuteCliCommand.c", numberOfOperands = 1)
        private String component = null;
    @CliOptionDescriptor(name = "ExecuteCliCommand.h", numberOfOperands = 1, required = true)
        private String host = null;
    @CliOptionDescriptor(name = "ExecuteCliCommand.e", numberOfOperands= 1, required = false)
        private String command = null;
    @CliOptionDescriptor(name = "ExecuteCliCommand.script", numberOfOperands= 1, required = false)
        private String script = null;
    @CliOptionDescriptor(name = "ExecuteCliCommand.u", numberOfOperands= 1, required = false)
        private String user;

    @CliOptionDescriptor(name = "ExecuteCliCommand.env", list=true, numberOfOperands= 1, required = false)
        private List<String> environment;
    
    @CliOptionDescriptor(name = "ExecuteCliCommand.mx", numberOfOperands= 1, required = false)
        private int maxRuntime;
    
    
    public void execute(AbstractCli cli) throws GrmException {

        // check that exactly one of -e or -script is given
        boolean requiredArgumentsOK = (command != null) != (script != null);
        if (!requiredArgumentsOK) {
            throw new GrmException("You need to specify exactly one of -e or -script parameter.");
        }

        Hostname hostname = Hostname.getInstance(host);
        ExecutionEnv env = cli.getExecutionEnv();
        
        Executor executor = null;
        if (component == null) {
            executor = ComponentService.<Executor>getComponentByTypeAndHost(env, Executor.class, hostname);
        } else {
            executor = ComponentService.<Executor>getComponentByNameTypeAndHost(env, component, Executor.class, hostname);
        }

        if (executor == null) {
            throw new GrmException("No executor found");
        }
        
        // handle special cases of -e cleanup first
        if ("cleanup".equals(command)) {
            executor.cleanUp();
            cli.out().printlnDirectly("executors spool directory cleared");
            return;
        }

        ShellCommand cmd = new ShellCommand();
        if (script != null) {
            // execute script
            File scriptFile = new File(script);
            cmd.setScript(new RemoteFile(scriptFile, scriptFile.getName()));
        } else {
            // execute command
            cmd.setCommand(command);
        }

        if (user != null) {
            cmd.setUser(user);
        }
        if (maxRuntime != 0) {
            cmd.setMaxRuntime(maxRuntime * 1000);
        }
        if (environment != null && environment.size() > 0) {
            for (String line : environment) {
                int index = line.indexOf('=');
                if (index <= 0) {
                    throw new GrmException("''{0}'' is not valid enviroment", line);
                }
                String name = line.substring(0, index);
                String value = line.substring(index + 1);
                cmd.addEnv(name, value);
            }
        }
        Result result = executor.execute(cmd);

        cli.out().printlnDirectly("Result: " + result.toString());
        cli.out().printlnDirectly(result.getResultAsString(""));
    }
}
