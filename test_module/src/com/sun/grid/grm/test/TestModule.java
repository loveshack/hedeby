/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.test;

import com.sun.grid.grm.bootstrap.AbstractModule;
import com.sun.grid.grm.bootstrap.ConfigPackage;
import com.sun.grid.grm.cli.CliCommand;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.test.cli.ExecuteCliCommand;
import com.sun.grid.grm.test.cli.GefCliCommand;
import com.sun.grid.grm.test.cli.PrintPrivilegeTableCliCommand;
import com.sun.grid.grm.test.cli.ShowConnectionEventsCliCommand;
import com.sun.grid.grm.test.cli.ValidateCliCommand;
import com.sun.grid.grm.test.resource.TestResourceType;
import com.sun.grid.grm.validate.Validator;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The test module contains functionality which must not be included into
 * the distribution.
 * 
 */
public class TestModule extends AbstractModule {

    public TestModule() {
        super("test");
    }
    
    @Override
    protected void loadCliExtension(Set<Class<? extends CliCommand>> extensions) {
        extensions.add(ExecuteCliCommand.class);
        extensions.add(ValidateCliCommand.class);
        extensions.add(ShowConnectionEventsCliCommand.class);
        extensions.add(PrintPrivilegeTableCliCommand.class);
        extensions.add(GefCliCommand.class);
    }

    @Override
    protected void loadValidatorExtension(Set<Class<? extends Validator>> extensions) {
        // No validator available
    }

    public String getVendor() {
        return "Sun Microsystems";
    }

    public String getVersion() {
        return "1.0";
    }

    public List<ConfigPackage> getConfigPackages() {
        return Collections.<ConfigPackage>emptyList();
    }
    
    @Override
    public Set<ResourceType> getResourceTypes() {
        Set<ResourceType> rt = new HashSet<ResourceType>(1);
        rt.add(TestResourceType.getInstance());
        return rt;
    }
}
