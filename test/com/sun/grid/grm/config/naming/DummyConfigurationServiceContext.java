/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.config.naming;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.I18NManager;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.naming.Binding;
import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.NotContextException;



public class DummyConfigurationServiceContext implements Context {
    
    private static final String BUNDLE = "com.sun.grid.grm.config.naming.dummymessages";
    //bundle containing the structure of contextes
    private static final String STRUCTURE_BUNDLE = "com.sun.grid.grm.config.naming.dummycscontext";
    private static final String ROOT_NODE = "root";
    //separator that is used in context to determine the path
    private static final String CONTEXT_SEPARATOR = " ";
    //defines the current working node
    private String context;
    //this is memory representation for all instances
    private static Node memory = null;
    private static final ConfigurationServiceParser myParser = new ConfigurationServiceParser();
    private final Lock lock = new ReentrantLock();
    
    /** Creates a new instance of DummyConfigurationServiceContext
     * Initializes the memory and sets the current working node information
     * @param path - context path for a given instance of Context
     */
    private DummyConfigurationServiceContext(String path) {
        if (memory == null) {
            initMemory();
        }
        if (path == null || path.equals(ROOT_NODE) || path.length() == 0) {
            context = ROOT_NODE;
        } else {
            context = path;
        }
    }
    
    /** Creates a new instance of DummyConfigurationServiceContext */
    public DummyConfigurationServiceContext() {
        this(null);
    }
    
    
    /*
     * This method returns object from a given Context or the Context itself
     * @param name name of a object or Context as Name type
     * @return object or a Context
     * @throw NamingException if object or Context was not found
     */
    public Object lookup(Name name) throws NamingException {
        if (name.isEmpty()) {
            return cloneContext();
        }
        Name nm = getMyComponents(name);
        String tempName = nm.get(0);
        
        if (nm.size() == 1) {
            Object ret = null;
            Node current;
            lock.lock();
            try{
                current = getCurrentNode(memory);
                ret = current.getObject(tempName);
            } finally {
                lock.unlock();
            }
            
            if (ret instanceof Node) {
                return new DummyConfigurationServiceContext(addContextLevel(tempName));
            }
            return ret;
            
        } else {
            DummyConfigurationServiceContext obj = new DummyConfigurationServiceContext(addContextLevel(tempName));
            if (obj != null) {
                return obj.lookup(nm.getSuffix(1));
            } else {
                throw new NameNotFoundException(I18NManager.formatMessage("context.name.not.found", BUNDLE, tempName));
            }
        }
    }
    /*
     * This method returns object from a given Context or the Context itself
     * @param name name of a object or Context as String type
     * @return object or a Context
     * @throw NamingException if object or Context was not found
     */
    public Object lookup(String name) throws NamingException {
        return lookup(new CompositeName(name));
    }
    /*
     * This method bind an object with the given name and then store it
     * in memory.
     * @param name name for a object to bind as Name type.
     * @param object ab object to bind.
     * @throw NamingException if an error when binding an object
     */
    public void bind(Name name, Object obj) throws NamingException {
        if (name.isEmpty()) {
            throw new InvalidNameException(
                    I18NManager.formatMessage("context.cant.bind.empty",
                    BUNDLE));
        }
        if (obj instanceof DummyConfigurationServiceContext) {
            throw new NamingException(I18NManager.formatMessage("dummycontext.error.cant_bind_context",
                    BUNDLE));
        }
        
        Name nm = getMyComponents(name);
        String tempName = nm.get(0);
        
        
        if (nm.size() == 1) {
            
            lock.lock();
            try {
                Node current = getCurrentNode(memory);
                if (current.containsObject(tempName)) {
                    throw new NameAlreadyBoundException(
                            I18NManager.formatMessage("context.use.rebind",
                            BUNDLE,
                            tempName));
                }
                
                try {
                    current.addLeaf(tempName, obj);
                } catch (GrmException ex) {
                    NamingException ne = new NamingException(I18NManager.formatMessage("dummycontext.error.bind_failed",
                            BUNDLE, tempName));
                    ne.initCause(ex);
                    throw ne;
                }
            } finally {
                lock.unlock();
            }
            
            
        } else {
            lock.lock();
            try {
                Node current = getCurrentNode(memory);
                if(!current.containsNode(tempName)) {
                    current.addNode(tempName);
                }
            } finally {
                lock.unlock();
            }
            
            DummyConfigurationServiceContext ctx = new DummyConfigurationServiceContext(addContextLevel(tempName));
            ctx.bind(nm.getSuffix(1),
                    obj);
        }
    }
    /*
     * This method bind an object with the given name and then store it
     * in memory.
     * @param name name for a object to bind as String type.
     * @param object ab object to bind.
     * @throw NamingException if an error when binding an object
     */
    public void bind(String name, Object obj) throws NamingException {
        bind(new CompositeName(name),
                obj);
    }
    /*
     * This method re-bind an object with the given name in the memory. If
     * there is no object with that name than its created.
     * @param name name for a object to re-bind as Name type.
     * @param object ab object to re-bind.
     * @throw NamingException if an error when re-binding an object
     */
    public void rebind(Name name, Object obj) throws NamingException {
        if (name.isEmpty()) {
            throw new InvalidNameException(
                    I18NManager.formatMessage("context.cant.bind.empty",
                    BUNDLE));
        }
        if (obj instanceof DummyConfigurationServiceContext) {
            throw new NamingException(I18NManager.formatMessage("dummycontext.error.cant_bind_context",
                    BUNDLE));
        }
        Name nm = getMyComponents(name);
        String tempName = nm.get(0);
        
        if (nm.size() == 1) {
            
            lock.lock();
            try {
                Node current = getCurrentNode(memory);
                if (current.containsNode(tempName)) {
                    throw new NamingException(
                            I18NManager.formatMessage("dummycontext.error.cant_rebind_context",
                            BUNDLE,
                            context, tempName));
                }
                try {
                    if (current.containsLeaf(tempName)) {
                        current.removeLeaf(tempName);
                    }
                    current.addLeaf(tempName, obj);
                } catch (GrmException ex) {
                    NamingException ne = new NamingException(I18NManager.formatMessage("dummycontext.error.rebind_failed",
                            BUNDLE, tempName));
                    ne.initCause(ex);
                    throw ne;
                }
            } finally {
                lock.unlock();
            }
        } else {
            lock.lock();
            try {
                Node current = getCurrentNode(memory);
                if (!current.containsNode(tempName)) {
                    throw new NameNotFoundException(
                            I18NManager.formatMessage("context.name.not.found",
                            BUNDLE,
                            tempName));
                }
            } finally {
                lock.unlock();
            }
            
            DummyConfigurationServiceContext ctx = new DummyConfigurationServiceContext(addContextLevel(tempName));
            ctx.rebind(nm.getSuffix(1),
                    obj);
        }
    }
    /*
     * This method re-bind an object with the given name in the memory. If
     * there is no object with that name than its created.
     * @param name name for a object to re-bind as String type.
     * @param object ab object to re-bind.
     * @throw NamingException if an error when re-binding an object
     */
    public void rebind(String name, Object obj) throws NamingException {
        rebind(new CompositeName(name), obj);
    }
    /*
     * This method unbind a object with the given name and then remove it
     * from the memory.
     * @param name name for a object to unbind as Name type.
     * @throw NamingException if an error when binding an object
     */
    public void unbind(Name name) throws NamingException {
        if (name.isEmpty()) {
            throw new InvalidNameException(
                    I18NManager.formatMessage("context.cant.unbind.empty",
                    BUNDLE));
        }
        Name nm = getMyComponents(name);
        String tempName = nm.get(0);
        
        if (nm.size() == 1) {           
            lock.lock();
            try {
                Node current = getCurrentNode(memory);;
                if (current.containsNode(tempName)) {
                    throw new NamingException(
                            I18NManager.formatMessage("dummycontext.error.cant_unbind_context",
                            BUNDLE,
                            context, tempName));
                }
                if (!current.containsLeaf(tempName)) {
                    throw new NameNotFoundException(
                            I18NManager.formatMessage("context.name.not.found",
                            BUNDLE,
                            tempName));
                }                              
                try {
                    current.removeObject(tempName);
                } catch (GrmException ex) {
                    NamingException ne = new NamingException(I18NManager.formatMessage("dummycontext.error.unbind_failed",
                            BUNDLE, tempName));
                    ne.initCause(ex);
                    throw ne;
                }                              
            } finally {
                lock.unlock();
            }                      
        } else {
            lock.lock();
            try {
                Node current = getCurrentNode(memory);
                if (!current.containsNode(tempName)) {
                    throw new NameNotFoundException(
                            I18NManager.formatMessage("context.name.not.found",
                            BUNDLE,
                            tempName));
                }
            } finally {
                lock.unlock();
            }           
            DummyConfigurationServiceContext ctx = new DummyConfigurationServiceContext(addContextLevel(tempName));
            ctx.unbind(nm.getSuffix(1));
        }
        
    }
    /*
     * This method unbind a object with the given name and then remove it
     * from the memory.
     * @param name name for a object to unbind as String type.
     * @throw NamingException if an error when binding an object
     */
    public void unbind(String name) throws NamingException {
        unbind(new CompositeName(name));
    }
    
    public void rename(Name oldName, Name newName) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public void rename(String oldName, String newName) throws NamingException {
        throw new UnsupportedOperationException();
    }
    /* This method return the key names for an object stored in a given Context.
     * @param name Context name to list of Name type.
     * @return NamingEnumeration<NameClassPair> object with object names that are
     * stored in given Context
     * @throw NamingException when given name cannot be listed.
     */
    public NamingEnumeration<NameClassPair> list(Name name) throws NamingException {
        if (name.isEmpty()) {
            Node current = null;
            Map<String, String> pairs = new HashMap<String, String>();
            Map<String, Object> tmp;
            lock.lock();
            try {
                current = getCurrentNode(memory);
                tmp = current.getObjects();
                for (String n : tmp.keySet()) {
                    Object obj = tmp.get(n);
                    if (obj instanceof Node) {
                        pairs.put(n, Context.class.getName());
                    } else {
                        pairs.put(n, obj.getClass().getName());
                    }
                }
            } finally {
                lock.unlock();
            }
            return new ListOfNames(pairs);          
        }        
        Object target = lookup(name);
        if (target instanceof DummyConfigurationServiceContext) {
            return ((DummyConfigurationServiceContext)target).list("");
        }
        throw new NotContextException(
                I18NManager.formatMessage("context.cannot.be.listed",
                BUNDLE,
                name));
    }
    /* This method return the key names for an object stored in a given Context.
     * @param name Context name to list of String type.
     * @return NamingEnumeration<NameClassPair> object with object names that are
     * stored in given Context
     * @throw NamingException when given name cannot be listed.
     */
    public NamingEnumeration<NameClassPair> list(String name) throws NamingException {
        return list(new CompositeName(name));
    }
    /* This method return the bindings stored in a given Context.
     * @param name Context name to list bindings from of Name type.
     * @return NamingEnumeration<Binding> object with objects that are
     * stored in given Context
     * @throw NamingException when given name cannot be listed.
     */
    public NamingEnumeration<Binding> listBindings(Name name) throws NamingException {
        if (name.isEmpty()) {
            Node current = null;
            Map<String, Object> pairs = new HashMap<String, Object>();
            Map<String, Object> tmp;
            lock.lock();
            try {
                current = getCurrentNode(memory);
                tmp = current.getObjects();
                for (String n : tmp.keySet()) {
                    Object obj = tmp.get(n);
                    if (obj instanceof Node) {
                        pairs.put(n, new DummyConfigurationServiceContext(addContextLevel(n)));
                    } else {
                        pairs.put(n, obj);
                    }
                }
            } finally {
                lock.unlock();
            }
            return new ListOfBindings(pairs);
        }
        
        Object target = lookup(name);
        if (target instanceof DummyConfigurationServiceContext) {
            return ((DummyConfigurationServiceContext)target).listBindings("");
        }
        throw new NotContextException(
                I18NManager.formatMessage("context.cannot.be.listed",
                BUNDLE,
                name));
        
    }
    /* This method return the bindings stored in a given Context.
     * @param name Context name to list bindings from of String type.
     * @return NamingEnumeration<Binding> object with objects that are
     * stored in given Context
     * @throw NamingException when given name cannot be listed.
     */
    public NamingEnumeration<Binding> listBindings(String name) throws NamingException {
        return listBindings(new CompositeName(name));
    }
    
    public void destroySubcontext(Name name) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public void destroySubcontext(String name) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public Context createSubcontext(Name name) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public Context createSubcontext(String name) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public Object lookupLink(Name name) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public Object lookupLink(String name) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public NameParser getNameParser(Name name) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public NameParser getNameParser(String name) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public Name composeName(Name name, Name prefix) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public String composeName(String name, String prefix) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public Object addToEnvironment(String propName, Object propVal) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public Object removeFromEnvironment(String propName) throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public Hashtable<?, ?> getEnvironment() throws NamingException {
        throw new UnsupportedOperationException();
    }
    
    public void close() throws NamingException {
    }
    
    public String getNameInNamespace() throws NamingException {
        throw new UnsupportedOperationException();
    }
    /**
     * Erease the memory of Context
     */
    
    public void clear() {
        lock.lock();
        try {
            memory = null;
        } finally {
            lock.unlock();
        }
        
    }
    
    private Context cloneContext() {
        return new DummyConfigurationServiceContext(context);
    }
    /**
     * Return list representing the context path, the root element is stripped so
     * the first element in list is element following the root element
     */
    private List<String> getContextPath() {
        List<String> ret = new LinkedList<String>();
        if (context == null || context.equals(ROOT_NODE)) {
            return ret;
        }
        String[] tmp = context.split(CONTEXT_SEPARATOR);
        for (int i=1; i<tmp.length;i++) {
            ret.add(tmp[i]);
        }
        return ret;
    }
    
    /**
     * Add a level to context path
     * @param level to be added
     * @return string representing current context path with added level
     */
    private String addContextLevel(String level) {
        StringBuilder sb = new StringBuilder();
        sb.append(context);
        sb.append(CONTEXT_SEPARATOR);
        sb.append(level);
        return sb.toString();
    }
    
    /**
     * All instances are working on memory to do some operation on memory we need
     * a way to get the current working Node
     * @param mem - copy of memory
     * @return current Node
     * @throws GrmException when operation on memory fails
     */
    private Node getCurrentNode(Node mem) throws NameNotFoundException {
        List<String> tmp = getContextPath();
        
        if (tmp.isEmpty()) {
            return mem;
        }
        Node tmpNode = mem;
        for (String node : tmp) {
            tmpNode = tmpNode.getNode(node);
        }
        return tmpNode;
    }
    
    /**
     * Initialize the memory with the defined objects
     * If any error occurs during initialization memory is set to null
     */
    private void initMemory() {
        //
        List<String> n = getStructureNodes();
        lock.lock();
        try {
            for (String contextPath : n) {
                try {
                    memory = createNodeStructure(memory, contextPath);
                } catch (NamingException ex) {
                    memory = null;
                }
            }
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * Get the information from resource bundle about contextes
     * that should be created in memory
     */
    private static List<String> getStructureNodes() {
        List<String> ret = new LinkedList<String>();
        ResourceBundle rb = ResourceBundle.getBundle(STRUCTURE_BUNDLE);
        Enumeration<String> elems = rb.getKeys();
        while(elems.hasMoreElements()) {
            ret.add(rb.getString(elems.nextElement()));
        }
        return ret;
    }
    
    /**
     * Create the memory nodes according to the path contained in context
     * Context is a string containing the path of following contextes separated with space character
     * if Node does not exist is created, if Node exists nothing is done,
     */
    private static Node createNodeStructure(Node node, String context) throws NamingException {
        List<String> ret = StringToContextes(context);
        if (ret.isEmpty()) {
            //Invalid context string, do nothing and return node
            return node;
        }
        // Initialize Node
        if (node == null) {
            node = new Node(ret.get(0));
        } else {
            //check whether the first element on list is a given Node - to avoid NPE
            if (node.getName() == null || ret.get(0) == null) {
                throw new NamingException(I18NManager.formatMessage("dummycontext.null_node_not_allowed", BUNDLE));
            }
            if (!node.getName().equals(ret.get(0))) {
                throw new NamingException(I18NManager.formatMessage("dummycontext.create_node_structure.node_not_root_for_context", BUNDLE, context));
            }
        }
        
        Node tmpNode = node;
        for (int n = 1; n<ret.size();n++) {
            if (tmpNode.containsNode(ret.get(n))) {
                tmpNode = tmpNode.getNode(ret.get(n));
            } else if (tmpNode.containsLeaf(ret.get(n))) {
                throw new NamingException(I18NManager.formatMessage("dummycontext.create_node_structure.name_already_used", BUNDLE, tmpNode.getName(), ret.get(n)));
            } else {
                tmpNode.addNode(ret.get(n));
                tmpNode = tmpNode.getNode(ret.get(n));
            }
            
        }
        return node;
    }
    
    /**
     * Convert string containing the path of contextes to List, used for
     * strings that are read from resource bundle file
     */
    private static List<String> StringToContextes(String context) {
        List<String> ret = new LinkedList<String>();
        String[] tmp = context.split(CONTEXT_SEPARATOR);
        for (int i = 0; i<tmp.length; i++) {
            ret.add(tmp[i]);
        }
        return ret;
    }
    
    /**
     * This method parse the name that is separated by comas to atoms.
     */
    protected Name getMyComponents(Name name) throws NamingException {
        if (name instanceof CompositeName) {
            if (name.size() > 1) {
                throw new InvalidNameException(
                        I18NManager.formatMessage("context.too.much.components",
                        BUNDLE,
                        name.toString()));
            }
            return myParser.parse(name.get(0));
        } else {
            return name;
        }
    }

    /*
     * Helper class
     */
    class BaseListOfNames {
        protected Iterator<String> iterator;
        
        BaseListOfNames(Set<String> names) {
            /* a new set has to be created - w/o that there is a risk
             * of concurrent modification 
             */
            this.iterator = new HashSet<String>(names).iterator();
        }
        
        public boolean hasMoreElements() {
            try {
                return hasMore();
            } catch (NamingException e) {
                return false;
            }
        }
        
        public boolean hasMore() throws NamingException {
            return iterator.hasNext();
        }      
        
        public void close() {
        }
    }
    
    
    /*
     * Helper class for enumerating names
     */
    class ListOfNames extends BaseListOfNames implements NamingEnumeration<NameClassPair> {
        private Map<String, String> pairs;
        ListOfNames(Map<String, String> pairs) {
            super(pairs.keySet());
            this.pairs = pairs;
        }
              
        public NameClassPair next() throws NamingException {
            String name = iterator.next();
            String className = pairs.get(name);
                                                
            return new NameClassPair(name, 
                                     className);
        }
        
        public NameClassPair nextElement() {
            try {
                return next();
            } catch (NamingException e) {
                NoSuchElementException ne = new NoSuchElementException();
                ne.initCause(e);
                throw ne;
            }
        }       
    }
    
    /*
     * Helper class for enumerating bindings
     */
    class ListOfBindings extends BaseListOfNames implements NamingEnumeration<Binding>{
        private Map<String, Object> binding;
        ListOfBindings(Map<String, Object> binding) {
            super(binding.keySet());
            this.binding = binding;
        }
        
        public Binding next() throws NamingException {
            String name = iterator.next();
            Object object = binding.get(name);

            return new Binding(name, object);
        }

        public Binding nextElement() {
            try {
                return next();
            } catch (NamingException ex) {
                NoSuchElementException ne = new NoSuchElementException();
                ne.initCause(ex);
                throw ne;
            }
        }
    }
}

