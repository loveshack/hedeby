/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.config.naming;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.I18NManager;
import java.util.HashMap;
import java.util.Map;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

/**
 * Helper class for representing hierarchically contextes in memory
 * context equals Node
 * any other object that is not a Node is a leaf
 * each Node can contain many Nodes and many leafs,
 * all objects stored in one Node should have unique names
 * Null can not be stored in memory.
 */
public class Node {
    private Map<String, Object> leaf;
    private String name;
    private static final String BUNDLE = "com.sun.grid.grm.config.naming.dummymessages";
    /**
     * Creates an instance of Node
     * @param name - string representing Node
     */
    public Node(String name) {
        this.name = name;
        leaf = new HashMap<String, Object>();
    }
    
    /**
     * Check whether the current Node contains leaf with a given name
     * @return true if current Node contains object with a given name that is not
     * a <code>Node</code> object, otherwise false
     */
    public boolean containsLeaf(String name) {
        if (leaf.containsKey(name)) {
            Object obj = leaf.get(name);
            if (!(obj instanceof Node)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Check whether the current Node contains Node with a given name
     * @return true if current Node contains object with a given name that is
     * a <code>Node</code> object, otherwise false
     */
    public boolean containsNode(String name) {
        if (leaf.containsKey(name)) {
            Object obj = leaf.get(name);
            if (obj instanceof Node) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Check whether the current Node contains Object with a given name
     * @return true if current Node contains object with a given name, otherwise false
     */
    public boolean containsObject(String name) {
        return leaf.containsKey(name);
    }
    
    /**
     * Retrieve object that was stored with name as a key, object can not be Node
     * @param name - key for object
     * @return Object - returns object that is not a <code>Node</code> and was stored with name as a key
     * @throws com.sun.grid.grm.GrmException if object can not be retrieved
     */
    public Object getLeaf(String name) throws GrmException {
        if(!containsLeaf(name)) {
            throw new GrmException("node.leaf_not_found", BUNDLE, getName(), name);
        }
        return leaf.get(name);
    }
    
    /**
     * Retrieve all Objects that are not Node(s) and are stored in current Node
     * @return Map with stored Objects and its keys, all returned objects are not Node(s)
     */
    public Map<String, Object> getLeaves() {
        Map<String,Object> ret = new HashMap<String, Object>();
        for (String key : leaf.keySet()) {
            Object obj = leaf.get(key);
            if (obj instanceof Node) {
                continue;
            }
            ret.put(key, obj);
        }
        return ret;
    }
    
    /**
     * Retrieve all Objects that are stored in current Node
     * @return Map with stored Objects and its keys
     */
    public Map<String, Object> getObjects() {
        return leaf;
    }
    
    /**
     * Retrieve Node that was stored with name as a key
     * @param name - key for Node
     * @return Object - returns Node that was stored with name as a key
     * @throws NameNotFoundException If the node is unknown
     */
    public Node getNode(String name) throws NameNotFoundException {
        if(!containsNode(name)) {
            throw new NameNotFoundException(I18NManager.formatMessage("node.node_not_found", BUNDLE, getName(), name));
        }
        
        return (Node)leaf.get(name);
    }
    /**
     * Retrieve all Nodes are stored in current Node
     * @return Map with stored Nodes and its keys
     */
    public Map<String, Node> getNodes() {
        Map<String,Node> ret = new HashMap<String, Node>();
        for (String key : leaf.keySet()) {
            Object obj = leaf.get(key);
            if (obj instanceof Node) {
                ret.put(key, (Node)obj);
            }
        }
        return ret;
    }
    /**
     * Retrieve object that was stored with name as a key
     * @param name - key for object
     * @return Object - returns object that was stored with name as a key
     * @throws NameNotFoundException If no object has been stored under this name
     */
    public Object getObject(String name) throws NameNotFoundException {
        if (containsObject(name)) {
            return leaf.get(name);
        }
        throw new NameNotFoundException(I18NManager.formatMessage("node.object_not_found", BUNDLE, getName(), name));
    }
    
    /**
     * Getter for the name of current Node
     * @return name - string identifying current Node
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Store object in Node, object and key can not be null
     * @param name - key under which object is stored
     * @param obj - object to be stored
     * @throws com.sun.grid.grm.GrmException if object can not be stored
     */
    public void addObject(String name, Object obj) throws GrmException {
        if (name == null || obj == null) {
            throw new GrmException("node.error.add_object.null_arguments", BUNDLE, getName());
        }
        if (containsObject(name)) {
            throw new GrmException("node.error.add_object.object_already_exists", BUNDLE, getName(), name);
        }
        leaf.put(name, obj);
    }
    
    /**
     * Store object in Node, object and key can not be null, object can not be Node
     * @param name - key under which object is stored
     * @param obj - object to be stored, can not be <code>Node</code>
     * @throws com.sun.grid.grm.GrmException if object can not be stored
     */
    public void addLeaf(String name, Object obj) throws GrmException {
        if (name == null || obj == null) {
            throw new GrmException("node.error.add_leaf.null_arguments", BUNDLE, getName());
        }
        
        if (obj instanceof Node) {
            throw new GrmException("node.error.add_leaf.can_not_add_node", BUNDLE, getName(), name);
        }
        if (containsObject(name)) {
            throw new GrmException("node.error.add_leaf.object_already_exists", BUNDLE, getName(), name);
        }
        leaf.put(name, obj);
        
    }
    /**
     * Store object in Node, stores new Node with specified name
     * @param name - key under which object is stored, can not be null
     * @throws NamingException if Node can not be stored
     */
    public void addNode(String name) throws NamingException {
        
        if (name == null) {
            throw new NamingException(I18NManager.formatMessage("node.error.add_node.null_node", BUNDLE, getName()));
        }
        
        if (containsObject(name)) {
            throw new NamingException(I18NManager.formatMessage("node.error.add_node.object_already_exists", BUNDLE, getName(), name));
        }
        leaf.put(name, new Node(name));
    }
    
    /**
     * Remove object that is stored in current Node, object should be empty Node
     * @param name - key for Node to be removed
     * @throws com.sun.grid.grm.GrmException if Node can not be removed
     */
    public void removeNode(String name) throws NamingException {
        if (!containsNode(name)) {
            throw new NameNotFoundException(I18NManager.formatMessage("node.error.remove_node.node_not_found", BUNDLE, getName(), name));
        }
        
        if (getNode(name).getObjects().isEmpty()) {
            leaf.remove(name);
        } else {
            throw new NamingException(I18NManager.formatMessage("node.error.remove_node.node_has_elems", BUNDLE, getName(), name));
        }
    }
    
    /**
     * Remove object that is stored in current Node
     * @param name - key for object to be removed
     * @throws com.sun.grid.grm.GrmException if object can not be removed
     */
    public void removeObject(String name) throws GrmException {
        if (containsObject(name)) {
            leaf.remove(name);
            return;
        }
        throw new GrmException("node.error.remove_object.object_not_found", BUNDLE, getName(), name);
    }
    
    /**
     * Remove object that is stored in current Node, object should not be <code>Node</code>
     * @param name - key for object to be removed
     * @throws com.sun.grid.grm.GrmException if object can not be removed
     */
    public void removeLeaf(String name) throws GrmException {
        if (!containsLeaf(name)) {
            throw new GrmException("node.error.remove_leaf.leaf_not_found", BUNDLE, getName(), name);
        }
        leaf.remove(name);
    }
}
