/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.config.naming;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.AbstractModule;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ConfigPackage;
import com.sun.grid.grm.bootstrap.ConfigPackageImpl;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.Module;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.cli.CliCommand;
import com.sun.grid.grm.config.test.ConfigurationMockup;
import com.sun.grid.grm.config.test.ObjectFactory;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.validate.Validator;
import java.io.File;
import java.io.FileWriter;
import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import junit.framework.*;
import javax.naming.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * JUnit test that test the ConfigurationServiceContext class.
 *
 */
public class ConfigurationServiceContextTest extends TestCase {

    private Context ctx = null;
    private String systemName = new String("config");
    private File distDir = new File("dist");
    private File tmpDir = null;
    private File systemDir = null;
    private File csDir = null;
    private File componentDir = null;
    private File active_componentsDir = null;
    private File spoolDir = null;
    private Marshaller m;
    private Unmarshaller u;
    private ObjectFactory of;
    private File test2 = null;
    private ConfigurationMockup cm = null;
    private Module module = new TestModule();
    
    /** Creates a new instance of ConfigurationServiceContextTest */
    public ConfigurationServiceContextTest(String testName) {
        super(testName);
    }


    @Override
    protected void setUp() throws Exception {
        tmpDir = TestUtil.getTmpDir(this);
        assertTrue("Can not create tmp directory", tmpDir.mkdirs());
        setupConfigurationServiceDirectoryStructure();
        Modules.addModule(module);
        test2 = new File(csDir, BootstrapConstants.CS_ACTIVE_COMPONENT + File.separatorChar + "rp.xml");
        JAXBContext jaxbContext = JAXBContext.newInstance("com.sun.grid.grm.config.test");
        u = jaxbContext.createUnmarshaller();
        m = jaxbContext.createMarshaller();
        of = new ObjectFactory();
        cm = of.createConfigurationMockup();
        cm.setName("ResourceProvider");
        cm.setType("configuration");
        cm.setVersion(new BigInteger("5"));
    }


    @Override
    protected void tearDown() throws Exception {
        Modules.removeModule(module);
        deleteConfigurationServiceStructure(tmpDir);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(ConfigurationServiceContextTest.class);
        return suite;
    }
     
    public void testCSContextBind() throws Exception {       
       ctx.bind(BootstrapConstants.CS_ACTIVE_COMPONENT + ".rp", cm);
       assertTrue("Configuration file wasnt created", test2.exists());
    }
    
    public void testCSContextRebind() throws Exception {       
       ctx.bind(BootstrapConstants.CS_ACTIVE_COMPONENT + ".rp", cm);
       cm.setName("ResourceProvider555");
       cm.setType("rock");
       cm.setVersion(new BigInteger("50"));
       ctx.rebind(BootstrapConstants.CS_ACTIVE_COMPONENT + ".rp", cm);
       ConfigurationMockup cm2 = (ConfigurationMockup)ctx.lookup(BootstrapConstants.CS_ACTIVE_COMPONENT+".rp");
       cm.setName("ResourceProvider444");
       cm.setType("rock444");
       cm.setVersion(new BigInteger("50444"));
       ctx.rebind(BootstrapConstants.CS_ACTIVE_COMPONENT + ".rp", cm);
       ConfigurationMockup cm24 = (ConfigurationMockup)ctx.lookup(BootstrapConstants.CS_ACTIVE_COMPONENT+".rp");
       assertEquals("Values arent same", cm24.getName(), "ResourceProvider444");
       assertEquals("Values arent same", cm24.getType(), "rock444");
       assertTrue("Configuration file wasnt created", test2.exists());
    }
   
    public void testCSContextList() throws Exception {
       m.marshal(of.createConfigurationMockup(cm), new FileWriter(test2));
       String name1 = new String();
       NamingEnumeration<NameClassPair> ne = ctx.list(BootstrapConstants.CS_ACTIVE_COMPONENT);
       while (ne.hasMore()) {
           NameClassPair ncp = ne.next();
           name1 = ncp.getName();
       }

       assertEquals("There was no expected file listed", test2.getName(), name1 + ".xml");
    }
    
    public void testCSContextListBindings() throws Exception {
       m.marshal(of.createConfigurationMockup(cm), new FileWriter(test2));
       ConfigurationMockup name1 = null;
       NamingEnumeration<Binding> ne = ctx.listBindings(BootstrapConstants.CS_ACTIVE_COMPONENT);
       while (ne.hasMore()) {
           Binding ncp = ne.next();
           name1 = (ConfigurationMockup) ncp.getObject();
       }
       assertEquals("There was no expected binding listed", "ResourceProvider", name1.getName());
    }
    
    public void testCSContextLookup() throws Exception {
       m.marshal(of.createConfigurationMockup(cm), new FileWriter(test2));
       ConfigurationMockup rpc2 = (ConfigurationMockup)ctx.lookup(BootstrapConstants.CS_ACTIVE_COMPONENT + ".rp");
       String rpc2_name = rpc2.getName();
       assertEquals("There was no expected file found", "ResourceProvider", rpc2_name);
    }
    
    public void testCSContextUnbind() throws Exception {
       m.marshal(of.createConfigurationMockup(cm), new FileWriter(test2));
       ctx.unbind(BootstrapConstants.CS_ACTIVE_COMPONENT + ".rp");  
       assertFalse("Configuration file still exists", test2.exists());
    }
    
    private void setupConfigurationServiceDirectoryStructure() throws Exception {
        tmpDir.mkdirs();
        systemDir = new File(tmpDir, systemName);
        systemDir.mkdir();
        spoolDir = new File(systemDir, "spool");
        spoolDir.mkdir();
        csDir = new File(spoolDir, BootstrapConstants.CS_ID);
        csDir.mkdir();
        componentDir = new File(csDir, BootstrapConstants.CS_COMPONENT);
        componentDir.mkdir();
        active_componentsDir = new File(csDir, BootstrapConstants.CS_ACTIVE_COMPONENT);
        active_componentsDir.mkdir();

        Map<String, Object> properties = new HashMap<String, Object>();

        properties.put(ExecutionEnv.AUTO_START, true);
        properties.put(ExecutionEnv.NO_SSL, true);
        
        Hostname csHost = Hostname.getLocalHost();
        int port = 5000;

        ExecutionEnv env = ExecutionEnvFactory.newInstance(systemName , systemDir, distDir, csHost, port, properties);
        
        this.ctx = env.getContext();
    }
    
    private void deleteConfigurationServiceStructure(File directory) throws Exception {
        Platform.getPlatform().removeDir(tmpDir, true);
    }
    
    private static class TestModule extends AbstractModule {
        
        public TestModule() {
            super("ConfigurationServiceContextTest");
        }
        protected void loadCliExtension(Set<Class<? extends CliCommand>> extensions) {
            // Ignore
        }

        public String getVendor() {
            return "Sun Microsystems";
        }

        public String getVersion() {
            return "1.0";
        }

        public List<ConfigPackage> getConfigPackages() {
            List<ConfigPackage> cpList = new ArrayList<ConfigPackage>();   
            ClassLoader cl = getClass().getClassLoader();
            URL localURL = cl.getResource("com/sun/grid/grm/config/test/configurationMockupScheme.xsd");
            cpList.add(new ConfigPackageImpl("com.sun.grid.grm.config.test", 
                            "http://hedeby.sunsource.net/hedeby-test", 
                            "test", 
                            localURL, 
                            Collections.<String>emptyList()));           
            return cpList;
        }

        protected void loadValidatorExtension(Set<Class<? extends Validator>> extensions) {
            //Ignore
        }
        
    }
    
}
