/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.config.naming;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.executor.ExecutorConfig;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * JUnit test for FileCache class
 */
public class FileCacheTest extends TestCase{
    
    private File tmpDir;
    private File systemDir;
    private String systemName = "test";
    private File spoolDir;
    private File csDir;
    private File componentDir;
    private File active_componentsDir;
    private File emptyFile;
    private File nonExistingFile;
    
    /** Creates a new instance of FileCacheTest */
    public FileCacheTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        tmpDir = TestUtil.getTmpDir(this);
        assertTrue("Can not create tmp directory", tmpDir.mkdirs());
        setupConfigurationServiceDirectoryStructure();
    }


    @Override
    protected void tearDown() throws Exception {
        deleteConfigurationServiceStructure(tmpDir);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(FileCacheTest.class);
        return suite;
    }
    
    /**
     * Test creation of instance
     */
    public void testGetInstance() throws Exception {       
       
        FileCache.CachedFile f = FileCache.getInstance(systemDir);
       
        assertEquals("File equals failed", f.getPath(), systemDir);
       
    }
    
    /**
     * Test create() on file
     */
    public void testCreateOnFile() throws Exception {       
       
       FileCache.CachedFile f = FileCache.getInstance(emptyFile);
       try {
           f.create("newFile", null);
           assertFalse("Create operation on file should fail",true);
       } catch(IOException ex) {
           assertTrue("Create operation failed on file", true);
       }
    }
    
    /**
     * Test createDir() on file
     */
    public void testCreateDironFile() throws Exception {       
       
       FileCache.CachedFile f = FileCache.getInstance(emptyFile);
       try {
           f.createDir("newDir");
           assertFalse("CreateDir operation on file should fail",true);
       } catch(IOException ex) {
           assertTrue("CreateDir operation failed on file", true);
       }
    }
    
    /**
     * Test list() on file
     */
    public void testListonFile() throws Exception {       
       
      FileCache.CachedFile f = FileCache.getInstance(emptyFile);
       try {
           f.list();
           assertFalse("List operation on file should fail",true);
       } catch(IOException ex) {
           assertTrue("List operation failed on file", true);
       }
    }
    
    
    /**
     * Test reading on directory
     */
    public void testReadonDir() throws Exception {       
       FileCache.CachedFile f = FileCache.getInstance(systemDir);
       try {
           f.read();
           assertFalse("Read operation on dir should fail",true);
       } catch(IOException ex) {
           assertTrue("Read operation failed on dir", true);
       }

    }
    
    /**
     * Test writting on directory
     */
    public void testWriteonDir() throws Exception {       
       
       FileCache.CachedFile f = FileCache.getInstance(systemDir);
       try {
           f.write(null);
           assertFalse("Write operation on dir should fail",true);
       } catch(IOException ex) {
           assertTrue("Write operation failed on dir", true);
       }
       
    }
    
    /**
     * Test creation of directory
     */
    public void testCreateDir() throws Exception {       
       File newDir = new File(spoolDir, "new");
       if (newDir.exists()) {
           assertTrue("Failed, directory should not exist", false);
       }
       
       FileCache.CachedFile dir = FileCache.getInstance(spoolDir);
       FileCache.CachedFile createdDir = dir.createDir("new");
       FileCache.CachedFile testDir = FileCache.getInstance(newDir);
       
       assertTrue("Dir has not been created - file handle", newDir.isDirectory());
       assertTrue("Dir has not been created - createdDir, CachedFile object", createdDir.getPath().isDirectory());
       assertTrue("Dir has not been created - test cache, CachedFile object", testDir.getPath().isDirectory());
       assertEquals("Created object and object retrrieved from cache are not equal", createdDir, testDir);
    }
    
    /**
     * Test creation of file
     */
    public void testCreate() throws Exception {       
       File test = new File(csDir, "elem");
       if (test.exists()) {
           assertTrue("Failed, file should not exist", false);
       }
       FileCache.CachedFile cs = FileCache.getInstance(csDir);
       ExecutorConfig conf = new ExecutorConfig();
       conf.setIdleTimeout(546);
       FileCache.CachedFile newFile = cs.create("elem", conf);
       ExecutorConfig obj = (ExecutorConfig)XMLUtil.load(test);
       assertEquals("Retrieved value is different from stored one", conf.getIdleTimeout(), obj.getIdleTimeout());
       assertEquals("Files are not equal", test, newFile.getPath());
    }
    
    /**
     * Test reading from file
     */
    public void testReadFile() throws Exception {       
       
       File test = new File(csDir, "elem");
       ExecutorConfig conf = new ExecutorConfig();
       conf.setIdleTimeout(546);
       XMLUtil.store(conf, test);
       
       ExecutorConfig obj = (ExecutorConfig)FileCache.getInstance(test).read();
       
       assertEquals("Retrieved value is different from stored one", conf.getIdleTimeout(), obj.getIdleTimeout());

    }
    
    /**
     * Test writing to file
     */
    public void testWriteFile() throws Exception {       
       File test = new File(csDir, "elem");
       ExecutorConfig conf = new ExecutorConfig();
       conf.setIdleTimeout(546);
       
       FileCache.getInstance(test).write(conf);
       
       ExecutorConfig obj = (ExecutorConfig)XMLUtil.load(test);
       
       assertEquals("Retrieved value is different from stored one", conf.getIdleTimeout(), obj.getIdleTimeout());
       
       conf.setIdleTimeout(645);
       
       FileCache.getInstance(test).write(conf);
       
       ExecutorConfig obj1 = (ExecutorConfig)XMLUtil.load(test);
       
       assertEquals("Retrieved value is different from stored one", conf.getIdleTimeout(), obj1.getIdleTimeout());
       
       //check finaaly what is in memory
       
       ExecutorConfig obj2 = (ExecutorConfig)FileCache.getInstance(test).read();
       
       assertEquals("Retrieved value is different from stored one", conf.getIdleTimeout(), obj2.getIdleTimeout());
    }
    
    /**
     * Test deletion of directory
     */
    public void testDeleteDir() throws Exception {       
       
       FileCache.CachedFile f = FileCache.getInstance(systemDir);
       f.delete();
       
       FileCache.CachedFile f1 = FileCache.getInstance(systemDir);

       assertFalse("Dir should be removed", f1.getPath().exists());
       assertFalse("Dir should be removed", systemDir.exists());
    }
    
    /**
     * Test deletion of file
     */
    public void testDeleteFile() throws Exception {       
       
       FileCache.CachedFile f = FileCache.getInstance(emptyFile);
       f.delete();
       
       FileCache.CachedFile f1 = FileCache.getInstance(emptyFile);
       assertFalse("File should be removed", f1.getPath().exists());
       assertFalse("File should be removed", emptyFile.exists());
       
    }
    
    /**
     * ----$tmpDir
     *       \----------systemName
     *                      \---emptyFile
     *                       \----------spool
     *                                    \-----cs
     *                                          \-----component
     *                                          \-----active_component
     */
    private void setupConfigurationServiceDirectoryStructure() throws Exception {
        tmpDir.mkdirs();
        systemDir = new File(tmpDir, systemName);
        systemDir.mkdir();
        spoolDir = new File(systemDir, "spool");
        spoolDir.mkdir();
        csDir = new File(spoolDir, BootstrapConstants.CS_ID);
        csDir.mkdir();
        componentDir = new File(csDir, BootstrapConstants.CS_COMPONENT);
        componentDir.mkdir();
        active_componentsDir = new File(csDir, BootstrapConstants.CS_ACTIVE_COMPONENT);
        active_componentsDir.mkdir();
        emptyFile = new File(systemName, "emptyFile");
        emptyFile.createNewFile();
        nonExistingFile = new File(systemName, "nonExistingFile");

    }
    /** 
     * Do clean up on filesystem
     */
    private void deleteConfigurationServiceStructure(File directory) throws Exception {
        Platform.getPlatform().removeDir(directory, true);
    }
    
}
