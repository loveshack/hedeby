/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun ndMicrosystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.config;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.AbstractModule;
import com.sun.grid.grm.bootstrap.ConfigPackage;
import com.sun.grid.grm.bootstrap.ConfigPackageImpl;
import com.sun.grid.grm.bootstrap.Module;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.cli.CliCommand;
import com.sun.grid.grm.config.test.ObjectFactory;
import com.sun.grid.grm.config.test.ConfigurationMockup;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.validate.Validator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import junit.framework.*;
import org.xml.sax.SAXParseException;

/**
 *  Test for the class PreferencesUtil
 */
public class XMLUtilTest extends TestCase {
    
    private final static Logger log = Logger.getLogger(XMLUtilTest.class.getName());

    private File tmp;
    private File tmpDir;
    private Module module = new TestModule();
    
    private Marshaller m;
    private Unmarshaller u;
    private ObjectFactory of;
    
    
    public XMLUtilTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        log.entering(XMLUtilTest.class.getName(), "setUp");
        
        tmpDir = TestUtil.getTmpDir(this);
        assertTrue("Can not create tmp directory", tmpDir.mkdirs());
        tmp = new File(tmpDir, "test.xml");
        Modules.addModule(module);
        JAXBContext jc = JAXBContext.newInstance("com.sun.grid.grm.config.test");
        u = jc.createUnmarshaller();
        m = jc.createMarshaller();
        of = new ObjectFactory();
        
        log.exiting(XMLUtilTest.class.getName(), "setUp");
    }

    @Override
    protected void tearDown() throws Exception {
        log.entering(XMLUtilTest.class.getName(), "tearDown");
        
        Modules.removeModule(module);
        Platform.getPlatform().removeDir(tmpDir, true);
        
        log.exiting(XMLUtilTest.class.getName(), "tearDown");
    }

    public void testStore() throws Exception {
        log.entering(XMLUtilTest.class.getName(), "testStore");
        
        ConfigurationMockup cfg = of.createConfigurationMockup();
        cfg.setName("name");
        cfg.setType("type");
        cfg.setVersion(new BigInteger("1"));
        XMLUtil.store(cfg, tmp);
        ConfigurationMockup res = load();
        assertEquals("name is different", cfg.getName(), res.getName());
        assertEquals("type is different", cfg.getType(), res.getType());
        assertEquals("version is different", cfg.getVersion(), res.getVersion());    
        
        log.exiting(XMLUtilTest.class.getName(), "testStore");
    }

    /**
     * This test checks regression of issue 685.
     * Issue 685 describes the problem that attributes with blanks are not handled
     * correctly.
     * 
     * @throws java.lang.Exception
     */
    public void testIssue685() throws Exception {
        log.entering(XMLUtilTest.class.getName(), "testIssue685");

        ConfigurationMockup cfg = of.createConfigurationMockup();
        cfg.setName("name with blank");
        cfg.setType("type");
        cfg.setVersion(new BigInteger("1"));
        XMLUtil.store(cfg, tmp);
        ConfigurationMockup res = load();
        assertEquals("name is different", cfg.getName(), res.getName());
        assertEquals("type is different", cfg.getType(), res.getType());
        assertEquals("version is different", cfg.getVersion(), res.getVersion());

        log.exiting(XMLUtilTest.class.getName(), "testIssue685");
    }

    public void testStoreIllegalArgumentException() throws Exception {
        log.entering(XMLUtilTest.class.getName(), "testStoreIllegalArgumentException");
        
        ObjectFactory of = new ObjectFactory();
        ConfigurationMockup cfg = of.createConfigurationMockup();
        cfg.setName("testStoreExceptionName");
        cfg.setType("testStoreExceptionName");
        cfg.setVersion(new BigInteger("1"));
        try {
            XMLUtil.store(cfg, null);
            fail("XMLUtil.store(object, null) should throw an excpetion");
        } catch (Exception e) {
            assertTrue("XMLUtil.store(object, null) should throw an excpetion", true);
        }
        log.exiting(XMLUtilTest.class.getName(), "testStoreIllegalArgumentException");
    }
    
    public void testStoreJAXBException() throws Exception {
        log.entering(XMLUtilTest.class.getName(), "testStoreJAXBException");
        
        FooBar foobar = new FooBar();
        try {
            XMLUtil.store(foobar, tmp);
            fail("XMLUtil.store(foobar, tmpurvinek" +
                    ") should throw an excpetion");
        } catch (GrmXMLException e) {
            log.log(Level.FINE, "Exepcted error occured", e);
            assertTrue("XMLUtil.store(object, null) should throw an excpetion", true);
        }
        log.exiting(XMLUtilTest.class.getName(), "testStoreJAXBException");
    }

    public void testLoad() throws Exception {
        log.entering(XMLUtilTest.class.getName(), "testLoad");

        ConfigurationMockup cfg = of.createConfigurationMockup();
        cfg.setName("testLoadName");
        cfg.setType("testLoadType");
        cfg.setVersion(new BigInteger("1"));
        store(cfg);
        ConfigurationMockup res = (ConfigurationMockup)XMLUtil.load(tmp);
        
        assertEquals("name is different", cfg.getName(), res.getName());
        assertEquals("type is different", cfg.getType(), res.getType());
        assertEquals("version is different", cfg.getVersion(), res.getVersion());
        log.exiting(XMLUtilTest.class.getName(), "testLoad");
    }

    public void testLoadFileDoesNotExist() throws Exception {
        tmp.delete();
        try {
            XMLUtil.loadAndValidate(tmp);
            fail("load on a non existing file must fail");
        } catch(GrmXMLException ex) {
            log.log(Level.INFO, "Expected exception caught: " + ex.getLocalizedMessage(), ex);
            assertTrue("Cause must be an instanceof IOException", ex.getCause() instanceof IOException);
        }
    }

    public void testLoadFileInvalidProlog() throws Exception {
        PrintWriter pw = new PrintWriter(tmp);
        pw.println("blubber");
        pw.close();
        try {
            XMLUtil.loadAndValidate(tmp);
            fail("load on xml file with invalid prolog must fail");
        } catch(GrmXMLException ex) {
            log.log(Level.INFO, "Expected exception caught: " + ex.getLocalizedMessage(), ex);
            assertTrue("Cause must be an instanceof of SAXParseException", ex.getCause() instanceof SAXParseException);
        }
    }

    public void testLoadFileInvalidEpilog() throws Exception {
        PrintWriter pw = new PrintWriter(tmp);
        pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        pw.print("<nextResourceId nextResourceId='1'");
        pw.print(" xmlns='http://hedeby.sunsource.net/hedeby-common'");
        pw.print(" xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'/>");
        pw.println("blubber");
        pw.close();
        try {
            XMLUtil.loadAndValidate(tmp);
            fail("load on xml file with invalid epilog must fail");
        } catch(GrmXMLException ex) {
            log.log(Level.INFO, "Expected exception caught: " + ex.getLocalizedMessage(), ex);
            assertTrue("Cause must be an instanceof of SAXParseException", ex.getCause() instanceof SAXParseException);
        }
    }

    public void testLoadFileMissingEndOfAttribute() throws Exception {
        PrintWriter pw = new PrintWriter(tmp);
        pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        pw.print("<nextResourceId nextResourceId='1");
        pw.print(" xmlns='http://hedeby.sunsource.net/hedeby-common'");
        pw.println(" xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'/>");
        pw.close();
        try {
            XMLUtil.loadAndValidate(tmp);
            fail("load on xml file with invalid epilog must fail");
        } catch(GrmXMLException ex) {
            log.log(Level.INFO, "Expected exception caught: " + ex.getLocalizedMessage(), ex);
            assertTrue("Cause must be an instanceof of SAXParseException", ex.getCause() instanceof SAXParseException);
        }
    }

    public void testLoadFileMissingEndOfTag() throws Exception {
        PrintWriter pw = new PrintWriter(tmp);
        pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        pw.print("<nextResourceId nextResourceId='1'");
        pw.print(" xmlns='http://hedeby.sunsource.net/hedeby-common'");
        pw.println(" xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");
        pw.close();
        try {
            XMLUtil.loadAndValidate(tmp);
            fail("load on xml file with invalid epilog must fail");
        } catch(GrmXMLException ex) {
            log.log(Level.INFO, "Expected exception caught: " + ex.getLocalizedMessage(), ex);
            assertTrue("Cause must be an instanceof of SAXParseException", ex.getCause() instanceof SAXParseException);
        }
    }

    public void testLoadFileMissingStartOfTag() throws Exception {
        PrintWriter pw = new PrintWriter(tmp);
        pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        pw.print("nextResourceId nextResourceId='1'");
        pw.print(" xmlns='http://hedeby.sunsource.net/hedeby-common'");
        pw.println(" xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'/>");
        pw.close();
        try {
            XMLUtil.loadAndValidate(tmp);
            fail("load on xml file with invalid epilog must fail");
        } catch(GrmXMLException ex) {
            log.log(Level.INFO, "Expected exception caught: " + ex.getLocalizedMessage(), ex);
            assertTrue("Cause must be an instanceof of SAXParseException", ex.getCause() instanceof SAXParseException);
        }
    }

    public void testLoadIllegalArgumentException() throws Exception {
        log.entering(XMLUtilTest.class.getName(), "testLoadIllegalArgumentException");
        
        try {
            XMLUtil.load((File)null);
            fail("XMLUtil.load(null) should throw an excpetion");
        } catch (GrmXMLException e) {
            assertTrue("XMLUtil.load(null) should throw an excpetion", true);
        }
        log.exiting(XMLUtilTest.class.getName(), "testLoadIllegalArgumentException");
    }
    
    public void testLoadJAXBException() throws Exception {
        log.entering(XMLUtilTest.class.getName(), "testLoadJAXBException");
        try {
            XMLUtil.load(tmp);
            fail("XMLUtil.load(tmp) should throw an excpetion");
        } catch (GrmXMLException e) {
            log.log(Level.FINE, "Exepcted error occured", e);
            assertTrue("XMLUtil.load(null) should throw an excpetion", true);
        }
        log.exiting(XMLUtilTest.class.getName(), "testLoadJAXBException");
    }

    private void store(ConfigurationMockup o) throws Exception {
        m.marshal(of.createConfigurationMockup(o), new FileWriter(tmp));
    }

    private ConfigurationMockup load() throws Exception {
        return (ConfigurationMockup)((JAXBElement)u.unmarshal(tmp)).getValue();
    }
    
    private class FooBar {       
    }
    
    private static class TestModule extends AbstractModule {
        
        public TestModule() {
            super("xml-test");
        }
        protected void loadCliExtension(Set<Class<? extends CliCommand>> extensions) {
            // Ignore
        }

        public String getVendor() {
            return "Sun Microsystems";
        }

        public String getVersion() {
            return "1.0";
        }

        public List<ConfigPackage> getConfigPackages() {
            List<ConfigPackage> cpList = new ArrayList<ConfigPackage>();          
            ClassLoader cl = getClass().getClassLoader();
            URL localURL = cl.getResource("com/sun/grid/grm/config/test/configurationMockupScheme.xsd");
            cpList.add(new ConfigPackageImpl("com.sun.grid.grm.config.test", 
                            "http://hedeby.sunsource.net/hedeby-test", 
                            "test", 
                            localURL, 
                            Collections.<String>emptyList()));           
            return cpList;
        }

        protected void loadValidatorExtension(Set<Class<? extends Validator>> extensions) {
            //Ignore
        }
        
    }
    
}
