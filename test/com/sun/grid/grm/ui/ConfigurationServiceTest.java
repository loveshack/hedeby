/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui;

import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ConfigurationService test
 */
public class ConfigurationServiceTest extends CSMockupTestCase {

    private final static Logger log = Logger.getLogger(ConfigurationServiceTest.class.getName());
    private ExecutionEnv env;
    private ConfigurationService cq;

    /** Creates a new instance of ComponentWrapperTest
     * @param name
     */
    public ConfigurationServiceTest(String name) {
        super(name);
    }

    /**
     *
     * @throws java.lang.Exception
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        env = getEnv();
        cq = new ConfigurationServiceEx(env, "cs", "cs");
    }

    /**
     *
     * @throws java.lang.Exception
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testRun() throws Exception {
        Command<Void> d = new ConcurrentForCommand();
        Command<Void> m = new NotConcurrentForCommand();

        for (int i = 0; i < 100; i++) {
            if ((i % 10) == 0) {
                cq.run(m);
            } else {
                cq.run(d);
            }
        }
        assertTrue("testRun execution failed", true);
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testConcurrentCallConcurrent() throws Exception {
        Command<Void> d = new ConcurrentCallConcurrentCommand(cq);
        cq.run(d);
        assertTrue("testConcurrentCallConcurrent execution failed", true);
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testConcurrentCallNotConcurrent() throws Exception {
        Command<Void> d = new ConcurrentCallNotConcurrentCommand(cq);
        cq.run(d);
        assertTrue("testConcurrentCallNotConcurrent execution failed", true);
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testNotConcurrentCallConcurrent() throws Exception {
        Command<Void> d = new NotConcurrentCallConcurrentCommand(cq);

        cq.run(d);
        assertTrue("testNotConcurrentCallConcurrent execution failed", true);
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testNotConcurrentCallNotConcurrent() throws Exception {
        Command<Void> d = new NotConcurrentCallNotConcurrentCommand(cq);

        try {
            cq.run(d);
            fail("testNotConcurrentCallNotConcurrent should throw an exception");
        } catch (GrmException e) {
            log.log(Level.FINE, "Expected error occured", e);
            assertTrue("testNotConcurrentCallNotConcurrent should throw an exception", true);
        }
    }

    public void testNotConcurrentCallNotConcurrentwithSession() throws Exception {
        Command<Void> d = new NestedModifyObjectsCommand(cq);


        cq.run(d);
        assertTrue("testNotConcurrentCallNotConcurrentwithSession", true);

    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testNotConcurrentCallNotConcurrentwithWrongSession() throws Exception {
        Command<Void> d = new NestedModifyTheSameObjectsCommand(cq);

        try {
            cq.run(d);
            fail("testNonConcurrentCallNonConcurrentwithWrongSession should throw an exception");
        } catch (GrmException e) {
            log.log(Level.FINE, "Expected error occured", e);
            assertTrue("NonConcurrentCallNonConcurrentwithWrongSession should throw an exception", true);
        }
    }
    public void testMultithreadedSubmission() throws Exception {
        ConfigurationServiceEx cq = new ConfigurationServiceEx(env, "cs", "cs");
        Thread t = new Thread(new Helper(cq));
        t.start();
        NotConcurrentForCommand cmd = new NotConcurrentForCommand();
        cq.run(cmd);
        for (int i = 0; i < 10; i++) {
            Thread u = new Thread(new Skelter(cq));
            u.start();
        }
        assertTrue("testMultithreadedSubmission execution failed", true);
    }

    private class Helper implements Runnable {

        private ConfigurationServiceEx _cq;

        public Helper(ConfigurationServiceEx cq) {
            _cq = cq;
        }

        public void run() {
            try {
                NotConcurrentForCommand cmd = new NotConcurrentForCommand();
                _cq.run(cmd);
            } catch (Exception ex) {
                log.log(Level.WARNING, "Unexpected error occured", ex);
            }
        }
    }

    private class Skelter implements Runnable {

        private ConfigurationServiceEx _cq;

        public Skelter(ConfigurationServiceEx cq) {
            _cq = cq;
        }

        public void run() {
            try {
                ConcurrentForCommand cmd = new ConcurrentForCommand();
                _cq.run(cmd);
            } catch (Exception ex) {
                log.log(Level.WARNING, "Unexpected error occured", ex);
            }
        }
    }
}
