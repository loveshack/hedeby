/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.util.prefs.FilePreferences;
import java.io.File;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import junit.framework.TestCase;

/**
 * This class tests the AddSystemCommand class.
 * <p>
 * Two consecutive installations on the same host are done in each test case.
 * In the 16 tests, all combinations of master and managed host install as well
 * as user and system preferences are tested. Each test checks in the end,
 * whether the respective system exists and that the installations succeeded or
 * failed as expected.
 * <p>
 * The test cases are named with four word combination. The first two
 * words refer to the first install, the third and fourth words refer to the
 * second install.
 * <p>
 * To be able to run the test as a non-root user (and to not be influenced by
 * any preinstalled system), the tests use a special directory (provided by
 * TestUtil.getTmpDir()) to store the preference .
 */
public class AddSystemCommandTest extends TestCase {
    private final static PreferencesType USER = PreferencesType.USER;
    private final static PreferencesType SYSTEM = PreferencesType.SYSTEM;
    private final static boolean MASTER_HOST = true;
    private final static boolean MANAGED_HOST = false;
    private final Logger logger = Logger.getLogger(AddSystemCommandTest.class.getName());
    
    private enum InstallStatus { OK, NOK };
    private enum SDMSystemExists { YES, NO };
    
    private InstallationUseCase iuc;

    public AddSystemCommandTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        File tmpDir = TestUtil.getTmpDir(this);
        
        // reset rootDir fields (and caching) in FilePreferences
        Field systemRootDirField = FilePreferences.class.getDeclaredField("systemRootDir");
        systemRootDirField.setAccessible(true);
        systemRootDirField.set(FilePreferences.class, new File(tmpDir, "systemPrefs"));
        systemRootDirField.setAccessible(false);
                
        Field systemRootField = FilePreferences.class.getDeclaredField("systemRoot");
        systemRootField.setAccessible(true);
        systemRootField.set(FilePreferences.class, null);
        systemRootField.setAccessible(false);
        
        Field userRootDirField = FilePreferences.class.getDeclaredField("userRootDir");
        userRootDirField.setAccessible(true);
        userRootDirField.set(FilePreferences.class, new File(tmpDir, "userPrefs"));
        userRootDirField.setAccessible(false);
                
        Field userRootField = FilePreferences.class.getDeclaredField("userRoot");
        userRootField.setAccessible(true);
        userRootField.set(FilePreferences.class, null);
        userRootField.setAccessible(false);
    }

    @Override
    protected void tearDown() throws Exception {
        iuc.cleanUpSystem();
        super.tearDown();
    }


    public void testSystemManagedSystemManaged() {
        iuc = new InstallationUseCase(SYSTEM, MANAGED_HOST, SYSTEM, MANAGED_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.NOK, SDMSystemExists.YES, SDMSystemExists.NO);
    }
    
    public void testSystemManagedSystemMaster() {
        iuc = new InstallationUseCase(SYSTEM, MANAGED_HOST, SYSTEM, MASTER_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.NOK, SDMSystemExists.YES, SDMSystemExists.NO);
    }
    
    public void testSystemManagedUserManaged() {
        iuc = new InstallationUseCase(SYSTEM, MANAGED_HOST, USER, MANAGED_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.OK, SDMSystemExists.YES, SDMSystemExists.YES);
    }
    
    public void testSystemManagedUserMasterm() {
        iuc = new InstallationUseCase(SYSTEM, MANAGED_HOST, USER, MASTER_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.OK, SDMSystemExists.YES, SDMSystemExists.YES);
    }

    public void testSystemMasterSystemManaged() {
        iuc = new InstallationUseCase(SYSTEM, MASTER_HOST, SYSTEM, MANAGED_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.NOK, SDMSystemExists.YES, SDMSystemExists.NO);
    }

    public void testSystemMasterSystemMaster() {
        iuc = new InstallationUseCase(SYSTEM, MASTER_HOST, SYSTEM, MASTER_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.NOK, SDMSystemExists.YES, SDMSystemExists.NO);
    }

    public void testSystemMasterUserManaged() {
        iuc = new InstallationUseCase(SYSTEM, MASTER_HOST, USER, MANAGED_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.OK, SDMSystemExists.YES, SDMSystemExists.YES);
    }

    public void testSystemMasterUserMaster() {
        iuc = new InstallationUseCase(SYSTEM, MASTER_HOST, USER, MASTER_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.OK, SDMSystemExists.YES, SDMSystemExists.YES);
    }

    public void testUserManagedSystemManaged() {
        iuc = new InstallationUseCase(USER, MANAGED_HOST, SYSTEM, MANAGED_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.OK, SDMSystemExists.YES, SDMSystemExists.YES);
    }

    public void testUserManagedSystemMaster() {
        iuc = new InstallationUseCase(USER, MANAGED_HOST, SYSTEM, MASTER_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.OK, SDMSystemExists.YES, SDMSystemExists.YES);
    }

    public void testUserManagedUserManaged() {
        iuc = new InstallationUseCase(USER, MANAGED_HOST, USER, MANAGED_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.NOK, SDMSystemExists.NO, SDMSystemExists.YES);
    }

    public void testUserManagedUserMaster() {
        iuc = new InstallationUseCase(USER, MANAGED_HOST, USER, MASTER_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.NOK, SDMSystemExists.NO, SDMSystemExists.YES);
    }

    public void testUserMasterSystemManaged() {
        iuc = new InstallationUseCase(USER, MASTER_HOST, SYSTEM, MANAGED_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.OK, SDMSystemExists.YES, SDMSystemExists.YES);
    }

    public void testUserMasterSystemMaster() {
        iuc = new InstallationUseCase(USER, MASTER_HOST, SYSTEM, MASTER_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.OK, SDMSystemExists.YES, SDMSystemExists.YES);
    }

    public void testUserMasterUserManaged() {
        iuc = new InstallationUseCase(USER, MASTER_HOST, USER, MANAGED_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.NOK, SDMSystemExists.NO, SDMSystemExists.YES);
    }

    public void testUserMasterUserMaster() {
        iuc = new InstallationUseCase(USER, MASTER_HOST, USER, MASTER_HOST);
        iuc.execUseCase();
        iuc.cmpResultWithExpected(InstallStatus.OK, InstallStatus.NOK, SDMSystemExists.NO, SDMSystemExists.YES);
    }

    public void testManagedOnMasterInstallError() throws GrmException {
        iuc = new InstallationUseCase(SYSTEM, MASTER_HOST, SYSTEM, MANAGED_HOST);

        // do both installs manually, first master install:
        iuc.cleanUpSystem();
        try {
            iuc.addDummySystem(iuc.prefTypeFirst, iuc.isMasterInstallFirst);
        } catch (GrmException ex) {
            // the first install should work
            logger.log(Level.SEVERE, null, ex);
            throw ex;
        }

        // ... second the managed host install
        try {
            iuc.addDummySystem(iuc.prefTypeSecond, iuc.isMasterInstallSecond);
        } catch (GrmException ex) {
            // should throw the correct error
            logger.log(Level.INFO, "Expected exception:", ex);
            TestUtil.assertMatchingError("Correct exception should be thrown.",
                    "AddSystemCommand.cannot_inst_managed_on_master", AddSystemCommand.BUNDLE, ex);
            return;
        }
        fail("Managed host install unexpectedly did not produce error");
    }
    
    class InstallationUseCase {

        private PreferencesType prefTypeFirst,  prefTypeSecond;
        private boolean isMasterInstallFirst,  isMasterInstallSecond;
        private boolean firstInstallSucceeded,  secondInstallSucceeded;

        private ExecutionEnv env;
        private String sysName;

        public InstallationUseCase(PreferencesType prefTypeFirst, boolean isMasterInstallFirst, PreferencesType prefTypeSecond, boolean isMasterInstallSecond) {

            this.prefTypeFirst = prefTypeFirst;
            this.prefTypeSecond = prefTypeSecond;
            this.isMasterInstallFirst = isMasterInstallFirst;
            this.isMasterInstallSecond = isMasterInstallSecond;
            
            env = DummyExecutionEnvFactory.newInstance();
            sysName = env.getSystemName();
        }
        
        private void addDummySystem(PreferencesType prefType, boolean isMasterInstall) throws GrmException {
            String csURL = env.getCSHost() + ":" + env.getCSPort();
            if (!isMasterInstall) {
                // installing managed host:
                // managed host install needs a configuration service URL,
                // so set any resolvable host (NOT localhost) as a 'dummy' master host
                csURL = "www.sun.com:" + env.getCSPort();
            } 
            AddSystemCommand asc = new AddSystemCommand(sysName, csURL, env.getLocalSpoolDir().getAbsolutePath(), env.getDistDir().getAbsolutePath(), prefType, isMasterInstall);
            asc.execute(env);
        }

        public void cleanUpSystem() {
            try {
                PreferencesUtil.deleteSystem(sysName, SYSTEM);
                PreferencesUtil.deleteSystem(sysName, USER);
            } catch (BackingStoreException ex) {
                Logger.getLogger(InstallationUseCase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void execUseCase() {
            cleanUpSystem();

            firstInstallSucceeded = false;
            secondInstallSucceeded = false;

            try {
                addDummySystem(prefTypeFirst, isMasterInstallFirst);
                firstInstallSucceeded = true;
            } catch (GrmException ex) {
                Logger.getLogger(InstallationUseCase.class.getName()).log(Level.SEVERE, null, ex);
            }

            try {
                addDummySystem(prefTypeSecond, isMasterInstallSecond);
                secondInstallSucceeded = true;
            } catch (GrmException ex) {
                Logger.getLogger(InstallationUseCase.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void cmpResultWithExpected(InstallStatus first, InstallStatus second, SDMSystemExists sysPref, SDMSystemExists userPref) {
            if (first == InstallStatus.OK) {
                assertTrue(genUseCaseDesc() + ": First install successful", firstInstallSucceeded);
            } else {
                assertFalse(genUseCaseDesc() + ": First install failed as expected", firstInstallSucceeded);
            }

            if (second == InstallStatus.OK) {
                assertTrue(genUseCaseDesc() + ": Second install successful", secondInstallSucceeded);
            } else {
                assertFalse(genUseCaseDesc() + ": Second install failed as expected", secondInstallSucceeded);
            }

            if (sysPref == SDMSystemExists.YES) {
                assertTrue(genUseCaseDesc() + ": bootstrap SYSTEM exists.", PreferencesUtil.existsSystem(sysName, SYSTEM));
            } else {
                assertFalse(genUseCaseDesc() + ": No bootstrap SYSTEM found as expected.", PreferencesUtil.existsSystem(sysName, SYSTEM));
            }

            if (userPref == SDMSystemExists.YES) {
                assertTrue(genUseCaseDesc() + ": bootstrap USER exists.", PreferencesUtil.existsSystem(sysName, USER));
            } else {
                assertFalse(genUseCaseDesc() + ": No bootstrap USER found as expected.", PreferencesUtil.existsSystem(sysName, USER));
            }
        }

        private String genUseCaseDesc() {
            String p1 = prefTypeFirst == SYSTEM ? "SYSTEM" : "USER";
            String p2 = prefTypeSecond == SYSTEM ? "SYSTEM" : "USER";
            String m1 = isMasterInstallFirst ? "master" : "managed";
            String m2 = isMasterInstallSecond ? "master" : "managed";

            return "UseCase(" + p1 + "," + m1 + "," + p2 + "," + m2 + ")";
        }
    }
}



