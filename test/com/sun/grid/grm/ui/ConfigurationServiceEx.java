/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.impl.ConfigurationServiceImpl;
import java.util.logging.Logger;

/**
 * Just for JUnit tests needs. Extended functionality that prints out the
 * iteration number.
 * 
 */
public class ConfigurationServiceEx extends ConfigurationServiceImpl {
    private static int CMD_COUNTER = 0;
    private final static Logger log = Logger.getLogger(ConfigurationServiceEx.class.getName());

    public ConfigurationServiceEx(ExecutionEnv env, String name, String config)  throws GrmException {
        super(env, name, config);
    }
    
    @Override
    public <T> Result<T> run(Command<T> c) throws GrmException, SchedulingTimeoutException {
        log.fine("run: " + CMD_COUNTER++);
        return super.run(c);
    }
    
}
