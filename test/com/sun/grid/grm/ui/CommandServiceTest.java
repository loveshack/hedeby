/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui;

import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.management.ComponentWrapper;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.impl.CommandServiceImpl;
import com.sun.grid.grm.ui.impl.ConfigurationServiceImpl;
import javax.management.ObjectName;

public class CommandServiceTest extends CSMockupTestCase {

    private ObjectName on;
    private ExecutionEnv env;
    private ComponentWrapper cw;
    private CommandServiceImpl commandService;

    /** Creates a new instance of ComponentWrapperTest
     * @param name
     */
    public CommandServiceTest(String name) {
        super(name);
    }

    /**
     *
     * @throws java.lang.Exception
     */
    @Override
    @SuppressWarnings("unchecked")
    protected void setUp() throws Exception {
        super.setUp(null);
        env = getEnv();

        on = ComponentInfo.createObjectName(env, BootstrapConstants.CS_ID, ConfigurationService.class);
        start();
        commandService = new CommandServiceImpl(env);
        ConfigurationService cq = new ConfigurationServiceImpl(env, "cs", "cs");
        cw = new ComponentWrapper(cq, ConfigurationService.class, on);
        getMBeanServer().registerMBean(cw, cw.getObjectName());
    }

    /**
     *
     * @throws java.lang.Exception
     */
    @Override
    protected void tearDown() throws Exception {
        getMBeanServer().unregisterMBean(cw.getObjectName());
        super.tearDown();
    }

    /**
     * Tests the basic CommandServiceImpl functionality to execute a Command
     * and resist the deadlock caused by call of CommandServiceImpl.execute nested 
     * in an executed command.
     * 
     * @throws java.lang.Exception
     */
    public void testExecute() throws Exception {
        Command<GlobalConfig> d = new GetGlobalConfigurationCommand();
        commandService.execute(d);
        assertTrue("testExecute execution failed", true);
    }
    
    /**
     * Tests the ability of underlying CommandServiceImpl infrastructutre 
     * (ConfigurationServiceInvocationHandler) to detect running inside a JVM which 
     * hosts ConfigurationService and bypass the JMX.
     * 
     * 
     * @throws java.lang.Exception
     */
    
    /*
     * Problem with this test is following...when the system property BootstrapConstants.IS_CS
     * is set to true the CommandServiceImpl is trying to get ConfigurationService instance from locally
     * running JVMImpl...but JVMImpl.getInstance() returns null as it was not initialized. 
     * We have (for now) no way to instantiate JVMImpl for testing purposes
     * JVMImpl is started from ParentStartupService
     */
    /**
     * Decided not to fix this now
     */
//    public void testExecuteDirectCSJvm() throws Exception {
//        System.setProperty(BootstrapConstants.IS_CS, Boolean.TRUE.toString());
//        Command<Void> d = new SecondLevelDeadLockCommand();
//        commandService.execute(d);
//        assertTrue("testExecuteDirectCSJvm execution failed", true);
//    }
}
