/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.ui.component.GetJVMConfigurationCommand;
import java.io.FilePermission;
import junit.framework.TestCase;

/**
 * Test for class CommandPermission
 */
public class CommandPermissionTest extends TestCase {
    
    /** Creates a new instance of CommandPermissionTest 
     *  @param name the name of the test
     */
    public CommandPermissionTest(String name) {
        super(name);
    }
    
    public void testEqualsTrue() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        CommandPermission p2 = new CommandPermission(GetConfigurationCommand.class);
        assertEquals("command permissions on same command class must be equal", p1, p2);
    }
    
    public void testEqualsSameClassFalse() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        CommandPermission p3 = new CommandPermission(GetJVMConfigurationCommand.class);
        assertNotSame("command permissions with different command class are not equal", p1, p3);
    }
    
    public void testEqualsDifferentClassFalse() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        FilePermission p4 = new FilePermission(TestUtil.getTmpDir(this).getPath(), "read");
        assertNotSame("CommandPermission is not equal to a FilePermission", p1, p4);
    }
    
    public void testHashcodeTrue() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        CommandPermission p2 = new CommandPermission(GetConfigurationCommand.class);
        assertEquals("command permissions for same command class must have the same hashcode", p1.hashCode(), p2.hashCode());
    }
    
    public void testHashcodeSameClassFalse() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        CommandPermission p3 = new CommandPermission(GetJVMConfigurationCommand.class);
        assertNotSame("command permissions for different command class must have the different hashcodes", p1.hashCode(), p3.hashCode());
    }
    
    public void testHashcodeDifferentClassFalse() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        FilePermission p4 = new FilePermission(TestUtil.getTmpDir(this).getPath(), "read");
        assertNotSame("hashcode of permissions with different classes must be different", p1.hashCode(), p4.hashCode());
    }
    
    public void testImpliesPlainNamesTrue() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        CommandPermission p2 = new CommandPermission(GetConfigurationCommand.class);
        assertTrue("command permissions for same command class implies each other", p1.implies(p2));
    }
    
    public void testImpliesPlainNameToPrefixFalse() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        CommandPermission p3 = new CommandPermission(GetConfigurationCommand.class.getPackage().getName() + "*");
        assertFalse("command permission for concrete command does not imply the command permission for the package", p1.implies(p3));
    }
    
    public void testImpliesPrefixToPlainNameTrue() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        CommandPermission p3 = new CommandPermission(GetConfigurationCommand.class.getPackage().getName() + "*");
        assertTrue("command permission for whole package implies command permission for a command of this package", p3.implies(p1));
    }
    
    public void testImpliesDifferentClassFalse() {
        CommandPermission p3 = new CommandPermission(GetConfigurationCommand.class.getPackage().getName() + "*");
        FilePermission p4 = new FilePermission(TestUtil.getTmpDir(this).getPath(), "read");

        assertFalse("command permission does not imply a permission with different class", p3.implies(p4));
    }
    
    public void testImpliesWildcardToPlainNameTrue() {
        CommandPermission p1 = new CommandPermission(GetConfigurationCommand.class);
        CommandPermission p5 = new CommandPermission("*");
        assertTrue("command permission for all commands implies command permission for concrete command", p5.implies(p1));
    }
    
    public void testImpliesWildcardToPrefixTrue() {
        CommandPermission p3 = new CommandPermission(GetConfigurationCommand.class.getPackage().getName() + "*");
        CommandPermission p5 = new CommandPermission("*");
        assertTrue("command permission for all commands implies command permission for concrete package", p5.implies(p3));
    }
    
    public void testImpliesPrefixToWildcardFalse() {
        CommandPermission p3 = new CommandPermission(GetConfigurationCommand.class.getPackage().getName() + "*");
        CommandPermission p5 = new CommandPermission("*");
        assertFalse("command permission for package does not imply command permission for all commandes", p3.implies(p5));
        
    }
}
