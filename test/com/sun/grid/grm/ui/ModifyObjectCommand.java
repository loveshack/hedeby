/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author rm199614
 */
public class ModifyObjectCommand extends AbstractSystemCommand<Void> implements CSModifyCommand{
    private static final long serialVersionUID = -2007072301L;
    private String name;
    /** Creates a new instance of ModifyObjectCommand */
    public ModifyObjectCommand(String name) {
        this.name = name;
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        int a = 0;
        for (int i=0; i<1000; i++) {
            a++;
        }
        return CommandResult.VOID_RESULT;
    }

    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(name);
    }
    
}
