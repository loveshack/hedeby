/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.impl.ConfigurationServiceImpl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.naming.NameNotFoundException;

/**
 *
 */
public class GetNextResourceIdCommandTest extends CSMockupTestCase {
    
    private ExecutionEnv env;
    
    public GetNextResourceIdCommandTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
        this.setSystemVersion(env);

    }

    @Override
    protected void tearDown() throws Exception {
        try {
            env.getContext().unbind(GetNextResourceIdCommand.RESOURCE_IDS_NAME);
        } catch(NameNotFoundException ex) {
            // fine
        }
        this.clearSystemVersion(env);
    }

    public void testSuccess() throws Exception {
        
        GetNextResourceIdCommand cmd = new GetNextResourceIdCommand();
        
        long id = cmd.execute(env).getReturnValue();
        
        assertEquals("first id must be one", 1, id);
        
        cmd = new GetNextResourceIdCommand();
        id = cmd.execute(env).getReturnValue();

        assertEquals("second id must be 2", 2, id);
        
    }
    
    public void testUndo() throws Exception {
        
        GetNextResourceIdCommand cmd = new GetNextResourceIdCommand();
        
        long id = cmd.execute(env).getReturnValue();
        assertEquals("first id must be 1", 1, id);
        
        cmd.undo(env);
        id = cmd.execute(env).getReturnValue();
        assertEquals("first id must be still 1", 1, id);
    }
    
    /**
     * Test the concurrent access of the GetNextResourceIdCommand.
     * 
     * <p>This test executes 100 instances of the GetNextResourceIdCommand concurrently
     *    via a thread pool. 
     *    All returns resource id ranges are stored in a list. Finally it is checked
     *    the the resource id list does not contain a duplicate entries.
     * 
     * @throws java.lang.Exception
     */
    public void testConcurrentAccess() throws Exception {

        // The DummyConfigurionService does not support handle concurrency in the same way
        // as the ConfigurationServiceImpl, it does not honour the list of modified names
        // of the Commands.
        // => use the ConfigurationServiceImpl
        final ConfigurationServiceImpl cs = new ConfigurationServiceImpl(env, "test", "test");
        
        List<Future<Long>> futures = new LinkedList<Future<Long>>();

        ExecutorService exe = Executors.newFixedThreadPool(5);
        try {
            int max = 100;
            for(int i = 0; i < max; i++) {

                Callable<Long> callable = new Callable<Long>() {
                    public Long call() throws Exception {
                        return cs.run(new GetNextResourceIdCommand()).getReturnValue();
                    }
                };
                futures.add(exe.submit(callable));
            }

            final List<Long> ids = new ArrayList<Long>(max);
            for(Future<Long> f: futures) {
                ids.add(f.get());
            }

            Collections.sort(ids);
            for(int i = 0; i < max; i++) {
                int execptedId = i+1;
                assertEquals("id[ " + i + "] must be " + execptedId, execptedId, ids.get(i).intValue());
            }
        } finally {
            exe.shutdownNow();
        }
    }
    
}
