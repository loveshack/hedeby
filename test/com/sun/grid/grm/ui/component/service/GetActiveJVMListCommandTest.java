/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.config.naming.DummyConfigurationServiceContext;
import java.util.List;
import javax.naming.Context;
import junit.framework.TestCase;

/**
 *
 */
public class GetActiveJVMListCommandTest extends TestCase {
    
    public GetActiveJVMListCommandTest(String testName) {
        super(testName);
    }

    private ExecutionEnv env;

    @Override
    protected void setUp() throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        Context ctx = env.getContext();
        if (ctx instanceof DummyConfigurationServiceContext) {
            ((DummyConfigurationServiceContext)ctx).clear();
        }
        super.tearDown();
    }

    private void setupJvms(String [] jvms) throws GrmException {
        for(String jvm: jvms) {
            ActiveJvm exe = new ActiveJvm();
            exe.setHost("localhost");
            exe.setName(jvm);

            StoreActiveJVMCommand cmd = new StoreActiveJVMCommand(exe);
            env.getCommandService().execute(cmd);
        }
    }
    public void testGetByJvmName() throws Exception {
        String [] jvms = new String [] {
            "executor_vm", "cs_vm", "rp_vm"
        };
        setupJvms(jvms);
        GetActiveJVMListCommand lcmd = new GetActiveJVMListCommand();
        lcmd.setJvmName("cs_vm");

        List<ActiveJvm> list = env.getCommandService().execute(lcmd).getReturnValue();

        assertEquals("Invalid number of jvms in list", 1, list.size());
        assertEquals("First entry must be the cs_vm", "cs_vm", list.get(0).getName());
    }
    
    public void testGetByJvmNameNotExists() throws Exception {
        String [] jvms = new String [] {
            "executor_vm", "rp_vm"
        };
        setupJvms(jvms);
        GetActiveJVMListCommand lcmd = new GetActiveJVMListCommand();
        lcmd.setJvmName("cs_vm");

        List<ActiveJvm> list = env.getCommandService().execute(lcmd).getReturnValue();

        assertEquals("Invalid number of jvms in list", 0, list.size());
    }
}
