/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.ui.component.AddComponentCommand;
import com.sun.grid.grm.ui.component.AddJVMConfigurationCommand;
import com.sun.grid.grm.ui.component.GetComponentCommand;
import com.sun.grid.grm.ui.component.GetJVMConfigurationsCommand;
import com.sun.grid.grm.ui.component.RemoveJVMConfigurationCommand;
import com.sun.grid.grm.config.common.MultiComponent;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.config.common.HostSet;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.ui.component.GetComponentConfigurationCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.Collections;
import java.util.List;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.ui.install.CreateManagedHostDirsCommand;


public class CommandsTest extends CSMockupTestCase {

    private ExecutionEnv env;



    /** Creates a new instance of ComponentWrapperTest
     * @param name
     */
    public CommandsTest(String name) {
        super(name);
    }


    /**
     *
     * @throws java.lang.Exception
     */
    @Override
    protected void setUp() throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
        setUpContext(env);
    }

/**
     *
     * @throws java.lang.Exception
     */
    @Override
    protected void tearDown() throws Exception {

        clearContext(env);
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testAddJVMConfigurationCommandSuccess() throws Exception {

        AddJVMConfigurationCommand addjvm = new AddJVMConfigurationCommand();

        JvmConfig jvm = new JvmConfig();
        jvm.setName("test_jvm");
        jvm.setPort(1080);
        jvm.setUser("mb199851");
        addjvm.setJVMConfig(jvm);

        Result<Void> r = env.getCommandService().<Void>execute(addjvm);

        assertNotNull("Result is null", r);

        GlobalConfig g = (GlobalConfig)env.getContext().lookup("global");
        assertNotNull("Global config was not stored", g);

        JvmConfig resultjvm = null;
        for (JvmConfig rjvm : g.getJvm()) {
            if (rjvm.getName().equals("test_jvm")) {
                resultjvm = rjvm;
            }
        }
        assertNotNull("JVM was not stored", resultjvm);
    }

    /**
     * for some reason, GrmException is wrapped to undeclared exc.
     * Adding existing Jvm
     * @throws java.lang.Exception
     * 
     */
    public void testAddJVMConfigurationCommandFail() throws Exception {

        AddJVMConfigurationCommand addjvm = new AddJVMConfigurationCommand();
        
        JvmConfig jvm = new JvmConfig();
        jvm.setName("test_jvm");
        addjvm.setJVMConfig(null);

        try {
            Result<Void> r = env.getCommandService().<Void>execute(addjvm);
            fail("Test had to throw a GrmException");
        } catch (GrmException grm) {
            assertTrue("Test had to throw a GrmException", true);
        } 
    }

    /**
     * It has to be defined whether to throw exception
     * if JVM is not found or not. We have to define it for all 
     * remove operations, right now it seems more appropriate not to throw an 
     * exception.
     *
     * @throws java.lang.Exception
     */
    public void testRemoveJVMConfigurationCommandNotFoundFail() throws Exception {

        RemoveJVMConfigurationCommand removejvm = new RemoveJVMConfigurationCommand();

        removejvm.setJVMName("jvm10");

        try {
            Result<Void> r = env.getCommandService().<Void>execute(removejvm);
            assertTrue("Test should not throw a GrmException", true);
        } catch (GrmException grm) {
            fail("Test should not throw a GrmException");
        } 
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testRemoveJVMConfigurationCommandSuccess() throws Exception {

        //check whether jvm exists
        GlobalConfig g = (GlobalConfig)env.getContext().lookup("global");
        assertNotNull("Before command run - Global config was not stored", g);

        JvmConfig resultjvm = null;
        for (JvmConfig jvm : g.getJvm()) {
            if (jvm.getName().equals("executor_vm")) {
                resultjvm = jvm;
            }
        }
        assertNotNull("JVM was not removed", resultjvm);
        
        
        RemoveJVMConfigurationCommand removejvm = new RemoveJVMConfigurationCommand();

        removejvm.setJVMName("executor_vm");

        Result<Void> r = env.getCommandService().<Void>execute(removejvm);

        assertNotNull("Result is null", r);
        assertNull("Return value of the result is not null", r.getReturnValue());
        
        g = null;
        g = (GlobalConfig)env.getContext().lookup("global");
        assertNotNull("Global config was not stored", g);

        resultjvm = null;
        for (JvmConfig jvm : g.getJvm()) {
            if (jvm.getName().equals("executor_vm")) {
                resultjvm = jvm;
            }
        }
        assertNull("JVM was not removed", resultjvm);
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testGetJVMConfigurationCommandSuccess() throws Exception {

        GetJVMConfigurationsCommand getjvm = new GetJVMConfigurationsCommand();

        getjvm.setJVMName("rp_vm");

        Result<List<JvmConfig>> r = env.getCommandService().<List<JvmConfig>>execute(getjvm);

        assertNotNull("Result is null", r);
        assertNotNull("Return value of the result is null", r.getReturnValue());

        assertEquals("Filter JVM name: " + getjvm.getJVMName() + " and retrieved JVM name: " + r.getReturnValue().get(0).getName() + " are different ", "rp_vm", r.getReturnValue().get(0).getName());
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testGetJVMConfigurationCommandFail() throws Exception {

        GetJVMConfigurationsCommand getjvm = new GetJVMConfigurationsCommand();
        // non-existing jvm
        getjvm.setJVMName("jvm10");

        Result<List<JvmConfig>> r = env.getCommandService().<List<JvmConfig>>execute(getjvm);

        assertNotNull("Result is null", r);
        assertNotNull("Return value of the result is null", r.getReturnValue());

        assertEquals("JVM config was found", 0, r.getReturnValue().size());
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testAddComponentCommandSuccess() throws Exception {

        AddComponentCommand addcompo = new AddComponentCommand();

        MultiComponent c = new MultiComponent();

        HostSet hosts = new HostSet();
        hosts.getExclude().add("latte");
        hosts.getInclude().add("mocca");

        c.setName("test_component");
        c.setClassname("com.sun.grid.grm.executor.Executor");
        c.setHosts(hosts);

        addcompo.setComponent(c);
        addcompo.setJVMName("additional_vm");

        Result<Void> r = env.getCommandService().<Void>execute(addcompo);

        assertNotNull("Result is null", r);        
        
        GlobalConfig g = (GlobalConfig)env.getContext().lookup("global");
        assertNotNull("Global config was not stored", g);

        JvmConfig resultjvm = null;
        for (JvmConfig jvm : g.getJvm()) {
            if (jvm.getName().equals("additional_vm")) {
                resultjvm = jvm;
            }
        }
        assertNotNull("JVM was not stored", resultjvm);

        MultiComponent resultcmp = null;
        for (Component cmp : resultjvm.getComponent()) {
            if (cmp.getName().equals("test_component")) {
                resultcmp = (MultiComponent)cmp;
            }
        }
        assertNotNull("Component was not stored", resultcmp);
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testAddComponentCommandJVMNotFoundFail() throws Exception {

        AddComponentCommand addcompo = new AddComponentCommand();

        MultiComponent c = new MultiComponent();

        HostSet hosts = new HostSet();
        hosts.getExclude().add("latte");
        hosts.getInclude().add("mocca");

        c.setName("test_component");
        c.setClassname("com.sun.grid.grm.executor.Executor");
        c.setHosts(hosts);

        addcompo.setComponent(c);
        addcompo.setJVMName("jvm10");

        try {
            Result<Void> r = env.getCommandService().<Void>execute(addcompo);
            fail("Test had to throw a GrmException");
        } catch (GrmException grm) {
            assertTrue("Test had to throw a GrmException", true);
        }
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testAddComponentCommandComponentAlreadyExistsFail() throws Exception {

        AddComponentCommand addcompo = new AddComponentCommand();

        MultiComponent c = new MultiComponent();

        HostSet hosts = new HostSet();
        hosts.getExclude().add("latte");
        hosts.getInclude().add("mocca");

        c.setName("spare_pool");
        c.setClassname("com.sun.grid.grm.executor.Executor");
        c.setHosts(hosts);

        addcompo.setComponent(c);
        addcompo.setJVMName("additional_vm");

        try {
            Result<Void> r = env.getCommandService().<Void>execute(addcompo);
            fail("Test had to throw a GrmException");
        } catch (GrmException grm) {
            assertTrue("Test had to throw a GrmException", true);
        }
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testGetComponentCommandComponentNameSetSuccess() throws Exception {

        GetComponentCommand getcompo = new GetComponentCommand();

        getcompo.setComponentName("executor");

        Result<List<Component>> r = env.getCommandService().<List<Component>>execute(getcompo);

        assertNotNull("Result is null", r);
        assertNotNull("Return value of the result is null", r.getReturnValue());

        assertEquals("Filter component name: " + getcompo.getComponentName() + " and retrieved component name are different", getcompo.getComponentName(), r.getReturnValue().get(0).getName());
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testGetComponentCommandComponentNameHostNameSetSuccess() throws Exception {

        GetComponentCommand getcompo = new GetComponentCommand();

        getcompo.setComponentName("spare_pool");
        getcompo.setHostname(Hostname.getLocalHost().getHostname());

        Result<List<Component>> r = env.getCommandService().<List<Component>>execute(getcompo);

        assertNotNull("Result is null", r);
        assertNotNull("Return value of the result is null", r.getReturnValue());

        assertEquals("Filter component name: " + getcompo.getComponentName() + " and retrieved component name are different", getcompo.getComponentName(), r.getReturnValue().get(0).getName());
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testGetComponentCommandComponentNameHostNameJVMNameSetSuccess() throws Exception {

        GetComponentCommand getcompo = new GetComponentCommand();

        getcompo.setComponentName("spare_pool");
        getcompo.setHostname(Hostname.getLocalHost().getHostname());
        getcompo.setJVMName("rp_vm");

        Result<List<Component>> r = env.getCommandService().<List<Component>>execute(getcompo);

        assertNotNull("Result is null", r);
        assertNotNull("Return value of the result is null", r.getReturnValue());

        assertEquals("Filter component name: " + getcompo.getComponentName() + " and retrieved component name are different", getcompo.getComponentName(), r.getReturnValue().get(0).getName());
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testGetComponentCommandComponentNameJVMNameSetSuccess() throws Exception {

        GetComponentCommand getcompo = new GetComponentCommand();

        getcompo.setComponentName("spare_pool");
        getcompo.setJVMName("rp_vm");

        Result<List<Component>> r = env.getCommandService().<List<Component>>execute(getcompo);

        assertNotNull("Result is null", r);
        assertNotNull("Return value of the result is null", r.getReturnValue());

        assertEquals("Filter component name: " + getcompo.getComponentName() + " and retrieved component name are different", getcompo.getComponentName(), r.getReturnValue().get(0).getName());
    }

    /**
     *
     * @throws java.lang.Exception
     */
    public void testGetComponentCommandJVMNameSetSuccess() throws Exception {
        
        int count = -1;
        
        GlobalConfig g = (GlobalConfig)env.getContext().lookup("global");
        for (JvmConfig jvm : g.getJvm()) {
            if (jvm.getName().equals("rp_vm")) {
                count = jvm.getComponent().size();
            }
        }
        
        assertNotSame("Jvm not found", count, -1);
        
        GetComponentCommand getcompo = new GetComponentCommand();

        getcompo.setJVMName("rp_vm");

        Result<List<Component>> r = env.getCommandService().<List<Component>>execute(getcompo);

        assertNotNull("Result is null", r);
        assertNotNull("Return value of the result is null", r.getReturnValue());

        assertEquals("Wrong number of components retrieved: " + r.getReturnValue().size(), count, r.getReturnValue().size());
    }

    /**
     * Define a behaviourwhat to do if component has empty string as "config"
     * @throws java.lang.Exception
     */
    public void testGetComponentConfigurationCommandSuccess() throws Exception {

        int count = -1;
        
        GlobalConfig g = (GlobalConfig)env.getContext().lookup("global");
        for (JvmConfig jvm : g.getJvm()) {
            if (jvm.getName().equals("rp_vm")) {
                count = jvm.getComponent().size();
            }
        }
        
        assertNotSame("Jvm not found", count, -1);
        
        GetComponentConfigurationCommand<ComponentConfig> getcompo = new GetComponentConfigurationCommand<ComponentConfig>();

        getcompo.setJVMName("rp_vm");

        Result<List<ComponentConfig>> r = env.getCommandService().<List<ComponentConfig>>execute(getcompo);

        assertNotNull("Result is null", r);
        assertNotNull("Return value of the result is null", r.getReturnValue());

        assertEquals("Wrong number of components retrieved: " + r.getReturnValue().size(), count, r.getReturnValue().size());
    }
    
    
    /**
     * Define a behaviourwhat to do if component has empty string as "config"
     * @throws java.lang.Exception
     */
    public void testCreateManagedHostDirsCommandSuccess() throws Exception {

        File tmp = TestUtil.getTmpDir(this);
        assertTrue("Can not create tmp directory", tmp.mkdirs());
        
        
        try {        
            File localSpoolDir = new File(tmp, "localspool");
            File distDir = new File(tmp, "dist");
            File grmUtilDir = new File(distDir, "util");
            File templDir = new File(grmUtilDir, "templates");
            templDir.mkdirs();
            File templateFile = new File(templDir, "logging.properties.template");
            templateFile.createNewFile();
            
            ExecutionEnv tmpEnv = ExecutionEnvFactory.newInstance("blubber", localSpoolDir, distDir, 
                                          Hostname.getLocalHost(), 0, Collections.<String, Object>emptyMap());
            
            CreateManagedHostDirsCommand cmd = new CreateManagedHostDirsCommand(System.getProperty("user.name"));

            Result<Void> r = tmpEnv.getCommandService().<Void>execute(cmd);

            assertNotNull("Result is null", r);

            assertTrue("localSpoolDir was not created", localSpoolDir.exists());
            assertTrue("log directory was not created", PathUtil.getLogPath(tmpEnv).exists());
            assertTrue("tmp directory was not created", PathUtil.getLocalTmpPath(tmpEnv).exists());
            assertTrue("security directory was not created", PathUtil.getLocalSecurityPath(tmpEnv).exists());
            assertTrue("run directory was not created", PathUtil.getLocalSpoolRunPath(tmpEnv).exists());
        } finally {
            Platform.getPlatform().removeDir(tmp, true);
        }
    }           
}
