/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * NeedTest.java
 * JUnit based test
 *
 * Created on May 3, 2006, 1:33 PM
 */

package com.sun.grid.grm.service;

import com.sun.grid.grm.resource.Resource;
import junit.framework.*;
import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.FilterVariable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TODO (RH) review this test
 *
 */
public class NeedTest extends TestCase {
    
    private final static Logger log = Logger.getLogger(NeedTest.class.getName());
    
    public NeedTest(String testName) {
        super(testName);
    }
    
    protected void setUp() throws Exception {
    }
    
    protected void tearDown() throws Exception {
    }
    
    public static Test suite() {
        TestSuite suite = new TestSuite(NeedTest.class);
        
        return suite;
    }
    
    /**
     * Test of urgency getter and setter, of class com.sun.grid.grm.service.Need.
     */
    public void testUrgency() {
        log.log(Level.FINE,"getUrgency");
        
        Usage urgency = new Usage(5);
        Need instance = new Need(urgency);
        
        Usage result = instance.getUrgency();
        assertEquals(urgency, result);
    }
    
    /**
     * Test of quantity getter and setter, of
     * class com.sun.grid.grm.service.Need.
     */
    public void testQuantity() {
        log.log(Level.FINE,"quantity");
        
        int quantity = 0;
        Need instance = new Need(new Usage(2), quantity);
        
        int result = instance.getQuantity();
        assertEquals(quantity, result);
    }
    
    public void testRequestFilter() {
        log.log(Level.FINE,"requestFilter");
        CompareFilter<Resource> filter = new CompareFilter<Resource>(new FilterVariable<Resource>("host"), new FilterConstant<Resource>("foo.bar"), CompareFilter.Operator.EQ);
        Need instance = new Need(filter, new Usage(2));
        assertEquals(instance.getResourceFilter().toString(), filter.toString());
    }
}
