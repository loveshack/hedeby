/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.MinResourceSLOConfig;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterHelper;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 */
public class MinResourceSLOTest extends TestCase {

    private final static Logger log = Logger.getLogger(MinResourceSLOTest.class.getName());
    private Resource res;
    private MinResourceSLOConfig sloConfig;
    private ServiceMockup service;

    public MinResourceSLOTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ExecutionEnv env = DummyExecutionEnvFactory.newInstance();
        res = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        sloConfig = new MinResourceSLOConfig();
        service = new ServiceMockup("service");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        res = null;
        sloConfig = null;
        service = null;
    }


    private void assertSingleNeed(SLOState result, int quantity, int urgency, Filter<Resource> requestFilter) {
        assertEquals("SLO run must produce exactly one need", 1, result.getNeeds().size());
        Need need = result.getNeeds().get(0);
        assertEquals("MinResourceSLO produced a need with invalid quantity", quantity, need.getQuantity());
        assertEquals("MinResourceSLO produced a need with invalid urgency", urgency, need.getUrgency().getLevel());
        assertEquals("MinResourceSLO produced a need with request", requestFilter, need.getResourceFilter());
    }

    private void assertNoNeed(SLOState result) {
        assertEquals("SLO run must not produce a need", 0, result.getNeeds().size());
    }

    private void assertNoUsage(SLOState result) {
        assertEquals("Usage map should have no element", 0, result.getUsageMap().size());
    }
    private void assertUsage(SLOState result, int usage) {
        assertEquals("Usage map should have just one element", 1, result.getUsageMap().size());
        assertTrue("Usage map should contain resource " + res, result.getUsageMap().containsKey(res));
        assertEquals("Usage map should contain resource " + res + " with usage " + usage,
                usage, result.getUsageMap().get(res).getLevel());
    }

    /**
     * Tests that the MinResourceSLO does not produce a Need and does not give to
     * a resource usage if the min attribute is 0.
     * Test regression of issue 710.
     *
     * @throws java.lang.Exception
     */
    public void testMinZero() throws Exception {
        log.entering(getClass().getName(), "testMinZero");

        sloConfig.setName("slo");
        sloConfig.setUrgency(10);
        sloConfig.setMin(0);
        sloConfig.setRequest(null);

        service.addResource(res);

        MinResourceSLO instance = new MinResourceSLO(sloConfig);
        SLOState result = instance.update(service.createSLOContext());
        assertNoNeed(result);
        assertNoUsage(result);

        log.exiting(getClass().getName(), "testMinZero");
    }

    
    /**
     * Tests that MinResourceSLO produces a Need if min attribute is 1 and
     * the service has no resource.
     * 
     * @throws java.lang.Exception
     */
    public void testNeedEmptyRequestFilter() throws Exception {
        log.entering(getClass().getName(), "testNeedEmptyRequestFilter");

        sloConfig.setName("slo");
        sloConfig.setUrgency(10);
        sloConfig.setMin(1);
        sloConfig.setRequest(null);

        MinResourceSLO instance = new MinResourceSLO(sloConfig);

        SLOState result = instance.update(service.createSLOContext());
        assertSingleNeed(result, 1, 10, ConstantFilter.<Resource>alwaysMatching());
        assertNoUsage(result);

        log.exiting(getClass().getName(), "testNeedEmptyRequestFilter");
    }

    public void testNeedWithRequestFilter() throws Exception {
        log.entering(getClass().getName(), "testNeedWithRequestFilter");

        String reqFilter = "1 = 'bar'";
        sloConfig.setName("slo");
        sloConfig.setUrgency(10);
        sloConfig.setMin(1);
        sloConfig.setRequest(reqFilter);

        MinResourceSLO instance = new MinResourceSLO(sloConfig);

        SLOState result = instance.update(service.createSLOContext());
        assertEquals("SLO name is different", sloConfig.getName(), result.getSloName());
        assertSingleNeed(result, 1, 10, FilterHelper.<Resource>parse(reqFilter));
        assertNoUsage(result);

        log.exiting(getClass().getName(), "testNeedWithRequestFilter");
    }

    public void testResourceFilter() throws Exception {
        log.entering(getClass().getName(), "testResourceFilterNotMatching");

        String reqFilter = "foo = 'bar'";
        sloConfig.setName("slo");
        sloConfig.setUrgency(10);
        sloConfig.setResourceFilter(reqFilter);
        sloConfig.setMin(1);
        
        res.setProperty("foo", "bar");
        service.addResource(res);

        SLOState result;
        MinResourceSLO instance;
        
        instance = new MinResourceSLO(sloConfig);
        result = instance.update(service.createSLOContext());
        assertNoNeed(result);
        assertUsage(result, 10);

        // Change the resource that the resource filter does no longer match
        res.setProperty( "foo", "foo");
        result = instance.update(service.createSLOContext());
        assertSingleNeed(result, 1, 10, ConstantFilter.<Resource>alwaysMatching());
        assertNoUsage(result);

        
        log.entering(getClass().getName(), "testResourceFilterNotMatching");
    }
}
