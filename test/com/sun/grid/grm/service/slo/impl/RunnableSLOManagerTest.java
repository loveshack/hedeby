/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.slo.InvalidatedSLOContextException;
import com.sun.grid.grm.service.slo.SLO;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.SLOContextFactory;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.service.slo.event.AbstractSLOManagerEvent;
import com.sun.grid.grm.service.slo.event.SLOErrorEvent;
import com.sun.grid.grm.service.slo.event.SLOManagerEventListener;
import com.sun.grid.grm.service.slo.event.SLOUpdatedEvent;
import com.sun.grid.grm.service.slo.event.UsageMapUpdatedEvent;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 */
public class RunnableSLOManagerTest extends TestCase {

    private final static Logger log = Logger.getLogger(RunnableSLOManagerTest.class.getName());
    
    private final static ThreadGroup THREAD_GROUP = new ThreadGroup("RunnableSLOManagerTest");
    
    private final static ThreadFactory THREAD_FACTORY = new ThreadFactory() {
        public Thread newThread(Runnable r) {
            return new Thread(THREAD_GROUP, r);
        }
    };
    
    public RunnableSLOManagerTest(String testName) {
        super(testName);
    }
    private static SLOContextFactory sloContextFactory = new SLOContextFactoryMockup();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        
        for(int i = 0; i < 10; i++) {
            if (THREAD_GROUP.activeCount() == 0) {
                return;
            } else {
                Thread.sleep(100);
            }
        }
        fail("RunnableSLOManager did not shutdown all threads");
    }
    
    public void testStop() throws Exception {
        
        RunnableSLOManager sloManager = new RunnableSLOManager(getName(), sloContextFactory, 100);

        MySLOManagerEventListener lis = new MySLOManagerEventListener();

        sloManager.addSLOManagerListener(lis);

        BlockingSLO slo = new BlockingSLO("slo1");
        sloManager.setSLOs(Collections.singletonList((SLO)slo));

        Thread t = THREAD_FACTORY.newThread(sloManager);
        t.start();
        log.fine("Waiting for update of BlockingSLO");
        slo.waitForUpdate();
        log.fine("update method of BlockingSLO is active => stopping the sloManager");
        sloManager.shutdown();

        assertTrue("Timeout while waiting for interrupt", lis.waitForError(InvalidatedSLOContextException.class, 1000));

        log.fine("slo calculation has been interrupted => waiting for end of slo manager thread");
        t.join(1000);
        assertFalse("Thread did not die", t.isAlive());
    }
    
    
    public void testCancelRequestForRemovedSLO() throws Exception {
        
        RunnableSLOManager sloManager = new RunnableSLOManager(getName(), sloContextFactory, 100);
        MySLOManagerEventListener lis = new MySLOManagerEventListener();
        
        sloManager.addSLOManagerListener(lis);

        SLO [] slos = {
            new DummySLO("slo0",new Need(Usage.MAX_VALUE, 1)),
            new DummySLO("slo1", new Need(Usage.MAX_VALUE, 1))
        };
        
        sloManager.setSLOs(Arrays.asList(slos));
        
        Thread t = THREAD_FACTORY.newThread(sloManager);
        t.start();
        
        for(SLO slo: slos) {
            SLOUpdatedEvent evt = lis.waitForSLOUpdateEvent(slo.getName(), 2000);
            assertNotNull("Timeout while waiting for slo update event", evt);
            assertEquals("slo state must have one need", evt.getNeeds().size(), 1);

            Need need = evt.getNeeds().get(0);

            assertEquals("slo state has invalid urgency ", need.getUrgency(), Usage.MAX_VALUE);
            assertEquals("slo state has invalid quantity", need.getQuantity(), 1);
        }
        
        // Delete slo1      
        sloManager.setSLOs(Collections.<SLO>singletonList(slos[0]));
        
        // SLO Manager must send cancel resource request for slo1
        SLOUpdatedEvent evt = lis.waitForSLOUpdateEvent(slos[1].getName(), 2000);
        assertNotNull("Timeout while waiting for slo update event", evt);
        assertEquals("slo state must have on need", 1, evt.getNeeds().size());
        
        Need need = evt.getNeeds().get(0);
        assertEquals("slo state has invalid quantity", need.getQuantity(), Need.EMPTY_NEED.getQuantity());
        
        
        sloManager.shutdown();
        t.join(1000);
        assertFalse("Thread did not die", t.isAlive());
    }
    
    public void testDisableAndEnable() throws Exception {
        RunnableSLOManager sloManager = new RunnableSLOManager(getName(), sloContextFactory, 100);
        StartStopSLOManagerListener lis = new StartStopSLOManagerListener();
        
        sloManager.addSLOManagerListener(lis);

        DummySLO slo = new DummySLO("slo1", new Need(Usage.MAX_VALUE, 1));
        sloManager.setSLOs(Collections.<SLO>singletonList(slo));
        
        Thread t = THREAD_FACTORY.newThread(sloManager);
        //we enable the manager and count the slo update events for some time
        t.start();
        Thread.sleep(2000);
        //we disable the manager and count the events
        sloManager.disable();        
        int eventCount = lis.getEventCount();
        //when manager is stopped we will try to trigger update on SLO
        sloManager.triggerUpdate();
        Thread.sleep(2000);
        //we will check that number of events before the update and now is same
        assertEquals("ERROR: Events were sent after slo Manager was stopped!!!",lis.getEventCount(), eventCount);
        //we will enable slo manager again and count the slo update events for some time
        sloManager.enable();
        Thread.sleep(2000);
        //we will shutdown slo definatelly and count the events
        sloManager.shutdown();
        eventCount = lis.getEventCount();
        //when manager is shutdown we will try to trigger update on SLO
        sloManager.triggerUpdate();
        Thread.sleep(2000);
        //we will check that number of events before the update and now is same
        assertEquals("ERROR: Events were sent after slo Manager was shutdown!!!",lis.getEventCount(), eventCount);
        t.join(1000);
        assertFalse("Thread did not die", t.isAlive());
    }
    
    public void testCancelRequestForEmptySLOs() throws Exception {
        
        RunnableSLOManager sloManager = new RunnableSLOManager(getName(), sloContextFactory, 100);
        MySLOManagerEventListener lis = new MySLOManagerEventListener();
        
        sloManager.addSLOManagerListener(lis);

        DummySLO slo = new DummySLO("slo1", new Need(Usage.MAX_VALUE, 1));
        sloManager.setSLOs(Collections.<SLO>singletonList(slo));
        
        Thread t = THREAD_FACTORY.newThread(sloManager);
        t.start();
        
        SLOUpdatedEvent evt = lis.waitForSLOUpdateEvent(slo.getName(), 2000);
        assertNotNull("Timeout while waiting for slo update event", evt);
        assertEquals("slo state must have on need", 1, evt.getNeeds().size());
        
        Need need = evt.getNeeds().get(0);
        
        assertEquals("slo state has invalid urgency ", need.getUrgency(), Usage.MAX_VALUE);
        assertEquals("slo state has invalid quantity", need.getQuantity(), 1);

        sloManager.setSLOs(Collections.<SLO>emptyList());
        
        evt = lis.waitForSLOUpdateEvent(slo.getName(), 2000);
        assertNotNull("Timeout while waiting for slo update event", evt);
        assertEquals("slo state must have on need", 1, evt.getNeeds().size());
        
        need = evt.getNeeds().get(0);
        assertEquals("slo state has invalid quantity", need.getQuantity(), Need.EMPTY_NEED.getQuantity());
        
        
        sloManager.shutdown();
        t.join(1000);
        assertFalse("Thread did not die", t.isAlive());
    }
    
    
    /**
     * This method tests that the shutdown method has no effect if the
     * RunnableSLOManager is not active.
     * 
     * @throws java.lang.Exception
     */
    public void testShutdownIfNotActive() throws Exception {
        log.entering(RunnableSLOManager.class.getName() , "testShutdownIfStopped");
        
        RunnableSLOManager sloManager = new RunnableSLOManager(getName(), sloContextFactory, 100);
        MySLOManagerEventListener lis = new MySLOManagerEventListener();
        
        sloManager.addSLOManagerListener(lis);

        DummySLO slo = new DummySLO("slo1", new Need(Usage.MAX_VALUE, 1));
        sloManager.setSLOs(Collections.<SLO>singletonList(slo));
        
        sloManager.shutdown();
        
        Thread t = THREAD_FACTORY.newThread(sloManager);
        t.start();
        
        log.fine("Waiting for first SLO update event");
        
        SLOUpdatedEvent evt = lis.waitForSLOUpdateEvent(slo.getName(), 2000);
        assertNotNull("Timeout while waiting for slo update event", evt);
        assertEquals("slo state must have on need", 1, evt.getNeeds().size());
        
        // check that the SLOManager did not die after the first
        // run
        // Clear the event history to be sure that we get results from
        // new update run
        lis.clear();
        
        log.fine("Waiting for seconds SLO update event");
        evt = lis.waitForSLOUpdateEvent(slo.getName(), 2000);
        assertNotNull("Timeout while waiting for slo update event", evt);
        assertEquals("slo state must have on need", 1, evt.getNeeds().size());
        
        log.fine("Shutdown the SLO manager");
        sloManager.shutdown();
        t.join(1000);
        
        log.exiting(RunnableSLOManager.class.getName() , "testShutdownIfStopped");
    }
    
    static class SLOContextFactoryMockup implements SLOContextFactory {

        public SLOContext createSLOContext() throws GrmException {
            return new DefaultSLOContext(Collections.<Resource>emptyList());
        }
    }

    public class DummySLO implements SLO {

        private final String name;
        private Need   need;
        
        public DummySLO(String name, Need need) {
            this.name = name;
            this.need = need;
        }
        
        public String getName() {
            return name;
        }
        
        public synchronized void setNeed(Need need) {
            this.need = need;
        } 

        public synchronized SLOState update(SLOContext ctx) throws GrmException {
            List<Need> needs = null;
            if(need == null) {
                needs = Collections.<Need>emptyList();
            } else {
                needs = Collections.<Need>singletonList(need);
            }
            return new DefaultSLOState(name, Collections.<Resource,Usage>emptyMap(), needs);
        }
    }
    
    public class BlockingSLO implements SLO {

        private final String name;
        
        public BlockingSLO(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public synchronized void waitForUpdate() throws InterruptedException {
            wait();
        }
        
        public SLOState update(SLOContext ctx) throws GrmException {
            try {
                while(true) {
                    synchronized(this) {
                        notifyAll();
                    }
                    if(Thread.currentThread().isInterrupted()) {
                        ctx.invalidate();
                    }
                    ctx.validate();
                    Thread.sleep(10);
                }
            } catch(InterruptedException ex) {
                //
            }
            return DefaultSLOState.createSLOStateWithNoNeed(name);
        }
    }
    
    class StartStopSLOManagerListener implements SLOManagerEventListener {

        List<SLOUpdatedEvent> list = new LinkedList<SLOUpdatedEvent>();
        
        public void sloUpdated(SLOUpdatedEvent event) {
            list.add(event);
        }

        public void usageMapUpdated(UsageMapUpdatedEvent event) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void sloUpdateError(SLOErrorEvent event) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        
        public int getEventCount() {
            return list.size();
        }
        
    }

    class MySLOManagerEventListener implements SLOManagerEventListener {

        private List<AbstractSLOManagerEvent> history = new LinkedList<AbstractSLOManagerEvent>();

        private void addEvent(AbstractSLOManagerEvent evt) {
            synchronized(this) {
                history.add(evt);
                notifyAll();
            }
            log.log(Level.FINE, "event {0} added to history", evt);
        }
        public void sloUpdated(SLOUpdatedEvent event) {
            addEvent(event);
        }

        public void usageMapUpdated(UsageMapUpdatedEvent event) {
            addEvent(event);
        }

        public void sloUpdateError(SLOErrorEvent event) {
            addEvent(event);
        }
        
        public void clear() {
            synchronized(this) {
                history.clear();
            }
        }
        public SLOUpdatedEvent waitForSLOUpdateEvent(String sloName, long timeout)  throws InterruptedException {
            long endTime = System.currentTimeMillis() + timeout;
            while (System.currentTimeMillis() < endTime) {
                synchronized (this) {
                    for (AbstractSLOManagerEvent evt : history) {
                        if (evt instanceof SLOUpdatedEvent) {
                            SLOUpdatedEvent eevt = (SLOUpdatedEvent) evt;
                            if(eevt.getSLOName().equals(sloName)) {
                                log.log(Level.FINE, "Found slo update event for slo {0}", sloName);
                                history.clear();
                                return eevt;
                            }
                        }
                    }
                    long remainingTime = endTime - System.currentTimeMillis();
                    if (remainingTime > 0) {
                        log.log(Level.FINE, "Waiting for slo update event of slo {0} ({1,number,0}ms)", 
                                new Object [] { sloName, remainingTime });
                        wait(remainingTime);
                    }
                }
            }
            log.log(Level.FINE, "Timeout while waiting for slo update event for slo {0}", sloName);
            return null;
        }
        
        public boolean waitForSLOUpdate(String sloName, long timeout)  throws InterruptedException {
            return waitForSLOUpdateEvent(sloName, timeout) != null;
        }
        
        public boolean waitForError(Class<?> expectedErrorClass, long timeout) throws InterruptedException {
            long endTime = System.currentTimeMillis() + timeout;
            while (System.currentTimeMillis() < endTime) {
                synchronized (this) {
                    for (AbstractSLOManagerEvent evt : history) {
                        if (evt instanceof SLOErrorEvent) {
                            SLOErrorEvent eevt = (SLOErrorEvent) evt;
                            Exception error = eevt.getError();
                            if (error.getClass().isAssignableFrom(expectedErrorClass)) {
                                history.clear();
                                return true;
                            }
                        }
                    }
                    long remainingTime = endTime - System.currentTimeMillis();
                    if (remainingTime > 0) {
                        log.log(Level.FINE, "Waiting for slo error event ({0}, {1,number,0}ms)", 
                                new Object [] { expectedErrorClass.getName(), remainingTime });
                        wait(remainingTime);
                    }
                }
            }
            log.log(Level.FINE, "Timeout while waiting for slo error event ({0})", expectedErrorClass.getName());
            return false;
        }
    }
}
