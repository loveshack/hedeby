package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.SLOContextFactory;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class ServiceMockup implements Service, SLOContextFactory {

    private final String name;
    private final List<Resource> list = new LinkedList<Resource>();

    public ServiceMockup(String name) {
        super();
        this.name = name;
    }

    public void startService() throws GrmException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public void stopService(boolean isFreeResources) throws GrmException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public ServiceState getServiceState() {
        return ServiceState.RUNNING;
    }

    public void addServiceEventListener(ServiceEventListener eventListener) {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public void removeServiceEventListener(ServiceEventListener eventListener) {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public List<Resource> getResources() throws ServiceNotActiveException {
        return list;
    }

    public Resource getResource(ResourceId resourceId) throws UnknownResourceException, ServiceNotActiveException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, ServiceNotActiveException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public void resetResource(ResourceId resource) throws UnknownResourceException, ServiceNotActiveException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    /**
     * @param resource to be added
     * @param sloName if the resource is added because of a SLO request it contains the SLO name
     * @throws com.sun.grid.grm.resource.InvalidResourceException
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     */
    public void addResource(Resource resource, String sloName) throws InvalidResourceException, ServiceNotActiveException {
        addResource(resource);
    }

    public void addResource(Resource resource) throws InvalidResourceException, ServiceNotActiveException {
        list.add(resource);
    }

    public void start() throws GrmException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public void stop(boolean isForced) throws GrmException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public void reload(boolean isForced) throws GrmException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public ComponentState getState() {
        return ComponentState.STARTED;
    }

    public void addComponentEventListener(ComponentEventListener componentEventListener) {
    }

    public void removeComponentEventListener(ComponentEventListener componentEventListener) {
    }

    public String getName() {
        return name;
    }

    public void modifyResource(ResourceId resource, ResourceChangeOperation operations) throws UnknownResourceException, ServiceNotActiveException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<SLOState> getSLOStates() throws GrmRemoteException {
        throw new UnsupportedOperationException("Not needed for tests.");
    }

    public List<Resource> getResources(Filter filter) throws ServiceNotActiveException, GrmRemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Hostname getHostname() throws GrmRemoteException {
        return Hostname.getLocalHost();
    }

    public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws ServiceNotActiveException, UnknownResourceException, GrmRemoteException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public SLOContext createSLOContext() throws GrmException {
        return new DefaultSLOContext(getResources());
    }

    public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
