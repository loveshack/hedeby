/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/

/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.config.common.ResourceProperty;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.slo.SLOState;

import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import junit.framework.TestCase;

import java.util.logging.Logger;

public class PermanentRequestSLOTest extends TestCase {

    private final static Logger log = Logger.getLogger(PermanentRequestSLOTest.class.getName());
    
    private Resource res;
    private ResourceProperty property;
    private PermanentRequestSLOConfig sloConfig;
    private ServiceMockup service;

    public PermanentRequestSLOTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ExecutionEnv env = DummyExecutionEnvFactory.newInstance();
        res = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        property = new ResourceProperty();
        sloConfig = new PermanentRequestSLOConfig();
        service = new ServiceMockup("service");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        res = null;
        property = null;
        sloConfig = null;
        service = null;
    }

    /**
     * Test of update method, of class PermanentRequestSLO.
     * @throws Exception 
     */
    public void testUpdateNoFilter() throws Exception {
        log.fine("update");

        property.setName(HostResourceType.HOSTNAME.getName());
        property.setValue(HostResourceType.HOSTNAME.getValue(res.getProperties()).toString());

        sloConfig.setName("slo");
        sloConfig.setUrgency(10);
        sloConfig.setQuantity(1);
        sloConfig.setRequest(null);

        service.addResource(res);

        PermanentRequestSLO instance = new PermanentRequestSLO(sloConfig);
        
        SLOState result = instance.update(service.createSLOContext());
        assertEquals("SLO name is different", sloConfig.getName(),
                result.getSloName());
        assertTrue("Needs list should contain just one need",
                result.getNeeds().size() == 1);

        Need n = result.getNeeds().get(0);
        assertTrue("Need quantity should be one", n.getQuantity() == 1);
        assertTrue("Need urgency is different",
                n.getUrgency().getLevel() == sloConfig.getUrgency());
        assertTrue("Need request is not correct", n.getResourceFilter() instanceof ConstantFilter);

        assertTrue("Usage map should have just one element",
                result.getUsageMap().size() == 1);
        assertTrue("Usage map should contain resource " + res,
                result.getUsageMap().containsKey(res));
        assertTrue("Usage map should contain resource " + res + " with urgency " +
                sloConfig.getUrgency(),
                result.getUsageMap().get(res).getLevel() == sloConfig.getUrgency());
    }

    /**
     * Test of update method, of class PermanentRequestSLO.
     * @throws java.lang.Exception 
     */
    public void testUpdateFilterPassed() throws Exception {
        log.fine("update");

        property.setName(ResourceVariableResolver.TYPE_VAR);
        property.setValue("host");

        sloConfig.setResourceFilter(String.format("%s = \"%s\"", HostResourceType.HOSTNAME.getName(),
                HostResourceType.HOSTNAME.getValue(res.getProperties())));
        sloConfig.setName("slo");
        sloConfig.setUrgency(10);
        sloConfig.setQuantity(1);
        sloConfig.setRequest(null);

        service.addResource(res);

        PermanentRequestSLO instance = new PermanentRequestSLO(sloConfig);
        SLOState result = instance.update(service.createSLOContext());
        assertEquals("SLO name is different", sloConfig.getName(),
                result.getSloName());
        assertTrue("Needs list should contain just one need",
                result.getNeeds().size() == 1);

        Need n = result.getNeeds().get(0);
        assertTrue("Need quantity should be one", n.getQuantity() == 1);
        assertTrue("Need urgency is different",
                n.getUrgency().getLevel() == sloConfig.getUrgency());
        assertTrue("Need request is not correct", n.getResourceFilter() instanceof ConstantFilter);

        assertTrue("Usage map should have just one element",
                result.getUsageMap().size() == 1);
        assertTrue("Usage map should contain resource " + res,
                result.getUsageMap().containsKey(res));
        assertTrue("Usage map should contain resource " + res + " with urgency " +
                sloConfig.getUrgency(),
                result.getUsageMap().get(res).getLevel() == sloConfig.getUrgency());
    }

    /**
     * Test of update method, of class PermanentRequestSLO.
     * @throws Exception 
     */
    public void testUpdateFilterNotPassedBadValue() throws Exception {
        log.fine("update");

        property.setName(ResourceVariableResolver.TYPE_VAR);
        property.setValue("host");

        String firstOp = HostResourceType.HOSTNAME.getName();
        String secondOp = String.format("\"%sfoobar\"", HostResourceType.HOSTNAME.getValue(res.getProperties()));

        String filter = String.format("%s = %s", firstOp, secondOp);
        sloConfig.setName("slo");
        sloConfig.setUrgency(10);
        sloConfig.setQuantity(1);
        sloConfig.setRequest(filter);
        sloConfig.setResourceFilter(filter);

        service.addResource(res);

        PermanentRequestSLO instance = new PermanentRequestSLO(sloConfig);
        SLOState result = instance.update(service.createSLOContext());
        assertEquals("SLO name is different", sloConfig.getName(),
                result.getSloName());
        assertTrue("Needs list should contain just one need",
                result.getNeeds().size() == 1);

        Need n = result.getNeeds().get(0);
        assertTrue("Need quantity should be one", n.getQuantity() == 1);
        assertTrue("Need urgency is different",
                n.getUrgency().getLevel() == sloConfig.getUrgency());

        Filter nFilter = n.getResourceFilter();
        assertTrue("Need request filter must be a CompareFilter", nFilter instanceof CompareFilter);

        CompareFilter cFilter = (CompareFilter) nFilter;
        assertEquals("Invalid first operant of request filter", firstOp, cFilter.getFirstOperant().toString());
        assertEquals("Invalid second operant of request filter", secondOp, cFilter.getSecondOperant().toString());
        assertEquals("Invalid operator of request filter", CompareFilter.Operator.EQ, cFilter.getOperator());

        assertTrue("Usage map should have no element",
                result.getUsageMap().size() == 0);
    }
    
        /**
     * This test makes sure that it is possible to set a usage on the PermanentRequestSLO
     * that is separate from the urgency.
     * 
     * @throws java.lang.Exception
     */
    public void testSettingSeparateUsage() throws Exception {
        log.fine("testSettingSeparateUsage");
        
        // setup configuration with one resource and urgency 1 (==usage)
        sloConfig.setName("slo");
        sloConfig.setUrgency(1);
        sloConfig.setQuantity(1);
        
        service.addResource(res);
        PermanentRequestSLO instance = new PermanentRequestSLO(sloConfig);
        
        SLOState result = instance.update(service.createSLOContext());
        assertEquals("Needs list should contain just one need", 1, result.getNeeds().size());
        Need n = result.getNeeds().get(0);
        assertEquals("Need urgency is 1", 1, n.getUrgency().getLevel());
        assertEquals("Usage map should have just one element", 1, result.getUsageMap().size());
        assertTrue("Usage map should contain resource " + res, result.getUsageMap().containsKey(res));
        assertEquals("Usage map should contain resource " + res + " with usage 1", 1, result.getUsageMap().get(res).getLevel());
        
        // now change the usage to 10
        sloConfig.setUsage(10);
        instance = new PermanentRequestSLO(sloConfig);
        
        result = instance.update(service.createSLOContext());
        assertEquals("Needs list should still contain just one need", 1, result.getNeeds().size());
        n = result.getNeeds().get(0);
        assertEquals("Need urgency is still 1", 1, n.getUrgency().getLevel());
        assertEquals("Usage map should still have just one element", 1, result.getUsageMap().size());
        assertTrue("Usage map should still contain resource " + res, result.getUsageMap().containsKey(res));
        assertEquals("Usage map should contain resource " + res + " with usage 10", 10, result.getUsageMap().get(res).getLevel());
    }
}
