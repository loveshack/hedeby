/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.impl.ResourceIdImpl;
import com.sun.grid.grm.service.ServiceState;
import java.util.Collections;
import java.util.List;

/**
 * Test for service caching proxy. The test tests situation, when remote service and
 * SCP acts as not active (remote service state is STARTING|SHUTDOWN|STOPPED|ERROR|UNKNOWN). 
 */
public class SCPRefreshRemoteServiceNotActiveTest extends AbstractSCPRefreshTest {

    public SCPRefreshRemoteServiceNotActiveTest(String aTestName) {
        super(aTestName);
    }

    /**
     * Helper method that drives tests for all variations or remote service state
     * that are supported by this test suite.
     * 
     * @param info                      description of a test
     * @param cachedState               original cached of a resource (if cached) or null if resource was not cached before
     * @param remoteState               cached of a resource owned by remote service
     * @param refreshedState            cached of a resource after refresh (result of event processing)
     * @param isUnique                  flag signaling whether the subject resource is unique
     * @param willBeAmbiguous           flag signaling whether the subject resource will become ambiguous
     * @param willBeRegistered          flag signaling whether the subject resource will be registered after refresh
     * @param attributesChanged         flag signaling whether the remote resource's attributes changed during test since resource was cached
     * @param cachedResourceDiffers     flag signaling whether the cached resource properties is different from remote resource properties
     * @param cachedResourceIsAmbiguous flag signaling whether the cached resource is ambiguous
     * @param remoteStatic          flag signaling whether the remote resource is static
     * @param id                        resource id for the resource
     * @throws java.lang.Exception      
     */
    @Override
    protected void genericResourceRefreshTestVariations(final String short_info,
            final Resource.State cachedState,
            final Resource.State remoteState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final boolean attributesChanged,
            final boolean cachedResourceDiffers,
            final boolean cachedResourceIsAmbiguous,
            final boolean remoteStatic,
            final ResourceId id) throws Exception {
        genericResourceRefreshTest(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, cachedResourceDiffers, cachedResourceIsAmbiguous, remoteStatic, id, ServiceState.STARTING);
        //genericResourceRefreshTest(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, cachedResourceDiffers, cachedResourceIsAmbiguous, remoteStatic, id, ServiceState.SHUTDOWN);
        genericResourceRefreshTest(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, cachedResourceDiffers, cachedResourceIsAmbiguous, remoteStatic, id, ServiceState.STOPPED);
        genericResourceRefreshTest(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, cachedResourceDiffers, cachedResourceIsAmbiguous, remoteStatic, id, ServiceState.ERROR);
        genericResourceRefreshTest(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, cachedResourceDiffers, cachedResourceIsAmbiguous, remoteStatic, id, ServiceState.UNKNOWN);
    }

    /**
     * The method will resolve whether the outgoing events in what order should be triggered for
     * given combination of state of cached and remote resource.
     *     
     * This method works also for change in ambiguous flag and static field, as Resource is using
     * the same method for identifying the change in ambiguos flag or static field.
     * 
     * Below is description of what events have to be sent - delimiter state the incoming (remote)
     * event, column headers are descriptive.
     *
     * cached   cached state    remote  remote state    remote diff     comment                     forwarded events
     * *        *               *       *               *               -                           -
     *
     * @param cached state of the cached resource or null if resource was not cached
     * @param remote state of the remote resource or null if remote service does not own resource
     * @return ordered list of events that has to be sent be SCP
     */
    @Override
    public List<Class> whatEventsWillBeTriggered(Resource.State cached,
            Resource.State remote,
            boolean attributesChanged) {
        return Collections.<Class>emptyList();
    }
    
    
    // ---------------------- JUNIT TESTS SECTION UNIQUE REMOTE RESOURCE -----------------------------
    public void testUnknownResourceRemoteASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.ASSIGNING, null, true, false, false, false, id);
    }

    public void testUnknownResourceRemoteASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.ASSIGNED, null, true, false, false, false, id);
    }

    public void testUnknownResourceRemoteUNASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteUNASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.UNASSIGNING, null, true, false, false, false, id);
    }

    public void testUnknownResourceRemoteERROR() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteERROR";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.ERROR, null, true, false, false, false, id);
    }

    public void testUnknownResourceRemoteUNASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteUNASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.UNASSIGNED, null, true, false, false, false, id);
    }

    public void testASSIGNINGResourceRemoteASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteASSIGNING";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, true, false, id);
    }

    public void testASSIGNINGResourceRemoteASSIGNINGChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteASSIGNINGChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNED, Resource.State.ASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteASSIGNED";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNED, Resource.State.ASSIGNING, true, true, false, id);
    }

    public void testASSIGNINGResourceRemoteASSIGNEDChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteASSIGNEDChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNED, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteUNASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUNASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteUNASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteUNASSIGNING";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, true, true, false, id);
    }

    public void testASSIGNINGResourceRemoteUNASSIGNINGChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUNASSIGNINGChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteUknown() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUknown";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, null, Resource.State.ASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteUnknown() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteUnknown";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, null, Resource.State.ASSIGNING, true, true, false, id);
    }

    public void testASSIGNINGResourceRemoteERROR() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteERROR";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ERROR, Resource.State.ASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteERROR() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteERROR";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ERROR, Resource.State.ASSIGNING, true, true, false, id);
    }

    public void testASSIGNINGResourceRemoteERRORChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteERRORChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ERROR, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteUNASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUNASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNED, Resource.State.ASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteUNASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteUNASSIGNED";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNED, Resource.State.ASSIGNING, true, true, false, id);
    }

    public void testASSIGNINGResourceRemoteUNASSIGNEDChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUNASSIGNEDChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNED, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteASSIGNING";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, true, false, id);
    }

    public void testASSIGNEDResourceRemoteASSIGNINGChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteASSIGNINGChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteASSIGNED";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, true, false, id);
    }

    public void testASSIGNEDResourceRemoteASSIGNEDChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteASSIGNEDChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteUNASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUNASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteUNASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteUNASSIGNING";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, true, false, id);
    }

    public void testASSIGNEDResourceRemoteUNASSIGNINGChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUNASSIGNINGChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteUknown() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUknown";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, null, Resource.State.ASSIGNED, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteUknown() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteUknown";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, null, Resource.State.ASSIGNED, true, true, false, id);
    }

    public void testASSIGNEDResourceRemoteERROR() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteERROR";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteERROR() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteERROR";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ERROR, Resource.State.ASSIGNED, true, true, false, id);
    }

    public void testASSIGNEDResourceRemoteERRORChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteERRORChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteUNASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUNASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNED, Resource.State.ASSIGNED, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteUNASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteUNASSIGNED";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNED, Resource.State.ASSIGNED, true, true, false, id);
    }

    public void testASSIGNEDResourceRemoteUNASSIGNEDChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUNASSIGNEDChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNED, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteASSIGNING";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, true, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteASSIGNINGChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteASSIGNINGChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteASSIGNED";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, true, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteASSIGNEDChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteASSIGNEDChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUNASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUNASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteUNASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteUNASSIGNING";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUNASSIGNINGChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUNASSIGNINGChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUknown() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUknown";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, null, Resource.State.UNASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteUknown() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteUknown";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, null, Resource.State.UNASSIGNING, true, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteERROR() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteERROR";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ERROR, Resource.State.UNASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteERROR() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteERROR";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ERROR, Resource.State.UNASSIGNING, true, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteERRORChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteERRORChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ERROR, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUNASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUNASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNED, Resource.State.UNASSIGNING, true, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteUNASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteUNASSIGNED";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNED, Resource.State.UNASSIGNING, true, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUNASSIGNEDChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUNASSIGNEDChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNED, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testERRORResourceRemoteASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNING, Resource.State.ERROR, true, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteASSIGNING";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNING, Resource.State.ERROR, true, true, false, id);
    }

    public void testERRORResourceRemoteASSIGNINGChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteASSIGNINGChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNING, Resource.State.ERROR, true, false, false, id);
    }

    public void testERRORResourceRemoteASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNED, Resource.State.ERROR, true, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteASSIGNED";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNED, Resource.State.ERROR, true, true, false, id);
    }

    public void testERRORResourceRemoteASSIGNEDChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteASSIGNEDChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNED, Resource.State.ERROR, true, false, false, id);
    }

    public void testERRORResourceRemoteUNASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUNASSIGNING";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNING, Resource.State.ERROR, true, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteUNASSIGNING() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteUNASSIGNING";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNING, Resource.State.ERROR, true, true, false, id);
    }

    public void testERRORResourceRemoteUNASSIGNINGChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUNASSIGNINGChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNING, Resource.State.ERROR, true, false, false, id);
    }

    public void testERRORResourceRemoteUknown() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUknown";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, null, Resource.State.ERROR, true, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteUknown() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteUknown";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, null, Resource.State.ERROR, true, true, false, id);
    }

    public void testERRORResourceRemoteERROR() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteERROR";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ERROR, Resource.State.ERROR, true, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteERROR() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteERROR";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ERROR, Resource.State.ERROR, true, true, false, id);
    }

    public void testERRORResourceRemoteERRORChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteERRORChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ERROR, Resource.State.ERROR, true, false, false, id);
    }

    public void testERRORResourceRemoteUNASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUNASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNED, Resource.State.ERROR, true, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteUNASSIGNED() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteUNASSIGNED";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNED, Resource.State.ERROR, true, true, false, id);
    }

    public void testERRORResourceRemoteUNASSIGNEDChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUNASSIGNEDChanged";
        genericChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNED, Resource.State.ERROR, true, false, false, id);
    }
    // ---------------------- JUNIT TESTS SECTION - AMBIGUOUS REMOTE RESOURCE ------------------------------
    public void testUnknownResourceRemoteASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.ASSIGNING, null, false, false, false, false, id);
    }

    public void testUnknownResourceRemoteASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteASSIGNEDNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.ASSIGNED, null, false, false, false, false, id);
    }

    public void testUnknownResourceRemoteUNASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteUNASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.UNASSIGNING, null, false, false, false, false, id);
    }

    public void testUnknownResourceRemoteERRORNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteERRORNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.ERROR, null, false, false, false, false, id);
    }

    public void testUnknownResourceRemoteUNASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteUNASSIGNEDNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, null, Resource.State.UNASSIGNED, null, false, false, false, false, id);
    }

    public void testASSIGNINGResourceRemoteASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteASSIGNINGNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteASSIGNINGNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteASSIGNINGNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testASSIGNINGResourceRemoteASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteASSIGNEDNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNED, Resource.State.ASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteASSIGNEDNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNED, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteASSIGNEDNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteASSIGNEDNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNED, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testASSIGNINGResourceRemoteUNASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUNASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteUNASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteUNASSIGNINGNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteUNASSIGNINGNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteUNASSIGNINGNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testASSIGNINGResourceRemoteUknownNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUknownNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, null, Resource.State.ASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteUknownNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteUnknownNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, null, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testASSIGNINGResourceRemoteERRORNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteERRORNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ERROR, Resource.State.ASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteERRORNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteERRORNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ERROR, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteERRORNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteERRORNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.ERROR, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testASSIGNINGResourceRemoteUNASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUNASSIGNEDNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNED, Resource.State.ASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteUNASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteUNASSIGNEDNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNED, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoteUNASSIGNEDNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoteUNASSIGNEDNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNED, Resource.State.ASSIGNING, false, true, false, id);
    }

    public void testASSIGNEDResourceRemoteASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteASSIGNINGNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteASSIGNINGNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteASSIGNINGNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testASSIGNEDResourceRemoteASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteASSIGNEDNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteASSIGNEDNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteASSIGNEDNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteASSIGNEDNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testASSIGNEDResourceRemoteUNASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUNASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteUNASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteUNASSIGNINGNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteUNASSIGNINGNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteUNASSIGNINGNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testASSIGNEDResourceRemoteUknownNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUknownNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, null, Resource.State.ASSIGNED, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteUknownNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteUknownNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, null, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testASSIGNEDResourceRemoteERRORNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteERRORNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ERROR, Resource.State.ASSIGNED, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteERRORNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteERRORNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteERRORNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteERRORNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testASSIGNEDResourceRemoteUNASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUNASSIGNEDNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNED, Resource.State.ASSIGNED, false, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteUNASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteUNASSIGNEDNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNED, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoteUNASSIGNEDNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoteUNASSIGNEDNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNED, Resource.State.ASSIGNED, false, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteASSIGNINGNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteASSIGNINGNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteASSIGNINGNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteASSIGNED";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteASSIGNEDNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteASSIGNEDNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteASSIGNEDNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUNASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUNASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteUNASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteUNASSIGNINGNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteUNASSIGNINGNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteUNASSIGNINGNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUknownNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUknown";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, null, Resource.State.UNASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteUknownNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteUknownNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, null, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteERRORNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteERRORNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ERROR, Resource.State.UNASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteERRORNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteERRORNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ERROR, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteERRORNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteERRORNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.ERROR, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUNASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUNASSIGNEDNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNED, Resource.State.UNASSIGNING, false, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteUNASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteUNASSIGNEDNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNED, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoteUNASSIGNEDNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoteUNASSIGNEDNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNED, Resource.State.UNASSIGNING, false, true, false, id);
    }

    public void testERRORResourceRemoteASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNING, Resource.State.ERROR, false, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteASSIGNINGNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNING, Resource.State.ERROR, false, true, false, id);
    }

    public void testAmbiguousERRORResourceRemoteASSIGNINGNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteASSIGNINGNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNING, Resource.State.ERROR, false, true, false, id);
    }

    public void testERRORResourceRemoteASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteASSIGNEDNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNED, Resource.State.ERROR, false, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteASSIGNEDNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNED, Resource.State.ERROR, false, true, false, id);
    }

    public void testAmbiguousERRORResourceRemoteASSIGNEDNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteASSIGNEDNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNED, Resource.State.ERROR, false, true, false, id);
    }

    public void testERRORResourceRemoteUNASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUNASSIGNINGNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNING, Resource.State.ERROR, false, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteUNASSIGNINGNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteUNASSIGNINGNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNING, Resource.State.ERROR, false, true, false, id);
    }

    public void testAmbiguousERRORResourceRemoteUNASSIGNINGNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteUNASSIGNINGNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNING, Resource.State.ERROR, false, true, false, id);
    }

    public void testERRORResourceRemoteUknownNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUknownNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, null, Resource.State.ERROR, false, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteUknownNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteUknownNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, null, Resource.State.ERROR, false, true, false, id);
    }

    public void testERRORResourceRemoteERRORNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteERRORNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ERROR, Resource.State.ERROR, false, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteERRORNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteERRORNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ERROR, Resource.State.ERROR, false, true, false, id);
    }

    public void testAmbiguousERRORResourceRemoteERRORNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteERRORNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.ERROR, Resource.State.ERROR, false, true, false, id);
    }

    public void testERRORResourceRemoteUNASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUNASSIGNEDNotUnique";
        genericNonAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNED, Resource.State.ERROR, false, false, false, false, id);
    }

    public void testAmbiguousERRORResourceRemoteUNASSIGNEDNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteUNASSIGNEDNotUnique";
        genericAmbiguousCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNED, Resource.State.ERROR, false, true, false, id);
    }

    public void testAmbiguousERRORResourceRemoteUNASSIGNEDNotUniqueChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoteUNASSIGNEDNotUniqueChanged";
        genericAmbiguousChangedCachedResourceRefreshTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNED, Resource.State.ERROR, false, true, false, id);
    }
//    ----------------- NONAMBIGUOUS STATIC ------------------------------------
    public void testUnknownResourceRemoteASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, null, Resource.State.ASSIGNING, null, true, false, false, id);
    }

    public void testUnknownResourceRemoteASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, null, Resource.State.ASSIGNED, null, true, false, false, id);
    }

    public void testUnknownResourceRemoteUNASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteUNASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, null, Resource.State.UNASSIGNING, null, true, false, false, id);
    }

    public void testUnknownResourceRemoteERRORStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteERRORStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, null, Resource.State.ERROR, null, true, false, false, id);
    }

    public void testUnknownResourceRemoteUNASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoteUNASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, null, Resource.State.UNASSIGNED, null, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNING, Resource.State.ASSIGNED, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteUNASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUNASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteUknownStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUknownStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNING, null, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteERRORStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteERRORStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNING, Resource.State.ERROR, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNINGResourceRemoteUNASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoteUNASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNING, Resource.State.UNASSIGNED, Resource.State.ASSIGNING, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNED, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteUNASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUNASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteUknownStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUknownStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNED, null, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteERRORStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteERRORStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNED, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testASSIGNEDResourceRemoteUNASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoteUNASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ASSIGNED, Resource.State.UNASSIGNED, Resource.State.ASSIGNED, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUNASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUNASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUknownStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUknownStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.UNASSIGNING, null, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteERRORStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteERRORStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.UNASSIGNING, Resource.State.ERROR, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testUNASSIGNINGResourceRemoteUNASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoteUNASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.UNASSIGNING, Resource.State.UNASSIGNED, Resource.State.UNASSIGNING, true, false, false, id);
    }

    public void testERRORResourceRemoteASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNING, Resource.State.ERROR, true, false, false, id);
    }

    public void testERRORResourceRemoteASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ERROR, Resource.State.ASSIGNED, Resource.State.ERROR, true, false, false, id);
    }

    public void testERRORResourceRemoteUNASSIGNINGStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUNASSIGNINGStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNING, Resource.State.ERROR, true, false, false, id);
    }

    public void testERRORResourceRemoteUknownStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUknownStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ERROR, null, Resource.State.ERROR, true, false, false, id);
    }

    public void testERRORResourceRemoteERRORStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteERRORStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ERROR, Resource.State.ERROR, Resource.State.ERROR, true, false, false, id);
    }

    public void testERRORResourceRemoteUNASSIGNEDStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoteUNASSIGNEDStatic";
        genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(short_info, Resource.State.ERROR, Resource.State.UNASSIGNED, Resource.State.ERROR, true, false, false, id);
    }

    /* COMMENT - tests for INPROCESS and UNASSIGNED cached resources are not needed, as it is
     * buggy implementation */
    /* COMMENT - the only not explicitly covered cases are :
     * 
     * - "cached resource, remote not unique with changed properties" -> as change
     * in ambiguous flag uses the same mechanism as resource property change, this
     * case is covered by "cached resource, remote not unique resource"
     *  
     * - "ambiguous cached resource, unique remote resource with changed properties" ->
     * as change in amibuous flag uses the same mechanism as resource property changes,
     * this case is covered by "ambiguous cached resource, unique remote resource"
     * 
     * TODO cover these cases explicitly (for sure) in the future
     */
}
