/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.ComponentTestAdapter;

/**
 * Junit test for testing the service state transitions with a dummy component
 */
public class ServiceContainerStateTransitionTest extends AbstractServiceStateTransitionTester<MockupServiceContainer> {

    public ServiceContainerStateTransitionTest(String name) {
        super(name);
    }

    private class MyServiceTestAdapter extends DefaultServiceTestAdapter<MockupServiceContainer> {

        @Override
        protected MockupServiceContainer createComponent() {
            addFeatures(ServiceFeature.values());
            return new MockupServiceContainer(null, "a_" + getName());
        }

        private void addFeatures(ServiceFeature... features) {
            for (ServiceFeature f : features) {
                super.addFeature(f, new MyFeatureHandler(f));
            }
        }

        public class MyFeatureHandler implements FeatureHandler {

            private final ServiceFeature f;

            public MyFeatureHandler(ServiceFeature f) {
                this.f = f;
            }

            public void setup() throws GrmException {
                getComponent().setup(f);
            }

            public void teardown() {
                getComponent().teardown(f);
            }

            public void waitUntil() throws InterruptedException {
                getComponent().waitUntil(f);
            }
        }
    }

    @Override
    protected ComponentTestAdapter<MockupServiceContainer> createComponentTestAdapter() throws Exception {
        return new MyServiceTestAdapter();
    }
}
