/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.impl.FixedResourceIdFactory;
import com.sun.grid.grm.resource.impl.ResourceImpl;
import com.sun.grid.grm.resource.impl.ResourcePropertyInsertOperation;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.AbstractServiceChangedResourceEvent;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Base class for for service caching proxy event protocol test - use case of event actions.
 */
public class AbstractSCPEventsTest extends TestCase {

    protected static final Logger log = Logger.getLogger(AbstractSCPEventsTest.class.getName());
    /* time for which should listeners wait for delivering events */
    protected static final long TIMEOUT = 200;
    /* when we do not want to perform refresh action after SCP creation */
    protected static final boolean INF = false;
    protected ResourceType rt;
    protected final Map<String, Object> new_rp = new HashMap<String, Object>();
    protected final String host = Hostname.getLocalHost().getHostname();
    protected ExecutionEnv env;

    public AbstractSCPEventsTest(String aTestName) {
        super(aTestName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        env = DummyExecutionEnvFactory.newInstance();
        rt = DefaultResourceFactory.getResourceType("test");
        new_rp.put("test", "test");
        /* cleanup spooled cache */
        Platform.getPlatform().removeDir(env.getLocalSpoolDir(), true);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        /* cleanup spooled cache */
        Platform.getPlatform().removeDir(env.getLocalSpoolDir(), true);
    }

    /**
     * The method will resolve whether the outgoing events in what order should be triggered for
     * given combination of state of cached resource and incoming event from remote service.
     *     
     * More details are provided in child classes.
     *
     * @param state state of the cached resource or null if resource was not cached
     * @param remote state of the remote resource or null if remote service does not own resource
     * @return ordered list of events that has to be sent be SCP
     */
    public List<Class> whatEventsWillBeTriggered(Resource.State state,
            Class<?> in,
            boolean attributesChanged) {
        // should be overriden
        return Collections.<Class>emptyList();
    }

    /**
     * The method will resolve in what state the remote resource was when
     * service has sent the event.
     * 
     * Some events are not strictly related to state (reset, changed) - in that
     * case, ASSIGNED state is returned.
     * 
     * @param event the event that sent the remote service
     * @return resource state of the remote resource
     */
    private Resource.State eventToState(Class<?> event) {
        if (event.isAssignableFrom(RemoveResourceEvent.class)) {
            return Resource.State.UNASSIGNING;
        }
        if (event.isAssignableFrom(ResourceResetEvent.class)) {
            return Resource.State.ASSIGNED;
        }
        if (event.isAssignableFrom(ResourceRemovedEvent.class)) {
            return Resource.State.UNASSIGNED;
        }
        if (event.isAssignableFrom(AddResourceEvent.class)) {
            return Resource.State.ASSIGNING;
        }
        if (event.isAssignableFrom(ResourceAddedEvent.class)) {
            return Resource.State.ASSIGNED;
        }
        if (event.isAssignableFrom(ResourceErrorEvent.class)) {
            return Resource.State.ERROR;
        }
        if (event.isAssignableFrom(ResourceChangedEvent.class)) {
            return Resource.State.ASSIGNED;
        }
        if (event.isAssignableFrom(ResourceRejectedEvent.class)) {
            return Resource.State.UNASSIGNED;
        }
        return Resource.State.UNASSIGNED;
    }

    /**
     * The method will create an instance of event to be sent by FiringServiceMockup
     * 
     * @param event class of the event to be sent
     * @param r resource to be contained in event
     * @param serviceName name of service to be contained in event
     * @param message a message to be contained in event
     * @return instance of resource event
     */
    private AbstractServiceChangedResourceEvent createResourceEvent(Class<?> event, Resource r, String serviceName, String message) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (event.isAssignableFrom(RemoveResourceEvent.class)) {
            return RemoveResourceEvent.class.getDeclaredConstructor(long.class, String.class, Resource.class, String.class).newInstance(0, serviceName, r, message);
        }
        if (event.isAssignableFrom(ResourceResetEvent.class)) {
            return ResourceResetEvent.class.getDeclaredConstructor(long.class, String.class, Resource.class, String.class).newInstance(0, serviceName, r, message);
        }
        if (event.isAssignableFrom(ResourceRemovedEvent.class)) {
            return ResourceRemovedEvent.class.getDeclaredConstructor(long.class, String.class, Resource.class, String.class).newInstance(0, serviceName, r, message);
        }
        if (event.isAssignableFrom(AddResourceEvent.class)) {
            return AddResourceEvent.class.getDeclaredConstructor(long.class, String.class, Resource.class, String.class).newInstance(0, serviceName, r, message);
        }
        if (event.isAssignableFrom(ResourceAddedEvent.class)) {
            return ResourceAddedEvent.class.getDeclaredConstructor(long.class, String.class, Resource.class, String.class).newInstance(0, serviceName, r, message);
        }
        if (event.isAssignableFrom(ResourceErrorEvent.class)) {
            return ResourceErrorEvent.class.getDeclaredConstructor(long.class, String.class, Resource.class, String.class).newInstance(0, serviceName, r, message);
        }
        if (event.isAssignableFrom(ResourceChangedEvent.class)) {
            return ResourceChangedEvent.class.getDeclaredConstructor(long.class, String.class, Resource.class, Collection.class, String.class).newInstance(0, serviceName, r, Collections.<ResourceChanged>emptyList(), message);
        }
        if (event.isAssignableFrom(ResourceRejectedEvent.class)) {
            return ResourceRejectedEvent.class.getDeclaredConstructor(long.class, String.class, Resource.class, String.class).newInstance(0, serviceName, r, message);
        }
        throw new IllegalStateException("not correct event class");
    }

    /**
     * helper method, that sets cached state of scp. should be used by 
     * genericResourceRefreshTestVariations() to test all wanted service states
     *
     * @param info                  description of a test
     * @param incomingEvent         event sent by remote service and received by SCP
     * @param cachedState           original state of a resource (if cached) or null if resource was not cached before
     * @param refreshedState        state of a resource after refresh (result of event processing)
     * @param isUnique              flag signaling whether the subject resource is unique
     * @param willBeAmbiguous       flag signaling whether the subject resource will become ambiguous
     * @param willBeRegistered          flag signaling whether the subject resource will be registered after refresh
     * @param attributesChanged         flag signaling whether the remote resource's attributes changed during test since resource was cached
     * @param cachedResourceDiffers     flag signaling whether the cached resource properties is different from remote resource properties
     * @param cachedResourceIsAmbiguous flag signaling whether the cached resource is ambiguous
     * @param remoteStatic          flag signaling whether the remote resource is static
     * @param id                    resource id for the resource
     * @throws java.lang.Exception      
     */
    protected boolean genericResourceEventTest(final String short_info,
            final Class incomingEvent,
            final Resource.State cachedState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final boolean attributesChanged,
            final boolean cachedResourceDiffers,
            final boolean cachedResourceIsAmbiguous,
            final boolean remoteStatic,
            final ResourceId id,
            final ServiceState ss) throws Exception {
        log.fine(short_info);
        log.fine("scp cached service state: " + ss);
        /* resource manager mockup takes an argument that specifies whether
         * registerResourceId method has to return true or false.
         * as these test are not testing real resource manager implementation or 
         * any real service implementation, the returned value has to be 
         * the same as value of flg indicating whether the resourc is unique */
        ResourceManagerMockup rm = new ResourceManagerMockup(isUnique);
        /* not 100% ok, but we believe that ResourceImpl is bugfree, anyway we
         * just need Resource.State members */
        Resource remote_resource = new ResourceImpl(rt, new FixedResourceIdFactory(id), rt.createResourcePropertiesForName(id.getId()));
        
        /* resource has to come from service, so we need to set its state according
         * to incomingEvent */
        remote_resource.setState(eventToState(incomingEvent));

        List<Class> eventsToReceive = whatEventsWillBeTriggered(cachedState, incomingEvent, attributesChanged);

        FiringResourceEventsServiceMockup s = new FiringResourceEventsServiceMockup(short_info, host);

        
        // The SCP test does not like the ResourceChangedEvent containing a single ResourceAnnotationChanged
        // instances. Such events has been introduced with the fix of issue 618. The IgnoreResourceAnnotationChangedEventFilter
        // does not accept such events.
        // This filter is a hack to avoid the reimplementation of many SCP junit tests
        GenericOrderWatchingResourceSEL sel = new GenericOrderWatchingResourceSEL(remote_resource, eventsToReceive
                , IgnoreResourceAnnotationChangedEventFilter.getInstance());

        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, INF);
        
        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong)field.get(instance)).set(0);
        field.setAccessible(false);
        
        try {
            instance.setServiceState(ss);
            instance.addServiceEventListener(sel);
            Resource cached_resource;
            if (cachedState != null) {
                ResourceStore res = instance.getResourceStore();
                /* prepare cached resource instance */
                cached_resource = remote_resource.clone();
                cached_resource.setState(cachedState);
//                cached_resource.setAmbiguous(cachedResourceIsAmbiguous);
                res.add(cached_resource);
            }
            /* now make remote resource 'changed' if needed */
            if (cachedResourceDiffers) {
                ResourcePropertyInsertOperation insert = new ResourcePropertyInsertOperation("test", "test");
                remote_resource.modify(insert);
            }
            /* now make remote resource 'static' if needed - it is safe now, as cached_resource is already cloned */
            remote_resource.setStatic(remoteStatic);

            s.fireResourceEvent(createResourceEvent(incomingEvent, remote_resource, short_info, short_info));

            
            while(instance.hasActiveActions()) {
                Thread.sleep(10);
            }
            log.fine("actions in SCP finished");

            while (instance.svcEventForwarder.getNumberOfPendingEvents() > 0) {
                Thread.sleep(10);
            }
            log.fine("Event support has delivered all events");
                    
            
            /* wait for action processing ... */
            boolean ret = sel.hasAll(TIMEOUT);
            
            
            StringBuilder sb = new StringBuilder();
            for (Class c : sel.getAwaitEvents()) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                sb.append(c.getName().toString());
            }
            sb.insert(0, "Events that should have been sent by SCP: ");
            String should_been_sent = sb.toString();

            sb = new StringBuilder();
            for (Class c : sel.getReceivedEvents()) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                sb.append(c.getName().toString());
            }
            sb.insert(0, "Events that have been sent by SCP: ");
            String been_sent = sb.toString();

            /* check cached resource instance */
            ResourceStore res = instance.getResourceStore();
            try {
                cached_resource = res.getResource(id);
            } catch (UnknownResourceException ure) {
                cached_resource = null;
            }

            log.fine(should_been_sent);
            log.fine(been_sent);
            assertTrue(should_been_sent, ret);
            boolean order_is_ok = true;
            for (int i = 0; i < sel.getAwaitEvents().size(); i++) {
                order_is_ok = order_is_ok && sel.getAwaitEvents().get(i).equals(sel.getReceivedEvents().get(i));
            }
            assertTrue("Event were received in wrong order: \n " + should_been_sent + "\n" + been_sent, order_is_ok);

//            if (willBeRegistered) {
//                assertEquals("Resource should be registered", id, rm.getRegisteredId());
//            } else {
//                assertNull("Resource should not be registered", rm.getRegisteredId());
//            }

            if (refreshedState != null) {
                assertEquals("Expected different refreshed resource state", refreshedState, cached_resource.getState());
//                assertEquals("Expected different ambiguous state", willBeAmbiguous, cached_resource.isAmbiguous());
            } else {
                assertNull("Resource should not be cached", cached_resource);
            }
        } finally {
            instance.destroy();
        }
        return true;
    }

    /**
     * Helper method that drives tests for all variations or scp cached service state
     * that are supported by this test suite.
     * 
     * @param short_info                  description of a test
     * @param incomingEvent         event sent by remote service and received by SCP
     * @param cachedState           original state of a resource (if cached) or null if resource was not cached before
     * @param refreshedState        state of a resource after refresh (result of event processing)
     * @param isUnique              flag signaling whether the subject resource is unique
     * @param willBeAmbiguous       flag signaling whether the subject resource will become ambiguous
     * @param willBeRegistered          flag signaling whether the subject resource will be registered after refresh
     * @param attributesChanged         flag signaling whether the remote resource's attributes changed during test since resource was cached
     * @param cachedResourceDiffers     flag signaling whether the cached resource properties is different from remote resource properties
     * @param cachedResourceIsAmbiguous flag signaling whether the cached resource is ambiguous
     * @param remoteStatic          flag signaling whether the remote resource is static
     * @param id                    resource id for the resource
     * @throws java.lang.Exception      
     */
    protected void genericResourceEventTestVariations(final String short_info,
            final Class incomingEvent,
            final Resource.State cachedState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final boolean attributesChanged,
            final boolean cachedResourceDiffers,
            final boolean cachedResourceIsAmbiguous,
            final boolean remoteStatic,
            final ResourceId id) throws Exception {
        // todo need to override
    }

    /**
     * Convenient way to trigger test for a event sync. Cached resource is
     * not ambiguous.
     */
    protected void genericNonAmbiguousCachedResourceEventTest(final String short_info,
            final Class incomingEvent,
            final Resource.State cachedState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final boolean attributesChanged,
            final ResourceId id) throws Exception {
        genericResourceEventTestVariations(short_info, incomingEvent, cachedState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, false, false, false, id);
    }

    /**
     * Convenient way to trigger test for a event sync. Cached resource is
     * not ambiguous, but remote resource is static.
     */
    protected void genericNonAmbiguousCachedResourceEventRemoteStaticTest(final String short_info,
            final Class incomingEvent,
            final Resource.State cachedState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final ResourceId id) throws Exception {
        boolean attributesChanged;
        if (refreshedState == null || cachedState == null) {
            attributesChanged = false;
        } else {
            attributesChanged = true;
        }
        genericResourceEventTestVariations(short_info, incomingEvent, cachedState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, false, false, true, id);
    }

    /**
     * Convenient way to trigger test for a event sync. Cached resource is
     * not ambiguous, remote resource has different properties.
     */
    protected void genericNonAmbiguousCachedResourceEventRemoteChangedTest(final String short_info,
            final Class incomingEvent,
            final Resource.State cachedState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final ResourceId id) throws Exception {
        boolean attributesChanged;
        if (refreshedState == null || cachedState == null) {
            attributesChanged = false;
        } else {
            attributesChanged = true;
        }
        genericResourceEventTestVariations(short_info, incomingEvent, cachedState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, true, false, false, id);
    }

    /**
     * Convenient way to trigger test for a refresh method. Cached resource is
     * ambiguous (before the refresh), but not different from remote resource.
     */
    protected void genericAmbiguousCachedResourceEventTest(final String short_info,
            final Class incomingEvent,
            final Resource.State cachedState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final boolean attributesChanged,
            final ResourceId id) throws Exception {
        genericResourceEventTestVariations(short_info, incomingEvent, cachedState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, false, true, false, id);
    }

    public void testPlaceHolder() {
        assertTrue("to make test pass", true);
    }
}
