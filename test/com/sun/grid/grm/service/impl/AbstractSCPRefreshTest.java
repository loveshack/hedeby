/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.impl.FixedResourceIdFactory;
import com.sun.grid.grm.resource.impl.ResourceImpl;
import com.sun.grid.grm.resource.impl.ResourcePropertyInsertOperation;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.util.Platform;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Base class for for service caching proxy event protocol test - use case of refresh action.
 */
public class AbstractSCPRefreshTest extends TestCase {

    protected static final Logger log = Logger.getLogger(AbstractSCPRefreshTest.class.getName());
    /* time for which should listeners wait for delivering events */
    protected static final long TIMEOUT = 200;
    /* use this value when debugging junit tests */
    //    private static final long DEBUG_TIMEOUT = Long.MAX_VALUE;
    protected static final boolean INF = false;
    protected ResourceType rt;
    protected final String host = "remote_host";
    protected ExecutionEnv env;

    public AbstractSCPRefreshTest(String aTestName) {
        super(aTestName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        env = DummyExecutionEnvFactory.newInstance();
        rt = DefaultResourceFactory.getResourceType("test");
        /* cleanup spooled cache */
        Platform.getPlatform().removeDir(env.getLocalSpoolDir(), true);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        /* cleanup spooled cache */
        Platform.getPlatform().removeDir(env.getLocalSpoolDir(), true);
    }

    /**
     * The method will resolve whether the outgoing events in what order should be triggered for
     * given combination of state of cached and remote resource.
     *     
     * More details are provided in child classes.
     * 
     * @param cached state of the cached resource or null if resource was not cached
     * @param remote state of the remote resource or null if remote service does not own resource
     * @return ordered list of events that has to be sent be SCP
     */
    public List<Class> whatEventsWillBeTriggered(Resource.State cached,
            Resource.State remote,
            boolean attributesChanged) {
        return Collections.<Class>emptyList();
    }

    /* helper method, that sets state of remote service. should be used by 
     * genericResourceRefreshTestVariations() to test all wanted service states*/
    protected void genericResourceRefreshTest(final String short_info,
            final Resource.State cachedState,
            final Resource.State remoteState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final boolean attributesChanged,
            final boolean cachedResourceDiffers,
            final boolean cachedResourceIsAmbiguous,
            final boolean remoteStatic,
            final ResourceId id,
            final ServiceState ss) throws Exception {
        log.fine(short_info);
        log.fine("service state: " + ss);
        /* resource manager mockup takes an argument that specifies whether
         * registerResourceId method has to return true or false.
         * as these test are not testing real resource manager implementation or 
         * any real service implementation, the returned value has to be 
         * the same as value of flg indicating whether the resourc is unique */
        ResourceManagerMockup rm = new ResourceManagerMockup(isUnique);
        /* not 100% ok, but we believe that ResourceImpl is bugfree, anyway we
         * just need Resource.State members */
        Resource remote_resource = new ResourceImpl(rt, new FixedResourceIdFactory(id), rt.createResourcePropertiesForName(host));
        
        GenericServiceMockup s = new GenericServiceMockup(short_info, host);
        s.start();
        s.svcState = ss;
        if (remoteState != null) {
            remote_resource.setState(remoteState);
            s.addResource(remote_resource);
        }

        List<Class> eventsToReceive = whatEventsWillBeTriggered(cachedState, remoteState, attributesChanged);

        
        // The SCP test does not like the ResourceChangedEvent containing a single ResourceAnnotationChanged
        // instances. Such events has been introduced with the fix of issue 618. The IgnoreResourceAnnotationChangedEventFilter
        // does not accept such events.
        // This filter is a hack to avoid the reimplementation of many SCP junit tests
        GenericOrderWatchingResourceSEL sel = new GenericOrderWatchingResourceSEL(remote_resource, eventsToReceive, 
                IgnoreResourceAnnotationChangedEventFilter.getInstance());
        
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);
        
        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong)field.get(instance)).set(s.getCurrentEventId());
        field.setAccessible(false);
        
        try {
            instance.addServiceEventListener(sel);
            Field f;
            Resource cached_resource;
            if (cachedState != null) {
                ResourceStore res = instance.getResourceStore();
                /* prepare cached resource instance */
                cached_resource = remote_resource.clone();
                cached_resource.setState(cachedState);
//                cached_resource.setAmbiguous(cachedResourceIsAmbiguous);
                res.add(cached_resource);
            }
            /* now make remote resource 'changed' if needed */
            if (cachedResourceDiffers) {
                ResourcePropertyInsertOperation insert = new ResourcePropertyInsertOperation("test", "test");
                remote_resource.modify(insert);
            }
            /* now make remote resource 'static' if needed - it is safe now, as cached_resource is already cloned */
            remote_resource.setStatic(remoteStatic);

            /* execute refresh method in SCP */
            instance.fullRefresh();

            while(instance.hasActiveActions()) {
                Thread.sleep(10);
            }
            
            while(instance.svcEventForwarder.getNumberOfPendingEvents() > 0) {
                Thread.sleep(10);
            }
            
            
            /* wait for action processing ... */
            boolean ret = sel.hasAll(TIMEOUT);
            StringBuilder sb = new StringBuilder();
            for (Class c : sel.getAwaitEvents()) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                sb.append(c.getName().toString());
            }
            sb.insert(0, "Events that should have been sent by SCP: ");
            String should_been_sent = sb.toString();

            sb = new StringBuilder();
            for (Class c : sel.getReceivedEvents()) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                sb.append(c.getName().toString());
            }
            sb.insert(0, "Events that have been sent by SCP: ");
            String been_sent = sb.toString();

            /* check cached resource instance */
            ResourceStore res = instance.getResourceStore();
            try {
                cached_resource = res.getResource(id);
            } catch (UnknownResourceException ure) {
                cached_resource = null;
            }

            log.fine(should_been_sent);
            log.fine(been_sent);
            assertTrue(should_been_sent + "\n" + been_sent, ret);
            boolean order_is_ok = true;
            for (int i = 0; i < sel.getAwaitEvents().size(); i++) {
                order_is_ok = order_is_ok && sel.getAwaitEvents().get(i).equals(sel.getReceivedEvents().get(i));
            }
            assertTrue("Event were received in wrong order: \n " + should_been_sent + "\n" + been_sent, order_is_ok);

//            log.fine("Should the resource " +  id  + " be registered? " + willBeRegistered);
//            if (willBeRegistered) {
//                assertEquals("Resource should be registered", id, rm.getRegisteredId());
//            } else {
//                assertNull("Resource should not be registered", rm.getRegisteredId());
//            }

            if (refreshedState != null) {
                assertEquals("Expected different refreshed resource state", refreshedState, cached_resource.getState());
//                assertEquals("Expected different ambiguous state", willBeAmbiguous, cached_resource.isAmbiguous());
            } else {
                assertNull("Resource should not be cached", cached_resource);
            }
        } finally {
            instance.destroy();
        }  
    }

    /**
     * Helper method that drives tests for all variations or remote service state
     * that are supported by this test suite.
     * 
     * @param info                      description of a test
     * @param cachedState               original cached of a resource (if cached) or null if resource was not cached before
     * @param remoteState               cached of a resource owned by remote service
     * @param refreshedState            cached of a resource after refresh (result of event processing)
     * @param isUnique                  flag signaling whether the subject resource is unique
     * @param willBeAmbiguous           flag signaling whether the subject resource will become ambiguous
     * @param willBeRegistered          flag signaling whether the subject resource will be registered after refresh
     * @param attributesChanged         flag signaling whether the remote resource's attributes changed during test since resource was cached
     * @param cachedResourceDiffers     flag signaling whether the cached resource properties is different from remote resource properties
     * @param cachedResourceIsAmbiguous flag signaling whether the cached resource is ambiguous
     * @param remoteStatic          flag signaling whether the remote resource is static
     * @param id                        resource id for the resource
     * @throws java.lang.Exception      
     */
    protected void genericResourceRefreshTestVariations(final String short_info,
            final Resource.State cachedState,
            final Resource.State remoteState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final boolean attributesChanged,
            final boolean cachedResourceDiffers,
            final boolean cachedResourceIsAmbiguous,
            final boolean remoteStatic,
            final ResourceId id) throws Exception {
        // todo need to override
    }

    /**
     * Convenient way to trigger test for a refresh method. Cached resource is
     * not ambiguous, not different from remote resource.
     */
    protected void genericNonAmbiguousCachedResourceRefreshTest(final String short_info,
            final Resource.State cachedState,
            final Resource.State remoteState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final boolean attributesChanged,
            final ResourceId id) throws Exception {
        genericResourceRefreshTestVariations(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, false, false, false, id);
    }

    /**
     * Convenient way to trigger test for a refresh method. Cached resource is
     * not ambiguous, but different from remote resource (before the refresh).
     */
    protected void genericChangedCachedResourceRefreshTest(final String short_info,
            final Resource.State cachedState,
            final Resource.State remoteState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final ResourceId id) throws Exception {
        genericResourceRefreshTestVariations(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, true, true, false, false, id);
    }

    /**
     * Convenient way to trigger test for a refresh method. Cached resource is
     * ambiguous (before the refresh), but not different from remote resource (before the refresh).
     */
    protected void genericAmbiguousCachedResourceRefreshTest(final String short_info,
            final Resource.State cachedState,
            final Resource.State remoteState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final ResourceId id) throws Exception {
        genericResourceRefreshTestVariations(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, false, false, true, false, id);
    }

    /**
     * Convenient way to trigger test for a refresh method. Cached resource is
     * ambiguous (before the refresh) and different from remote resource (before the refresh).
     */
    protected void genericAmbiguousChangedCachedResourceRefreshTest(final String short_info,
            final Resource.State cachedState,
            final Resource.State remoteState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final ResourceId id) throws Exception {
        genericResourceRefreshTestVariations(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, true, true, true, false, id);
    }

    /**
     * Convenient way to trigger test for a refresh method. Cached resource is not
     * ambiguous (before the refresh), remote resource is static.
     */
    protected void genericNonAmbiguousCachedResourceRefreshRemoteStaticTest(final String short_info,
            final Resource.State cachedState,
            final Resource.State remoteState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final ResourceId id) throws Exception {
        boolean attributesChanged;
        if (refreshedState == null || cachedState == null) {
            attributesChanged = false;
        } else {
            attributesChanged = true;
        }
        genericResourceRefreshTestVariations(short_info, cachedState, remoteState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, false, false, true, id);
    }

    public void testPlaceHolder() {
        assertTrue("to make test pass", true);
    }
}
