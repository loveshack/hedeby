/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.RequestQueue;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdAndName;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.ServiceStoreException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.ui.resource.ResourceActionResult;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import junit.framework.TestCase;

/**
 *
 * test for ServiceCachingProxyManager. Right now we test that after service goes in 
 * STOPPED, UNKNOW or ERROR state requests and orders have to be pruned
 */
public class ServiceCachingProxyManagerTest extends TestCase {
    
    private ExecutionEnv env;
    private final String name = "service1";
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        env = DummyExecutionEnvFactory.newInstance();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testServiceStoppedStatePruneResourcesOrders() throws Exception {
        ServiceStore<ServiceCachingProxy> ss = new ServiceStoreMockup();
        ResourceManagerMockup rm = new ResourceManagerMockup(ss);

        
        ServiceMockup service = new ServiceMockup(name, 50);
        ServiceCachingProxy scp = new ServiceCachingProxy(env, service, rm, true);
        
        scp.awaitEndOfFullRefresh(10000);
        
        ServiceCachingProxyManager scpm = new ServiceCachingProxyManager(env, ss, rm);  
        startServiceCachingProxyManager(scpm);
        scpm.addService(scp);
        rm.addOrder(name, "order1");
        rm.addRequest(name, "request1");
        
        assertEquals("ERROR: There should be one request", 1, rm.getRequestCount());
        assertEquals("ERROR: There should be one order", 1, rm.getOrdersCount());
        
        service.setServiceState(ServiceState.STOPPED);
        Thread.sleep(2000);
        assertEquals("ERROR: request queue has to be empty", 0, rm.getRequestCount());
        assertEquals("ERROR: orders queue has to be empty", 0, rm.getOrdersCount());
    }
    
    public void testServiceErrorStatePruneResourcesOrders() throws Exception {
        ServiceStore<ServiceCachingProxy> ss = new ServiceStoreMockup();
        ResourceManagerMockup rm = new ResourceManagerMockup(ss);

        
        ServiceMockup service = new ServiceMockup(name, 50);
        ServiceCachingProxy scp = new ServiceCachingProxy(env, service, rm, true);
        
        scp.awaitEndOfFullRefresh(10000);
        
        ServiceCachingProxyManager scpm = new ServiceCachingProxyManager(env, ss, rm);  
        startServiceCachingProxyManager(scpm);
        scpm.addService(scp);
        rm.addOrder(name, "order1");
        rm.addRequest(name, "request1");
        
        assertEquals("ERROR: There should be one request", 1, rm.getRequestCount());
        assertEquals("ERROR: There should be one order", 1, rm.getOrdersCount());
        
        service.setServiceState(ServiceState.ERROR);
        Thread.sleep(2000);
        assertEquals("ERROR: request queue has to be empty", 0, rm.getRequestCount());
        assertEquals("ERROR: orders queue has to be empty", 0, rm.getOrdersCount());
    }
    
    public void testServiceUnknownStatePruneResourcesOrders() throws Exception {
        ServiceStore<ServiceCachingProxy> ss = new ServiceStoreMockup();
        ResourceManagerMockup rm = new ResourceManagerMockup(ss);

        
        ServiceMockup service = new ServiceMockup(name, 50);
        ServiceCachingProxy scp = new ServiceCachingProxy(env, service, rm, true);
        
        scp.awaitEndOfFullRefresh(10000);
        scp.setServiceState(ServiceState.RUNNING);
        ServiceCachingProxyManager scpm = new ServiceCachingProxyManager(env, ss, rm);
        startServiceCachingProxyManager(scpm);
        scpm.addService(scp);
        rm.addOrder(name, "order1");
        rm.addRequest(name, "request1");
        
        assertEquals("ERROR: There should be one request", 1, rm.getRequestCount());
        assertEquals("ERROR: There should be one order", 1, rm.getOrdersCount());
        

        service.setServiceState(ServiceState.UNKNOWN);
        Thread.sleep(2000);
        assertEquals("ERROR: request queue has to be empty", 0, rm.getRequestCount());
        assertEquals("ERROR: orders queue has to be empty", 0, rm.getOrdersCount());
        
    }
    
    private void startServiceCachingProxyManager(ServiceCachingProxyManager scpm) throws Exception {
        Field f = ServiceCachingProxyManager.class.getSuperclass().getDeclaredField("started");
        f.setAccessible(true);
        f.set(scpm, true);
        f.setAccessible(false);
    }
    
    private class ServiceStoreMockup implements ServiceStore<ServiceCachingProxy> {

        private final List<ServiceCachingProxy> services = new LinkedList<ServiceCachingProxy>();


        public Set<String> getServiceNames() {
            Set<String> set = new HashSet<String>();

            for (ServiceCachingProxy s : services) {
                set.add(s.getName());
            }

            return set;
        }

        public void clear() throws ServiceStoreException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public ServiceCachingProxy addService(ServiceCachingProxy service) throws ServiceStoreException {
            services.add(service);

            return service;
        }

        public ServiceCachingProxy removeService(String serviceName) throws UnknownServiceException, ServiceStoreException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServiceCachingProxy getService(String serviceName) throws UnknownServiceException, ServiceStoreException {
            for (ServiceCachingProxy s : services) {
                if (s.getName().equals(serviceName)) {
                    return s;
                }
            }
            throw new UnknownServiceException();
        }

        public List<ServiceCachingProxy> getServices() throws ServiceStoreException {
            return services;
        }
    }
    
    private class ServiceMockup implements Service {

        private final String name;
        private final int usage;
        private final boolean snae;
        private final List<Resource> list = new LinkedList<Resource>();
        private ServiceEventSupport ses;
        ServiceState state = ServiceState.RUNNING;

        public ServiceMockup(String name, int usage) {
            super();
            this.name = name;
            this.usage = usage;
            this.snae = false;
            ses = ServiceEventSupport.newInstance(name, Hostname.getLocalHost());
        }

        public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
            return new DefaultServiceSnapshot(ses.getSequenceNumberOfNextEvent(), ComponentState.STARTED, state, 
                    Collections.<Resource>emptySet(), 
                    Collections.<String,Collection<Need>>emptyMap());
        }

        
        public ServiceMockup(String name, int usage, boolean snae) {
            super();
            this.name = name;
            this.usage = usage;
            this.snae = snae;
            ses = ServiceEventSupport.newInstance(name, Hostname.getLocalHost());
        }
        
        public void setServiceState(ServiceState state) {
             switch (state) {
                case ERROR:
                    this.state = ServiceState.ERROR;
                    ses.fireServiceError();
                    break;
                case RUNNING:
                    this.state = ServiceState.RUNNING;
                    ses.fireServiceRunning();
                    break;
                case SHUTDOWN:
                    this.state = ServiceState.SHUTDOWN;
                    ses.fireServiceShutdown();
                    break;
                case STARTING:
                    this.state = ServiceState.STARTING;
                    ses.fireServiceStarting();
                    break;
                case STOPPED:
                    this.state = ServiceState.STOPPED;
                    ses.fireServiceStopped();
                    break;
                case UNKNOWN:
                    this.state = ServiceState.UNKNOWN;
                    ses.fireServiceUnknown();
                    break;
                default:
                    throw new IllegalStateException();
            }
        }

        public void startService() throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void stopService(boolean isFreeResources)
                throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public ServiceState getServiceState() {
            return state;
        }

        public void addServiceEventListener(ServiceEventListener eventListener) {
            ses.addServiceEventListener(eventListener);
        }

        public void removeServiceEventListener(
                ServiceEventListener eventListener) {
            ses.removeServiceEventListener(eventListener);
        }

        public List<Resource> getResources() throws ServiceNotActiveException {
            return list;
        }

        public Resource getResource(ResourceId resourceId)
                throws UnknownResourceException, ServiceNotActiveException {
            for (Resource r : list) {
                if (r.getId().equals(resourceId)) {
                    return r;
                }
            }

            throw new UnknownResourceException();
        }

        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr)
                throws UnknownResourceException, ServiceNotActiveException {
            if (snae) {
                throw new ServiceNotActiveException();
            }
            Iterator<Resource> iterator = list.iterator();
            boolean found = false;

            while (iterator.hasNext()) {
                Resource r = iterator.next();

                if (r.getId().equals(resourceId)) {
                    iterator.remove();
                    found = true;

                    break;
                }
            }

            if (!found) {
                throw new UnknownResourceException();
            }
        }

        public void resetResource(ResourceId resource)
                throws UnknownResourceException, ServiceNotActiveException {
            for (Resource r : list) {
                if (r.getId().equals(resource)) {
                    r.setState(Resource.State.ASSIGNED);
                }
            }

            throw new UnknownResourceException();
        }

        public void addResource(Resource resource)
                throws InvalidResourceException, ServiceNotActiveException {
            list.add(resource);
        }
                
        /**
         * This method is added because of an Interface change in service
         * @param resource to be added
         * @param sloName if the resource is added because of a SLO request it contains the SLO name
         * @throws com.sun.grid.grm.resource.InvalidResourceException
         * @throws com.sun.grid.grm.service.ServiceNotActiveException
         */
        public void addResource(Resource resource,String sloName)
                throws InvalidResourceException, ServiceNotActiveException {
            addResource(resource);
        }
        

        public void start() throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void stop(boolean isForced) throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void reload(boolean isForced) throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public ComponentState getState() {
            return ComponentState.STARTED;
        }

        public String getName() {
            return name;
        }

        public void modifyResource(ResourceId resource, ResourceChangeOperation opeartions) throws UnknownResourceException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<SLOState> getSLOStates() throws GrmRemoteException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResources(Filter<Resource> filter) throws ServiceNotActiveException, GrmRemoteException {
            List<Resource> ret = new ArrayList<Resource>(list.size());
            ResourceVariableResolver resolver = new ResourceVariableResolver();
            for (Resource res : list) {
                resolver.setResource(res);
                if (filter.matches(resolver)) {
                    ret.add(res);
                }
            }
            return ret;
        }

        public void addComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
            
        }

        public void removeComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Hostname getHostname() throws GrmRemoteException {
            return Hostname.getLocalHost();
        }

        public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws ServiceNotActiveException, UnknownResourceException, GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
    
    private class ResourceManagerMockup implements ResourceManager {

        private ServiceStore services;
        private final Map<String, Resource> inprocess = new HashMap<String, Resource>();
        private Map<String, String> orders = new HashMap<String, String>();
        private Map<String, String> requestqueue = new HashMap<String, String>();

        public ResourceManagerMockup(ServiceStore s) {
            services = s;
        }

        public void addRequest(String service, String req) {
            requestqueue.put(service, req);
        }
        
        public void addOrder(String service, String order) {
            orders.put(service, order);
        }
        
        public int getRequestCount() {
            return requestqueue.size();
        }
        
        public int getOrdersCount() {
            return orders.size();
        }
        
        public void addResource(Resource resource) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<ResourceActionResult> removeResources(List<String> resIdOrNameList, ResourceRemovalDescriptor descr) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean resetResource(ResourceId resource)
                throws UnknownResourceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean resetResource(ResourceId resource, String service)
                throws UnknownResourceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean modifyResource(ResourceId resource,
                ResourceChangeOperation opeartions) throws UnknownResourceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public Resource getResource(ResourceId id)
                throws UnknownResourceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResources(String service)
                throws UnknownServiceException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean moveResource(String service, ResourceId resourceId,
                boolean isForced, boolean isStatic)
                throws UnknownResourceException, UnknownServiceException,
                ServiceNotActiveException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResourcesInProcess() {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void addResourceProviderEventListener(
                ResourceProviderEventListener lis) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void removeResourceProviderEventListener(
                ResourceProviderEventListener lis) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceAddedEvent(String service, Resource added) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceChangedEvent(String service, Resource modified, Collection<ResourceChanged> changes) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceErrorEvent(String serviceName, Resource bad, String message) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceRemovedEvent(String serviceName, Resource released) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processAddResourceEvent(String serviceName, Resource resource) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processRemoveResourceEvent(String serviceName, Resource resource) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceRejectedEvent(String serviceName, Resource rejected) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceResetEvent(String serviceName, Resource reset) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processOrphanedResources() {

        }

        public boolean registerResourceID(ResourceId resourceId, String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean unregisterResourceID(ResourceId resourceId, String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean removeResource(ResourceId resourceId, String service, ResourceRemovalDescriptor descr) throws UnknownResourceException, UnknownServiceException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean addResourceToProcess(String source, Resource resource) {
            inprocess.put(source, resource);
            return true;
        }

        public boolean removeResourceFromProcess(String source, ResourceId resource) {
            Resource r = inprocess.get(source);
            if (r != null) {
                if (r.getId().equals(resource)) {
                    inprocess.remove(source);
                    return true;
                }
            }
            return false;
        }

        public int putResourceIDOnBlackList(ResourceId resourceId, String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public int removeResourceIDFromBlackList(ResourceId resourceId, String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean isResourceIDOnBlackList(ResourceId resourceId, String service) {
            return false;
        }

        public void start() {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void stop() {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceRequestEvent(ResourceRequestEvent event) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void setRequestQueue(RequestQueue rq) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void triggerRequestQueueReprocessing() {

        }

        public Resource getResource(String serviceName, ResourceId resourceId) throws UnknownServiceException, ServiceNotActiveException, UnknownResourceException, InvalidServiceException {
            try {
                Service s = services.getService(serviceName);
                return s.getResource(resourceId);
            } catch (Exception ex) {
                return null;
            }
        }

        public boolean addResource(String service, Resource resource) throws InvalidResourceException, UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean addResource(String service, Resource resource,String snloName) throws InvalidResourceException, UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }
        public List<ResourceIdAndName> getBlackList(String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResources(String service, Filter filter) throws UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResourcesInProcess(Filter filter) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        /**
         * The implementation is a shortcut to simulate the processing  of the processPurgeOrdersAndRequestsEvent in ResourceManagerImpl
         * @param source
         */
        public void processPurgeOrdersAndRequestsEvent(String source) {
            requestqueue.remove(source);
            orders.remove(source);
        }

        public List<ResourceActionResult> resetResources(List<String> idOrNameList) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ResourceActionResult modifyResource(ResourceId resId, Map<String, Object> properties) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Resource getResourceByIdOrName(String idOrName) throws ServiceNotActiveException, InvalidResourceException, UnknownResourceException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> moveResources(List<String> resIdOrName, String service, boolean isFored, boolean isStatic) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> putResourcesOnBlackList(List<String> idOrNameList, String service) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> removeResourcesFromBlackList(List<String> idOrNameList, String service) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> modifyResources(Map<String, Map<String, Object>> resIdOrNameMapWithProperties) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public DefaultService getDefaultService() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<Resource> getUnassignedResources(Filter<Resource> filter) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        
    }
}
