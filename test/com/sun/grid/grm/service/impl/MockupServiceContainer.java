/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentAdapterFactory;
import com.sun.grid.grm.ComponentNotActiveException;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.impl.ComponentExecutors;
import com.sun.grid.grm.service.impl.AbstractServiceStateTransitionTester.ServiceFeature;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 *  Implementation of the <code>ServiceContainer</code> for a <code>MockupService</code>. 
 * 
 *  It is used to test the behavior of <code>AbstractServiceContainer</code>.
 *  This <code>ServiceContainer</code> delegates the logic to a instance of
 *  <code>MockupServiceAdapterImpl</code>.
 */
public class MockupServiceContainer extends AbstractServiceContainer<MockupServiceAdapterImpl,ExecutorService,SparePoolServiceConfig>
     implements MockupService {

    private final List<ServiceFeature> pendingFeatures = new LinkedList<ServiceFeature>();
    
    public MockupServiceContainer(ExecutionEnv env, String name) {
        super(env, name, 
              ComponentExecutors.<MockupServiceContainer,SparePoolServiceConfig>newCachedThreadPoolExecutorServiceFactory(),
              new MyServiceAdapterFactory());
    }
    
    private static class MyServiceAdapterFactory implements ComponentAdapterFactory<MockupServiceContainer,MockupServiceAdapterImpl,ExecutorService,SparePoolServiceConfig> {

        public MockupServiceAdapterImpl createAdapter(MockupServiceContainer component, SparePoolServiceConfig config) throws GrmException {
            return new MockupServiceAdapterImpl(component);
        }

        public boolean hasImplementationChanged(MockupServiceContainer component, MockupServiceAdapterImpl adapter, SparePoolServiceConfig config) throws GrmException {
            return false;
        }
        
    }

    @Override
    protected void postCreateAdapter(MockupServiceAdapterImpl oldAdapter, MockupServiceAdapterImpl newAdapter) {
        super.postCreateAdapter(oldAdapter, newAdapter);
        for(ServiceFeature f: pendingFeatures) {
            try {
                newAdapter.setup(f);
            } catch (GrmException ex) {
                throw new IllegalStateException("Setup of feature " + f + " failed", ex);
            }
        }
    }

    
    /**
     * Override this method to prevent loading the config from CS
     * @return the string "config"
     */
    @Override
    protected SparePoolServiceConfig loadConfig() {
        return new SparePoolServiceConfig();
    }

    public void setup(ServiceFeature f) throws GrmException {
        try {
            getAdapter().setup(f);
        } catch (ComponentNotActiveException ex) {
            pendingFeatures.add(f);
        }
    }

    public void teardown(ServiceFeature f) {
        try {
            getAdapter().teardown(f);
        } catch (ComponentNotActiveException ex) {
            // Ignore
        }
    }

    public void waitUntil(ServiceFeature f) throws InterruptedException {
        try {
            getAdapter().waitUntil(f);
        } catch (ComponentNotActiveException ex) {
            // Ignore
        }
    }
    
}
