/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.event.AbstractServiceEvent;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

class GenericOrderWatchingResourceSEL extends ServiceEventAdapter {

    private final static Logger log = Logger.getLogger(GenericOrderWatchingResourceSEL.class.getName());
    private final Lock lock = new ReentrantLock();
    private final Condition condition = lock.newCondition();
    private List<Class> awaitEvents;
    private List<Class> receivedEvents;
    /* flag signaling that listener has just wait some time - if it
    does not need to wait for any other event */
    private Resource toWatch;

    private final EventFilter eventFilter;
    
    /**
     * Construct a new instance of GenericOrderWatchingResourceSEL which accepts
     * all events
     * 
     * @param toWatch   the watched resources
     * @param watchedEvents  the list of watched events
     */
    public GenericOrderWatchingResourceSEL(Resource toWatch, List<Class> watchedEvents) {
        this(toWatch, watchedEvents, NULL_EVENT_FILTER);
    }    
    
    /**
     * Construct a new instance of GenericOrderWatchingResourceSEL with an specific
     * event filter
     * 
     * @param toWatch   the watched resources
     * @param watchedEvents  the list of watched events
     * @param eventFilter  the event filter
     */
    public GenericOrderWatchingResourceSEL(Resource toWatch, List<Class> watchedEvents, EventFilter eventFilter) {
        super();
        this.toWatch = toWatch;
        this.awaitEvents = watchedEvents;
        this.receivedEvents = new ArrayList<Class>(watchedEvents.size());
        this.eventFilter = eventFilter;
    }
    

    private boolean acceptEvent(AbstractServiceEvent evt) {
        if (eventFilter.accept(evt)) {
             log.fine("Event " + evt + "accepted by filter");
            return true;
        }
        log.fine("Event " + evt + " not accepted by filter");
        return false;
    }
    private void addEvent(AbstractServiceEvent evt, Class clazz) {
        if (acceptEvent(evt)) {
            lock.lock();
            try {
                receivedEvents.add(clazz);
                log.log(Level.FINE, "Event {0} received", clazz);
                condition.signal();
            } finally {
                lock.unlock();
            }
        }
    }
            
    @Override
    public void addResource(AddResourceEvent evt) {
        if (evt.getResource().equals(toWatch)) {
            addEvent(evt, AddResourceEvent.class);
        }
    }

    @Override
    public void resourceAdded(ResourceAddedEvent evt) {
        if (evt.getResource().equals(toWatch)) {
            addEvent(evt, ResourceAddedEvent.class);
        }
    }

    @Override
    public void removeResource(RemoveResourceEvent evt) {
        if (evt.getResource().equals(toWatch)) {
            addEvent(evt, RemoveResourceEvent.class);
        }
    }

    @Override
    public void resourceRemoved(ResourceRemovedEvent evt) {
        if (evt.getResource().equals(toWatch)) {
            addEvent(evt, ResourceRemovedEvent.class);
        }
    }

    @Override
    public void resourceError(ResourceErrorEvent evt) {
        if (evt.getResource().equals(toWatch)) {
            addEvent(evt, ResourceErrorEvent.class);
        }
    }

    @Override
    public void resourceRejected(ResourceRejectedEvent evt) {
        if (evt.getResource().equals(toWatch)) {
            addEvent(evt, ResourceRejectedEvent.class);
        }
    }

    @Override
    public void resourceReset(ResourceResetEvent evt) {
        if (evt.getResource().equals(toWatch)) {
            addEvent(evt, ResourceResetEvent.class);
        }
    }

    @Override
    public void resourceChanged(ResourceChangedEvent evt) {
        if (evt.getResource().equals(toWatch)) {
            addEvent(evt, ResourceChangedEvent.class);
        }
    }

    public boolean hasAll(long timeOut) throws InterruptedException {
        long endTime = System.currentTimeMillis() + timeOut;
        lock.lock();
        try {
            while (awaitEvents.size() > receivedEvents.size()) {
                long rest = endTime - System.currentTimeMillis();
                if (rest > 0 ) {
                    condition.await(rest, TimeUnit.MILLISECONDS);
                } else {
                    break;
                }
            }
            return awaitEvents.size() == receivedEvents.size();
        } finally {
            lock.unlock();
        }
    }

    public List<Class> getAwaitEvents() {
        lock.lock();
        try {
            return new ArrayList<Class>(awaitEvents);
        } finally {
            lock.unlock();
        }
    }

    public List<Class> getReceivedEvents() {
        lock.lock();
        try {
            return new ArrayList<Class>(receivedEvents);
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * Generic filter for service events
     */
    public interface EventFilter {
        
        /**
         * Check whether <code>evt</code> is an accepted event
         * @param evt  the service event
         * @return <code>true</code> if <code>evt</code> is accepted
         *         otherwise <code>false</code>
         */
        public boolean accept(AbstractServiceEvent evt);
    }
    
    /**
     * This <code>EventFilter</code> accepts all events. 
     * It is the default <code>EventFilter</code>
     */
    public final static EventFilter NULL_EVENT_FILTER = new EventFilter() {
        public boolean accept(AbstractServiceEvent evt) {
            return true;
        }
    };
    
    
}
