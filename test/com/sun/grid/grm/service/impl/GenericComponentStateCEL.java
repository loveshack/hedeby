/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.event.ComponentEventAdapter;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Generic component event listener, that waits for event delivery within given timeout.
 * 
 */
class GenericComponentStateCEL extends ComponentEventAdapter {

    private final Logger log = Logger.getLogger(GenericComponentStateCEL.class.getName());
    private final Lock lock = new ReentrantLock();
    private final Condition cond = lock.newCondition();
    
    private final Set<ComponentState> expectedStates = new HashSet<ComponentState>();
    private final Set<ComponentState> receivedEvents = new HashSet<ComponentState>();
    
    GenericComponentStateCEL() {
        super();
    }
    
    private void expect(ComponentState state, boolean expect) {
        if(expect) {
            expectedStates.add(state);
        } else {
            expectedStates.remove(state);
        }
    }
    
    public GenericComponentStateCEL awaitStarting(boolean awaitStarting) {
        expect(ComponentState.STARTING, awaitStarting);
        return this;
    }
    
    public GenericComponentStateCEL awaitStarted(boolean awaitStarted) {
        expect(ComponentState.STARTED, awaitStarted);
        return this;
    }
    
    public GenericComponentStateCEL awaitReloading(boolean awaitReloading) {
        expect(ComponentState.RELOADING, awaitReloading);
        return this;
    }
    
    public GenericComponentStateCEL awaitUnknown(boolean awaitUnknown) {
        expect(ComponentState.UNKNOWN, awaitUnknown);
        return this;
    }
    
    public GenericComponentStateCEL awaitStopping(boolean awaitStopping) {
        expect(ComponentState.STOPPING, awaitStopping);
        return this;
    }
    
    public GenericComponentStateCEL awaitStopped(boolean awaitStopped) {
        expect(ComponentState.STOPPED, awaitStopped);
        return this;
    }
    
    public boolean hasAll(long timeOut) throws InterruptedException {
        long endTime = System.currentTimeMillis() + timeOut;
        while(true) {
            lock.lock();
            try {
                if (receivedEvents.containsAll(expectedStates)) {
                    return true;
                }
                long rest = endTime - System.currentTimeMillis();
                if (rest <= 0) {
                    return false;
                } else {
                    cond.await(rest, TimeUnit.MILLISECONDS);
                }
            } finally {
                lock.unlock();
            }
        }
    }

    private void addEvent(ComponentState state, ComponentStateChangedEvent evt) {
        log.log(Level.INFO, "Got event {0}", evt);
        lock.lock();
        try {
            receivedEvents.add(state);
            cond.signalAll();
        } finally {
            lock.unlock();
        }
    }
    
    @Override
    public void componentStarting(ComponentStateChangedEvent evt) {
        addEvent(ComponentState.STARTING, evt);
    }

    @Override
    public void componentStarted(ComponentStateChangedEvent evt) {
        addEvent(ComponentState.STARTED, evt);
    }

    @Override
    public void componentUnknown(ComponentStateChangedEvent evt) {
        addEvent(ComponentState.UNKNOWN, evt);
    }

    @Override
    public void componentStopping(ComponentStateChangedEvent evt) {
        addEvent(ComponentState.STOPPING, evt);
    }

    @Override
    public void componentReloading(ComponentStateChangedEvent evt) {
        addEvent(ComponentState.RELOADING, evt);
    }

    @Override
    public void componentStopped(ComponentStateChangedEvent evt) {
        addEvent(ComponentState.STOPPED, evt);
    }

    private boolean hasEvent(ComponentState state) {
        lock.lock();
        try {
            return receivedEvents.contains(state);
        } finally {
            lock.unlock();
        }
    }
    public boolean isGotStarting() {
        return hasEvent(ComponentState.STARTING);
    }

    public boolean isGotStarted() {
        return hasEvent(ComponentState.STARTED);
    }

    public boolean isGotReloading() {
        return hasEvent(ComponentState.RELOADING);
    }

    public boolean isGotUnknown() {
        return hasEvent(ComponentState.UNKNOWN);
    }

    public boolean isGotStopping() {
        return hasEvent(ComponentState.STOPPING);
    }

    public boolean isGotStopped() {
        return hasEvent(ComponentState.STOPPED);
    }
}
