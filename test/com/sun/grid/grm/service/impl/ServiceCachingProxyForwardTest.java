/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventAdapter;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentEventSupport;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.impl.ResourcePropertyInsertOperation;
import com.sun.grid.grm.resource.impl.ResourcePropertyUpdated;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.descriptor.ResourceRemovalFromSystemDescriptor;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import com.sun.grid.grm.service.impl.AbstractServiceStateTransitionTester.ServiceFeature;
import com.sun.grid.grm.service.impl.AbstractServiceStateTransitionTester.ServiceTester;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.EventListenerSupport;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.util.filter.Filter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Test for service caching proxy. 
 */
public class ServiceCachingProxyForwardTest extends TestCase {

    private static final Logger log = Logger.getLogger(ServiceCachingProxyForwardTest.class.getName());
    /* time for which should listeners wait for delivering events */
    private static final long TIMEOUT = 1000L;
    /* when we do not want to perform refresh action after SCP creation */
    private static final boolean INF = false;
    private ResourceType rt;
    private final Map<String, Object> new_rp = new HashMap<String, Object>();
    private final String host = "remote_host";
    private ExecutionEnv env;

    public ServiceCachingProxyForwardTest(String aTestName) {
        super(aTestName);
    }

    @Override
    protected void setUp() throws Exception {
        log.entering(getClass().getName(), "setUp");
        super.setUp();
        env = DummyExecutionEnvFactory.newInstance();
        rt = DefaultResourceFactory.getResourceType("test");
        new_rp.put("test", "test");
        /* cleanup spooled cache */
        Platform.getPlatform().removeDir(env.getLocalSpoolDir(), true);
        log.exiting(getClass().getName(), "setUp");
    }

    @Override
    protected void tearDown() throws Exception {
        log.entering(getClass().getName(), "tearDown");
        super.tearDown();
        /* cleanup spooled cache */
        Platform.getPlatform().removeDir(env.getLocalSpoolDir(), true);
        log.exiting(getClass().getName(), "tearDown");
    }

    private void awaitActionProcessed(ServiceCachingProxy scp) throws InterruptedException {
        while (scp.hasActiveActions()) {
            Thread.sleep(10);
        }
        log.fine("actions in SCP finished");

        while (scp.svcEventForwarder.getNumberOfPendingEvents() > 0) {
            Thread.sleep(10);
        }
        log.fine("Event support has delivered all events");

    }

    private boolean awaitActionProcessed(ServiceCachingProxy scp, GenericComponentStateCEL cel) throws InterruptedException {
        awaitActionProcessed(scp);
        return cel.hasAll(TIMEOUT);
    }

    private boolean awaitActionProcessed(ServiceCachingProxy scp, GenericServiceStateSEL sel) throws InterruptedException {
        awaitActionProcessed(scp);
        return sel.hasAll(TIMEOUT);
    }

    private boolean awaitActionProcessed(ServiceCachingProxy scp, GenericResourceSEL sel) throws InterruptedException {
        awaitActionProcessed(scp);
        return sel.hasAll(TIMEOUT);
    }

    /**
     * Test of destroy method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testDestroy() throws Exception {
        log.entering(getClass().getName(), "testDestroy");
        final String name = "destroy";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, INF);
        instance.destroy();
        Field f = ServiceCachingProxy.class.getDeclaredField("isDestroy");
        f.setAccessible(true);
        boolean res1 = f.getBoolean(instance);
        assertTrue("isDestroy should be true", res1);
        f.setAccessible(false);
        f = ServiceCachingProxy.class.getDeclaredField("actionExecutor");
        f.setAccessible(true);
        Object res2 = f.get(instance);
        assertNull("actionExecutor should be null", res2);
        f.setAccessible(false);
        assertEquals("serviceEventListener should be nullServiceEventListener", s.sel, s.nullServiceEventListener);
        assertEquals("componentEventListener should be nullComponentEventListener", s.cel, s.nullComponentEventListener);
        log.exiting(getClass().getName(), "testDestroy");
    }

    /**
     * Test of getState method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testGetState() throws Exception {
        log.entering(getClass().getName(), "testGetState");
        final String name = "getState";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericComponentStateCEL cel = new GenericComponentStateCEL().awaitStarted(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);
        try {
            instance.addComponentEventListener(cel);
            ComponentState expResult = s.getState();
            ComponentState result = instance.getState();
            assertEquals("Service component state and service proxy component state should be equal", expResult, result);
            s.setState(ComponentState.STARTED);
            /* wait for action processing ... */
            awaitActionProcessed(instance, cel);
            expResult = s.getState();
            result = instance.getState();
            assertEquals("Service component state and service proxy component state should be equal", expResult, result);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testGetState");
    }

    /**
     * Test of getState method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testGetStateEventFwd() throws Exception {
        log.entering(getClass().getName(), "testGetStateEventFwd");
        final String name = "getStateEventFwd";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericComponentStateCEL cel = new GenericComponentStateCEL().awaitStarted(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addComponentEventListener(cel);
            s.setState(ComponentState.STARTED);
            /* wait for action processing ... */
            awaitActionProcessed(instance, cel);
            assertTrue("Component listener should get event with STARTED state", cel.isGotStarted());
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testGetStateEventFwd");
    }

    /**
     * Test of getServiceState method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testGetServiceState() throws Exception {
        log.entering(getClass().getName(), "testGetServiceState");
        final String name = "getServiceState";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericServiceStateSEL sel = new GenericServiceStateSEL().awaitRunning(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);
            ServiceState expResult = s.getServiceState();
            ServiceState result = instance.getServiceState();
            assertEquals("Service service state should be UNKNOWN", ServiceState.UNKNOWN, result);
            assertEquals("Service proxy service state should be UNKNOWN", ServiceState.UNKNOWN, result);
            assertEquals("Service service state and service proxy service state should be equal", expResult, result);
            s.setServiceState(ServiceState.RUNNING);
            /* wait for action processing ... */
            awaitActionProcessed(instance, sel);
            expResult = s.getServiceState();
            result = instance.getServiceState();
            assertEquals("Service service state should be RUNNING", ServiceState.RUNNING, result);
            assertEquals("Service proxy service state should be RUNNING", ServiceState.RUNNING, result);
            assertEquals("Service service state and service proxy service state should be equal", expResult, result);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testGetServiceState");
    }

    /**
     * Test of getState method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testGetServiceStateEventFwd() throws Exception {
        log.entering(getClass().getName(), "testGetServiceStateEventFwd");
        final String name = "getServiceStateEventFwd";
        ServiceMockup s = new ServiceMockup(false, name, host);
        s.svcState = ServiceState.UNKNOWN;
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericServiceStateSEL sel = new GenericServiceStateSEL().awaitStarting(true).awaitRunning(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);
            s.setServiceState(ServiceState.RUNNING);
            /* wait for events ... */
            awaitActionProcessed(instance, sel);
            assertTrue("Service listener should get event with STARTING state", sel.isGotStarting());
            assertTrue("Service listener should get event with RUNNING state", sel.isGotRunning());
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testGetServiceStateEventFwd");
    }

    /**
     * Test of getSLOStates method, of class ServiceCachingProxy.
     */
    public void testGetSLOStates() {
        log.entering(getClass().getName(), "testGetServiceStateEventFwd");
        /* no test yet */
        log.exiting(getClass().getName(), "testGetServiceStateEventFwd");
    }

    /**
     * Test of startService method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testStartService() throws Exception {
        log.entering(getClass().getName(), "testStartService");
        final String name = "startService";
        ServiceMockup s = new ServiceMockup(false, name, host);
        s.setServiceState(ServiceState.STOPPED);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericServiceStateSEL sel = new GenericServiceStateSEL().awaitStarting(true).awaitRunning(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);
            instance.startService();
            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            ServiceState result = instance.getServiceState();
            assertEquals("Service service state should be RUNNING", ServiceState.RUNNING, result);
            assertTrue("Service listener should get events with RUNNING and STARTING state", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testStartService");
    }

    /**
     * Test of startService method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testStartServiceWrongEventOrder() throws Exception {
        log.entering(getClass().getName(), "testStartServiceWrongEventOrder");
        final String name = "startServiceWrongEventOrder";
        ServiceMockup s = new BadEventOrderServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericServiceStateSEL sel = new GenericServiceStateSEL().awaitStarting(true).awaitRunning(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);
            instance.startService();
            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            ServiceState result = instance.getServiceState();
            assertEquals("Service service state should be RUNNING", ServiceState.RUNNING, result);
            /* STARTING event has to be recreated */
            assertTrue("Service listener should get event with RUNNING state", sel.isGotRunning());
            assertTrue("Service listener should get event with STARTING state", sel.isGotStarting());
            assertTrue("Service listener should get both RUNNING and STARTING state events ", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testStartServiceWrongEventOrder");
    }

    /**
     * Test of stopService method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testStopService() throws Exception {
        log.entering(getClass().getName(), "testStopService");

        boolean isFreeResources = false;
        final String name = "stopService";
        ServiceMockup s = new ServiceMockup(false, name, host);
        s.setServiceState(ServiceState.RUNNING);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericServiceStateSEL sel = new GenericServiceStateSEL().awaitShutdown(true).awaitStopped(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);
        try {
            instance.addServiceEventListener(sel);
            instance.stopService(isFreeResources);
            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            ServiceState result = instance.getServiceState();
            assertEquals("Service service state should be STOPPED", ServiceState.STOPPED, result);
            assertTrue("Service listener should get events with STOPPED and SHUTDOWN state", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testStopService");
    }

    /**
     * Test of stopService method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testStopServiceWrongEventOrder() throws Exception {
        log.entering(getClass().getName(), "testStopServiceWrongEventOrder");
        boolean isFreeResources = false;
        final String name = "stopServiceWrongEventOrder";
        ServiceMockup s = new BadEventOrderServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericServiceStateSEL sel = new GenericServiceStateSEL().awaitShutdown(true).awaitStopped(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);
            instance.stopService(isFreeResources);
            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            ServiceState result = instance.getServiceState();
            assertEquals("Service service state should be STOPPED", ServiceState.STOPPED, result);
            assertTrue("Service listener should get event with STOPPED state", sel.isGotStopped());
            assertTrue("Service listener should get event with SHUTDOWN state", sel.isGotShutdown());
            assertTrue("Service listener should get both STOPPED and SHUTDOWN state events ", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testStopServiceWrongEventOrder");
    }

    /**
     * Test of addServiceEventListener method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testAddServiceEventListener() throws Exception {
        log.entering(getClass().getName(), "testAddServiceEventListener");
        ServiceEventListener serviceEventListener = new GenericServiceStateSEL();
        final String name = "addServiceEventListener";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            Field f = ServiceCachingProxy.class.getDeclaredField("svcEventForwarder");
            f.setAccessible(true);
            ServiceEventSupport res = (ServiceEventSupport) f.get(instance);
            f.setAccessible(false);
            f = ServiceEventSupport.class.getDeclaredField("listeners");
            f.setAccessible(true);
            EventListenerSupport els = (EventListenerSupport) f.get(res);
            f.setAccessible(false);
            assertFalse("ServiceEventSupport should not have any listener", els.hasListener());
            instance.addServiceEventListener(serviceEventListener);
            assertTrue("ServiceEventSupport should have listener", els.hasListener());
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testAddServiceEventListener");
    }

    /**
     * Test of removeServiceEventListener method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testRemoveServiceEventListener() throws Exception {
        log.entering(getClass().getName(), "testRemoveServiceEventListener");
        ServiceEventListener serviceEventListener = new GenericServiceStateSEL();
        final String name = "removeServiceEventListener";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            Field f = ServiceCachingProxy.class.getDeclaredField("svcEventForwarder");
            f.setAccessible(true);
            ServiceEventSupport res = (ServiceEventSupport) f.get(instance);
            f.setAccessible(false);
            res.addServiceEventListener(serviceEventListener);
            f = ServiceEventSupport.class.getDeclaredField("listeners");
            f.setAccessible(true);
            EventListenerSupport els = (EventListenerSupport) f.get(res);
            f.setAccessible(false);
            assertTrue("ServiceEventSupport should have listener", els.hasListener());
            instance.removeServiceEventListener(serviceEventListener);
            assertFalse("ServiceEventSupport should not have any listener", els.hasListener());
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testRemoveServiceEventListener");
    }

    /**
     * Test of getResources method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testGetResources() throws Exception {
        log.entering(getClass().getName(), "testGetResources");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        resource.setState(Resource.State.ASSIGNED);
        final String name = "getResources";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        s.resources.put(resource.getId(), resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        /* use AddResourceSEL - resource event are sent as last from refresh action */
        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitAdd(true).awaitAdded(true);
        /* give some time to register the listener */
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);

            instance.fullRefresh();

            /* let SCP to synchronize */
            awaitActionProcessed(instance, sel);
            assertEquals("List of resources returned by SCP should contain just 1 resource", instance.getResources().size(), 1);
            assertTrue("List of resources returned by SCP should contain the resource " + resource.getId(), instance.getResources().contains(resource));
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testGetResources");
    }

    /**
     * Test of getResource method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testGetResource() throws Exception {
        log.entering(getClass().getName(), "testGetResource");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ASSIGNED);
        final String name = "getResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        /* use AddResourceSEL - resource event are sent as last from refresh action */
        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitAdd(true).awaitAdded(true);
        /* give some time to register the listener */
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);
            instance.fullRefresh();
            /* let SCP to synchronize */
            awaitActionProcessed(instance, sel);
            assertEquals("Resource returned by SCP is not as expected", resource, instance.getResource(id));
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testGetResource");
    }

    /**
     * Test of removeResource method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testRemoveResource() throws Exception {
        log.entering(getClass().getName(), "testRemoveResource");

        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ASSIGNED);
        final String name = "removeResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitRemove(true).awaitRemoved(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.setState(ComponentState.STARTED);
            instance.setServiceState(ServiceState.RUNNING);
            instance.addServiceEventListener(sel);

            instance.getResourceStore().add(resource.clone());

            instance.removeResource(id, ResourceReassignmentDescriptor.INSTANCE);

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            try {
                instance.getResourceStore().getResource(id);
                fail("Resource should be removed");
            } catch (UnknownResourceException ure) {
                /* it's ok - resource is removed */
            }

            assertTrue("Listener should receive both RemoveResourceEvent and ResourceRemovedEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testRemoveResource");
    }

    /**
     * Test of removeResource method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testRemoveResourceWrongEventOrder() throws Exception {
        log.entering(getClass().getName(), "testRemoveResourceWrongEventOrder");

        log.info("removeResourceWrongEventOrder");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ASSIGNED);
        final String name = "removeResourceWrongEventOrder";
        ServiceMockup s = new BadEventOrderServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitRemove(true).awaitRemoved(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.setState(ComponentState.STARTED);
            instance.setServiceState(ServiceState.RUNNING);
            instance.addServiceEventListener(sel);

            ResourceStore res = instance.getResourceStore();
            res.add(resource.clone());
            instance.removeResource(id, ResourceReassignmentDescriptor.INSTANCE);

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            try {
                res.getResource(id);
                fail("Resource should be removed");
            } catch (UnknownResourceException ure) {
                /* it's ok - resource is removed */
            }

            assertTrue("Listener should receive both RemoveResourceEvent and ResourceRemovedEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testRemoveResourceWrongEventOrder");
    }

    /**
     * Test of full refresh action, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testRefreshRemoveResource() throws Exception {
        log.entering(getClass().getName(), "testRefreshRemoveResource");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ASSIGNED);
        final String name = "refreshRemoveResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitRemove(true).awaitRemoved(true);
        /* the refresh action will be triggered manualy */
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);
            ResourceStore res = instance.getResourceStore();
            res.add(resource.clone());
            try {
                res.getResource(id);
            } catch (UnknownResourceException ure) {
                fail("SCP should have the resource");
            }
            instance.fullRefresh();

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            try {
                res.getResource(id);
                fail("Resource should be removed");
            } catch (UnknownResourceException ure) {
                /* it's ok - resource is removed */
            }

            assertTrue("Listener should receive both RemoveResourceEvent and ResourceRemovedEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testRefreshRemoveResource");
    }

    /**
     * Test of Issue 327:
     * 
     * GEAdapter must not send property changed events for removed resources
     * 
     * This test is testing whether the SCP is immune to behavior described in
     * i327.
     * 
     * @throws java.lang.Exception 
     */
    public void testIssue327() throws Exception {
        log.entering(getClass().getName(), "testIssue372");

        log.info("issue327");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ASSIGNED);
        final String name = "issue327";
        ServiceMockup s = new Issue327ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitRemove(true).awaitRemoved(true).awaitResourceChanged(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.setState(ComponentState.STARTED);
            instance.setServiceState(ServiceState.RUNNING);
            instance.addServiceEventListener(sel);

            ResourceStore res = instance.getResourceStore();
            res.add(resource.clone());
            instance.removeResource(id, ResourceReassignmentDescriptor.INSTANCE);


            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            try {
                res.getResource(id);
                fail("Resource should be removed");
            } catch (UnknownResourceException ure) {
                /* it's ok - resource is removed */
            }

            // Issue327ServiceMockup does not send the RemoveResourceEvent, but SCP must
            // generate this event => the selector will receive it
            assertTrue("Listener should receive the RemoveResourceEvent", sel.isGotRemove());
            assertTrue("Listener should receive the ResourceRemovedEvent", sel.isGotRemoved());
            assertFalse("Listener should not receive the ResourcePropertiesChangedEvent", sel.isGotResourceChanged());
            assertFalse("Listener should not receive all the events - RemoveResourceEvent, ResourceRemovedEvent and ResourcePropertiesChangedEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testIssue372");
    }

    /**
     * Test of resetResource method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testResetResource() throws Exception {
        log.entering(getClass().getName(), "testResetResource");

        log.info("resetResource");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ERROR);
        final String name = "resetResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitReset(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.setState(ComponentState.STARTED);
            instance.setServiceState(ServiceState.RUNNING);
            instance.addServiceEventListener(sel);
            ResourceStore res = instance.getResourceStore();
            Resource clone = resource.clone();
            clone.setState(Resource.State.ERROR);
            res.add(clone);
            instance.resetResource(id);

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);

            assertTrue("Listener should receive the ResourceResetEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testResetResource");
    }

    /**
     * Test of full refresh action, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testRefreshResetResource() throws Exception {
        log.entering(getClass().getName(), "testRefreshResetResource");
        log.info("refreshResetResource");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        final String name = "refreshResetResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        resource.setState(Resource.State.ASSIGNED);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitReset(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);
            ResourceStore res = instance.getResourceStore();
            Resource clone = resource.clone();
            clone.setState(Resource.State.ERROR);
            res.add(clone);
            try {
                Resource got = res.getResource(id);
                assertEquals("cached resource should be in ERROR state", Resource.State.ERROR, got.getState());
            } catch (UnknownResourceException ure) {
                fail("SCP should have the resource");
            }
            instance.fullRefresh();

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            try {
                Resource got = res.getResource(id);
                assertEquals("cached resource should be in ASSIGNED state", Resource.State.ASSIGNED, got.getState());
            } catch (UnknownResourceException ure) {
                fail("SCP should have the resource");
            }

            assertTrue("Listener should receive the ResourceResetEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testRefreshResetResource");
    }

    /**
     * Test of modifyResource method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testModifyResource() throws Exception {
        log.entering(getClass().getName(), "testModifyResource");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ASSIGNED);
        final String name = "modifyResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitResourceChanged(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, INF);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.setState(ComponentState.STARTED);
            instance.setServiceState(ServiceState.RUNNING);
            instance.addServiceEventListener(sel);
            ResourceStore res = instance.getResourceStore();
            res.add(resource.clone());
            /* it's not important to specify some properties - we just want to 
             * test event relying */
            ResourcePropertyInsertOperation insert = new ResourcePropertyInsertOperation("test", "test");
            instance.modifyResource(id, insert);

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);

            assertTrue("Listener should receive the ResourcePropertiesChangedEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testModifyResource");

    }

    /**
     * Test of full refresh action, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testRefreshModifyResource() throws Exception {
        log.entering(getClass().getName(), "testRefreshModifyResource");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ASSIGNED);
        final String name = "refreshModifyResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        resource.setState(Resource.State.ASSIGNED);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitResourceChanged(true);
        /* refresh action will be triggered manually */
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);
        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);
            ResourceStore res = instance.getResourceStore();
            /* bit of messy code :( */
            Resource clone = resource.clone();
            ResourcePropertyInsertOperation insert = new ResourcePropertyInsertOperation("test", "test");
            clone.modify(insert);
            res.add(clone);
            try {
                Resource got = res.getResource(id);
                assertNotNull("SCP should have the resource", got);
            } catch (UnknownResourceException ure) {
                fail("SCP should have the resource");
            }
            instance.fullRefresh();
            /* end of messy code :( */

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            //boolean ret = sel.hasAll(TIMEOUT + 10 * SHORT);
            try {
                Resource got = res.getResource(id);
                assertNotNull("SCP should have the resource", got);
            } catch (UnknownResourceException ure) {
                fail("SCP should have the resource");
            }

            assertTrue("Listener should receive the ResourcePropertiesChangedEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testRefreshModifyResource");
    }

    /**
     * Test of addResource method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testAddResource() throws Exception {
        log.entering(getClass().getName(), "testAddResource");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        final String name = "addResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitAdd(true).awaitAdded(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.setState(ComponentState.STARTED);
            instance.setServiceState(ServiceState.RUNNING);
            instance.addServiceEventListener(sel);
            instance.addResource(resource.clone());

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            ResourceStore res = instance.getResourceStore();
            Resource result = res.getResource(id);

            assertEquals("Resources should be equal", result, resource);
            assertTrue("Listener should receive both AddResourceEvent and ResourceAddedEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testAddResource");
    }

    /**
     * Test of addResource method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testAddResourceWrongEventOrder() throws Exception {
        log.entering(getClass().getName(), "testAddResourceWrongEventOrder");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        final String name = "addResourceWrongEventOrder";
        ServiceMockup s = new BadEventOrderServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitAdd(true).awaitAdded(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.setState(ComponentState.STARTED);
            instance.setServiceState(ServiceState.RUNNING);
            instance.addServiceEventListener(sel);
            instance.addResource(resource.clone());

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            ResourceStore res = instance.getResourceStore();
            Resource result = res.getResource(id);

            assertEquals("Resources should be equal", result, resource);
            assertTrue("Listener should receive both AddResourceEvent and ResourceAddedEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testAddResourceWrongEventOrder");
    }

    /**
     * Test of full refresh action, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testRefreshAddResource() throws Exception {
        log.entering(getClass().getName(), "testRefreshAddResource");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ASSIGNED);
        final String name = "refreshAddResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitAdd(true).awaitAdded(true);
        /* refresh action will be triggered manually */
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addServiceEventListener(sel);

            instance.fullRefresh();

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);
            ResourceStore res = instance.getResourceStore();
            Resource result = res.getResource(id);

            assertEquals("Resources should be equal", result, resource);
            assertTrue("Listener should receive both AddResourceEvent and ResourceAddedEvent", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testRefreshAddResource");

    }

    /**
     * Test of start method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testStart() throws Exception {
        log.entering(getClass().getName(), "testStart");
        final String name = "start";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericComponentStateCEL cel = new GenericComponentStateCEL().awaitStarting(true).awaitStarted(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addComponentEventListener(cel);
            instance.start();
            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, cel);
            ComponentState result = instance.getState();
            assertEquals("Service component state should be STARTED", ComponentState.STARTED, result);
            assertTrue("Component listener should get events with STARTING and STARTED state", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testStart");
    }

    /**
     * Test of stop method, ofro class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testStop() throws Exception {
        log.entering(getClass().getName(), "testStop");
        boolean isForced = false;
        final String name = "stop";
        ServiceMockup s = new ServiceMockup(false, name, host);
        s.setState(ComponentState.STARTED);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericComponentStateCEL cel = new GenericComponentStateCEL().awaitStopping(true).awaitStopped(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addComponentEventListener(cel);
            instance.stop(isForced);
            /* wait for action processing ... */
            boolean ret = cel.hasAll(TIMEOUT);
            ComponentState result = instance.getState();
            assertEquals("Service component state should be STOPPED", ComponentState.STOPPED, result);
            assertTrue("Component listener should get both events with STOPPED and STOPPING state", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testStop");
    }

    /**
     * Test of reload method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testReloadForced() throws Exception {
        log.entering(getClass().getName(), "testReloadForced");
        boolean isForced = true;
        final String name = "reloadForced";
        ServiceMockup s = new ServiceMockup(false, name, host);
        s.setState(ComponentState.STARTED);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericComponentStateCEL cel = new GenericComponentStateCEL().awaitStarting(true).awaitStarted(true).awaitStopping(true).awaitStopped(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addComponentEventListener(cel);
            instance.reload(isForced);
            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, cel);
            ComponentState result = instance.getState();
            assertEquals("Service component state should be STARTED", ComponentState.STARTED, result);
            assertTrue("Component listener should get all these events - STOPPING, STOPPED, STARTING, STARTED", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testReloadForced");
    }

    /**
     * Test of reload method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testReloadForcedWrongEventOrder() throws Exception {
        log.entering(getClass().getName(), "testReloadForcedWrongEventOrder");
        boolean isForced = true;
        final String name = "reloadForcedWrongEventOrder";
        ServiceMockup s = new BadEventOrderServiceMockup(false, name, host);
        s.setState(ComponentState.STARTED);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericComponentStateCEL cel = new GenericComponentStateCEL().awaitStarted(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addComponentEventListener(cel);
            instance.reload(isForced);
            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, cel);
            ComponentState result = instance.getState();
            assertEquals("Service component state should be STARTED", ComponentState.STARTED, result);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testReloadForcedWrongEventOrder");
    }

    /**
     * Test of reload method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testReloadNotForced() throws Exception {
        log.entering(getClass().getName(), "testReloadNotForced");
        boolean isForced = false;
        final String name = "reloadNotForced";
        ServiceMockup s = new ServiceMockup(false, name, host);
        s.setState(ComponentState.STARTED);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericComponentStateCEL cel = new GenericComponentStateCEL().awaitReloading(true).awaitStarted(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addComponentEventListener(cel);
            instance.reload(isForced);
            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, cel);
            ComponentState result = instance.getState();
            assertEquals("Service component state should be STARTED", ComponentState.STARTED, result);
            assertTrue("Component listener should get all these events - RELOADING, STARTED", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testReloadNotForced");
    }

    /**
     * Test of reload method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testReloadNotForcedWrongEventOrder() throws Exception {
        log.entering(getClass().getName(), "testReloadNotForcedWrongEventOrder");
        boolean isForced = false;
        final String name = "reloadNotForcedWrongEventOrder";
        ServiceMockup s = new BadEventOrderServiceMockup(false, name, host);
        s.setState(ComponentState.STARTED);
        ResourceManager rm = new ResourceManagerMockup(true);
        GenericComponentStateCEL cel = new GenericComponentStateCEL().awaitReloading(true).awaitStarted(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            instance.addComponentEventListener(cel);
            instance.reload(isForced);
            /* wait for action processing ... */
            instance.reload(isForced);
            boolean ret = awaitActionProcessed(instance, cel);
            ComponentState result = instance.getState();
            assertEquals("Service component state should be STARTED", ComponentState.STARTED, result);
            assertFalse("Component listener should not get event with RELOADING state", cel.isGotReloading());
            assertTrue("Component listener should get event with STARTED state", cel.isGotStarted());
            assertFalse("Component listener should not get all these events - RELOADING, STARTED", ret);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testReloadNotForcedWrongEventOrder");
    }

    /**
     * Test of addComponentEventListener method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testAddComponentEventListener() throws Exception {
        log.entering(getClass().getName(), "testAddComponentEventListener");
        ComponentEventListener componentEventListener = new GenericComponentStateCEL();
        final String name = "addComponentEventListener";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            Field f = ServiceCachingProxy.class.getDeclaredField("svcEventForwarder");
            f.setAccessible(true);
            ComponentEventSupport res = (ComponentEventSupport) f.get(instance);
            f.setAccessible(false);
            f = ComponentEventSupport.class.getDeclaredField("listeners");
            f.setAccessible(true);
            EventListenerSupport els = (EventListenerSupport) f.get(res);
            f.setAccessible(false);
            assertFalse("ComponentEventSupport should not have any listener", els.hasListener());
            instance.addComponentEventListener(componentEventListener);
            assertTrue("ComponentEventSupport should have listener", els.hasListener());
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testAddComponentEventListener");
    }

    /**
     * Test of removeComponentEventListener method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testRemoveComponentEventListener() throws Exception {
        log.entering(getClass().getName(), "testRemoveComponentEventListener");
        ComponentEventListener componentEventListener = new GenericComponentStateCEL();
        final String name = "removeComponentEventListener";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);

        // Simulate the we are in sync with the service
        Field field = ServiceCachingProxy.class.getDeclaredField("nextExpectedEventSequenceNumber");
        field.setAccessible(true);
        ((AtomicLong) field.get(instance)).set(s.eventId.get());
        field.setAccessible(false);

        try {
            Field f = ServiceCachingProxy.class.getDeclaredField("svcEventForwarder");
            f.setAccessible(true);
            ComponentEventSupport res = (ComponentEventSupport) f.get(instance);
            f.setAccessible(false);
            res.addComponentEventListener(componentEventListener);
            f = ComponentEventSupport.class.getDeclaredField("listeners");
            f.setAccessible(true);
            EventListenerSupport els = (EventListenerSupport) f.get(res);
            f.setAccessible(false);
            assertTrue("ComponentEventSupport should have listener", els.hasListener());
            instance.removeComponentEventListener(componentEventListener);
            assertFalse("ComponentEventSupport should not have any listener", els.hasListener());
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testRemoveComponentEventListener");
    }

    /**
     * Test of getName method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testGetName() throws Exception {
        log.entering(getClass().getName(), "testGetName");
        final String name = "getName";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);
        try {
            String expResult = s.getName();
            String result = instance.getName();
            assertEquals("Service's and SCP's names should be equal", expResult, result);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testGetName");
    }

    /**
     * Test of getHostname method, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testGetHostname() throws Exception {
        log.entering(getClass().getName(), "testGetHostname");
        final String name = "getHostname";
        ServiceMockup s = new ServiceMockup(false, name, host);
        ResourceManager rm = new ResourceManagerMockup(true);
        ServiceCachingProxy instance = new ServiceCachingProxy(env, s, rm, false);
        try {
            Hostname expResult = s.getHostname();
            Hostname result = instance.getHostname();
            assertEquals("Service's and SCP's hostnames should be equal", expResult, result);
        } finally {
            instance.destroy();
        }
        log.exiting(getClass().getName(), "testGetHostname");
    }

    /**
     * Test of toString method, of class ServiceCachingProxy.
     */
    public void testToString() {
        log.entering(getClass().getName(), "testToString");
        log.info("toString");
        /* no test yet */
        log.exiting(getClass().getName(), "testToString");
    }

    private class ServiceMockup implements Service {

        protected final Map<ResourceId, Resource> resources;
        protected final boolean throwremote;
        protected final Hostname hostname;
        protected final String name;
        /* mockup needs just one listener */
        protected ServiceEventListener sel;
        protected final ServiceEventListener nullServiceEventListener = new ServiceEventAdapter();
        protected ComponentEventListener cel;
        protected final ComponentEventListener nullComponentEventListener = new ComponentEventAdapter();
        protected volatile ComponentState cmpState;
        protected volatile ServiceState svcState;
        protected AtomicLong eventId = new AtomicLong(0);

        /**
         * Creates mockup instance of service.
         * 
         * @param throwremote if true, each method will throw GrmRemoteException
         * @param name service name
         * @param host host on which mockup "is" running
         */
        public ServiceMockup(boolean throwremote, String name, String host) {
            this.resources = new HashMap<ResourceId, Resource>();
            this.throwremote = throwremote;
            this.hostname = Hostname.getInstance(host);
            this.name = name;
            this.cmpState = ComponentState.UNKNOWN;
            this.svcState = ServiceState.UNKNOWN;
            this.sel = nullServiceEventListener;
            this.cel = nullComponentEventListener;
        }

        public void startService() throws GrmException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            setServiceState(ServiceState.STARTING);
            setServiceState(ServiceState.RUNNING);
        }

        public void stopService(boolean isFreeResources) throws GrmException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            setServiceState(ServiceState.SHUTDOWN);
            setServiceState(ServiceState.STOPPED);
        }

        /* event ID has to be set manually before calling this!!! -
         * it is needed for junit tests */
        public void setServiceState(ServiceState state) {
            this.svcState = state;
            ServiceStateChangedEvent ssce = new ServiceStateChangedEvent(eventId.getAndIncrement(), name, svcState);
            switch (svcState) {
                case ERROR:
                    sel.serviceError(ssce);
                    break;
                case RUNNING:
                    sel.serviceRunning(ssce);
                    break;
                case SHUTDOWN:
                    sel.serviceShutdown(ssce);
                    break;
                case STARTING:
                    sel.serviceStarting(ssce);
                    break;
                case STOPPED:
                    sel.serviceStopped(ssce);
                    break;
                case UNKNOWN:
                    sel.serviceUnknown(ssce);
                    break;
                default:
                    throw new IllegalStateException();
            }
        }

        public ServiceState getServiceState() throws GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            return svcState;
        }

        public synchronized List<SLOState> getSLOStates() throws GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void addServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            sel = eventListener;
        }

        public void removeServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            sel = nullServiceEventListener;
        }

        public List<Resource> getResources(Filter filter) throws ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            return new LinkedList<Resource>(resources.values());
        }

        public List<Resource> getResources() throws ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            return new LinkedList<Resource>(resources.values());
        }

        public Resource getResource(ResourceId resourceId) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            Resource r = resources.get(resourceId);
            if (r == null) {
                throw new UnknownResourceException();
            }
            return r;
        }

        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            Resource r = resources.get(resourceId);
            if (r == null) {
                throw new UnknownResourceException();
            }
            RemoveResourceEvent rre = new RemoveResourceEvent(eventId.getAndIncrement(), name, r, "remove");
            sel.removeResource(rre);
            resources.remove(resourceId);
            ResourceRemovedEvent rre1 = new ResourceRemovedEvent(eventId.getAndIncrement(), name, r, "removed");
            sel.resourceRemoved(rre1);
        }

        public void resetResource(ResourceId resource) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            Resource r = resources.get(resource);
            if (r == null) {
                throw new UnknownResourceException();
            }
            r.setState(Resource.State.ASSIGNED);
            ResourceResetEvent rre = new ResourceResetEvent(eventId.getAndIncrement(), name, r, "reset");
            resources.put(r.getId(), r);
            sel.resourceReset(rre);
        }

        /**
         * Part of the interface definition
         * @param resource to be added
         * @param sloName will be dismissed if provided
         * @throws com.sun.grid.grm.resource.InvalidResourceException
         * @throws com.sun.grid.grm.service.ServiceNotActiveException
         * @throws com.sun.grid.grm.GrmRemoteException
         */
        public void addResource(Resource resource, String sloName) throws InvalidResourceException, ServiceNotActiveException, GrmRemoteException {
            addResource(resource);
        }

        public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void addResource(Resource resource) throws InvalidResourceException, ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            resource.setState(Resource.State.ASSIGNING);
            AddResourceEvent are = new AddResourceEvent(eventId.getAndIncrement(), name, resource, "add");
            sel.addResource(are);
            resources.put(resource.getId(), resource);
            resource.setState(Resource.State.ASSIGNED);
            ResourceAddedEvent rae = new ResourceAddedEvent(eventId.getAndIncrement(), name, resource, "added");
            sel.resourceAdded(rae);
        }

        public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            Resource r = resources.get(resource);
            if (r == null) {
                throw new UnknownResourceException();
            }
            Collection<ResourceChanged> changed = r.modify(operations);
            ResourceChangedEvent rcpe = new ResourceChangedEvent(eventId.getAndIncrement(), name, r, changed, "modify");
            resources.put(r.getId(), r);
            sel.resourceChanged(rcpe);
        }

        public void modifyResource(ResourceId resource, ResourceChangeOperation operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
            modifyResource(resource,Collections.singleton(operations));
        }

        public void start() throws GrmException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            setState(ComponentState.STARTING);
            setState(ComponentState.STARTED);
        }

        public void stop(boolean isForced) throws GrmException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            setState(ComponentState.STOPPING);
            setState(ComponentState.STOPPED);
        }

        public void reload(boolean isForced) throws GrmException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            if (isForced) {
                stop(false);
                start();
            } else {
                setState(ComponentState.RELOADING);
                setState(ComponentState.STARTED);
            }

        }

        /* event ID has to be set manually before calling this!!! -
         * it is needed for junit tests */
        public void setState(ComponentState state) {
            this.cmpState = state;
            ComponentStateChangedEvent csce = new ComponentStateChangedEvent(eventId.getAndIncrement(), name, hostname, cmpState);
            switch (cmpState) {
                case RELOADING:
                    cel.componentReloading(csce);
                    break;
                case STARTED:
                    cel.componentStarted(csce);
                    break;
                case STARTING:
                    cel.componentStarting(csce);
                    break;
                case STOPPED:
                    cel.componentStopped(csce);
                    break;
                case STOPPING:
                    cel.componentStopping(csce);
                    break;
                case UNKNOWN:
                    cel.componentUnknown(csce);
                    break;
                default:
                    throw new IllegalStateException();
            }
        }

        public ComponentState getState() throws GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            return cmpState;
        }

        public void addComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            cel = componentEventListener;
        }

        public void removeComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            cel = nullComponentEventListener;
        }

        public String getName() throws GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            return name;
        }

        public Hostname getHostname() throws GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            return hostname;
        }

        public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws ServiceNotActiveException, UnknownResourceException, GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
            return new DefaultServiceSnapshot(eventId.get(), cmpState, svcState,
                    new HashSet<Resource>(resources.values()),
                    Collections.<String, Collection<Need>>emptyMap());
        }
    }

    private class BadEventOrderServiceMockup extends ServiceMockup {

        public BadEventOrderServiceMockup(boolean throwremote, String name, String host) {
            super(throwremote, name, host);
        }

        @Override
        public void startService() throws GrmException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            svcState = ServiceState.STARTING;
            ServiceStateChangedEvent ssce1 = new ServiceStateChangedEvent(eventId.get(), name, svcState);
            svcState = ServiceState.RUNNING;
            ServiceStateChangedEvent ssce2 = new ServiceStateChangedEvent(eventId.getAndIncrement(), name, svcState);
            sel.serviceRunning(ssce2);
            sel.serviceStarting(ssce1);
        }

        @Override
        public void stopService(boolean isFreeResources) throws GrmException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            svcState = ServiceState.SHUTDOWN;
            ServiceStateChangedEvent ssce1 = new ServiceStateChangedEvent(eventId.getAndIncrement(), name, svcState);
            svcState = ServiceState.STOPPED;
            ServiceStateChangedEvent ssce2 = new ServiceStateChangedEvent(eventId.getAndIncrement(), name, svcState);
            sel.serviceStopped(ssce2);
            sel.serviceShutdown(ssce1);
        }

        @Override
        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            Resource r = resources.get(resourceId);
            if (r == null) {
                throw new UnknownResourceException();
            }
            resources.remove(resourceId);
            RemoveResourceEvent rre = new RemoveResourceEvent(eventId.get(), name, r, "remove");
            ResourceRemovedEvent rre1 = new ResourceRemovedEvent(eventId.getAndIncrement(), name, r, "removed");
            sel.resourceRemoved(rre1);
            sel.removeResource(rre);
        }

        @Override
        public void resetResource(ResourceId resource) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            Resource r = resources.get(resource);
            if (r == null) {
                throw new UnknownResourceException();
            }
            r.setState(Resource.State.ASSIGNED);
            ResourceResetEvent rre = new ResourceResetEvent(eventId.getAndIncrement(), name, r, "reset");
            resources.put(r.getId(), r);
            sel.resourceReset(rre);
        }

        @Override
        public void addResource(Resource resource) throws InvalidResourceException, ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            ResourceAddedEvent rae = new ResourceAddedEvent(eventId.getAndIncrement(), name, resource, "added");
            AddResourceEvent are = new AddResourceEvent(eventId.getAndIncrement(), name, resource, "add");
            resources.put(resource.getId(), resource);
            sel.resourceAdded(rae);
            sel.addResource(are);
        }

        @Override
        public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            Resource r = resources.get(resource);
            if (r == null) {
                throw new UnknownResourceException();
            }
            Collection<ResourceChanged> changed = r.modify(operations);
            ResourceChangedEvent rcpe = new ResourceChangedEvent(eventId.getAndIncrement(), name, r, changed, "modify");
            resources.put(r.getId(), r);
            sel.resourceChanged(rcpe);
        }
        @Override
        public void modifyResource(ResourceId resource, ResourceChangeOperation operation) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
            modifyResource(resource, Collections.singleton(operation));
        }

        @Override
        public void start() throws GrmException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            cmpState = ComponentState.STARTING;
            ComponentStateChangedEvent csce1 = new ComponentStateChangedEvent(eventId.get(), name, hostname, cmpState);
            cmpState = ComponentState.STARTED;
            ComponentStateChangedEvent csce2 = new ComponentStateChangedEvent(eventId.getAndIncrement(), name, hostname, cmpState);
            cel.componentStarted(csce2);
            cel.componentStarting(csce1);
        }

        @Override
        public void stop(boolean isForced) throws GrmException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            cmpState = ComponentState.STOPPED;
            ComponentStateChangedEvent csce2 = new ComponentStateChangedEvent(eventId.getAndIncrement(), name, hostname, cmpState);
            cmpState = ComponentState.STOPPING;
            ComponentStateChangedEvent csce1 = new ComponentStateChangedEvent(eventId.getAndIncrement(), name, hostname, cmpState);
            cel.componentStopped(csce2);
            cel.componentStopping(csce1);
        }

        @Override
        public void reload(boolean isForced) throws GrmException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            if (isForced) {
                stop(false);
                start();
            } else {
                cmpState = ComponentState.RELOADING;
                ComponentStateChangedEvent csce1 = new ComponentStateChangedEvent(eventId.get(), name, hostname, cmpState);
                cmpState = ComponentState.STARTED;
                ComponentStateChangedEvent csce2 = new ComponentStateChangedEvent(eventId.getAndIncrement(), name, hostname, cmpState);
                cel.componentStarted(csce2);
                cel.componentReloading(csce1);
            }
        }
    }

    private class Issue327ServiceMockup extends ServiceMockup {

        public Issue327ServiceMockup(boolean throwremote, String name, String host) {
            super(throwremote, name, host);
        }

        @Override
        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
            if (throwremote) {
                throw new GrmRemoteException();
            }
            Resource r = resources.remove(resourceId);
            if (r == null) {
                throw new UnknownResourceException();
            }
            r.setState(Resource.State.UNASSIGNED);
            ResourceRemovedEvent rre1 = new ResourceRemovedEvent(eventId.getAndIncrement(), name, r, "removed");
            sel.resourceRemoved(rre1);
            r.setState(Resource.State.UNASSIGNED);
            ResourcePropertyUpdated updated = new ResourcePropertyUpdated("test", "test1", "test2");
            ResourceChangedEvent rpce = new ResourceChangedEvent(eventId.getAndIncrement(), name, r, Collections.<ResourceChanged>singleton(updated), "modified");
            sel.resourceChanged(rpce);
        }
    }

    /**
     * Test of full refresh action, of class ServiceCachingProxy.
     * @throws java.lang.Exception 
     */
    public void testRefreshIssue545() throws Exception {
        log.info("refreshIssue545");
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.ASSIGNED);
        final String name = "refreshAddResource";
        ServiceMockup s = new ServiceMockup(false, name, host);
        /* setup service mockup */
        s.setState(ComponentState.STARTED);
        s.setServiceState(ServiceState.RUNNING);
        s.resources.put(id, resource);
        ResourceManager rm = new ResourceManagerMockup(true);

        GenericResourceSEL sel = new GenericResourceSEL(resource).awaitAdd(true).awaitAdded(true).awaitRemove(true).awaitRemoved(true);
        /* refresh action will be triggered manually */
        final DummySCP instance = new DummySCP(env, s, rm);
        try {
            instance.addServiceEventListener(sel);
            Method m = ServiceCachingProxy.class.getDeclaredMethod("startActionProcessing");
            m.setAccessible(true);
            m.invoke(instance);
            m.setAccessible(false);

            List<Future<Void>> futures = new LinkedList<Future<Void>>();

            futures.add(instance.addAction(instance.new DummyAction(0, resource)));
            futures.add(instance.addAction(instance.new DummyAction(1, resource)));
            futures.add(instance.addAction(instance.new DummyAction(2, resource)));
            futures.add(instance.addAction(instance.new DummyAction(3, resource)));
            assertTrue("Scheduled actions map should contain 4 actions", instance.getScheduledActionSize() == 4);

            instance.fullRefresh();

            /* wait for action processing ... */
            boolean ret = awaitActionProcessed(instance, sel);


            for (int i = 0; i < 10; i++) {
                boolean allDone = true;
                for (Future<Void> f : futures) {
                    allDone &= f.isDone();
                }
                if (allDone) {
                    break;
                } else {
                    Thread.sleep(100);
                }
                if (i == 9) {
                    fail("Submitted actions are not finished within give timeout, may signal a problem!");
                }
            }

            assertTrue("Scheduled actions map should be empty", instance.isScheduledActionMapEmpty());
            assertTrue("Listener should receive AddResourceEvent, ResourceAddedEvent, RemoveResourceEvent, ResourceRemovedEvent", ret);
        } finally {
            instance.destroy();
        }


    }

    /**
     * Test that an ongoing resource remove action can be interrupted with a reload.  (issue 667)
     * The test ensures that the resource remove finishes before the service state changes to SHUTDOWN
     *
     * @throws java.lang.Exception
     */
    public void testRemoveResourceAndReload() throws Exception {
        log.entering(getClass().getName(), "testRemoveResourceAndReload");
        @SuppressWarnings("unchecked")
        final ServiceTester serviceTester = new ServiceTester(new ServiceContainerStateTransitionTest("ms").createComponentTestAdapter());
        serviceTester.setup(ServiceFeature.BLOCK_RELOAD_SERVICE);
        serviceTester.setup(ServiceFeature.BLOCK_REMOVE_RESOURCE);

        ResourceManager rm = new ResourceManagerMockup(true);
        final ServiceCachingProxy instance = new ServiceCachingProxy(env, serviceTester.getService(), rm, true);
        instance.awaitEndOfFullRefresh(10000);

        //startup service and check the state change!
        instance.start();
        serviceTester.expect(ServiceState.STARTING, ServiceState.RUNNING);
        serviceTester.expect(ComponentState.STARTING, ComponentState.STARTED);

        //now create and add a resource, working as service is running
        Resource resource = DefaultResourceFactory.createResourceByName(env, rt, "id");
        ResourceId id = resource.getId();
        resource.setState(Resource.State.UNASSIGNED);
        GenericResourceSEL selResAdded = new GenericResourceSEL(resource).awaitAdd(true).awaitAdded(true);
        instance.addServiceEventListener(selResAdded);
        serviceTester.getService().addResource(resource);
        awaitActionProcessed(instance, selResAdded);

        //now remove the resource but the trap ensures that it will be blocked
        instance.removeResource(id, ResourceRemovalFromSystemDescriptor.INSTANCE);
        serviceTester.waitUntil(ServiceFeature.BLOCK_REMOVE_RESOURCE);

        //now reload the service!
        instance.reload(false);

        //The service should hang in shutdown as destroyServiceElements is ongoing. (waiting for the resource_remove action to finish)
        serviceTester.expect(ComponentState.RELOADING);
        serviceTester.expect(ServiceState.SHUTDOWN);

        //now finish the removal! this will finish the shut down service elements 
        serviceTester.teardown(ServiceFeature.BLOCK_REMOVE_RESOURCE);

        //if reload service is called, the service is already in reloading state handler block it here ...
        serviceTester.waitUntil(ServiceFeature.BLOCK_RELOAD_SERVICE);


        //to ensure that the service state is now UNKNOWN
        serviceTester.expect(ServiceState.UNKNOWN);

        serviceTester.teardown(ServiceFeature.BLOCK_RELOAD_SERVICE);

        serviceTester.expect(ServiceState.RUNNING);
        serviceTester.expect(ComponentState.STARTED);

        log.exiting(getClass().getName(), "testRemoveResourceAndReload");
    }

    class DummySCP extends ServiceCachingProxy {

        public DummySCP(ExecutionEnv env, Service s, ResourceManager rm) throws InvalidServiceException {
            super(env, s, rm, INF);
        }

        boolean isScheduledActionMapEmpty() {
            return scheduledActions.isEmpty();
        }

        int getScheduledActionSize() {
            return scheduledActions.size();
        }

        Future<Void> addAction(Action a) {
            Future<Void> future = actionExecutor.submit(a);
            scheduledActions.put(a, future);
            return future;
        }

        class DummyAction extends Action {

            private int ticker;
            private Resource resource;

            DummyAction(int ticker, Resource resource) {
                this.ticker = ticker;
                this.resource = resource;
            }

            @Override
            public void execute() throws Exception {
                switch (ticker) {
                    case 0:
                        svcEventForwarder.fireAddResource(resource, "");
                        break;
                    case 1:
                        svcEventForwarder.fireResourceAdded(resource, "");
                        break;
                    case 2:
                        svcEventForwarder.fireRemoveResource(resource, "");
                        break;
                    case 3:
                        svcEventForwarder.fireResourceRemoved(resource, "");
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
