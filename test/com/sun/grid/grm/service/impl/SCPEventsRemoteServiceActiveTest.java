/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.impl.ResourceIdImpl;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Test for service caching proxy. The test tests situation, when remote service and
 * SCP acts as active (scp cached service state is RUNNING). 
 */
public class SCPEventsRemoteServiceActiveTest extends AbstractSCPEventsTest {

    public SCPEventsRemoteServiceActiveTest(String aTestName) {
        super(aTestName);
    }

     /**
     * The method will resolve whether the outgoing events in what order should be triggered for
     * given combination of state of cached resource and incoming event from remote service.
     *     
     * This method works also for change in ambiguous flag and static field, as Resource is using
     * the same method for identifying the change in ambiguous flag or static field.
     * 
     * Below is description of what events have to be sent - delimiter state the incoming (remote)
     * event, column headers are descriptive.
     * 
     * cached       state of cached     remote resource diff    forwarded events
     * --------- AddResourceEvent ------------------------------------------
     * no           -                   -                       AddResourceEvent
     * yes          ASSIGNING           no                      -
     * yes          ASSIGNING           yes                     ResourceChangedEvent
     * yes          ASSIGNED            no                      AddResourceEvent
     * yes          ASSIGNED            yes                     AddResourceEvent, ResourceChangedEvent
     * yes          ERROR               no                      AddResourceEvent
     * yes          ERROR               yes                     AddResourceEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      AddResourceEvent
     * yes          UNASSIGNING         yes                     AddResourceEvent, ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) AddResourceEvent
     * yes          UNASSIGNED          yes                     (bug) AddResourceEvent, ResourceChangedEvent 
     * --------- ResourceAddedEvent ------------------------------------------
     * no           -                   -                       AddResourceEvent, ResourceAddedEvent
     * yes          ASSIGNING           no                      ResourceAddedEvent
     * yes          ASSIGNING           yes                     ResourceAddedEvent, ResourceChangedEvent
     * yes          ASSIGNED            no                      -
     * yes          ASSIGNED            yes                     ResourceAddedEvent, ResourceChangedEvent
     * yes          ERROR               no                      ResourceResetEvent
     * yes          ERROR               yes                     ResourceResetEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      AddResourceEvent, ResourceAddedEvent
     * yes          UNASSIGNING         yes                     AddResourceEvent, ResourceAddedEvent, ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) ResourceResetEvent, 
     * yes          UNASSIGNED          yes                     (bug) ResourceResetEvent, ResourceChangedEvent
     * --------- RemoveResourceEvent ------------------------------------------
     * no           -                   -                       AddResourceEvent, ResourceAddedEvent, RemoveResourceEvent
     * yes          ASSIGNING           no                      ResourceAddedEvent, RemoveResourceEvent
     * yes          ASSIGNING           yes                     ResourceAddedEvent, RemoveResourceEvent, ResourceChangedEvent
     * yes          ASSIGNED            no                      RemoveResourceEvent
     * yes          ASSIGNED            yes                     RemoveResourceEvent, ResourceChangedEvent
     * yes          ERROR               no                      RemoveResourceEvent
     * yes          ERROR               yes                     RemoveResourceEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      -
     * yes          UNASSIGNING         yes                     ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) RemoveResourceEvent
     * yes          UNASSIGNED          yes                     (bug) RemoveResourceEvent, ResourceChangedEvent
     * --------- ResourceRemovedEvent ------------------------------------------
     * no           -                   -                       - (log resource with its properties)
     * yes          ASSIGNING           -                       ResourceAddedEvent, RemoveResourceEvent, ResourceRemovedEvent
     * yes          ASSIGNED            -                       RemoveResourceEvent, ResourceRemovedEvent
     * yes          ERROR               -                       RemoveResourceEvent, ResourceRemovedEvent
     * yes          UNASSIGNING         -                       -
     * yes          UNASSIGNED          -                       (bug) RemoveResourceEvent, ResourceRemovedEvent
     * --------- ResourceRejectedEvent ------------------------------------------
     * no           -                   -                       - (log resource with its properties)
     * yes          ASSIGNING           -                       ResourceRejectedEvent
     * yes          ASSIGNED            -                       ResourceRejectedEvent
     * yes          ERROR               -                       ResourceRejectedEvent
     * yes          UNASSIGNING         -                       ResourceRejectedEvent
     * yes          UNASSIGNED          -                       (bug) ResourceRejectedEvent
     * --------- ResourceErrorEvent ------------------------------------------
     * no           -                   -                       AddResourceEvent, ResourceAddedEvent
     * yes          ASSIGNING           no                      ResourceAddedEvent
     * yes          ASSIGNING           yes                     ResourceAddedEvent, ResourceChangedEvent
     * yes          ASSIGNED            no                      -
     * yes          ASSIGNED            yes                     ResourceAddedEvent, ResourceChangedEvent
     * yes          ERROR               no                      ResourceResetEvent
     * yes          ERROR               yes                     ResourceResetEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      AddResourceEvent, ResourceAddedEvent
     * yes          UNASSIGNING         yes                     AddResourceEvent, ResourceAddedEvent, ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) ResourceResetEvent, 
     * yes          UNASSIGNED          yes                     (bug) ResourceResetEvent, ResourceChangedEvent
     * --------- ResourceResetEvent ------------------------------------------
     * no           -                   -                       -
     * yes          ASSIGNING           no                      -
     * yes          ASSIGNING           yes                     ResourceChangedEvent
     * yes          ASSIGNED            no                      -
     * yes          ASSIGNED            yes                     ResourceChangedEvent
     * yes          ERROR               no                      ResourceResetEvent
     * yes          ERROR               yes                     ResourceResetEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      -
     * yes          UNASSIGNING         yes                     ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) ResourceResetEvent 
     * yes          UNASSIGNED          yes                     (bug) ResourceResetEvent, ResourceChangedEvent
     * --------- ResourceChangedEvent ------------------------------------------
     * no           -                   -                       -
     * yes          ASSIGNING           no                      -
     * yes          ASSIGNING           yes                     ResourceChangedEvent
     * yes          ASSIGNED            no                      -
     * yes          ASSIGNED            yes                     ResourceChangedEvent
     * yes          ERROR               no                      -
     * yes          ERROR               yes                     ResourceChangedEvent
     * yes          UNASSIGNING         no                      -
     * yes          UNASSIGNING         yes                     ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) -
     * yes          UNASSIGNED          yes                     (bug) ResourceChangedEvent
     * 
     * @param state state of the cached resource or null if resource was not cached
     * @param remote state of the remote resource or null if remote service does not own resource
     * @return ordered list of events that has to be sent be SCP
     */
    @Override
    public List<Class> whatEventsWillBeTriggered(Resource.State state,
            Class<?> in,
            boolean attributesChanged) {
        if (in.isAssignableFrom(AddResourceEvent.class)) {
            if (Resource.State.ASSIGNING.equals(state)) {
                if (attributesChanged) {
                    return Collections.<Class>singletonList(ResourceChangedEvent.class);
                } else {
                    return Collections.<Class>emptyList();
                }
            } else if (Resource.State.ASSIGNED.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(AddResourceEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(AddResourceEvent.class);
                }
            } else if (Resource.State.ERROR.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(AddResourceEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(AddResourceEvent.class);
                }
            } else if (Resource.State.UNASSIGNING.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(AddResourceEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(AddResourceEvent.class);
                }
            } else if (Resource.State.UNASSIGNED.equals(state)) {
                return Collections.<Class>emptyList();
            } else {
                return Collections.<Class>singletonList(AddResourceEvent.class);
            }
        } else if (in.isAssignableFrom(ResourceAddedEvent.class)) {
            if (Resource.State.ASSIGNING.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(ResourceAddedEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(ResourceAddedEvent.class);
                }
            } else if (Resource.State.ASSIGNED.equals(state)) {
                if (attributesChanged) {
                    return Collections.<Class>singletonList(ResourceChangedEvent.class);
                } else {
                    return Collections.<Class>emptyList();
                }
            } else if (Resource.State.ERROR.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(ResourceResetEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(ResourceResetEvent.class);
                }
            } else if (Resource.State.UNASSIGNING.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(3);
                    l.add(AddResourceEvent.class);
                    l.add(ResourceAddedEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(AddResourceEvent.class);
                    l.add(ResourceAddedEvent.class);
                    return l;
                }
            } else if (Resource.State.UNASSIGNED.equals(state)) {
                return Collections.<Class>emptyList();
            } else {
                List<Class> l = new ArrayList<Class>(2);
                l.add(AddResourceEvent.class);
                l.add(ResourceAddedEvent.class);
                return l;
            }
        } else if (in.isAssignableFrom(RemoveResourceEvent.class)) {
            if (Resource.State.ASSIGNING.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(3);
                    l.add(ResourceAddedEvent.class);
                    l.add(RemoveResourceEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(ResourceAddedEvent.class);
                    l.add(RemoveResourceEvent.class);
                    return l;
                }
            } else if (Resource.State.ASSIGNED.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(RemoveResourceEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(RemoveResourceEvent.class);
                }
            } else if (Resource.State.ERROR.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(RemoveResourceEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(RemoveResourceEvent.class);
                }
            } else if (Resource.State.UNASSIGNING.equals(state)) {
                if (attributesChanged) {
                    return Collections.<Class>singletonList(ResourceChangedEvent.class);
                } else {
                    return Collections.<Class>emptyList();
                }
            } else if (Resource.State.UNASSIGNED.equals(state)) {
                return Collections.<Class>emptyList();
            } else {
                List<Class> l = new ArrayList<Class>(3);
                l.add(AddResourceEvent.class);
                l.add(ResourceAddedEvent.class);
                l.add(RemoveResourceEvent.class);
                return l;
            }
        } else if (in.isAssignableFrom(ResourceRemovedEvent.class)) {
            if (Resource.State.ASSIGNING.equals(state)) {
                List<Class> l = new ArrayList<Class>(2);
                l.add(ResourceAddedEvent.class);
                l.add(RemoveResourceEvent.class);
                l.add(ResourceRemovedEvent.class);
                return l;
            } else if (Resource.State.ASSIGNED.equals(state)) {
                List<Class> l = new ArrayList<Class>(2);
                l.add(RemoveResourceEvent.class);
                l.add(ResourceRemovedEvent.class);
                return l;
            } else if (Resource.State.ERROR.equals(state)) {
                List<Class> l = new ArrayList<Class>(2);
                l.add(RemoveResourceEvent.class);
                l.add(ResourceRemovedEvent.class);
                return l;
            } else if (Resource.State.UNASSIGNING.equals(state)) {
                return Collections.<Class>singletonList(ResourceRemovedEvent.class);
            } else if (Resource.State.UNASSIGNED.equals(state)) {
                return Collections.<Class>singletonList(ResourceRemovedEvent.class);
            } else {
                return Collections.<Class>emptyList();
            }
        } else if (in.isAssignableFrom(ResourceRejectedEvent.class)) {
            if (Resource.State.ASSIGNING.equals(state)) {
                return Collections.<Class>singletonList(ResourceRejectedEvent.class);
            } else if (Resource.State.ASSIGNED.equals(state)) {
                return Collections.<Class>singletonList(ResourceRejectedEvent.class);
            } else if (Resource.State.ERROR.equals(state)) {
                return Collections.<Class>singletonList(ResourceRejectedEvent.class);
            } else if (Resource.State.UNASSIGNING.equals(state)) {
                return Collections.<Class>singletonList(ResourceRejectedEvent.class);
            } else if (Resource.State.UNASSIGNED.equals(state)) {
                return Collections.<Class>singletonList(ResourceRejectedEvent.class);
            } else {
                return Collections.<Class>emptyList();
            }
        } else if (in.isAssignableFrom(ResourceErrorEvent.class)) {
            if (Resource.State.ASSIGNING.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(ResourceErrorEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(ResourceErrorEvent.class);
                }
            } else if (Resource.State.ASSIGNED.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(ResourceErrorEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(ResourceErrorEvent.class);
                }
            } else if (Resource.State.ERROR.equals(state)) {
                if (attributesChanged) {
                    return Collections.<Class>singletonList(ResourceChangedEvent.class);
                } else {
                    return Collections.<Class>emptyList();
                }
            } else if (Resource.State.UNASSIGNING.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(ResourceErrorEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(ResourceErrorEvent.class);
                }
            } else if (Resource.State.UNASSIGNED.equals(state)) {
                return Collections.<Class>emptyList();
            } else {
                List<Class> l = new ArrayList<Class>(2);
                l.add(AddResourceEvent.class);
                l.add(ResourceErrorEvent.class);
                return l;
            }
        } else if (in.isAssignableFrom(ResourceResetEvent.class)) {
            if (Resource.State.ASSIGNING.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(ResourceResetEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(ResourceResetEvent.class);
                }
            } else if (Resource.State.ASSIGNED.equals(state)) {
                if (attributesChanged) {
                    return Collections.<Class>singletonList(ResourceChangedEvent.class);
                } else {
                    return Collections.<Class>emptyList();
                }
            } else if (Resource.State.ERROR.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(ResourceResetEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    return Collections.<Class>singletonList(ResourceResetEvent.class);
                }
            } else if (Resource.State.UNASSIGNING.equals(state)) {
                if (attributesChanged) {
                    List<Class> l = new ArrayList<Class>(3);
                    l.add(AddResourceEvent.class);
                    l.add(ResourceAddedEvent.class);
                    l.add(ResourceChangedEvent.class);
                    return l;
                } else {
                    List<Class> l = new ArrayList<Class>(2);
                    l.add(AddResourceEvent.class);
                    l.add(ResourceAddedEvent.class);
                    return l;
                }
            } else if (Resource.State.UNASSIGNED.equals(state)) {
                return Collections.<Class>emptyList();
            } else {
                List<Class> l = new ArrayList<Class>(2);
                l.add(AddResourceEvent.class);
                l.add(ResourceAddedEvent.class);
                return l;
            }
        } else if (in.isAssignableFrom(ResourceChangedEvent.class)) {
            if (Resource.State.ASSIGNING.equals(state)) {
                if (attributesChanged) {
                    return Collections.<Class>singletonList(ResourceChangedEvent.class);
                } else {
                    return Collections.<Class>emptyList();
                }
            } else if (Resource.State.ASSIGNED.equals(state)) {
                if (attributesChanged) {
                    return Collections.<Class>singletonList(ResourceChangedEvent.class);
                } else {
                    return Collections.<Class>emptyList();
                }
            } else if (Resource.State.ERROR.equals(state)) {
                if (attributesChanged) {
                    return Collections.<Class>singletonList(ResourceChangedEvent.class);
                } else {
                    return Collections.<Class>emptyList();
                }
            } else if (Resource.State.UNASSIGNING.equals(state)) {
                if (attributesChanged) {
                    return Collections.<Class>singletonList(ResourceChangedEvent.class);
                } else {
                    return Collections.<Class>emptyList();
                }
            } else if (Resource.State.UNASSIGNED.equals(state)) {
                return Collections.<Class>emptyList();
            } else {
                return Collections.<Class>emptyList();
            }
        } else {
            return Collections.<Class>emptyList();
        }
    }

    /**
     * Helper method that drives tests for all variations or scp cached service state
     * that are supported by this test suite.
     * 
     * @param info                  description of a test
     * @param incomingEvent         event sent by remote service and received by SCP
     * @param cachedState           original state of a resource (if cached) or null if resource was not cached before
     * @param refreshedState        state of a resource after refresh (result of event processing)
     * @param isUnique              flag signaling whether the subject resource is unique
     * @param willBeAmbiguous       flag signaling whether the subject resource will become ambiguous
     * @param willBeRegistered          flag signaling whether the subject resource will be registered after refresh
     * @param attributesChanged         flag signaling whether the remote resource's attributes changed during test since resource was cached
     * @param cachedResourceDiffers     flag signaling whether the cached resource properties is different from remote resource properties
     * @param cachedResourceIsAmbiguous flag signaling whether the cached resource is ambiguous
     * @param remoteStatic          flag signaling whether the remote resource is static
     * @param id                    resource id for the resource
     * @throws java.lang.Exception      
     */
    @Override
    protected void genericResourceEventTestVariations(final String short_info,
            final Class incomingEvent,
            final Resource.State cachedState,
            final Resource.State refreshedState,
            final boolean isUnique,
            final boolean willBeAmbiguous,
            final boolean willBeRegistered,
            final boolean attributesChanged,
            final boolean cachedResourceDiffers,
            final boolean cachedResourceIsAmbiguous,
            final boolean remoteStatic,
            final ResourceId id) throws Exception {
        genericResourceEventTest(short_info, incomingEvent, cachedState, refreshedState, isUnique, willBeAmbiguous, willBeRegistered, attributesChanged, cachedResourceDiffers, cachedResourceIsAmbiguous, remoteStatic, id, ServiceState.RUNNING);
    }
// -------------------------- JUNIT TESTS SECTION ------------------------------
    public void testUnknownResourceAddResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceAddResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, null, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testUnknownResourceResourceAddedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceAddedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, null, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testUnknownResourceRemoveResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoveResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, null, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testUnknownResourceResourceRemovedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRemovedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, null, null, true, false, false, false, id);
    }

    public void testUnknownResourceResourceRejectedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRejectedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, null, null, true, false, false, false, id);
    }

    public void testUnknownResourceResourceResetEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceResetEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, null, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testUnknownResourceResourceErrorEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceErrorEvent";
        /* TODO maybe we should change SCP, that it will not forward error event and not cache error resource, and make it rely only on full refresh that is already able to handle it */
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, null, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testUnknownResourceResourceChangedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceChangedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, null, null, true, false, false, false, id);
    }

    public void testASSIGNINGResourceAddResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceAddResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testASSIGNINGResourceResourceAddedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceAddedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testASSIGNINGResourceRemoveResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoveResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testASSIGNINGResourceResourceRemovedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRemovedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNING, null, true, false, false, false, id);
    }

    public void testASSIGNINGResourceResourceRejectedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRejectedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNING, null, true, false, false, false, id);
    }

    public void testASSIGNINGResourceResourceResetEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceResetEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testASSIGNINGResourceResourceErrorEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceErrorEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNING, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testASSIGNINGResourceResourceChangedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceChangedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testASSIGNEDResourceAddResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceAddResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testASSIGNEDResourceResourceAddedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceAddedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testASSIGNEDResourceRemoveResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoveResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testASSIGNEDResourceResourceRemovedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRemovedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNED, null, true, false, false, false, id);
    }

    public void testASSIGNEDResourceResourceRejectedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRejectedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNED, null, true, false, false, false, id);
    }

    public void testASSIGNEDResourceResourceResetEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceResetEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testASSIGNEDResourceResourceErrorEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceErrorEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNED, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testASSIGNEDResourceResourceChangedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceChangedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testERRORResourceAddResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceAddResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ERROR, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testERRORResourceResourceAddedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceAddedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testERRORResourceRemoveResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoveResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ERROR, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testERRORResourceResourceRemovedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRemovedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ERROR, null, true, false, false, false, id);
    }

    public void testERRORResourceResourceRejectedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRejectedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ERROR, null, true, false, false, false, id);
    }

    public void testERRORResourceResourceResetEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceResetEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testERRORResourceResourceErrorEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceErrorEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ERROR, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testERRORResourceResourceChangedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceChangedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ERROR, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testUNASSIGNINGResourceAddResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceAddResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceAddedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceAddedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoveResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoveResourceEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceRemovedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRemovedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.UNASSIGNING, null, true, false, false, false, id);
    }

    public void testUNASSIGNINGResourceResourceRejectedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRejectedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.UNASSIGNING, null, true, false, false, false, id);
    }

    public void testUNASSIGNINGResourceResourceResetEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceResetEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceErrorEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceErrorEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.UNASSIGNING, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceChangedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceChangedEvent";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testUnknownResourceAddResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceAddResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, null, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testUnknownResourceResourceAddedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceAddedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, null, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testUnknownResourceRemoveResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoveResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, null, Resource.State.UNASSIGNING, false, true, true, false, id);
    }

    public void testUnknownResourceResourceRemovedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRemovedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, null, null, false, true, false, false, id);
    }

    public void testUnknownResourceResourceRejectedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRejectedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, null, null, false, true, false, false, id);
    }

    public void testUnknownResourceResourceResetEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceResetEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, null, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testUnknownResourceResourceErrorEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceErrorEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, null, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testUnknownResourceResourceChangedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceChangedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, null, null, false, true, false, false, id);
    }

    public void testASSIGNINGResourceAddResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceAddResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testASSIGNINGResourceResourceAddedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceAddedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testASSIGNINGResourceRemoveResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoveResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, false, true, true, false, id);
    }

    public void testASSIGNINGResourceResourceRemovedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRemovedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNING, null, false, true, false, false, id);
    }

    public void testASSIGNINGResourceResourceRejectedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRejectedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNING, null, false, true, false, false, id);
    }

    public void testASSIGNINGResourceResourceResetEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceResetEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testASSIGNINGResourceResourceErrorEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceErrorEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNING, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testASSIGNINGResourceResourceChangedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceChangedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testASSIGNEDResourceAddResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceAddResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testASSIGNEDResourceResourceAddedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceAddedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testASSIGNEDResourceRemoveResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoveResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, false, true, true, false, id);
    }

    public void testASSIGNEDResourceResourceRemovedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRemovedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNED, null, false, true, false, false, id);
    }

    public void testASSIGNEDResourceResourceRejectedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRejectedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNED, null, false, true, false, false, id);
    }

    public void testASSIGNEDResourceResourceResetEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceResetEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testASSIGNEDResourceResourceErrorEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceErrorEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNED, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testASSIGNEDResourceResourceChangedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceChangedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testERRORResourceAddResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceAddResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ERROR, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testERRORResourceResourceAddedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceAddedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testERRORResourceRemoveResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoveResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ERROR, Resource.State.UNASSIGNING, false, true, true, false, id);
    }

    public void testERRORResourceResourceRemovedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRemovedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ERROR, null, false, true, false, false, id);
    }

    public void testERRORResourceResourceRejectedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRejectedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ERROR, null, false, true, false, false, id);
    }

    public void testERRORResourceResourceResetEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceResetEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testERRORResourceResourceErrorEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceErrorEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ERROR, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testERRORResourceResourceChangedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceChangedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ERROR, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testUNASSIGNINGResourceAddResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceAddResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceAddedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceAddedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testUNASSIGNINGResourceRemoveResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoveResourceEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceRemovedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRemovedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.UNASSIGNING, null, false, true, false, false, id);
    }

    public void testUNASSIGNINGResourceResourceRejectedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRejectedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.UNASSIGNING, null, false, true, false, false, id);
    }

    public void testUNASSIGNINGResourceResourceResetEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceResetEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceErrorEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceErrorEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.UNASSIGNING, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceChangedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceChangedEventNotUnique";
        genericNonAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, true, false, id);
    }
//  -------------------------- UNIQUE REMOTE STATIC -------------------------------------
    public void testUnknownResourceAddResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceAddResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, null, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testUnknownResourceResourceAddedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceAddedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, null, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testUnknownResourceRemoveResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoveResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, null, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testUnknownResourceResourceRemovedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRemovedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, null, null, true, false, false, id);
    }

    public void testUnknownResourceResourceRejectedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRejectedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, null, null, true, false, false, id);
    }

    public void testUnknownResourceResourceResetEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceResetEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, null, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testUnknownResourceResourceErrorEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceErrorEventRemoteStatic";
        /* TODO maybe we should change SCP, that it will not forward error event and not cache error resource, and make it rely only on full refresh that is already able to handle it */
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, null, Resource.State.ERROR, true, false, true, id);
    }

    public void testUnknownResourceResourceChangedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceChangedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, null, null, true, false, false, id);
    }

    public void testASSIGNINGResourceAddResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceAddResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testASSIGNINGResourceResourceAddedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceAddedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testASSIGNINGResourceRemoveResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoveResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testASSIGNINGResourceResourceRemovedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRemovedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNING, null, true, false, false, id);
    }

    public void testASSIGNINGResourceResourceRejectedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRejectedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNING, null, true, false, false, id);
    }

    public void testASSIGNINGResourceResourceResetEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceResetEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testASSIGNINGResourceResourceErrorEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceErrorEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNING, Resource.State.ERROR, true, false, true, id);
    }

    public void testASSIGNINGResourceResourceChangedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceChangedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testASSIGNEDResourceAddResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceAddResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testASSIGNEDResourceResourceAddedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceAddedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testASSIGNEDResourceRemoveResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoveResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testASSIGNEDResourceResourceRemovedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRemovedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNED, null, true, false, false, id);
    }

    public void testASSIGNEDResourceResourceRejectedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRejectedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNED, null, true, false, false, id);
    }

    public void testASSIGNEDResourceResourceResetEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceResetEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testASSIGNEDResourceResourceErrorEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceErrorEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNED, Resource.State.ERROR, true, false, true, id);
    }

    public void testASSIGNEDResourceResourceChangedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceChangedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testERRORResourceAddResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceAddResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, Resource.State.ERROR, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testERRORResourceResourceAddedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceAddedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testERRORResourceRemoveResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoveResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, Resource.State.ERROR, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testERRORResourceResourceRemovedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRemovedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, Resource.State.ERROR, null, true, false, false, id);
    }

    public void testERRORResourceResourceRejectedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRejectedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, Resource.State.ERROR, null, true, false, false, id);
    }

    public void testERRORResourceResourceResetEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceResetEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testERRORResourceResourceErrorEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceErrorEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, Resource.State.ERROR, Resource.State.ERROR, true, false, true, id);
    }

    public void testERRORResourceResourceChangedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceChangedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, Resource.State.ERROR, Resource.State.ERROR, true, false, true, id);
    }

    public void testUNASSIGNINGResourceAddResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceAddResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testUNASSIGNINGResourceResourceAddedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceAddedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testUNASSIGNINGResourceRemoveResourceEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoveResourceEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testUNASSIGNINGResourceResourceRemovedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRemovedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, Resource.State.UNASSIGNING, null, true, false, false, id);
    }

    public void testUNASSIGNINGResourceResourceRejectedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRejectedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, Resource.State.UNASSIGNING, null, true, false, false, id);
    }

    public void testUNASSIGNINGResourceResourceResetEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceResetEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testUNASSIGNINGResourceResourceErrorEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceErrorEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, Resource.State.UNASSIGNING, Resource.State.ERROR, true, false, true, id);
    }

    public void testUNASSIGNINGResourceResourceChangedEventRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceChangedEventRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testUnknownResourceAddResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceAddResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, null, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testUnknownResourceResourceAddedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceAddedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, null, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testUnknownResourceRemoveResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoveResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, null, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testUnknownResourceResourceRemovedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRemovedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, null, null, false, true, false, id);
    }

    public void testUnknownResourceResourceRejectedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRejectedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, null, null, false, true, false, id);
    }

    public void testUnknownResourceResourceResetEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceResetEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, null, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testUnknownResourceResourceErrorEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceErrorEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, null, Resource.State.ERROR, false, true, true, id);
    }

    public void testUnknownResourceResourceChangedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceChangedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, null, null, false, true, false, id);
    }

    public void testASSIGNINGResourceAddResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceAddResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testASSIGNINGResourceResourceAddedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceAddedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testASSIGNINGResourceRemoveResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoveResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testASSIGNINGResourceResourceRemovedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRemovedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNING, null, false, true, false, id);
    }

    public void testASSIGNINGResourceResourceRejectedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRejectedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNING, null, false, true, false, id);
    }

    public void testASSIGNINGResourceResourceResetEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceResetEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testASSIGNINGResourceResourceErrorEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceErrorEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNING, Resource.State.ERROR, false, true, true, id);
    }

    public void testASSIGNINGResourceResourceChangedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceChangedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testASSIGNEDResourceAddResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceAddResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testASSIGNEDResourceResourceAddedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceAddedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testASSIGNEDResourceRemoveResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoveResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testASSIGNEDResourceResourceRemovedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRemovedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNED, null, false, true, false, id);
    }

    public void testASSIGNEDResourceResourceRejectedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRejectedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNED, null, false, true, false, id);
    }

    public void testASSIGNEDResourceResourceResetEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceResetEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testASSIGNEDResourceResourceErrorEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceErrorEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNED, Resource.State.ERROR, false, true, true, id);
    }

    public void testASSIGNEDResourceResourceChangedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceChangedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testERRORResourceAddResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceAddResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, Resource.State.ERROR, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testERRORResourceResourceAddedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceAddedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testERRORResourceRemoveResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoveResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, Resource.State.ERROR, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testERRORResourceResourceRemovedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRemovedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, Resource.State.ERROR, null, false, true, false, id);
    }

    public void testERRORResourceResourceRejectedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRejectedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, Resource.State.ERROR, null, false, true, false, id);
    }

    public void testERRORResourceResourceResetEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceResetEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testERRORResourceResourceErrorEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceErrorEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, Resource.State.ERROR, Resource.State.ERROR, false, true, true, id);
    }

    public void testERRORResourceResourceChangedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceChangedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, Resource.State.ERROR, Resource.State.ERROR, false, true, true, id);
    }

    public void testUNASSIGNINGResourceAddResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceAddResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, AddResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testUNASSIGNINGResourceResourceAddedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceAddedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceAddedEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testUNASSIGNINGResourceRemoveResourceEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoveResourceEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, RemoveResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testUNASSIGNINGResourceResourceRemovedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRemovedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRemovedEvent.class, Resource.State.UNASSIGNING, null, false, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceRejectedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRejectedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceRejectedEvent.class, Resource.State.UNASSIGNING, null, false, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceResetEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceResetEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceResetEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testUNASSIGNINGResourceResourceErrorEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceErrorEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceErrorEvent.class, Resource.State.UNASSIGNING, Resource.State.ERROR, false, true, true, id);
    }

    public void testUNASSIGNINGResourceResourceChangedEventNotUniqueRemoteStatic() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceChangedEventNotUniqueRemoteStatic";
        genericNonAmbiguousCachedResourceEventRemoteStaticTest(short_info, ResourceChangedEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, true, id);
    }
//    
//    -------------------------- AMBIGUOUS -------------------------------------
    public void testAmbiguousASSIGNINGResourceAddResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceAddResourceEvent";
        genericAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceAddedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceAddedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoveResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoveResourceEvent";
        genericAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceRemovedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceRemovedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNING, null, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceRejectedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceRejectedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNING, null, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceResetEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceResetEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceErrorEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceErrorEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNING, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceChangedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceChangedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceAddResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceAddResourceEvent";
        genericAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceAddedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceAddedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoveResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoveResourceEvent";
        genericAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceRemovedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceRemovedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNED, null, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceRejectedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceRejectedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNED, null, true, false, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceResetEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceResetEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceErrorEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceErrorEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNED, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceChangedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceChangedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testAmbiguousERRORResourceAddResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceAddResourceEvent";
        genericAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ERROR, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousERRORResourceResourceAddedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceAddedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testAmbiguousERRORResourceRemoveResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoveResourceEvent";
        genericAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ERROR, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousERRORResourceResourceRemovedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceRemovedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ERROR, null, true, false, false, false, id);
    }

    public void testAmbiguousERRORResourceResourceRejectedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceRejectedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ERROR, null, true, false, false, false, id);
    }

    public void testAmbiguousERRORResourceResourceResetEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceResetEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testAmbiguousERRORResourceResourceErrorEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceErrorEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ERROR, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testAmbiguousERRORResourceResourceChangedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceChangedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ERROR, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceAddResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceAddResourceEvent";
        genericAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceAddedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceAddedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoveResourceEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoveResourceEvent";
        genericAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceRemovedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceRemovedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.UNASSIGNING, null, true, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceRejectedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceRejectedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.UNASSIGNING, null, true, false, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceResetEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceResetEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceErrorEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceErrorEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.UNASSIGNING, Resource.State.ERROR, true, false, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceChangedEvent() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceChangedEvent";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceAddResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceAddResourceEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceAddedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceAddedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceRemoveResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceRemoveResourceEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceRemovedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceRemovedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNING, null, false, true, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceRejectedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceRejectedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNING, null, false, true, false, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceResetEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceResetEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceErrorEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceErrorEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNING, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNINGResourceResourceChangedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNINGResourceResourceChangedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceAddResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceAddResourceEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceAddedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceAddedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceRemoveResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceRemoveResourceEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceRemovedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceRemovedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNED, null, false, true, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceRejectedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceRejectedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNED, null, false, true, false, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceResetEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceResetEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceErrorEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceErrorEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNED, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testAmbiguousASSIGNEDResourceResourceChangedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousASSIGNEDResourceResourceChangedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testAmbiguousERRORResourceAddResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceAddResourceEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.ERROR, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testAmbiguousERRORResourceResourceAddedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceAddedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testAmbiguousERRORResourceRemoveResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceRemoveResourceEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.ERROR, Resource.State.UNASSIGNING, false, true, true, false, id);
    }

    public void testAmbiguousERRORResourceResourceRemovedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceRemovedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.ERROR, null, false, true, false, false, id);
    }

    public void testAmbiguousERRORResourceResourceRejectedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceRejectedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.ERROR, null, false, true, false, false, id);
    }

    public void testAmbiguousERRORResourceResourceResetEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceResetEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testAmbiguousERRORResourceResourceErrorEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceErrorEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.ERROR, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testAmbiguousERRORResourceResourceChangedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousERRORResourceResourceChangedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.ERROR, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceAddResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceAddResourceEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, AddResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, false, true, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceAddedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceAddedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceAddedEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceRemoveResourceEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceRemoveResourceEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, RemoveResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceRemovedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceRemovedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRemovedEvent.class, Resource.State.UNASSIGNING, null, false, true, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceRejectedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceRejectedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceRejectedEvent.class, Resource.State.UNASSIGNING, null, false, true, false, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceResetEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceResetEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceResetEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceErrorEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceErrorEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceErrorEvent.class, Resource.State.UNASSIGNING, Resource.State.ERROR, false, true, true, false, id);
    }

    public void testAmbiguousUNASSIGNINGResourceResourceChangedEventNotUnique() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "AmbiguousUNASSIGNINGResourceResourceChangedEventNotUnique";
        genericAmbiguousCachedResourceEventTest(short_info, ResourceChangedEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, true, false, id);
    }
//------------------------------UNIQUE CHANGED----------------------------------
    public void testASSIGNINGResourceAddResourceEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceAddResourceEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testASSIGNINGResourceResourceAddedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceAddedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testASSIGNINGResourceRemoveResourceEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoveResourceEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testASSIGNINGResourceResourceRemovedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRemovedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNING, null, true, false, false, id);
    }

    public void testASSIGNINGResourceResourceRejectedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRejectedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNING, null, true, false, false, id);
    }

    public void testASSIGNINGResourceResourceResetEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceResetEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testASSIGNINGResourceResourceErrorEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceErrorEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNING, Resource.State.ERROR, true, false, true, id);
    }

    public void testASSIGNINGResourceResourceChangedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceChangedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testASSIGNEDResourceAddResourceEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceAddResourceEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testASSIGNEDResourceResourceAddedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceAddedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testASSIGNEDResourceRemoveResourceEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoveResourceEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testASSIGNEDResourceResourceRemovedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRemovedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNED, null, true, false, false, id);
    }

    public void testASSIGNEDResourceResourceRejectedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRejectedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNED, null, true, false, false, id);
    }

    public void testASSIGNEDResourceResourceResetEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceResetEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testASSIGNEDResourceResourceErrorEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceErrorEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNED, Resource.State.ERROR, true, false, true, id);
    }

    public void testASSIGNEDResourceResourceChangedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceChangedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testERRORResourceAddResourceEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceAddResourceEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, AddResourceEvent.class, Resource.State.ERROR, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testERRORResourceResourceAddedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceAddedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceAddedEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testERRORResourceRemoveResourceEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoveResourceEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, RemoveResourceEvent.class, Resource.State.ERROR, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testERRORResourceResourceRemovedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRemovedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRemovedEvent.class, Resource.State.ERROR, null, true, false, false, id);
    }

    public void testERRORResourceResourceRejectedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRejectedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRejectedEvent.class, Resource.State.ERROR, null, true, false, false, id);
    }

    public void testERRORResourceResourceResetEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceResetEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceResetEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testERRORResourceResourceErrorEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceErrorEventRemoteChangedRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceErrorEvent.class, Resource.State.ERROR, Resource.State.ERROR, true, false, true, id);
    }

    public void testERRORResourceResourceChangedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceChangedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceChangedEvent.class, Resource.State.ERROR, Resource.State.ERROR, true, false, true, id);
    }

    public void testUNASSIGNINGResourceAddResourceEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceAddResourceEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, AddResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, true, false, true, id);
    }

    public void testUNASSIGNINGResourceResourceAddedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceAddedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceAddedEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testUNASSIGNINGResourceRemoveResourceEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoveResourceEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, RemoveResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testUNASSIGNINGResourceResourceRemovedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRemovedEvent";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRemovedEvent.class, Resource.State.UNASSIGNING, null, true, false, false, id);
    }

    public void testUNASSIGNINGResourceResourceRejectedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRejectedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRejectedEvent.class, Resource.State.UNASSIGNING, null, true, false, false, id);
    }

    public void testUNASSIGNINGResourceResourceResetEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceResetEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceResetEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, true, false, true, id);
    }

    public void testUNASSIGNINGResourceResourceErrorEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceErrorEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceErrorEvent.class, Resource.State.UNASSIGNING, Resource.State.ERROR, true, false, true, id);
    }

    public void testUNASSIGNINGResourceResourceChangedEventRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceChangedEventRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceChangedEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, true, false, true, id);
    }

    public void testUnknownResourceAddResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceAddResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, AddResourceEvent.class, null, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testUnknownResourceResourceAddedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceAddedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceAddedEvent.class, null, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testUnknownResourceRemoveResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceRemoveResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, RemoveResourceEvent.class, null, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testUnknownResourceResourceRemovedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRemovedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRemovedEvent.class, null, null, false, true, false, id);
    }

    public void testUnknownResourceResourceRejectedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceRejectedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRejectedEvent.class, null, null, false, true, false, id);
    }

    public void testUnknownResourceResourceResetEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceResetEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceResetEvent.class, null, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testUnknownResourceResourceErrorEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceErrorEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceErrorEvent.class, null, Resource.State.ERROR, false, true, true, id);
    }

    public void testUnknownResourceResourceChangedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UnknownResourceResourceChangedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceChangedEvent.class, null, null, false, true, false, id);
    }

    public void testASSIGNINGResourceAddResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceAddResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testASSIGNINGResourceResourceAddedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceAddedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testASSIGNINGResourceRemoveResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceRemoveResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNING, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testASSIGNINGResourceResourceRemovedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRemovedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNING, null, false, true, false, id);
    }

    public void testASSIGNINGResourceResourceRejectedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceRejectedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNING, null, false, true, false, id);
    }

    public void testASSIGNINGResourceResourceResetEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceResetEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testASSIGNINGResourceResourceErrorEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceErrorEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNING, Resource.State.ERROR, false, true, true, id);
    }

    public void testASSIGNINGResourceResourceChangedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNINGResourceResourceChangedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNING, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testASSIGNEDResourceAddResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceAddResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, AddResourceEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testASSIGNEDResourceResourceAddedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceAddedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceAddedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testASSIGNEDResourceRemoveResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceRemoveResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, RemoveResourceEvent.class, Resource.State.ASSIGNED, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testASSIGNEDResourceResourceRemovedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRemovedEventNotUnique";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRemovedEvent.class, Resource.State.ASSIGNED, null, false, true, false, id);
    }

    public void testASSIGNEDResourceResourceRejectedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceRejectedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRejectedEvent.class, Resource.State.ASSIGNED, null, false, true, false, id);
    }

    public void testASSIGNEDResourceResourceResetEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceResetEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceResetEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testASSIGNEDResourceResourceErrorEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceErrorEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceErrorEvent.class, Resource.State.ASSIGNED, Resource.State.ERROR, false, true, true, id);
    }

    public void testASSIGNEDResourceResourceChangedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ASSIGNEDResourceResourceChangedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceChangedEvent.class, Resource.State.ASSIGNED, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testERRORResourceAddResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceAddResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, AddResourceEvent.class, Resource.State.ERROR, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testERRORResourceResourceAddedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceAddedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceAddedEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testERRORResourceRemoveResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceRemoveResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, RemoveResourceEvent.class, Resource.State.ERROR, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testERRORResourceResourceRemovedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRemovedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRemovedEvent.class, Resource.State.ERROR, null, false, true, false, id);
    }

    public void testERRORResourceResourceRejectedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceRejectedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRejectedEvent.class, Resource.State.ERROR, null, false, true, false, id);
    }

    public void testERRORResourceResourceResetEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceResetEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceResetEvent.class, Resource.State.ERROR, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testERRORResourceResourceErrorEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceErrorEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceErrorEvent.class, Resource.State.ERROR, Resource.State.ERROR, false, true, true, id);
    }

    public void testERRORResourceResourceChangedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "ERRORResourceResourceChangedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceChangedEvent.class, Resource.State.ERROR, Resource.State.ERROR, false, true, true, id);
    }

    public void testUNASSIGNINGResourceAddResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceAddResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, AddResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNING, false, true, true, id);
    }

    public void testUNASSIGNINGResourceResourceAddedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceAddedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceAddedEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testUNASSIGNINGResourceRemoveResourceEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceRemoveResourceEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, RemoveResourceEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, true, id);
    }

    public void testUNASSIGNINGResourceResourceRemovedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRemovedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRemovedEvent.class, Resource.State.UNASSIGNING, null, false, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceRejectedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceRejectedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceRejectedEvent.class, Resource.State.UNASSIGNING, null, false, true, false, id);
    }

    public void testUNASSIGNINGResourceResourceResetEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceResetEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceResetEvent.class, Resource.State.UNASSIGNING, Resource.State.ASSIGNED, false, true, true, id);
    }

    public void testUNASSIGNINGResourceResourceErrorEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceErrorEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceErrorEvent.class, Resource.State.UNASSIGNING, Resource.State.ERROR, false, true, true, id);
    }

    public void testUNASSIGNINGResourceResourceChangedEventNotUniqueRemoteChanged() throws Exception {
        /* any resource id is valid */
        ResourceId id = new ResourceIdImpl(1);
        final String short_info = "UNASSIGNINGResourceResourceChangedEventNotUniqueRemoteChanged";
        genericNonAmbiguousCachedResourceEventRemoteChangedTest(short_info, ResourceChangedEvent.class, Resource.State.UNASSIGNING, Resource.State.UNASSIGNING, false, true, true, id);
    }
    /* COMMENT - the only not explicitly covered cases are :
     * 
     * - "ambiguous static/nonstatic cached resource, remote unique/nonunique static/nonstatic with changed properties" -> as change
     * in ambiguous flag uses the same mechanism as resource property change, this
     * case is covered by "cached resource, remote unique/not unique resource with changed properties" (problem with static)
     * 
     * - "static cached resource, remote unique/not unique static/nonstatic with changed properties" -> (problem with static)
     *  
     * TODO cover these cases explicitly (for sure) in the future
     */
}
