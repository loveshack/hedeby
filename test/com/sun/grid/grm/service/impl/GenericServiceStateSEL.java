/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Generic service event listener that waits for service events delivery within given timeout.
 */
class GenericServiceStateSEL extends ServiceEventAdapter {
    
    private final Logger log = Logger.getLogger(GenericServiceStateSEL.class.getName());
    private final Lock lock = new ReentrantLock();
    private final Condition cond = lock.newCondition();
    
    private final Set<ServiceState> expected = new HashSet<ServiceState>();
    private final Set<ServiceState> received = new HashSet<ServiceState>();
    
    GenericServiceStateSEL() {
        super();
    }
    
    private void await(ServiceState state, boolean await) {
        if (await) {
            expected.add(state);
        } else {
            expected.remove(state);
        }
    }
    public GenericServiceStateSEL awaitStarting(boolean awaitStarting) {
       await(ServiceState.STARTING, awaitStarting);
       return this;
    }
    
    public GenericServiceStateSEL awaitRunning(boolean awaitRunning) {
       await(ServiceState.RUNNING, awaitRunning);
       return this;
    }
    
    public GenericServiceStateSEL awaitError(boolean awaitError) {
       await(ServiceState.ERROR, awaitError);
       return this;
    }
    
    public GenericServiceStateSEL awaitUnknown(boolean awaitUnknown) {
       await(ServiceState.UNKNOWN, awaitUnknown);
       return this;
    }
    
    public GenericServiceStateSEL awaitShutdown(boolean awaitShutdown) {
       await(ServiceState.SHUTDOWN, awaitShutdown);
       return this;
    }
    
    public GenericServiceStateSEL awaitStopped(boolean awaitStopped) {
       await(ServiceState.STOPPED, awaitStopped);
       return this;
    }

    public boolean hasAll(long timeOut) throws InterruptedException {
        long endTime = System.currentTimeMillis() + timeOut;
        while(true) {
            lock.lock();
            try {
                if (received.containsAll(expected)) {
                    return true;
                }
                long rest = endTime - System.currentTimeMillis();
                if (rest <= 0) {
                    return false;
                } else {
                    cond.await(rest, TimeUnit.MILLISECONDS);
                }
            } finally {
                lock.unlock();
            }
        }
    }
    
    private void addEvent(ServiceState state, ServiceStateChangedEvent evt) {
        log.log(Level.INFO, "Got event {0}", evt);
        lock.lock();
        try {
            received.add(state);
            cond.signalAll();
        } finally {
            lock.unlock();
        }
    }
    

    @Override
    public void serviceStarting(ServiceStateChangedEvent evt) {
        addEvent(ServiceState.STARTING, evt);
    }

    @Override
    public void serviceRunning(ServiceStateChangedEvent evt) {
        addEvent(ServiceState.RUNNING, evt);
    }

    @Override
    public void serviceUnknown(ServiceStateChangedEvent evt) {
        addEvent(ServiceState.UNKNOWN, evt);
    }

    @Override
    public void serviceShutdown(ServiceStateChangedEvent evt) {
        addEvent(ServiceState.SHUTDOWN, evt);
    }

    @Override
    public void serviceError(ServiceStateChangedEvent evt) {
        addEvent(ServiceState.ERROR, evt);
    }

    @Override
    public void serviceStopped(ServiceStateChangedEvent evt) {
        addEvent(ServiceState.STOPPED, evt);
    }

    private boolean hasEvent(ServiceState state) {
        lock.lock();
        try  {
            return received.contains(state);
        } finally {
            lock.unlock();
        }
    }
    public boolean isGotStarting() {
        return hasEvent(ServiceState.STARTING);
    }

    public boolean isGotRunning() {
        return hasEvent(ServiceState.RUNNING);
    }

    public boolean isGotError() {
        return hasEvent(ServiceState.ERROR);
    }

    public boolean isGotUnknown() {
        return hasEvent(ServiceState.UNKNOWN);
    }

    public boolean isGotShutdown() {
        return hasEvent(ServiceState.SHUTDOWN);
    }

    public boolean isGotStopped() {
        return hasEvent(ServiceState.STOPPED);
    }
}
