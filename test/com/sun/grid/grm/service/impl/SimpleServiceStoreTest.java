/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.ServiceStoreException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Test for basic functionality of service store
 */
public class SimpleServiceStoreTest extends TestCase {

    private final static Logger log = Logger.getLogger(SimpleServiceStoreTest.class.getName());

    public SimpleServiceStoreTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of addService method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testAddService() throws Exception {
        log.info("addService");
        ServiceMockup service = new ServiceMockup("a", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        Map<String, ServiceMockup> storage = (Map<String, ServiceMockup>) f.get(instance);
        f.setAccessible(false);
        assertTrue("underlying storage should be empty", storage.isEmpty());
        ServiceMockup result = instance.addService(service);
        assertNull("addService should return null", result);
        assertTrue("underlying storage should contain service with name 'a'", storage.containsKey(service.getName()));
        assertEquals("underlying storage should contain just one element", storage.size(), 1);
    }

    /**
     * Test of addService method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testAddServiceFailGetName() throws Exception {
        log.info("addServiceFailGetName");
        ServiceMockup service = new ServiceMockup("a", true);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        Map<String, ServiceMockup> storage = (Map<String, ServiceMockup>) f.get(instance);
        f.setAccessible(false);
        assertTrue("underlying storage should be empty", storage.isEmpty());
        try {
            instance.addService(service);
            fail("addService should throw an exception");
        } catch (ServiceStoreException sse) {
            assertTrue("underlying storage should throw an exception", true);
        }
        assertTrue("underlying storage should be empty", storage.isEmpty());
    }

    /**
     * Test of addService method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testAddServiceFailStorageException() throws Exception {
        log.info("addServiceFailStorageException");
        ServiceMockup service = new ServiceMockup("a", true);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        f.set(instance, null);
        f.setAccessible(false);
        try {
            instance.addService(service);
            fail("addService should throw an exception");
        } catch (ServiceStoreException sse) {
            assertTrue("addService should throw an exception", true);
        }
    }

    /**
     * Test of removeService method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testRemoveService() throws Exception {
        log.info("removeService");
        ServiceMockup service = new ServiceMockup("a", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        Map<String, ServiceMockup> storage = (Map<String, ServiceMockup>) f.get(instance);
        f.setAccessible(false);
        storage.put(service.getName(), service);
        assertEquals("underlying storage should contain just one element", storage.size(), 1);
        ServiceMockup result = instance.removeService(service.getName());
        assertEquals("removeService returned service different than expected", result, service);
        assertTrue("underlying storage should be empty", storage.isEmpty());
    }

    /**
     * Test of removeService method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testRemoveServiceFailUnknownService() throws Exception {
        log.info("removeServiceFailUnknownService");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        Map<String, ServiceMockup> storage = (Map<String, ServiceMockup>) f.get(instance);
        f.setAccessible(false);
        storage.put(servicea.getName(), servicea);
        assertEquals("underlying storage should contain just one element", storage.size(), 1);
        try {
            instance.removeService(serviceb.getName());
            fail("removeService should throw an exception");
        } catch (UnknownServiceException use) {
            assertTrue("removeService should throw an exception", true);
        }
        assertEquals("underlying storage should contain just one element", storage.size(), 1);
        assertTrue("underlying storage should contain service with name 'a'", storage.containsKey(servicea.getName()));
    }

    /**
     * Test of removeService method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testRemoveServiceFailStorageException() throws Exception {
        log.info("removeServiceFailStorageException");
        ServiceMockup service = new ServiceMockup("a", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        f.set(instance, null);
        f.setAccessible(false);
        try {
            instance.removeService(service.getName());
            fail("removeService should throw an exception");
        } catch (ServiceStoreException sse) {
            assertTrue("removeService should throw an exception", true);
        }
    }

    /**
     * Test of getServiceNames method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testGetServiceNames() throws Exception {
        log.info("getServiceNames");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        Map<String, ServiceMockup> storage = (Map<String, ServiceMockup>) f.get(instance);
        f.setAccessible(false);
        storage.put(servicea.getName(), servicea);
        storage.put(serviceb.getName(), serviceb);
        assertEquals("underlying storage should contain two elements", storage.size(), 2);
        assertTrue("underlying storage should contain service with name 'a'", storage.containsKey(servicea.getName()));
        assertTrue("underlying storage should contain service with name 'b'", storage.containsKey(serviceb.getName()));
        Set<String> result = instance.getServiceNames();
        assertEquals("service names should contain two elements", result.size(), 2);
        assertTrue("service names should contain service with name 'a'", result.contains(servicea.getName()));
        assertTrue("service names should contain service with name 'b'", result.contains(serviceb.getName()));
    }

    /**
     * Test of getService method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testGetService() throws Exception {
        log.info("getService");
        ServiceMockup service = new ServiceMockup("a", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        Map<String, ServiceMockup> storage = (Map<String, ServiceMockup>) f.get(instance);
        f.setAccessible(false);
        storage.put(service.getName(), service);
        assertEquals("underlying storage should contain just one element", storage.size(), 1);
        ServiceMockup result = instance.getService(service.getName());
        assertEquals("getService returned service different than expected", result, service);
    }

    /**
     * Test of getService method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testGetServiceFailStorageException() throws Exception {
        log.info("getServiceFailStorageException");
        ServiceMockup service = new ServiceMockup("a", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        f.set(instance, null);
        f.setAccessible(false);
        try {
            instance.getService(service.getName());
            fail("getService should throw an exception");
        } catch (ServiceStoreException sse) {
            assertTrue("getService should throw an exception", true);
        }
    }

    /**
     * Test of getService method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testGetServiceFailUnknownService() throws Exception {
        log.info("getServiceFailUnknownService");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        Map<String, ServiceMockup> storage = (Map<String, ServiceMockup>) f.get(instance);
        f.setAccessible(false);
        storage.put(servicea.getName(), servicea);
        assertEquals("underlying storage should contain just one element", storage.size(), 1);
        try {
            instance.getService(serviceb.getName());
            fail("getService should throw an exception");
        } catch (UnknownServiceException use) {
            assertTrue("getService should throw an exception", true);
        }
        assertEquals("underlying storage should contain just one element", storage.size(), 1);
        assertTrue("underlying storage should contain service with name 'a'", storage.containsKey(servicea.getName()));
    }

    /**
     * Test of getServices method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testGetServices() throws Exception {
        log.info("getServices");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        Map<String, ServiceMockup> storage = (Map<String, ServiceMockup>) f.get(instance);
        f.setAccessible(false);
        storage.put(servicea.getName(), servicea);
        storage.put(serviceb.getName(), serviceb);
        assertEquals("underlying storage should contain two elements", storage.size(), 2);
        assertTrue("underlying storage should contain service with name 'a'", storage.containsKey(servicea.getName()));
        assertTrue("underlying storage should contain service with name 'b'", storage.containsKey(serviceb.getName()));
        List<ServiceMockup> result = instance.getServices();
        assertEquals("services should contain two elements", result.size(), 2);
        assertTrue("services should contain servicea", result.contains(servicea));
        assertTrue("services should contain serviceb", result.contains(serviceb));
    }

    /**
     * Test of clear method, of class SimpleServiceStore.
     * @throws java.lang.Exception 
     */
    public void testClear() throws Exception {
        log.info("clear");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        SimpleServiceStore<ServiceMockup> instance = new SimpleServiceStore<ServiceMockup>();
        Field f = SimpleServiceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        Map<String, ServiceMockup> storage = (Map<String, ServiceMockup>) f.get(instance);
        f.setAccessible(false);
        storage.put(servicea.getName(), servicea);
        storage.put(serviceb.getName(), serviceb);
        assertEquals("underlying storage should contain two elements", storage.size(), 2);
        assertTrue("underlying storage should contain service with name 'a'", storage.containsKey(servicea.getName()));
        assertTrue("underlying storage should contain service with name 'b'", storage.containsKey(serviceb.getName()));
        instance.clear();
        assertTrue("underlying storage should be empty", storage.isEmpty());
    }

    private class ServiceMockup implements Service {

        private boolean forceExceptions;
        private final String name;

        public ServiceMockup(String name, boolean forceExceptions) {
            super();
            this.name = name;
            this.forceExceptions = forceExceptions;
        }

        public void startService() {
            // do nothing
        }

        public void stopService(boolean isFreeResources)
                throws GrmException {
            // do nothing
        }

        public ServiceState getServiceState() {
            return ServiceState.RUNNING;
        }

        public void addServiceEventListener(ServiceEventListener eventListener) {
            // do nothing
        }

        public void removeServiceEventListener(
                ServiceEventListener eventListener) {
            // do nothing
        }        

        public List<Resource> getResources() {
            return Collections.<Resource>emptyList();
        }

        public Resource getResource(ResourceId resourceId)
                throws UnknownResourceException {
            return null;
        }
        
        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr)
                throws UnknownResourceException {
            // do nothing
        }

        public void resetResource(ResourceId resource)
                throws UnknownResourceException {
            // do nothing
        }

        public void addResource(Resource resource) {
            // do nothing
        }
        
        public void addResource(Resource resource,String sloName) {
            // do nothing
        }

        public void start() {
            // do nothing
        }

        public void stop(boolean isForced) {
            // do nothing
        }

        public void reload(boolean isForced) {
            // do nothing
        }

        public ComponentState getState() {
            return ComponentState.STARTED;
        }

        public void addComponentEventListener(ComponentEventListener componentEventListener) {
            // do nothing
        }

        public void removeComponentEventListener(ComponentEventListener componentEventListener) {
            // do nothing
        }

        public String getName() throws GrmRemoteException {
            if (forceExceptions) {
                throw new GrmRemoteException();
            }
            return name;
        }

        public void modifyResource(ResourceId resource, ResourceChangeOperation operations) {
            // do nothing
        }

        public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
            // do nothing
        }

        public List<SLOState> getSLOStates() {
            return Collections.<SLOState>emptyList();
        }

        public List<Resource> getResources(Filter filter) {
            return Collections.<Resource>emptyList();
        }

        public Hostname getHostname() throws GrmRemoteException {
            return Hostname.getLocalHost();
        }

        public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws ServiceNotActiveException, UnknownResourceException, GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
