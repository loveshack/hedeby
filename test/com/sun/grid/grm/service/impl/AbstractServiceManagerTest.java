/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventListener;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventSupport;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.ServiceStoreException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.ui.ConfigurationService;
import com.sun.grid.grm.ui.impl.ConfigurationServiceImpl;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Test for basic functionality of service manager
 */
public class AbstractServiceManagerTest extends CSMockupTestCase {

    private static ExecutionEnv env;
    private final static Logger log = Logger.getLogger(AbstractServiceManagerTest.class.getName());

    public AbstractServiceManagerTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        //do not call super.setUp() This test case only needs the setUpContext functionality,
        //but not the rest of the setup. The tests handle everything else whithin the their methods!
        env = DummyExecutionEnvFactory.newInstance();
        setUpContext(env);
        setSystemVersion(env);
    }

    @Override
    protected void tearDown() throws Exception {
        //Do *NOT* call super.tearDown(), because super.setUp() was not called
        clearContext(env);
        clearSystemVersion(env);
    }

    /**
     * Test of addService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testAddService() throws Exception {
        log.info("addService");
        ServiceMockup service = new ServiceMockup("a", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        boolean result = instance.addService(service);
        assertTrue("addService should return true", result);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains("a"));
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
    }

    /**
     * Test of addService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testAddServiceFail() throws Exception {
        log.info("addServiceFail");
        ServiceMockup service = new ServiceMockup("a", true);
        ServiceStoreMockup store = new ServiceStoreMockup();
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        try {
            instance.addService(service);
            fail("addService should throw an exception");
        } catch (InvalidServiceException ise) {
            assertTrue("addService should throw an exception", true);
        }
        assertTrue("underlying store should reamin empty", store.getServices().isEmpty());
    }

    /**
     * Test of addService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testAddServiceFailServiceAlreadyAdded() throws Exception {
        log.info("addServiceFailServiceAlreadyAdded");
        ServiceMockup service = new ServiceMockup("a", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(service);
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        boolean result = instance.addService(service);
        assertFalse("addService should return false", result);
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
    }

    /**
     * Test of addService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testAddServiceSSE() throws Exception {
        log.info("addServiceSSE");
        ServiceMockup service = new ServiceMockup("a", false);
        FaultyServiceStoreMockup store = new FaultyServiceStoreMockup();
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        boolean result = instance.addService(service);
        assertFalse("addService should return false", result);
    }

    /**
     * Test of removeService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testRemoveService() throws Exception {
        log.info("removeService");
        ServiceMockup service = new ServiceMockup("a", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(service);
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        boolean result = instance.removeService(service.getName());
        assertTrue("removeService should return true", result);
        assertFalse("underlying store should not contain service with name 'a'", store.getServiceNames().contains("a"));
        assertTrue("underlying store should be empty", store.getServices().isEmpty());
    }

    /**
     * Test of removeService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testRemoveServiceFail() throws Exception {
        log.info("removeServiceFail");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        try {
            instance.removeService(serviceb.getName());
            fail("removeService should throw an exception");
        } catch (UnknownServiceException use) {
            assertTrue("removeService should throw an exception", true);
        }
        assertTrue("underlying store should still contain service with name 'a'", store.getServiceNames().contains("a"));
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
    }

    /**
     * Test of removeService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testRemoveServiceSSEWhileGetService() throws Exception {
        log.info("removeServiceSSE");
        ServiceMockup service = new ServiceMockup("a", false);
        FaultyServiceStoreMockup store = new FaultyServiceStoreMockup();
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        try {
            instance.removeService(service.getName());
            fail("removeService should throw an exception");
        } catch (UnknownServiceException use) {
            assertTrue("removeService should throw an exception", true);
        }
    }

    /**
     * Test of removeService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testRemoveServiceSSEWhileRemoveService() throws Exception {
        log.info("removeServiceSSE");
        ServiceMockup service = new ServiceMockup("a", false);
        RemoveDisabledServiceStoreMockup store = new RemoveDisabledServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(service);
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        boolean result = instance.removeService(service.getName());
        assertFalse("remvoeService should return false", result);
        assertTrue("underlying store should still contain service with name 'a'", store.getServiceNames().contains("a"));
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
    }

    /**
     * Test of getService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testGetService() throws Exception {
        log.info("getService");
        ServiceMockup service = new ServiceMockup("a", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(service);
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        ServiceMockup result = instance.getService(service.getName());
        assertEquals("returned service is different than expected", result, service);
    }

    /**
     * Test of getService method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testGetServiceFail() throws Exception {
        log.info("getServiceFail");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        try {
            instance.getService(serviceb.getName());
            fail("getService should throw an exception");
        } catch (UnknownServiceException use) {
            assertTrue("getService should throw an exception", true);
        }
    }

    /**
     * Test of getServiceNames method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testGetServiceNames() throws Exception {
        log.info("getServiceNames");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        store.addService(serviceb);
        assertEquals("underlying store should contain exactly two elements", store.getServices().size(), 2);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        assertTrue("underlying store should contain service with name 'b'", store.getServiceNames().contains(serviceb.getName()));
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        Set<String> result = instance.getServiceNames();
        assertEquals("returned names should contain exactly two elements", result.size(), 2);
        assertTrue("returned names should contain name 'a'", result.contains(servicea.getName()));
        assertTrue("returned names should contain name 'b'", result.contains(serviceb.getName()));
    }

    /**
     * Test of getServiceNames method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testGetServiceNamesSSE() throws Exception {
        log.info("getServiceNamesSSE");
        FaultyServiceStoreMockup store = new FaultyServiceStoreMockup();
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        Set<String> result = instance.getServiceNames();
        assertTrue("returned names should be empty", result.isEmpty());
    }

    /**
     * Test of getServices method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testGetServices() throws Exception {
        log.info("getServices");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        store.addService(serviceb);
        assertEquals("underlying store should contain exactly two elements", store.getServices().size(), 2);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        assertTrue("underlying store should contain service with name 'b'", store.getServiceNames().contains(serviceb.getName()));
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        List<ServiceMockup> result = instance.getServices();
        assertEquals("returned services should contain exactly two elements", result.size(), 2);
        assertTrue("returned services should contain service with name 'a'", result.contains(servicea));
        assertTrue("returned services should contain service with name 'b'", result.contains(serviceb));
    }

    /**
     * Test of getServices method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testGetServiceSSE() throws Exception {
        log.info("getServicesSSE");
        FaultyServiceStoreMockup store = new FaultyServiceStoreMockup();
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        List<ServiceMockup> result = instance.getServices();
        assertTrue("returned services should be empty", result.isEmpty());
    }

    /**
     * Test of start method, of class AbstractServiceManager.
     * 
     * The test depends on a TestUtil.getPredefinedSystem() - to pass this test,
     * only one active component entry implementing Service.class with name "spare_pool" 
     * should be registered to CS Mockup.
     * 
     * @throws java.lang.Exception 
     */
    public void testStart() throws Exception {
        log.info("start");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        store.addService(serviceb);
        assertEquals("underlying store should contain exactly two elements", store.getServices().size(), 2);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        assertTrue("underlying store should contain service with name 'b'", store.getServiceNames().contains(serviceb.getName()));
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        Field f = AbstractServiceManager.class.getDeclaredField("cs");
        f.setAccessible(true);
        f.set(instance, new ConfigurationServiceImpl(env, "cs", "cs"));
        f.setAccessible(false);
        instance.start();
        assertEquals("underlying store should contain just one element", 1, store.getServices().size());
        assertTrue("underlying store should contain service with name 'spare_pool'", store.getServiceNames().contains("spare_pool"));
        f = AbstractServiceManager.class.getDeclaredField("started");
        f.setAccessible(true);
        boolean result = (Boolean) f.get(instance);
        f.setAccessible(false);
        assertTrue("manager should be started", result);
    }

    /**
     * Test of ConfigurationServiceEventListener, of class AbstractServiceManager.
     * 
     * // TODO more proper way would be if CS mockup support CS notifications
     * 
     * @throws java.lang.Exception 
     */
    public void testConfigurationObjectAdded() throws Exception {
        log.info("configurationObjectAdded");
        ServiceStoreMockup store = new ServiceStoreMockup();
        assertTrue("underlying store should be empty", store.getServiceNames().isEmpty());
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        ConfigurationService cs = new ConfigurationServiceImpl(env, "cs", "cs");
        Field f = AbstractServiceManager.class.getDeclaredField("cs");
        f.setAccessible(true);
        f.set(instance, cs);
        f.setAccessible(false);
        f = AbstractServiceManager.class.getDeclaredField("csEventListener");
        f.setAccessible(true);
        ConfigurationServiceEventListener csEventListener = (ConfigurationServiceEventListener) f.get(instance);
        f.setAccessible(false);
        cs.addConfigurationServiceEventListener(csEventListener);
        f = AbstractServiceManager.class.getDeclaredField("started");
        f.setAccessible(true);
        f.set(instance, Boolean.TRUE);
        f.setAccessible(false);
        f = ConfigurationServiceImpl.class.getDeclaredField("listeners");
        f.setAccessible(true);
        ConfigurationServiceEventSupport listeners = (ConfigurationServiceEventSupport) f.get(cs);
        f.setAccessible(false);
        listeners.fireConfigurationObjectAdded("a", "host", "active_component", "object added", Service.class.getName());
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains("a"));
    }

    /**
     * Test of ConfigurationServiceEventListener, of class AbstractServiceManager.
     * Test of ignoring the event if not started.
     * 
     * // TODO more proper way would be if CS mockup support CS notifications
     * 
     * @throws java.lang.Exception 
     */
    public void testConfigurationObjectAddedIgnored() throws Exception {
        log.info("configurationObjectAddedIgnored");
        ServiceStoreMockup store = new ServiceStoreMockup();
        assertTrue("underlying store should be empty", store.getServiceNames().isEmpty());
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        ConfigurationService cs = new ConfigurationServiceImpl(env, "cs", "cs");
        Field f = AbstractServiceManager.class.getDeclaredField("cs");
        f.setAccessible(true);
        f.set(instance, cs);
        f.setAccessible(false);
        f = AbstractServiceManager.class.getDeclaredField("csEventListener");
        f.setAccessible(true);
        ConfigurationServiceEventListener csEventListener = (ConfigurationServiceEventListener) f.get(instance);
        f.setAccessible(false);
        cs.addConfigurationServiceEventListener(csEventListener);
        f = ConfigurationServiceImpl.class.getDeclaredField("listeners");
        f.setAccessible(true);
        ConfigurationServiceEventSupport listeners = (ConfigurationServiceEventSupport) f.get(cs);
        f.setAccessible(false);
        listeners.fireConfigurationObjectAdded("a", "host", "active_component", "object added", Service.class.getName());
        assertTrue("underlying store should still be empty", store.getServiceNames().isEmpty());
    }

    /**
     * Test of ConfigurationServiceEventListener, of class AbstractServiceManager.
     * 
     * @throws java.lang.Exception 
     */
    public void testConfigurationObjectRemoved() throws Exception {
        log.info("configurationObjectRemoved");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        store.addService(serviceb);
        assertEquals("underlying store should contain exactly two elements", store.getServices().size(), 2);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        assertTrue("underlying store should contain service with name 'b'", store.getServiceNames().contains(serviceb.getName()));
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        ConfigurationService cs = new ConfigurationServiceImpl(env, "cs", "cs");
        Field f = AbstractServiceManager.class.getDeclaredField("cs");
        f.setAccessible(true);
        f.set(instance, cs);
        f.setAccessible(false);
        f = AbstractServiceManager.class.getDeclaredField("csEventListener");
        f.setAccessible(true);
        ConfigurationServiceEventListener csEventListener = (ConfigurationServiceEventListener) f.get(instance);
        f.setAccessible(false);
        cs.addConfigurationServiceEventListener(csEventListener);
        f = AbstractServiceManager.class.getDeclaredField("started");
        f.setAccessible(true);
        f.set(instance, Boolean.TRUE);
        f.setAccessible(false);
        f = ConfigurationServiceImpl.class.getDeclaredField("listeners");
        f.setAccessible(true);
        ConfigurationServiceEventSupport listeners = (ConfigurationServiceEventSupport) f.get(cs);
        f.setAccessible(false);
        listeners.fireConfigurationObjectRemoved("b", "host", "active_component", "object removed", Service.class.getName());
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains("a"));
    }

    /**
     * Test of ConfigurationServiceEventListener, of class AbstractServiceManager.
     * Test of ignoring the event if not started.
     * 
     * @throws java.lang.Exception 
     */
    public void testConfigurationObjectRemovedIgnored() throws Exception {
        log.info("configurationObjectRemovedIgnored");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        store.addService(serviceb);
        assertEquals("underlying store should contain exactly two elements", store.getServices().size(), 2);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        assertTrue("underlying store should contain service with name 'b'", store.getServiceNames().contains(serviceb.getName()));
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        ConfigurationService cs = new ConfigurationServiceImpl(env, "cs", "cs");
        Field f = AbstractServiceManager.class.getDeclaredField("cs");
        f.setAccessible(true);
        f.set(instance, cs);
        f.setAccessible(false);
        f = AbstractServiceManager.class.getDeclaredField("csEventListener");
        f.setAccessible(true);
        ConfigurationServiceEventListener csEventListener = (ConfigurationServiceEventListener) f.get(instance);
        f.setAccessible(false);
        cs.addConfigurationServiceEventListener(csEventListener);
        f = ConfigurationServiceImpl.class.getDeclaredField("listeners");
        f.setAccessible(true);
        ConfigurationServiceEventSupport listeners = (ConfigurationServiceEventSupport) f.get(cs);
        f.setAccessible(false);
        listeners.fireConfigurationObjectRemoved("b", "host", "active_component", "object removed", Service.class.getName());
        assertEquals("underlying store should still contain two elements", store.getServices().size(), 2);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        assertTrue("underlying store should contain service with name 'b'", store.getServiceNames().contains(serviceb.getName()));
    }

    /**
     * Test of ConfigurationServiceEventListener, of class AbstractServiceManager.
     * 
     * @throws java.lang.Exception 
     */
    public void testConfigurationObjectChanged() throws Exception {
        log.info("configurationObjectRemoved");
        ServiceMockup servicea = new ServiceMockup("a", false);
        servicea.setNotCreatedByFactory();
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        assertEquals("underlying store should contain exactly one element", store.getServices().size(), 1);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        assertFalse("service with name 'a' should not be created by factory method", servicea.isCreatedByFactory());
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        ConfigurationService cs = new ConfigurationServiceImpl(env, "cs", "cs");
        Field f = AbstractServiceManager.class.getDeclaredField("cs");
        f.setAccessible(true);
        f.set(instance, cs);
        f.setAccessible(false);
        f = AbstractServiceManager.class.getDeclaredField("csEventListener");
        f.setAccessible(true);
        ConfigurationServiceEventListener csEventListener = (ConfigurationServiceEventListener) f.get(instance);
        f.setAccessible(false);
        cs.addConfigurationServiceEventListener(csEventListener);
        f = AbstractServiceManager.class.getDeclaredField("started");
        f.setAccessible(true);
        f.set(instance, Boolean.TRUE);
        f.setAccessible(false);
        f = ConfigurationServiceImpl.class.getDeclaredField("listeners");
        f.setAccessible(true);
        ConfigurationServiceEventSupport listeners = (ConfigurationServiceEventSupport) f.get(cs);
        f.setAccessible(false);
        listeners.fireConfigurationObjectChanged("a", "host", "active_component", "object changed", Service.class.getName());
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains("a"));
        ServiceMockup result = store.getService(servicea.getName());
        assertTrue("service with name 'a' should be created by factory method", result.isCreatedByFactory());
    }

    /**
     * Test of ConfigurationServiceEventListener, of class AbstractServiceManager.
     * Test of ignoring the event if not started.
     * 
     * @throws java.lang.Exception 
     */
    public void testConfigurationObjectChangedIgnored() throws Exception {
        log.info("configurationObjectRemoved");
        ServiceMockup servicea = new ServiceMockup("a", false);
        servicea.setNotCreatedByFactory();
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        assertEquals("underlying store should contain exactly one element", store.getServices().size(), 1);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        assertFalse("service with name 'a' should not be created by factory method", servicea.isCreatedByFactory());
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        ConfigurationService cs = new ConfigurationServiceImpl(env, "cs", "cs");
        Field f = AbstractServiceManager.class.getDeclaredField("cs");
        f.setAccessible(true);
        f.set(instance, cs);
        f.setAccessible(false);
        f = AbstractServiceManager.class.getDeclaredField("csEventListener");
        f.setAccessible(true);
        ConfigurationServiceEventListener csEventListener = (ConfigurationServiceEventListener) f.get(instance);
        f.setAccessible(false);
        cs.addConfigurationServiceEventListener(csEventListener);
        f = ConfigurationServiceImpl.class.getDeclaredField("listeners");
        f.setAccessible(true);
        ConfigurationServiceEventSupport listeners = (ConfigurationServiceEventSupport) f.get(cs);
        f.setAccessible(false);
        listeners.fireConfigurationObjectChanged("a", "host", "active_component", "object changed", Service.class.getName());
        assertEquals("underlying store should contain just one element", store.getServices().size(), 1);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains("a"));
        ServiceMockup result = store.getService(servicea.getName());
        assertFalse("service with name 'a' should not be created by factory method", result.isCreatedByFactory());
    }

    /**
     * Test of stop method, of class AbstractServiceManager.
     * @throws java.lang.Exception 
     */
    public void testStop() throws Exception {
        log.info("stop");
        ServiceMockup servicea = new ServiceMockup("a", false);
        ServiceMockup serviceb = new ServiceMockup("b", false);
        ServiceStoreMockup store = new ServiceStoreMockup();
        /* service can be added to store like this - we consider store mockup 100% reliable */
        store.addService(servicea);
        store.addService(serviceb);
        assertEquals("underlying store should contain exactly two elements", store.getServices().size(), 2);
        assertTrue("underlying store should contain service with name 'a'", store.getServiceNames().contains(servicea.getName()));
        assertTrue("underlying store should contain service with name 'b'", store.getServiceNames().contains(serviceb.getName()));
        AbstractServiceManager<ServiceMockup> instance = new ServiceManagerMockup(env, store, false);
        Field f = AbstractServiceManager.class.getDeclaredField("cs");
        f.setAccessible(true);
        f.set(instance, new ConfigurationServiceImpl(env, "cs", "cs"));
        f.setAccessible(false);
        f = AbstractServiceManager.class.getDeclaredField("started");
        f.setAccessible(true);
        f.set(instance, Boolean.TRUE);
        f.setAccessible(false);
        instance.stop();
        assertTrue("underlying store should be empty", store.getServiceNames().isEmpty());
        f = AbstractServiceManager.class.getDeclaredField("started");
        f.setAccessible(true);
        boolean result = (Boolean) f.get(instance);
        f.setAccessible(false);
        assertFalse("manager should be stopped", result);
    }

    private class ServiceManagerMockup extends AbstractServiceManager<ServiceMockup> {

        private boolean forceExceptions;

        public ServiceManagerMockup(ExecutionEnv env, ServiceStore<ServiceMockup> serviceStore, boolean forceExceptions) throws GrmException {
            super(env, serviceStore);
            this.forceExceptions = forceExceptions;
        }

        @Override
        protected ServiceMockup createManagedService(String s) throws InvalidServiceException {
            return new ServiceMockup(s, forceExceptions);
        }
    }

    private class ServiceMockup implements Service {

        private boolean forceExceptions;
        private final String name;
        private boolean createdByFactory;

        public ServiceMockup(String name, boolean forceExceptions) {
            super();
            this.name = name;
            this.forceExceptions = forceExceptions;
            this.createdByFactory = true;
        }

        public boolean isCreatedByFactory() {
            return createdByFactory;
        }

        public void setNotCreatedByFactory() {
            createdByFactory = false;
        }

        public void startService() {
            // do nothing
        }

        public void stopService(boolean isFreeResources)
                throws GrmException {
            // do nothing
        }

        public ServiceState getServiceState() {
            return ServiceState.RUNNING;
        }

        public void addServiceEventListener(ServiceEventListener eventListener) {
            // do nothing
        }

        public void removeServiceEventListener(
                ServiceEventListener eventListener) {
            // do nothing
        }

        public List<Resource> getResources() {
            return Collections.<Resource>emptyList();
        }

        public Resource getResource(ResourceId resourceId)
                throws UnknownResourceException {
            return null;
        }

        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr)
                throws UnknownResourceException {
            // do nothing
        }

        public void resetResource(ResourceId resource)
                throws UnknownResourceException {
            // do nothing
        }

        public void addResource(Resource resource) {
            // do nothing
        }

        /**
         * This method was added because of an interface change for issue 607
         * However the implementation does nothing in alignment with the other methods
         * @param resource a resource to be added
         * @param sloName if the resource is added because of a SLO request it contains the SLO name
         */
        public void addResource(Resource resource, String sloName) {
            // do nothing
        }

        public void start() {
            // do nothing
        }

        public void stop(boolean isForced) {
            // do nothing
        }

        public void reload(boolean isForced) {
            // do nothing
        }

        public ComponentState getState() {
            return ComponentState.STARTED;
        }

        public void addComponentEventListener(
                ComponentEventListener stateChangedLister) {
            // do nothing
        }

        public void removeComponentEventListener(
                ComponentEventListener stateChangedListener) {
            // do nothing
        }

        public String getName() throws GrmRemoteException {
            if (forceExceptions) {
                throw new GrmRemoteException();
            }
            return name;
        }

        public void modifyResource(ResourceId resource, ResourceChangeOperation operations) {
            // do nothing
        }

        public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
            // do nothing
        }

        public List<SLOState> getSLOStates() {
            return Collections.<SLOState>emptyList();
        }

        public List<Resource> getResources(Filter filter) {
            return Collections.<Resource>emptyList();
        }

        public Hostname getHostname() throws GrmRemoteException {
            return Hostname.getLocalHost();
        }

        public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws ServiceNotActiveException, UnknownResourceException, GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private class ServiceStoreMockup implements ServiceStore<ServiceMockup> {

        private final Map<String, ServiceMockup> services = new HashMap<String, ServiceMockup>();

        public ServiceMockup addService(ServiceMockup service) throws ServiceStoreException {
            try {
                return services.put(service.getName(), service);
            } catch (GrmRemoteException gre) {
                throw new ServiceStoreException();
            }
        }

        public ServiceMockup removeService(String serviceName)
                throws UnknownServiceException {
            ServiceMockup s = services.remove(serviceName);
            if (s == null) {
                throw new UnknownServiceException();
            } else {
                return s;
            }
        }

        public Set<String> getServiceNames() {
            return services.keySet();
        }

        public ServiceMockup getService(String serviceName)
                throws UnknownServiceException {
            ServiceMockup s = services.get(serviceName);
            if (s == null) {
                throw new UnknownServiceException();
            } else {
                return s;
            }
        }

        public List<ServiceMockup> getServices() {
            return new LinkedList<ServiceMockup>(services.values());
        }

        public void clear() {
            services.clear();
        }
    }

    private class FaultyServiceStoreMockup implements ServiceStore<ServiceMockup> {

        public void clear() throws ServiceStoreException {
            throw new ServiceStoreException();
        }

        public AbstractServiceManagerTest.ServiceMockup addService(AbstractServiceManagerTest.ServiceMockup service) throws ServiceStoreException {
            throw new ServiceStoreException();
        }

        public AbstractServiceManagerTest.ServiceMockup removeService(String serviceName) throws ServiceStoreException {
            throw new ServiceStoreException();
        }

        public Set<String> getServiceNames() throws ServiceStoreException {
            throw new ServiceStoreException();
        }

        public AbstractServiceManagerTest.ServiceMockup getService(String serviceName) throws ServiceStoreException {
            throw new ServiceStoreException();
        }

        public List<AbstractServiceManagerTest.ServiceMockup> getServices() throws ServiceStoreException {
            throw new ServiceStoreException();
        }
    }

    private class RemoveDisabledServiceStoreMockup implements ServiceStore<ServiceMockup> {

        private final Map<String, ServiceMockup> services = new HashMap<String, ServiceMockup>();

        public ServiceMockup addService(ServiceMockup service) throws ServiceStoreException {
            try {
                return services.put(service.getName(), service);
            } catch (GrmRemoteException gre) {
                throw new ServiceStoreException();
            }
        }

        public ServiceMockup removeService(String serviceName)
                throws ServiceStoreException {
            throw new ServiceStoreException();
        }

        public Set<String> getServiceNames() {
            return services.keySet();
        }

        public ServiceMockup getService(String serviceName)
                throws UnknownServiceException {
            ServiceMockup s = services.get(serviceName);
            if (s == null) {
                throw new UnknownServiceException();
            } else {
                return s;
            }
        }

        public List<ServiceMockup> getServices() {
            return new LinkedList<ServiceMockup>(services.values());
        }

        public void clear() {
            services.clear();
        }
    }
}
