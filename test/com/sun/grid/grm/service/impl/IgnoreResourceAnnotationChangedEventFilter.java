/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.impl.ResourceAnnotationChanged;
import com.sun.grid.grm.service.event.AbstractServiceEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.impl.GenericOrderWatchingResourceSEL.EventFilter;
import java.util.Collection;

/**
 * This EventFilter does not accept <code>ResourceChangedEvent</code>s
 * with exactly one instance of <code>ResourceAnnotationChanged</code> in the
 * changes.
 */
class IgnoreResourceAnnotationChangedEventFilter implements EventFilter {

    private final static IgnoreResourceAnnotationChangedEventFilter instance = new IgnoreResourceAnnotationChangedEventFilter();
    
    private IgnoreResourceAnnotationChangedEventFilter() {
    }

    /**
     * Get the singleton instance of of this class
     * @return the singleton instance
     */
    public static IgnoreResourceAnnotationChangedEventFilter getInstance() {
        return instance;
    }
    
    /**
     * Is <code>evt</code> accepted?
     * 
     * @param evt the event
     * @return <code>false</code> if <code>evt</code> is an instance of ResourceChangedEvent
     *         and the changes contains only one element which describes that the
     *         annotation of the resource has been changed.
     *         otherwise <code>true</code>
     */
    public final boolean accept(AbstractServiceEvent evt) {
        if (evt instanceof ResourceChangedEvent) {
            Collection<ResourceChanged> changes = ((ResourceChangedEvent) evt).getProperties();
            if (changes.size() == 1 && changes.iterator().next() instanceof ResourceAnnotationChanged) {
                return false;
            }
        }
        return true;
    }
}
