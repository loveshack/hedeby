/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.RequestQueue;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdAndName;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.ui.resource.ResourceActionResult;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;


/**
 * ResourceManager mockup offering possiblity to register/unregister resource id.
 * It is used for SCP event protokol junit tests.
 */
class ResourceManagerMockup implements ResourceManager {

    private final boolean regResult;
    private AtomicReference<ResourceId> registeredId = new AtomicReference<ResourceId>();

    public ResourceManagerMockup(boolean regResult) {
        super();
        this.regResult = regResult;
    }

    public void addResource(Resource resource) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<ResourceActionResult> removeResources(List<String> resIdOrNameList, ResourceRemovalDescriptor descr) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean removeResource(ResourceId resourceId, String service, ResourceRemovalDescriptor descr) throws UnknownResourceException, UnknownServiceException, ServiceNotActiveException, InvalidResourceException, InvalidServiceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean resetResource(ResourceId resource) throws UnknownResourceException, InvalidResourceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean resetResource(ResourceId resource, String service) throws UnknownResourceException, InvalidResourceException, UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean modifyResource(ResourceId resource, ResourceChangeOperation operations) throws UnknownResourceException, InvalidResourceException, UnknownServiceException, ServiceNotActiveException, InvalidServiceException, InvalidResourcePropertiesException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Resource getResource(ResourceId id) throws UnknownResourceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Resource getResource(String serviceName, ResourceId resourceId) throws UnknownServiceException, ServiceNotActiveException, UnknownResourceException, InvalidServiceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Resource> getResources(String service) throws UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean addResource(String service, Resource resource) throws InvalidResourceException, UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean moveResource(String service, ResourceId resourceId, boolean isForced, boolean isStatic) throws UnknownResourceException, UnknownServiceException, ServiceNotActiveException, InvalidResourceException, InvalidServiceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean addResourceToProcess(String source, Resource resource) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean removeResourceFromProcess(String source, ResourceId resource) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Resource> getResourcesInProcess() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addResourceProviderEventListener(ResourceProviderEventListener lis) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeResourceProviderEventListener(ResourceProviderEventListener lis) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processResourceRequestEvent(ResourceRequestEvent event) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processResourceAddedEvent(String service, Resource added) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processResourceChangedEvent(String service, Resource modified, Collection<ResourceChanged> changed) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processResourceErrorEvent(String serviceName, Resource bad, String message) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processResourceRemovedEvent(String serviceName, Resource released) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processAddResourceEvent(String serviceName, Resource resource) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processRemoveResourceEvent(String serviceName, Resource resource) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processResourceRejectedEvent(String serviceName, Resource rejected) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processResourceResetEvent(String serviceName, Resource reset) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void processOrphanedResources() {
    }

    public int putResourceIDOnBlackList(ResourceId resourceId, String service) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<ResourceIdAndName> getBlackList(String service) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int removeResourceIDFromBlackList(ResourceId resourceId, String service) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isResourceIDOnBlackList(ResourceId resourceId, String service) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void start() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void stop() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setRequestQueue(RequestQueue rq) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void triggerRequestQueueReprocessing() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Resource> getResources(String service, Filter filter) throws UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Resource> getResourcesInProcess(Filter filter) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ResourceId getRegisteredId() {
        return registeredId.get();
    }

    public void setRegisteredId(ResourceId registeredId) {
        this.registeredId.set(registeredId);
    }

    public void processPurgeOrdersAndRequestsEvent(String source) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<ResourceActionResult> resetResources(List<String> idOrNameList) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ResourceActionResult modifyResource(ResourceId resId, Map<String, Object> properties) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Resource getResourceByIdOrName(String idOrName) throws ServiceNotActiveException, InvalidResourceException, UnknownResourceException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<ResourceActionResult> moveResources(List<String> resIdOrName, String service, boolean isFored, boolean isStatic) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<ResourceActionResult> putResourcesOnBlackList(List<String> idOrNameList, String service) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<ResourceActionResult> removeResourcesFromBlackList(List<String> idOrNameList, String service) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<ResourceActionResult> modifyResources(Map<String, Map<String, Object>> resIdOrNameMapWithProperties) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public DefaultService getDefaultService() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Resource> getUnassignedResources(Filter<Resource> filter) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
