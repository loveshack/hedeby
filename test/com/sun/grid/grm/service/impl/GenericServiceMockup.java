/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.event.ComponentEventAdapter;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Service mockup needed for SCP event protocol tests.
 */
class GenericServiceMockup implements Service {

    protected final Map<ResourceId, Resource> resources;
    protected final Hostname hostname;
    protected final String name;
    protected ServiceEventListener sel;
    protected final ServiceEventListener nullServiceEventListener = new ServiceEventAdapter();
    protected ComponentEventListener cel;
    protected final ComponentEventListener nullComponentEventListener = new ComponentEventAdapter();
    protected volatile ComponentState cmpState;
    protected volatile ServiceState svcState;
    protected AtomicLong svcEventId = new AtomicLong(0);

    public GenericServiceMockup(String name, String host) {
        super();
        this.resources = new HashMap<ResourceId, Resource>();
        this.hostname = Hostname.getInstance(host);
        this.name = name;
        this.cmpState = ComponentState.UNKNOWN;
        this.svcState = ServiceState.UNKNOWN;
        this.sel = nullServiceEventListener;
        this.cel = nullComponentEventListener;
    }
    
    public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
        return new DefaultServiceSnapshot(svcEventId.get(), cmpState, svcState, 
                new HashSet<Resource>(resources.values()), 
                Collections.<String,Collection<Need>>emptyMap());
    }
    

    public void startService() throws GrmException {
        setServiceState(ServiceState.STARTING);
        setServiceState(ServiceState.RUNNING);
    }

    public void stopService(boolean isFreeResources) throws GrmException {
        setServiceState(ServiceState.SHUTDOWN);
        setServiceState(ServiceState.STOPPED);
    }
    
    public long getCurrentEventId() {
        return svcEventId.get();
    }

    public void setServiceState(ServiceState state) {
        this.svcState = state;
        ServiceStateChangedEvent ssce = new ServiceStateChangedEvent(svcEventId.getAndIncrement(), name, svcState);
        switch (svcState) {
            case ERROR:
                sel.serviceError(ssce);
                break;
            case RUNNING:
                sel.serviceRunning(ssce);
                break;
            case SHUTDOWN:
                sel.serviceShutdown(ssce);
                break;
            case STARTING:
                sel.serviceStarting(ssce);
                break;
            case STOPPED:
                sel.serviceStopped(ssce);
                break;
            case UNKNOWN:
                sel.serviceUnknown(ssce);
                break;
            default:
                throw new IllegalStateException();
        }
    }

    public ServiceState getServiceState() {
        return svcState;
    }

    public List<SLOState> getSLOStates() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addServiceEventListener(ServiceEventListener eventListener) {
        sel = eventListener;
    }

    public void removeServiceEventListener(ServiceEventListener eventListener) {
        sel = nullServiceEventListener;
    }

    public List<Resource> getResources(Filter filter) {
        return new LinkedList<Resource>(resources.values());
    }

    public List<Resource> getResources() {
        return new LinkedList<Resource>(resources.values());
    }

    public Resource getResource(ResourceId resourceId) throws UnknownResourceException {
        Resource r = resources.get(resourceId);
        if (r == null) {
            throw new UnknownResourceException();
        }
        return r;
    }

    public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException {
        Resource r = resources.get(resourceId);
        if (r == null) {
            throw new UnknownResourceException();
        }
        RemoveResourceEvent rre = new RemoveResourceEvent(svcEventId.getAndIncrement(), name, r, "remove");
        sel.removeResource(rre);
        resources.remove(resourceId);
        ResourceRemovedEvent rre1 = new ResourceRemovedEvent(svcEventId.getAndIncrement(), name, r, "removed");
        sel.resourceRemoved(rre1);
    }

    public void resetResource(ResourceId resource) throws UnknownResourceException {
        Resource r = resources.get(resource);
        if (r == null) {
            throw new UnknownResourceException();
        }
        r.setState(Resource.State.ASSIGNED);
        ResourceResetEvent rre = new ResourceResetEvent(svcEventId.getAndIncrement(), name, r, "reset");
        resources.put(r.getId(), r);
        sel.resourceReset(rre);
    }
    
    /**
     * For this mockup service there is no use of the sloName. It is dismissed
     * by calling addResource(Resource)
     * @param resource added resource
     * @param sloName if the resource is added because of a SLO request it contains the SLO name
     */
    public void addResource(Resource resource, String sloName){
        addResource(resource);
    }

    public void addResource(Resource resource) {
        AddResourceEvent are = new AddResourceEvent(svcEventId.getAndIncrement(), name, resource, "add");
        sel.addResource(are);
        resources.put(resource.getId(), resource);
        ResourceAddedEvent rae = new ResourceAddedEvent(svcEventId.getAndIncrement(), name, resource, "added");
        sel.resourceAdded(rae);
    }

    public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
        throw new UnsupportedOperationException("Not supported yet.");
    }



    public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, InvalidResourcePropertiesException {
        Resource r = resources.get(resource);
        if (r == null) {
            throw new UnknownResourceException();
        }
        Collection<ResourceChanged> changed = r.modify(operations);
        ResourceChangedEvent rcpe = new ResourceChangedEvent(svcEventId.getAndIncrement(), name, r, changed, "modify");
        resources.put(r.getId(), r);
        sel.resourceChanged(rcpe);
    }

    public void modifyResource(ResourceId resource, ResourceChangeOperation operation) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
        modifyResource(resource, Collections.singleton(operation));
    }

    public void start() {
        setState(ComponentState.STARTING);
        setState(ComponentState.STARTED);
    }

    public void stop(boolean isForced) {
        setState(ComponentState.STOPPING);
        setState(ComponentState.STOPPED);
    }

    public void reload(boolean isForced) {
        if (isForced) {
            stop(false);
            start();
        } else {
            setState(ComponentState.RELOADING);
            setState(ComponentState.STARTED);
        }
    }

    public void setState(ComponentState state) {
        this.cmpState = state;
        ComponentStateChangedEvent csce = new ComponentStateChangedEvent(svcEventId.getAndIncrement(), name, hostname, cmpState);
        switch (cmpState) {
            case RELOADING:
                cel.componentReloading(csce);
                break;
            case STARTED:
                cel.componentStarted(csce);
                break;
            case STARTING:
                cel.componentStarting(csce);
                break;
            case STOPPED:
                cel.componentStopped(csce);
                break;
            case STOPPING:
                cel.componentStopping(csce);
                break;
            case UNKNOWN:
                cel.componentUnknown(csce);
                break;
            default:
                throw new IllegalStateException();
        }
    }

    public ComponentState getState() {
        return cmpState;
    }

    public void addComponentEventListener(ComponentEventListener componentEventListener) {
        cel = componentEventListener;
    }

    public void removeComponentEventListener(ComponentEventListener componentEventListener) {
        cel = nullComponentEventListener;
    }

    public String getName() {
        return name;
    }

    public Hostname getHostname() {
        return hostname;
    }

    public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws UnknownResourceException {
        Resource r = resources.get(resource);
        if (r == null) {
            throw new UnknownResourceException();
        }
        r.setAmbiguous(ambiguous);
        resources.put(r.getId(), r);
    }

}
