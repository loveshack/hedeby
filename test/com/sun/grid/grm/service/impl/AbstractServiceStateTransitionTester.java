/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentNotActiveException;
import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.ComponentTestAdapter;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.ComponentTester;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.AbstractServiceEvent;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import com.sun.grid.grm.util.InvalidStateTransistionException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract base class for testing service state transitions.
 * 
 * <p>This class adds additional tests for checking also the valid handlng of the service state
 * transitions.</p>
 * 
 * The method method follows a naming convention:
 * <pre>
 *     public void test&lt;Initial component state&gt;&lt;Initial service state&gt;_&lt;action&gt;()
 * </pre>
 * 
 * <pre>
 *     CSTATE   STATE         Description
 *     started  starting   => tested  in testStartedStarting_...
 *     started  running    => tested  in testStartedRunning_...
 *     started  shutdown   => tested  in testStartedShutdown_...
 *     started  unknown    => tested  in testStartedUnknown_...
 *     starting starting   => state can not be reached
 *     starting running    => state can not be reached
 *     starting shutdown   => can not test it because AbstractServiceAdapter provides no functionality to block
 *                            the component startup before service is going into STARTING state
 *     starting unknown    => can not test it because AbstractServiceAdapter provides no functionality to block
 *                            the component startup before service is going into STARTING state
 *     stopped  starting   => state can not be reached
 *     stopped  running    => state can not be reached
 *     stopped  shutdown   => state can not be reached
 *     stopped  unknown    => tested in testStoppedUnknown
 *     stopping starting   => state can not be reached
 *     stopping running    => can not test it because AbstractServiceAdapter provides no functionality to block
 *                            the component shutdown before service is going into SHUTDOWN state
 *     stopping shutdown   => can not test it because AbstractServiceAdapter provides no functionality to block
 *                            the component shutdown
 *     stopping unknown    => can not test it because AbstractServiceAdapter provides no functionality to block
 *                            the component shutdown
 * </pre>
 * @param <T> the type of tested service
 */
public abstract class AbstractServiceStateTransitionTester<T extends Service> extends AbstractComponentStateTransitionTester<T> {

    private final static Logger log = Logger.getLogger(AbstractServiceStateTransitionTester.class.getName());
    private ServiceTesterProxy<T> service;

    public AbstractServiceStateTransitionTester(String name) {
        super(name);
    }

    @Override
    protected ComponentTester<T> createComponentTester(ComponentTestAdapter<T> adapter) throws Exception {
        ServiceTester<T> ret = new ServiceTester<T>(adapter);
        this.service = new ServiceTesterProxy<T>(ret);
        return ret;
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testStoppedUnknown_Start() {
        log.entering(getClass().getName(), "testStoppedUnknown_Start");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED);
        log.exiting(getClass().getName(), "testStoppedUnknown_Start");
    }
    
    public void testStoppedUnknown_Reload() {
        log.entering(getClass().getName(), "testStoppedUnknown_Reload");
        service.reload(false).expect(InvalidStateTransistionException.class)
                  .assertStates(ComponentState.STOPPED, ServiceState.UNKNOWN);
        log.exiting(getClass().getName(), "testStoppedUnknown_Reload");
    }
    
    public void testStoppedUnknown_Stop() {
        log.entering(getClass().getName(), "testStoppedUnknown_Stop");
        service.stop(false).expect(InvalidStateTransistionException.class)
                .assertStates(ComponentState.STOPPED, ServiceState.UNKNOWN);
        log.exiting(getClass().getName(), "testStoppedUnknown_Stop");
    }
    
    public void testStoppedUnknown_StopForced() {
        log.entering(getClass().getName(), "testStoppedUnknown_StopForced");
        service.stop(true).expect(InvalidStateTransistionException.class)
                .assertStates(ComponentState.STOPPED, ServiceState.UNKNOWN);
        log.exiting(getClass().getName(), "testStoppedUnknown_StopForced");
    }

    public void testStoppedUnknown_StartService() {
        log.entering(getClass().getName(), "testStoppedUnknown_StartService");
        service.startService().expect(ComponentNotActiveException.class)
                              .assertStates(ComponentState.STOPPED, ServiceState.UNKNOWN);
        log.exiting(getClass().getName(), "testStoppedUnknown_StartService");
    }
    
    public void testStartedStoppingStopped_StartService() {
        log.entering(getClass().getName(), "testStartedStoppingStopped_StartService");
        
        service.start().expect(ComponentState.STARTING)
                       .expect(ServiceState.STARTING,ServiceState.RUNNING)
                       .expect(ComponentState.STARTED)
               .stop(false).expect(ComponentState.STOPPING)
                       .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                       .expect(ComponentState.STOPPED)
               .startService().expect(ComponentNotActiveException.class)
               .assertStates(ComponentState.STOPPED, ServiceState.UNKNOWN);
        log.exiting(getClass().getName(), "testStartedStoppingStopped_StartService");
    }

    public void testStoppedUnknown_StopService() {
        log.entering(getClass().getName(), "testStoppedUnknown_StopService");
        
        service.stopService(false).expect(ComponentNotActiveException.class)
                                  .assertStates(ComponentState.STOPPED, ServiceState.UNKNOWN);
        
        log.exiting(getClass().getName(), "testStoppedUnknown_StopService");
    }
    
    public void testStartedStoppingStopped_StopService() {
        log.entering(getClass().getName(), "testStartedStoppingStopped_StopService");
        
        service.start().expect(ComponentState.STARTING)
                       .expect(ServiceState.STARTING,ServiceState.RUNNING)
                       .expect(ComponentState.STARTED)
               .stop(false).expect(ComponentState.STOPPING)
                       .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                       .expect(ComponentState.STOPPED)
               .stopService(false).expect(ComponentNotActiveException.class)
                       .assertStates(ComponentState.STOPPED, ServiceState.UNKNOWN);

        
        log.exiting(getClass().getName(), "testStartedStoppingStopped_StopService");
    }

    public void testStartedRunning_Start() {
        log.entering(getClass().getName(), "testStartedRunning_Start");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .start().expect(InvalidStateTransistionException.class)
                       .assertStates(ComponentState.STARTED, ServiceState.RUNNING);
        log.exiting(getClass().getName(), "testStartedRunning_Start");
    }
    
    public void testStartedRunning_Reload() {
        log.entering(getClass().getName(), "testStartedRunning_Reload");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .reload(false)
               .expect(ComponentState.RELOADING)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN, ServiceState.RUNNING)
               .expect(ComponentState.STARTED);
        log.exiting(getClass().getName(), "testStartedRunning_Reload");
    }
    
    public void testStartedRunning_Stop() {
        log.entering(getClass().getName(), "testStartedRunning_Stop");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stop(false)
               .expect(ComponentState.STOPPING)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
               .expect(ComponentState.STOPPED);
        log.exiting(getClass().getName(), "testStartedRunning_Stop");
    }
    
    public void testStartedRunning_StopForced() {
        log.entering(getClass().getName(), "testStartedRunning_StopForced");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stop(true)
               .expect(ComponentState.STOPPING)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
               .expect(ComponentState.STOPPED);
        log.exiting(getClass().getName(), "testStartedRunning_StopForced");
    }

    public void testStartedRunning_StartService() {
        log.entering(getClass().getName(), "testStartedRunning_StartService");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .startService().expect(InvalidStateTransistionException.class)
               .assertStates(ComponentState.STARTED, ServiceState.RUNNING);
        log.exiting(getClass().getName(), "testStartedRunning_StartService");
    }


    public void testStartedRunning_StopService() {
        log.entering(getClass().getName(), "testStartedRunning_StartService");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stopService(false)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN);
        log.exiting(getClass().getName(), "testStartedRunning_StartService");
    }
    
    public void testStartedRunning_Reconnect() {
        log.entering(getClass().getName(), "testStartedRunning_Reconnect");
        if (service.isSupported(ServiceFeature.RECONNECT)) {
            service.start()
                   .expect(ComponentState.STARTING)
                   .expect(ServiceState.STARTING, ServiceState.RUNNING)
                   .expect(ComponentState.STARTED)
                   .setup(ServiceFeature.RECONNECT)
                   .expect(ServiceState.UNKNOWN)
                   .teardown(ServiceFeature.RECONNECT)
                   .expect(ServiceState.RUNNING);
        }
        log.exiting(getClass().getName(), "testStartedRunning_Reconnect");
    }
    

    public void testStartedStarting_Start() {
        log.entering(getClass().getName(), "testStartedStarting_Start");
        
        if (service.isSupported(ServiceFeature.BLOCK_START_SERVICE)) {
            service.start().expect(ComponentState.STARTING)
                   .expect(ServiceState.STARTING, ServiceState.RUNNING)
                   .expect(ComponentState.STARTED)
                   .stopService(false)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                   .setup(ServiceFeature.BLOCK_START_SERVICE)
                   .startService()
                   .expect(ServiceState.STARTING)
                   .start()
                   .expect(InvalidStateTransistionException.class)
                   .assertStates(ComponentState.STARTED, ServiceState.STARTING)
                   .teardown(ServiceFeature.BLOCK_START_SERVICE)
                   .expect(ServiceState.RUNNING)
                   .stop(false)
                   .expect(ComponentState.STOPPING)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                   .expect(ComponentState.STOPPED);
        }
        
        log.exiting(getClass().getName(), "testStartedStarting_Start");
    }
    
    public void testStartedStarting_Stop() {
        log.entering(getClass().getName(), "testStartedStarting_Stop");
        
        if (service.isSupported(ServiceFeature.BLOCK_START_SERVICE)) {
            service.start().expect(ComponentState.STARTING)
                   .expect(ServiceState.STARTING, ServiceState.RUNNING)
                   .expect(ComponentState.STARTED)
                   .stopService(false)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                   .setup(ServiceFeature.BLOCK_START_SERVICE)
                   .startService()
                   .expect(ServiceState.STARTING)
                   .stop(false)
                   .expect(InvalidStateTransistionException.class)
                   .assertStates(ComponentState.STARTED, ServiceState.STARTING)
                   .teardown(ServiceFeature.BLOCK_START_SERVICE)
                   .expect(ServiceState.RUNNING)
                   .stop(false)
                   .expect(ComponentState.STOPPING)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                   .expect(ComponentState.STOPPED);
        }
        
        log.exiting(getClass().getName(), "testStartedStarting_Stop");
    }
    
    public void testStartedStarting_StopForced() throws Exception {
        log.entering(getClass().getName(), "testStartedStarting_StopForced");
        
        if (service.isSupported(ServiceFeature.BLOCK_START_SERVICE)) {
            service.start().expect(ComponentState.STARTING)
                   .expect(ServiceState.STARTING, ServiceState.RUNNING)
                   .expect(ComponentState.STARTED)
                   .stopService(false)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                   .setup(ServiceFeature.BLOCK_START_SERVICE)
                   .startService()
                   .expect(ServiceState.STARTING)
                   .waitUntil(ServiceFeature.BLOCK_START_SERVICE);
            service.stop(true)
                   .expect(ComponentState.STOPPING)
                   .expect(ServiceState.ERROR)
                   .expect(ComponentState.STOPPED);
        }
        log.exiting(getClass().getName(), "testStartedStarting_StopForced");
    }
    
    public void testStartedStarting_Reload() {
        log.entering(getClass().getName(), "testStartedStarting_Reload");
        
        if (service.isSupported(ServiceFeature.BLOCK_START_SERVICE)) {
            service.start().expect(ComponentState.STARTING)
                   .expect(ServiceState.STARTING, ServiceState.RUNNING)
                   .expect(ComponentState.STARTED)
                   .stopService(false)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                   .setup(ServiceFeature.BLOCK_START_SERVICE)
                   .startService()
                   .expect(ServiceState.STARTING)
                   .reload(false)
                   .expect(InvalidStateTransistionException.class)
                   .assertStates(ComponentState.STARTED, ServiceState.STARTING)
                   .teardown(ServiceFeature.BLOCK_START_SERVICE)
                   .expect(ServiceState.RUNNING)
                   .reload(false)
                   .expect(ComponentState.RELOADING)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN, ServiceState.RUNNING)
                   .expect(ComponentState.STARTED);
        }
        log.exiting(getClass().getName(), "testStartedStarting_Reload");
    }
    
    
    public void testStartedStarting_StopService() {
        log.entering(getClass().getName(), "testStartedStarting_StopService");
        
        if (service.isSupported(ServiceFeature.BLOCK_START_SERVICE)) {
            service.start().expect(ComponentState.STARTING)
                   .expect(ServiceState.STARTING, ServiceState.RUNNING)
                   .expect(ComponentState.STARTED)
                   .stopService(false)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                   .setup(ServiceFeature.BLOCK_START_SERVICE)
                   .startService()
                   .expect(ServiceState.STARTING)
                   .stopService(false)
                   .expect(InvalidStateTransistionException.class)
                   .assertStates(ComponentState.STARTED, ServiceState.STARTING)
                   .teardown(ServiceFeature.BLOCK_START_SERVICE)
                   .expect(ServiceState.RUNNING)
                   .stopService(false)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN);
        }
        log.exiting(getClass().getName(), "testStartedStarting_StopService");
    }
    
    public void testStartedStarting_StartService() {
        log.entering(getClass().getName(), "testStartedStarting_StartService");
        
        if (service.isSupported(ServiceFeature.BLOCK_START_SERVICE)) {
            service.start().expect(ComponentState.STARTING)
                   .expect(ServiceState.STARTING, ServiceState.RUNNING)
                   .expect(ComponentState.STARTED)
                   .stopService(false)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                   .setup(ServiceFeature.BLOCK_START_SERVICE)
                   .startService()
                   .expect(ServiceState.STARTING)
                   .startService()
                   .expect(InvalidStateTransistionException.class)
                   .assertStates(ComponentState.STARTED, ServiceState.STARTING)
                   .teardown(ServiceFeature.BLOCK_START_SERVICE)
                   .expect(ServiceState.RUNNING)
                   .stopService(false)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN);
        }
        log.exiting(getClass().getName(), "testStartedStarting_StartService");
    }
    
    public void testStartedStarting_Reconnect() {
        log.entering(getClass().getName(), "testStartedStarting_Reconnect");
        
        if (service.isSupported(ServiceFeature.BLOCK_START_SERVICE)) {
            service.start().expect(ComponentState.STARTING)
                   .expect(ServiceState.STARTING, ServiceState.RUNNING)
                   .expect(ComponentState.STARTED)
                   .stopService(false)
                   .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
                   .setup(ServiceFeature.BLOCK_START_SERVICE)
                   .startService()
                   .expect(ServiceState.STARTING)
                   .setup(ServiceFeature.RECONNECT)
                   .expect(InvalidStateTransistionException.class)
                   .assertStates(ComponentState.STARTED, ServiceState.STARTING);
        }
        log.exiting(getClass().getName(), "testStartedStarting_Reconnect");
    }
    
    public void testStartedShutdown_Start() {
        log.entering(getClass().getName(), "testStartedShutdown_Start");
        if (service.isSupported(ServiceFeature.BLOCK_STOP_SERVICE)) {
           service.start().expect(ComponentState.STARTING)
                  .expect(ServiceState.STARTING, ServiceState.RUNNING)
                  .expect(ComponentState.STARTED)
                  .setup(ServiceFeature.BLOCK_STOP_SERVICE)
                  .stopService(false)
                  .expect(ServiceState.SHUTDOWN)
                  .start()
                  .expect(InvalidStateTransistionException.class)
                  .assertStates(ComponentState.STARTED, ServiceState.SHUTDOWN);
        }
        log.exiting(getClass().getName(), "testStartedShutdown_Start");
    }
    
    public void testStartedShutdown_Reload() {
        log.entering(getClass().getName(), "testStartedShutdown_Reload");
        if (service.isSupported(ServiceFeature.BLOCK_STOP_SERVICE)) {
           service.start().expect(ComponentState.STARTING)
                  .expect(ServiceState.STARTING, ServiceState.RUNNING)
                  .expect(ComponentState.STARTED)
                  .setup(ServiceFeature.BLOCK_STOP_SERVICE)
                  .stopService(false)
                  .expect(ServiceState.SHUTDOWN)
                  .reload(false)
                  .expect(InvalidStateTransistionException.class)
                  .assertStates(ComponentState.STARTED, ServiceState.SHUTDOWN);
        }
        log.exiting(getClass().getName(), "testStartedShutdown_Reload");
    }
    
    public void testStartedShutdown_Stop() {
        log.entering(getClass().getName(), "testStartedShutdown_Stop");
        if (service.isSupported(ServiceFeature.BLOCK_STOP_SERVICE)) {
           service.start().expect(ComponentState.STARTING)
                  .expect(ServiceState.STARTING, ServiceState.RUNNING)
                  .expect(ComponentState.STARTED)
                  .setup(ServiceFeature.BLOCK_STOP_SERVICE)
                  .stopService(false)
                  .expect(ServiceState.SHUTDOWN)
                  .stop(false)
                  .expect(InvalidStateTransistionException.class)
                  .assertStates(ComponentState.STARTED, ServiceState.SHUTDOWN);
        }
        log.exiting(getClass().getName(), "testStartedShutdown_Stop");
    }
    
    public void testStartedShutdown_StopForced() throws Throwable {
        log.entering(getClass().getName(), "testStartedShutdown_StopForced");
        try {
            if (service.isSupported(ServiceFeature.BLOCK_STOP_SERVICE)) {
               service.start().expect(ComponentState.STARTING)
                      .expect(ServiceState.STARTING, ServiceState.RUNNING)
                      .expect(ComponentState.STARTED)
                      .setup(ServiceFeature.BLOCK_STOP_SERVICE)
                      .stopService(false)
                      .expect(ServiceState.SHUTDOWN)
                      .waitUntil(ServiceFeature.BLOCK_STOP_SERVICE);
               service.stop(true)
                      .expect(ComponentState.STOPPING)
                      .expect(ServiceState.ERROR)
                      .expect(ComponentState.STOPPED);
            }
        } catch(Throwable t) {
            log.throwing(getClass().getName(), "testStartedShutdown_StopForced", t);
            throw t;
        }
        log.exiting(getClass().getName(), "testStartedShutdown_StopForced");
    }

    public void testStartedShutdown_StartService() {
        log.entering(getClass().getName(), "testStartedShutdown_StartService");
        if (service.isSupported(ServiceFeature.BLOCK_STOP_SERVICE)) {
           service.start().expect(ComponentState.STARTING)
                  .expect(ServiceState.STARTING, ServiceState.RUNNING)
                  .expect(ComponentState.STARTED)
                  .setup(ServiceFeature.BLOCK_STOP_SERVICE)
                  .stopService(false)
                  .expect(ServiceState.SHUTDOWN)
                  .startService()
                  .expect(InvalidStateTransistionException.class)
                  .assertStates(ComponentState.STARTED, ServiceState.SHUTDOWN);
        }
        log.exiting(getClass().getName(), "testStartedShutdown_StartService");
    }

    public void testStartedShutdown_StopService() {
        log.entering(getClass().getName(), "testStartedShutdown_StopService");
        if (service.isSupported(ServiceFeature.BLOCK_STOP_SERVICE)) {
           service.start().expect(ComponentState.STARTING)
                  .expect(ServiceState.STARTING, ServiceState.RUNNING)
                  .expect(ComponentState.STARTED)
                  .setup(ServiceFeature.BLOCK_STOP_SERVICE)
                  .stopService(false)
                  .expect(ServiceState.SHUTDOWN)
                  .stopService(false)
                  .expect(InvalidStateTransistionException.class)
                  .assertStates(ComponentState.STARTED, ServiceState.SHUTDOWN);
        }
        log.exiting(getClass().getName(), "testStartedShutdown_StopService");
    }

    public void testStartedShutdown_Reconnect() {
        log.entering(getClass().getName(), "testStartedShutdown_Reconnect");
        if (service.isSupported(ServiceFeature.BLOCK_STOP_SERVICE)) {
           service.start().expect(ComponentState.STARTING)
                  .expect(ServiceState.STARTING, ServiceState.RUNNING)
                  .expect(ComponentState.STARTED)
                  .setup(ServiceFeature.BLOCK_STOP_SERVICE)
                  .stopService(false)
                  .expect(ServiceState.SHUTDOWN)
                  .setup(ServiceFeature.RECONNECT)
                  .expect(InvalidStateTransistionException.class)
                  .assertStates(ComponentState.STARTED, ServiceState.SHUTDOWN);
        }
        log.exiting(getClass().getName(), "testStartedShutdown_Reconnect");
    }
    
    
    public void testStartedUnknown_Start() {
        log.entering(getClass().getName(), "testStartedUnknown_Start");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stopService(false)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
               .start()
               .expect(InvalidStateTransistionException.class)
               .assertStates(ComponentState.STARTED, ServiceState.UNKNOWN);
        log.exiting(getClass().getName(), "testStartedUnknown_Start");
    }
    
    public void testStartedUnknown_Reload() {
        log.entering(getClass().getName(), "testStartedUnknown_Reload");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stopService(false)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
               .reload(false)
               .expect(ComponentState.RELOADING, ComponentState.STARTED);
        log.exiting(getClass().getName(), "testStartedUnknown_Reload");
    }
    
    public void testStartedUnknown_Stop() {
        log.entering(getClass().getName(), "testStartedUnknown_Stop");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stopService(false)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
               .stop(false)
               .expect(ComponentState.STOPPING, ComponentState.STOPPED);
        log.exiting(getClass().getName(), "testStartedUnknown_Stop");
    }
    
    public void testStartedUnknown_StopForced() {
        log.entering(getClass().getName(), "testStartedUnknown_StopForced");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stopService(false)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
               .stop(true)
               .expect(ComponentState.STOPPING, ComponentState.STOPPED);
        log.exiting(getClass().getName(), "testStartedUnknown_StopForced");
    }

    public void testStartedUnknown_StartService() {
        log.entering(getClass().getName(), "testStartedUnknown_StartService");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stopService(false)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
               .startService()
               .expect(ServiceState.STARTING, ServiceState.RUNNING);
        log.exiting(getClass().getName(), "testStartedUnknown_StartService");
    }
    
    public void testStartedUnknown_StopService() {
        log.entering(getClass().getName(), "testStartedUnknown_StopService");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stopService(false)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
               .stopService(false)
               .expect(InvalidStateTransistionException.class)
               .assertStates(ComponentState.STARTED, ServiceState.UNKNOWN);
        log.exiting(getClass().getName(), "testStartedUnknown_StopService");
    }
    
    public void testStartedUnknown_Reconnect() {
        log.entering(getClass().getName(), "testStartedUnknown_StopService");
        service.start()
               .expect(ComponentState.STARTING)
               .expect(ServiceState.STARTING, ServiceState.RUNNING)
               .expect(ComponentState.STARTED)
               .stopService(false)
               .expect(ServiceState.SHUTDOWN, ServiceState.UNKNOWN)
               .setup(ServiceFeature.RECONNECT)
               .expect(InvalidStateTransistionException.class)
               .assertStates(ComponentState.STARTED, ServiceState.UNKNOWN);
        log.exiting(getClass().getName(), "testStartedUnknown_StopService");
    }
    
    
    static class ServiceTesterProxy<T extends Service> {

        private final ServiceTester<T> tester;

        public boolean isSupported(Feature f) {
            return tester.isSupported(f);
        }
        
        public boolean isSupported(ServiceFeature f) {
            return tester.isSupported(f);
        }
        
        public ServiceTesterProxy<T> setup(ServiceFeature f) {
            tester.setup(f);
            return this;
        }

        public ServiceTesterProxy<T> teardown(ServiceFeature f) {
            tester.teardown(f);
            return this;
        }
        
        public ServiceTesterProxy<T> waitUntil(ServiceFeature f) throws InterruptedException {
            tester.waitUntil(f);
            return this;
        }
        
        public ServiceTesterProxy<T> assertStates(ComponentState compState, ServiceState serviceState) {
            try {
                assertEquals("Invalid component state", compState, tester.getComponent().getState());
                assertEquals("Invalid service state", serviceState, tester.getComponent().getServiceState());
            } catch(GrmRemoteException ex) {
                log.log(Level.WARNING, "Got unexpected exception", ex);
                fail("component#getState and component#getServiceState must not throw an exception");
            }
            return this;
        }
        
        public ServiceTesterProxy(ServiceTester<T> tester) {
            this.tester = tester;
        }

        public ServiceTesterProxy<T> start() {
            tester.start();
            return this;
        }

        public ServiceTesterProxy<T> stop(boolean forced) {
            tester.stop(forced);
            return this;
        }

        public ServiceTesterProxy<T> startService() {
            tester.startService();
            return this;
        }

        public ServiceTesterProxy<T> stopService(boolean freeResources) {
            tester.stopService(freeResources);
            return this;
        }

        public ServiceTesterProxy<T> reload(boolean forced) {
            tester.reload(forced);
            return this;
        }

        public ServiceTesterProxy<T> expect(ServiceState... states) {
            tester.expect(states);
            return this;
        }

        public ServiceTesterProxy<T> expect(ComponentState... states) {
            tester.expect(states);
            return this;
        }

        public ServiceTesterProxy<T> expect(Class<? extends Throwable> ex) {
            tester.expect(ex);
            return this;
        }
        
    }

    public enum ServiceFeature {
        BLOCK_REMOVE_RESOURCE,
        BLOCK_RELOAD_SERVICE,
        BLOCK_START_SERVICE,
        BLOCK_STOP_SERVICE,
        RECONNECT
    }

    public abstract static class DefaultServiceTestAdapter<T extends Service> extends DefaultComponentTestAdapter<T> {

        private final Map<ServiceFeature, FeatureHandler> featureMap = new HashMap<ServiceFeature, FeatureHandler>();
        
        public void addFeature(ServiceFeature f, FeatureHandler handler) {
            featureMap.put(f, handler);
        }
        
        public boolean isSupported(ServiceFeature f) {
            return featureMap.containsKey(f);
        }
        
        public void setup(ServiceFeature f) throws GrmException {
            featureMap.get(f).setup();
        }
        
        public void teardown(ServiceFeature f) throws GrmException {
            featureMap.get(f).teardown();
        }
        
        public void waitUntil(ServiceFeature f) throws InterruptedException {
            featureMap.get(f).waitUntil();
        }
    }

    protected static class ServiceTester<T extends Service> extends ComponentTester<T> implements ServiceEventListener {

        private final LinkedList<AbstractServiceEvent> eventHistory = new LinkedList<AbstractServiceEvent>();

        public ServiceTester(ComponentTestAdapter<T> adapter) throws GrmRemoteException {
            super(adapter);
            getComponent().addServiceEventListener(this);
        }
        
        public boolean isSupported(ServiceFeature f) {
            try {
                return ((DefaultServiceTestAdapter)getAdapter()).isSupported(f);
            } catch(ClassCastException ex) {
                return false;
            }
        }
        
        //needed for access to the ServiceTesters Service
        public Service getService() {
            return getAdapter().getComponent();
        }
         
        public void setup(ServiceFeature f) {
            try {
                ((DefaultServiceTestAdapter)getAdapter()).setup(f);
            } catch(Exception ex) {
                addError(ex);
            }
        }
        
        public void teardown(ServiceFeature f) {
            try {
                ((DefaultServiceTestAdapter)getAdapter()).teardown(f);
            } catch(Exception ex) {
                addError(ex);
            }
        }
        
        public void waitUntil(ServiceFeature f) throws InterruptedException {
            ((DefaultServiceTestAdapter)getAdapter()).waitUntil(f);
        }
 
        public ServiceTester<T> stopService(boolean freeResources) {
            try {
                getComponent().stopService(freeResources);
            } catch (Throwable ex) {
                addError(ex);
            }
            return this;
        }

        public ServiceTester<T> startService() {
            try {
                getComponent().startService();
            } catch (Throwable ex) {
                addError(ex);
            }
            return this;
        }

        private void addEvent(AbstractServiceEvent event) {
            lock.lock();
            try {
                eventHistory.add(event);
                changedCondition.signalAll();
            } finally {
                lock.unlock();
            }
        }

        public void resourceRequest(ResourceRequestEvent event) {
            addEvent(event);
        }

        public void addResource(AddResourceEvent event) {
            addEvent(event);
        }

        public void resourceAdded(ResourceAddedEvent event) {
            addEvent(event);
        }

        public void removeResource(RemoveResourceEvent event) {
            addEvent(event);
        }

        public void resourceRemoved(ResourceRemovedEvent event) {
            addEvent(event);
        }

        public void resourceRejected(ResourceRejectedEvent event) {
            addEvent(event);
        }

        public void resourceError(ResourceErrorEvent event) {
            addEvent(event);
        }

        public void resourceReset(ResourceResetEvent event) {
            addEvent(event);
        }

        public void resourceChanged(ResourceChangedEvent event) {
            addEvent(event);
        }

        public void serviceStarting(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceRunning(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceUnknown(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceShutdown(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceError(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceStopped(ServiceStateChangedEvent event) {
            addEvent(event);
        }
       
        public ServiceTester<T> expect(ServiceState... states) {
            assertStates(TIMEOUT, states);
            return this;
        }

        /**
         * Assert that the next ServiceStateChangedEvent signalizes that the service
         * state has been changed to <code>state</code>
         * 
         * @param state   the next expected service state
         * @param timeout max waiting time ins milliseconds
         */
        public void assertNextState(ServiceState state, long timeout) {
            long endTime = System.currentTimeMillis() + timeout;
            lock.lock();
            AbstractServiceEvent event=null;
            try {
                do{    
                    while (eventHistory.isEmpty()) {
                        long rest = endTime - System.currentTimeMillis();
                        if (rest <= 0) {
                            fail("service did not go into " + state + " state (timeout " + timeout + "ms)");
                        } else {
                            changedCondition.await(rest, TimeUnit.MILLISECONDS);
                        }
                    }
              
                    event = eventHistory.removeFirst();
                    if(event instanceof ServiceStateChangedEvent){
                        assertEquals(ServiceStateChangedEvent.class, event.getClass());
                        assertEquals(state, ((ServiceStateChangedEvent) event).getNewState());
                    } //otherwise skip event!
                } while (!(event instanceof ServiceStateChangedEvent));
            } catch (InterruptedException ex) {
                fail("Waiting for service state " + state + " has been interrupted");
            } finally {
                lock.unlock();
            }
        }

        public void assertStates(long timeout, ServiceState... states) {
            long endTime = System.currentTimeMillis() + timeout;

            for (ServiceState state : states) {
                lock.lock();
                try {
                    if (!errorHistory.isEmpty()) {
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        pw.println("component state changed events expected, but I got errors:");
                        for (Throwable t : errorHistory) {
                            t.printStackTrace(pw);
                            pw.println();
                        }
                        pw.flush();
                        fail(sw.getBuffer().toString());
                    }
                    long rest = endTime - System.currentTimeMillis();
                    if (rest <= 0) {
                        fail("service did not go into state " + state + " (timeout " + timeout + "ms)");
                    }
                    assertNextState(state, rest);
                } finally {
                    lock.unlock();
                }
            }
        }
    }
}
