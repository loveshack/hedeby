/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.Barrier;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.resource.adapter.ResourceAdapterStore;
import com.sun.grid.grm.resource.adapter.impl.DefaultResourceAdapter;
import com.sun.grid.grm.resource.adapter.impl.ResourceErrorOPR;
import com.sun.grid.grm.resource.adapter.impl.SimpleResourceAdapterStore;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManagerSetup;
import com.sun.grid.grm.service.impl.AbstractServiceStateTransitionTester.ServiceFeature;
import com.sun.grid.grm.service.slo.SLO;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManager;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of the <code>ServiceAdapter</code> for a <code>MockupService</code>.
 */
public class MockupServiceAdapterImpl extends AbstractServiceAdapter<ExecutorService, SparePoolServiceConfig, ResourceId, DefaultResourceAdapter>
        implements MockupService {

    private final static Logger log = Logger.getLogger(MockupServiceAdapterImpl.class.getName());
    private final Barrier blockStartServiceBarrier = Barrier.newDisabledInstance("blockStartService");
    private final Barrier blockStopServiceBarrier = Barrier.newDisabledInstance("blockStopService");
    // a barrier to delay the reload
    private final Barrier blockReloadServiceBarrier = Barrier.newDisabledInstance("blockReloadService");
    // a barrier to delay the remove resource action (issue 667)
    private final Barrier blockRemoveResourceBarrier = Barrier.newDisabledInstance("blockRemoveResource");

    public MockupServiceAdapterImpl(MockupServiceContainer component) {
        super(component);
    }

    @Override
    protected long getFreeResourcesTimeout() {
        return 1000;
    }

    @Override
    protected RunnableSLOManager createSLOManager() {
        return new RunnableSLOManager(getName(), this);
    }


    @Override
    protected ResourceAdapterStore<ResourceId, DefaultResourceAdapter> createResourceAdapterStore(SparePoolServiceConfig config) {
        return new MockupSimpleResourceAdapterStore();
    }

    @Override
    protected ResourceFactory<DefaultResourceAdapter> createResourceFactory() {
        return DefaultResourceFactory.getInstance();
    }

    @Override
    protected Map<ResourceId,Object> doStartService(SparePoolServiceConfig config) throws GrmException {
        try {
            blockStartServiceBarrier.waitUntilWakeup();
            return Collections.<ResourceId,Object>emptyMap();
        } catch (InterruptedException ex) {
            throw new GrmException("blockStartServiceBarrier has been interrupted");
        }
    }

    @Override
    protected ServiceState doStopService(boolean forced) throws GrmException {
        if (forced) {
            blockStartServiceBarrier.interrupt();
            blockStopServiceBarrier.interrupt();
        } else {
            try {
                blockStopServiceBarrier.waitUntilWakeup();
            } catch (InterruptedException ex) {
                throw new GrmException("blockStopServiceBarrier has been interrupted", ex);
            }
        }
        return ServiceState.UNKNOWN;
    }

    @Override
    protected Map<ResourceId,Object> doReloadService(SparePoolServiceConfig config, boolean forced) throws GrmException {
        if (forced) {
            blockReloadServiceBarrier.interrupt();
        } else {
            try {
                blockReloadServiceBarrier.waitUntilWakeup();
            } catch (InterruptedException ex) {
                throw new GrmException("blockReloadServiceBarrier has been interrupted", ex);
            }
        }
        return Collections.<ResourceId,Object>emptyMap();
    }

    public void setup(ServiceFeature f) throws GrmException {
        log.log(Level.FINE, "Setup feature {0}", f);
        switch (f) {
            case BLOCK_START_SERVICE:
                blockStartServiceBarrier.enable(true);
                break;
            case BLOCK_STOP_SERVICE:
                blockStopServiceBarrier.enable(true);
                break;
            case BLOCK_RELOAD_SERVICE:
                blockReloadServiceBarrier.enable(true);
                break;
            case BLOCK_REMOVE_RESOURCE:
                blockRemoveResourceBarrier.enable(true);
                break;
            case RECONNECT:
                super.reconnect(new ReconnectAction());
                try {
                    reconnectBarrier.waitUntilBlocked();
                } catch (InterruptedException ex) {
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown feature " + f);
        }
    }

    public void teardown(ServiceFeature f) {
        log.log(Level.FINE, "Teardown feature {0}", f);
        switch (f) {
            case BLOCK_START_SERVICE:
                blockStartServiceBarrier.enable(false);
                break;
            case BLOCK_STOP_SERVICE:
                blockStopServiceBarrier.enable(false);
                break;
            case BLOCK_RELOAD_SERVICE:
                blockReloadServiceBarrier.enable(false);
                break;
            case BLOCK_REMOVE_RESOURCE:
                blockRemoveResourceBarrier.enable(false);
                break;
            case RECONNECT:
                reconnectBarrier.wakeup();
                break;
            default:
                throw new IllegalArgumentException("Unknown feature " + f);
        }
    }
    private final Barrier reconnectBarrier = Barrier.newEnabledInstance("reconnect");

    private class ReconnectAction implements Callable<Map<ResourceId,Object>> {

        public Map<ResourceId,Object> call() throws Exception {
            try {
                log.log(Level.FINE, "ReconnectAction is active");
                reconnectBarrier.waitUntilWakeup();
                log.log(Level.FINE, "ReconnectAction finished");
                return Collections.<ResourceId,Object>emptyMap();
            } catch(InterruptedException ex) {
                log.log(Level.FINE, "ReconnectAction interrupted");
                throw ex;
            } catch(Exception ex) {
                log.log(Level.FINE, "ReconnectAction error", ex);
                throw ex;
            }
        }
    }

    @Override
    protected RunnableSLOManagerSetup createSLOManagerSetup(SparePoolServiceConfig config) throws GrmException {
        return new RunnableSLOManagerSetup(60000, Collections.<SLO>emptyList());
    }

    public void waitUntil(ServiceFeature f) throws InterruptedException {
        switch(f) {
            case BLOCK_START_SERVICE:
                blockStartServiceBarrier.waitUntilBlocked();
                break;
            case BLOCK_STOP_SERVICE:
                blockStopServiceBarrier.waitUntilBlocked();
                break;
            case BLOCK_RELOAD_SERVICE:
                blockReloadServiceBarrier.waitUntilBlocked();
                break;
            case BLOCK_REMOVE_RESOURCE:
                blockRemoveResourceBarrier.waitUntilBlocked();
                break;
            default:
                throw new IllegalArgumentException("ServiceFeature " + f + " does not support waitUntil");
        }
    }

    class MockupSimpleResourceAdapterStore extends SimpleResourceAdapterStore {

        /**
         * Creates a DefaultResourceAdapter
         * @param resource the resource
         * @return the DefaultResourceAdapter
         */
        @Override
        public DefaultResourceAdapter createResourceAdapter(Resource resource) {
            return new MockupDefaultResourceAdapter(resource);
        }
    }

    class MockupDefaultResourceAdapter extends DefaultResourceAdapter {

        MockupDefaultResourceAdapter(Resource resource) {
            super(resource);
        }

      
        @Override
        protected RAOperationResult doUninstall(ResourceRemovalDescriptor descr) {
            try {
                blockRemoveResourceBarrier.waitUntilWakeup();
            } catch (InterruptedException ex) {
                return new ResourceErrorOPR(resource.clone(), "blockRemoveResourceBarrier has been interrupted");
            }
            return super.doUninstall(descr);
        }
        
    }
}
