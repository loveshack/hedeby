/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Generic service event listeners that waits for resource events delivery within given timeout.
 */
class GenericResourceSEL extends ServiceEventAdapter {

    private final Lock lock = new ReentrantLock();
    private final Condition addCondition = lock.newCondition();
    private final Condition addedCondition = lock.newCondition();
    private final Condition removeCondition = lock.newCondition();
    private final Condition removedCondition = lock.newCondition();
    private final Condition errorCondition = lock.newCondition();
    private final Condition rejectedCondition = lock.newCondition();
    private final Condition resetCondition = lock.newCondition();
    private final Condition propertiesChangedCondition = lock.newCondition();
    /* flag signaling that listener has to wait for delivery of add event */
    private boolean awaitAdd = false;
    /* flag signaling that listener has to wait for delivery of added event */
    private boolean awaitAdded = false;
    /* flag signaling that listener has to wait for delivery of remove event */
    private boolean awaitRemove = false;
    /* flag signaling that listener has to wait for delivery of removed event */
    private boolean awaitRemoved = false;
    /* flag signaling that listener has to wait for delivery of error event */
    private boolean awaitError = false;
    /* flag signaling that listener has to wait for delivery of rejected event */
    private boolean awaitRejected = false;
    /* flag signaling that listener has to wait for delivery of reset event */
    private boolean awaitReset = false;
    /* flag signaling that listener has to wait for delivery of propertiesChanged event */
    private boolean awaitResourceChanged = false;
    /* flag signaling that listener has received add event */
    private boolean gotAdd = false;
    /* flag signaling that listener has received added event */
    private boolean gotAdded = false;
    /* flag signaling that listener has received remove event */
    private boolean gotRemove = false;
    /* flag signaling that listener has received removed event */
    private boolean gotRemoved = false;
    /* flag signaling that listener has received error event */
    private boolean gotError = false;
    /* flag signaling that listener has received rejected event */
    private boolean gotRejected = false;
    /* flag signaling that listener has received reset event */
    private boolean gotReset = false;
    /* flag signaling that listener has received propertiesChanged event */
    private boolean gotResourceChanged = false;
    private final Condition justWaitCondition = lock.newCondition();
    private Resource toWatch;

    //, boolean awaitAdd, boolean awaitAdded, boolean awaitRemove, boolean awaitRemoved, boolean awaitError, boolean awaitRejected, boolean awaitReset, boolean awaitResourceChanged
    public GenericResourceSEL(Resource toWatch) {
        super();
        this.toWatch = toWatch;
        this.awaitAdd = false;
        this.awaitAdded = false;
        this.awaitRemove = false;
        this.awaitRemoved = false;
        this.awaitError = false;
        this.awaitRejected = false;
        this.awaitReset = false;
        this.awaitResourceChanged = false;
    }

    public GenericResourceSEL awaitAdd(boolean awaitAdd) {
        this.awaitAdd = awaitAdd;
        return this;
    }

    public GenericResourceSEL awaitAdded(boolean awaitAdded) {
        this.awaitAdded = awaitAdded;
        return this;
    }

    public GenericResourceSEL awaitRemove(boolean awaitRemove) {
        this.awaitRemove = awaitRemove;
        return this;
    }

    public GenericResourceSEL awaitRemoved(boolean awaitRemoved) {
        this.awaitRemoved = awaitRemoved;
        return this;
    }

    public GenericResourceSEL awaitError(boolean awaitError) {
        this.awaitError = awaitError;
        return this;
    }

    public GenericResourceSEL awaitRejected(boolean awaitRejected) {
        this.awaitRejected = awaitRejected;
        return this;
    }

    public GenericResourceSEL awaitReset(boolean awaitReset) {
        this.awaitReset = awaitReset;
        return this;
    }

    public GenericResourceSEL awaitResourceChanged(boolean awaitResourceChanged) {
        this.awaitResourceChanged = awaitResourceChanged;
        return this;
    }

    /* helper method signaling that listener has just wait some time - if it
    does not need to wait for any other event */
    public boolean justWait() {
        return !this.awaitAdd && !this.awaitAdded && !this.awaitRemove && !this.awaitRemoved && !this.awaitError && !this.awaitRejected && !this.awaitReset && !this.awaitResourceChanged;
    }

    @Override
    public void addResource(AddResourceEvent evt) {
        lock.lock();
        try {
            gotAdd = evt.getResource().equals(toWatch);
            addCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void resourceAdded(ResourceAddedEvent evt) {
        lock.lock();
        try {
            gotAdded = evt.getResource().equals(toWatch);
            addedCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void removeResource(RemoveResourceEvent evt) {
        lock.lock();
        try {
            gotRemove = evt.getResource().equals(toWatch);
            removeCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void resourceRemoved(ResourceRemovedEvent evt) {
        lock.lock();
        try {
            gotRemoved = evt.getResource().equals(toWatch);
            removedCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void resourceError(ResourceErrorEvent evt) {
        lock.lock();
        try {
            gotError = evt.getResource().equals(toWatch);
            errorCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void resourceRejected(ResourceRejectedEvent evt) {
        lock.lock();
        try {
            gotRejected = evt.getResource().equals(toWatch);
            rejectedCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void resourceReset(ResourceResetEvent evt) {
        lock.lock();
        try {
            gotReset = evt.getResource().equals(toWatch);
            resetCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void resourceChanged(ResourceChangedEvent evt) {
        lock.lock();
        try {
            gotResourceChanged = evt.getResource().equals(toWatch);
            propertiesChangedCondition.signal();
        } finally {
            lock.unlock();
        }
    }

    public boolean hasAll(long timeOut) throws InterruptedException {
        while (true) {
            lock.lock();
            try {
                if (awaitAdd && !isGotAdd()) {
                    gotAdd = addCondition.await(timeOut, TimeUnit.MILLISECONDS);
                }
                break;
            } finally {
                lock.unlock();
            }
        }
        while (true) {
            lock.lock();
            try {
                if (awaitAdded && !isGotAdded()) {
                    gotAdded = addedCondition.await(timeOut, TimeUnit.MILLISECONDS);
                }
                break;
            } finally {
                lock.unlock();
            }
        }
        while (true) {
            lock.lock();
            try {
                if (awaitRemove && !isGotRemove()) {
                    gotRemove = removeCondition.await(timeOut, TimeUnit.MILLISECONDS);
                }
                break;
            } finally {
                lock.unlock();
            }
        }
        while (true) {
            lock.lock();
            try {
                if (awaitRemoved && !isGotRemoved()) {
                    gotRemoved = removedCondition.await(timeOut, TimeUnit.MILLISECONDS);
                }
                break;
            } finally {
                lock.unlock();
            }
        }
        while (true) {
            lock.lock();
            try {
                if (awaitError && !isGotError()) {
                    gotError = errorCondition.await(timeOut, TimeUnit.MILLISECONDS);
                }
                break;
            } finally {
                lock.unlock();
            }
        }
        while (true) {
            lock.lock();
            try {
                if (awaitRejected && !isGotRejected()) {
                    gotRejected = rejectedCondition.await(timeOut, TimeUnit.MILLISECONDS);
                }
                break;
            } finally {
                lock.unlock();
            }
        }
        while (true) {
            lock.lock();
            try {
                if (awaitReset && !isGotReset()) {
                    gotReset = resetCondition.await(timeOut, TimeUnit.MILLISECONDS);
                }
                break;
            } finally {
                lock.unlock();
            }
        }
        while (true) {
            lock.lock();
            try {
                if (awaitResourceChanged && !isGotResourceChanged()) {
                    gotResourceChanged = propertiesChangedCondition.await(timeOut, TimeUnit.MILLISECONDS);
                }
                break;
            } finally {
                lock.unlock();
            }
        }
        if (justWait()) {
            lock.lock();
            try {
                justWaitCondition.await(timeOut, TimeUnit.MILLISECONDS);
            } finally {
                lock.unlock();
            }
        }
        return (awaitAdded == isGotAdded()) && (awaitAdd == isGotAdd()) && (awaitRemove == isGotRemove()) && (awaitRemoved == isGotRemoved()) && (awaitError == isGotError()) && (awaitRejected == isGotRejected()) && (awaitReset == isGotReset()) && (awaitResourceChanged == isGotResourceChanged());
    }

    public boolean isGotAdd() {
        return gotAdd;
    }

    public boolean isGotAdded() {
        return gotAdded;
    }

    public boolean isGotRemove() {
        return gotRemove;
    }

    public boolean isGotRemoved() {
        return gotRemoved;
    }

    public boolean isGotError() {
        return gotError;
    }

    public boolean isGotRejected() {
        return gotRejected;
    }

    public boolean isGotReset() {
        return gotReset;
    }

    public boolean isGotResourceChanged() {
        return gotResourceChanged;
    }
}
