/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.event.ComponentEventAdapter;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.event.AbstractServiceChangedResourceEvent;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * mockup to test event protocol between SCP and remote service. it will
 * not allow to perform full refresh (this is wanted behavior) by SCP, as 
 * all FiringResourceEventsServiceMockup methods will throw exception */
class FiringResourceEventsServiceMockup implements Service {

    protected final Map<ResourceId, Resource> resources;
    protected final Hostname hostname;
    protected final String name;
    protected ServiceEventListener sel;
    protected final ServiceEventListener nullServiceEventListener = new ServiceEventAdapter();
    protected ComponentEventListener cel;
    protected final ComponentEventListener nullComponentEventListener = new ComponentEventAdapter();
    protected volatile ComponentState cmpState;
    protected volatile ServiceState svcState;
    protected long svcEventId = 0;
    protected long cmpEventId = 0;

    public FiringResourceEventsServiceMockup(String name, String host) {
        super();
        this.resources = new HashMap<ResourceId, Resource>();
        this.hostname = Hostname.getInstance(host);
        this.name = name;
        this.cmpState = ComponentState.UNKNOWN;
        this.svcState = ServiceState.UNKNOWN;
        this.sel = nullServiceEventListener;
        this.cel = nullComponentEventListener;
    }
    
    public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
        return new DefaultServiceSnapshot(svcEventId, cmpState, svcState, new HashSet<Resource>(resources.values()), 
                Collections.<String,Collection<Need>>emptyMap());
    }
    

    public void startService() {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void stopService(boolean isFreeResources) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public ServiceState getServiceState() {
        throw new UnsupportedOperationException("Not supported.");
    }

    public List<SLOState> getSLOStates() {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void addServiceEventListener(ServiceEventListener eventListener) {
        sel = eventListener;
    }

    public void removeServiceEventListener(ServiceEventListener eventListener) {
        sel = nullServiceEventListener;
    }

    public List<Resource> getResources(Filter filter) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public List<Resource> getResources() {
        throw new UnsupportedOperationException("Not supported.");
    }

    public Resource getResource(ResourceId resourceId) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void resetResource(ResourceId resource) {
        throw new UnsupportedOperationException("Not supported.");
    }
   
    public void addResource(Resource resource,String sloName) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void addResource(Resource resource) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void modifyResource(ResourceId resource, ResourceChangeOperation operations) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void start() {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void stop(boolean isForced) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void reload(boolean isForced) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public ComponentState getState() {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void addComponentEventListener(ComponentEventListener componentEventListener) {
        cel = componentEventListener;
    }

    public void removeComponentEventListener(ComponentEventListener componentEventListener) {
        cel = nullComponentEventListener;
    }

    public String getName() {
        return name;
    }

    public Hostname getHostname() {
        return hostname;
    }

    public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) {
        throw new UnsupportedOperationException("Not supported.");
    }

    public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) {
    }

    public void fireResourceEvent(AbstractServiceChangedResourceEvent event) {
        if (event instanceof AddResourceEvent) {
            sel.addResource((AddResourceEvent) event);
        }
        if (event instanceof ResourceAddedEvent) {
            sel.resourceAdded((ResourceAddedEvent) event);
        }
        if (event instanceof RemoveResourceEvent) {
            sel.removeResource((RemoveResourceEvent) event);
        }
        if (event instanceof ResourceRemovedEvent) {
            sel.resourceRemoved((ResourceRemovedEvent) event);
        }
        if (event instanceof ResourceErrorEvent) {
            sel.resourceError((ResourceErrorEvent) event);
        }
        if (event instanceof ResourceResetEvent) {
            sel.resourceReset((ResourceResetEvent) event);
        }
        if (event instanceof ResourceChangedEvent) {
            sel.resourceChanged((ResourceChangedEvent) event);
        }
        if (event instanceof ResourceRejectedEvent) {
            sel.resourceRejected((ResourceRejectedEvent) event);
        }
    }

    public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
