/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.event;

import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.impl.ResourcePropertyDeleted;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.util.EventListenerSupportTest;
import com.sun.grid.grm.util.Hostname;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 */
public class ServiceEventSupportTest extends TestCase {

    private final static Logger log = Logger.getLogger(EventListenerSupportTest.class.getName());
    private Resource resource;

    public ServiceEventSupportTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ExecutionEnv env = DummyExecutionEnvFactory.newInstance();
        resource = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testFireAddResource() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireAddResource");
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String msg = "addResource";
        ses.fireAddResource(resource, msg);
        try {
            AddResourceEvent evt = (AddResourceEvent)hist.assertEvent(0, 1000);
            assertEquals("AddResourceEvent contains an invalid message", msg, evt.getMessage());
            assertEquals("AddResourceEvent contains an invalid resource", resource, evt.getResource());
            assertEquals("AddResourceEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireAddResource must produce an AddResourceEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireAddResource");
    }
    
    public void testFireRemoveResource() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireRemoveResource");
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String msg = "removeResource";
        ses.fireRemoveResource(resource, msg);
        try {
            RemoveResourceEvent evt = (RemoveResourceEvent)hist.assertEvent(0, 1000);
            assertEquals("RemoveResourceEvent contains an invalid message", msg, evt.getMessage());
            assertEquals("RemoveResourceEvent contains an invalid resource", resource, evt.getResource());
            assertEquals("RemoveResourceEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireRemoveResource must produce an RemoveResourceEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireRemoveResource");
    }

    public void testFireResourceAdded() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireResourceAdded");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String msg = "resourceAdded";
        ses.fireResourceAdded(resource, msg);
        try {
            ResourceAddedEvent evt = (ResourceAddedEvent)hist.assertEvent(0, 1000);
            assertEquals("ResourceAddedEvent contains an invalid message", msg, evt.getMessage());
            assertEquals("ResourceAddedEvent contains an invalid resource", resource, evt.getResource());
            assertEquals("ResourceAddedEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireResourceAdded must produce an ResourceAddedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireResourceAdded");
    }
    
    public void testFireResourceChanged() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireResourceChanged");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String msg = "resourceChanged";
        Collection<ResourceChanged> changes = new ArrayList<ResourceChanged>(1);
        changes.add(new ResourcePropertyDeleted("static", false));
        ses.fireResourceChanged(resource, changes, msg);
        try {
            ResourceChangedEvent evt = (ResourceChangedEvent)hist.assertEvent(0, 1000);
            assertEquals("ResourceChangedEvent contains an invalid message", msg, evt.getMessage());
            assertEquals("ResourceChangedEvent contains an invalid resource", resource, evt.getResource());
            assertEquals("ResourceChangedEvent contains an invalid service", service, evt.getServiceName());
            assertEquals("ResourceChangedEvent contains invalid changes", changes.size(), evt.getProperties().size());
        } catch(ClassCastException ex) {
            fail("fireResourceChanged must produce an ResourceChangedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireResourceChanged");
    }
    
    public void testFireResourceError() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireResourceError");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String msg = "resourceError";
        ses.fireResourceError(resource, msg);
        try {
            ResourceErrorEvent evt = (ResourceErrorEvent)hist.assertEvent(0, 1000);
            assertEquals("ResourceErrorEvent contains an invalid message", msg, evt.getMessage());
            assertEquals("ResourceErrorEvent contains an invalid resource", resource, evt.getResource());
            assertEquals("ResourceErrorEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireResourceError must produce an ResourceErrorEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireResourceError");
    }
    
    public void testFireResourceRejected() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireResourceRejected");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String msg = "resourceRejected";
        ses.fireResourceRejected(resource, msg);
        try {
            ResourceRejectedEvent evt = (ResourceRejectedEvent)hist.assertEvent(0, 1000);
            assertEquals("ResourceErrorEvent contains an invalid message", msg, evt.getMessage());
            assertEquals("ResourceErrorEvent contains an invalid resource", resource, evt.getResource());
            assertEquals("ResourceErrorEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireResourceRejected must produce a ResourceRejectedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireResourceRejected");
    }

    public void testFireResourceRemoved() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireResourceRemoved");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String msg = "resourceRemoved";
        ses.fireResourceRemoved(resource, msg);
        try {
            ResourceRemovedEvent evt = (ResourceRemovedEvent)hist.assertEvent(0, 1000);
            assertEquals("ResourceRemovedEvent contains an invalid message", msg, evt.getMessage());
            assertEquals("ResourceRemovedEvent contains an invalid resource", resource, evt.getResource());
            assertEquals("ResourceRemovedEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireResourceRemoved must produce a ResourceRemovedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireResourceRemoved");
    }

    public void testFireResourceRequest() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireResourceRequest");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String slo = "slo";
        List<Need> needs = new ArrayList<Need>(1);
        needs.add(new Need(Usage.MAX_VALUE));
        ses.fireResourceRequest(slo, needs);
        try {
            ResourceRequestEvent evt = (ResourceRequestEvent)hist.assertEvent(0, 1000);
            assertEquals("ResourceRequestEvent contains an invalid slo ", slo, evt.getSLOName());
            assertEquals("ResourceRequestEvent contains an invalid needs ", needs.size(), evt.getNeeds().size());
            assertEquals("ResourceRequestEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireResourceRemoved must produce a ResourceRemovedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireResourceRequest");
    }
    
    public void testFireResourceRequestSingleNeed() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireResourceRequestSingleNeed");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String slo = "slo";
        ses.fireResourceRequest(slo, new Need(Usage.MAX_VALUE));
        try {
            ResourceRequestEvent evt = (ResourceRequestEvent)hist.assertEvent(0, 1000);
            assertEquals("ResourceRequestEvent contains an invalid slo ", slo, evt.getSLOName());
            assertEquals("ResourceRequestEvent contains an invalid needs ", 1, evt.getNeeds().size());
            assertEquals("ResourceRequestEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireResourceRemoved must produce a ResourceRemovedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireResourceRequestSingleNeed");
    }
    
    public void testFireResourceReset() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireResourceReset");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        String msg = "resourceReset";
        ses.fireResourceReset(resource, msg);
        try {
            ResourceResetEvent evt = (ResourceResetEvent)hist.assertEvent(0, 1000);
            assertEquals("ResourceResetEvent contains an invalid message", msg, evt.getMessage());
            assertEquals("ResourceResetEvent contains an invalid resource", resource, evt.getResource());
            assertEquals("ResourceResetEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireResourceReset must produce a ResourceResetEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireResourceReset");
    }
    
    public void testFireServiceError() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireServiceError");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        ses.fireServiceError();
        try {
            ServiceStateChangedEvent evt = (ServiceStateChangedEvent)hist.assertEvent(0, 1000);
            assertEquals("ServiceStateChangedEvent contains invalid state", ServiceState.ERROR, evt.getNewState());
            assertEquals("ServiceStateChangedEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireServiceError must produce a ServiceStateChangedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireServiceError");
    }
    
    public void testFireServiceRunning() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireServiceRunning");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        ses.fireServiceRunning();
        try {
            ServiceStateChangedEvent evt = (ServiceStateChangedEvent)hist.assertEvent(0, 1000);
            assertEquals("ServiceStateChangedEvent contains invalid state", ServiceState.RUNNING, evt.getNewState());
            assertEquals("ServiceStateChangedEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireServiceRunning must produce a ServiceStateChangedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireServiceRunning");
    }
    
    public void testFireServiceShutdown() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireServiceShutdown");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        ses.fireServiceShutdown();
        try {
            ServiceStateChangedEvent evt = (ServiceStateChangedEvent)hist.assertEvent(0, 1000);
            assertEquals("ServiceStateChangedEvent contains invalid state", ServiceState.SHUTDOWN, evt.getNewState());
            assertEquals("ServiceStateChangedEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireServiceShutdown must produce a ServiceStateChangedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireServiceShutdown");
    }
    
    public void testFireServiceStarting() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireServiceStarting");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        ses.fireServiceStarting();
        try {
            ServiceStateChangedEvent evt = (ServiceStateChangedEvent)hist.assertEvent(0, 1000);
            assertEquals("ServiceStateChangedEvent contains invalid state", ServiceState.STARTING, evt.getNewState());
            assertEquals("ServiceStateChangedEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireServiceStarting must produce a ServiceStateChangedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireServiceStarting");
    }
    
    public void testFireServiceStopped() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireServiceStopped");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        ses.fireServiceStopped();
        try {
            ServiceStateChangedEvent evt = (ServiceStateChangedEvent)hist.assertEvent(0, 1000);
            assertEquals("ServiceStateChangedEvent contains invalid state", ServiceState.STOPPED, evt.getNewState());
            assertEquals("ServiceStateChangedEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireServiceStopped must produce a ServiceStateChangedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireServiceStopped");
    }

    public void testFireServiceUnknown() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testFireServiceUnknown");
        
        ServiceEventHistory hist = new ServiceEventHistory();
        String service = "test";
        ServiceEventSupport ses = ServiceEventSupport.newInstance(service, Hostname.getLocalHost());
        ses.addServiceEventListener(hist);
        ses.fireServiceUnknown();
        try {
            ServiceStateChangedEvent evt = (ServiceStateChangedEvent)hist.assertEvent(0, 1000);
            assertEquals("ServiceStateChangedEvent contains invalid state", ServiceState.UNKNOWN, evt.getNewState());
            assertEquals("ServiceStateChangedEvent contains an invalid service", service, evt.getServiceName());
        } catch(ClassCastException ex) {
            fail("fireServiceUnknown must produce a ServiceStateChangedEvent (" + ex.getLocalizedMessage() + ")");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireServiceUnknown");
    }
    
    /**
     * This test fires from different threads service events and records all
     * events in a history. Finally it checks the events arrived in the correct
     * order according to the sequence number.
     * 
     * @throws java.lang.Exception
     */
    public void testEventOrder() throws Exception {
        log.entering(ServiceEventSupportTest.class.getName(), "testEventOrderForASynchronEvents");

        ServiceEventHistory hist = new ServiceEventHistory();

        final ServiceEventSupport ses = ServiceEventSupport.newInstance("test", Hostname.getLocalHost());
        ses.addServiceEventListener(hist);

        
        final int threadCount = 10;
        final int max = 1000;
        final AtomicInteger counter = new AtomicInteger();

        ExecutorService executor = Executors.newCachedThreadPool();

        for (int i = 0; i < threadCount; i++) {
            executor.submit(new Runnable() {

                public void run() {
                    while (true) {
                        int id = counter.getAndIncrement();
                        if (id >= max) {
                            break;
                        }
                        fireRandomEvent(ses);
                        Thread.yield();
                    }
                }
            });
        }

        executor.shutdown();

        // The sequence number of the events will start with 0
        // We can use the index to determine the correct event order
        for (int i = 0; i < max; i++) {
            hist.assertEvent(i, 1000);
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testEventOrderForASynchronEvents");
    }
    
    private void fireRandomEvent(ServiceEventSupport ses) {

        int index = (int) (Math.random() * 15);
        switch (index) {
            case 0:
                ses.fireAddResource(resource, "");
                break;
            case 1:
                ses.fireRemoveResource(resource, "");
            case 2:
                ses.fireResourceAdded(resource, "");
            case 3:
                ses.fireResourceChanged(resource, Collections.<ResourceChanged>emptyList(), "");
                break;
            case 4:
                ses.fireResourceError(resource, "");
            case 5:
                ses.fireResourceRejected(resource, "");
            case 6:
                ses.fireResourceRemoved(resource, "");
            case 7:
                ses.fireResourceRequest("slo", Collections.<Need>emptyList());
                break;
            case 8:
                ses.fireResourceRequest("slo", new Need(Usage.MIN_VALUE));
                break;
            case 9:
                ses.fireResourceReset(resource, "");
            case 10:
                ses.fireServiceError();
                break;
            case 11:
                ses.fireServiceRunning();
                break;
            case 12:
                ses.fireServiceShutdown();
                break;
            case 13:
                ses.fireServiceStarting();
                break;
            case 14:
                ses.fireServiceStopped();
                break;
            case 15:
                ses.fireServiceUnknown();
                break;
            default:
                throw new IllegalStateException("Cound not map index " + index + " to event");
        }
        log.exiting(ServiceEventSupportTest.class.getName(), "testFireAddResource");
    }
    

    private static class ServiceEventHistory implements ServiceEventListener {

        private LinkedList<AbstractServiceEvent> events = new LinkedList<AbstractServiceEvent>();
        private Lock lock = new ReentrantLock();
        private Condition cond = lock.newCondition();

        private void addEvent(AbstractServiceEvent event) {
            lock.lock();
            try {
                log.log(Level.FINE, "Got event " + event);
                events.addLast(event);
                cond.signalAll();
            } finally {
                lock.unlock();
            }
        }

        public AbstractServiceEvent assertEvent(long sequenceNumber, long timeout) throws InterruptedException {
            long endTime = System.currentTimeMillis() + timeout;
            lock.lock();
            try {
                log.log(Level.FINE, "assert event " + sequenceNumber);
                while (events.isEmpty()) {
                    long rest = endTime - System.currentTimeMillis();
                    if (rest <= 0) {
                        fail("Timeout while waiting for event " + sequenceNumber);
                    }
                    cond.await(rest, TimeUnit.MILLISECONDS);
                }
                AbstractServiceEvent evt = events.removeFirst();
                assertEquals("Got unexpected event " + evt, sequenceNumber, evt.getSequenceNumber());
                return evt;
            } finally {
                lock.unlock();
            }
        }

        public void resourceRequest(ResourceRequestEvent event) {
            addEvent(event);
        }

        public void addResource(AddResourceEvent event) {
            addEvent(event);
        }

        public void resourceAdded(ResourceAddedEvent event) {
            addEvent(event);
        }

        public void removeResource(RemoveResourceEvent event) {
            addEvent(event);
        }

        public void resourceRemoved(ResourceRemovedEvent event) {
            addEvent(event);
        }

        public void resourceRejected(ResourceRejectedEvent event) {
            addEvent(event);
        }

        public void resourceError(ResourceErrorEvent event) {
            addEvent(event);
        }

        public void resourceReset(ResourceResetEvent event) {
            addEvent(event);
        }

        public void resourceChanged(ResourceChangedEvent event) {
            addEvent(event);
        }

        public void serviceStarting(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceRunning(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceUnknown(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceShutdown(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceError(ServiceStateChangedEvent event) {
            addEvent(event);
        }

        public void serviceStopped(ServiceStateChangedEvent event) {
            addEvent(event);
        }
    }
}
