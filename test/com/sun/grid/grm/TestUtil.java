/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.HostSet;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.MultiComponent;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.config.common.SLOSet;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.config.executor.ExecutorConfig;
import com.sun.grid.grm.config.resourceprovider.PriorityPolicyConfig;
import com.sun.grid.grm.config.resourceprovider.PriorityPolicyManagerConfig;
import com.sun.grid.grm.config.resourceprovider.ResourceProviderConfig;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.executor.impl.ExecutorImpl;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.impl.ResourceProviderImpl;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.sparepool.SparePoolServiceImpl;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import junit.framework.TestCase;

/**
 *  Utility class for tests
 */
public class TestUtil {

    private static final File tmpDir = new File(System.getProperty("java.io.tmpdir"));
    private static final File userTmpDir = new File(tmpDir, System.getProperty("user.name"));

    /**
     *  get the tmp directory for a test.
     *
     *  @param test the test
     * @return the tmp directory for a test
     */
    public static File getTmpDir(TestCase test) {
        return new File(userTmpDir, test.getName() + System.currentTimeMillis());
    }

    /**
     * Creates a <code>Pattern</code> to match an error message against output
     * created with a message in a bundle.
     * <p>
     * The bundle <code>bundleName</code> is examined for the <code>bundleKey</code>.
     * The message template obtained in this way is converted into a
     * <code>Pattern</code>. This <code>Pattern</code> can be used to see if an
     * error message (with filled out placeholders) matches could be
     * derived from the template.
     * <p>
     * This works only for bundle messages in the <code>MessageFormat</code> format,
     * i.e. containing placeholders like {0}, {1}, etc. and NOT for printf like
     * messages.
     * 
     * @param bundleKey 
     *        Key of the message of bundle <code>bundleName</code> that is used
     *        for the pattern creation
     * @param bundleName
     *        Name of the used bundle.
     * @return created pattern
     * @see assertErrorMessage
     */
    public static Pattern createErrorMessagePattern(String bundleKey, String bundleName) {
        String raw_msg = getBundleMsg(bundleKey, bundleName);

        String[] parts = raw_msg.split("\\{\\d[^}]*\\}", -1);

        if (parts.length == 0) {
            return Pattern.compile("");
        }

        // build up the pattern string
        StringBuilder pattern = new StringBuilder(raw_msg.length());
        for (String p : parts) {
            // escape metacharacters and put ".*" pattern where {0}, {1}, etc. was
            pattern.append(Pattern.quote(p));
            pattern.append(".*");
        }
        // remove the last appended ".*"
        pattern.setLength(pattern.length() - 2);

        // Pattern.DOTALL allows that '.' matches newline characters as well
        // (important for multi-line error messages)
        return Pattern.compile(pattern.toString(), Pattern.DOTALL);
    }

    /**
     * Asserts that the error message in <code>ex</code> matches the error
     * message referenced in bundle <code>bundleName</code>.
     * <p>
     * This works only for bundle messages in the <code>MessageFormat</code>
     * format, i.e. containing placeholders like {0}, {1}, etc. and NOT for
     * printf like messages.
     * <p>
     * If the error message does not match, an <code>AssertError</code> with
     * <code>assert_message</code> is thrown.
     * 
     * @param assert_message 
     *        String that is used to construct the <code>AssertionError</code>.
     * @param bundleKey 
     *        Key of the message of bundle <code>bundleName</code> that is used for the comparison.
     * @param bundleName
     *        Name of the used bundle.
     * @param ex
     *        Throwable to compare with, ex.getLocalizedMessage() is used to get the
     *        message to compare with.
     * @see createErrorMessagePattern
     */
    public static void assertMatchingError(String assert_message, String bundleKey, String bundleName, Throwable ex) {
        String errorMsg = ex.getLocalizedMessage();
        Matcher m = createErrorMessagePattern(bundleKey, bundleName).matcher(errorMsg);

        if (m.matches()) {
            return;
        } else {
            throw new AssertionError(String.format("%s Got: %s but expected to match template: %s", assert_message, errorMsg, getBundleMsg(bundleKey, bundleName)));
        }
    }

    /**
     * Asserts that the error message in <code>ex</code> matches the error message
     * referenced in bundle <code>bundleName</code>.
     * <p>
     * This works only for bundle messages in the <code>MessageFormat</code>
     * format, i.e. containing placeholders like {0}, {1}, etc. and NOT for
     * printf like messages.
     * <p>
     * If the error message does not match, an <code>AssertError</code> is thrown.
     * 
     * @param bundleKey 
     *        Key of the message of bundle bundleName that is used for the comparison.
     * @param bundleName
     *        Name of the used bundle.
     * @param ex
     *        Throwable to compare with, ex.getLocalizedMessage() is used to get the
     *        message to compare with.
     */
    public static void assertMatchingError(String bundleKey, String bundleName, Throwable ex) {
        assertMatchingError("assertMatchingError -", bundleKey, bundleName, ex);
    }

    /**
     * Asserts that the error message in <code>ex</code> does NOT match the
     * error message referenced in bundle <code>bundleName</code>.
     * <p>
     * This works only for bundle messages in the <code>MessageFormat</code>
     * format, i.e. containing placeholders like {0}, {1}, etc. and NOT for
     * printf like messages.
     * <p>
     * If the error message does match, an <code>AssertError</code> with
     * <code>assert_message</code> is thrown.
     * 
     * @param assert_message 
     *        String that is used to construct the <code>AssertionError</code>.
     * @param bundleKey 
     *        Key of the message of bundle <code>bundleName</code> that is used
     *        for the comparison.
     * @param bundleName
     *        Name of the used bundle.
     * @param ex
     *        Throwable to compare with, ex.getLocalizedMessage() is used to get the
     *        message to compare with.
     * @see createErrorMessagePattern
     */
    public static void assertNotMatchingError(String assert_message, String bundleKey, String bundleName, Throwable ex) {
        String errorMsg = ex.getLocalizedMessage();
        Matcher m = createErrorMessagePattern(bundleKey, bundleName).matcher(errorMsg);

        if (m.matches()) {
            throw new AssertionError(assert_message + " : " + errorMsg +
                    " unexpectedly matches template: " + getBundleMsg(bundleKey, bundleName));
        } else {
            return;
        }
    }

    /**
     * Asserts that the error message in <code>ex</code> does NOT match the error
     * message referenced in bundle <code>bundleName</code>.
     * <p>
     * This works only for bundle messages in the <code>MessageFormat</code>
     * format, i.e. containing placeholders like {0}, {1}, etc. and NOT for
     * printf like messages.
     * <p>
     * If the error message does match, an <code>AssertError</code> is thrown.
     * 
     * @param bundleKey 
     *        Key of the message of bundle <code>bundleName</code> that is used
     *        for the comparison.
     * @param bundleName
     *        Name of the used bundle.
     * @param ex
     *        Throwable to compare with, ex.getLocalizedMessage() is used to get the
     *        message to compare with.
     */
    public static void assertNotMatchingError(String bundleKey, String bundleName, Throwable ex) {
        assertNotMatchingError("assertNotMatchingError -", bundleKey, bundleName, ex);
    }

    /**
     * Gets a message from a resource bundle.
     * <p>
     * Normally this could be problematic as not all bundles are accessible
     * from everywhere. But this is a test utility method and in the test all
     * bundles are accessible so it is fine.
     * 
     * @param bundleKey
     * @param bundleName
     * @return message with key <code>bundleKey</code> from resource bundle
     *         <code>bundleName</code>
     */
    private static String getBundleMsg(String bundleKey, String bundleName) {
        return ResourceBundle.getBundle(bundleName).getString(bundleKey);
    }

    /**
     * Here we should define objects that should be added to dummy Context
     *
     * This method returns Map with the Objects that should be placed in context
     * the key in this Map is the context path for storing object in context
     * 
     * The method returns the following objects
     * GlobalConfig with configured 3 jvms "executor_vm, additional_vm, rp_vm"
     * Components: ResourceProvider, Executor, SparePool with its configurations
     *
     * @param env execution enviroment for dummy
     * @return Map with Objects for predefined system
     */
    public static Map<String, Object> getPredefinedSystem(ExecutionEnv env) {
        Map<String, Object> ret = new HashMap<String, Object>();
        GlobalConfig global = new GlobalConfig();

        //Create jvm0
        JvmConfig jvm0 = new JvmConfig();
        jvm0.setName("rp_vm");
        jvm0.setPort(0);
        jvm0.setUser("testUser");

        //Create ResourceProvider
        Component rp = new Singleton();
        rp.setAutostart(true);
        rp.setName("rp");
        rp.setClassname(ResourceProviderImpl.class.getName());
        ((Singleton) rp).setHost(Hostname.getLocalHost().getHostname());

        //Add RP to global
        jvm0.getComponent().add(rp);

        // ("component.rp", rpConf) to be added
        ResourceProviderConfig rpConfig = new ResourceProviderConfig();
        PriorityPolicyManagerConfig ppmc = new PriorityPolicyManagerConfig();
        PriorityPolicyConfig pmc = new PriorityPolicyConfig();
        pmc.setName("spare_pool_pp");
        pmc.setService("spare_pool");
        pmc.setValue(10);
        ppmc.getPriority().add(pmc);
        rpConfig.setPolicies(ppmc);

        //Create ResourceProvider
        Component sp = new Singleton();
        sp.setAutostart(true);
        sp.setName("spare_pool");
        sp.setClassname(SparePoolServiceImpl.class.getName());
        ((Singleton) sp).setHost(Hostname.getLocalHost().getHostname());

        //Add Spare Pool to global
        jvm0.getComponent().add(sp);

        ActiveComponent ac = new ActiveComponent();
        ac.setHost(Hostname.getLocalHost().getHostname());
        ac.setJvm(jvm0.getName());
        ac.setMBeanName(ComponentInfo.createObjectName(env, sp.getName(), SparePoolServiceImpl.class).getCanonicalName());

        ret.put("active_component.spare_pool", ac);

        //("component.spare_pool", spConfig) to be added
        SparePoolServiceConfig spConfig = new SparePoolServiceConfig();
        // prepare SLO config
        PermanentRequestSLOConfig sloConf = new PermanentRequestSLOConfig();
        sloConf.setName("PermanentRequestSLO");
        sloConf.setUrgency(1);
        // create a condigtion for filtering the affected resources
        sloConf.setResourceFilter(String.format("%s = \"%s\"", ResourceVariableResolver.TYPE_VAR, HostResourceType.HOST_TYPE));
        // create a filter for request
        sloConf.setRequest(String.format("%s = \"%s\"", ResourceVariableResolver.TYPE_VAR, HostResourceType.HOST_TYPE));
        // assign slo to spare pool config
        SLOSet sloset = new SLOSet();
        sloset.getSlo().add(sloConf);
        spConfig.setSlos(sloset);

        JvmConfig jvm1 = new JvmConfig();
        jvm1.setName("executor_vm");
        jvm1.setPort(0);
        jvm1.setUser("rootUser");

        Component executor = new MultiComponent();
        executor.setAutostart(true);
        executor.setName("executor");
        executor.setClassname(ExecutorImpl.class.getName());
        HostSet hosts = new HostSet();
        hosts.getInclude().add(".*");
        ((MultiComponent) executor).setHosts(hosts);

        jvm1.getComponent().add(executor);

        //("component.executor", exec) to be added
        ExecutorConfig exec = new ExecutorConfig();

        JvmConfig jvm2 = new JvmConfig();
        jvm2.setName("additional_vm");
        jvm2.setPort(0);
        jvm2.setUser("testUser");


        // create result
        global.getJvm().add(jvm0);
        global.getJvm().add(jvm1);
        global.getJvm().add(jvm2);
        ret.put("global", global);
        ret.put("component.executor", exec);
        ret.put("component.rp", rpConfig);
        ret.put("component.spare_pool", spConfig);

        return ret;
    }

    /**
     * This method expects a set of objects of any kind and test if they are
     * mutual equal unequal according to expectedValue. Every possible compare is 
     * done only in one direction (no symmetry) and not for same object (no reflexivity)
     * @param objs Array of objects that should be mutual equal / unequal
     * @param expectedValue a flag that indicates if objs are mutual equal / unequal
     */
    public static void assertAllMutualEquals(Object[] objs, boolean expectedValue) {
        assertAllMutualEquals(null, objs, expectedValue);
    }

    /**
     * This method expects a set of objects of any kind and test if they are
     * mutual equal unequal according to expectedValue. Every possible compare is 
     * done only in one direction (no symmetry) and not for same object (no reflexivity)
     * @param msg Message that will be displayed in the case of an error
     * @param objs Array of objects that should be mutual equal / unequal
     * @param expectedValue a flag that indicates if objs are mutual equal / unequal
     */
    public static void assertAllMutualEquals(String msg, Object[] objs, boolean expectedValue) {
        String err = "Call of equals has returned unexpected value:";
        for (int i = 0; i < objs.length; i++) {
            Object a = objs[i];
            if (a == null) {
                continue;
            // Warning do not compare same objects! It will fail if expectedValue =false!
            }
            for (int j = 0; j < objs.length; j++) {
                if (i == j) {
                    continue;
                }
                Object b = objs[j];
                String descr = "[" + i + ":" + a + "],[" + j + ":" + b + "]";
                TestCase.assertEquals(concat(msg, err, descr), expectedValue, a.equals(b));
            }
        }
    }

    /**
     * A method that checks the equals contract for a set of objects. These objects
     * can be equal or unequal. It is just checked if they obey the equals contract.
     * Reflexivity, symmetry and transitivity is checked. As all combinations of
     * objects are checked this method has a runtime complexity of O(n^3) where n is
     * the number of objects to be compared!
     * @param objs Array of Objects to be examined. It can contain everything. It can 
     * contain null and the same object multiple times. Elements do not
     * need to be mutual equal or unequal.
     */
    public static void assertAllObeyEqualsContract(Object[] objs) {
        assertAllObeyEqualsContract(null, objs);
    }

    /**
     * A method that checks the equals contract for a set of objects. These objects
     * can be equal or unequal. It is just checked if they obey the equals contract.
     * Reflexivity, symmetry and transitivity is checked. As all combinations of
     * objects are checked this method has a runtime complexity of O(n^3) where n is
     * the number of objects to be compared!
     * @param msg Message that will be displayed in the case of an error
     * @param objs Array of Objects to be examined. It can contain everything. It can 
     * contain null and the same object multiple times. Elements do not
     * need to be mutual equal or unequal.
     */
    public static void assertAllObeyEqualsContract(String msg, Object[] objs) {
        assertAllObeyReflexivity(msg, objs);
        assertAllObeySymmetry(msg, objs);
        assertAllObeyTransitivity(msg, objs);

    }

    /**
     * A method that checks the hashCode contract for a set of objects. These objects
     * can be equal or unequal. It is just checked if they obey the hashcode contract.
     * If two objects are equal their hashcode should be the same
     * @param objs Array of Objects to be examined. It can contain everything. It can 
     * contain null and the same object multiple times. Elements do not
     * need to be mutual equal or unequal.
     */
    public static void assertAllObeyHashCodeContract(Object objs[]) {
        assertAllObeyHashCodeContract(null, objs);
    }

    /**
     * A method that checks the hashCode contract for a set of objects. These objects
     * can be equal or unequal. It is just checked if they obey the hashcode contract.
     * If two objects are equal their hashcode should be the same.
     * @param msg Message that will be displayed in the case of an error.
     * @param objs Array of objs Objects be examined. It can contain everything. It can 
     * contain null and the same object multiple times. Elements do not
     * need to be mutual equal or unequal. 
     */
    public static void assertAllObeyHashCodeContract(String msg, Object objs[]) {
        for (int i = 0; i < objs.length; i++) {
            Object a = objs[i];
            for (int j = i + 1; j < objs.length; j++) {
                Object b = objs[j];
                String descr = "[" + i + ":" + a + "],[" + j + ":" + b + "]";
                assertObeysHashCodeContract(msg, descr, a, b);
            }
        }
    }

    private static void assertObeysHashCodeContract(String msg, String descr, Object a, Object b) {
        String err = "hashCode contract violation: equal objects return different hashCodes:";
        if (a != null && b != null) {
            if (a.equals(b)) {
                TestCase.assertEquals(concat(msg, err, descr), a.hashCode(), b.hashCode());
            }
        }
    }

    private static void assertAllObeyReflexivity(String msg, Object[] objs) {
        for (int i = 0; i < objs.length; i++) {
            Object a = objs[i];
            String descr = "[" + i + ":" + a + "]";
            assertObeysReflexivity(msg, descr, a);
        }
    }

    private static void assertObeysReflexivity(String msg, String descr, Object a) {
        String err = "Equals contract violation: Not reflexive: ";
        if (a != null) {
            TestCase.assertTrue(concat(msg, err, descr), a.equals(a));
        }
    }

    private static void assertAllObeySymmetry(String msg, Object[] objs) {
        for (int i = 0; i < objs.length; i++) {
            Object a = objs[i];
            for (int j = i + 1; j < objs.length; j++) {
                Object b = objs[j];
                String descr = "[" + i + ":" + a + "],[" + j + ":" + b + "]";
                assertObeysSymmetry(msg, descr, a, b);
            }
        }
    }

    private static void assertObeysSymmetry(String msg, String descr, Object a, Object b) {
        String err = "Equals contract violation: Not symmetrical:";
        boolean aEqualsB = (a == null ? false : a.equals(b));
        boolean bEqualsA = (b == null ? false : b.equals(a));
        TestCase.assertTrue(concat(msg, err, descr), aEqualsB == bEqualsA);
    }

    private static void assertAllObeyTransitivity(String msg, Object[] objs) {
        for (int i = 0; i < objs.length; i++) {
            Object a = objs[i];
            for (int j = i + 1; j < objs.length; j++) {
                Object b = objs[j];
                for (int k = j + 1; k < objs.length; k++) {
                    Object c = objs[k];
                    String descr = "[" + i + ":" + a + "],[" + j + ":" + b + "],[" + k + ":" + c + "]";
                    assertObeysTransitivity(msg, descr, a, b, c);
                }
            }
        }
    }

    private static void assertObeysTransitivity(String msg, String descr, Object a, Object b, Object c) {
        String err = "Equals contract violation: Not transitive:";
        boolean aEqualsB = (a == null ? false : a.equals(b));
        boolean bEqualsC = (b == null ? false : b.equals(c));
        boolean aEqualsC = (a == null ? false : a.equals(c));
        if (aEqualsB && bEqualsC) {
            TestCase.assertTrue(concat(msg, err, descr), aEqualsC);
        }
    }

    private static String concat(String msg, String err, String descr) {
        if (msg != null) {
            return msg + " " + err + " " + descr;
        } else {
            return err + " " + descr;
        }
    }
}
