/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.EventHistory;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.resource.adapter.impl.NullOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceRemovedOPR;
import com.sun.grid.grm.resource.impl.ResourceAnnotationChanged;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.util.Hostname;
import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import junit.framework.TestCase;

/**
 *
 */
public class OPRTest extends TestCase {

    private Resource resource;

    public OPRTest(String testName) {
        super(testName);
    }
    private Map<Class<? extends RAOperationResult>, String> TYPE_MAP;

    void init() {
        if (TYPE_MAP != null) {
            return;
        }
        TYPE_MAP = new HashMap<Class<? extends RAOperationResult>, String>();

        TYPE_MAP.put(AddResourceOPR.class, "addResource");
        TYPE_MAP.put(ResourceAddedOPR.class, "resourceAdded");

        TYPE_MAP.put(ResourceResetOPR.class, "resourceReset");
        TYPE_MAP.put(ResourceRejectedOPR.class, "resourceRejected");

        TYPE_MAP.put(RemoveResourceOPR.class, "removeResource");
        TYPE_MAP.put(ResourceRemovedOPR.class, "resourceRemoved");

        TYPE_MAP.put(ResourceChangedOPR.class, "resourceChanged");
        TYPE_MAP.put(ResourceErrorOPR.class, "resourceError");
        TYPE_MAP.put(NullOPR.class, null);
    }

    private RAOperationResult create(Class<? extends RAOperationResult> type, String msg) throws Exception {
        try {
            if (type.equals(NullOPR.class)) {
                return NullOPR.getInstance();
            } else if (type.equals(ResourceChangedOPR.class)) {
                Constructor<? extends RAOperationResult> cons = type.getConstructor(Resource.class, Collection.class, String.class);
                ResourceAnnotationChanged change = new ResourceAnnotationChanged("a", "b");
                return cons.newInstance(resource, Collections.singleton(change), msg);
            } else if (type.equals(RemoveResourceOPR.class) || type.equals(ResourceRemovedOPR.class)) {
                Constructor<? extends RAOperationResult> cons = type.getConstructor(Resource.class, String.class, ResourceRemovalDescriptor.class);
                return cons.newInstance(resource, msg, ResourceReassignmentDescriptor.getInstance(false));
            } else {
                Constructor<? extends RAOperationResult> cons = type.getConstructor(Resource.class, String.class);
                return cons.newInstance(resource, msg);
            }
        } catch (Throwable ex) {
            throw new IllegalStateException("Can not create SimpleOPR " + type, ex);
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        init();
        ExecutionEnv env = DummyExecutionEnvFactory.newInstance();
        resource = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testContains() throws Exception {
        for (Class<? extends RAOperationResult> type : TYPE_MAP.keySet()) {
            RAOperationResult opr = create(type, "foo");
            for (Class<? extends RAOperationResult> othertype : TYPE_MAP.keySet()) {
                if (type.equals(othertype)) {
                    assertTrue(type.getName() + " must contain " + othertype.getName(), opr.contains(othertype));
                } else {
                    assertFalse(type.getName() + " must not contain " + othertype.getName(), opr.contains(othertype));
                }
            }
        }
    }

    public void testGetResource() throws Exception {
        for (Class<? extends RAOperationResult> type : TYPE_MAP.keySet()) {
            if(type.isAssignableFrom(AbstractSimpleOPR.class)) {
                AbstractSimpleOPR opr = (AbstractSimpleOPR)create(type, "foo");
                assertEquals(resource, opr.getResource());
            }
        }
    }

    public void testGetMessage() throws Exception {
        for (Class<? extends RAOperationResult> type : TYPE_MAP.keySet()) {
            if(type.isAssignableFrom(AbstractSimpleOPR.class)) {
                AbstractSimpleOPR opr = (AbstractSimpleOPR)create(type, "foo");
                assertEquals("foo", opr.getMessage());
            }
        }
    }

    public void testEvent() throws Exception {

        EventHistory<ServiceEventListener> eventHistory = EventHistory.newInstance(ServiceEventListener.class);
        ServiceEventSupport evtSupport = ServiceEventSupport.newInstance("service", Hostname.getLocalHost());
        evtSupport.addServiceEventListener(eventHistory.getProxy());

        for (Class<? extends RAOperationResult> type : TYPE_MAP.keySet()) {
            RAOperationResult opr = create(type, "foo");
            opr.fireEvents(evtSupport);
            while(evtSupport.getNumberOfPendingEvents() > 0) {
                Thread.sleep(100);
            }
            String expectedEvent = TYPE_MAP.get(opr.getClass());
            if(expectedEvent == null) {
                assertEquals(type + " must not produce an event", 0, eventHistory.size());
            } else {
                assertTrue("missing " + TYPE_MAP.get(type) + " event call for " + type, eventHistory.hasEvent(expectedEvent, 100));
            }
            eventHistory.clear();
        }

    }

    public void testListOPR() throws Exception {

        ListOPR opr = new ListOPR();

        for (Class<? extends RAOperationResult> type : TYPE_MAP.keySet()) {
            RAOperationResult sopr = create(type, "foo");
            opr.add(sopr);
        }

        for (Class<? extends RAOperationResult> type : TYPE_MAP.keySet()) {
            assertTrue("ListOPR must contain a " + type, opr.contains(type));
        }

        EventHistory<ServiceEventListener> eventHistory = EventHistory.newInstance(ServiceEventListener.class);
        ServiceEventSupport evtSupport = ServiceEventSupport.newInstance("service", Hostname.getLocalHost());
        evtSupport.addServiceEventListener(eventHistory.getProxy());

        opr.fireEvents(evtSupport);
        for (Class<? extends RAOperationResult> type : TYPE_MAP.keySet()) {
            String expectedEvent = TYPE_MAP.get(opr.getClass());
            if(expectedEvent != null) {
                assertTrue("missing " + TYPE_MAP.get(type) + " event call for " + type, eventHistory.hasEvent(expectedEvent, 100));
            }
        }
    }
}
