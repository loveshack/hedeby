/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.adapter.InvalidResourceStateException;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.resource.adapter.ResourceTransition;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import junit.framework.TestCase;

/**
 * This test the the behavior of the state handlers in the AbstractResourceAdapter
 * It uses a DefaultResourceAdapter. To produce Resources in ERROR state
 * The MyResourceAdapter overrides the install method.
 */
public class DefaultResourceAdapterTest extends TestCase {

    private Resource resource;
    private MyResourceAdapter ra;

    public DefaultResourceAdapterTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ExecutionEnv env = DummyExecutionEnvFactory.newInstance();
        resource = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        ra = new MyResourceAdapter(resource);
    }

    public void test_UNASSIGNED_prepareInstall() throws Exception {
        ResourceTransition trans = ra.prepareInstall();
        assertTrue("prepareInstall on an UNASSIGNED resource must return a " + AddResourceOPR.class,
                trans.contains(AddResourceOPR.class));
    }

    public void test_UNASSIGNED_install() throws Exception {
        try {
            ResourceTransition trans = new DefaultResourceTransition(0, NullOPR.getInstance());
            ra.install(trans);
            fail("install on a UNASSIGNED resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_UNASSIGNED_prepareUninstall() throws Exception {
        try {
            ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
            fail("prepareUninstall on a UNASSIGNED resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_UNASSIGNED_uninstall() throws Exception {
        try {
            ResourceTransition trans = new DefaultResourceTransition(0, NullOPR.getInstance());
            ra.uninstall(trans, ResourceReassignmentDescriptor.INSTANCE);
            fail("uninstall on a UNASSIGNED resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_UNASSIGNED_prepareReset() throws Exception {
        ResourceTransition trans = ra.prepareReset();
        assertTrue("prepareReset on an UNASSIGNED resource must return a " + AddResourceOPR.class,
                trans.contains(AddResourceOPR.class));
    }

    public void test_UNASSIGNED_reset() throws Exception {
        try {
            ResourceTransition trans = new DefaultResourceTransition(0, NullOPR.getInstance());
            ra.reset(trans);
            fail("reset on a UNASSIGNED resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }
// ASSIGNING -------------------------------------------------------------------
    public void test_ASSIGNING_prepareInstall() throws Exception {
        ra.prepareInstall();
        try {
            ra.prepareInstall();
            fail("prepareInstall on a ASSIGNING resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ASSIGNING_install() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ResourceTransition t1 = ra.install(t0);
        assertTrue("reset on an ASSIGNING resource must return a " + ResourceAddedOPR.class,
                t1.contains(ResourceAddedOPR.class));
    }

    public void test_ASSIGNING_prepareUninstall() throws Exception {
        ra.prepareInstall();
        try {
            ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
            fail("prepareUninstall on a ASSIGNING resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ASSIGNING_uninstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        try {
            ra.uninstall(t0, ResourceReassignmentDescriptor.INSTANCE);
            fail("uninstall on a ASSIGNING resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ASSIGNING_prepareReset() throws Exception {
        ra.prepareInstall();
        ResourceTransition t = ra.prepareReset();
        assertTrue("reset on an ASSIGNING resource must return a " + AddResourceOPR.class,
                    t.contains(AddResourceOPR.class));
    }

    public void test_ASSIGNING_reset() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ResourceTransition t1 = ra.reset(t0);
        assertTrue("reset on an ASSIGNING resource must return a " + ResourceResetOPR.class,
                t1.contains(ResourceResetOPR.class));
    }
// ASSIGNED -------------------------------------------------------------------
    public void test_ASSIGNED_prepareInstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.install(t0);
        try {
            ra.prepareInstall();
            fail("prepareInstall on a ASSIGNED resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ASSIGNED_install() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ResourceTransition t1 = ra.install(t0);
        try {
            ra.install(t1);
            fail("install on a ASSIGNED resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ASSIGNED_prepareUninstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.install(t0);
        ResourceTransition t1 = ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
        assertTrue("prepareUninstall on an ASSIGNED resource must return a " + RemoveResourceOPR.class,
                t1.contains(RemoveResourceOPR.class));
    }

    public void test_ASSIGNED_uninstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ResourceTransition t1 = ra.install(t0);
        try {
            ra.uninstall(t1, ResourceReassignmentDescriptor.INSTANCE);
            fail("uninstall on a ASSIGNED resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ASSIGNED_prepareReset() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ResourceTransition t1 = ra.install(t0);
        ResourceTransition t2 = ra.prepareReset();
        assertTrue("reset on an ASSIGNED resource must return a " + AddResourceOPR.class,
                t2.contains(AddResourceOPR.class));
    }

    public void test_ASSIGNED_reset() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ResourceTransition t1 = ra.install(t0);
        try {
            ra.reset(t1);
            fail("reset on a ASSIGNED resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }
// UNASSIGNING -------------------------------------------------------------------
    public void test_UNASSIGNING_prepareInstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.install(t0);
        ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
        try {
            ra.prepareInstall();
            fail("prepareInstall on a UNASSIGNING resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_UNASSIGNING_install() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.install(t0);
        ResourceTransition t1 = ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
        try {
            ra.install(t1);
            fail("install on a UNASSIGNING resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_UNASSIGNING_prepareUninstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.install(t0);
        ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
        try {
            ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
            fail("prepareUninstall on a UNASSIGNING resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_UNASSIGNING_uninstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.install(t0);
        ResourceTransition t1 = ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
        ResourceTransition t2 = ra.uninstall(t1, ResourceReassignmentDescriptor.INSTANCE);
        assertTrue("uninstall on an UNASSIGNING resource must return a " + ResourceRemovedOPR.class,
                t2.contains(ResourceRemovedOPR.class));
    }

    public void test_UNASSIGNING_prepareReset() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.install(t0);
        ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
        ResourceTransition t1 = ra.prepareReset();
        assertTrue("reset on an UNASSIGNING resource must return a " + AddResourceOPR.class,
                t1.contains(AddResourceOPR.class));
    }

    public void test_UNASSIGNING_reset() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.install(t0);
        ResourceTransition t1 = ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
        try {
            ra.reset(t1);
            fail("reset on a UNASSIGNING resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }
// ERROR -------------------------------------------------------------------
    public void test_ERROR_prepareInstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.error(true).install(t0);
        try {
            ra.prepareInstall();
            fail("prepareInstall on a ERROR resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ERROR_install() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ResourceTransition t1 = ra.error(true).install(t0);
        try {
            ra.install(t1);
            fail("install on a ERROR resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ERROR_prepareUninstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.error(true).install(t0);
        try {
            ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
            fail("prepareUninstall on a ERROR resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ERROR_uninstall() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ResourceTransition t1 = ra.error(true).install(t0);
        try {
            ra.uninstall(t1, ResourceReassignmentDescriptor.INSTANCE);
            fail("uninstall on a ERROR resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    public void test_ERROR_prepareReset() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ra.error(true).install(t0);
        ResourceTransition t1 = ra.prepareReset();
        assertTrue("reset on an ERROR resource must return a " + AddResourceOPR.class,
                t1.contains(AddResourceOPR.class));
    }

    public void test_ERROR_reset() throws Exception {
        ResourceTransition t0 = ra.prepareInstall();
        ResourceTransition t1 = ra.error(true).install(t0);
        try {
            ra.reset(t1);
            fail("reset on a ERROR resource must throw an " + InvalidResourceStateException.class.getName());
        } catch (InvalidResourceStateException ex) {
            // Fine
        }
    }

    private class MyResourceAdapter extends DefaultResourceAdapter {

        private boolean error;
        
        public MyResourceAdapter(Resource resource) {
            super(resource);
        }
        
        public MyResourceAdapter error(boolean error) {
            this.error = error;
            return this;
        }

        @Override
        protected RAOperationResult doInstall() {
            if (error) {
                resource.setState(Resource.State.ERROR);
                return new ResourceErrorOPR(getResource(), "resource error");
            }
            return super.doInstall();
        }

    }
}
