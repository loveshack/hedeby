/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.ResourceIdException;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.service.descriptor.ResourceRemovalFromSystemDescriptor;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.Set;
import junit.framework.TestCase;

/**
 *
 */
public class DefaultResourceAdapterStoreTest extends TestCase {
    
    private File spoolDir;
    private ExecutionEnv env;
    public DefaultResourceAdapterStoreTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
        spoolDir = TestUtil.getTmpDir(this);
        spoolDir.mkdirs();
    }

    @Override
    protected void tearDown() throws Exception {
        Platform.getPlatform().removeDir(spoolDir, true);
    }

    private Resource createResource(String hostname) throws InvalidResourcePropertiesException, ResourceIdException {
        return DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), hostname);
    }
    
    public void testCreateResourceAdapter() throws Exception {
        
        DefaultResourceAdapterFileStore store = new DefaultResourceAdapterFileStore(spoolDir);
        Resource res = createResource("localhost");
        ResourceAdapter ra = store.createResourceAdapter(res);
        
        assertEquals("resource adapter cotains an invalid resource" , res, ra.getResource());
    }
    
    public void testCreateResourceAdapterForElement() throws Exception {
        DefaultResourceAdapterFileStore store = new DefaultResourceAdapterFileStore(spoolDir);
        Resource res = createResource("localhost");
        ResourceAdapter ra = store.createResourceAdapterForElement(res);
        
        assertEquals("resource adapter cotains an invalid resource" ,res, ra.getResource());
    }
    
    public void testStoreLoadAndRemoveLoad() throws Exception {
        
        DefaultResourceAdapterFileStore store = new DefaultResourceAdapterFileStore(spoolDir);
        Resource res = createResource("localhost");
        DefaultResourceAdapter ra = store.createResourceAdapter(res);

        store.store(ra);
        assertEquals("store must contain one element", 1, store.size());
        assertEquals("resource adapter cotains an invalid resource" ,res, ra.getResource());
        
        Set<Resource> resources = store.getResources();
        assertEquals("resource set must contain one element", 1, resources.size());

        Set<DefaultResourceAdapter> ras = store.getResourceAdapters();
        assertEquals("resource adapter set must contain one element", 1, ras.size());
        
        store.load();
        assertEquals("resource adapter cotains an invalid resource" ,res, ra.getResource());
        
        resources = store.getResources();
        assertEquals("resource set must contain one element", 1, resources.size());

        ras = store.getResourceAdapters();
        assertEquals("resource adapter set must contain one element", 1, ras.size());
        
        
        store.remove(res.getId(), ResourceRemovalFromSystemDescriptor.getInstance(false));
        assertEquals("store must be empty", 0, store.size());
        
        resources = store.getResources();
        assertEquals("resource set must not contain an element", 0, resources.size());

        ras = store.getResourceAdapters();
        assertEquals("resource adapter set must not contain an element", 0, ras.size());
        
        store.load();
        assertEquals("store must be empty", 0, store.size());
    }
}
