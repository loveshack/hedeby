/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * PriorityTest.java
 * JUnit based test
 *
 * Created on May 2, 2006, 6:47 AM
 */
package com.sun.grid.grm.resource.policy;

import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import java.util.Collections;
import java.util.List;
import junit.framework.*;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.util.filter.ConstantFilter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO (MB) review this test
 */
public class PriorityTest extends TestCase {

    private final static Logger log = Logger.getLogger(PriorityTest.class.getName());

    public PriorityTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(PriorityTest.class);

        return suite;
    }

    /**
     * Test of applyPolicy method, of class com.sun.grid.grm.resource.policy.Priority.
     * @throws java.lang.Exception 
     */
    public void testApplyPolicy() throws Exception {
        log.log(Level.FINE,"applyPolicy");
        
        Need need = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(5));
        String container1 = "1";
        String container2 = "2";
        Policy instance = new PriorityPolicy("0", container1, 49);

        instance.init(new ResourceViewImpl());
        float result1 = instance.applyPolicy(need, container1);
        assertEquals(2.45f, result1);
        float result2 = instance.applyPolicy(need, container2);
        assertEquals(1.0f, result2);
    }

    /**
     * Test of equals method, of class com.sun.grid.grm.resource.policy.Priority.
     */
    public void testEquals() throws Exception {
        log.log(Level.FINE, "equals");

        Policy policy1 = new PriorityPolicy("1", "c1", 49);
        Policy policy2 = new PriorityPolicy("2", "c2", 49);
        Policy policy3 = new PriorityPolicy("3", "c1", 51);
        Policy policy4 = new PriorityPolicy("4", "c2", 51);
        Policy policy5 = new PriorityPolicy("5", "c1", 49);

        assertTrue(policy1.equals(policy5));
        assertFalse(policy1.equals(policy2));
        assertFalse(policy1.equals(policy3));
        assertFalse(policy1.equals(policy4));
        assertFalse(policy2.equals(policy3));
        assertFalse(policy2.equals(policy4));
        assertFalse(policy3.equals(policy4));
    }

    /**
     * Test of hashCode method, of class
     * com.sun.grid.grm.resource.policy.Priority.
     */
    public void testHashCode() throws Exception {
        log.log(Level.FINE, "hashCode");

        Policy policy1 = new PriorityPolicy("1", "c1", 49);
        Policy policy2 = new PriorityPolicy("2", "c2", 49);
        Policy policy3 = new PriorityPolicy("3", "c1", 51);
        Policy policy4 = new PriorityPolicy("4", "c2", 51);
        Policy policy5 = new PriorityPolicy("5", "c1", 49);
        Policy policy6 = new PriorityPolicy("6", "c2", 49);
        Policy policy7 = new PriorityPolicy("7", "c1", 51);
        Policy policy8 = new PriorityPolicy("8", "c2", 51);

        assertTrue(policy1.hashCode() == policy5.hashCode());
        assertTrue(policy2.hashCode() == policy6.hashCode());
        assertTrue(policy3.hashCode() == policy7.hashCode());
        assertTrue(policy4.hashCode() == policy8.hashCode());
    }

    private class ResourceViewImpl implements ResourceView {

        public int getResourceCount(Resource.State state, Map<String, Object> pattern) {
            return 0;
        }

        public int getResourceCount(ComponentInfo container, Map<String, Object> pattern) {
            return 0;
        }

        public List<String> getServiceIds() {
            return Collections.<String>emptyList();
        }
    }
}
