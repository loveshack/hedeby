/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * SimplePolicyManagerTest.java
 * JUnit based test
 *
 * Created on May 2, 2006, 6:47 AM
 */

package com.sun.grid.grm.resource.policy;

import com.sun.grid.grm.config.resourceprovider.PriorityPolicyConfig;
import com.sun.grid.grm.config.resourceprovider.PriorityPolicyManagerConfig;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.util.filter.ConstantFilter;
import junit.framework.*;
import java.util.logging.Logger;

/**
 */
public class PriorityPolicyManagerTest extends TestCase {
    
    private final static Logger log = Logger.getLogger(PriorityPolicyManagerTest.class.getName());
    
    public PriorityPolicyManagerTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
    }
    
    @Override
    protected void tearDown() throws Exception {
    }
    
    public static Test suite() {
        TestSuite suite = new TestSuite(PriorityPolicyManagerTest.class);
        return suite;
    }
    
    public void testDefaultPolicyDefaultValue() {
        PriorityPolicyManagerConfig conf = new PriorityPolicyManagerConfig();
        assertEquals("default value of default priority must be 49", 49, conf.getDefaultPriority());
    }
    
    public void testDefaultPolicy() {
        Usage usage = new Usage(50);
        int priority = 30;
        
        PriorityPolicyManagerConfig conf = new PriorityPolicyManagerConfig();
        conf.setDefaultPriority(priority);
        PolicyManager pm = Policies.createManager(conf);
        Need need = new Need(ConstantFilter.<Resource>alwaysMatching(), usage);
        
        float nurg = pm.applyPolicies(need, "blubber");
        assertEquals(PriorityPolicy.applyPolicy(need, priority), nurg);
    }
    
    public void testSpecificService() {
        
        Usage usage = new Usage(50);
        int defaultPriority = 30;
        
        int priority = 50;
        String serviceName = "blubber";
        
        PriorityPolicyManagerConfig conf = new PriorityPolicyManagerConfig();
        conf.setDefaultPriority(defaultPriority);
        
        PriorityPolicyConfig pc = new PriorityPolicyConfig();
        pc.setName("ap");
        pc.setService(serviceName);
        pc.setValue(priority);
        conf.getPriority().add(pc);
        
        PolicyManager pm = Policies.createManager(conf);
        Need need = new Need(ConstantFilter.<Resource>alwaysMatching(), usage);
        
        float nurg = pm.applyPolicies(need, serviceName);
        assertEquals(PriorityPolicy.applyPolicy(need, priority), nurg);
    }
}
