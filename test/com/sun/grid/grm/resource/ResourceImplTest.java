/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;

import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.impl.ResourceImpl;
import com.sun.grid.grm.util.Hostname;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * The test tests functionality of ResourceImpl class
 */
public class ResourceImplTest extends TestCase {
    private final static Logger log = Logger.getLogger(ResourceImplTest.class.getName());

    private ExecutionEnv env;
    
    public ResourceImplTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        env = DummyExecutionEnvFactory.newInstance();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    //this test tests if its possible to clone a resource even with unresolvable id
    public void testResourceCloneWithUnresolveableHostname() throws Exception {
        
        ResourceType hrt = HostResourceType.getInstance();
        Resource hostResource = DefaultResourceFactory.createResourceByName(env, hrt, "localhost");
        Field f = ResourceImpl.class.getDeclaredField("properties");
        f.setAccessible(true);
        @SuppressWarnings("unchecked")
        HashMap<String, Object> mapa = (HashMap<String, Object>)f.get(hostResource);
        Hostname hostname = null;
        
        Constructor[] con = Hostname.class.getDeclaredConstructors();
        for (Constructor c: con) {
           c.setAccessible(true);
           for (Class clazz : c.getParameterTypes()) {
               if (clazz == String.class) {
                   do {
                        hostname = (Hostname)c.newInstance("unknownHost"+System.currentTimeMillis());
                   } while (hostname.isResolved() == true);
               }
           }
        }

        mapa.put(HostResourceType.HOSTNAME.getName(), hostname);
        f.set(hostResource, mapa);
        Resource res = hostResource.clone();
        
        assertEquals("This two properties of resources should be same",hostResource.getProperties(), res.getProperties());
        assertEquals("Annotation has to be same",hostResource.getAnnotation(),res.getAnnotation());
        assertEquals("ID has to be same",hostResource.getId(),res.getId());
        assertEquals("State has to be same",hostResource.getState(),res.getState());
        assertEquals("Type has to be same",hostResource.getType(),res.getType());
        assertEquals("Usage has to be same",hostResource.getUsage(),res.getUsage());
        assertEquals("Ambiguous flag  has to be same",hostResource.isAmbiguous(),res.isAmbiguous());
        assertEquals("Static flag  has to be same",hostResource.isStatic(),res.isStatic());
    }

}
