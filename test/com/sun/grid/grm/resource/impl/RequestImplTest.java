/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/

/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.order.OrderFactory;
import com.sun.grid.grm.resource.OrderStore;
import com.sun.grid.grm.resource.order.OrderType;
import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.RequestQueue;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceIdAndName;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.filter.OrderVariableResolver;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.resource.policy.PolicyManager;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.ServiceStoreException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.event.ServiceEventListener;

import com.sun.grid.grm.service.impl.DefaultService;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.resource.order.Order;
import com.sun.grid.grm.resource.order.ResourceRequestOrder;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.ui.resource.ResourceActionResult;
import com.sun.grid.grm.util.Deque;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import java.io.File;
import junit.framework.TestCase;

import java.lang.reflect.Field;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class RequestImplTest extends TestCase {

    private static final Logger log = Logger.getLogger(RequestImplTest.class.getName());
    private ServiceStore<ServiceMockup> services;
    private ResourceManager resources;
    private OrderStore orders;
    private PolicyManager policies;
    private ServiceMockup service1;
    private ServiceMockup service2;
    private File file;
    private ExecutionEnv env;

    public RequestImplTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        file = TestUtil.getTmpDir(this);
        env = DummyExecutionEnvFactory.newInstance();
        orders = new SimpleOrderStore();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        Platform.getPlatform().removeDir(file, true);
    }

    /**
     * Test of equals method, of class RequestImpl.
     */
    public void testEquals() {
        log.entering(RequestImplTest.class.getName(), "testEquals");

        String serviceName1 = "service1";
        String sloName1 = "slo1";

        ResourceRequestEvent event1 = new ResourceRequestEvent(0, serviceName1,
                sloName1, Collections.<Need>emptyList());

        ResourceRequestEvent event2 = new ResourceRequestEvent(0, serviceName1,
                sloName1, Collections.<Need>emptyList());

        RequestImpl<ServiceMockup> instance1 = new RequestImpl<ServiceMockup>(event1, services, resources,
                orders, policies);
        RequestImpl<ServiceMockup> instance2 = new RequestImpl<ServiceMockup>(event2, services, resources,
                orders, policies);

        assertEquals("The Requests should be equal", instance1, instance2);
        log.exiting(RequestImplTest.class.getName(), "testEquals");
    }

    /**
     * Test of equals method, of class RequestImpl.
     */
    public void testEqualsDifferentService() {
        log.entering(RequestImplTest.class.getName(), "testEqualsDifferentService");

        String serviceName1 = "service1";
        String serviceName2 = "service2";
        String sloName = "slo2";

        ResourceRequestEvent event1 = new ResourceRequestEvent(0, serviceName1,
                sloName, Collections.<Need>emptyList());

        ResourceRequestEvent event2 = new ResourceRequestEvent(0, serviceName2,
                sloName, Collections.<Need>emptyList());

        RequestImpl<ServiceMockup> instance1 = new RequestImpl<ServiceMockup>(event1, services, resources,
                orders, policies);
        RequestImpl<ServiceMockup> instance2 = new RequestImpl<ServiceMockup>(event2, services, resources,
                orders, policies);

        assertNotSame("The Requests with different service should be different",
                instance1, instance2);
        
        log.exiting(RequestImplTest.class.getName(), "testEqualsDifferentService");
    }

    /**
     * Test of equals method, of class RequestImpl.
     */
    public void testEqualsDifferentSlo() {
        log.entering(RequestImplTest.class.getName(), "equalsDifferentSlo");

        String serviceName = "service1";
        String sloName1 = "slo1";
        String sloName2 = "slo2";

        ResourceRequestEvent event1 = new ResourceRequestEvent(0, serviceName,
                sloName1, Collections.<Need>emptyList());

        ResourceRequestEvent event2 = new ResourceRequestEvent(0, serviceName,
                sloName2, Collections.<Need>emptyList());

        RequestImpl<ServiceMockup> instance1 = new RequestImpl<ServiceMockup>(event1, services, resources,
                orders, policies);
        RequestImpl<ServiceMockup> instance2 = new RequestImpl<ServiceMockup>(event2, services, resources,
                orders, policies);

        assertNotSame("The Requests with different slo should be different",
                instance1, instance2);
        
        log.exiting(RequestImplTest.class.getName(), "equalsDifferentSlo");
    }

    /**
     * Test of equals method, of class RequestImpl.
     */
    public void testEqualsDifferentSloAndService() {
        log.entering(RequestImplTest.class.getName(), "testEqualsDifferentSloAndService");

        String serviceName1 = "service1";
        String sloName1 = "slo1";
        String serviceName2 = "service2";
        String sloName2 = "slo2";

        ResourceRequestEvent event1 = new ResourceRequestEvent(0, serviceName1,
                sloName1, Collections.<Need>emptyList());

        ResourceRequestEvent event2 = new ResourceRequestEvent(0, serviceName2,
                sloName2, Collections.<Need>emptyList());

        RequestImpl<ServiceMockup> instance1 = new RequestImpl<ServiceMockup>(event1, services, resources,
                orders, policies);
        RequestImpl<ServiceMockup> instance2 = new RequestImpl<ServiceMockup>(event2, services, resources,
                orders, policies);

        assertNotSame("The Requests with different slo and service should be different",
                instance1, instance2);
        
        log.exiting(RequestImplTest.class.getName(), "testEqualsDifferentSloAndService");
    }

    /**
     * Test of hashCode method, of class RequestImpl.
     */
    public void testHashCode() {
        log.entering(RequestImplTest.class.getName(), "testHashCode");

        String serviceName = "service";
        String sloName = "slo";

        ResourceRequestEvent event = new ResourceRequestEvent(0, serviceName,
                sloName, Collections.<Need>emptyList());

        RequestImpl<ServiceMockup> instance = new RequestImpl<ServiceMockup>(event, services, resources,
                orders, policies);
        int expResult = (((7 * 53) + serviceName.hashCode()) * 53) +
                sloName.hashCode();
        int result = instance.hashCode();
        assertEquals("The hascodes should be equal", expResult, result);
        
        log.exiting(RequestImplTest.class.getName(), "testHashCode");
    }

    /**
     * Test of hashCode method, of class RequestImpl.
     */
    public void testHashCodeNullSlo() {
        log.entering(RequestImplTest.class.getName(), "testHashCodeNullSlo");

        String serviceName = "service";
        String sloName = null;

        ResourceRequestEvent event = new ResourceRequestEvent(0, serviceName,
                sloName, Collections.<Need>emptyList());

        RequestImpl<ServiceMockup> instance = new RequestImpl<ServiceMockup>(event, services, resources,
                orders, policies);
        int expResult = ((7 * 53) + serviceName.hashCode()) * 53;
        int result = instance.hashCode();
        assertEquals("The hascodes should be equal", expResult, result);
        
        log.exiting(RequestImplTest.class.getName(), "testHashCodeNullSlo");
    }

    /**
     * Test of getServiceName method, of class RequestImpl.
     */
    public void testGetServiceName() {
        log.entering(RequestImplTest.class.getName(), "testGetServiceName");
        
        log.info("getServiceName");

        String expResult = "service";
        ResourceRequestEvent event = new ResourceRequestEvent(0, expResult,
                "slo", Collections.<Need>emptyList());

        RequestImpl<ServiceMockup> instance = new RequestImpl<ServiceMockup>(event, services, resources,
                orders, policies);
        String result = instance.getServiceName();
        assertEquals("The service name should be equal", expResult, result);

        log.exiting(RequestImplTest.class.getName(), "testGetServiceName");
    }

    /**
     * Test of getSLOName method, of class RequestImpl.
     */
    public void testGetSloName() {
        log.entering(RequestImplTest.class.getName(), "testGetSloName");

        String expResult = "slo";
        ResourceRequestEvent event = new ResourceRequestEvent(0, "service",
                expResult, Collections.<Need>emptyList());

        RequestImpl<ServiceMockup> instance = new RequestImpl<ServiceMockup>(event, services, resources,
                orders, policies);
        String result = instance.getSLOName();
        assertEquals("The slo name should be equal", expResult, result);
        
        log.exiting(RequestImplTest.class.getName(), "testGetSloName");
    }

    /**
     * Test of getNeeds method, of class RequestImpl.
     */
    public void testGetNeeds() {
        log.entering(RequestImplTest.class.getName(), "testGetNeeds");

        // todo make it completely right, create a RRE mockup
        List<Need> expResult = new ArrayList<Need>(1);
        Need n = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(99), 1);
        expResult.add(n);

        ResourceRequestEvent event = new ResourceRequestEvent(0, "service",
                "slo", expResult);
        RequestImpl<ServiceMockup> instance = new RequestImpl<ServiceMockup>(event, services, resources,
                orders, policies);
        List<Need> result = instance.getCurrentNeeds();
        assertEquals("The event and request should have the same need count", expResult.size(), result.size());
        assertTrue("The request has to contain all needs from event", result.containsAll(expResult));
        
        log.exiting(RequestImplTest.class.getName(), "testGetNeeds");
    }

    /**
     * Tests the processing of a request.
     * Functional test, relies on concrete implementations.
     *
     * Exceptions and results are documented in the body of the method.
     *
     * @throws java.lang.Exception
     */
    @SuppressWarnings("unchecked")
    public void testExecute() throws Exception {
        log.entering(RequestImplTest.class.getName(), "testExecute");
        
        services = new ServiceStoreMockup();
        resources = new ResourceManagerMockup(services);
        policies = new PolicyManagerMockup();
        service1 = new ServiceMockup("service1", 99);
        service2 = new ServiceMockup("service2", 1);
        services.addService(service1);
        services.addService(service2);
        String sloName ="slo";
        Resource r1 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        r1.setState(Resource.State.ASSIGNED);

        Resource r2 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        r2.setState(Resource.State.ASSIGNED);
        service1.addResource(r1);
        service2.addResource(r2);

        // todo make it completely right, create a RRE mockup
        List<Need> needs1 = new ArrayList<Need>(1);
        Need n1 = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(99), 1);
        needs1.add(n1);

        ResourceRequestEvent event1 = new ResourceRequestEvent(0,
                service1.getName(), sloName, needs1);

        Request request1 = new RequestImpl(event1, services, resources, orders,
                policies);
        boolean result = request1.execute();
        assertFalse("The first request should not be executed successfully, order are not commited", result);
        assertEquals("There must exist one order", 1, orders.size());

        // commit the order
        Order order = orders.get(OrderVariableResolver.newTargetFilter(service1.getName()));
        
        assertNotNull("There must exist an order for service1", order);
        ((ResourceRequestOrder)order).commit();
        
        assertEquals("Need processor must have one commited order", 1, request1.getNeedProcessors().get(0).getCommitedQuantity());
        
        result = request1.execute();
        assertTrue("The first request should be executed successfully", result);
        
        // after first run, the system should be in state:
        // 1. request has to have zero need
        // 2. service2 has to have 0 resources
        // 3. service1 has to have 1 resource
        // 4. orderstore has to have order for resourcer2 to be assigned to service1
        assertTrue("The first request should have no need", request1.getNeedProcessors().isEmpty());

        assertTrue("The service2 should have no resources", service2.list.isEmpty());

        assertEquals("There should be just one order stored in order store", 1, orders.size());

        Order o = orders.get(OrderVariableResolver.newTargetFilter(service1.getName()));
        assertNotNull("There must exist an order for service1", o);
        assertTrue("Order should be designated for resource2",
                o.getResourceId().equals(r2.getId()));
        assertTrue("Order's target should be service1",
                o.getTarget().equals(service1.getName()));
        assertTrue("Order's type should be ASSIGN",
                o.getType().equals(OrderType.ASSIGN));

        //this test was added to test the API change implemented for issue 608
        assertTrue("Order should be an ResourceRequestOrder", ResourceRequestOrder.class.isAssignableFrom(o.getClass()));
        assertTrue("Order should contain the sloName as it is an assignment order.",sloName.equals(((ResourceRequestOrder)o).getSloName()));


        // todo make it completely right, create a RRE mockup
        List<Need> needs2 = new ArrayList<Need>(1);
        Need n2 = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(99), 1);
        needs2.add(n2);

        // make the new resource request come from a "new" SLO (slo1) to avoid that
        // the obsolete order in the order store gets matched
        // TODO we need to test more combinations of new requests with same need
        //      and from same slo/service combination (need processor reusing)
        ResourceRequestEvent event2 = new ResourceRequestEvent(0,
                service1.getName(), "slo1", needs2);

        Request request2 = new RequestImpl(event2, services, resources, orders,
                policies);
        result = request2.execute();
        assertFalse("The second request should not be executed successfully",
                result);
        // after second run, the system should be in state:
        // 1. request has to have 1 need
        // 2. service2 has to have 0 resources
        // 3. service1 has to have 1 resource
        // 4. orderstore has to have order for resourcer2 to be assigned to service1
        assertEquals("The second request should have just one need", 1, request2.getNeedProcessors().size());
        assertTrue("The service2 should have no resources", service2.list.isEmpty());
        assertEquals("The service1 should have just one resource", 1, service1.list.size());

        assertEquals("There should be just one order stored in order store", 1, orders.size());

        o = orders.get(OrderVariableResolver.newTargetFilter(service1.getName()));
        assertNotNull("There must be an order with service1 as target in the order store", o);
        assertTrue("Order should be designated for resource2",
                o.getResourceId().equals(r2.getId()));
        assertTrue("Order's target should be service1",
                o.getTarget().equals(service1.getName()));
        assertTrue("Order's type should be ASSIGN",
                o.getType().equals(OrderType.ASSIGN));
        
        log.exiting(RequestImplTest.class.getName(), "testExecute");
    }

    /**
     * This test checks that the double execution of a resource request with
     * uncommitted existing orders does not create additional orders.
     * 
     * <p>The problem is described in issue 649. If the time needed for the commit
     * of the order is longer than the SLO update interval it happened that
     * orders for too many resources for the need were created.</p> 
     * 
     * <p>The test performs the following steps:</p>
     * 
     * <ul>
     *   <li>There exists two services (service1 and service2)</li>
     *   <li>service2 owns two resources (r1 and r2)</li>
     *   <li>service1 has a need for one resource, this need can be fulfilled with
     *      r1 or r2</li>
     *   <li>A resource request is processed, an order for r1 or r2 must be created</li>
     *   <li>The order will be not committed</li>
     *   <li>The resource request is processed again, no new order must be created</li>
     * </ul>
     * 
     * @throws java.lang.Exception
     */
    @SuppressWarnings("unchecked")
    public void testDoubleExecute_issue649() throws Exception {
        log.entering(RequestImplTest.class.getName(), "testDoubleExecute_issue649");
        
        services = new ServiceStoreMockup();
        resources = new ResourceManagerMockup(services);
        policies = new PolicyManagerMockup();
        service1 = new ServiceMockup("service1", 99);
        service2 = new ServiceMockup("service2", 1);
        services.addService(service1);
        services.addService(service2);
        String sloName ="slo";
        Resource r1 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        r1.setState(Resource.State.ASSIGNED);

        Resource r2 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        r2.setState(Resource.State.ASSIGNED);
        
        // Add resource r1 and r2 to service2
        service2.addResource(r1);
        service2.addResource(r2);

        List<Need> needs1 = new ArrayList<Need>(1);
        Need n1 = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(99), 1);
        needs1.add(n1);

        ResourceRequestEvent event1 = new ResourceRequestEvent(0,
                service1.getName(), sloName, needs1);

        Request request1 = new RequestImpl(event1, services, resources, orders,
                policies);
        boolean result = request1.execute();
        assertFalse("The first request should not be executed successfully, order is not commited", result);
        assertEquals("There must exist one order", 1, orders.size());
        
        result = request1.execute();
        assertFalse("The second execution of the request should not be executed successfully, order is not commited", result);
        assertEquals("There must still exist one order", 1, orders.size());
        
        log.exiting(RequestImplTest.class.getName(), "testDoubleExecute_issue649");
    }
    
    /**
     * Tests the processing of a request that has 0 quantity.
     * Functional test, relies on concrete implementations.
     *
     * Exceptions and results are documented in the body of the method.
     *
     * @throws java.lang.Exception
     */
    @SuppressWarnings("unchecked")
    public void testExecuteZeroQuantity() throws Exception {
        log.entering(RequestImplTest.class.getName(), "testExecuteZeroQuantity");
        
        services = new ServiceStoreMockup();
        resources = new ResourceManagerMockup(services);
        policies = new PolicyManagerMockup();
        service1 = new ServiceMockup("service1", 99);
        service2 = new ServiceMockup("service2", 1);
        services.addService(service1);
        services.addService(service2);

        Resource r1 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        r1.setState(Resource.State.ASSIGNED);

        Resource r2 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        r2.setState(Resource.State.ASSIGNED);
        service1.addResource(r1);
        service2.addResource(r2);

        // todo make it completely right, create a RRE mockup
        List<Need> needs1 = new ArrayList<Need>(1);
        Need n1 = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(99), 0);
        needs1.add(n1);

        ResourceRequestEvent event1 = new ResourceRequestEvent(0,
                service1.getName(), "slo", needs1);

        Request request1 = new RequestImpl(event1, services, resources, orders,
                policies);
        boolean result = request1.execute();
        assertTrue("The first request should be executed successfully", result);

        // after first run, the system should be in state:
        // 1. request has to have zero need
        // 2. service2 has to have 1 resources
        // 3. service1 has to have 1 resource
        // 4. orderstore has to have no order for resource2
        // 5. request queue should have no pending request from service1 - this
        // is matter of RequestQueueImpl$Caller.call method, no check here

        assertTrue("The first request should have no need", request1.getNeedProcessors().isEmpty());
        assertEquals("The service2 should have 1 resource", 1, service2.list.size());
        assertEquals("The service1 should have 1 resource", 1, service1.list.size());
        assertEquals("There should be no order stored in order store", 0, orders.size());

        log.exiting(RequestImplTest.class.getName(), "testExecuteZeroQuantity");
    }

    /**
     * Tests the processing of a request that contains empty need list.
     * Functional test, relies on concrete implementations.
     *
     * Exceptions and results are documented in the body of the method.
     *
     * @throws java.lang.Exception
     */
    @SuppressWarnings("unchecked")
    public void testExecuteZeroLengthNeedList() throws Exception {
        log.entering(RequestImplTest.class.getName(), "testExecuteZeroLengthNeedList");

        services = new ServiceStoreMockup();
        resources = new ResourceManagerMockup(services);
        policies = new PolicyManagerMockup();
        service1 = new ServiceMockup("service1", 99);
        service2 = new ServiceMockup("service2", 1);
        services.addService(service1);
        services.addService(service2);

        Resource r1 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        r1.setState(Resource.State.ASSIGNED);

        Resource r2 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        r2.setState(Resource.State.ASSIGNED);
        service1.addResource(r1);
        service2.addResource(r2);

        // todo make it completely right, create a RRE mockup
        List<Need> needs1 = Collections.<Need>emptyList();

        ResourceRequestEvent event1 = new ResourceRequestEvent(0,
                service1.getName(), "slo", needs1);

        Request request1 = new RequestImpl(event1, services, resources, orders,
                policies);
        boolean result = request1.execute();
        assertTrue("The first request should be executed successfully", result);

        // after first run, the system should be in state:
        // 1. request has to have zero need
        // 2. service2 has to have 1 resources
        // 3. service1 has to have 1 resource
        // 4. orderstore has to have no order for resource2
        // 5. request queue should have no pending request from service1 - this
        // is matter of RequestQueueImpl$Caller.call method, no check here
        assertTrue("The first request should have no need", request1.getNeedProcessors().isEmpty());

        assertEquals("The service2 should have 1 resource", 1, service2.list.size());
        assertEquals("The service1 should have 1 resource", 1, service1.list.size());
        assertEquals("There should be no order stored in order store", 0, orders.size());
        
        log.exiting(RequestImplTest.class.getName(), "testExecuteZeroLengthNeedList");
    }

    /**
     * Tests the fact that during a request processing, after an order is stored in
     * order store, and an error occurs during a call to "Service.removeResource()",
     * correct order is removed and previous one (normal order) is still available.
     * Functional test, relies on concrete implementations.
     *
     * Exceptions and results are documented in the body of the method.
     *
     * @throws java.lang.Exception
     */
    @SuppressWarnings({"unchecked"})
    public void testExecute_rollBackNormalOrder() throws Exception {
        log.entering(RequestImplTest.class.getName(), "testExecute_rollBackNormalOrder");
        
        services = new ServiceStoreMockup();
        resources = new ResourceManagerMockup(services);
        policies = new PolicyManagerMockup();
        service1 = new ServiceMockup("service1", 99);
        service2 = new ServiceMockup("service2", 1, true);
        services.addService(service1);
        services.addService(service2);

        ResourceType trt = DefaultResourceFactory.getResourceType("test");

        Resource r1 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "hostA");
        r1.setState(Resource.State.ASSIGNED);

        Resource r2 = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "hostB");
        r2.setState(Resource.State.ASSIGNED);
        service1.addResource(r1);
        service2.addResource(r2);

        // todo make it completely right, create a RRE mockup
        List<Need> needs1 = new ArrayList<Need>(1);
        Need n1 = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(99), 1);
        needs1.add(n1);

        ResourceRequestEvent event1 = new ResourceRequestEvent(0,
                service1.getName(), "slo", needs1);

        Request request1 = new RequestImpl(event1, services, resources, orders,
                policies);
              
        

        orders.add(OrderFactory.createResourceRequestAssignOrder(r2.getId(), "service2", "target", new NeedProcessor(n1), "slo"));

        boolean result = request1.execute();
        assertFalse("The first request should not be executed successfully", result);

        // after first run, the system should be in state:
        // 1. request has to have 1 need
        // 2. service2 has to have 1 resource
        // 3. service1 has to have 1 resource
        // 4. orderstore has to have order for resource2 to be assigned to target
        List<NeedProcessor> hlp1 = null;
        Field f = AbstractRequest.class.getDeclaredField("needProcessors");
        f.setAccessible(true);
        hlp1 = (List<NeedProcessor>) f.get(request1);
        f.setAccessible(false);
        assertTrue("The first request should have a need", hlp1.size() != 0);

        List<Service> hlp2 = null;
        f = ServiceMockup.class.getDeclaredField("list");
        f.setAccessible(true);
        hlp2 = (List<Service>) f.get(service2);
        f.setAccessible(false);
        assertTrue("The service2 should have just one resource", hlp2.size() == 1);

        List<Service> hlp3 = null;
        f = ServiceMockup.class.getDeclaredField("list");
        f.setAccessible(true);
        hlp3 = (List<Service>) f.get(service1);
        f.setAccessible(false);
        assertTrue("The service1 should have just one resource",
                hlp3.size() == 1);

        Map<String, Deque<Order>> hlp4 = null;
        f = SimpleOrderStore.class.getDeclaredField("orders");
        f.setAccessible(true);
        hlp4 = (Map<String, Deque<Order>>) f.get(orders);
        f.setAccessible(false);
        assertTrue("There should be just one order stored in order store",
                hlp4.size() == 1);

        Order o = hlp4.entrySet().iterator().next().getValue().getFirst();
        assertTrue("Order should be designated for resource2",
                o.getResourceId().equals(r2.getId()));
        assertTrue("Order's target should be target",
                o.getTarget().equals("target"));
        assertTrue("Order's type should be ASSIGN",
                o.getType().equals(OrderType.ASSIGN));

        log.exiting(RequestImplTest.class.getName(), "testExecute_rollBackNormalOrder");
    }

    /**
     * Tests the fact that during a request processing, after an order is stored in
     * order store, and an error occurs during a call to "Service.removeResource()",
     * correct order is removed and previous one (maintenance order) is still available.
     * Functional test, relies on concrete implementations.
     *
     * Exceptions and results are documented in the body of the method.
     *
     * @throws java.lang.Exception
     */
    @SuppressWarnings({"unchecked"})
    public void testExecute_rollBackMaintenanceOrder() throws Exception {
        log.entering(RequestImplTest.class.getName(), "testExecute_rollBackMaintenanceOrder");

        services = new ServiceStoreMockup();
        resources = new ResourceManagerMockup(services);
        policies = new PolicyManagerMockup();
        service1 = new ServiceMockup("service1", 99);
        service2 = new ServiceMockup("service2", 1, true);
        services.addService(service1);
        services.addService(service2);

        ResourceType trt = DefaultResourceFactory.getResourceType("test");

        Resource r1 = DefaultResourceFactory.createResourceByName(env, trt, "hostA");
        r1.setState(Resource.State.ASSIGNED);
        
        Resource r2 = DefaultResourceFactory.createResourceByName(env, trt, "hostB");
        r2.setState(Resource.State.ASSIGNED);
        service1.addResource(r1);
        service2.addResource(r2);

        // todo make it completely right, create a RRE mockup
        List<Need> needs1 = new ArrayList<Need>(1);
        Need n1 = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(99), 1);
        needs1.add(n1);

        ResourceRequestEvent event1 = new ResourceRequestEvent(0,
                service1.getName(), "slo", needs1);

        Request request1 = new RequestImpl(event1, services, resources, orders,
                policies);


        orders.add(OrderFactory.createResourceRequestAssignOrder(r2.getId(), "service2", "target", new NeedProcessor(n1), "slo"));

        boolean result = request1.execute();
        assertFalse("The first request should not be executed successfully", result);

        // after first run, the system should be in state:
        // 1. request has to have 1 need
        // 2. service2 has to have 1 resource
        // 3. service1 has to have 1 resource
        // 4. orderstore has to have order for resource2 to be assigned to target
        List<NeedProcessor> hlp1 = null;
        Field f = AbstractRequest.class.getDeclaredField("needProcessors");
        f.setAccessible(true);
        hlp1 = (List<NeedProcessor>) f.get(request1);
        f.setAccessible(false);
        assertTrue("The first request should have a need", hlp1.size() != 0);

        List<Service> hlp2 = null;
        f = ServiceMockup.class.getDeclaredField("list");
        f.setAccessible(true);
        hlp2 = (List<Service>) f.get(service2);
        f.setAccessible(false);
        assertTrue("The service2 should have just one resource", hlp2.size() == 1);

        List<Service> hlp3 = null;
        f = ServiceMockup.class.getDeclaredField("list");
        f.setAccessible(true);
        hlp3 = (List<Service>) f.get(service1);
        f.setAccessible(false);
        assertTrue("The service1 should have just one resource",
                hlp3.size() == 1);

        Map<String, Deque<Order>> hlp4 = null;
        f = SimpleOrderStore.class.getDeclaredField("orders");
        f.setAccessible(true);
        hlp4 = (Map<String, Deque<Order>>) f.get(orders);
        f.setAccessible(false);
        assertTrue("There should be just one order stored in order store",
                hlp4.size() == 1);

        Order o = hlp4.entrySet().iterator().next().getValue().getFirst();
        assertTrue("Order should be designated for resource2",
                o.getResourceId().equals(r2.getId()));
        assertTrue("Order's target should be target",
                o.getTarget().equals("target"));
        assertTrue("Order's type should be ASSIGN",
                o.getType().equals(OrderType.ASSIGN));
        
        log.exiting(RequestImplTest.class.getName(), "testExecute_rollBackMaintenanceOrder");
    }
    
    /**
     * This test is testing that both resources with same urgency are candidates for move order
     * and if there is no other order created for the resource which order is already stored in
     * orderstore.
     * 
     * Tests that two resources with same urgency are candidates for a move order and
     * that no other order is created for a resource that has already an order in
     * the order store
     * @throws java.lang.Exception
     */
    @SuppressWarnings({"unchecked"})
    public void testResourcesWithSameUrgency() throws Exception {
        log.entering(RequestImplTest.class.getName(), "testResourcesWithSameUrgency");
        
        services = new ServiceStoreMockup();
        resources = new ResourceManagerMockup(services);
        policies = new PolicyManagerMockup();
        service1 = new ServiceMockup("service1", 99);
        service2 = new ServiceMockup("service2", 1);
        services.addService(service1);
        services.addService(service2);

        ResourceType trt = DefaultResourceFactory.getResourceType("test");

        Resource r1 = DefaultResourceFactory.createResourceByName(env, trt, "hostA");
        r1.setState(Resource.State.ASSIGNED);

        
        Resource r2 = DefaultResourceFactory.createResourceByName(env, trt, "hostB");
        r2.setState(Resource.State.ASSIGNED);
        service2.addResource(r1);
        service2.addResource(r2);

        // todo make it completely right, create a RRE mockup
        List<Need> needs1 = new ArrayList<Need>(1);
        Need n1 = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(99), 10);
        needs1.add(n1);

        ResourceRequestEvent event1 = new ResourceRequestEvent(0,
                service1.getName(), "slo", needs1);

        Request request1 = new RequestImpl(event1, services, resources, orders,
                policies);
        
       
        r1.setUsage(new Usage(50));
        r2.setUsage(new Usage(50));
        
        request1.execute();
        
        assertEquals("Awaited quantity of need processor should be 2", 2, request1.getNeedProcessors().get(0).getAwaitedQuantity());
        assertEquals("Number of orders should be 2", 2, orders.size());
        service2.addResource(r1);
        request1.execute();
        assertEquals("Awaited quantity of need processor should still be 2", 2, request1.getNeedProcessors().get(0).getAwaitedQuantity());
        assertEquals("Number of orders should still be 2", 2, orders.size());
        Resource r3 = DefaultResourceFactory.createResourceByName(env, trt, "hostC");
        r3.setState(Resource.State.ASSIGNED);
        service2.addResource(r3);
        request1.execute();
        assertEquals("Awaited quantity of need processor should be increased to 3", 3, request1.getNeedProcessors().get(0).getAwaitedQuantity());
        assertEquals("Number of orders should be increased to 3", 3, orders.size());
        
        log.exiting(RequestImplTest.class.getName(), "testResourcesWithSameUrgency");
    }

    private class ServiceMockup implements Service {

        private final String name;
        private final int usage;
        private final boolean snae;
        private final List<Resource> list = new LinkedList<Resource>();

        public ServiceMockup(String name, int usage) {
            super();
            this.name = name;
            this.usage = usage;
            this.snae = false;
        }

        public ServiceMockup(String name, int usage, boolean snae) {
            super();
            this.name = name;
            this.usage = usage;
            this.snae = snae;
        }

        public void startService() throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void stopService(boolean isFreeResources)
                throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public ServiceState getServiceState() {
            return ServiceState.RUNNING;
        }

        public void addServiceEventListener(ServiceEventListener eventListener) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void removeServiceEventListener(
                ServiceEventListener eventListener) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResources() throws ServiceNotActiveException {
            return list;
        }

        public Resource getResource(ResourceId resourceId)
                throws UnknownResourceException, ServiceNotActiveException {
            for (Resource r : list) {
                if (r.getId().equals(resourceId)) {
                    return r;
                }
            }

            throw new UnknownResourceException();
        }

        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr)
                throws UnknownResourceException, ServiceNotActiveException {
            if (snae) {
                throw new ServiceNotActiveException();
            }
            Iterator<Resource> iterator = list.iterator();
            boolean found = false;

            while (iterator.hasNext()) {
                Resource r = iterator.next();

                if (r.getId().equals(resourceId)) {
                    iterator.remove();
                    found = true;

                    break;
                }
            }

            if (!found) {
                throw new UnknownResourceException();
            }
        }

        public void resetResource(ResourceId resource)
                throws UnknownResourceException, ServiceNotActiveException {
            for (Resource r : list) {
                if (r.getId().equals(resource)) {
                    r.setState(Resource.State.ASSIGNED);
                }
            }

            throw new UnknownResourceException();
        }

        public void addResource(Resource resource)
                throws InvalidResourceException, ServiceNotActiveException {
            list.add(resource);
        }

        public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        /**
         * This method is added because of an Interface change in service
         * @param resource to be added
         * @param sloName if the resource is added because of a SLO request it contains the SLO name
         * @throws com.sun.grid.grm.resource.InvalidResourceException
         * @throws com.sun.grid.grm.service.ServiceNotActiveException
         */
        public void addResource(Resource resource,String sloName)
                throws InvalidResourceException, ServiceNotActiveException {
            addResource(resource);
        }


        public void start() throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void stop(boolean isForced) throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void reload(boolean isForced) throws GrmException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public ComponentState getState() {
            return ComponentState.STARTED;
        }

        public String getName() {
            return name;
        }

        public void modifyResource(ResourceId resource, ResourceChangeOperation opeartions) throws UnknownResourceException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<SLOState> getSLOStates() throws GrmRemoteException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResources(Filter<Resource> filter) throws ServiceNotActiveException, GrmRemoteException {
            List<Resource> ret = new ArrayList<Resource>(list.size());
            ResourceVariableResolver resolver = new ResourceVariableResolver();
            for (Resource res : list) {
                resolver.setResource(res);
                if (filter.matches(resolver)) {
                    ret.add(res);
                }
            }
            return ret;
        }

        public void addComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void removeComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Hostname getHostname() throws GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws ServiceNotActiveException, UnknownResourceException, GrmRemoteException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private class ServiceStoreMockup implements ServiceStore<ServiceMockup> {

        private final List<ServiceMockup> services = new LinkedList<ServiceMockup>();

        public ServiceMockup addService(ServiceMockup service) {
            services.add(service);

            return service;
        }

        public ServiceMockup removeService(String serviceName)
                throws UnknownServiceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public Set<String> getServiceNames() {
            Set<String> set = new HashSet<String>();

            for (ServiceMockup s : services) {
                set.add(s.getName());
            }

            return set;
        }

        public ServiceMockup getService(String serviceName)
                throws UnknownServiceException {
            for (ServiceMockup s : services) {
                if (s.getName().equals(serviceName)) {
                    return s;
                }
            }
            throw new UnknownServiceException();
        }

        public List<ServiceMockup> getServices() {
            return services;
        }

        public void clear() throws ServiceStoreException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }
    }

    private class ResourceManagerMockup implements ResourceManager {

        public Resource getResourceByIdOrName(String idOrName) throws ServiceNotActiveException, InvalidResourceException, UnknownResourceException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public ResourceActionResult modifyResource(ResourceId resId, Map<String, Object> properties) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> moveResources(List<String> resIdOrName, String service, boolean isFored, boolean isStatic) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> putResourcesOnBlackList(List<String> idOrNameList, String service) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> removeResources(List<String> resIdOrName, ResourceRemovalDescriptor descr) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> removeResourcesFromBlackList(List<String> idOrNameList, String service) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> resetResources(List<String> idOrNameList) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        private ServiceStore services;
        private final Map<String, Resource> inprocess = new HashMap<String, Resource>();
        private final DefaultService defaultService;
        
        public ResourceManagerMockup(ServiceStore s) {
            services = s;
            defaultService = new DefaultService(env, "rp", new SimpleResourceStore());
        }

        public boolean resetResource(ResourceId resource)
                throws UnknownResourceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean resetResource(ResourceId resource, String service)
                throws UnknownResourceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean modifyResource(ResourceId resource,
                ResourceChangeOperation opeartions) throws UnknownResourceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public Resource getResource(ResourceId id)
                throws UnknownResourceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResources(String service)
                throws UnknownServiceException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean moveResource(String service, ResourceId resourceId,
                boolean isForced, boolean isStatic)
                throws UnknownResourceException, UnknownServiceException,
                ServiceNotActiveException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResourcesInProcess() {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void addResourceProviderEventListener(
                ResourceProviderEventListener lis) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void removeResourceProviderEventListener(
                ResourceProviderEventListener lis) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceAddedEvent(String service, Resource added) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceChangedEvent(String service, Resource modified, Collection<ResourceChanged> changes) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceErrorEvent(String serviceName, Resource bad, String message) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceRemovedEvent(String serviceName, Resource released) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processAddResourceEvent(String serviceName, Resource resource) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processRemoveResourceEvent(String serviceName, Resource resource) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceRejectedEvent(String serviceName, Resource rejected) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceResetEvent(String serviceName, Resource reset) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processOrphanedResources() {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean registerResourceID(ResourceId resourceId, String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean unregisterResourceID(ResourceId resourceId, String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean removeResource(ResourceId resourceId, String service, ResourceRemovalDescriptor descr) throws UnknownResourceException, UnknownServiceException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean addResourceToProcess(String source, Resource resource) {
            inprocess.put(source, resource);
            return true;
        }

        public boolean removeResourceFromProcess(String source, ResourceId resource) {
            Resource r = inprocess.get(source);
            if (r != null) {
                if (r.getId().equals(resource)) {
                    inprocess.remove(source);
                    return true;
                }
            }
            return false;
        }

        public int putResourceIDOnBlackList(ResourceId resourceId, String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public int removeResourceIDFromBlackList(ResourceId resourceId, String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean isResourceIDOnBlackList(ResourceId resourceId, String service) {
            return false;
        }

        public void start() {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void stop() {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processResourceRequestEvent(ResourceRequestEvent event) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void setRequestQueue(RequestQueue rq) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void triggerRequestQueueReprocessing() {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public Resource getResource(String serviceName, ResourceId resourceId) throws UnknownServiceException, ServiceNotActiveException, UnknownResourceException, InvalidServiceException {
            try {
                Service s = services.getService(serviceName);
                return s.getResource(resourceId);
            } catch (Exception ex) {
                return null;
            }
        }

        public boolean addResource(String service, Resource resource) throws InvalidResourceException, UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public boolean addResource(String service, Resource resource,String snloName) throws InvalidResourceException, UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }
        public List<ResourceIdAndName> getBlackList(String service) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResources(String service, Filter filter) throws UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public List<Resource> getResourcesInProcess(Filter filter) {
            throw new UnsupportedOperationException("Not needed for tests.");
        }

        public void processPurgeOrdersAndRequestsEvent(String source) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<ResourceActionResult> modifyResources(Map<String, Map<String, Object>> resIdOrNameMapWithProperties) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public String getName() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void addResource(Resource resource) throws ServiceNotActiveException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public DefaultService getDefaultService() {
            return defaultService;
        }

        public List<Resource> getUnassignedResources(Filter<Resource> filter) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        
    }
}
