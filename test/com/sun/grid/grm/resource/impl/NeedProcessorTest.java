/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Junit test for the NeedProcessor
 */
public class NeedProcessorTest extends TestCase {
    
    private final Logger log = Logger.getLogger(NeedProcessorTest.class.getName());
    
    public NeedProcessorTest(String testName) {
        super(testName);
    }            

    public void testSingleResourceAwaitCommit() throws Exception {
        log.entering(NeedProcessorTest.class.getName(), "testSingleResourceAwaitCommit");
        
        NeedProcessor np = new NeedProcessor(new Need(Usage.MIN_VALUE, 1));
        
        assertEquals("awaited quantity must be 0", 0, np.getAwaitedQuantity());
        assertEquals("commited quantity must be 0", 0, np.getCommitedQuantity());
        assertEquals("remaining quantity must be 1", 1, np.getRemainingQuantity());
        assertFalse("Full quantity is not awaited", np.isFullQuantityAwaited());
        
        ResourceId resId = new ResourceIdImpl(1);
        
        np.addAwaitedResource(resId);
        
        assertEquals("awaited quantity must be 1", 1, np.getAwaitedQuantity());
        assertEquals("commited quantity must be 0", 0, np.getCommitedQuantity());
        assertEquals("remaining quantity must be 1", 1, np.getRemainingQuantity());
        assertTrue("Full quantity is awaited", np.isFullQuantityAwaited());
        
        np.commitAwaitedResource(resId);
        
        assertEquals("awaited quantity must be 0", 0, np.getAwaitedQuantity());
        assertEquals("commited quantity must be 1", 1, np.getCommitedQuantity());
        assertEquals("remaining quantity must be 0", 0, np.getRemainingQuantity());
        assertTrue("Full quantity is awaited", np.isFullQuantityAwaited());
        
        log.exiting(NeedProcessorTest.class.getName(), "testSingleResourceAwaitCommit");
    }
    
    public void testSingleResourceAwaitCancel() throws Exception {
        log.entering(NeedProcessorTest.class.getName(), "testSingleResourceAwaitCancel");
        
        NeedProcessor np = new NeedProcessor(new Need(Usage.MIN_VALUE, 1));
        
        assertEquals("awaited quantity must be 0", 0, np.getAwaitedQuantity());
        assertEquals("commited quantity must be 0", 0, np.getCommitedQuantity());
        assertEquals("remaining quantity must be 1", 1, np.getRemainingQuantity());
        assertFalse("Full quantity is not awaited", np.isFullQuantityAwaited());
        
        ResourceId resId = new ResourceIdImpl(1);
        
        np.addAwaitedResource(resId);
        
        assertEquals("awaited quantity must be 1", 1, np.getAwaitedQuantity());
        assertEquals("commited quantity must be 0", 0, np.getCommitedQuantity());
        assertEquals("remaining quantity must be 1", 1, np.getRemainingQuantity());
        assertTrue("Full quantity is awaited", np.isFullQuantityAwaited());
        
        np.cancelAwaitedResource(resId);
        
        assertEquals("awaited quantity must be 0", 0, np.getAwaitedQuantity());
        assertEquals("commited quantity must be 1", 0, np.getCommitedQuantity());
        assertEquals("remaining quantity must be 0", 1, np.getRemainingQuantity());
        assertFalse("Full quantity is awaited", np.isFullQuantityAwaited());
        assertTrue("NP must know the resource as canceled resource", np.hadCanceledOrderForResource(resId));
        
        log.exiting(NeedProcessorTest.class.getName(), "testSingleResourceAwaitCancel");
    }
    
    public void testCurrentNeed() throws Exception {
        log.entering(NeedProcessorTest.class.getName(), "testCurrentNeed");
        
        Need orgNeed = new Need(Usage.MIN_VALUE, 1);
        
        NeedProcessor np = new NeedProcessor(new Need(Usage.MIN_VALUE, 1));

        Need curNeed = np.getCurrentNeed();
        assertEquals("Current need must have same quantity as org need", orgNeed.getQuantity(), curNeed.getQuantity());
        
        ResourceId resId = new ResourceIdImpl(1);
        np.addAwaitedResource(resId);
        
        curNeed = np.getCurrentNeed();
        assertEquals("Current need must have same quantity as org need, as an awaited resource does not change the remaining quantity",
                     orgNeed.getQuantity(), curNeed.getQuantity());
        
        
        np.commitAwaitedResource(resId);
        
        curNeed = np.getCurrentNeed();
        
        assertEquals("Current need must have quantity which is one lower then the quanitity of the orgNeed", 
                orgNeed.getQuantity() - 1, curNeed.getQuantity());
        
        log.exiting(NeedProcessorTest.class.getName(), "testCurrentNeed");
    }
    
}
