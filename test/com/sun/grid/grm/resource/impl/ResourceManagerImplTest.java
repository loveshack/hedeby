/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventAdapter;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.order.OrderFactory;
import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.RequestQueue;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.Resource.State;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.filter.OrderVariableResolver;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.resource.management.ManagementEventListener;
import com.sun.grid.grm.resource.order.Order;
import com.sun.grid.grm.resource.order.ResourceRequestOrder;
import com.sun.grid.grm.resource.policy.PolicyManager;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.ServiceStoreException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import com.sun.grid.grm.service.impl.DefaultService;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Test for resource manager implementation.
 */
public class ResourceManagerImplTest extends TestCase {

    private static final Logger log = Logger.getLogger(ResourceManagerImplTest.class.getName());

    private ExecutionEnv env;

    public ResourceManagerImplTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        env = DummyExecutionEnvFactory.newInstance();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Tests the delivery of the SLO name if a resource is added to a service
     * In issue 608 a extension of the service API:
     * If a resource is added to a service in response of an SLO request, the
     * corresponding SLO name provided if a new resource is added.
     *
     * The test is implemented as a round trip test to show that the SLO
     * information is correctly transferred from a request of a service to
     * the point where the service gets a new resource.
     *
     * It tests two problems:
     * 1) The SLO name has to be transfered from the Request to an assignmentOrder
     * 2) The SLO name has to be transfered from assignmentOrder to service.addResource
     *
     * The test works as a functional round trip test. We currently can not test
     * the functionality on the TS level because SDM currently does not have a
     * Service that depends on the SLO information. Here the problem is solved by
     * introducing a mockup service, that is aware of the SLO  name information.
     * The test depends on the implementation of the Request object (here RequestImpl
     * is used)
     * @throws java.lang.Exception
     */
    @SuppressWarnings("unchecked")
    public void testSloNameRoundtrip_issue608() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testSloNameRoundtrip_issue608");
        
        //the SLO name that will be provided by service.resourceAdd()
        String referenceSloName ="TEST_SLO";
        //setup two mockup services
        SloDependentServiceMockup service1 = new SloDependentServiceMockup("service1", "host");
        SloDependentServiceMockup service2 = new SloDependentServiceMockup("service2", "host");

        //the services are stored in a service store
        ServiceStoreMockup serviceStore = new ServiceStoreMockup();
        serviceStore.addService(service1);
        serviceStore.addService(service2);

        //setup two resources
        ResourceType resourceType = DefaultResourceFactory.getResourceType("test");
        Resource resourceA = DefaultResourceFactory.createResourceByName(env, resourceType,"resourceA");
        Resource resourceB = DefaultResourceFactory.createResourceByName(env, resourceType, "resourceB");

        //assign one resource to each service
        service1.addResource(resourceA);
        service2.addResource(resourceB);
        assertNull("service1.addResource(resourceA) should not provide a SLO name, but it provided:"+service1.getSloNameOfLastResource(),service1.getSloNameOfLastResource());
        assertNull("service2.addResource(resourceB) should not provide a SLO name, but it provided:"+service2.getSloNameOfLastResource(),service2.getSloNameOfLastResource());

        //setup a ResourceManager
        ResourceStoreMockup resourceStore = new ResourceStoreMockup();
        SimpleOrderStore orderStore = new SimpleOrderStore();
        DefaultService ds = new DefaultService(env, "rp", resourceStore);
        ResourceManagerImpl resourceManager = new ResourceManagerImpl("ResourceManager", serviceStore, ds, orderStore);
        RequestQueueMockup requestQueue = new RequestQueueMockup();
        resourceManager.setRequestQueue(requestQueue);
        resourceManager.start();

        try {
            //create a ResourceRequest for service1
            //This simulates a request by an SLO
            List<Need> needList = new ArrayList<Need>(1);
            // Create a need for any resouce that service1 can get a hand on!
            Need need = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(99), 1);
            needList.add(need);
            ResourceRequestEvent resourceRequestEvent = new ResourceRequestEvent(0,service1.getName(),referenceSloName, needList);
            //Needed to construct a Request object
            //The request Object wraps the Resource Request object
            Request request = new RequestImpl(resourceRequestEvent, serviceStore, resourceManager, orderStore, new PolicyManagerMockup());


            //Now execute the request in place of the SLO=>we should receive an
            //assignment order with the sloName
            boolean result = request.execute();

            // With the fix of issue 632 the quantity of the request will be decremented
            // Once the order is fulfilled. After the execute method the order is ongoing
            // the result is false
            assertFalse("The first request should not be executed immediatly", result);
            assertTrue("There should be exactly one order in order store", orderStore.size()==1);

            ResourceRequestOrder rrOrder = (ResourceRequestOrder)orderStore.get(ConstantFilter.<Order>alwaysMatching());
            assertTrue("The order has the wrong SLO name:"+ rrOrder.getSloName(), referenceSloName.equals(rrOrder.getSloName()));


            //The service that got the resource removed now should send a ResourceRemovedEvent
            //via ServiceEventListener to the resource provider. This is short cutted by
            //calling the resourceManager directly:
            resourceManager.processResourceRemovedEvent(service2.getName(), resourceB);
            assertTrue("Timeout waiting for a resourceAdd with sloName call!",service1.waitForAddResourceCalled(5000));
            assertTrue("Wrong SLO name found:"+service1.getSloNameOfLastResource(),referenceSloName.equals(service1.getSloNameOfLastResource()));

            resourceManager.processAddResourceEvent(service1.getName(), resourceB);
            ResourceManagerImpl.Action action = resourceManager.submitResourceAddedEvent(service1.getName(), resourceB);
            
            assertTrue("RM did not process resource added event", action.waitUntilExecuted(2000));
            
            assertTrue("There should be no order in order store", orderStore.size()==0);
        } finally {
            resourceManager.stop();
        }
        
        log.exiting(ResourceManagerImplTest.class.getName(), "testSloNameRoundtrip_issue608");
    }


    /**
     * This test addresses the main aspect of issue 608
     * An assignmentOrder that stores an SLOName reaches resourceManager. In
     * the next step a resource is moved from the source service described in the
     * order to the target service. The test ensures that the moved resource is
     * added with the corresponding SLO name.
     * @throws java.lang.Exception
     */
    @SuppressWarnings("unchecked")
    public void testSloNameFromOrderToService() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testSloNameFromOrderToService");
        
        //the SLO name that will be provided by service.resourceAdd()
        String referenceSloName ="TEST_SLO";
        //setup two mockup services
        SloDependentServiceMockup service1 = new SloDependentServiceMockup("service1", "host");
        SloDependentServiceMockup service2 = new SloDependentServiceMockup("service2", "host");

        //the services are stored in a service store
        ServiceStoreMockup serviceStore = new ServiceStoreMockup();
        serviceStore.addService(service1);
        serviceStore.addService(service2);

        //setup two resources
        ResourceType resourceType = DefaultResourceFactory.getResourceType("test");
        Resource resourceA = DefaultResourceFactory.createResourceByName(env, resourceType, "resourceA");
        Resource resourceB = DefaultResourceFactory.createResourceByName(env, resourceType, "resourceB");

        //assign one resource to each service
        service1.addResource(resourceA);
        service2.addResource(resourceB);
        assertNull("service1.addResource(resourceA) should not provide a SLO name, but it provided:"+service1.getSloNameOfLastResource(),service1.getSloNameOfLastResource());
        assertNull("service2.addResource(resourceB) should not provide a SLO name, but it provided:"+service2.getSloNameOfLastResource(),service2.getSloNameOfLastResource());

        //setup a ResourceManager
        ResourceStoreMockup resourceStore = new ResourceStoreMockup();
        SimpleOrderStore orderStore = new SimpleOrderStore();
        DefaultService ds = new DefaultService(env, "rp", resourceStore);
        ResourceManagerImpl resourceManager = new ResourceManagerImpl("ResourceManager", serviceStore, ds, orderStore);
        RequestQueueMockup requestQueue = new RequestQueueMockup();
        resourceManager.setRequestQueue(requestQueue);
        resourceManager.start();

        //In the real system the order created as a consequence of an SLO Request
        orderStore.add(OrderFactory.createResourceRequestAssignOrder(resourceB.getId(), "service2", "service1", new NeedProcessor(new Need(Usage.MIN_VALUE, 1)), referenceSloName));
        //The service that got the resource removed now should send a ResourceRemovedEvent
        //via ServiceEventListener to the resource provider. This is short cutted by
        //calling the resourceManager directly:
        resourceManager.processOrder(service2.getName(), resourceB);
        assertTrue("Timeout waiting for a resourceAdd with sloName call!",service1.waitForAddResourceCalled(5000));
        assertTrue("Wrong SLO name found:"+service1.getSloNameOfLastResource(),referenceSloName.equals(service1.getSloNameOfLastResource()));
        
        // The processOrder method does not delete the order of followup action has
        // ben correctly triggered. This is here the case. 
        assertEquals("There should be one order in order store", 1, orderStore.size());
        
        
        
        log.exiting(ResourceManagerImplTest.class.getName(), "testSloNameFromOrderToService");
    }



    /**
     * Test of removeResource method, of class ResourceManagerImpl.
     * The test focuses on a correct processing of an order, if an error occurs
     * during the call to "Service.removeResource(...)".
     *
     * Expectations and results are described in the body of the method.
     *
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public void testRemoveResource_rollBackOrder() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testRemoveResource_rollBackOrder");
        
        SimpleOrderStore osm = new SimpleOrderStore();
        ServiceMockup sm = new ServiceMockup("service_mockup", "host");
        ResourceType trt = DefaultResourceFactory.getResourceType("test");
        Resource resource = DefaultResourceFactory.createResourceByName(env, trt, "service_mockup");
        sm.addResource(resource);
        ServiceStoreMockup ssm = new ServiceStoreMockup();
        ssm.addService(sm);
        ResourceStoreMockup rsm = new ResourceStoreMockup();
        RequestQueueMockup rqm = new RequestQueueMockup();
        DefaultService ds = new DefaultService(env, "rp", rsm);
        ResourceManagerImpl instance = new ResourceManagerImpl("ResourceManager", ssm, ds, osm);
        instance.setRequestQueue(rqm);
        instance.start();
        try {
            Order o = OrderFactory.createMaintenanceAssignOrder(resource.getId(), sm.getName(), "target", false);
            osm.add(o);

            try {
                instance.removeResource(resource.getId(), ResourceReassignmentDescriptor.INSTANCE);
                fail("Service must throw an ServiceNotActiveException");
            } catch(ServiceNotActiveException ex) {
                // fine, expected exception
            }
            Order result = osm.remove(OrderVariableResolver.newOrderFilter(o));
            assertEquals("the order stored in order store and original order have to be the same", o, result);
        } finally {
            instance.stop();
        }
        log.exiting(ResourceManagerImplTest.class.getName(), "testRemoveResource_rollBackOrder");
    }

    /**
     * Test of removeResource method, of class ResourceManagerImpl.
     * The test focuses on a correct processing of an order, if an error occurs
     * during the call to "Service.removeResource(...)".
     *
     * Expectations and results are described in the body of the method.
     *
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public void testRemoveResourceFromSource_rollBackOrder() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testRemoveResourceFromSource_rollBackOrder");
        
        SimpleOrderStore osm = new SimpleOrderStore();
        ServiceMockup sm = new ServiceMockup("service_mockup", "host");
        ResourceType trt = DefaultResourceFactory.getResourceType("test");
        Resource resource = DefaultResourceFactory.createResourceByName(env, trt, "service_mockup");
        sm.addResource(resource);
        ServiceStoreMockup ssm = new ServiceStoreMockup();
        ssm.addService(sm);
        ResourceStoreMockup rsm = new ResourceStoreMockup();
        RequestQueueMockup rqm = new RequestQueueMockup();
        DefaultService ds = new DefaultService(env, "rp", rsm);
        ResourceManagerImpl instance = new ResourceManagerImpl("ResourceManager", ssm, ds, osm);
        instance.setRequestQueue(rqm);
        instance.start();
        try {
            Order o = OrderFactory.createMaintenanceAssignOrder(resource.getId(), "service_mockup", "target", false);
            osm.add(o);

            try {
                instance.removeResource(resource.getId(), ResourceReassignmentDescriptor.INSTANCE);
                fail("resource manager was supposed to throw service not active exception");
            } catch (ServiceNotActiveException snae) {
                // pass
            }

            assertTrue("order store must have just one order", osm.size() == 1);

            Order result = osm.remove(OrderVariableResolver.newOrderFilter(o));
            assertEquals("the order stored in order store and original order have to be the same", o, result);
        } finally {
            instance.stop();
        }
        log.exiting(ResourceManagerImplTest.class.getName(), "testRemoveResourceFromSource_rollBackOrder");
    }

    /**
     * Test of moveResource method, of class ResourceManagerImpl.
     * The test focuses on a correct processing of an order, if an error occurs
     * during the call to "Service.removeResource(...)".
     *
     * Expectations and results are described in the body of the method.
     *
     * @throws Exception
     */
    public void testMoveResource_rollBackOrder() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testMoveResource_rollBackOrder");
        
        SimpleOrderStore osm = new SimpleOrderStore();
        ServiceMockup sm = new ServiceMockup("service_mockup", "host");
        ServiceMockup target = new ServiceMockup("target", "host");
        ResourceType trt = DefaultResourceFactory.getResourceType("test");
        Resource resource = DefaultResourceFactory.createResourceByName(env, trt, "service_mockup");
        sm.addResource(resource);
        ServiceStoreMockup ssm = new ServiceStoreMockup();
        ssm.addService(sm);
        ssm.addService(target);
        ResourceStoreMockup rsm = new ResourceStoreMockup();
        RequestQueueMockup rqm = new RequestQueueMockup();
        DefaultService ds = new DefaultService(env, "rp", rsm);
        @SuppressWarnings("unchecked")
        ResourceManagerImpl instance = new ResourceManagerImpl("ResourceManager", ssm, ds, osm);
        instance.setRequestQueue(rqm);
        instance.start();
        try {
            Order o = OrderFactory.createMaintenanceAssignOrder(resource.getId(), "service_mockup", "target", false);
            osm.add(o);

            try {
                instance.moveResource("target", resource.getId(), false, false);
                fail("resource manager was supposed to throw service not active exception");
            } catch (ServiceNotActiveException snae) {
                //pass
            }
            assertTrue("order store must have just one order", osm.size() == 1);

            Order result = osm.remove(OrderVariableResolver.newOrderFilter(o));
            assertEquals("the order stored in order store and original order have to be the same", o, result);
        } finally {
            instance.stop();
        }
        
        log.exiting(ResourceManagerImplTest.class.getName(), "testMoveResource_rollBackOrder");
    }

    /**
     * Tests that ResourceManagerImpl cancels an exiting order if a service sends
     * the event sequence REMOVE_RESOURCE, ADD_RESOURCE, RESOURCE_ADDED.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAddAddedEventCancelsOrder() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testRemoveAddAddedEventCancelsOrder");
        
        SimpleOrderStore osm = new SimpleOrderStore();
        
        String source = "service_mockup";
        String targetName = "target";
        
        ServiceMockup sm = new ServiceMockup(source, "host");
        ServiceMockup target = new ServiceMockup(targetName, "host");
        
        ResourceType trt = DefaultResourceFactory.getResourceType("test");
        Resource resource = DefaultResourceFactory.createResourceByName(env, trt, "service_mockup");
        
        sm.addResource(resource);
        ServiceStoreMockup ssm = new ServiceStoreMockup();
        ssm.addService(sm);
        ssm.addService(target);
        ResourceStoreMockup rsm = new ResourceStoreMockup();
        RequestQueueMockup rqm = new RequestQueueMockup();
        DefaultService ds = new DefaultService(env, "rp", rsm);
        @SuppressWarnings("unchecked")
        ResourceManagerImpl instance = new ResourceManagerImpl("ResourceManager", ssm, ds, osm);
        
        instance.setRequestQueue(rqm);
        instance.start();
        try {
            NeedProcessor np = new NeedProcessor(new Need(Usage.MAX_VALUE, 1));
            np.addAwaitedResource(resource.getId());
            Order o = OrderFactory.createResourceRequestAssignOrder(resource.getId(), "service_mockup", "target", np, "slo");
            osm.add(o);

            instance.processRemoveResourceEvent(source, resource);
            instance.processAddResourceEvent(source, resource);
            
            ResourceManagerImpl.Action action = instance.submitResourceAddedEvent(source, resource);
            assertTrue("RM did not process the resource added event", action.waitUntilExecuted(2000));

            assertEquals("The committed quantity must be null, because the order must be cancled", np.getCommitedQuantity(), 0);
            assertTrue("need processor must have the resource in the canceled order list", np.hadCanceledOrderForResource(resource.getId()));
            assertEquals("The order store must be empty", 0, osm.size());
        } finally {
            instance.stop();
        }
        log.exiting(ResourceManagerImplTest.class.getName(), "testRemoveAddAddedEventCancelsOrder");
    }

    /**
     * Tests that ResourceManagerImpl cancels an exiting order if a service sends
     * the event sequence REMOVE_RESOURCE, RESOURCE_ADDED.
     * 
     * The ResourceManagerImpl must also cancel the order if service did not send
     * the ADD_RESOURCE event.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAddedEventCancelsOrder() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testRemoveAddedEventCancelsOrder");
        
        SimpleOrderStore osm = new SimpleOrderStore();
        
        String source = "source";
        String targetName = "target";
        
        ServiceMockup sm = new ServiceMockup(source, "host");
        ServiceMockup target = new ServiceMockup(targetName, "host");
        
        ResourceType trt = DefaultResourceFactory.getResourceType("test");
        Resource resource = DefaultResourceFactory.createResourceByName(env, trt, "res");
        
        sm.addResource(resource);
        ServiceStoreMockup ssm = new ServiceStoreMockup();
        ssm.addService(sm);
        ssm.addService(target);
        ResourceStoreMockup rsm = new ResourceStoreMockup();
        RequestQueueMockup rqm = new RequestQueueMockup();
        DefaultService ds = new DefaultService(env, "rp", rsm);
        @SuppressWarnings("unchecked")
        ResourceManagerImpl instance = new ResourceManagerImpl("ResourceManager", ssm, ds, osm);
        
        instance.setRequestQueue(rqm);
        instance.start();
        try {
            NeedProcessor np = new NeedProcessor(new Need(Usage.MAX_VALUE, 1));
            np.addAwaitedResource(resource.getId());
            Order o = OrderFactory.createResourceRequestAssignOrder(resource.getId(), source, targetName, np, "slo");
            osm.add(o);

            instance.processRemoveResourceEvent(source, resource);
            
            ResourceManagerImpl.Action action = instance.submitResourceAddedEvent(source, resource);
            assertTrue("RM did not process resource added event", action.waitUntilExecuted(2000));
            
            assertEquals("The committed quantity must be null, because the order must be cancled", np.getCommitedQuantity(), 0);
            assertTrue("need processor must have the resource in the canceled order list", np.hadCanceledOrderForResource(resource.getId()));
            assertEquals("The order store must be empty", 0, osm.size());
        } finally {
            instance.stop();
        }
        log.exiting(ResourceManagerImplTest.class.getName(), "testRemoveAddedEventCancelsOrder");
    }
    
    /**
     * Tests that ResourceManagerImpl cancels an exiting order if a service sends
     * the event sequence REMOVE_RESOURCE, RESOURCE_ERROR.
     * @throws java.lang.Exception
     */
    public void testRemoveErrorEventCancelsOrder() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testRemoveErrorEventCancelsOrder");
        SimpleOrderStore osm = new SimpleOrderStore();
        
        ServiceMockup sm = new ServiceMockup("source", "host");
        ServiceMockup target = new ServiceMockup("target", "host");
        
        ResourceType trt = DefaultResourceFactory.getResourceType("test");
        Resource resource = DefaultResourceFactory.createResourceByName(env, trt, "res");
        
        sm.addResource(resource);
        ServiceStoreMockup ssm = new ServiceStoreMockup();
        ssm.addService(sm);
        ssm.addService(target);
        ResourceStoreMockup rsm = new ResourceStoreMockup();
        RequestQueueMockup rqm = new RequestQueueMockup();
        DefaultService ds = new DefaultService(env, "rp", rsm);
        @SuppressWarnings("unchecked")
        ResourceManagerImpl instance = new ResourceManagerImpl("ResourceManager", ssm, ds, osm);
        
        instance.setRequestQueue(rqm);
        instance.start();
        try {
            NeedProcessor np = new NeedProcessor(new Need(Usage.MAX_VALUE, 1));
            np.addAwaitedResource(resource.getId());
            Order o = OrderFactory.createResourceRequestAssignOrder(resource.getId(), sm.getName(), target.getName(), np, "slo");
            osm.add(o);

            instance.submitRemoveResourceEvent(sm.getName(), resource);
            
            ResourceManagerImpl.Action action = instance.submitResourceErrorEvent(sm.getName(), resource, "Could not remove resource");
            assertTrue("RM did not process resource error event", action.waitUntilExecuted(2000));
            
            assertEquals("The committed quantity must be null, because the order must be cancled", np.getCommitedQuantity(), 0);
            assertTrue("need processor must have the resource in the canceled order list", np.hadCanceledOrderForResource(resource.getId()));
            assertEquals("The order store must be empty", 0, osm.size());
        } finally {
            instance.stop();
        }
        log.exiting(ResourceManagerImplTest.class.getName(), "testRemoveErrorEventCancelsOrder");
    }
    
    /**
     * Tests that ResourceManagerImpl cancels an exiting order if a source service sends
     * REMOVE_RESOURCE, RESOURCE_REMOVED and target service sends RESOURCE_ADD, RESOURCE_REJECTED
     * events
     * @throws java.lang.Exception
     */
    public void testRemoveRemovedAddRejectedEventCancelsOrder() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testRemoveRemovedAddRejectedEventCancelsOrder");
        SimpleOrderStore osm = new SimpleOrderStore();
        
        ServiceMockup sm = new ServiceMockup("source", "host").throwServiceNotActiveOnResourceRemove(false);
        ServiceMockup target = new ServiceMockup("target", "host").throwServiceNotActiveOnResourceRemove(false);
        
        ResourceType trt = DefaultResourceFactory.getResourceType("test");
        Resource resource = DefaultResourceFactory.createResourceByName(env, trt, "res");
        
        sm.addResource(resource);
        ServiceStoreMockup ssm = new ServiceStoreMockup();
        ssm.addService(sm);
        ssm.addService(target);
        ResourceStoreMockup rsm = new ResourceStoreMockup();
        RequestQueueMockup rqm = new RequestQueueMockup();
        DefaultService ds = new DefaultService(env, "rp", rsm);
        @SuppressWarnings("unchecked")
        ResourceManagerImpl instance = new ResourceManagerImpl("ResourceManager", ssm, ds, osm);
        
        instance.setRequestQueue(rqm);
        instance.start();
        try {
            NeedProcessor np = new NeedProcessor(new Need(Usage.MAX_VALUE, 1));
            np.addAwaitedResource(resource.getId());
            Order o = OrderFactory.createResourceRequestAssignOrder(resource.getId(), sm.getName(), target.getName(), np, "slo");
            osm.add(o);

            instance.processRemoveResourceEvent(sm.getName(), resource);
            
            ResourceManagerImpl.Action action = instance.submitRemoveResourceEvent(sm.getName(), resource);
            assertTrue("RM did not process remove resource event", action.waitUntilExecuted(2000));
            
            sm.removeResource(resource.getId(), ResourceReassignmentDescriptor.INSTANCE);
            
            action = instance.submitResourceRemovedEvent(sm.getName(), resource);
            assertTrue("RM did not process resource removed event", action.waitUntilExecuted(2000));
            
            instance.processAddResourceEvent(target.getName(), resource);
            action = instance.submitResourceRejectedEvent(target.getName(), resource);
            assertTrue("RM did not process resource rejected event", action.waitUntilExecuted(2000));

            assertEquals("The committed quantity must be null, because the order must be cancled", 0, np.getCommitedQuantity());
            assertTrue("need processor must have the resource in the canceled order list", np.hadCanceledOrderForResource(resource.getId()));
            assertEquals("The order store must be empty", 0, osm.size());
        } finally {
            instance.stop();
        }
        log.exiting(ResourceManagerImplTest.class.getName(), "testRemoveRemovedAddRejectedEventCancelsOrder");
    }
    
    /**
     * Tests that a resource request order is canceled if the target service throws and InvalidResourceException
     * when addResource is called.
     * 
     * The tests performs the following steps:
     * 
     * <ul>
     *    <li>Create a ResourceRequestAssignOrder for resource res (source to target)</li>
     *    <li>inject REMOVE_RESOURCE(res,source) event into RM</li>
     *    <li>Configure the service that it throws an InvalidResourceException with the next
     *        addResource call.</li>
     *    <li>inject RESOURCE_REMOVED(res,source) event into RM</li>
     *    <li>Check that the order has been canceled</li>
     * </ul>
     * 
     * 
     * The service service produces the event REMOVE_RESOURCE and RESOURCE_REMOVED.
     * @throws java.lang.Exception
     */
    public void testRemoveRemovedInvalidResourceExceptionCancelsOrder() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testRemoveRemovedInvalidResourceExceptionCancelsOrder");
        SimpleOrderStore osm = new SimpleOrderStore();
        
        ServiceMockup sm = new ServiceMockup("source", "host").throwServiceNotActiveOnResourceRemove(false);
        ServiceMockup target = new ServiceMockup("target", "host")
                .throwServiceNotActiveOnResourceRemove(false);
        
        ResourceType trt = DefaultResourceFactory.getResourceType("test");
        Resource resource = DefaultResourceFactory.createResourceByName(env, trt, "res");
        
        sm.addResource(resource);
        ServiceStoreMockup ssm = new ServiceStoreMockup();
        ssm.addService(sm);
        ssm.addService(target);
        ResourceStoreMockup rsm = new ResourceStoreMockup();
        RequestQueueMockup rqm = new RequestQueueMockup();
        DefaultService ds = new DefaultService(env, "rp", rsm);
        @SuppressWarnings("unchecked")
        ResourceManagerImpl instance = new ResourceManagerImpl("ResourceManager", ssm, ds, osm);
        
        instance.setRequestQueue(rqm);
        instance.start();
        try {
            NeedProcessor np = new NeedProcessor(new Need(Usage.MAX_VALUE, 1));
            np.addAwaitedResource(resource.getId());
            Order o = OrderFactory.createResourceRequestAssignOrder(resource.getId(), sm.getName(), target.getName(), np, "slo");
            osm.add(o);

            // Start the resource remove process from the source service
            ResourceManagerImpl.Action action = instance.submitRemoveResourceEvent(sm.getName(), resource);
            assertTrue("RM did not process remove resource event", action.waitUntilExecuted(2000));
            
            sm.removeResource(resource.getId(), ResourceReassignmentDescriptor.INSTANCE);
            
            // The call of target.addResource should throw and InvalidResourceException
            target.throwInvalidResourceExceptionOnAddResource(true);
            
            // With the ResourceRemovedEvent the assigment process of the resource to target is started
            action = instance.submitResourceRemovedEvent(sm.getName(), resource);
            assertTrue("RM did not process resource removed event", action.waitUntilExecuted(2000));

            // The order must be canceled
            assertEquals("The committed quantity must be null, because the order must be cancled", 0, np.getCommitedQuantity());
            assertTrue("need processor must have the resource in the canceled order list", np.hadCanceledOrderForResource(resource.getId()));
            assertEquals("The order store must be empty", 0, osm.size());
        } finally {
            instance.stop();
        }
        log.exiting(ResourceManagerImplTest.class.getName(), "testRemoveRemovedInvalidResourceExceptionCancelsOrder");
    }
    
    
    public void testAddedEventCommitsOrder() throws Exception {
        log.entering(ResourceManagerImplTest.class.getName(), "testAddedEventCommitsOrder");
        SimpleOrderStore osm = new SimpleOrderStore();
        
        ServiceMockup sm = new ServiceMockup("source", "host").throwServiceNotActiveOnResourceRemove(false);
        ServiceMockup target = new ServiceMockup("target", "host")
                .throwServiceNotActiveOnResourceRemove(false);
        
        ResourceType trt = DefaultResourceFactory.getResourceType("test");
        Resource resource = DefaultResourceFactory.createResourceByName(env, trt, "res");
        
        sm.addResource(resource);
        ServiceStoreMockup ssm = new ServiceStoreMockup();
        ssm.addService(sm);
        ssm.addService(target);
        ResourceStoreMockup rsm = new ResourceStoreMockup();
        RequestQueueMockup rqm = new RequestQueueMockup();
        DefaultService ds = new DefaultService(env, "rp", rsm);
        @SuppressWarnings("unchecked")
        ResourceManagerImpl instance = new ResourceManagerImpl("ResourceManager", ssm, ds, osm);
        
        instance.setRequestQueue(rqm);
        instance.start();
        try {
            NeedProcessor np = new NeedProcessor(new Need(Usage.MAX_VALUE, 1));
            np.addAwaitedResource(resource.getId());
            Order o = OrderFactory.createResourceRequestAssignOrder(resource.getId(), sm.getName(), target.getName(), np, "slo");
            osm.add(o);

            // Start the resource remove process from the source service
            ResourceManagerImpl.Action action = instance.submitRemoveResourceEvent(sm.getName(), resource);
            assertTrue("RM did not process remove resource event", action.waitUntilExecuted(2000));
            
            sm.removeResource(resource.getId(), ResourceReassignmentDescriptor.INSTANCE);
            
            action = instance.submitResourceRemovedEvent(sm.getName(), resource);
            assertTrue("RM did not process resource removed event", action.waitUntilExecuted(2000));
            
            action = instance.submitAddResourceEvent(target.getName(), resource);
            assertTrue("RM did not process add resource event", action.waitUntilExecuted(2000));
            target.addResource(resource);
            
            action = instance.submitResourceAddedEvent(target.getName(), resource);
            assertTrue("RM did not process resource added event", action.waitUntilExecuted(2000));
            
            // The order must be comitted
            assertEquals("The committed quantity must be 1, because the order must be committed (" + np + ")", 1, np.getCommitedQuantity());
            assertFalse("need processor must not have the resource in the canceled order list", np.hadCanceledOrderForResource(resource.getId()));
            assertEquals("The order store must be empty", 0, osm.size());
        } finally {
            instance.stop();
        }
        log.exiting(ResourceManagerImplTest.class.getName(), "testAddedEventCommitsOrder");
    }
    
    private class ServiceStoreMockup implements
            ServiceStore<ServiceMockup> {

        private Map<String, ServiceMockup> store = new HashMap<String, ServiceMockup>(1);

        public void clear() {
            store.clear();
        }

        public ServiceMockup addService(ServiceMockup service) {
            return store.put(service.getName(), service);
        }

        public ServiceMockup removeService(String serviceName) throws UnknownServiceException {
            ServiceMockup s = store.remove(serviceName);
            if (s != null) {
                return s;
            } else {
                throw new UnknownServiceException();
            }
        }

        public Set<String> getServiceNames() throws ServiceStoreException {
            return store.keySet();
        }

        public ServiceMockup getService(String serviceName) throws UnknownServiceException {
            ServiceMockup s = store.get(serviceName);
            if (s != null) {
                return s;
            } else {
                throw new UnknownServiceException();
            }
        }

        public List<ServiceMockup> getServices() {
            return new ArrayList<ServiceMockup>(store.values());
        }
    }

    private class ResourceStoreMockup implements
            ResourceStore {

        private Map<ResourceId, Resource> store = new HashMap<ResourceId, Resource>(5);

        public void loadResources() throws ResourceStoreException {
        }
        
        public void clear() throws ResourceStoreException {
            store.clear();
        }

        public Resource add(Resource resource) {
            return store.put(resource.getId(), resource);
        }

        public Resource remove(ResourceId resourceId) throws UnknownResourceException {
            Resource r = store.remove(resourceId);
            if (r != null) {
                return r;
            } else {
                throw new UnknownResourceException();
            }

        }

        public Set<ResourceId> getResourceIds() {
            return store.keySet();
        }

        public Resource getResource(ResourceId resourceId) throws UnknownResourceException {
            Resource r = store.get(resourceId);
            if (r != null) {
                return r;
            } else {
                throw new UnknownResourceException();
            }
        }

        public List<Resource> getResources() {
            return new ArrayList<Resource>(store.values());
        }

        public List<Resource> getResources(Filter<Resource> filter) {
            List<Resource> list = new ArrayList<Resource>(store.size());
            ResourceVariableResolver rvr = new ResourceVariableResolver();
            for (Resource r : store.values()) {
                rvr.setResource(r);
                if (filter.matches(rvr)) {
                    list.add(r);
                }
            }
            return list;
        }

    }

    private class RequestQueueMockup implements
            RequestQueue {

        public void addEvent(ResourceRequestEvent event) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void triggerReprocessing() {
            // pass
        }

        public void start() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void stop() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setPolicyProvider(PolicyManager policyProvider) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void addManagementEventListener(ManagementEventListener lis) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void removeManagementEventListener(ManagementEventListener lis) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<Request> getRequests(Filter<Request> requestFilter) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public List<Request> removeRequests(Filter<Request> requestFilter) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    /**
     * A sub class of ServiceMockup with a slightly different behavior to serve
     * the testSloNameRoundtrip test
     */
    private class SloDependentServiceMockup extends ServiceMockup{
        private String sloNameOfLastResource;
        private boolean addResourceWithSloNameCalled;
        SloDependentServiceMockup(String name, String host){
            super(name, host);
        }
        /**
         * Overridden because sloName needs to be observable
         * After the call resource is in ASSIGNED state
         * @param resource to be added
         * @param sloName name of the requesting SLO if any
         */
        @Override
        synchronized public void addResource(Resource resource,String sloName) throws InvalidResourceException {
          sloNameOfLastResource = sloName;
          super.addResource(resource,sloName);
          resource.setState(State.ASSIGNED);
          addResourceWithSloNameCalled=true;
           notifyAll();
        }

         /**
          * After the add resource is in ASSIGNED state
          * @param resource to be added
          */
        @Override
        synchronized public void addResource(Resource resource) throws InvalidResourceException {
          super.addResource(resource);
          resource.setState(State.ASSIGNED);
        }


        /**
         * @return the SLO name that was provided by the last addResource call
         */
        public String getSloNameOfLastResource(){
            return sloNameOfLastResource;
        }

        /**
         * Overridden because a slightly different behavior is needed (no ServiceNotActiveException+
         * real removal of resource from internal list!)
         * @param descr the descriptor of the resource remove operation
         * @throws com.sun.grid.grm.resource.UnknownResourceException
         */
        @Override
        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException {
            Resource r = resources.get(resourceId);
            if (r == null) {
                throw new UnknownResourceException();
            }
            resources.remove(r);
        }
        public void resetSloNameOfLastResource(){
            sloNameOfLastResource =null;
        }

        synchronized public boolean waitForAddResourceCalled(int timeout)throws InterruptedException{
            long endTime = System.currentTimeMillis() + timeout;
            while (!addResourceWithSloNameCalled && System.currentTimeMillis() < endTime) {
                long remainingTime = endTime - System.currentTimeMillis();
                if (remainingTime > 0) {
                    wait(remainingTime);
                }
            }
            return addResourceWithSloNameCalled;
        }
    }

    private class ServiceMockup implements Service {

        protected final Map<ResourceId, Resource> resources;
        protected final Hostname hostname;
        protected final String name;
        /* mockup needs just one listener */
        protected ServiceEventListener sel;
        protected final ServiceEventListener nullServiceEventListener = new ServiceEventAdapter();
        protected ComponentEventListener cel;
        protected final ComponentEventListener nullComponentEventListener = new ComponentEventAdapter();
        protected volatile ComponentState cmpState;
        protected volatile ServiceState svcState;
        protected long svcEventId = 0;
        protected long cmpEventId = 0;

        private volatile boolean throwServiceNotActiveOnResourceRemove;
        private volatile boolean throwInvalidResourceExceptionOnAddResource;
        
        /**
         * Creates mockup instance of service.
         *
         * @param throwremote if true, each method will throw GrmRemoteException
         * @param name service name
         * @param host host on which mockup "is" running
         */
        public ServiceMockup(String name, String host) {
            this.resources = new HashMap<ResourceId, Resource>();
            this.hostname = Hostname.getInstance(host);
            this.name = name;
            this.cmpState = ComponentState.UNKNOWN;
            this.svcState = ServiceState.UNKNOWN;
            this.sel = nullServiceEventListener;
            this.cel = nullComponentEventListener;
            throwServiceNotActiveOnResourceRemove = true;
            throwInvalidResourceExceptionOnAddResource = false;
        }
        
        ServiceMockup throwServiceNotActiveOnResourceRemove(boolean value) {
            throwServiceNotActiveOnResourceRemove = value;
            return this;
        }
        
        ServiceMockup throwInvalidResourceExceptionOnAddResource(boolean value) {
            throwInvalidResourceExceptionOnAddResource = value;
            return this;
        }

        public void startService() throws GrmException {
            setServiceState(ServiceState.STARTING);
            svcEventId++;
            setServiceState(ServiceState.RUNNING);
        }

        public void stopService(boolean isFreeResources) throws GrmException {
            setServiceState(ServiceState.SHUTDOWN);
            svcEventId++;
            setServiceState(ServiceState.STOPPED);
        }

        public void setServiceEventId(long id) {
            this.svcEventId = id;
        }

        /* event ID has to be set manually before calling this!!! -
         * it is needed for junit tests */
        public void setServiceState(ServiceState state) {
            this.svcState = state;
            ServiceStateChangedEvent ssce = new ServiceStateChangedEvent(svcEventId, name, svcState);
            switch (svcState) {
                case ERROR:
                    sel.serviceError(ssce);
                    break;
                case RUNNING:
                    sel.serviceRunning(ssce);
                    break;
                case SHUTDOWN:
                    sel.serviceShutdown(ssce);
                    break;
                case STARTING:
                    sel.serviceStarting(ssce);
                    break;
                case STOPPED:
                    sel.serviceStopped(ssce);
                    break;
                case UNKNOWN:
                    sel.serviceUnknown(ssce);
                    break;
                default:
                    throw new IllegalStateException();
            }
        }

        public ServiceState getServiceState() {
            return svcState;
        }

        public List<SLOState> getSLOStates() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void addServiceEventListener(ServiceEventListener eventListener) {
            sel = eventListener;
        }

        public void removeServiceEventListener(ServiceEventListener eventListener) {
            sel = nullServiceEventListener;
        }

        public List<Resource> getResources(Filter filter) {
            return new LinkedList<Resource>(resources.values());
        }

        public List<Resource> getResources() {
            return new LinkedList<Resource>(resources.values());
        }

        public Resource getResource(ResourceId resourceId) throws UnknownResourceException {
            Resource r = resources.get(resourceId);
            if (r == null) {
                throw new UnknownResourceException();
            }
            return r;
        }

        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, ServiceNotActiveException {
            Resource r = resources.remove(resourceId);
            if (r == null) {
                throw new UnknownResourceException();
            }
            if (throwServiceNotActiveOnResourceRemove) {
                throw new ServiceNotActiveException();
            }
        }

        public void resetResource(ResourceId resource) throws UnknownResourceException {
            Resource r = resources.get(resource);
            if (r == null) {
                throw new UnknownResourceException();
            }
            r.setState(Resource.State.ASSIGNED);
            svcEventId++;
            ResourceResetEvent rre = new ResourceResetEvent(svcEventId, name, r, "reset");
            resources.put(r.getId(), r);
            sel.resourceReset(rre);
        }

        /**
         * @param resource  the resource to add
         * @param sloName if addResource is consequence of an SLO request, otherwise null
         */
        public void addResource(Resource resource,String sloName) throws InvalidResourceException {
           
           addResource(resource);
        }

        public void addResource(Resource resource)  throws InvalidResourceException {
            if (throwInvalidResourceExceptionOnAddResource) {
                throw new InvalidResourceException("invalid resource");
            }
            AddResourceEvent are = new AddResourceEvent(svcEventId, name, resource, "add");
            svcEventId++;
            sel.addResource(are);
            resources.put(resource.getId(), resource);
            ResourceAddedEvent rae = new ResourceAddedEvent(svcEventId, name, resource, "added");
            svcEventId++;
            sel.resourceAdded(rae);
        }

        public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, InvalidResourcePropertiesException {
            Resource r = resources.get(resource);
            if (r == null) {
                throw new UnknownResourceException();
            }
            Collection<ResourceChanged> changed = r.modify(operations);
            svcEventId++;
            ResourceChangedEvent rcpe = new ResourceChangedEvent(svcEventId, name, r, changed, "modify");
            resources.put(r.getId(), r);
            sel.resourceChanged(rcpe);
        }

        public void modifyResource(ResourceId resource, ResourceChangeOperation operation) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
            modifyResource(resource, Collections.singleton(operation));
        }

        public void start() {
            setState(ComponentState.STARTING);
            cmpEventId++;
            setState(ComponentState.STARTED);
        }

        public void stop(boolean isForced) {
            setState(ComponentState.STOPPING);
            cmpEventId++;
            setState(ComponentState.STOPPED);
        }

        public void reload(boolean isForced) {
            if (isForced) {
                cmpEventId++;
                stop(false);
                cmpEventId++;
                start();
            } else {
                cmpEventId++;
                setState(ComponentState.RELOADING);
                cmpEventId++;
                setState(ComponentState.STARTED);
            }

        }

        public void setComponentEventId(long id) {
            this.cmpEventId = id;
        }

        /* event ID has to be set manually before calling this!!! -
         * it is needed for junit tests */
        public void setState(ComponentState state) {
            this.cmpState = state;
            ComponentStateChangedEvent csce = new ComponentStateChangedEvent(cmpEventId, name, hostname, cmpState);
            switch (cmpState) {
                case RELOADING:
                    cel.componentReloading(csce);
                    break;
                case STARTED:
                    cel.componentStarted(csce);
                    break;
                case STARTING:
                    cel.componentStarting(csce);
                    break;
                case STOPPED:
                    cel.componentStopped(csce);
                    break;
                case STOPPING:
                    cel.componentStopping(csce);
                    break;
                case UNKNOWN:
                    cel.componentUnknown(csce);
                    break;
                default:
                    throw new IllegalStateException();
            }
        }

        public ComponentState getState() {
            return cmpState;
        }

        public void addComponentEventListener(ComponentEventListener componentEventListener) {
            cel = componentEventListener;
        }

        public void removeComponentEventListener(ComponentEventListener componentEventListener) {
            cel = nullComponentEventListener;
        }

        public String getName() {
            return name;
        }

        public Hostname getHostname() {
            return hostname;
        }

        public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws UnknownResourceException {
            Resource r = resources.get(resource);
            if (r == null) {
                throw new UnknownResourceException();
            }
            r.setAmbiguous(ambiguous);
            resources.put(r.getId(), r);
        }

        public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}
