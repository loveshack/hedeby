/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., Marcht, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/

/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.OrderStore;
import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.management.ManagementEventSupport;
import com.sun.grid.grm.resource.policy.PolicyManager;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.event.ResourceRequestEvent;

import com.sun.grid.grm.util.filter.ConstantFilter;
import junit.framework.TestCase;

import java.lang.reflect.Field;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Logger;

public class RequestQueueImplTest extends TestCase {

    private final static Logger log = Logger.getLogger(RequestQueueImplTest.class.getName());
    private ServiceStore<Service> services;
    private ResourceManager resources;
    private OrderStore orders;
    private PolicyManager policies;
    private ManagementEventSupport mes;

    public RequestQueueImplTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "setUp");
        super.setUp();
        mes = new ManagementEventSupport(this.getName());
        orders = new SimpleOrderStore();
        log.exiting(RequestQueueImplTest.class.getName(), "setUp");
    }

    @Override
    protected void tearDown() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "tearDown");
        super.tearDown();
        log.exiting(RequestQueueImplTest.class.getName(), "tearDown");
    }

    /**
     * Test of addEvent method, of class RequestQueueImpl.
     */
    @SuppressWarnings("unchecked")
    public void testAddEventIsActive() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testAddEventIsActive");

        // todo make it completely right, create a RRE mockup
        ResourceRequestEvent event = new ResourceRequestEvent(0, "service",
                "slo", Collections.<Need>emptyList());
        RequestQueueImpl instance = new RequestQueueImpl(services, resources, orders, policies, mes);

        BlockingQueue<Request> hlp1;

        Field f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        f.set(instance, true);
        f.setAccessible(false);

        f = RequestQueueImpl.class.getDeclaredField("queued");
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);

        assertTrue("The RequestQueue is not supposed to have a queued event at this point",
                hlp1.size() == 0);

        instance.addEvent(event);
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed to have a queued event at this point",
                hlp1.size() == 1);
        
        log.exiting(RequestQueueImplTest.class.getName(), "testAddEventIsActive");
    }

    /**
     * Test of addEvent method, of class RequestQueueImpl.
     */
    @SuppressWarnings("unchecked")
    public void testAddEventIsActiveOverwrite() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testAddEventIsActiveOverwrite");

        // todo make it completely right, create a RRE mockup
        ResourceRequestEvent event1 = new ResourceRequestEvent(0, "service",
                "slo", Collections.<Need>emptyList());

        // todo make it completely right, create a RRE mockup
        List<Need> needs2 = new ArrayList<Need>(1);
        Need n = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(1), 1);
        needs2.add(n);

        ResourceRequestEvent event2 = new ResourceRequestEvent(1, "service",
                "slo", needs2);

        RequestQueueImpl instance = new RequestQueueImpl(services, resources, orders, policies, mes);

        BlockingQueue<Request> hlp1;

        Field f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        f.set(instance, true);
        f.setAccessible(false);

        f = RequestQueueImpl.class.getDeclaredField("queued");
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);

        assertTrue("The RequestQueue is not supposed to have a queued event at this point",
                hlp1.size() == 0);

        instance.addEvent(event1);
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed to have a queued event at this point",
                hlp1.size() == 1);

        Request r1 = hlp1.peek();

        instance.addEvent(event2);
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed to have just one queued event at this point",
                hlp1.size() == 1);

        Request r2 = hlp1.peek();

        assertEquals("The Requests should be equal", r1, r2);

        assertFalse("The needs stored in first request should differ from needs stored in second request",
                r1.getCurrentNeeds().size() == r2.getCurrentNeeds().size());
        log.exiting(RequestQueueImplTest.class.getName(), "testAddEventIsActiveOverwrite");
    }

    @SuppressWarnings("unchecked")
    public void testEventSorting() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testEventSorting");

        Need highNeed = new Need(new Usage(2));
        Need lowNeed = new Need(new Usage(1));


        ResourceRequestEvent lowEvent = new ResourceRequestEvent(0, "service",
                "slo1", Collections.singleton(lowNeed));
        ResourceRequestEvent highEvent = new ResourceRequestEvent(0, "service",
                "slo2", Collections.singleton(highNeed));


        RequestQueueImpl instance = new RequestQueueImpl(services, resources, orders, policies, mes);

        BlockingQueue<Request> hlp1;

        Field f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        f.set(instance, true);
        f.setAccessible(false);

        f = RequestQueueImpl.class.getDeclaredField("queued");
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);

        assertTrue("The RequestQueue is not supposed to have a queued event at this point",
                hlp1.size() == 0);

        instance.addEvent(lowEvent);
        instance.addEvent(highEvent);
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertEquals("The RequestQueue was supposed to have two queued events at this point", 2, hlp1.size());

        assertEquals("The first queued element must be the high event", highNeed.getUrgency(),  hlp1.take().getCurrentNeeds().get(0).getUrgency());
        assertEquals("The seconds queued element must be the low event", lowNeed.getUrgency(), hlp1.take().getCurrentNeeds().get(0).getUrgency());

        log.exiting(RequestQueueImplTest.class.getName(), "testEventSorting");
    }


    /**
     * Test of addEvent method, of class RequestQueueImpl.
     */
    @SuppressWarnings("unchecked")
    public void testAddEventIsNotActive() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testAddEventIsNotActive");

        // todo make it completely right, create a RRE mockup
        ResourceRequestEvent event = new ResourceRequestEvent(0, "service",
                "slo", Collections.<Need>emptyList());
        RequestQueueImpl instance = new RequestQueueImpl(services, resources, orders, policies, mes);

        BlockingQueue<Request> hlp1;

        Field f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        f.set(instance, false);
        f.setAccessible(false);

        f = RequestQueueImpl.class.getDeclaredField("queued");
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);

        assertTrue("The RequestQueue is not supposed to have a queued event at this point",
                hlp1.size() == 0);

        instance.addEvent(event);
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue is not supposed to have a queued event at this point",
                hlp1.size() == 0);
        log.exiting(RequestQueueImplTest.class.getName(), "testAddEventIsNotActive");
    }

    /**
     * Test of triggerReprocessing method, of class RequestQueueImpl.
     */
    @SuppressWarnings("unchecked")
    public void testTriggerReprocessingBasic() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testTriggerReprocessingBasic");

        // todo make it completely right, create a RRE mockup
        ResourceRequestEvent event = new ResourceRequestEvent(0, "service",
                "slo", Collections.<Need>emptyList());
        RequestQueueImpl instance = new RequestQueueImpl(services, resources, orders, policies, mes);

        BlockingQueue<Request> hlp1;

        Field f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        f.set(instance, true);
        f.setAccessible(false);

        f = RequestQueueImpl.class.getDeclaredField("pending");
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);

        assertTrue("The RequestQueue is not supposed to have a pending event at this point",
                hlp1.size() == 0);

        hlp1.add(new RequestImpl(event, services, resources, orders, policies));
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed to have a pending event at this point",
                hlp1.size() == 1);

        BlockingQueue<Request> hlp2;

        f = RequestQueueImpl.class.getDeclaredField("queued");
        f.setAccessible(true);
        hlp2 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);

        assertTrue("The RequestQueue is not supposed to have a queued event at this point",
                hlp2.size() == 0);

        instance.triggerReprocessing();

        f.setAccessible(true);
        hlp2 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed to have a queued event at this point",
                hlp2.size() == 1);

        f = RequestQueueImpl.class.getDeclaredField("pending");
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed not to have a pending event at this point",
                hlp1.size() == 0);
        log.exiting(RequestQueueImplTest.class.getName(), "testTriggerReprocessingBasic");
    }

    /**
     * Test of triggerReprocessing method, of class RequestQueueImpl.
     */
    @SuppressWarnings("unchecked")
    public void testTriggerReprocessingNotOverWrite() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testTriggerReprocessingNotOverWrite");

        // todo make it completely right, create a RRE mockup
        ResourceRequestEvent event1 = new ResourceRequestEvent(0, "service",
                "slo", Collections.<Need>emptyList());

        // todo make it completely right, create a RRE mockup
        List<Need> needs2 = new ArrayList<Need>(1);
        Need n = new Need(ConstantFilter.<Resource>alwaysMatching(), new Usage(1), 1);
        needs2.add(n);

        ResourceRequestEvent event2 = new ResourceRequestEvent(1, "service",
                "slo", needs2);

        RequestQueueImpl instance = new RequestQueueImpl(services, resources, orders, policies, mes);

        BlockingQueue<Request> hlp1;
        BlockingQueue<Request> hlp2;

        Field f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        f.set(instance, true);
        f.setAccessible(false);

        f = RequestQueueImpl.class.getDeclaredField("queued");
        f.setAccessible(true);
        hlp2 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);

        assertTrue("The RequestQueue is not supposed to have a queued event at this point",
                hlp2.size() == 0);

        Request r2 = new RequestImpl(event2, services, resources, orders,
                policies);
        hlp2.add(r2);
        f.setAccessible(true);
        hlp2 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed to have a queued event at this point",
                hlp2.size() == 1);

        f = RequestQueueImpl.class.getDeclaredField("pending");
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);

        assertTrue("The RequestQueue is not supposed to have a pending event at this point",
                hlp1.size() == 0);

        Request r1 = new RequestImpl(event1, services, resources, orders,
                policies);
        hlp1.add(r1);
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed to have a pending event at this point",
                hlp1.size() == 1);

        instance.triggerReprocessing();

        f = RequestQueueImpl.class.getDeclaredField("queued");
        f.setAccessible(true);
        hlp2 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed to have a queued event at this point",
                hlp2.size() == 1);

        f = RequestQueueImpl.class.getDeclaredField("pending");
        f.setAccessible(true);
        hlp1 = (BlockingQueue<Request>) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue was supposed not to have a pending event at this point",
                hlp1.size() == 0);

        Request r = hlp2.peek();

        assertEquals("The Requests should be equal", r, r2);

        assertTrue("The needs stored in first request should be the same as need stored in second request",
                r.getCurrentNeeds().size() == r2.getCurrentNeeds().size());
        log.exiting(RequestQueueImplTest.class.getName(), "testTriggerReprocessingNotOverWrite");
    }

    /**
     * Test of setPolicyProvider method, of class RequestQueueImpl.
     */
    public void testSetPolicyProvider() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testSetPolicyProvider");

        PolicyManager policyProvider = new PolicyManagerMockup();
        RequestQueueImpl<Service> instance = new RequestQueueImpl<Service>(services, resources, orders, policies, mes);

        instance.setPolicyProvider(policyProvider);

        PolicyManager hlp1 = null;
        Field f = RequestQueueImpl.class.getDeclaredField("policy");
        f.setAccessible(true);
        hlp1 = (PolicyManager) f.get(instance);
        f.setAccessible(false);
        assertEquals("The original Policy and policy stored in RequestQueue are not equal",
                hlp1, policyProvider);
        log.exiting(RequestQueueImplTest.class.getName(), "testSetPolicyProvider");
    }

    /**
     * Test of start method, of class RequestQueueImpl.
     * @throws java.lang.Exception 
     */
    public void testStart() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testStart");

        RequestQueueImpl<Service> instance = new RequestQueueImpl<Service>(services, resources, orders, policies, mes);
        instance.start();

        Boolean hlp1 = false;
        Field f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        hlp1 = (Boolean) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue is not active", hlp1);
        log.exiting(RequestQueueImplTest.class.getName(), "testStart");
    }

    /**
     * Test of start method, of class RequestQueueImpl.
     * @throws java.lang.Exception 
     */
    @SuppressWarnings({"unchecked"})
    public void testStartStopStart() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testStartStopStart");

        RequestQueueImpl instance = new RequestQueueImpl(services, resources, orders, policies, mes);
        instance.start();

        Boolean hlp1 = false;
        Field f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        hlp1 = (Boolean) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue is not active", hlp1);

        instance.stop();

        f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        hlp1 = (Boolean) f.get(instance);
        f.setAccessible(false);
        assertFalse("The RequestQueue is active", hlp1);

        ExecutorService hlp2;
        f = RequestQueueImpl.class.getDeclaredField("consumerExecutor");
        f.setAccessible(true);
        hlp2 = (ExecutorService) f.get(instance);
        f.setAccessible(false);
        assertNull("The RequestQueue's consumer executor is not null", hlp2);

        Future<Void> hlp3;
        f = RequestQueueImpl.class.getDeclaredField("consumerFuture");
        f.setAccessible(true);
        hlp3 = (Future<Void>) f.get(instance);
        f.setAccessible(false);
        assertNull("The RequestQueue's consumer future is not null", hlp3);

        instance.start();

        f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        hlp1 = (Boolean) f.get(instance);
        f.setAccessible(false);
        assertTrue("The RequestQueue is not active", hlp1);

        instance.stop();
        log.exiting(RequestQueueImplTest.class.getName(), "testStartStopStart");
    }

    /**
     * Test of stop method, of class RequestQueueImpl.
     * @throws java.lang.Exception 
     */
    @SuppressWarnings("unchecked")
    public void testStop() throws Exception {
        log.entering(RequestQueueImplTest.class.getName(), "testStop");

        RequestQueueImpl instance = new RequestQueueImpl(services, resources, orders, policies, mes);
        Field f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        f.set(instance, true);
        f.setAccessible(false);

        ExecutorService s = Executors.newSingleThreadExecutor(new ThreadFactory() {

            public Thread newThread(Runnable r) {
                return new Thread(r);
            }
        });

        Future<Void> future = s.submit(new ConsumerMockup());

        f = RequestQueueImpl.class.getDeclaredField("consumerExecutor");
        f.setAccessible(true);
        f.set(instance, s);
        f.setAccessible(false);

        f = RequestQueueImpl.class.getDeclaredField("consumerFuture");
        f.setAccessible(true);
        f.set(instance, future);
        f.setAccessible(false);

        instance.stop();

        Boolean hlp1 = false;
        f = RequestQueueImpl.class.getDeclaredField("active");
        f.setAccessible(true);
        hlp1 = (Boolean) f.get(instance);
        f.setAccessible(false);
        assertFalse("The RequestQueue is active", hlp1);

        ExecutorService hlp2;
        f = RequestQueueImpl.class.getDeclaredField("consumerExecutor");
        f.setAccessible(true);
        hlp2 = (ExecutorService) f.get(instance);
        f.setAccessible(false);
        assertNull("The RequestQueue's consumer executor is not null", hlp2);

        Future<Void> hlp3;
        f = RequestQueueImpl.class.getDeclaredField("consumerFuture");
        f.setAccessible(true);
        hlp3 = (Future<Void>) f.get(instance);
        f.setAccessible(false);
        assertNull("The RequestQueue's consumer future is not null", hlp3);
        
        log.exiting(RequestQueueImplTest.class.getName(), "testStop");
    }

    private class ConsumerMockup implements Callable<Void> {

        public Void call() throws Exception {
            while (true) {
            }
        }
    }
}
