/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.order.OrderFactory;
import com.sun.grid.grm.resource.OrderStore;
import com.sun.grid.grm.resource.order.OrderType;
import com.sun.grid.grm.resource.ResourceId;

import com.sun.grid.grm.resource.filter.OrderVariableResolver;
import com.sun.grid.grm.resource.order.Order;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.util.Deque;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.ConstantFilter;
import junit.framework.*;

import java.lang.reflect.Field;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimpleOrderStoreTest extends TestCase {

    private final static Logger log = Logger.getLogger(SimpleOrderStoreTest.class.getName());
    private final static ResourceId id1 = new ResourceIdImpl(1);
    private final static ResourceId id2 = new ResourceIdImpl(2);
    private final static String source1 = "source1";
    private final static String source2 = "source2";
    private final static String dummyServiceName = "DUMMY";
    private final static String fooServiceName = "foo";
    private static OrderStore instance;
    
    private final static Need NEED = new Need(Usage.MIN_VALUE, 1);
    private final static NeedProcessor NP = new NeedProcessor(NEED);
    

    public SimpleOrderStoreTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(SimpleOrderStoreTest.class);

        return suite;
    }

    @Override
    protected void setUp() throws Exception {
        instance = new SimpleOrderStore();
    }

    @Override
    protected void tearDown() throws Exception {
        instance = null;
    }

    /**
     * Tests adding a normal (non-maintenance) order.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testAddNormal() throws Exception {
        log.log(Level.FINE, "testAddNormal");

        Order order1 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id2, source2, dummyServiceName, NP, "slo1");

        Map<String, Deque<Order>> orders = getOrders(instance);

        instance.add(order1);
        instance.add(order2);

        assertSame("First inserted order and order retrieved from the first position in list are not the same",
                order1, orders.get(order1.getHashKey()).getFirst());
        assertSame("Second inserted order and order retrieved from the second position in list are not the same",
                order2, orders.get(order2.getHashKey()).getFirst());
    }

    /**
     * Tests adding a maintenance order.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testAddMaintenance() throws Exception {
        log.log(Level.FINE, "testAddMaintenance");

        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, false);
        Order order2 = OrderFactory.createMaintenanceAssignOrder(id2, source2, dummyServiceName, false);

        Map<String, Deque<Order>> orders = getOrders(instance);

        instance.add(order1);
        instance.add(order2);

        assertSame("First inserted order and retrieved order are not the same",
                order1, orders.get(order1.getHashKey()).getFirst());
        assertSame("Second inserted order and retrieved order are not the same",
                order2, orders.get(order2.getHashKey()).getFirst());
    }

    /**
     * Tests adding a maintenance order and a possible overwriting of it.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testAddOverwriteMaintenance() throws Exception {
        log.log(Level.FINE, "testAddMaintenance");

        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, false);
        Order order2 = OrderFactory.createMaintenanceAssignOrder(id1, source1, fooServiceName, false);

        Map<String, Deque<Order>> orders = getOrders(instance);

        instance.add(order1);
        instance.add(order2);

        assertEquals("Only one deque should be stored", 1, orders.size());
        assertEquals("Both orders should be stored in deque", 2, orders.get(order1.getHashKey()).size());
        assertSame("First inserted order should be last now", order1, orders.get(order1.getHashKey()).getLast());
        assertSame("Second inserted order should be first now", order2, orders.get(order2.getHashKey()).getFirst());
    }

    /**
     * Tests adding a normal and then maintenance order.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testAddNormalThenMaintenance() throws Exception {
        log.log(Level.FINE, "testAddNormalThenMaintenance");

        Order order1 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id2, source2, dummyServiceName, NP, "slo1");
        
        Map<String, Deque<Order>> orders = getOrders(instance);

        Order query = OrderFactory.createMaintenanceAssignOrder(id2, source2, dummyServiceName, false);
        
        instance.add(order1);
        instance.add(order2);

        assertSame("First inserted order and order retrieved from the first position in normal list are not the same",
                order1, orders.get(order1.getHashKey()).getFirst());
        assertSame("Second inserted order and order retrieved from the maintenance map are not the same",
                order2, orders.get(query.getHashKey()).getFirst());
    }

    /**
     * Tests adding a maintenance and then normal order and a possible overwriting of it.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testAddMaintenanceThenNormal() throws Exception {
        log.log(Level.FINE, "testAddMaintenanceThenNormal");

        Order order1 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo");
        Order order2 = OrderFactory.createMaintenanceAssignOrder(id2, source2, dummyServiceName, false);

        Map<String, Deque<Order>> orders = getOrders(instance);

        Order query = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");
        
        instance.add(order1);
        instance.add(order2);

        assertSame("First inserted order and order retrieved from the maintenance map are not the same",
                order1, orders.get(query.getHashKey()).getFirst());
        assertSame("Second inserted order and order retrieved from the first position in normal list are not the same",
                order2, orders.get(order2.getHashKey()).getFirst());
    }

    /**
     * Tests precedence of removing a maintenance order over normal orders.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveMaintenancePrecedence() throws Exception {
        log.log(Level.FINE, "testRemoveMaintenancePrecedence");

        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, false);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo");
        Order order3 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, false);
        Order order4 = OrderFactory.createResourceRequestAssignOrder(id2, source1, dummyServiceName, NP, "slo");
        Order order5 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, true);
        Order order6 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");

        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order5);
        addOrder(orders, order2);
        addOrder(orders, order4);
        addOrder(orders, order6);

        AndFilter<Order> filter = new AndFilter<Order>(2);
        filter.add(OrderVariableResolver.newSourceFilter(source1));
        filter.add(OrderVariableResolver.newResourceIdFilter(id1));

        assertSame(
                "Order store did not take the maintenance order for resource " +
                id1.getId() + " from service " + source1, order5, instance.remove(filter));
        assertFalse(
                "Order store did not remove the first (maintenance) order for resource " +
                id1.getId() + " from service " + source1, orders.get(order5.getHashKey()).contains(order5));
        assertSame(
                "Order store did not take the second youngest order for resource " +
                id1.getId() + " from service " + source1, order1, instance.remove(filter));
        assertFalse(
                "Order store did not remove the second (maintenance) order for resource " +
                id1.getId() + " from service " + source1, orders.get(order1.getHashKey()).contains(order1));
        assertSame(
                "Order store did not take the third youngest order for resource " +
                id1.getId() + " from service " + source1, order2, instance.remove(filter));
        assertFalse(
                "Order store did not remove the third (maintenance) order for resource " +
                id1.getId() + " from service " + source1, orders.get(order2.getHashKey()).contains(order2));
        assertSame(
                "Order store did not take the fourth youngest order for resource " +
                id1.getId() + " from service " + source1, order6, instance.remove(filter));
        assertNull(
                "Order store should not have deque for order the fourth (maintenance) order for resource " +
                id1.getId() + " from service " + source1, orders.get(order6.getHashKey()));
   }

    /**
     * Tests removing of all orders if a filter is null.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAll() throws Exception {
        log.log(Level.FINE, "testRemoveAll");

        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, false);
        Order order2 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, true);
        Order order3 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, false);
        Order order4 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, true);

        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        List<Order> result = instance.removeAll(null);
        assertTrue("Order store should remove all orders", orders.isEmpty());
        assertTrue("Order store should be empty", instance.size() == 0);
        assertTrue("Order store should return in result order1:" + order1, result.contains(order1));
        assertTrue("Order store should return in result order2:" + order2, result.contains(order2));
        assertTrue("Order store should return in result order3:" + order3, result.contains(order3));
        assertTrue("Order store should return in result order4:" + order4, result.contains(order4));
    }

    /**
     * Tests removing no order if filter do not match any order.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveNone() throws Exception {
        log.log(Level.FINE, "testRemoveAll");

        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, false);
        Order order2 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, true);
        Order order3 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, false);
        Order order4 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, true);

        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        List<Order> result = instance.removeAll(ConstantFilter.<Order>neverMatching());
        assertTrue("Order store should not remove any orders", result.isEmpty());
        assertTrue("Order store keep order:" + order1, orders.get(order1.getHashKey()).contains(order1));
        assertTrue("Order store keep order:" + order2, orders.get(order2.getHashKey()).contains(order2));
        assertTrue("Order store keep order:" + order3, orders.get(order3.getHashKey()).contains(order3));
        assertTrue("Order store keep order:" + order4, orders.get(order4.getHashKey()).contains(order4));
    }

    /**
     * Tests removing orders if maintenance filter is set to true.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAllMaintenanceFilterTrue() throws Exception {
        log.log(Level.FINE, "testRemoveAllMaintenanceFilterTrue");

        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, false);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo");
        Order order3 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, false);
        Order order4 = OrderFactory.createResourceRequestAssignOrder(id2, source1, dummyServiceName, NP, "slo1");

        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        List<Order> result = instance.removeAll(OrderVariableResolver.newMaintenanceFilter(true));
        assertEquals("Order store should have left 2 normal orders", 2, instance.size());
        assertTrue("Order store should return in result order:" + order1, result.contains(order1));
        assertTrue("Order store should return in result order:" + order3, result.contains(order3));
        assertTrue("Order store should not remove normal order:" + order2, orders.get(order2.getHashKey()).contains(order2));
        assertTrue("Order store should not remove normal order:" + order4, orders.get(order4.getHashKey()).contains(order4));
    }

    /**
     * Tests removing orders if maintenance filter is set to false.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAllMaintenanceFilterFalse() throws Exception {
        log.log(Level.FINE, "testRemoveAllMaintenanceFilterFalse");

        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, false);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo");
        Order order3 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, false);
        Order order4 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");

        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        List<Order> result = instance.removeAll(OrderVariableResolver.newMaintenanceFilter(false));
        assertTrue("Order store should have left 2 normal orders", instance.size() == 2);
        assertTrue("Order store should return in result order:" + order2, result.contains(order2));
        assertTrue("Order store should return in result order:" + order4, result.contains(order4));
        assertTrue("Order store should not remove normal order:" + order1, orders.get(order1.getHashKey()).contains(order1));
        assertTrue("Order store should not remove normal order:" + order3, orders.get(order3.getHashKey()).contains(order3));
    }

    /**
     * Tests removing the order based on order filter.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveOrderFilter() throws Exception {
        log.log(Level.FINE, "testRemoveTheOrder");

        Order order1 = OrderFactory.createMaintenanceRemoveOrder(id1, source1);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");
        Order order3 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, false);
        Order order4 = OrderFactory.createResourceRequestAssignOrder(id2, source1, dummyServiceName, NP, "slo1");
        
        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        Order result = instance.remove(OrderVariableResolver.newOrderFilter(order1));
        assertSame("Order store did not remove correct order", order1, result);
        assertTrue("Order store should have 3 orders left", instance.size() == 3);
        result = instance.remove(OrderVariableResolver.newOrderFilter(order3));
        assertSame("Order store did not remove correct order", order3, result);
        assertTrue("Order store should have 2 orders left", instance.size() == 2);
        result = instance.remove(OrderVariableResolver.newOrderFilter(order4));
        assertSame("Order store did not remove correct order", order4, result);
        assertTrue("Order store should have 1 orders left", instance.size() == 1);
        result = instance.remove(OrderVariableResolver.newOrderFilter(order2));
        assertSame("Order store did not remove correct order", order2, result);
        assertTrue("Order store should have 0 orders left", instance.size() == 0);
    }

    /**
     * Tests removing all orders if maintenance filter is set to true.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAllOrderFilter() throws Exception {
        log.log(Level.FINE, "testRemoveTheOrderAll");

        Order order1 = OrderFactory.createMaintenanceRemoveOrder(id1, source1);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");
        Order order3 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, false);
        Order order4 = OrderFactory.createResourceRequestAssignOrder(id2, source1, dummyServiceName, NP, "slo1");
        
        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        List<Order> result = instance.removeAll(OrderVariableResolver.newOrderFilter(order1));
        assertSame("Order store did not remove correct order", order1, result.get(0));
        assertTrue("Order store should have 3 orders left", instance.size() == 3);
        result = instance.removeAll(OrderVariableResolver.newOrderFilter(order3));
        assertSame("Order store did not remove correct order", order3, result.get(0));
        assertTrue("Order store should have 2 orders left", instance.size() == 2);
        result = instance.removeAll(OrderVariableResolver.newOrderFilter(order4));
        assertSame("Order store did not remove correct order", order4, result.get(0));
        assertTrue("Order store should have 1 orders left", instance.size() == 1);
        result = instance.removeAll(OrderVariableResolver.newOrderFilter(order2));
        assertSame("Order store did not remove correct order", order2, result.get(0));
        assertTrue("Order store should have 0 orders left", instance.size() == 0);
    }

    /**
     * Tests removing orders based on a order type filter.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAllOrderTypeFilter() throws Exception {
        log.log(Level.FINE, "testRemoveAllOrderTypeFilterREMOVE");

        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, false);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo");
        Order order3 = OrderFactory.createMaintenanceRemoveOrder(id2, source1);
        Order order4 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");
        
        
        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        List<Order> result = instance.removeAll(OrderVariableResolver.newOrderTypeFilter(OrderType.REMOVE));
        assertEquals("Order store should remove just one REMOVE order", 3, instance.size());
        assertTrue("Order store should return in result order:" + order3, result.contains(order3));
        result = instance.removeAll(OrderVariableResolver.newOrderTypeFilter(OrderType.ASSIGN));
        assertEquals("Order store should remove two ASSIGN orders", 0, instance.size());
        assertTrue("Order store should return in result order:" + order1, result.contains(order1));
        assertTrue("Order store should return in result order:" + order2, result.contains(order2));
    }

    /**
     * Tests removing orders based on a resource id filter.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAllResourceIdFilter() throws Exception {
        log.log(Level.FINE, "testRemoveAllResourceIdFilter");

        Order order1 = OrderFactory.createMaintenanceRemoveOrder(id1, source1);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");
        Order order3 = OrderFactory.createMaintenanceAssignOrder(id2, source1, dummyServiceName, false);
        Order order4 = OrderFactory.createResourceRequestAssignOrder(id2, source1, dummyServiceName, NP, "slo1");
        
        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        List<Order> result = instance.removeAll(OrderVariableResolver.newResourceIdFilter(id1));
        assertTrue("Order store should remove all orders for id1: " + id1, instance.size() == 2);
        assertTrue("Order store should return in result order1:" + order1, result.contains(order1));
        assertTrue("Order store should return in result order2:" + order2, result.contains(order2));
        assertFalse("Order store should not return in result order3:" + order3, result.contains(order3));
        assertFalse("Order store should not return in result order4:" + order4, result.contains(order4));
    }

    /**
     * Tests removing orders based on a source filter.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAllSourceFilter() throws Exception {
        log.log(Level.FINE, "testRemoveAllSourceFilter");


        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source1, dummyServiceName, false);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo");
        Order order3 = OrderFactory.createMaintenanceRemoveOrder(id2, source2);
        Order order4 = OrderFactory.createResourceRequestAssignOrder(id2, source2, dummyServiceName, NP, "slo1");
        
        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        List<Order> result = instance.removeAll(OrderVariableResolver.newSourceFilter(source1));
        assertTrue("Order store should remove all orders for resources from service: " + source1, instance.size() == 2);
        assertTrue("Order store should return in result order1:" + order1, result.contains(order1));
        assertTrue("Order store should return in result order2:" + order2, result.contains(order2));
        assertFalse("Order store should not return in result order3:" + order3, result.contains(order3));
        assertFalse("Order store should not return in result order4:" + order4, result.contains(order4));
    }

    /**
     * Tests removing orders based on a target filter.
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testRemoveAllTargetFilter() throws Exception {
        log.log(Level.FINE, "testRemoveAllTargetFilter");

        Order order1 = OrderFactory.createMaintenanceAssignOrder(id1, source2, dummyServiceName, false);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo");
        Order order3 = OrderFactory.createMaintenanceRemoveOrder(id2, source2);
        Order order4 = OrderFactory.createResourceRequestAssignOrder(id2, source2, fooServiceName, NP, "slo");
        
        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order3);
        addOrder(orders, order2);
        addOrder(orders, order4);

        List<Order> result = instance.removeAll(OrderVariableResolver.newTargetFilter(dummyServiceName));
        assertTrue("Order store should remove all orders for resources to service: " + dummyServiceName, instance.size() == 2);
        assertTrue("Order store should return in result order1:" + order1, result.contains(order1));
        assertTrue("Order store should return in result order2:" + order2, result.contains(order2));
        assertFalse("Order store should not return in result order3:" + order3, result.contains(order3));
        assertFalse("Order store should not return in result order4:" + order4, result.contains(order4));
    }

    /**
     * Tests adding and removing orders in a "simulated multi threaded environment".
     * The use case is to test the correct result of "add(o),remove(o)" for 
     * various orders if it happens in different order, like:
     * 
     * add(o1);
     * add(o2);
     * remove(o2);
     * add(o3);
     * remove(o1);
     * remove(o3);
     * 
     * Exceptions and results are documented in the body of the method.
     * 
     * @throws java.lang.Exception
     */
    public void testSimulatedMultithreadedAddRemove() throws Exception {
        log.log(Level.FINE, "testSimulatedMultithreadedAddRemove");

        Order order1 = OrderFactory.createMaintenanceRemoveOrder(id1, source1);
        Order order2 = OrderFactory.createResourceRequestAssignOrder(id1, source1, dummyServiceName, NP, "slo1");
        Order order3 = OrderFactory.createMaintenanceAssignOrder(id2, source2, dummyServiceName, false);
        
        Map<String, Deque<Order>> orders = getOrders(instance);

        addOrder(orders, order1);
        addOrder(orders, order2);
        Order result = instance.remove(OrderVariableResolver.newOrderFilter(order2));
        assertEquals("Order store did not remove correct order", order2, result);
        assertTrue("Order store should have 1 orders left", instance.size() == 1);

        addOrder(orders, order3);
        result = instance.remove(OrderVariableResolver.newOrderFilter(order1));
        assertEquals("Order store did not remove correct order", order1, result);
        assertTrue("Order store should have 1 orders left", instance.size() == 1);

        result = instance.remove(OrderVariableResolver.newOrderFilter(order3));
        assertEquals("Order store did not remove correct order", order3, result);
        assertTrue("Order store should have 0 orders left", instance.size() == 0);

    }

    @SuppressWarnings("unchecked")
    private Map<String, Deque<Order>> getOrders(OrderStore store)
            throws Exception {
        Map<String, Deque<Order>> ret = null;

        Field f = SimpleOrderStore.class.getDeclaredField("orders");
        f.setAccessible(true);
        ret = (Map<String, Deque<Order>>) f.get(store);
        f.setAccessible(false);

        return ret;
    }

    private void addOrder(Map<String, Deque<Order>> orders, Order order) {
        Deque<Order> orderDeque = orders.get(order.getHashKey());
        if (orderDeque == null) {
            orderDeque = new Deque<Order>();
            orders.put(order.getHashKey(), orderDeque);
        }
        if (!order.isMaintenance()) {
            orderDeque.addLast(order);
        } else {
            orderDeque.addFirst(order);
        }
    }
}
