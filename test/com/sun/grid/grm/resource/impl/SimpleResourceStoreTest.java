/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/

/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.AbstractResourceType;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.UnknownResourceException;

import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import junit.framework.*;

import java.lang.reflect.Field;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class SimpleResourceStoreTest extends TestCase {
    private final static Logger log = Logger.getLogger(SimpleResourceStoreTest.class.getName());
    private final static Map<String,Object> rp = new HashMap<String,Object>();
    
    private final static MockupResourceType rt = new MockupResourceType();
    private final static ResourceId id1;
    private final static ResourceId id2;
    private final static Resource r1;
    private final static Resource r2;
    
    private static ResourceStore store;

    static {
        try {
            ExecutionEnv env = DummyExecutionEnvFactory.newInstance();
            r1 = DefaultResourceFactory.createResourceByName(env, rt, "test1");
            r2 = DefaultResourceFactory.createResourceByName(env, rt, "test2");
            id1 = r1.getId();
            id2 = r2.getId();
        } catch(Exception ex) {
            throw new IllegalStateException(ex.getMessage(), ex);
        }
    }
    
    public SimpleResourceStoreTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        store = new SimpleResourceStore();
    }

    @Override
    protected void tearDown() throws Exception {
        store = null;
    }

    public static Test suite() {
        TestSuite suite = new TestSuite(SimpleResourceStoreTest.class);

        return suite;
    }

    public void testDefaultConstructor() throws Exception {
        log.log(Level.FINE, "testDefaultConstructor");

        SimpleResourceStore tc = new SimpleResourceStore();
        Field f;
        Object field;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        field = f.get(tc);
        f.setAccessible(false);
        assertNotNull("Underlying storage is null", field);
    }

    @SuppressWarnings("unchecked")
    public void testAddOK() throws Exception {
        log.log(Level.FINE, "testAddOK");
        store.add(r1);

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        assertTrue("Resource " + r1.getId().getId() + " was NOT added",
            storage.containsKey(id1));
    }

    @SuppressWarnings("unchecked")
    public void testAddException() throws Exception {
        log.log(Level.FINE, "testAddException");

        try {
            store.add(null);
            fail("Should throw a ResourceStoreException");
        } catch (ResourceStoreException rse) {
            assertTrue("ResourceStoreException catched", true);
        }

    }

    @SuppressWarnings("unchecked")
    public void testClearOK() throws Exception {
        log.log(Level.FINE, "testClearOK");

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        storage.put(r1.getId(), r1);

        store.clear();
        assertTrue("Simple resource store is NOT empty", storage.isEmpty());
    }

    @SuppressWarnings("unchecked")
    public void testClearException() throws Exception {
        log.log(Level.FINE, "testClearException");

        Field f;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        f.set(store, null);
        f.setAccessible(false);

        try {
            store.clear();
            fail("Should throw a ResourceStoreException");
        } catch (ResourceStoreException rse) {
            assertTrue("ResourceStoreException catched", true);
        }
    }

    @SuppressWarnings("unchecked")
    public void testGetResourceOK() throws Exception {
        log.log(Level.FINE, "testGetResourceOK");

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        storage.put(r1.getId(), r1);

        Resource result = store.getResource(id1);
        assertEquals("Retrieved resource is not correct", result, r1);
    }

    @SuppressWarnings("unchecked")
    public void testGetResourceResourceStoreException()
        throws Exception {
        log.log(Level.FINE, "testGetResourceResourceStoreException");

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        storage.put(r1.getId(), r1);

        Resource result = null;

        try {
            result = store.getResource(null);
            fail("Should throw a ResourceStoreException");
        } catch (ResourceStoreException rse) {
            assertTrue("ResourceStoreException catched", true);
        }

        assertNull("Retrieved resource is NOT null", result);
    }

    @SuppressWarnings("unchecked")
    public void testGetResourcUnknownResourceException()
        throws Exception {
        log.log(Level.FINE, "testGetResourcUnknownResourceException");

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        storage.put(r1.getId(), r1);

        Resource result = null;

        try {
            result = store.getResource(id2);
            fail("Should throw a UnknownResourceException");
        } catch (UnknownResourceException ure) {
            assertTrue("UnknownResourceException catched", true);
        }

        assertNull("Retrieved resource is NOT null", result);
    }

    @SuppressWarnings("unchecked")
    public void testGetResourceIdsOK() throws Exception {
        log.log(Level.FINE, "testGetResourceIdsOK");

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        storage.put(r1.getId(), r1);

        Set<ResourceId> ids = store.getResourceIds();
        assertEquals("Simple resource store contains more resources than it should",
            ids.size(), 1);
        assertTrue("Simple resource store does not contain resource id " +
            id1.getId(), ids.contains(id1));
    }

    @SuppressWarnings("unchecked")
    public void testGetResourceIdsException() throws Exception {
        log.log(Level.FINE, "testGetResourceIdsException");

        Field f;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        f.set(store, null);
        f.setAccessible(false);

        try {
            store.getResourceIds();
            fail("Should throw a ResourceStoreException");
        } catch (ResourceStoreException rse) {
            assertTrue("ResourceStoreException catched", true);
        }
    }

    @SuppressWarnings("unchecked")
    public void testGetResourcesOK() throws Exception {
        log.log(Level.FINE, "testGetResourcesOK");

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        storage.put(r1.getId(), r1);

        List<Resource> resources = store.getResources();
        assertEquals("Simple resource store contains more resources than it should",
            resources.size(), 1);
        assertTrue("Simple resource store does not contain resource id " +
            r1.getId().getId(), resources.contains(r1));
    }

    @SuppressWarnings("unchecked")
    public void testGetResourcesException() throws Exception {
        log.log(Level.FINE, "testGetResourcesException");

        Field f;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        f.set(store, null);
        f.setAccessible(false);

        try {
            store.getResources();
            fail("Should throw a ResourceStoreException");
        } catch (ResourceStoreException rse) {
            assertTrue("ResourceStoreException catched", true);
        }
    }

    @SuppressWarnings("unchecked")
    public void testRemoveOK() throws Exception {
        log.log(Level.FINE, "testRemoveOK");

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        storage.put(r1.getId(), r1);

        Resource result = store.remove(id1);
        assertFalse("Simple resource store does not remove resource " +
            r1.getId().getId(), storage.containsKey(r1.getId()));
        assertEquals("Remove resource is not correct", r1, result);
    }

    @SuppressWarnings("unchecked")
    public void testRemoveResourceStoreException() throws Exception {
        log.log(Level.FINE, "testRemoveResourceStoreException");

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        storage.put(r1.getId(), r1);

        Resource result = null;

        try {
            result = store.remove(null);
            fail("Should throw a ResourceStoreException");
        } catch (ResourceStoreException rse) {
            assertTrue("ResourceStoreException catched", true);
        }

        assertNull("Removed resource is NOT null", result);
    }

    @SuppressWarnings("unchecked")
    public void testRemoveUnknownResourceException() throws Exception {
        log.log(Level.FINE, "testRemoveUnknownResourceException");

        Field f;
        Map<ResourceId, Resource> storage;
        f = SimpleResourceStore.class.getDeclaredField("storage");
        f.setAccessible(true);
        storage = (Map<ResourceId, Resource>) f.get(store);
        f.setAccessible(false);
        storage.put(r1.getId(), r1);

        Resource result = null;

        try {
            result = store.remove(id2);
            fail("Should throw a UnknownResourceException");
        } catch (UnknownResourceException ure) {
            assertTrue("UnknownResourceException catched", true);
        }

        assertNull("Removed resource is NOT null", result);
    }
    
    private static class MockupResourceType extends AbstractResourceType {

        private final static long serialVersionUID = -2009080301L;
        
        public String getName() {
             return "mockupResourceType";
        }

        public boolean isResourceBound(Map<String, Object> properties) {
            return true;
        }

        public String getResourceName(Map<String, Object> properties) throws InvalidResourcePropertiesException {
            return getUnboundResourceName(properties);
        }
    }
}
