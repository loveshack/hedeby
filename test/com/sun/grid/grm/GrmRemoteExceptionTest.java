/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.management.TestComponent;
import com.sun.grid.grm.management.TestComponent.TestComponentImpl;
import com.sun.grid.grm.management.TestComponent.TestComponentListenerImpl;
import com.sun.grid.grm.management.ComponentWrapper;
import com.sun.grid.grm.security.MockupInputStream;
import com.sun.grid.grm.security.MockupRmiClientSocketFactory;
import java.io.EOFException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.remote.rmi.RMIConnectorServer;

/**
 * The test addresses issue 533 - "NPE from JMX layer when network problems"
 * <p>The basic idea is to disturb the internal communication in the JMX layer and
 * check the thrown exceptions whether they are containing the NPE. The Communication
 * is mocked up. It is possible to make InputStreams throw an EOFException to simulate
 * communication problems.
 * </p>
 */
public class GrmRemoteExceptionTest extends CSMockupTestCase {

    private ComponentWrapper<TestComponent> testCompWrapper;
    private TestComponent testCompOrig;
    private TestComponentListenerImpl testCompListener;
    private ExecutionEnv env;
    
    
    private final static Logger log = Logger.getLogger(GrmRemoteExceptionTest.class.getName());

    public GrmRemoteExceptionTest(String testName) {
        super(testName);
    }

     /**
     * Sets up an mBean Server and a Component (TestComponent) to be used for testing purpose later
     * It mocks up the RmiClientSocketFactory to allow manipulation of the jmx communication process 
      * @throws Exception 
      */
    @Override
    protected void setUp() throws Exception {

        Map<String, Object> jmxEnv = new HashMap<String, Object>(3);
        // place a MockupRmiClientSocketFactory for later communication manipulation purpose 
        jmxEnv.put(RMIConnectorServer.RMI_CLIENT_SOCKET_FACTORY_ATTRIBUTE, new MockupRmiClientSocketFactory());
        
        super.setUp(jmxEnv);
        env = getEnv();
        start();
        //Setup the active JVM
        setupActiveJVM("some_vm");


        //Create and register an A MBean
        testCompOrig = new TestComponentImpl();
        ComponentInfo ci = new ComponentInfo(env.getCSHost(), "some_vm", ComponentInfo.createObjectName(env, "a", TestComponent.class));
        testCompWrapper = new ComponentWrapper<TestComponent>(testCompOrig, TestComponent.class, ci.getObjectName());
        ActiveComponent ac = ci.toActiveComponent();
        env.getContext().bind(BootstrapConstants.CS_ACTIVE_COMPONENT + "." + ci.getIdentifier(), ac);
        getMBeanServer().registerMBean(testCompWrapper, testCompWrapper.getObjectName());
        
        //TestComponentListenerImpl is added 
        testCompListener = new TestComponentListenerImpl();
        ComponentService.<TestComponent>getComponentByType(env, TestComponent.class).addTestComponentListener(testCompListener);

    }
    /**
     * clean up the system by shutting down the mBean server
     * @throws java.lang.Exception
     */
   @Override
   protected void tearDown() throws Exception {
        super.tearDown();
    }
   

    /**
     * The test tries to break a communication process defined in 
     * doSomeCommunicationAndProvokeEOFExceptionDuringStep with a mocked up 
     * EOFException. The communication process consists of a set of steps. 
     * It is repeated and every time a different step is disturbed with a mocked 
     * up EOFException
     * @throws java.lang.Exception 
     */
    public void testDisturbCommunicationWithEOF() throws Exception {
        int lastStep = 8; // steps from 0 - 8 ==> 9 times! 
        
        for (int i = 0; i <= lastStep; i++) {
            int reachedStep = doSomeCommunicationAndProvokeEOFExceptionDuringStep(i);
            if(reachedStep == i || reachedStep==lastStep){
                //Errors should happen in the step the problem is placed or not at all....
                log.log(Level.FINE,"OK! Communication was disturbed by EOF in step " + i + ". It reached step " + reachedStep + "!");
            }else{
                //... otherwise print a warning. It is known that disturbing communication can cause delayed side effects like the one below
                log.log(Level.WARNING,"Warning! Communication was disturbed by EOF in step " + i + ". It reached step " + reachedStep + "! ");
            }
        }
    }

    /*
     * This method is doing set of 9 basic communication steps (step 0 and 8 are dummy steps).
     * This set contains actions like receiving an TestComponent object, setting / getting values from the object and observes actions by an TestComponentListener
     * With exceptionStep a single step of the 9 steps can be choosen where an EOF Exception is mocked up to be thown in the communication background.
     * It will be thrown if any read of a ClientSockets input stream occurs during exceptionStep. The exception is caught and the step
     * when the error occured is reported as return value.
     */
    private int doSomeCommunicationAndProvokeEOFExceptionDuringStep(int exceptionStep) throws Exception {
        int step = 0; //counter for set of executed steps
        try {
            
            MockupInputStream.setForceEOFExceptionAtRead(step == exceptionStep); // step 0 // 
            //Do Nothing => no Exception placed;

            MockupInputStream.setForceEOFExceptionAtRead(++step == exceptionStep); // step 1
            TestComponent testComp = ComponentService.<TestComponent>getComponentByType(env, TestComponent.class);
          

            MockupInputStream.setForceEOFExceptionAtRead(++step == exceptionStep); // step 2 
            testComp.setA("a"); //communication because of state change 

            MockupInputStream.setForceEOFExceptionAtRead(++step == exceptionStep); // step 3
            assertEquals("a", testCompOrig.getA());

            MockupInputStream.setForceEOFExceptionAtRead(++step == exceptionStep); // step 4
            if (!testCompListener.waitFor("a", 500)) {
                return step; // this can fail also no explicit setForceEOFExceptionAtRead is placed 
            }
            MockupInputStream.setForceEOFExceptionAtRead(++step == exceptionStep); // step 5 
            testComp.setA("b"); //communication because of state change 

            MockupInputStream.setForceEOFExceptionAtRead(++step == exceptionStep); // step 6
            assertEquals("b", testCompOrig.getA());

            MockupInputStream.setForceEOFExceptionAtRead(++step == exceptionStep); // step 7
            if (!testCompListener.waitFor("b", 500)) {
                return step;
            }
            MockupInputStream.setForceEOFExceptionAtRead(++step == exceptionStep); // step 8
            // a final step that does nothing 
            

        } catch (Exception ex) {
            assertContainsCorrectlyWrappedEOFCause(ex);
        } finally {
            MockupInputStream.setForceEOFExceptionAtRead(false);
            ComponentService.<TestComponent>getComponentByType(env, TestComponent.class).setA(null);
        }
        return step;
    }
    
    /*
     *  the passed exception has to meet 3 constraints
     *  1. It should contain an EOFException (The mocked up one)
     *  2. This EOF should be wrapped in an GrmRemoteException (can be any outer exception)
     *  3. The GrmRemoteException should not contain "null" in the message (This addresses issue 533)
     */
     
    private void assertContainsCorrectlyWrappedEOFCause (Throwable ex) throws Exception {
        GrmRemoteException wrappingGrmRemoteException = null;
        while (ex != null) {
            //There should be an EOF Exception as a root cause
            if (ex instanceof EOFException) {
                //As any communication problem should be wrapped in a GRM Exception, this EOFException should be, too.
                assertNotNull("The caught EOFException is not wrapped in a GrmException!", wrappingGrmRemoteException);
                //here issue 533 - "NPE from JMX layer when network problems" is addressed
                assertFalse("The wrapping grmException for the caught EOFException contains with a null Message!", wrappingGrmRemoteException.getLocalizedMessage().contains("null"));
                //As the MockupInputStream directly throws a blank EOF there should be no further cause 
                //assertTrue("There should be no further Exception cause of found the EOF Exception", ex.getCause() ==null);
                return;
            }
            //found a GrmException that hopefully wraps an EOF Exception
            if (ex instanceof GrmRemoteException) {
                wrappingGrmRemoteException =  (GrmRemoteException)ex;
            }
            ex = ex.getCause();
        }
        //The cause of the examined Exception is not an EOFException
        fail("This Exception should not occur! " + ex.getClass().getName() + " " + ex.getLocalizedMessage() +". It does not contain an EOFException!");
    }
}