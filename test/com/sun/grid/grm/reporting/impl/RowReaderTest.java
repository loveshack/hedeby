/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.reporting.impl;

import com.sun.grid.grm.util.ThreadLocalStringBuffer;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import junit.framework.TestCase;

/**
 * JUnit test for the class RowReader
 */
public class RowReaderTest extends TestCase {

    public RowReaderTest(String testName) {
        super(testName);
    }

    private List<List<String>> parse(String[][] tokens) throws Exception {
        String str = buildStr(tokens);
        RowReader rr = new RowReader(new StringReader(str));

        List<List<String>> ret = new LinkedList<List<String>>();

        while (true) {
            List<String> line = new LinkedList<String>();
            if (rr.readLine(line)) {
                break;
            }
            ret.add(line);
        }
        assertTokens(tokens, ret);
        return ret;
    }

    private String buildStr(String[][] tokens) {
        ThreadLocalStringBuffer buf = ThreadLocalStringBuffer.aquire();
        try {
            PrintWriter pw = buf.getPrintWriter();

            for(int row = 0; row < tokens.length; row++) {
                if(tokens[row].length > 0) {
                    ArcoFormatter.quote(tokens[row][0], pw);
                    for (int i = 1; i < tokens[row].length; i++) {
                        pw.print(':');
                        ArcoFormatter.quote(tokens[row][i],pw);
                    }
                }
                pw.println();
            }
            pw.flush();
            return buf.toString();
        } finally {
            buf.release();
        }
    }

    private void assertTokens(String [][] tokens, List<List<String>> lines) {
        assertEquals("Expected " + tokens.length + " lines", tokens.length, lines.size());

        for(int row = 0; row < tokens.length; row++) {
            List<String> line = lines.get(row);
            assertEquals("Expected " + tokens[row].length + " tokens in row " + row, tokens[row].length, line.size());
            for(int col = 0; col < tokens[row].length; col++) {
                assertEquals("token[" + row + "][" + col + "] must be " + tokens[row][col], tokens[row][col], line.get(col));
            }
        }
    }

    public void testEmptyRow() throws Exception {
        parse(new String[0][]);
    }

    public void testSingleRowWithSingleString() throws Exception {
        String [][] tokens = {
            { "1" }
        };
        parse(tokens);
    }

    public void testSingleRowWithStrings() throws Exception {
        String tokens[][] = {
            { "1", "", ":", "\n", "\\", "\r" }
        };
        parse(tokens);
    }

    public void testMultipleRows() throws Exception {
        String tokens[][] = {
            { "1", "2ss", "3adf", "\n" },
            { "5", ":", "\t", "\r" }
        };
        parse(tokens);
    }

    public void testLastTokenIsEmpty() throws Exception {
        String tokens[][] = {
            { "1", "" }
        };
        parse(tokens);
    }

    public void testEmptyTokens() throws Exception {
        String tokens[][] = {
            { "", "" }
        };
        parse(tokens);
    }

    public void testEmptyTokensInMultipleLines() throws Exception {
        String tokens[][] = {
            { "", "" },
            { "", "" }
        };
        parse(tokens);
    }

    public void testBackslashBeforeEOF() throws Exception {

        RowReader rr = new RowReader(new StringReader("\\"));

        List<String> tokens = new LinkedList<String>();
        
        assertFalse("Must not reach EOF", rr.readLine(tokens));
        assertEquals("one token is expected", 1, tokens.size());
        assertEquals("token must be a backslash", "\\", tokens.get(0));
        tokens.clear();
        assertTrue("Must reach EOF", rr.readLine(tokens));
    }
}