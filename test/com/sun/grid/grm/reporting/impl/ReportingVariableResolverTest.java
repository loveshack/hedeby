/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.reporting.impl;

import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.reporting.RowIterator;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterHelper;
import com.sun.grid.grm.util.filter.TimeParser;
import java.util.Date;
import junit.framework.TestCase;

/**
 *
 */
public class ReportingVariableResolverTest extends TestCase {
    
    public ReportingVariableResolverTest(String testName) {
        super(testName);
    }            

    public void testTimeVarCompareWithDate() throws Exception {
        
        String timeStr = "12:00";
        Date date = TimeParser.parse(timeStr);
        DummyReport report = new DummyReport(ReportingVariableResolver.TIME.getName());
        Object[] row = new Object[] { date };
        ReportingVariableResolver resolver = new ReportingVariableResolver(report);
        resolver.setRowObject(row);
        
        Filter<Report> filter = FilterHelper.<Report>parse("TS{" + timeStr + "} = time");
        assertTrue("Filter '" + filter + "' must match", filter.matches(resolver));
    }
    
    public void testTimeVarComparedWithInteger() throws Exception {

        String timeStr = "12:00";
        Date date = TimeParser.parse(timeStr);
        DummyReport report = new DummyReport(ReportingVariableResolver.TIME.getName());
        Object[] row = new Object[] { date };
        ReportingVariableResolver resolver = new ReportingVariableResolver(report);
        resolver.setRowObject(row);
        
        Filter<Report> filter = FilterHelper.<Report>parse(" time != " + date.getTime());
        assertTrue("Filter '" + filter + "' must match", filter.matches(resolver));
    }

    public void testResourceIdFilter() throws Exception {
        DummyReport report = new DummyReport(ReportingVariableResolver.RESOURCE_ID.getName());
        Object[] row = new Object[] { "res#1" };
        ReportingVariableResolver resolver = new ReportingVariableResolver(report);
        resolver.setRowObject(row);

        Filter<Report> filter = FilterHelper.<Report>parse(" resource_id = 'res#1'");
        assertTrue("Filter '" + filter + "' must match", filter.matches(resolver));
    }

    public void testResourceNameFilter() throws Exception {
        DummyReport report = new DummyReport(ReportingVariableResolver.RESOURCE_NAME.getName());
        Object[] row = new Object[] { "localhost" };
        ReportingVariableResolver resolver = new ReportingVariableResolver(report);
        resolver.setRowObject(row);

        Filter<Report> filter = FilterHelper.<Report>parse(" resource_name = 'localhost'");
        assertTrue("Filter '" + filter + "' must match", filter.matches(resolver));
    }

    private class DummyReport implements Report {

        private final String col;
        public DummyReport(String col) {
            this.col = col;
        }
        public long getId() {
            return 0;
        }

        public int getColumnCount() {
            return 1;
        }

        public String getColumnName(int index) {
            return col;
        }

        public int getIndexForColumn(String colName) {
            if (colName.equals(col)) {
                return 0;
            }
            return -1;
        }

        public RowIterator iterator() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        
    }
}
