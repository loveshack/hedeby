/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.validate;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import junit.framework.TestCase;

public class ValidatorServiceTest extends TestCase {

    private final static Logger log = Logger.getLogger(ValidatorServiceTest.class.getName());
        
    private Map<Class, Class> validators;
    private static List<Class> testResult;
    

    public ValidatorServiceTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        log.entering(ValidatorServiceTest.class.getName(), "setup");
        super.setUp();
        validators = new HashMap<Class, Class>(6);
        validators.put(OneIMockup.class, OneIValidatorMockup.class);
        validators.put(OneCMockup.class, OneCValidatorMockup.class);
        validators.put(TwoIMockup.class, TwoIValidatorMockup.class);
        validators.put(TwoCMockup.class, TwoCValidatorMockup.class);
        validators.put(ThreeIMockup.class, ThreeIValidatorMockup.class);
        validators.put(ThreeCMockup.class, ThreeCValidatorMockup.class);
        setTestResult(new LinkedList<Class>());
        log.exiting(ValidatorServiceTest.class.getName(), "setup");
    }

    @Override
    protected void tearDown() throws Exception {
        log.entering(ValidatorServiceTest.class.getName(), "tearDown");
        super.tearDown();
        log.exiting(ValidatorServiceTest.class.getName(), "tearDown");
    }

    /**
     * Test of validate method, of class ValidatorService.
     */
    public void testValidateInterface() throws Exception {
        log.entering(ValidatorServiceTest.class.getName(), "testValidateInterface");
        ExecutionEnv env = null;
        Field f = ValidatorService.class.getDeclaredField("validators");
        f.setAccessible(true);
        f.set(null, validators);
        OneIMockup obj = new OneCMockup();
        ValidatorService.validate(env, obj);
        assertEquals("Only one validator should have been used", getTestResult().size(), 1);
        assertEquals("Validator for OneI interface should have been used", getTestResult().get(0), OneCMockup.class);
        log.exiting(ValidatorServiceTest.class.getName(), "testValidateInterface");
    }
    
    /**
     * Test of validate method, of class ValidatorService.
     */
    public void testValidateClassWInterface() throws Exception {
        log.entering(ValidatorServiceTest.class.getName(), "testValidateClassWInterface");
        
        ExecutionEnv env = null;
        Field f = ValidatorService.class.getDeclaredField("validators");
        f.setAccessible(true);
        f.set(null, validators);
        OneCMockup obj = new OneCMockup();
        ValidatorService.validate(env, obj);
        assertEquals("Only one validator should have been used", getTestResult().size(), 1);
        assertEquals("Validator for OneC class should have been used", getTestResult().get(0), OneCMockup.class);
        
        log.exiting(ValidatorServiceTest.class.getName(), "testValidateClassWInterface");
    }

    /**
     * Test of validateChain method, of class ValidatorService.
     */
    public void testValidateChainClassWInterface() throws Exception {
        log.entering(ValidatorServiceTest.class.getName(), "testValidateChainClassWInterface");
        
        ExecutionEnv env = null;
        Field f = ValidatorService.class.getDeclaredField("validators");
        f.setAccessible(true);
        f.set(null, validators);
        OneCMockup obj = new OneCMockup();
        ValidatorService.validateChain(env, obj);
        assertEquals("Two validators should have been used", getTestResult().size(), 2);
        assertEquals("Validator for OneC class should have been used first", getTestResult().get(0), OneCMockup.class);
        assertEquals("Validator for OneI interface should have been used second", getTestResult().get(1), OneIMockup.class);
        
        log.exiting(ValidatorServiceTest.class.getName(), "testValidateChainClassWInterface");
    }
    
    /**
     * Test of validateChain method, of class ValidatorService.
     */
    public void testValidateChainClassWSuperClassWInterfaces() throws Exception {
        log.entering(ValidatorServiceTest.class.getName(), "testValidateChainClassWSuperClassWInterfaces");

        ExecutionEnv env = null;
        Field f = ValidatorService.class.getDeclaredField("validators");
        f.setAccessible(true);
        f.set(null, validators);
        TwoCMockup obj = new TwoCMockup();
        ValidatorService.validateChain(env, obj);
        assertEquals("Five validators should have been used", getTestResult().size(), 5);
        assertEquals("Validator for TwoC class should have been used first", getTestResult().get(0), TwoCMockup.class);
        assertEquals("Validator for TwoI interface should have been used second", getTestResult().get(1), TwoIMockup.class);
        assertEquals("Validator for ThreeI interface should have been used third", getTestResult().get(2), ThreeIMockup.class);
        assertEquals("Validator for OneC class should have been used fourth", getTestResult().get(3), OneCMockup.class);
        assertEquals("Validator for OneI interface should have been used fifth", getTestResult().get(4), OneIMockup.class);
        
        log.exiting(ValidatorServiceTest.class.getName(), "testValidateChainClassWSuperClassWInterfaces");
    }
    
    /**
     * Test of validateChain method, of class ValidatorService.
     */
    public void testValidateChainClassWSuperClassesWInterfaces() throws Exception {
        log.entering(ValidatorServiceTest.class.getName(), "testValidateChainClassWSuperClassesWInterfaces");
        
        ExecutionEnv env = null;
        Field f = ValidatorService.class.getDeclaredField("validators");
        f.setAccessible(true);
        f.set(null, validators);
        ThreeCMockup obj = new ThreeCMockup();
        ValidatorService.validateChain(env, obj);
        assertEquals("Six validators should have been used", getTestResult().size(), 6);
        assertEquals("Validator for ThreeC class should have been used first", getTestResult().get(0), ThreeCMockup.class);
        assertEquals("Validator for TwoC class should have been used second", getTestResult().get(1), TwoCMockup.class);
        assertEquals("Validator for TwoI interface should have been used third", getTestResult().get(2), TwoIMockup.class);
        assertEquals("Validator for ThreeI interface should have been used fourth", getTestResult().get(3), ThreeIMockup.class);
        assertEquals("Validator for OneC class should have been used fifth", getTestResult().get(4), OneCMockup.class);
        assertEquals("Validator for OneI interface should have been used sixth", getTestResult().get(5), OneIMockup.class);
        log.exiting(ValidatorServiceTest.class.getName(), "testValidateChainClassWSuperClassesWInterfaces");
    }

    public static List<Class> getTestResult() {
        return testResult;
    }

    private void setTestResult(LinkedList<Class> linkedList) {
        testResult = linkedList;
    }
}
