/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.prefs.FilePreferences;
import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import junit.framework.*;

/**
 *  Test for the class PreferencesUtil
 */
public class PreferencesUtilTest extends TestCase {
    
    private String systemName = "PreferencesUtilTest" + System.currentTimeMillis();
    
    public PreferencesUtilTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        File tmpDir = TestUtil.getTmpDir(this);
        
        Field systemRootDirField = FilePreferences.class.getDeclaredField("systemRootDir");
        systemRootDirField.setAccessible(true);
        systemRootDirField.set(FilePreferences.class, new File(tmpDir, "systemPrefs"));
        systemRootDirField.setAccessible(false);
                
        Field systemRootField = FilePreferences.class.getDeclaredField("systemRoot");
        systemRootField.setAccessible(true);
        systemRootField.set(FilePreferences.class, null);
        systemRootField.setAccessible(false);
        
        Field userRootDirField = FilePreferences.class.getDeclaredField("userRootDir");
        userRootDirField.setAccessible(true);
        userRootDirField.set(FilePreferences.class, new File(tmpDir, "userPrefs"));
        userRootDirField.setAccessible(false);
                
        Field userRootField = FilePreferences.class.getDeclaredField("userRoot");
        userRootField.setAccessible(true);
        userRootField.set(FilePreferences.class, null);
        userRootField.setAccessible(false);
        
    }
    
    
    
    @Override
    protected void tearDown() throws Exception {
        // make sure the system is deleted
        PreferencesUtil.deleteSystem(systemName, PreferencesType.USER);
    }
    

    public void testAll() throws Exception {
        // create all files in temporary directory
        final File tmpDir = TestUtil.getTmpDir(this);
        File distDir = new File(tmpDir, "dist");
        File localSpoolDir = new File(tmpDir, "spool");
        final String localhost_name = Hostname.getLocalHost().getHostname();
        
        Map<String, Object> properties = new HashMap<String, Object>();
        
        properties.put(ExecutionEnv.AUTO_START, true);
        properties.put(ExecutionEnv.NO_SSL, true);
        
        Hostname csHost = Hostname.getLocalHost();
        int port = 5000;
        ExecutionEnv env = ExecutionEnvFactory.newInstance(systemName ,localSpoolDir, distDir, csHost, port, properties);
        
        // check isHost on non-existing system
        assertFalse("isHost - invalid system",
                PreferencesUtil.isHost(systemName, PreferencesType.USER, localhost_name));
        // ... make sure that the system does not exist now
        assertFalse("isHost - system did not come into existence",
                PreferencesUtil.existsSystem(systemName, PreferencesType.USER));
        // ... and test again to make sure that existsSystem did not create anything
        assertFalse("existsSystem - system did not come into existence",
                PreferencesUtil.existsSystem(systemName, PreferencesType.USER));        
        
        PreferencesUtil.addSystem(env, PreferencesType.USER);
        
        ExecutionEnv env1 = ExecutionEnvFactory.newInstanceFromPrefs(env.getSystemName(), PreferencesType.USER);
        
        // check isHost on existing system
        assertTrue("isHost - localhost does exist as a host in the system",
                PreferencesUtil.isHost(systemName, PreferencesType.USER, localhost_name));
        // ... and make sure that non-existent hostname does NOT show up
        assertFalse("isHost - invalid hostname", PreferencesUtil.isHost(systemName, PreferencesType.USER, "not*a*valid*hostname"));
        
        // compare attributes from preferences
        assertEquals("getSystemName matches", env.getSystemName(), env1.getSystemName());
        assertEquals("getDistDir matches", env.getDistDir().getAbsolutePath(), env1.getDistDir().getAbsolutePath());
        assertEquals("getLocalSpoolDir matches",
                env.getLocalSpoolDir().getAbsolutePath(), env1.getLocalSpoolDir().getAbsolutePath());
        assertEquals("property AUTOSTART matches",
                env.getProperties().get(ExecutionEnv.AUTO_START), env1.getProperties().get(ExecutionEnv.AUTO_START));
        assertEquals("property NO_SSL matches",
                env.getProperties().get(ExecutionEnv.NO_SSL), env1.getProperties().get(ExecutionEnv.NO_SSL));
        assertEquals("CS URL matches", env.getCSURL(), env1.getCSURL());
        
        String orgDefaultSystem = PreferencesUtil.getDefaultSystemFromPrefs();
        
        try {
            PreferencesUtil.setDefaultSystem(systemName);
            
            assertEquals(systemName, PreferencesUtil.getDefaultSystemFromPrefs());
            
            PreferencesUtil.resetDefaultSystem();
            assertNull(PreferencesUtil.getDefaultSystemFromPrefs());
            
        } finally {
            if(orgDefaultSystem != null) {
                PreferencesUtil.setDefaultSystem(orgDefaultSystem);
            }
        }
        
        
        assertTrue("System deleted OK", PreferencesUtil.deleteSystem(env.getSystemName(), PreferencesType.USER));
        assertFalse("System no longer exists after delete",
                PreferencesUtil.existsSystem(env.getSystemName(), PreferencesType.USER));
        
    }

}
