/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;
import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmComponentBase;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.config.executor.ExecutorConfig;
import com.sun.grid.grm.management.ComponentWrapper;
import com.sun.grid.grm.util.Hostname;
import java.util.List;
import junit.framework.Test;
import junit.framework.TestSuite;



public class ComponentServiceTest extends CSMockupTestCase {


    private AImpl aImpl;
    private ComponentWrapper<GrmComponent> aWrapper;

    private AAImpl aaImpl;
    private ComponentWrapper<GrmComponent> aaWrapper;
    private Hostname localhost;

    private ExecutionEnv env;
    

    public ComponentServiceTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
        super.setUp();

        localhost = Hostname.getLocalHost();

        env = this.getEnv();
        aImpl = new AImpl(env, "a");
        aWrapper = ComponentWrapper.newInstance(env, aImpl, A.class);
        ComponentInfo ci = new ComponentInfo(env.getCSHost(), BootstrapConstants.CS_JVM, aWrapper.getObjectName());
        ActiveComponent ac = ci.toActiveComponent();
        env.getContext().bind(BootstrapConstants.CS_ACTIVE_COMPONENT+"."+ci.getIdentifier(),ac);
        
        super.setupActiveJVM("some_vm");
        

        aaImpl = new AAImpl(env, "aa");
        aaWrapper = ComponentWrapper.newInstance(env, aaImpl, AA.class);
        ci = new ComponentInfo(env.getCSHost(), BootstrapConstants.CS_JVM, aaWrapper.getObjectName());
        ac = ci.toActiveComponent();
        env.getContext().bind(BootstrapConstants.CS_ACTIVE_COMPONENT+"."+ci.getIdentifier(),ac);   
        this.start();
        this.getMBeanServer().registerMBean(aWrapper, aWrapper.getObjectName());
        this.getMBeanServer().registerMBean(aaWrapper, aaWrapper.getObjectName());
    }
     

    @Override
    protected void tearDown() throws Exception {
        this.getMBeanServer().unregisterMBean(aWrapper.getObjectName());
        this.getMBeanServer().unregisterMBean(aaWrapper.getObjectName());
        super.tearDown();
    }


    public void testGetComponentInfo() throws Exception {

        ComponentInfo info = ComponentService.getComponentInfoByName(env, aImpl.getName());
        assertNotNull(info);
        assertEquals(info.getName(), aImpl.getName());
        assertEquals(info.getInterface(), A.class.getName());
        assertEquals(info.getObjectName(), aWrapper.getObjectName());

        info = ComponentService.getComponentInfoByName(env, aaImpl.getName());
        assertNotNull(info);
        assertEquals(info.getName(), aaImpl.getName());
        assertEquals(info.getInterface(), AA.class.getName());
        assertEquals(info.getObjectName(), aaWrapper.getObjectName());

        info = ComponentService.getComponentInfoByNameAndType(env, aImpl.getName(), A.class);
        assertNotNull(info);
        assertEquals(info.getName(), aImpl.getName());
        assertEquals(info.getInterface(), A.class.getName());
        assertEquals(info.getObjectName(), aWrapper.getObjectName());

        info = ComponentService.getComponentInfoByNameAndType(env, aaImpl.getName(), A.class);
        assertNotNull(info);
        assertEquals(info.getName(), aaImpl.getName());
        assertEquals(info.getInterface(), AA.class.getName());
        assertEquals(info.getObjectName(), aaWrapper.getObjectName());

        List<ComponentInfo> infos = ComponentService.getComponentInfosByTypeAndHost(env, A.class, localhost);
        assertEquals("Expect two component info, because a and aa implements A", 2, infos.size());
        boolean foundA = false;
        boolean foundAA = false;
        
        for(ComponentInfo ci: infos) {
            if(ci.getName().equals(aImpl.getName())) {
                foundA = true;
            } else if (ci.getName().equals(aaImpl.getName())) {
                foundAA = true;
            }
        }
        assertTrue("a not found", foundA);
        assertTrue("aa not found", foundAA);
        

        info = ComponentService.getComponentInfoByTypeAndHost(env, AA.class, localhost);
        assertNotNull(info);
        assertEquals(info.getName(), aaImpl.getName());
        assertEquals(info.getInterface(), AA.class.getName());
        assertEquals(info.getObjectName(), aaWrapper.getObjectName());

        info = ComponentService.getComponentInfoByNameAndHost(env, aImpl.getName(), localhost);
        assertNotNull(info);
        assertEquals(info.getName(), aImpl.getName());
        assertEquals(info.getInterface(), A.class.getName());
        assertEquals(info.getObjectName(), aWrapper.getObjectName());

        info = ComponentService.getComponentInfoByNameAndHost(env, aaImpl.getName(), localhost);
        assertNotNull(info);
        assertEquals(info.getName(), aaImpl.getName());
        assertEquals(info.getInterface(), AA.class.getName());
        assertEquals(info.getObjectName(), aaWrapper.getObjectName());

        // Negative tests
        assertNull(ComponentService.getComponentInfoByName(env, "b"));
        assertNull(ComponentService.getComponentInfoByNameAndType(env, "b", A.class));
        assertNull(ComponentService.getComponentInfoByNameAndType(env, "b", AA.class));
        assertNull(ComponentService.getComponentInfoByNameAndType(env, aImpl.getName(), AA.class));
    }

    public void testGetComponent() throws Exception {       
        List<A> as = ComponentService.<A>getComponentsByType(env, A.class);
        boolean foundA = false;
        boolean foundAA = false;
        for(A a: as) {
            if(aImpl.getName().equals(a.getName())) {
                foundA = true;
            } else if (aaImpl.getName().equals(a.getName())) {
                foundAA = true;
            }
        }
        assertTrue("a must be in the returned list", foundA);
        assertTrue("aa must be in the returned list", foundAA);

        A a = ComponentService.<A>getComponentByNameAndType(env, "a", A.class);
        assertNotNull(a);
        assertEquals(aImpl.getName(), a.getName());

        a = ComponentService.<A>getComponentByNameTypeAndHost(env, "a", A.class, localhost);
        assertNotNull(a);
        assertEquals(aImpl.getName(), a.getName());

        AA aa = ComponentService.<AA>getComponentByType(env, AA.class);
        assertNotNull(aa);
        assertEquals(aaImpl.getName(), aa.getName());       
    }

    public void testGetComponentInfos() throws Exception {

        List<ComponentInfo> infos = ComponentService.getComponentInfosByType(env, A.class);
        assertEquals(2, infos.size());

        infos = ComponentService.getComponentInfosByType(env, AA.class);
        assertEquals(1, infos.size());

        infos = ComponentService.getComponentInfosByTypeAndHost(env, A.class, localhost);
        assertEquals(2, infos.size());

        infos = ComponentService.getComponentInfosByTypeAndHost(env, AA.class, localhost);
        assertEquals(1, infos.size());

        infos = ComponentService.getComponentInfosByNameAndType(env, aImpl.getName(), A.class);
        assertEquals(1, infos.size());

        infos = ComponentService.getComponentInfosByNameAndType(env, aaImpl.getName(), A.class);
        assertEquals(1, infos.size());

        infos = ComponentService.getComponentInfosByNameAndType(env, aaImpl.getName(), AA.class);
        assertEquals(1, infos.size());

        // Negative tests
        infos = ComponentService.getComponentInfosByType(env, List.class);
        assertTrue(infos.isEmpty());
        infos = ComponentService.getComponentInfosByNameAndType(env, "b", A.class);
        assertTrue(infos.isEmpty());
        infos = ComponentService.getComponentInfosByNameAndType(env, "b", AA.class);
        assertTrue(infos.isEmpty());
    }



    public static Test suite() {
        TestSuite suite = new TestSuite(ComponentServiceTest.class);

        return suite;
    }

    public static interface A extends GrmComponent {
    }

    public static interface AA extends A {
    }

    public static class AImpl extends GrmComponentBase<ExecutorConfig> implements A {

        public AImpl(ExecutionEnv env, String name) {
            super(env, name);
        }

        public void start() throws GrmException {
        }

        public void stop(boolean isForced) throws GrmException {
        }

        public void reload(boolean isForced) throws GrmException {
        }
    }

    public static class AAImpl extends AImpl implements AA {

        public AAImpl(ExecutionEnv env, String name) {
            super(env, name);
        }
    }
}
