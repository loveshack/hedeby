/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.ObjectName;
import junit.framework.*;

/**
 *
 */
public class ComponentInfoTest extends TestCase {
    
    private final static Logger log = Logger.getLogger(ComponentInfoTest.class.getName());
    
    File basePath = null;
    
    public ComponentInfoTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        basePath =  TestUtil.getTmpDir(this);
        basePath.mkdir();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        Platform.getPlatform().removeDir(basePath, true);
    }
    
    public static Test suite() {
        TestSuite suite = new TestSuite(ComponentInfoTest.class);
        
        return suite;
    }
    
    public void testIsAssignableTo() throws Exception {
        
        Hostname host = Hostname.getLocalHost();
        ObjectName name = new ObjectName("a:name=1,type=" + Service.class.getName());
        ComponentInfo instance = new ComponentInfo(host, "jvm", name);

        assertTrue("Service is assignable to Service", instance.isAssignableTo(Service.class));
        assertTrue("Service is assignable to GrmComponent",  instance.isAssignableTo(GrmComponent.class));
        
        // Serive is an interface, not an object
        assertFalse("Service is not assignable to Object", instance.isAssignableTo(Object.class));
        assertFalse("Service is not assignable to OList", instance.isAssignableTo(List.class));
        
    }

    /**
     * Test of getName method, of class com.sun.grid.grm.bootstrap.ComponentInfo.
     * @throws java.lang.Exception 
     */
    public void testGetName() throws Exception {
        log.log(Level.FINE,"getName");
        
        Hostname host = Hostname.getLocalHost();
        ObjectName name = new ObjectName("a:name=1,type=" + Service.class.getName());
        ComponentInfo instance = new ComponentInfo(host, "jvm", name);
        
        ObjectName result = instance.getObjectName();
        assertEquals(name, result);
        
        try {
            instance = new ComponentInfo(host, "jvm", null);
            fail("Allowed null object name");
        } catch (NullPointerException e) {
            // Ignore
        }
    }

    /**
     * Test of hashCode method, of class com.sun.grid.grm.bootstrap.ComponentInfo.
     * @throws java.lang.Exception 
     */
    public void testHashCode() throws Exception {
        log.log(Level.FINE,"hashCode");
        
        Hostname host = Hostname.getLocalHost();
        ObjectName name = new ObjectName("a:name=1,type=" + Service.class.getName());
        ComponentInfo instance1 = new ComponentInfo(host, "jvm", name);
        ComponentInfo instance2 = new ComponentInfo(host, "jvm", name);
        
        int expResult = instance1.hashCode();
        int result = instance2.hashCode();
        assertEquals("unexpected hashcode", expResult, result);
    }

    /**
     * Test of equals method, of class com.sun.grid.grm.bootstrap.ComponentInfo.
     * @throws java.lang.Exception 
     */
    public void testEquals() throws Exception {
        log.log(Level.FINE,"equals");
        
        Hostname host = Hostname.getLocalHost();
        Hostname host1 = Hostname.getInstance("foo");
        ObjectName name = new ObjectName("a:name=1,type=" + Service.class.getName());
        ComponentInfo instance1 = new ComponentInfo(host, "jvm", name);
        ComponentInfo instance2 = new ComponentInfo(host, "jvm", name);
        ComponentInfo instance3 = new ComponentInfo(host1, "jvm", name);
        
        assertTrue("jvm@localhost[0] == jvm@localhost[1]", instance1.equals(instance2));
        assertTrue("jvm@localhost[1] == jvm@localhost[0]", instance2.equals(instance1));
        assertFalse("jvm@localhost[0] != jvm@foo", instance1.equals(instance3));
        assertFalse("jvm@localhost[1] != jvm@foo", instance2.equals(instance3));
    }

}

