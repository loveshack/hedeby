/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.bootstrap;

import junit.framework.*;

/**
 * TODO (rh) remove this test with cleanup of GrmName
 */
public class SystemUtilTest extends TestCase {
    public SystemUtilTest(String testName) throws UnregisterComponentException {
        super(testName);
    }
    
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public static Test suite() {
        TestSuite suite = new TestSuite(SystemUtilTest.class);
        
        return suite;
    }
    
    public void test_Valid_Component_Names() throws InvalidComponentNameException{
        String validNames [] = {
            "ValidName", "AnaotherValidName1", "123", "a", "B", "0", "test_underscore"
        };
        for (String validName : validNames) {
            SystemUtil.validateComponentName(validName);
        }
    }
    
    public void test_Invalid_Component_Names() {
        String invalidNames [] = {
            "prefix*", "Part#", "part1+", "??xekuhtor" , "A_Invalid-Name", "add//as", "a+", "", "-", " ", "ad ad", "ad/dd"
        };
        
        for (String invalidName : invalidNames) {
            try {
                SystemUtil.validateComponentName(invalidName);
                String errorMessage = "System name \""+ invalidName + "\" should throw InvalidGrmNameException.";
                this.fail(errorMessage);
            } catch (InvalidComponentNameException ige) {
                ; //OK
            }
            
        }
    }

    public void test_Valid_System_Names() throws InvalidComponentNameException{
        String validNames [] = {
            "ValidName", "AnaotherValidName1", "123", "a", "B", "0"
        };
        for (String validName : validNames) {
            SystemUtil.validateSystemName(validName);
        }
    }

    public void test_Invalid_System_Names() {
        String invalidNames [] = {
            "prefix*", "Part#", "part1+", "??xekuhtor" , "A_Invalid-Name", "add//as", "a+", "", "-", " ", "ad ad", "ad/dd", "test_underscore"
        };

        for (String invalidName : invalidNames) {
            try {
                SystemUtil.validateSystemName(invalidName);
                String errorMessage = "System name \""+ invalidName + "\" should throw InvalidGrmNameException.";
                this.fail(errorMessage);
            } catch (InvalidComponentNameException ige) {
                ; //OK
            }

        }
    }
    
}

