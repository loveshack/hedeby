/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.naming.DummyConfigurationServiceContext;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdException;
import com.sun.grid.grm.resource.impl.DummyResourceIdFactory;
import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.security.NullSecurityContextFactory;
import com.sun.grid.grm.ui.CommandService;
import com.sun.grid.grm.ui.impl.DummyCommandServiceImpl;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import javax.naming.Context;

/**
 * Factory for the dummy execution env
 */
public class DummyExecutionEnvFactory {

    /**
     * This constant defines the default port for the CS jvm of a dummy system.
     * The value is 7778. This port is used by junit tests to setup a dummy system
     * If more then one junit test using a dummy system runs at the same time
     * at the same host it may come to problems (because port is already bound).
     * In this case the user should set the system property
     * <code>com.sun.grid.grm.cs_dummy_port</code> to define a different port.
     *
     * This constant is package private because it is used inside of junit tests
     */
    static final int DEFAULT_CS_PORT_FOR_DUMMY_SYSTEM = 7778;

    /**
     * Defines the name of the system property which holds the port of the cs
     * jvm of a dummy system (<code>com.sun.grid.grm.cs_dummy_port</code>).
     *
     * This constant is package private because it is used inside of junit tests
     */
    static final String CS_DUMMY_PORT_SYSTEM_PROPERTY = "com.sun.grid.grm.cs_dummy_port";

    /**
     * Create a new instance of the <code>ExecutionEnv</code>.
     * This instance should be used in Junit test, commands operate on memory,
     * CommandService for such ExecutionEnv is a dummy one.
     * where cshost is set to LocalHost
     * where csPort port is set to some default value
     * where systemName is set to "dummySystem"
     * where localSpoolDir is set to new File(System.getProperty("java.io.tmpdir"), "localspool")
     * where distDir path is set to new File(System.getProperty("java.io.tmpdir"), "dist")
     *
     * @return the <code>ExecutionEnv</code>
     */
    public static ExecutionEnv newInstance() {
        try {
            File tmpDir = getTmpDir();
            return new ExecutionEnvImpl.Builder("dummySystem")
                    .csInfo(Hostname.getLocalHost(), getDummyPort())
                    .localSpoolDir(new File(tmpDir, BootstrapConstants.LOCAL_SPOOL_KEY))
                    .distDir(new File(tmpDir, BootstrapConstants.DIST_KEY))
                    .securityContextFactory(NullSecurityContextFactory.getInstance())
                    .contextFactory(DUMMY_CONTEXT_FACTORY)
                    .csFactory(DUMMY_COMMAND_SERVICE_FACTORY)
                    .resourceIdFactoryFactory(DUMMY_RESOURCE_ID_FACTORY_FACTORY)
                    .newInstance();
        } catch (GrmSecurityException ex) {
            throw new IllegalStateException("Must not happen, NULL_SECURITY_CONTEXT_FACTORY does not throw an exception");
        }
    }

    private final static FactoryWithParam<FactoryWithException<ResourceId,ResourceIdException>,ExecutionEnv> DUMMY_RESOURCE_ID_FACTORY_FACTORY = new FactoryWithParam<FactoryWithException<ResourceId,ResourceIdException>,ExecutionEnv>() {
        public FactoryWithException<ResourceId, ResourceIdException> newInstance(ExecutionEnv param) {
            return new DummyResourceIdFactory();
        }
    };
    
    private final static FactoryWithParam<CommandService,ExecutionEnv> DUMMY_COMMAND_SERVICE_FACTORY = new FactoryWithParam<CommandService,ExecutionEnv>() {
        public CommandService newInstance(ExecutionEnv env) {
            return new DummyCommandServiceImpl(env);
        }
    };

    private final static FactoryWithParamAndException<Context,ExecutionEnv,GrmException> DUMMY_CONTEXT_FACTORY = new FactoryWithParamAndException<Context,ExecutionEnv,GrmException>() {
        public Context newInstance(ExecutionEnv env) throws GrmException {
            return new DummyConfigurationServiceContext();
        }
    };

    /**
     * This method get port for the CS jvm of a dummy system.
     *
     * <p>If CS port for dummy systems can be set via the system property
     *    <code>com.sun.grid.grm.cs_dummy_port</code>. If this property is
     *    defined and it has an valid integer value the CS port for dummy
     *    systems is taken from this system property. Otherwise the default
     *    dummy port is used</p>
     * @return
     */
    private static int getDummyPort() {
        String prop = System.getProperty(CS_DUMMY_PORT_SYSTEM_PROPERTY);
        int ret;
        if (prop == null) {
            ret = DEFAULT_CS_PORT_FOR_DUMMY_SYSTEM;
        } else {
            try {
                ret = Integer.parseInt(prop);
                if (ret <= 0 && ret >= 65535 ) {
                    ret = DEFAULT_CS_PORT_FOR_DUMMY_SYSTEM;
                }
            } catch(NumberFormatException nfe) {
                ret = DEFAULT_CS_PORT_FOR_DUMMY_SYSTEM;
            }
        }
        return ret;
    }

    private static File getTmpDir() {
        File baseDir = new File(System.getProperty("java.io.tmpdir"), System.getProperty("user.name"));
        int i = 0;
        File ret;
        do {
            ret = new File(baseDir, "dummySystem" + i++);
        } while(ret.exists());
        Platform.getPlatform().deleteOnExit(ret);
        return ret;
    }
}
