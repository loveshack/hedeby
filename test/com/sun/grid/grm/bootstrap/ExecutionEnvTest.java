/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.util.Hostname;
import java.util.logging.Logger;
import javax.management.remote.JMXServiceURL;
import junit.framework.*;

/**
 * Right now this test just check if JMX URL produced in ExecutionEnv is ok.
 */
public class ExecutionEnvTest extends TestCase {
    
    private final static Logger log = Logger.getLogger(ExecutionEnvTest.class.getName());
    public ExecutionEnvTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testValidJMXURL() throws Exception {
        log.entering(ExecutionEnvTest.class.getName(), "testValidJMXURL");
        String csHost = "dummyHost";
        String systemName = "test";
        int csPort = 123;
        ExecutionEnv ee = new ExecutionEnvImpl.Builder(systemName).csInfo(Hostname.getInstance(csHost), csPort).newInstance();
        JMXServiceURL csURL = new JMXServiceURL("service:jmx:rmi://"
                + csHost
                + "/jndi/rmi://"
                + csHost
                + ":"
                + csPort
                + "/"
                + systemName
                );
        assertEquals(ee.getCSHost(), Hostname.getInstance("dummyHost"));
        assertEquals(ee.getCSPort(), 123);
        assertEquals(ee.getCSURL(), csURL);
        
        log.exiting(ExecutionEnvTest.class.getName(), "testValidJMXURL");
    }
    
    /**
     * This method checks that the CS port of a dummy system is taken from the
     * system property cs_dummy_port
     * @throws java.lang.Exception
     */
    public void testDummyPort() throws Exception {
        log.entering(ExecutionEnvTest.class.getName(), "testDummyPort");
        
        String value = System.getProperty(DummyExecutionEnvFactory.CS_DUMMY_PORT_SYSTEM_PROPERTY);
        int expectedPort = 0;
        if (value == null) {
            expectedPort = DummyExecutionEnvFactory.DEFAULT_CS_PORT_FOR_DUMMY_SYSTEM;
        } else {
            try {
                expectedPort = Integer.parseInt(value);
                if (expectedPort <= 0 && expectedPort >= 65535) {
                    fail("System property " + DummyExecutionEnvFactory.CS_DUMMY_PORT_SYSTEM_PROPERTY + " must contain a posstive integer value");
                }
            } catch(NumberFormatException ex) {
                fail("System property " + DummyExecutionEnvFactory.CS_DUMMY_PORT_SYSTEM_PROPERTY + " must contain a posstive integer value");
            }                
        }
        
        log.fine("Check that dummy CS system uses port " + expectedPort);
        
        ExecutionEnv env = DummyExecutionEnvFactory.newInstance();
        assertEquals("CS port of the dummy system must be equals to the content of system property " + DummyExecutionEnvFactory.CS_DUMMY_PORT_SYSTEM_PROPERTY,
                expectedPort, env.getCSPort());
        
        log.exiting(ExecutionEnvTest.class.getName(), "testDummyPort");
        
    }

}
