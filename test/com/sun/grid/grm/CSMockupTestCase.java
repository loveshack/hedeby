/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JvmInfo;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.config.naming.DummyConfigurationServiceContext;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.VersionString;
import com.sun.grid.grm.config.naming.FileCache;
import com.sun.grid.grm.util.Platform;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Collections;
import java.util.Map;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.naming.Context;
import junit.framework.TestCase;

/**
 * The class wraps all functionality to setup a SDM system with a ConnectorServer
 * 
 * The setup requires a registry that is managed statically in this class. There
 * have been issues with restarting the registry. the registry is lazy created once
 * for all test of the jUnit test suite.
 * 
 */
public class CSMockupTestCase extends TestCase {

    /** Creates a new instance of CSMockupTestCase
     * @param testName test case identifier
     */
    public CSMockupTestCase(String testName) {
        super(testName);
    }
    private volatile ExecutionEnv env;
    /*This registry object will be only created once for all tests in the junit test environment*/
    private volatile static Registry registry;
    private volatile MBeanServer mBeanServer;
    private volatile JMXConnectorServer jmxConnectorServer;
    private boolean localspool = false;

    @Override
    protected void setUp() throws Exception {
        setUp(Collections.<String, Object>emptyMap());
    }

    /**
     * Variant of the setUp method to pass a jmxEnv
     * @param jmxEnv
     * @throws java.lang.Exception
     */
    protected void setUp(Map<String, Object> jmxEnv) throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
        this.setUpContext(env);
        this.setSystemVersion(env);
        //lazy initialization of a static registry that is reused for all further tests
        if (registry == null) {
            registry = LocateRegistry.createRegistry(getEnv().getCSPort());
        }
        mBeanServer = MBeanServerFactory.createMBeanServer();
        jmxConnectorServer = JMXConnectorServerFactory.newJMXConnectorServer(env.getCSURL(), jmxEnv, mBeanServer);
    }

    @Override
    protected void tearDown() throws Exception {
        clearContext(env);
        clearSystemVersion(env);
        getJmxConnectorServer().stop();
        if (mBeanServer != null) {
            MBeanServerFactory.releaseMBeanServer(mBeanServer);
        }
        //we do *NOT* do a UnicastRemoteObject.unexportObject(registry, true); because we want to reuse the registry for the next test!
    }

    /**
     * Start the JmxConnector server
     * @throws java.io.IOException
     */
    public void start() throws IOException {
        getJmxConnectorServer().start();
    }

    /**
     * Reset of the MBeanServer and the JMXConnectionServer
     * @param jmxEnv
     * @throws java.lang.Exception
     */
    public void resetMBeanAndJMXConnectorServer(Map<String, Object> jmxEnv) throws Exception {
        getJmxConnectorServer().stop();
        if (mBeanServer != null) {
            MBeanServerFactory.releaseMBeanServer(mBeanServer);
        }
        do {
            Thread.sleep(1000);
        } while (MBeanServerFactory.findMBeanServer(null).contains(mBeanServer));

        mBeanServer = MBeanServerFactory.createMBeanServer();
        jmxConnectorServer = JMXConnectorServerFactory.newJMXConnectorServer(env.getCSURL(), jmxEnv, mBeanServer);
        jmxConnectorServer.start();
    }

    /**
     * setup an active JVM
     * @param name name of the JVM
     * @throws java.lang.Exception
     */
    public void setupActiveJVM(String name) throws Exception {
        //Setup the active JVM
        ActiveJvm aj = new ActiveJvm();
        aj.setName(name);
        aj.setPort(env.getCSPort());
        aj.setHost(env.getCSHost().getHostname());
        JvmInfo ji = new JvmInfo(aj.getName(), Hostname.getInstance(aj.getHost()));
        env.getContext().bind(BootstrapConstants.CS_ACTIVE_JVM + "." + ji.getIdentifier(), aj);
    }

    public ExecutionEnv getEnv() {
        return env;
    }

    public JMXConnectorServer getJmxConnectorServer() {
        return jmxConnectorServer;
    }

    public MBeanServer getMBeanServer() {
        return mBeanServer;
    }

    /**
     * Method for initializing the Context. The predefined components are placed in 
     * Context so basic system operation can be done on context
     * @param env ExecutionEnv from which the context is obtained
     * @throws java.lang.Exception 
     */
    public void setUpContext(ExecutionEnv env) throws Exception {
        Context ctx = env.getContext();
        Map<String, Object> content = TestUtil.getPredefinedSystem(env);
        for (String name : content.keySet()) {
            ctx.bind(name, content.get(name));
        }

    }

    /**
     * Clear the DummyConfiguratioServiceContext memory
     * @param env ExecutionEnv from which the context is obtained
     * @throws java.lang.Exception 
     */
    public void clearContext(ExecutionEnv env) throws Exception {

        Context ctx = env.getContext();
        if (ctx instanceof DummyConfigurationServiceContext) {
            ((DummyConfigurationServiceContext) ctx).clear();
        }
    }
    /**
     * Creates dummy version file in local spool so dummy ConfigurationServiceImpl can be created
     * @param env ExecutionEnv
     * @throws java.lang.Exception - when version file is not created
     */
    public void setSystemVersion(ExecutionEnv env) throws Exception {
        if (!env.getLocalSpoolDir().exists()) {
            if (!env.getLocalSpoolDir().mkdirs()) {
                throw new Exception("Failed to create localspool");
            }
            localspool = true;
        }
        VersionString v = new VersionString();
        v.setVersion("myVersion");
        FileCache.getInstance(PathUtil.getHostVersionFilePath(env)).write(v);
    }
    /**
     * Remove dummy version file from localspool
     * @param env ExecutionEnv
     * @throws java.lang.Exception - if cleanup fails
     */
    public void clearSystemVersion(ExecutionEnv env) throws Exception {


        if (!PathUtil.getHostVersionFilePath(env).delete()) {
            throw new GrmException("Failed to remove file: "+PathUtil.getHostVersionFilePath(env).getAbsolutePath());
        }
        if (localspool) {
            Platform.getPlatform().removeDir(env.getLocalSpoolDir(), true);
            localspool=false;
        }

    }
}
