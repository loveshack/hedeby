/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import junit.framework.TestCase;

/**
 *
 */
public class NumberParserTest extends TestCase {
    
    public NumberParserTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testFormatDouble() throws Exception {
        
        assertEquals("0", NumberParser.format(0.0));
        assertEquals("1G", NumberParser.format(NumberParser.BINARY_GIGA * 1.0));
        assertEquals("1M", NumberParser.format(NumberParser.BINARY_MEGA * 1.0));
        assertEquals("1K", NumberParser.format(NumberParser.BINARY_KILO * 1.0));

        assertEquals("-1K", NumberParser.format(NumberParser.BINARY_KILO * -1.0));
        
        assertEquals("1000", NumberParser.format(NumberParser.DECIMAL_KILO * 1.0));
        assertEquals("1000000", NumberParser.format(NumberParser.DECIMAL_MEGA * 1.0));
        assertEquals("1000000000", NumberParser.format(NumberParser.DECIMAL_GIGA * 1.0));
        
        assertEquals("0.0000000001", NumberParser.format(1E-10));
        assertEquals(Double.toString(Double.POSITIVE_INFINITY), NumberParser.format(Double.POSITIVE_INFINITY));
        assertEquals(Double.toString(Double.NEGATIVE_INFINITY), NumberParser.format(Double.NEGATIVE_INFINITY));
        assertEquals(Double.toString(Double.NaN), NumberParser.format(Double.NaN));
    }
    
    public void testFormatLong() throws Exception {

        assertEquals("0", NumberParser.format(0L));
        assertEquals("4G", NumberParser.format(NumberParser.BINARY_GIGA * 4L));
        assertEquals("1G", NumberParser.format(NumberParser.BINARY_GIGA * 1L));
        assertEquals("1M", NumberParser.format(NumberParser.BINARY_MEGA * 1L));
        assertEquals("1K", NumberParser.format(NumberParser.BINARY_KILO * 1L));

        assertEquals("-1K", NumberParser.format(NumberParser.BINARY_KILO * -1L));
        
        assertEquals("1000", NumberParser.format(NumberParser.DECIMAL_KILO * 1L));
        assertEquals("1000000", NumberParser.format(NumberParser.DECIMAL_MEGA * 1L));
        assertEquals("1000000000", NumberParser.format(NumberParser.DECIMAL_GIGA * 1L));
    }
    public void testFormatInt() throws Exception {

        assertEquals("0", NumberParser.format(0));
        assertEquals("1G", NumberParser.format(NumberParser.BINARY_GIGA * 1));
        assertEquals("1M", NumberParser.format(NumberParser.BINARY_MEGA * 1));
        assertEquals("1K", NumberParser.format(NumberParser.BINARY_KILO * 1));

        assertEquals("-1K", NumberParser.format(NumberParser.BINARY_KILO * -1));
        
        assertEquals("1000", NumberParser.format(NumberParser.DECIMAL_KILO * 1));
        assertEquals("1000000", NumberParser.format(NumberParser.DECIMAL_MEGA * 1));
        assertEquals("1000000000", NumberParser.format(NumberParser.DECIMAL_GIGA * 1));
    }

    public void testFormatShort() throws Exception {

        assertEquals("0", NumberParser.format((short)0));
        assertEquals("1K", NumberParser.format((short)(NumberParser.BINARY_KILO * 1)));
        assertEquals("-1K", NumberParser.format((short)(NumberParser.BINARY_KILO * -1)));
        assertEquals("1000", NumberParser.format((short)(NumberParser.DECIMAL_KILO * 1)));
    }
    
}
