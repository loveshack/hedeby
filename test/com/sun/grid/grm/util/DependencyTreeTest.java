/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 */
public class DependencyTreeTest extends TestCase {

    private final static Logger log = Logger.getLogger(DependencyTreeTest.class.getName());

    public DependencyTreeTest(String testName) {
        super(testName);
    }

    public void testDependecyTreeRegular() throws Exception {
        log.entering(DependencyTreeTest.class.getName(), "testDependecyTreeRegular");
        DependencyTree<Integer> dt = createDepTree();

        // check for correct output order
        int i = 1;
        for (Integer data : dt) {
            assertEquals("correct order", Integer.valueOf(i++), data);
        }
        log.exiting(DependencyTreeTest.class.getName(), "testDependecyTreeRegular");
    }

    public void testDependecyTreeMissingDependency() throws Exception {
        log.entering(DependencyTreeTest.class.getName(), "testDependecyTreeMissingDependency");
        DependencyTree<Integer> dt = createDepTree();

        // add an element that references an unknown other element
        dt.add("5", new Integer(5), Collections.<String>singletonList("7"));
        try {
            // the internal tests are done before an iterator is created
            dt.iterator();
        } catch (IllegalStateException ex) {
            assertTrue("must complain about element without data", ex.getLocalizedMessage().contains("has no data"));
        }
        log.exiting(DependencyTreeTest.class.getName(), "testDependecyTreeMissingDependency");
    }

    public void testDependecyTreeSimpleCycle() throws Exception {
        log.entering(DependencyTreeTest.class.getName(), "testDependecyTreeSimpleCycle");
        DependencyTree<Integer> dt = createDepTree();

        // now add a simple cycle (self referencing element)
        dt.add("5", new Integer(5), Collections.<String>singletonList("5"));
        try {
            // the internal tests are done before an iterator is returned
            dt.iterator();
        } catch (IllegalStateException ex) {
            assertTrue("must complain about simple cycle", ex.getLocalizedMessage().contains("cycle"));
        }
        log.exiting(DependencyTreeTest.class.getName(), "testDependecyTreeSimpleCycle");
    }

    public void testDependecyTreeComplexCycle() throws Exception {
        log.entering(DependencyTreeTest.class.getName(), "testDependecyTreeComplexCycle");
        DependencyTree<Integer> dt = createDepTree();

        // now add a complex cycle
        dt.add("5", 5, Collections.<String>singletonList("6"));
        dt.add("6", 6, Collections.<String>singletonList("7"));
        dt.add("7", 7, Collections.<String>singletonList("5"));
        try {
            // the internal tests are done before an iterator is returned
            dt.iterator();
        } catch (IllegalStateException ex) {
            assertTrue("must complain about complex cycle", ex.getLocalizedMessage().contains("cycle"));
        }
        log.exiting(DependencyTreeTest.class.getName(), "testDependecyTreeComplexCycle");
    }

    public void testDependecyTreeEmptyTree() throws Exception {
        log.entering(DependencyTreeTest.class.getName(), "testDependecyTreeEmptyTree");
        DependencyTree<Integer> dt = new DependencyTree<Integer>();

        // should throw no exception and have no elements
        Iterator<Integer> iter = dt.iterator();
        assertFalse("No elements to iterate over", iter.hasNext());

        log.exiting(DependencyTreeTest.class.getName(), "testDependecyTreeEmptyTree");
    }

    /**
     * create the following dependency tree:
     * 
     * <pre>
     *  1 <- 2 <- 3 <- 4
     *   \------------/
     * </pre>
     * @return the dependecy tree
     */
    private DependencyTree<Integer> createDepTree() {
        DependencyTree<Integer> dt = new DependencyTree<Integer>();

        List<String> l = new ArrayList<String>(2);
        l.add("1");
        l.add("3");
        dt.add("2", 2, Collections.<String>singletonList("1"));
        dt.add("4", 4, l);
        dt.add("1", 1, Collections.<String>emptyList());
        dt.add("3", 3, Collections.<String>singletonList("2"));
        return dt;
    }
}
