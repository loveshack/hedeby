/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import com.sun.grid.grm.bootstrap.ComponentInfo;
import javax.management.remote.JMXServiceURL;
import junit.framework.*;

/**
 *
 */
public class HostAndPortTest extends TestCase {
    
    public HostAndPortTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
    }

    protected void tearDown() throws Exception {
    }
    
    
    public void testUnresolvedJMXServiceURL() throws Exception {
        
        JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://test");
        
        HostAndPort hp = HostAndPort.newInstance(url);
        assertEquals("host must be test", "test", hp.getHost().getHostname());
        assertEquals("port must be 0", 0, hp.getPort());
        
        url =  new JMXServiceURL("service:jmx:rmi:///jndi/rmi://test:10");        
        hp = HostAndPort.newInstance(url);
        assertEquals("host must be test", "test", hp.getHost().getHostname());
        assertEquals("port must be 10", 10, hp.getPort());
        
        url =  new JMXServiceURL("service:jmx:rmi:///jndi/rmi://test:10/blubber");        
        hp = HostAndPort.newInstance(url);
        assertEquals("host must be test", "test", hp.getHost().getHostname());
        assertEquals("port must be 10", 10, hp.getPort());
        
        // Test a URL without a hostname
        url =  new JMXServiceURL("service:jmx:rmi:///jndi/rmi://:10");        
        hp = HostAndPort.newInstance(url);
        assertNull("host must be null", hp.getHost());
        assertEquals("port must be 10", 10, hp.getPort());
        
        // Test a URL with invalid port
        try {
            url =  new JMXServiceURL("service:jmx:rmi:///jndi/rmi://test:blabber");
            hp = HostAndPort.newInstance(url);
            fail("newInstanceUnresolvedInstance did not thow NumberFormatException");
        } catch(NumberFormatException ex) {
            // OK, we got the wanted exception
        }
        
        // Test a compliant url
        url =  new JMXServiceURL("service:jmx:test://test");        
        hp = HostAndPort.newInstance(url);
        assertEquals("host must be test", "test", hp.getHost().getHostname());
        assertEquals("port must be 0", 0, hp.getPort());
        
        url =  new JMXServiceURL("service:jmx:test://test:100");        
        hp = HostAndPort.newInstance(url);
        assertEquals("host must be test", "test", hp.getHost().getHostname());
        assertEquals("port must be 100", 100, hp.getPort());

        url =  new JMXServiceURL("service:jmx:test://test:100/blub");        
        hp = HostAndPort.newInstance(url);
        assertEquals("host must be test", "test", hp.getHost().getHostname());
        assertEquals("port must be 100", 100, hp.getPort());
        
    }

}
