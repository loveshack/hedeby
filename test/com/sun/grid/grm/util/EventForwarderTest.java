/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import junit.framework.TestCase;

/**
 * Junit test for the event forwarder
 */
public class EventForwarderTest extends TestCase {

    /**
     * Create a new instance of EventForwarderTest
     * @param testName name of the test
     */
    public EventForwarderTest(String testName) {
        super(testName);
    }

    /**
     * Test the synchron event delivery of the EventForwarder.
     * In addition the remove method is tested.
     * 
     * @throws java.lang.Exception
     */
    public void testSynchronAndRemove() throws Exception {

        EventForwarder<PropertyChangeListener> fw = EventForwarder.newSynchronInstance(PropertyChangeListener.class);

        MyPropertyChangedListener pcl = new MyPropertyChangedListener();

        fw.addListener(pcl);

        PropertyChangeEvent evt = new PropertyChangeEvent(this, "a", 1, 2);

        fw.getProxy().propertyChange(evt);

        assertTrue("the event must be forwarded to the listener", pcl.hasEvent(evt));
        
        fw.removeListener(pcl);
        pcl.clear();
        fw.getProxy().propertyChange(evt);
        assertFalse("The event must not be forwarded to the listner", pcl.hasEvent(evt));

    }

    /**
     * Test the asynchron event delivery of the EventForwarder.
     * @throws Exception 
     */
    public void testAsynchron() throws Exception {

        EventForwarder<PropertyChangeListener> fw = EventForwarder.newAsynchronInstance(PropertyChangeListener.class);

        MyPropertyChangedListener pcl = new MyPropertyChangedListener();
        PropertyChangeEvent evt = new PropertyChangeEvent(this, "a", 1, 2);

        fw.addListener(pcl);
        fw.getProxy().propertyChange(evt);
        assertTrue("the event must be forwarded to the listener", pcl.waitForEvent(evt, 1000));

    }

    private static class MyPropertyChangedListener implements PropertyChangeListener {

        private final Lock lock = new ReentrantLock();
        private final Condition cond = lock.newCondition();
        private PropertyChangeEvent evt;

        public void propertyChange(PropertyChangeEvent evt) {
            lock.lock();
            try {
                this.evt = evt;
                cond.signal();
            } finally {
                lock.unlock();
            }
        }

        public void clear() {
            lock.lock();
            try {
                this.evt = null;
            } finally {
                lock.unlock();
            }
        }

        public boolean hasEvent(PropertyChangeEvent evt) {
            lock.lock();
            try {
                return this.evt == evt;
            } finally {
                lock.unlock();
            }
        }

        public boolean waitForEvent(PropertyChangeEvent evt, long timeout) throws InterruptedException {
            long endTime = System.currentTimeMillis() + timeout;
            lock.lock();
            try {
                while (true) {
                    if (hasEvent(evt)) {
                        return true;
                    } else {
                        long rest = endTime - System.currentTimeMillis();
                        if (rest > 0) {
                            cond.await(rest, TimeUnit.MILLISECONDS);
                        } else {
                            return false;
                        }
                    }
                }

            } finally {
                lock.unlock();
            }
        }
    }
}
