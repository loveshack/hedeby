/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.prefs;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.channels.FileLock;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.prefs.Preferences;

import junit.framework.TestCase;

public class FilePreferencesTest extends TestCase {
    // we can not be sure if the currently open file descriptors remain open during the test.
    // thus we allow a tolerance of the file descriptor count. 
    private int FILE_DESCRIPTOR_TOLERANCE = 3;
    private File systemPrefsDir;
    private File userPrefsDir;
    private File tmpDir;
    private File tmpFile;
    private UnixOperatingSystemMXBeanWrapper fileDescriptorMBean;

    public FilePreferencesTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        tmpDir = TestUtil.getTmpDir(this);
        systemPrefsDir = new File(tmpDir, "systemPrefs");
        userPrefsDir = new File(tmpDir, "userPrefs");

        Field systemRootDirField = FilePreferences.class.getDeclaredField("systemRootDir");
        systemRootDirField.setAccessible(true);
        systemRootDirField.set(FilePreferences.class, systemPrefsDir);
        systemRootDirField.setAccessible(false);

        Field systemRootField = FilePreferences.class.getDeclaredField("systemRoot");
        systemRootField.setAccessible(true);
        systemRootField.set(FilePreferences.class, null);
        systemRootField.setAccessible(false);

        Field userRootDirField = FilePreferences.class.getDeclaredField("userRootDir");
        userRootDirField.setAccessible(true);
        userRootDirField.set(FilePreferences.class, userPrefsDir);
        userRootDirField.setAccessible(false);

        Field userRootField = FilePreferences.class.getDeclaredField("userRoot");
        userRootField.setAccessible(true);
        userRootField.set(FilePreferences.class, null);
        userRootField.setAccessible(false);

        //this file is used for a file lock
        tmpFile = createTmpFile();

        // try to get a UnixOperatingSystemMXBean. This bean can report open file handles
        OperatingSystemMXBean osMbean = ManagementFactory.getOperatingSystemMXBean();
        fileDescriptorMBean = new UnixOperatingSystemMXBeanWrapper(osMbean);
    }

    @Override
    protected void tearDown() throws Exception {
        Platform.getPlatform().removeDir(tmpDir, true);
        removeTmpFile();
    }

    private String getProperty(File file, String name) throws Exception {
        Properties props = new Properties();
        props.load(new FileInputStream(file));
        return props.getProperty(name);
    }

    /**
     * The issue can only be tested if the JVM really leaks file descriptors in 
     * case that files are not explicitly closed. This test ensures that this is 
     * indeed the case for the current JUnit JVM!
     * Warning, this test will loose 10 file descriptors if it works successfull
     * This is a wanted sacrifice to ensure that the problem is testable.
     * @throws java.lang.Exception 
     */
    public void testFileDescriptorLeakTestable() throws Exception {
        assertTrue("testFileDescriptorLeakage is not supported for this JVM! Please report your test platform to the mailing list!", (fileDescriptorMBean == null) || fileDescriptorMBean.isFileDescriptorMonitoringSupported());

        //check file descriptor amount in the beginning
        int beginCount = (int) fileDescriptorMBean.getOpenFileDescriptorCount();

        //try to loose some file descriptors
        for (int i = 0; i < 10; i++) {
            buggyWayToUseFileLocks(tmpFile);
        }
        System.gc();
        int diff = profileFileDescriptorCountDifference(10000);


        //check file descriptor amount after leaving 10 files unclosed
        int endCount = (int) fileDescriptorMBean.getOpenFileDescriptorCount();

        // if the jvm can leak fileDescriptors we will have a difference of more than 10 -FILE_DESCRIPTOR_TOLERANCE in the file descriptors count now
        // We leave this as a hard failing error because we are interested in the jvms/platforms on which this fails!
        assertTrue("The used JVM does not leak Filedescriptors as described in issue 601. " +
                " Please report your test platform to the mailinglist!", (endCount - beginCount) > (10 - FILE_DESCRIPTOR_TOLERANCE));

    }

    /**
     * tests if the File Descriptor Leak in File Preferences (Issue 601) is fixed
     * @throws java.lang.Exception 
     */
    public void testFileDescriptorLeakage() throws Exception {
        assertTrue("testFileDescriptorLeakage is not supported for this JVM! Please report your test platform to the mailing list!", (fileDescriptorMBean == null) || fileDescriptorMBean.isFileDescriptorMonitoringSupported());

        Preferences prefs = FilePreferences.getUserRoot();
        // do not use the original timer for the test but a faster one
        FilePreferences.syncTimer.cancel();
        FilePreferences.syncTimer = new Timer(true);

        //the timer task is defined as in FilePreferences
        TimerTask tt = new TimerTask() {

            public void run() {
                FilePreferences.syncWorld();
            }
        };

        // test first for 10 secs if there is no leakage before timer start up 
        int diff = profileFileDescriptorCountDifference(10000);
        assertTrue("Unexpected FileDescriptor change: Difference for testInterval:" + diff, Math.abs(diff) < FILE_DESCRIPTOR_TOLERANCE);

        // now switch on the fast Timer that does a syncWorld every 300 ms
        FilePreferences.syncTimer.schedule(tt, 0, 300);

        //test for 10 secs if there is now a leackage of file descriptors 
        diff = profileFileDescriptorCountDifference(10000);
        assertTrue("Unexpected FileDescriptor change with enabled syncTimer: Difference for testInterval:" + diff, Math.abs(diff) < FILE_DESCRIPTOR_TOLERANCE);

        // switch off the fast timer again
        FilePreferences.syncTimer.cancel();

    }

    /**
     * This method calulates the difference of the file descriptor count for a defined
     * time interval 
     * @param sleepTime time interval to sleep
     * @return difference in the file descriptor count
     */
    private int profileFileDescriptorCountDifference(int sleepTime) throws Exception {
        int beginCount = (int) fileDescriptorMBean.getOpenFileDescriptorCount();
        //System.out.println("beginCount:" + beginCount);

        try {
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            fail("The  sleep in profileFileDescriptorCountDifference should not be interrupted!");
        }
        int endCount = (int) fileDescriptorMBean.getOpenFileDescriptorCount();
        //System.out.println("endCount:" + endCount + "\n\n");

        return endCount - beginCount;
    }

    /**
     * creates a tmp file to test file descriptor leakage while locking
     * @return tmpFile
     */
    private File createTmpFile() {
        // this call ensures that userPrefsDir is created
        Preferences prefs = FilePreferences.getUserRoot();
        File file = new File(userPrefsDir + "tmpFile");

        if (!file.exists()) {
            try {
                assertTrue("Error could not create tmpFile: " + file.getAbsolutePath(), file.createNewFile());
            } catch (IOException e) {
                fail("Could not create file:" + file.getAbsolutePath());
            }
        }
        return file;
    }

    /**
     * Removes a previously created tmpFile
     * @return
     */
    private void removeTmpFile() {
        if (tmpFile.exists()) {
            assertTrue("Error could not delete tmpFile: " + tmpFile.getAbsolutePath(), tmpFile.delete());
        }
    }

    /**
     * This is code that can lead to lost file descriptors
     * @param tmpFile
     */
    private void buggyWayToUseFileLocks(File tmpFile) {
        //create a tmpfile and work with it but does not close it. This is the way 
        //we created a file descriptor leak described for 601

        // this set of commands lead to the file descriptor leak in 601 (unclosed raFile)
        RandomAccessFile raFile = null;
        try {
            raFile = new RandomAccessFile(tmpFile.getAbsolutePath(), "rw");
        } catch (IOException e) {
            fail("Could not rw Open random access file :" + tmpFile.getAbsolutePath());
        }
        FileLock lock = null;
        try {
            lock = raFile.getChannel().lock();
        } catch (IOException e) {
            fail("Could not get lock on file :" + tmpFile.getAbsolutePath());
        }
        try {
            lock.release();
        } catch (IOException e) {
            fail("Could not release lock on file :" + tmpFile.getAbsolutePath());
        }
    //here is the critical part The file is *NOT* closed because we want to see if the jvm leaks a file descriptor in this case!
    //we just forget about any reference to the file as done in the buggy code described in 601

    }

    public void testUserPut() throws Exception {
        Preferences prefs = FilePreferences.getUserRoot();
        String key = "blub";
        String value = "blab";

        prefs.put(key, value);
        prefs.flush();

        File file = new File(userPrefsDir, "prefs.properties");
        assertTrue("prefs.properties must exists", new File(userPrefsDir, "prefs.properties").exists());

        assertEquals("expect value " + value + " for property " + key, value, getProperty(file, key));
    }

    public void testUserPutInt() throws Exception {
        Preferences prefs = FilePreferences.getUserRoot();
        String key = "blub";
        int value = -1000;

        prefs.putInt(key, value);
        prefs.flush();

        File file = new File(userPrefsDir, "prefs.properties");
        assertTrue("prefs.properties must exists", new File(userPrefsDir, "prefs.properties").exists());

        assertEquals("expect value " + value + " for property " + key, value, Integer.parseInt(getProperty(file, key)));
    }

    public void testNewNode() throws Exception {

        Preferences prefs = FilePreferences.getUserRoot();

        Preferences child = prefs.node("c1");

        child.put("key", "value");
        child.flush();

        File dir = new File(userPrefsDir, "c1");

        assertTrue("node dir must exist", dir.exists());
        assertTrue("node dir must be a directry", dir.isDirectory());

        String[] childs = prefs.childrenNames();
        assertEquals("user prefs must have one child", 1, childs.length);
        assertEquals("child name must be c1", "c1", childs[0]);

        child.removeNode();
        child.flush();

        assertFalse("node dir must not exist", dir.exists());
    }

    public void testStrangeNodeName() throws Exception {

        Preferences prefs = FilePreferences.getUserRoot();

        Preferences child1 = prefs.node("c1");

        Preferences child = child1.node("c\\._#");
        child.put("key", "value");
        child.flush();

        File child1Dir = new File(userPrefsDir, "c1");

        File childDir = new File(child1Dir, "c#005c#002e#005f#0023");

        assertTrue("node dir must exist", childDir.exists());
        assertTrue("node dir must be a directory", childDir.isDirectory());

        child1.removeNode();
        child1.flush();


        assertFalse("node dir must not exist", child1Dir.exists());

    }

    /**
     * In JDK 1.5 there is an OperatingSystemMXBean that may implement the 
     * vendor specific interface com.sun.management.UnixOperatingSystemMXBean
     * This interface is needed to get hold on the open file descriptor count
     * The class that ensures that the test will not fail because of vendor specific
     * code
     */
    class UnixOperatingSystemMXBeanWrapper {
        // Generic type <?> is needed to suppress warnings in Java 1.6
        // see http://java.sun.com/javase/6/docs/technotes/guides/reflection/enhancements.html
        private Class<?> UnixOperatingSystemMXBeanClass;
        private OperatingSystemMXBean osMXBean;
        
        /**
         * Takes an OperatingSystemMXBean
         * @param osMXBean
         */
        UnixOperatingSystemMXBeanWrapper(OperatingSystemMXBean osMXBean) {
            this.osMXBean = osMXBean;
            //search all interfaces of the OperatingSystemMXBean object if it implements com.sun.management.UnixOperatingSystemMXBean
            Class[] allInterfaces = osMXBean.getClass().getInterfaces();
            for (Class curInterface : allInterfaces) {
                if (curInterface.getCanonicalName().equals("com.sun.management.UnixOperatingSystemMXBean")) {
                    UnixOperatingSystemMXBeanClass = curInterface;
                    break; 
                }
            }
        }

        /**
         * @return maxFileDescriptorCount or -1 if not supported
         * @throws java.lang.Exception
         */
        long getMaxFileDescriptorCount() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
            if (!isFileDescriptorMonitoringSupported()) {
                return -1;
            }
            Method getMaxFileDescriptorCountMethod = UnixOperatingSystemMXBeanClass.getDeclaredMethod("getMaxFileDescriptorCount");
            return (Long) getMaxFileDescriptorCountMethod.invoke(osMXBean);
        }

        /**
         * @return openFileDescriptorCount or -1 if not supported
         * @throws java.lang.Exception
         */
        long getOpenFileDescriptorCount() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
            if (!isFileDescriptorMonitoringSupported()) {
                return -1;
            }
            Method getOpenFileDescriptorCountMethod = UnixOperatingSystemMXBeanClass.getDeclaredMethod("getOpenFileDescriptorCount");
            return (Long) getOpenFileDescriptorCountMethod.invoke(osMXBean);
        }

        /**
         * @return true if UnixOperatingSystemMXBean is supported
         * @throws java.lang.Exception
         */
        boolean isFileDescriptorMonitoringSupported() {
            return (UnixOperatingSystemMXBeanClass != null);
        }
    }
}
