
/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import java.net.*;
import java.util.Hashtable;

/**
 * A mockuop for the InetAddressWrapper class.
 * 
 * <p>This class works as mockup for the InetAdress functionality wrapped in the 
 * InetAdressWrapper super class. This allows to simulate and test strange 
 * network problems related to InetAdress (see Issue 508#).</p>
 * 
 * <p>The mockup can be used to get artificial return values from set of wrapped
 * InetAddress methods. It can also be used to to provoke Exceptions these 
 * methods may throw.</p>
 * 
 * <p>By default none of the InetAdressWrapper behaviors are changed. The
 * mockup behaviour has to configured with the set of setter / getter 
 * methods. These methods correspond to the values an InetAddressWrapper object
 * may return and to Exceptions it may throw.</p>
 */
public class InetAddressMockup extends InetAddressWrapper {
 
    private InetAddress getLocalHostRetVal;
    private UnknownHostException getLocalHostUnknownHostException;
    
    private Hashtable<String,InetAddress> getByNameRetValTable= new Hashtable<String, InetAddress>();
    private Hashtable<String, UnknownHostException> getByNameUnknownHostExceptionTable = new  Hashtable<String, UnknownHostException>();
 
    /**
     * a proxy/mockup method 
     * it can be used to return any InetAddress instead of the local host
     * or throw a previously defined UnknownHostException 
     * If nothing is specified it bahaves like the super method
     * @return InetAddress of the localhost if no mockup behavior is specified. 
     * A mockup return value can be specified with setGetLocalHostRetVal(...)
     * @throws java.net.UnknownHostException if no mockup behavior is specified.
     * A mockup exception can be specified with setGetLocalHostUnknownHostException(...) 
     * @see  java.net.InetAddress
     */
    @Override
    public InetAddress getLocalHost() throws UnknownHostException {
        if (getLocalHostUnknownHostException != null) {
            throw getLocalHostUnknownHostException;
        }
        if (getLocalHostRetVal != null) {
            return getLocalHostRetVal;
        } else {
            return super.getLocalHost();
        }
    }
    
    /**
     * This is a proxy/mockup method 
     * it can be used to return any InetAddress instead of the local host
     * or throw a previously defined UnknownHostException 
     * If nothing is specified it bahaves like the super method
     * 
     * @param host specifies a host name of the InetAddress to be returned
     * @return InetAddress of host if no mockup behavior is specified. 
     * A mockup return value can be defined with putGetByNameRetVal(...)
     * @throws java.net.UnknownHostException if no mockup behavior is specified
     *  A mockup exception can be specified with setGetLocalHostUnknownHostException(...) 
     * @see  java.net.InetAddress
     */
    @Override
    public InetAddress getByName(String host) throws UnknownHostException {
        UnknownHostException uhe = getByNameUnknownHostExceptionTable.get(host);
        if (uhe != null) {
            throw uhe;
        }
        if (getByNameRetValTable != null) {
            InetAddress ia = getByNameRetValTable.get(host);
            if (ia != null) {
                return ia;
            }
        }
        return super.getByName(host);
    }

    /**
     * Setter for a return value mockup of getLocalHost(...)
     * @param aGetLocalHostRetVal a mockup InetAddress to be returned from call of getLocalHost(...)
     */
    public void setGetLocalHostRetVal(InetAddress aGetLocalHostRetVal) {
        getLocalHostRetVal = aGetLocalHostRetVal;
    }
  
   /**
    * Setter for a UnknownHostException mockup of getLocalHost(...)
    * @param exception a UnknownHostException to be thrown if getLocalHost(...) is called 
    */
    public void setGetLocalHostUnknownHostException(UnknownHostException exception) {
        getLocalHostUnknownHostException = exception;
    }

    /**
     * Putter for a return value mockup of (...). 
     * here a host, inetAddress pair has to be passed as parameter pair
     * @param host defines the host for which the mockup return value will be returned 
     * from call of getByNameRetVal(...)
     * @param inetAddress defineds the mockup return value that will be returned 
     * from call of getByNameRetVal(...)
     */
    public void putGetByNameRetVal(String host, InetAddress inetAddress) {
        getByNameRetValTable.put(host,inetAddress);
    }

    
    /**
     * Putter for a Exception mockup of getByNameRetVal(...). 
     * here a host, UnknownHostException pair has to be passed as parameter pair
     * @param host defines the host for which the mockup exception will be thrown
     * if getByNameRetVal(...) is called 
     * @param exception defineds the mockup exception that will be thrown
     * if getByNameRetVal(...) is called 
     */
    public void putGetByNameUnknownHostException(String host, UnknownHostException exception) {
        getByNameUnknownHostExceptionTable.put(host, exception);
    }
    
    /**
     * removes a return value mockup for the method getByNameRetVal(...)
     * @param host defines the host for which the mockup return value will be removed
     */
    public void removeGetByNameRetVal(String host) {
        getByNameRetValTable.remove(host);
    }

    /**
     * removes a exception mockup for the method getByNameRetVal(...)
     * @param host defines the host for which the mockup exception will be removed
     */
    public void removeGetByNameUnknownHostException(String host) {
        getByNameUnknownHostExceptionTable.remove(host);
    }
    
}
