/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.util;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.PathUtil;
import junit.framework.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  Tests the Platform class.
 *
 */
public class PlatformTest extends TestCase {
    
    private final static Logger log = Logger.getLogger(PlatformTest.class.getName());
    
    Platform platform;
    File tmpDir;
    
    public PlatformTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        platform = Platform.getPlatform();
        tmpDir = TestUtil.getTmpDir(this);
        tmpDir.mkdirs();
    }
    
    @Override
    protected void tearDown() throws Exception {
        platform.removeDir(tmpDir, true);
    }
    
    public static Test suite() {
        TestSuite suite = new TestSuite(PlatformTest.class);
        
        return suite;
    }
    
    /**
     * Test of getPlatform method, of class com.sun.grid.grm.executor.impl.Platform.
     */
    public void testGetPlatform() {
        log.entering("PlatformTest", "testGetPlatform");
        boolean expResult = true;
        Platform result = Platform.getPlatform();
        assertEquals(expResult, (result!=null));
        log.exiting("PlatformTest", "testGetPlatform");
    }
    
    
    
    /**
     * Test of makeExecutable method, of class com.sun.grid.grm.executor.impl.Platform.
     */
    /*
    public void testMakeExecutable() throws Exception {
        log.log(Level.FINE,"makeExecutable");
     
        File file = null;
        Platform instance = new Platform();
     
        instance.makeExecutable(file);
     
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
    
    
    /**
     * Test of execAs method, of class com.sun.grid.grm.executor.impl.Platform.
     */
    public void testExecAs() throws Exception {
        log.log(Level.FINE,"execAs");
        String user = System.getProperty("user.name");
        String command = null;
        String[] env = null;
        
        if (Platform.isWindowsOs()) {
            command = "echo %AVAR%";
            env = new String [] {
                "AVAR=blubber"
            };
        } else {
            command = "echo $AVAR";
            env = new String [] {
                "AVAR=blubber"
            };
        }
        File dir = null;
        List<String> stdout = new LinkedList<String>();
        List<String> stderr = new LinkedList<String>();
        
        int expResult = 0;
        int result = platform.execAs(user, command, env, dir, stdout, stderr, 0);
        log.log(Level.FINE,"out\n" + stdout.toString());
        log.log(Level.FINE,"stderr\n" +  stderr.toString());
        
        assertEquals(expResult, result);
        assertEquals("blubber", stdout.get(0));
        assertEquals(0, stderr.size());
        
    }

     /**
     * Test of execAs method, of class com.sun.grid.grm.executor.impl.Platform.
     */
    public void testIssue622() throws Exception {
        log.log(Level.FINE,"execAs");
        String user = System.getProperty("user.name");
        String command = null;
        String[] env = null;

        if (Platform.isWindowsOs()) {
            command = "echo %AVAR%";
            env = new String [] {
                "AVAR=blubber\""
            };
        } else {
            command = "echo $AVAR";
            env = new String [] {
                "AVAR=blubber\""
            };
        }
        File dir = null;
        List<String> stdout = new LinkedList<String>();
        List<String> stderr = new LinkedList<String>();

        int expResult = 0;
        int result = platform.execAs(user, command, env, dir, stdout, stderr, 0);
        log.log(Level.FINE,"out\n" + stdout.toString());
        log.log(Level.FINE,"stderr\n" +  stderr.toString());

        assertEquals(expResult, result);
        assertEquals("blubber\"", stdout.get(0));
        assertEquals(0, stderr.size());

    }
    
    /**
     * Test of exec method, of class com.sun.grid.grm.executor.impl.Platform.
     */
    public void testExec() throws Exception {
        log.log(Level.FINE,"exec");
        String command = null;
        String[] env = null;
        File dir = null;
        List<String> stdout = new LinkedList<String>();
        List<String> stderr = new LinkedList<String>();
        
        if (Platform.isWindowsOs()) {
            command = "echo blubber";
        } else {
            command = "/bin/echo blubber";
        }
        
        int result = platform.exec(command, env, dir, stdout, stderr, 0);
        log.log(Level.FINE,"out\n" + stdout.toString());
        log.log(Level.FINE,"stderr\n" +  stderr.toString());

        String resultStr = stdout.get(0).toString();
        assertEquals(0, result);
        assertEquals("blubber", resultStr);
        assertEquals(0, stderr.size());
    }
    
    /**
     * Test of chown method, of class com.sun.grid.grm.executor.impl.Platform.
     */
    public void testChown() throws Exception {
        log.log(Level.FINE,"chown");
        String owner = System.getProperty("user.name");
        boolean recursive = false;
        Platform instance = Platform.getPlatform();                
     
        File testFile = new File(tmpDir, "testChown.txt");
        testFile.deleteOnExit();
        testFile.createNewFile();

        instance.chown(testFile, owner, recursive);
        // Check somehow if chown succeeded
    }

    public void testCopy() throws Exception {
        log.log(Level.FINE,"copy");
        
        File file = new File(tmpDir, "testCopySource.txt");
        File targetFile = new File(tmpDir,"testCopyTarget.txt");
        boolean recursive = false;
        file.createNewFile();
        targetFile.deleteOnExit();
        platform.copy(file, targetFile, recursive);
        assertTrue(targetFile.exists());
        // Test copying
        // Test both types of copy!
    }
    
    public void testChmod() throws Exception {
        log.log(Level.FINE,"chmod");
        
        if (!Platform.isWindowsOs()) {
            File testFile = new File(tmpDir, "testChmod.txt");
            testFile.deleteOnExit();
            testFile.createNewFile();

            platform.chmod(testFile, "u-w");
            assertFalse(testFile.canWrite());
        }
    }
    
    /**
     * Test of link method, of class com.sun.grid.grm.executor.impl.Platform.
     */    
    /*    
    public void testLink() throws Exception {
        log.log(Level.FINE,"link");
     
        File file = null;
        File link = null;
        Platform instance = new Platform();
     
        instance.link(file, link);
     
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
    
    /**
     * Test of findFile method, of class com.sun.grid.grm.executor.impl.Platform.
     */
    public void testFindFile() {
        log.log(Level.FINE,"findFile");
     
        File dir = new File(".");
        String filename = "PlatformTest.java";
     
        List<File> expResult = new ArrayList<File>();
        expResult.add(new File("." + File.separatorChar + "test" + 
                File.separatorChar + "com" + File.separatorChar + "sun" + 
                File.separatorChar + "grid" + File.separatorChar + "grm" + 
                File.separatorChar + "util" + File.separatorChar + 
                "PlatformTest.java"));
        List<File> result = Platform.findFile(dir, filename);
        assertEquals(expResult, result);
    }

    public void testSplitCommand() throws Exception {
        log.log(Level.FINE,"splitCommand");
        
        String[] splitted;
        String   command = "TestApp TestArg1 /TestArg2 -TestArg3";
        
        splitted = Platform.getPlatform().splitCommand(command);
        /*
         * assertEquals(...)
         */
        assert(splitted[0].equals("TestApp"));
        assert(splitted[1].equals("TestArg1"));
        assert(splitted[2].equals("/TestArg2"));
        assert(splitted[3].equals("-TestArg3"));
    }
    
/*
 * fork JVM, let JVM write PID-File, kill JVM via PID, wait for exception
 */
    public void testGetPid() throws Exception {
        log.log(Level.FINE,"getPid");
        int Pid = Platform.getPlatform().getPid();
    }

/*
 * compare ID with UserName
 * system.GetProperty(user.name)
 */
    public void testIsSuperUser() throws Exception {
        log.log(Level.FINE,"isSuperUser");
        boolean bIsSupi = Platform.getPlatform().isSuperUser();
    }

     /**
     * Test of execute method, of class com.sun.grid.grm.executor.impl.Platform.
     */
    public void testExecute() throws Exception {
        log.log(Level.FINE,"execute");
        String tmpName="tmp."+Calendar.getInstance().getTimeInMillis();
        File tmp = new File(PathUtil.getTmpDir(),tmpName);

        String command = null;
        String[] env = null;
        File dir = PathUtil.getTmpDir();
        List<String> stdout = new LinkedList<String>();
        List<String> stderr = new LinkedList<String>();
        StringBuilder script = new StringBuilder();
        script.append("sleep 5");
        script.append("\n");
        script.append("echo blubber");
        script.append("\n");
        script.append("exit 3");
        script.append("\n");
        FileUtil.write(script.toString(), tmp);
        if (!tmp.exists()) {
            throw new Exception("Testing script not written");
        }
        if (Platform.isWindowsOs()) {
            return;
        } else {
            command = "/bin/sh "+tmp.getAbsolutePath();
        }

        int result = platform.exec(command, env, dir, stdout, stderr, 20);
        log.log(Level.FINE,"out\n" + stdout.toString());
        log.log(Level.FINE,"stderr\n" +  stderr.toString());

        String resultStr = stdout.get(0).toString();
        assertEquals(3, result);
        assertEquals("blubber", resultStr);
        assertEquals(0, stderr.size());

        tmp.delete();
    }

    /**
     * Test of execute method, of class com.sun.grid.grm.executor.impl.Platform.
     * Test if InterruptedException is thrown on timeout of execution.
     */
    public void testExecuteTimeout() throws Exception {
        log.log(Level.FINE,"execute");
        String tmpName="tmp."+Calendar.getInstance().getTimeInMillis();
        File tmp = new File(PathUtil.getTmpDir(),tmpName);

        String command = null;
        String[] env = null;
        File dir = PathUtil.getTmpDir();
        List<String> stdout = new LinkedList<String>();
        List<String> stderr = new LinkedList<String>();
        StringBuilder script = new StringBuilder();
        script.append("sleep 10");
        script.append("\n");
        script.append("echo blubber");
        script.append("\n");
        script.append("exit 3");
        script.append("\n");
        FileUtil.write(script.toString(), tmp);
        if (!tmp.exists()) {
            throw new Exception("Testing script not written");
        }
        if (Platform.isWindowsOs()) {
            return;
        } else {
            command = "/bin/sh "+tmp.getAbsolutePath();
        }
        try{
        int result = platform.exec(command, env, dir, stdout, stderr, 5);
        } catch(InterruptedException ex) {
            //we got timeout, everything is ok
            tmp.delete();
            return;
        }
        
        tmp.delete();
        //failed
        assertTrue("Expected InterruptException on timeout not thrown", false);
    }
    
    public void testGetFileOwner() throws Exception {
        File file = new File("build");
        String owner = Platform.getPlatform().getFileOwner(file);
        assertEquals("Owner of " + file + " must be user.name", System.getProperty("user.name"), owner);
    }

    public void testGetFileOwnerOnNotReadableFile() throws Exception {
        File file = new File("/etc/shadow");
        if (file.exists()) {
            String owner = Platform.getPlatform().getFileOwner(file);
            assertEquals("Owner of " + file + " must be root", "root", owner);
        } else {
            log.warning("Can not run test testGetFileOwnerOnNotReadableFile, file " + file + " does not exist");
        }
    }
    
    public void testGetFileOwnerFileNotExist() throws Exception {
        File file = new File("build");
        int i = 0; 
        while(file.exists()) {
            file = new File("build" + i);
        }
        try {
            Platform.getPlatform().getFileOwner(file);
            fail("getFileOwner on non existing file must fail");
        } catch(IOException ex) {
            // fine
        }
    }
}
