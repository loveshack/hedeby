/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * HostnameTest.java
 * JUnit based test
 *
 * Created on 11. Juli 2006, 10:54
 */
package com.sun.grid.grm.util;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.LoggerHandlerMockup;

import junit.framework.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.logging.LogRecord;

/**
 */
public class HostnameTest extends TestCase {

    private final static Logger log = Logger.getLogger(HostnameTest.class.getName());
    private InetAddress localHost;

    public HostnameTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        /* ensure that no hostName is not cached...*/
        resetHostNameCache();
        localHost = InetAddress.getLocalHost();
    }

    public static Test suite() {
        return new TestSuite(HostnameTest.class);
    }

    /**
     * Test the Error Message problem with an unresolved local hostname (iz#508)
     * @throws NoSuchFieldException
     * @throws IllegalAccessException 
     */
    public void testLocalHostNameUnknown() throws NoSuchFieldException, IllegalAccessException {
        try {
            // install an InetAddressMockup that throws an UnknownHostException when getLocalHost is called
            InetAddressMockup mockup = new InetAddressMockup();
            UnknownHostException uhe = new UnknownHostException("This error message allone is not meaningful. Usually it states: \"host:host\"");
            mockup.setGetLocalHostUnknownHostException(uhe);
            installInetAddressMockup(mockup);
            try {
                //ensure that the object is not cached
                resetHostNameCache();
                // this call will cause an UnknownHostException
                Hostname.getLocalHost();
                fail("The expected error behavior was not provoked!");
            } catch (IllegalStateException ex) {
                String expectedMessagePrefix = I18NManager.formatMessage("Hostname.error.UnknownHost", Hostname.BUNDLE_NAME, "");
                String caughtMessage = ex.getLocalizedMessage();
                // compare if expected Message is a prefix of caught message
                assertTrue("No meaningful error message if local host is unknown: " + caughtMessage, caughtMessage.startsWith(expectedMessagePrefix));
            }
        } finally {
            unInstallInetAddressMockup();
        }
    }

    /**
     * Test the Error Message problem with an unresolved local hostname (iz#508)
     * @throws NoSuchFieldException
     * @throws IllegalAccessException 
     */
    public void testIsMatching() throws NoSuchFieldException, IllegalAccessException {
        Hostname localhost = Hostname.getLocalHost();
        String pattern = ".*";
        boolean match = localhost.matches(pattern);
        assertTrue("The hostname " + localhost.getHostname() + " should match " + pattern, match);

        pattern = localhost.getHostname();
        match = localhost.matches(pattern);
        assertTrue("The hostname " + localhost.getHostname() + " should match " + pattern, match);

        pattern = ":::SOME PATTERN:::";
        match = localhost.matches(pattern);
        assertFalse("The hostname " + localhost.getHostname() + " should  not match " + pattern, match);

    }

    /**
     * Test problems with the match method of Hostname. The method expects a Hostname or a regexpr.
     * This leads to problems tested below! 
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    public void testUnfixxedMatchProblemsTest() throws NoSuchFieldException, IllegalAccessException {
        Hostname localhost = Hostname.getLocalHost();

        //This test proves that the match method is illdesigned
        //The pattern matches the current name. 
        String pattern = "www.sun.com";
        Hostname curHost = Hostname.getInstance("wwwAsunBcom");
        boolean match = curHost.matches(pattern);

        //This assert needs to be changed once the source of error is fixed!
        assertTrue("The hostname " + curHost.getHostname() + " should  not match " + pattern, match);

        //install a mockup that sniffs for a specific error message
        Logger hostnameLogger = Logger.getLogger(Hostname.class.getName(), Hostname.BUNDLE_NAME);
        LoggerHandlerMockup mockup = new LoggerHandlerMockup(hostnameLogger);
        //the MockupHandler is installed to log all Warnings but it does not report them to the parent Loggers.
        mockup.installMockup().minLogLevel(Level.WARNING).useParentHandlers(false);

        try {

            pattern = ":::SOME PATTERN:::";
            match = localhost.matches(pattern);
            assertFalse("The hostname " + localhost.getHostname() + " should  not match " + pattern, match);
            StringBuilder warning = new StringBuilder();
            for (LogRecord logRec : mockup.getLogRecordList()) {
                warning.append(logRec.getMessage() + "\n");
            }

            //this assert needs to be changed once the source of error is fixed!
            assertFalse("No Warning should be logged when the match is tried: " + warning, "".equals(warning));
        } finally {
            mockup.unistallMockup();
        }
    }

    /**
     * Test the timeout behavior of the resolve function 
     * @throws NoSuchFieldException
     * @throws IllegalAccessException 
     */
    public void testResolveTimeouts() throws NoSuchFieldException, IllegalAccessException {
        try {
            String localHostName = localHost.getHostName();//take Hostname directly from Hostname Class
            // install an InetAddressMockup that throws an UnknownHostException when getLocalHost is called
            InetAddressMockup mockup = new InetAddressMockup();

            //place an Unknown host exception for the local host for call of InetAddress getByName(<host>) 
            UnknownHostException uhex = new UnknownHostException("This exception is used to test the resolve timout issue");
            mockup.putGetByNameUnknownHostException(localHostName, uhex);
            installInetAddressMockup(mockup);



            //Now the provoke a getByName() call in the resolve() function in a set of different circumstances

            //get the allready cached Hostname object
            Hostname localhost = Hostname.getLocalHost();
            //localhost.isResolved() will return true is using cached infomation.
            assertTrue("Unexpected internal getByName(localhost) call for Hostname object localhost within RESOLVE_TIMEOUT!", localhost.isResolved());


            //expire the LastResolveSuccessTimestamp (RESOLVE_TIMEOUT = 10 * 60 * 1000)
            manipulateHostnameLastResolveSuccessTimestamp(localhost, -(Hostname.RESOLVE_TIMEOUT + 1));
            //localhost.isResolved() will now call InetAddress getByName(<host>) and raise internally the UnknownHostException. Thus it returns false
            assertFalse("No internal getByName(localhost) call for Hostname object localhost after RESOLVE_TIMEOUT!", localhost.isResolved());


            //deactivate the uhe Exception for local host name to prove that it that resolve fails without an getByName() call, as the method now works
            mockup.removeGetByNameUnknownHostException(localHostName);
            //The resolve has previously failed. Now it should not try a new resolve until the RESOLVE_RETRY_TIMEOUT expired 
            assertFalse("unexpected internal getByName(localhost) call for Hostname object localhost within RESOLVE_RETRY_TIMEOUT!", localhost.isResolved());


            //reactivate the uhe Exception for local host name to show that resolve fails without an getByName() call
            mockup.putGetByNameUnknownHostException(localHostName, uhex);
            //expire the LastResolveSuccessTimestamp (RESOLVE_RETRY_TIMEOUT = 60 * 1000;)
            manipulateHostnamelastResolveTryTimestamp(localhost, -(Hostname.RESOLVE_RETRY_TIMEOUT + 1));
            //now it should try the resolve and run into the exception
            assertFalse("No internal getByName(localhost) call for Hostname object localhost after RESOLVE_RETRY_TIMEOUT!", localhost.isResolved());


        } finally {
            // uninstall the mockup for a clean system
            unInstallInetAddressMockup();
            // reset the cache to remove all instances of unresolved Hostname objects within the RESOLVE_RETRY_TIMEOUT window
            resetHostNameCache();
        }
    }

    /**
     * Issue (iz#508) describes a situation where the rename of the machine hostname leads to
     * an unknown host exception. This test checks the system behavior if this rename event has 
     * been done properly (new DNS entry)
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     * @throws UnknownHostException 
     */
    public void testValidLocalHostRename() throws NoSuchFieldException, IllegalAccessException, UnknownHostException {
        try {
            String oldLocalHostName = "oldlocalhostname";
            String localHostName = localHost.getHostName();
            //after this command the system is in a state where the hostname changed from "oldlocalhostname" to the current name;
            fakeLocalHostRenameHappened(oldLocalHostName);

            //in the first 10 minutes nothing will be noticed because of the LastResolveSuccessTimestamp
            Hostname instance = Hostname.getLocalHost();
            String expAddress = localHost.getHostAddress();
            // this is still cached
            String address = instance.getIpAddress();
            assertEquals(expAddress, address);
            // this is a final member
            String name = instance.getHostname();
            assertEquals(oldLocalHostName, name);

            //expire the LastResolveSuccessTimestamp (RESOLVE_TIMEOUT = 10 * 60 * 1000)
            manipulateHostnameLastResolveSuccessTimestamp(instance, -(Hostname.RESOLVE_TIMEOUT + 1));

            // now the resolve will be done and the problem arise! Ip was valid but it is 
            //refreshed by resolving with the invalid name and thus null
            address = instance.getIpAddress();
            assertTrue(address == null);
            //name is still old one
            name = instance.getHostname();
            assertEquals(oldLocalHostName, name);

            //but the localHost() should give a correct local host object!
            //However it does not in the current implementation the hostname of the 
            //previous host is hard coded an cached! Thus the expected Address will 
            //be null. 
            instance = Hostname.getLocalHost();
            expAddress = null;//localHost.getHostAddress(); // null as a change of the local host is not supportet
            address = instance.getIpAddress();
            assertEquals(expAddress, address);

            //this test is not working too as the name is now again not the localhostName but the oldLocalhostName
            name = instance.getHostname();
            assertEquals(oldLocalHostName, name);
        } finally {
            resetHostNameCache();
        }
    }

    /**
     * This test checks the behavior of Hostname objects if the address changes
     * as it is the case for dynamic DNS systems!
     * @throws NoSuchFieldException
     * @throws IllegalAccessException 
     */
    public void testValidAddressChange() throws NoSuchFieldException, IllegalAccessException {
        //after this command the system is in a state where the hostname changed from "oldlocalhostname" to the current name;
        try {
            Hostname myHost = Hostname.getLocalHost();
            String myHostName = myHost.getHostname();
            String myHostIp = myHost.getIpAddress();
            String otherHostName = "www.sun.com";
            Hostname otherHost = Hostname.getInstance(otherHostName);
            otherHostName = otherHost.getHostname(); // this might differ from the initial name it can be another canonical name or ip address
            String otherHostIp = otherHost.getIpAddress();



            //until the ip is the local host Ip 
            assertEquals(myHostIp, myHost.getIpAddress());

            //now change the hostname to name of the other host. This is equivalent to the case that
            //a hostnameObject of otherhost experience a change of the ip address of its machine!
            replaceHostNameString(myHost, otherHostName);
            stampHostnameLastResolveSuccessTimestamp(myHost);

            // Nothing should happen as this happened in the LastResolveSuccessTimestamp time window
            assertEquals(myHostIp, myHost.getIpAddress());
            assertEquals(myHostName, myHost.getHostname());


            //now expire the LastResolveSuccessTimestamp (RESOLVE_TIMEOUT = 10 * 60 * 1000)
            manipulateHostnameLastResolveSuccessTimestamp(myHost, -(Hostname.RESOLVE_TIMEOUT + 1));

            // The Ip should be now the Ip of the other host!
            assertEquals(otherHostIp, myHost.getIpAddress());
            assertEquals(otherHostName, myHost.getHostname());


        } finally {
            resetHostNameCache();
        }
    }

    /**
     * Test of getHostname method, of class com.sun.grid.grm.util.Hostname.
     */
    public void testGetHostname() {
        String expResult = localHost.getCanonicalHostName();
        Hostname instance;
        String result;

        instance = Hostname.getInstance(localHost.getHostName());
        result = instance.getHostname();
        assertEquals(expResult, result);

        instance = Hostname.getInstance(localHost.getCanonicalHostName());
        result = instance.getHostname();
        assertEquals(expResult, result);

        instance = Hostname.getInstance(localHost.getHostAddress());
        result = instance.getHostname();
        assertEquals(expResult, result);

        instance = Hostname.getInstance(localHost.getHostAddress());
        result = instance.getHostname();
        assertEquals(expResult, result);
    }

    /**
     * Test of getIpAddress method, of class com.sun.grid.grm.util.Hostname.
     */
    public void testGetIpAddress() {
        log.log(Level.FINE, "getIpAddress");

        String expResult = localHost.getHostAddress();
        Hostname instance;
        String result;

        instance = Hostname.getInstance(localHost.getHostName());
        result = instance.getIpAddress();
        assertEquals(expResult, result);

        instance = Hostname.getInstance(localHost.getCanonicalHostName());
        result = instance.getIpAddress();
        assertEquals(expResult, result);

        instance = Hostname.getInstance(localHost.getHostAddress());
        result = instance.getIpAddress();
        assertEquals(expResult, result);



        // now some Ip adress cases that should fail!

        // Ivalid
        instance = Hostname.getInstance(":::::not a valid host name::::");
        result = instance.getIpAddress();
        assertTrue(result == null);

        // name != localhost
        instance = Hostname.getInstance("www.sun.com");
        result = instance.getIpAddress();
        assertFalse(expResult.equals(result));



    }

    public void testLocalHost() {
        log.log(Level.FINE, "localHost");


        Hostname instance;
        String result;

        String localHostname;
        String localIPStr;


        // Hostname instance must return offical hostname, not loopback

        // get instance of "localhost"
        instance = Hostname.getInstance("localhost");
        localIPStr = instance.getIpAddress();
        // localIPStr result must not contain loopback network ip
        if (localIPStr.equals("127.0.0.1")) {
            fail("Hostname instance returned loopback interface ip for localhost");
        }

        // Hostname must not be localhost
        localHostname = instance.getHostname();
        if (localHostname.toLowerCase().equals("localhost")) {
            fail("Hostname instance returned hostname \"localhost\" for localhost");
        }

        // now check what happens when we resolve real localhostname
        instance = Hostname.getInstance(localHostname);
        result = instance.getHostname();
        if (!result.toLowerCase().equals(localHostname.toLowerCase())) {
            fail("The real local hostname does not resolve to same string");
        }

        // also using local host ip must return local hostname
        instance = Hostname.getInstance(localIPStr);
        result = instance.getHostname();
        if (!result.toLowerCase().equals(localHostname.toLowerCase())) {
            fail("The real local ip does not resolve to real local hostname string");
        }


        // Now check with different names for localhost
        instance = Hostname.getInstance("localHost");
        result = instance.getHostname();
        if (!result.toLowerCase().equals(localHostname.toLowerCase())) {
            fail("'localHost' does not resolve to real local host name");
        }

        instance = Hostname.getInstance("127.0.0.1");
        result = instance.getHostname();
        if (!result.toLowerCase().equals(localHostname.toLowerCase())) {
            fail("'127.0.0.1' does not resolve to real local host name");
        }


        // Now use a resolvable host, but not loopback

        instance = Hostname.getInstance("255.255.255.255");
        result = instance.getHostname();
        if (!result.equals("255.255.255.255")) {
            fail("'255.255.255.255' should be resolved to hostname '255.255.255.255'");
        }


    }

    /**
     * Test of toString method, of class com.sun.grid.grm.util.Hostname.
     */
    public void testToString() {
        log.log(Level.FINE, "toString");
        Hostname instance = Hostname.getInstance(localHost.getHostName());

        String expResult = localHost.getCanonicalHostName();
        String result = instance.toString();
        assertEquals(expResult, result);

        instance = Hostname.getInstance(localHost.getHostName());
        expResult = localHost.getCanonicalHostName();
        result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class com.sun.grid.grm.util.Hostname.
     * 
     * Tests are done for a set of 10 different hostname objects
     * a, b, c are equal hostname objects and resolvable (sun.com)
     * d, e, f are different and resolvable
     * g, h are equal and not resolvable (sun com) (the not resolvable state )
     * i, j are different and not resolvable 
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     * @throws UnknownHostException 
     */
    public void testEqualsAndHashCode() throws NoSuchFieldException, IllegalAccessException, UnknownHostException {
        log.log(Level.FINE, "equals");
        boolean expResult = true;
        boolean result;

        //Do not take localhost because there is a specific check for localhost equivalence 
        //in getInstance();



        InetAddress sun = InetAddress.getByName("www.sun.com");

        //all the same and resolvable
        resetHostNameCache();//this ensures to get different objects
        Hostname a = Hostname.getInstance(sun.getHostName());
        resetHostNameCache();
        Hostname b = Hostname.getInstance(sun.getCanonicalHostName());
        resetHostNameCache();
        Hostname c = Hostname.getInstance(sun.getHostAddress());

        //all different and resolvable
        resetHostNameCache();
        Hostname d = Hostname.getInstance("www.sunsource.net");
        resetHostNameCache();
        Hostname e = Hostname.getInstance("java.sun.com");
        resetHostNameCache();
        Hostname f = Hostname.getInstance("www.sun.de");

        //all the same and not resolvable
        resetHostNameCache();
        Hostname g = Hostname.getInstance(sun.getHostName());
        makeHostnameRecentlyUnresolved(g);
        replaceHostNameString(g, sun.getHostAddress());//host name is sun
        replaceCanonicalNameString(g, "");

        resetHostNameCache();
        Hostname h = Hostname.getInstance(sun.getHostName());
        makeHostnameRecentlyUnresolved(h);
        replaceHostNameString(h, "");
        replaceCanonicalNameString(h, sun.getHostName());//canonical name is ww.sun.com


        //all different and not resolvable
        resetHostNameCache();
        Hostname i = Hostname.getInstance(":invalidName1:"); //unresolvable by definition
        replaceCanonicalNameString(i, i.getHostname());

        resetHostNameCache();
        Hostname j = Hostname.getInstance(":invalidName2:");
        replaceCanonicalNameString(i, j.getHostname());

        Object[] allEqual = new Object[]{a, b, c, g, h};
        Object[] allUnEqual = new Object[]{d, e, f, i, j, "notHostnameButString", null};

        TestUtil.assertAllMutualEquals(allEqual, true); //all are www.sun.com
        TestUtil.assertAllMutualEquals(allUnEqual, false); //all are mutual different

        Object[] all = new Object[]{a, b, c, d, e, f, g, h, i, j, "notAHostname", null/*also interesting*/};
        TestUtil.assertAllObeyEqualsContract(all);
        TestUtil.assertAllObeyHashCodeContract(all);
    }

    public void testIsResolved() {
        log.log(Level.FINE, "isHostNameResolving");

        Hostname instance = Hostname.getInstance(localHost.getHostName());
        boolean result = true;
        assertEquals(result, instance.isResolved());


        instance = Hostname.getInstance(localHost.getHostName());
        result = true;
        assertEquals(result, instance.isResolved());
    }

    /**
     * Test of clone method, of class com.sun.grid.grm.util.Hostname.
     */
    public void testClone() {
        log.log(Level.FINE, "clone");

        Hostname instance = Hostname.getInstance(localHost.getHostName());
        Hostname result = instance.clone();
        assertEquals(instance, result);
        assertEquals(instance.getHostname(), result.getHostname());
        assertEquals(localHost.getCanonicalHostName(), result.getHostname());


        instance = Hostname.getInstance(localHost.getHostName());
        result = instance.clone();
        assertEquals(instance, result);
        assertEquals(instance.getHostname(), result.getHostname());
        assertEquals(localHost.getCanonicalHostName(), result.getHostname());
    }

    /**
     * replace the staticInetAddressMethods field in Hostname with a mockup that 
     * produce a UnknownHostException when getLocalHost is called
     */
    private void installInetAddressMockup(InetAddressMockup mockup) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field inetAddressWrpField = Hostname.class.getDeclaredField("inetAddressWrp");
        inetAddressWrpField.setAccessible(true);
        //a mockup to throw the unknown Host exception 
        inetAddressWrpField.set(Hostname.class, mockup);
        inetAddressWrpField.setAccessible(false);
    }

    /**
     * put the staticInetAddressMethods back in place by removing a previously 
     * installed mockup
     */
    private void unInstallInetAddressMockup() throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field inetAddressWrpField = Hostname.class.getDeclaredField("inetAddressWrp");
        inetAddressWrpField.setAccessible(true);
        inetAddressWrpField.set(Hostname.class, new InetAddressWrapper());
        inetAddressWrpField.setAccessible(false);
    }

    private InetAddress createInetAddressForHost(String hostName, int intAddress) throws NoSuchFieldException, IllegalAccessException, UnknownHostException {
        InetAddress newAddress = null;

        newAddress = InetAddress.getLocalHost(); // local host object because no direct constructor

        java.lang.reflect.Field addressField = InetAddress.class.getDeclaredField("address");
        addressField.setAccessible(true);
        addressField.setInt(newAddress, intAddress);
        addressField.setAccessible(false);

        java.lang.reflect.Field hostNameField = InetAddress.class.getDeclaredField("hostName");
        hostNameField.setAccessible(true);
        hostNameField.set(newAddress, hostName);
        hostNameField.setAccessible(false);

        java.lang.reflect.Field canonicalHostNameField = InetAddress.class.getDeclaredField("canonicalHostName");
        canonicalHostNameField.setAccessible(true);
        canonicalHostNameField.set(newAddress, hostName);
        canonicalHostNameField.setAccessible(false);

        return newAddress;
    }

    private void manipulateHostnameLastResolveSuccessTimestamp(Hostname hostname, long timeOffset) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field lastResolveSuccessTimestampField = Hostname.class.getDeclaredField("lastResolveSuccessTimestamp");
        lastResolveSuccessTimestampField.setAccessible(true);
        long currentValue = lastResolveSuccessTimestampField.getLong(hostname);
        lastResolveSuccessTimestampField.setLong(hostname, currentValue + timeOffset);
        lastResolveSuccessTimestampField.setAccessible(false);
    }

    private void stampHostnameLastResolveSuccessTimestamp(Hostname hostname) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field lastResolveSuccessTimestampField = Hostname.class.getDeclaredField("lastResolveSuccessTimestamp");
        lastResolveSuccessTimestampField.setAccessible(true);
        lastResolveSuccessTimestampField.setLong(hostname, System.currentTimeMillis());
        lastResolveSuccessTimestampField.setAccessible(false);
    }

    private void stampHostnameLastResolveTryTimestampTimestamp(Hostname hostname) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field lastResolveTryTimestampField = Hostname.class.getDeclaredField("lastResolveTryTimestamp");
        lastResolveTryTimestampField.setAccessible(true);
        lastResolveTryTimestampField.setLong(hostname, System.currentTimeMillis());
        lastResolveTryTimestampField.setAccessible(false);
    }

    private void manipulateHostnamelastResolveTryTimestamp(Hostname hostname, long timeOffset) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field lastResolveTryTimestampField = Hostname.class.getDeclaredField("lastResolveTryTimestamp");
        lastResolveTryTimestampField.setAccessible(true);
        long currentValue = lastResolveTryTimestampField.getLong(hostname);
        lastResolveTryTimestampField.setLong(hostname, currentValue + timeOffset);
        lastResolveTryTimestampField.setAccessible(false);
    }

    private void makeHostnameRecentlyUnresolved(Hostname hostname) throws NoSuchFieldException, IllegalAccessException {
        this.replaceInetAddress(hostname, null);
        stampHostnameLastResolveTryTimestampTimestamp(hostname);
    }

    private void resetHostNameCache() throws NoSuchFieldException, IllegalAccessException {
        // this block resets the hostname cache 
        java.lang.reflect.Field cacheField = Hostname.class.getDeclaredField("cache");
        cacheField.setAccessible(true);
        HashMap cache = (HashMap) cacheField.get(Hostname.class);
        cache.clear();
        cacheField.setAccessible(false);

        // this block resets the separate localhost cache
        java.lang.reflect.Field localHostField = Hostname.class.getDeclaredField("localHost");
        localHostField.setAccessible(true);
        localHostField.set(Hostname.class, null);
        localHostField.setAccessible(false);
    }

    private void placeHostnameObjInCache(Hostname hostnameObj) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field cacheField = Hostname.class.getDeclaredField("cache");
        cacheField.setAccessible(true);
        @SuppressWarnings("unchecked")
        HashMap<String, Hostname> cache = (HashMap<String, Hostname>) cacheField.get(Hostname.class);
        cache.put(hostnameObj.getHostname(), hostnameObj);
        cacheField.setAccessible(false);
    }

    private void replaceInetAddress(Hostname hostname, InetAddress address) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field addressField = Hostname.class.getDeclaredField("address");
        addressField.setAccessible(true);
        addressField.set(hostname, address);
        addressField.setAccessible(false);
    }

    private void replaceHostNameString(Hostname hostname, String name) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field hostnameField = Hostname.class.getDeclaredField("hostname");
        hostnameField.setAccessible(true);
        hostnameField.set(hostname, name);
        hostnameField.setAccessible(false);
    }

    private void replaceCanonicalNameString(Hostname hostname, String canonicalName) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field canonicalNameField = Hostname.class.getDeclaredField("canonicalName");
        canonicalNameField.setAccessible(true);
        canonicalNameField.set(hostname, canonicalName);
        canonicalNameField.setAccessible(false);
    }

    private int getIntRepresentationOfInetAdress(InetAddress address) throws NoSuchFieldException, IllegalAccessException {
        int intAddress = -1;
        java.lang.reflect.Field addressField = InetAddress.class.getDeclaredField("address");
        addressField.setAccessible(true);
        intAddress = addressField.getInt(address);
        addressField.setAccessible(false);
        return intAddress;
    }

    /*
     * This method manipulates the localhost cache obj of Hostname  
     * that it looks like the previous localhost name was oldName and is not resolvable anymore. 
     * The old name is still in the LastResolveSuccessTimestamp time window
     * The current localhost name is the real local host name.
     * This is needed to test if the localhost cache obj can expire
     */
    private void fakeLocalHostRenameHappened(String oldName) throws NoSuchFieldException, IllegalAccessException, UnknownHostException {
        Hostname fakeOldHostNameObj = Hostname.getInstance(oldName);
        // the sdm system does not know jet that the local host is now invalid!
        //Give it a valid lastResolveSuccessTimestamp
        stampHostnameLastResolveSuccessTimestamp(fakeOldHostNameObj);
        //leave the valid local host ip Address but change the name!
        int localhostIntAddr = getIntRepresentationOfInetAdress(localHost);
        InetAddress localhostAddress = createInetAddressForHost(oldName, localhostIntAddr);
        replaceInetAddress(fakeOldHostNameObj, localhostAddress);
        // this block places the host into the cache
        placeHostnameObjInCache(fakeOldHostNameObj);


        // this block resets the separate localhost cache
        java.lang.reflect.Field localHostField = Hostname.class.getDeclaredField("localHost");
        localHostField.setAccessible(true);
        localHostField.set(Hostname.class, fakeOldHostNameObj);
        localHostField.setAccessible(false);
    }
}
