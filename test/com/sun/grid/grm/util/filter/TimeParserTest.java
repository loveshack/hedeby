/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util.filter;

import junit.framework.TestCase;

/**
 *
 * @author rh150277
 */
public class TimeParserTest extends TestCase {
    
    public TimeParserTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testParsing() throws Exception {
        
//      new TSFormat( "mm"         , "[0-5][0-9]", Calendar.MINUTE, Calendar.MINUTE )
//    , new TSFormat( "HH:mm"      , "[0-2][0-9]:[0-5][0-9]", Calendar.HOUR_OF_DAY, Calendar.MINUTE )
//    , new TSFormat( "HH:mm:ss"   , "[0-2][0-9]:[0-5][0-9]:[0-5][0-9]", Calendar.HOUR_OF_DAY, Calendar.SECOND )
//    , new TSFormat( "dd HH:mm"   , "[0-3][0-9] [0-2][0-9]:[0-5][0-9]", Calendar.DAY_OF_MONTH, Calendar.MINUTE )
//    , new TSFormat( "dd HH:mm:ss"  , "[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]", Calendar.DAY_OF_MONTH, Calendar.SECOND )
//    , new TSFormat( "MM/dd HH:mm", "[0-1][0-9]/[0-3][0-9] [0-2][0-9]:[0-5][0-9]", Calendar.MONDAY, Calendar.MINUTE)
//    , new TSFormat( "MM/dd HH:mm:ss", "[0-1][0-9]/[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]", Calendar.MONDAY, Calendar.SECOND)
//    , new TSFormat( "yyyy/MM/dd HH:mm", "[0-9]{4}/[0-1][0-9]/[0-3][0-9] [0-2][0-9]:[0-5][0-9]", Calendar.YEAR, Calendar.MINUTE)
//    , new TSFormat( "yyyy/MM/dd HH:mm:ss", "[0-9]{4}/[0-1][0-9]/[0-3][0-9] [0-2][0-9]:[0-5][0-9]", Calendar.YEAR, Calendar.SECOND)

        TimeParser.parse("12");
        TimeParser.parse("00:12");
        TimeParser.parse("00:12:00");
        
    }
}
