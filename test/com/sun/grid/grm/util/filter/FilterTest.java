/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.filter;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.NumberParser;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import junit.framework.*;

/**
 *
 */
public class FilterTest extends TestCase {
    
    private final Map<String,Object> valueMap = new HashMap<String,Object>();
    private MapFilterSourceAdapter<String> src = new MapFilterSourceAdapter<String>(valueMap);
    
    
    
    public FilterTest(String testName) {
        super(testName);
    }
    
    private void test(String filter, boolean result) throws Exception {
        FP test = new FP(filter,result,false);
        test.test();
    }
    
    public void testString() throws Exception {
        test("\"a\" = \"a\"", true);
        test("\"a\" != \"a\"", false);
        test("\"a\" > \"a\"", false);
        test("\"a\" < \"a\"", false);
        test("\"a\" >= \"a\"", true);
        test("\"a\" <= \"a\"", true);
        
        test("'a' = 'a'", true);
        test("'a' != 'a'", false);
        test("'a' > 'a'", false);
        test("'a' < 'a'", false);
        test("'a' >= 'a'", true);
        test("'a' <= 'a'", true);
        
        test("'aa' = 'a'", false);
        test("'aa' != 'a'", true);
        test("'aa' > 'a'", true);
        test("'aa' < 'a'", false);
        test("'aa' >= 'a'", true);
        test("'aa' <= 'a'", false);
    }
    
    public void testNumber() throws Exception {
        test("10 = 10"      , true);
        test("10.01 = 10.01", true);
        test("10 = 10.00"   , true);
        test("10 = '10'"    , true);
        
        test("10 != 10"      , false);
        test("10.01 != 10.01", false);
        test("10 != '10'"    , false);
        test("10 != 10.00"   , false);
        
        test("10 < 11"      , true);
        test("'10' < 11"    , true);
        test("10.01 < 10.02", true);
        test("10 < 10.01"   , true);
        test("true < 10" , false);
        test("true < 10.0" , false);
        
        test("10 <= 11"      , true);
        test("10.01 <= 10.02", true);
        test("10 <= 10.01"   , true);
        test("'10' <= 10.0"  , true);
        test("'10.0' <= 10"  , true);
        test("1k <= 1024"    , true);
        test("true <= 10" , false);
        test("true <= 10.0" , false);
        test("11 > 10"      , true);
        test("10.02 > 10.01", true);
        test("11 > 10.05"   , true);
        
        test("11 >= 10"      , true);
        test("10.02 >= 10.01", true);
        test("11 >= 10.05"   , true);
        
        test("10 = infinity", false);
        test("10 != infinity", true);
        test("10 < infinity", true);
        test("10 <= infinity", true);
        test("10 > infinity", false);
        test("10 >= infinity", false);
        
        valueMap.put("BLUBBER", Double.NaN);
        test("10 = BLUBBER", false);
        test("10 != BLUBBER", true);
        test("10 > BLUBBER", false);
        test("10 >= BLUBBER", false);
        test("10 < BLUBBER", true);
        test("10 <= BLUBBER", true);
    }

    public void testMatches() throws Exception {
        test ("\"a\" matches \"[a-z]\"", true);
        test ("'a' matches \"[a-z]\"", true);
        test ("1 matches \"[0-9]\"", true);
        test ("10.001 matches \"[0-9][0-9]+\\\\.[0-9]+\"", true);
    }
    
    public void testMultipliers() throws Exception {
        
        test("1K = 1024", true);
        test("1K = '1024'", true);
        test("1K <= '1024'", true);
        test("1k = 1000", true);
        test("1k <= 1000", true);
        test("1k > 999", true);
        
        valueMap.put("BINARY_GIGA", NumberParser.BINARY_GIGA );
        valueMap.put("BINARY_MEGA", NumberParser.BINARY_MEGA );
        valueMap.put("BINARY_KILO", NumberParser.BINARY_KILO );
        valueMap.put("DECIMAL_GIGA", NumberParser.DECIMAL_GIGA );
        valueMap.put("DECIMAL_MEGA", NumberParser.DECIMAL_MEGA );
        valueMap.put("DECIMAL_KILO", NumberParser.DECIMAL_KILO );
        
        test("BINARY_GIGA = 1G", true);
        test("BINARY_MEGA = 1M", true);
        test("BINARY_KILO = 1K", true);
        test("DECIMAL_GIGA = 1g", true);
        test("DECIMAL_MEGA = 1m", true);
        test("DECIMAL_KILO = 1k", true);
        
        test("1G > 1M", true);
        test("1M > 1K", true);
        test("1K > 1k", true);
        
        valueMap.put("INTEGER_MAX_VALUE", Integer.MAX_VALUE);
        valueMap.put("INTEGER_MAX_VALUE_AND_ONE", ((long)Integer.MAX_VALUE) + 1L);
        test("INTEGER_MAX_VALUE_AND_ONE > INTEGER_MAX_VALUE", true);
    }
    
    public void testNullValues() throws Exception {
        valueMap.put("BLUBBER", null);
        valueMap.put("BLABBER", null);
        
        test("BLUBBER > 0", false);
        test("BLUBBER < 0", false);
        test("BLUBBER = 0", false);
        test("BLUBBER != 0", true);
        test("BLUBBER = null", true);
        test("BLUBBER = BLABBER", true);
        
        test("0 > BLUBBER", false);
        test("0 < BLUBBER", false);
        test("0 = BLUBBER", false);
        test("0 != BLUBBER", true);
        test("null = BLUBBER", true);
    }
    
    public void testBoolean() throws Exception {
        test("false < true", true);
        test("false <= true", true);
        test("false > true", false);
        test("false >= true", false);
        test("true >= false", true);
        test("true >= true", true);
        test("false = false", true);
        test("true = false", false);
        valueMap.put("BLABBER", true);
        test("BLABBER = true", true);
        test("BLABBER = 'true'", true);
        test("BLABBER = \"true\"", true);
        
        // The string value will be parsed into a boolean false
        // 'true' or 'true' becomes the boolean value true
        // in all other cases the string value will be to the boolean value false
        test("true >= \"a\"" , true);
        test("true >= \"zzzz\"", true);
        
        // Boolean can not be compared with a number
        // result is always false
        test("true > 10", false);
        test("true >= 10", false);
        test("true < 10", false);
        test("true <= 10", false);
        test("true = 10", false);
        test("true != 10", true);
        
    }
    
    public void testNegativeNumbers() throws Exception {
        test("-1 = -1", true);
        test("-1.0 = -1", true);
        test("-1k < 0", true);
        test("-1k = -1000", true);
    }
    
    
    public void testStringQuoting() throws Exception {
        Filter<Object> filter = FilterHelper.<Object>parse("\"-\\\\-\" = 1");
        assertTrue(" filer must be a compare CompareFilter", filter instanceof CompareFilter);
        
        CompareFilter<Object> cf = (CompareFilter<Object>)filter;
        
        FilterValue<Object> op1 = cf.getFirstOperant();
        assertEquals("string is not correct quoted", "-\\-", op1.getValue(null));
    }
    
    public void testMultiLine() throws Exception {
        Filter<Resource> filter = FilterHelper.<Resource>parse("resourceHostname = \"hostA\" \r\n| resourceHostname = \"hostB\" \n| resourceHostname = \"hostC\"");
        assertTrue(" filer must be a OrFilter", filter instanceof OrFilter);
    }
    
    public void testCharQuoting() throws Exception {
        Filter<Object> filter = FilterHelper.<Object>parse("\'\\n\' = 1");
        assertTrue(" filer must be a compare CompareFilter", filter instanceof CompareFilter);
        
        CompareFilter<Object> cf = (CompareFilter<Object>)filter;
        
        FilterValue<Object> op1 = cf.getFirstOperant();
        assertEquals("character is not correct quoted (1)", '\n', op1.getValue(null));
        
        filter = FilterHelper.<Object>parse("\'a\\ta\'=0");
        assertTrue(" filer must be a compare CompareFilter", filter instanceof CompareFilter);
        
        cf = (CompareFilter<Object>)filter;
        
        op1 = cf.getFirstOperant();
        assertEquals("character is not correct quoted (2)", "a\ta", op1.getValue(null));
    }
    
    public void testCompound() throws Exception {
        test("!\"b\" >= \"a\"", false);
        test("!(\"b\" >= \"a\")", false);
        test("1 = 2 | 1 = 1", true);
        test("1 = 2 & 1 = 1", false);
        test("1 = 2 | 1 = 2 & 1 = 1", false);
        test("(1 = 1 | 1 = 2) & 1 = 1", true);
        test("1 = 2 | ! ( 1 = 2 & 1 = 1)", true);
    }
    
    public void testTimestamp() throws Exception {
        
        // TimeParser support 9 time formats
        String [] testCases = {
            "03",                     // mm   (mm=Minutes, MM=Day in month)
            "12:00",                  // hh:mm
            "12:00:01",               // hh:mm:ss
            "01 12:00",               // DD hh:mm
            "01 12:00:01",            // DD hh:mm:ss
            "11/01 12:00",            // MM/DD hh:mm
            "11/01 12:00:01",         // MM/DD hh:mm:ss
            "2009/11/01 12:00",       // YYYY/MM/DD hh:mm
            "2009/11/01 12:00:01"     // YYYY/MM/DD hh:mm:ss
        };
        
        for(String testCase: testCases) {
            Date date = TimeParser.parse(testCase);
            valueMap.put("date", date);

            test("TS{" + testCase + "} = date", true);
            test("TS{" + testCase + "} < date", false);
            test("TS{" + testCase + "} > date", false);
            test("TS{" + testCase + "} <= date", true);
            test("TS{" + testCase + "} >= date", true);
        }        
    }

    /**
     * Helper method that constructs two filters from the same expression and
     * checks that the resulting Filter objects are equal. It also checks that
     * their hashCode value is the same.
     * @param filterExpr the String from which the Filter is constructed
     * @throws java.lang.Exception
     */
    private void filterEqual(String filterExpr) throws Exception {
        Filter<Object> f1 = FilterHelper.<Object>parse(filterExpr);
        Filter<Object> f2 = FilterHelper.<Object>parse(filterExpr);
        filterEqual(f1, f2);
    }

    /**
     * Asserts that two Filters are equal and have the same hashCode.
     * @param f1 first Filter
     * @param f2 second Filter
     * @throws java.lang.Exception
     */
    private void filterEqual(Filter f1, Filter f2) throws Exception {
        assertEquals("Filter " + f1 + " must be equal to Filter " + f2, f1, f2);
        assertEquals("Filter " + f2 + " must be equal to Filter " + f1, f2, f1);
        assertEquals("Filter " + f1 + " must have same hashCode as Filter " + f2, f1.hashCode(), f2.hashCode());
    }

    /**
     * Helper method that constructs two filters and checks that the resulting
     * Filter objects are NOT equal.
     * @param filterExpr1 the String from which the first Filter is constructed
     * @param filterExpr2 the String from which the second Filter is constructed
     * @throws java.lang.Exception
     */
    private void filterNotEqual(String filterExpr1, String filterExpr2) throws Exception {
        Filter<Object> f1 = FilterHelper.<Object>parse(filterExpr1);
        Filter<Object> f2 = FilterHelper.<Object>parse(filterExpr2);
        filterNotEqual(f1, f2);
    }
    
    /**
     * Asserts that two Filters are NOT equal.
     * @param f1 first Filter
     * @param f2 second Filter
     * @throws java.lang.Exception
     */
    private void filterNotEqual(Filter f1, Filter f2) throws Exception {
        assertFalse("Filter " + f1 + " must NOT be equal to Filter " + f2, f1.equals(f2));
        assertFalse("Filter " + f2 + " must NOT be equal to Filter " + f1, f2.equals(f1));
    }

    /**
     * Tests the equality of different CompareFilter to each other.
     * @throws Exception
     */
    public void testEqualsCompareFilter() throws Exception {
        filterEqual("1=1");
        filterEqual("1<1");
        filterEqual("1>=1");
        filterEqual("1!=1");
        filterEqual("1=1");
        filterEqual("a=1");
        filterEqual("b=c");
        filterEqual("d>e");
        filterNotEqual("1=1", "1>1");
        filterNotEqual("2=1", "1=2"); // !! this is NOT equal for the equals method
    }

    /**
     * Tests the equality of different RegExpFilter to each other.
     * @throws Exception
     */
    public void testEqualsRegExpFilter() throws Exception {
        filterEqual("1 matches 'foo.*'");
        filterEqual("resource_id matches 'foo.*'");
        filterEqual("resource_id matches '.*foo.*'");
        filterNotEqual("1 matches 'foo.*'", "resource_id matches 'foo.*'");
    }

    /**
     * Tests the equality of different RegExpFilter to each other.
     * @throws Exception
     */
    public void testEqualsCompoundFilter() throws Exception {
        filterEqual("1=1 & b=c");
        filterEqual("1>1 | b<c");
        filterNotEqual("1=1 & b=c", "1=1 | b=c");
    }

    /**
     * Tests the equality of different NotFilter to each other.
     * @throws Exception
     */
    public void testEqualsNotFilter() throws Exception {
        filterEqual("!(1=1)");
        filterEqual("!(a matches 'b.*r')");
        filterNotEqual("!(1=1)", "!(!(1=1))");
    }

    /**
     * Tests the equality of different complex Filter to each other.
     * @throws Exception
     */
    public void testEqualsComplex() throws Exception {
        filterEqual("!(1=1 & b=c) | foo matches 'b.*r'");
        filterEqual("!(!(1=1 & b=c) | foo matches 'b.*r')");
    }

    /**
     * Tests the equality of always and never matching Filter to each other.
     * @throws Exception
     */
    public void testEqualsSpecialFilter() throws Exception {
        Filter<Object> f1 = ConstantFilter.alwaysMatching();
        Filter<Object> f2 = ConstantFilter.alwaysMatching();
        filterEqual(f1, f2);
        Filter<Object> f3 = ConstantFilter.neverMatching();
        Filter<Object> f4 = ConstantFilter.neverMatching();
        filterEqual(f3, f4);
        filterNotEqual(f1, f3);
    }

    private class FP<Object> {
        String filter;
        boolean result;
        boolean invalid;
        
        public FP(String filter, boolean result, boolean invalid) {
            this.filter = filter;
            this.result = result;
            this.invalid = invalid;
        }
        
        private void test() throws Exception {
            Filter<String> f = null;
            try {
                f = FilterHelper.<String>parse(filter);
                if(invalid) {
                    fail(filter + " must be not valid");
                }
            } catch(Exception ex) {
                if(invalid) {
                    return;
                }
                throw ex;
            }
            assertEquals(String.format("[%s] must be %s", filter, Boolean.toString(result)), result, f.matches(src));
            
        }
    }
}
