/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 */
public class EventListenerSupportTest extends TestCase {

    private final static Logger log = Logger.getLogger(EventListenerSupportTest.class.getName());
    
    public EventListenerSupportTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testSynchronousEvents() throws Exception {
        
        EventListenerSupport<DummyEventListener> els = EventListenerSupport.<DummyEventListener>newSynchronInstance(DummyEventListener.class);
        DummyEventHistory hist = new DummyEventHistory();
        els.addListener(hist);
        for(int i = 0; i < 10; i++) {
            els.getProxy().eventHappend(i);
        }
        
        for(int i = 0; i < 10; i++) {
            hist.assertEvent(i, 100);
        }
    }
    
    public void testASynchronEvents() throws Exception {
        log.entering(EventListenerSupportTest.class.getName(), "testASynchronEvents");
        
        final EventListenerSupport<DummyEventListener> els = EventListenerSupport.<DummyEventListener>newAsynchronInstance(DummyEventListener.class);
        DummyEventHistory hist = new DummyEventHistory();

        els.addListener(hist);
        
        ExecutorService executor = Executors.newCachedThreadPool();
        
        final Lock lock = new ReentrantLock();
        final AtomicInteger eventId = new AtomicInteger();
        int threadCount = 10;
        final int count = 1000;
        for(int i = 0; i < threadCount; i++) {
            executor.submit(new Runnable() {
                
                public void run() {
                    while(true) {
                        lock.lock();
                        try {
                            int id = eventId.getAndIncrement();
                            if (id < count) {
                                els.getProxy().eventHappend(id);
                            } else {
                                break;
                            }
                        } finally {
                            lock.unlock();
                        }
                        Thread.yield();
                    }
                }
            });
        }
        
        executor.shutdown();
        
        for(int i = 0; i < count; i++) {
            hist.assertEvent(i, 1000);
        }
        log.exiting(EventListenerSupportTest.class.getName(), "testASynchronEvents");
    }
    
    public interface DummyEventListener {
        public void eventHappend(int id);
    }
    
    private static class DummyEventHistory implements DummyEventListener {

        private LinkedList<Integer> events = new LinkedList<Integer>();
        private Lock lock = new ReentrantLock();
        private Condition cond = lock.newCondition();
        
        public void eventHappend(int id) {
            lock.lock();
            try {
                log.log(Level.FINE, "Got event " + id);
                events.addLast(id);
                cond.signalAll();
            } finally {
                lock.unlock();
            }
        }
        
        public void assertEvent(Integer id, long timeout) throws InterruptedException {
            long endTime = System.currentTimeMillis() + timeout;
            lock.lock();
            try {
                log.log(Level.FINE, "assert event " + id);
               while(events.isEmpty()) {
                   long rest = endTime - System.currentTimeMillis();
                   if (rest <= 0) {
                       fail("Timeout while waiting for event " + id);
                   }
                   cond.await(rest, TimeUnit.MILLISECONDS);
               }
               assertEquals("Got unexpected event", id, events.removeFirst());
            } finally {
                lock.unlock();
            }
        } 
    }
    
    
}
