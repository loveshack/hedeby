/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.FilterVariable;
import com.sun.grid.grm.util.filter.VariableResolver;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class which stores the history of events
 * 
 * @param <T> the type of the event listeer
 */
public class EventHistory<T>  {

    private final Logger log = Logger.getLogger(EventHistory.class.getName());
    private final Lock lock = new ReentrantLock();
    private final Condition gotEventCondition = lock.newCondition();
    
    private final T proxy;
    private final List<Event> history = new LinkedList<Event>();
    private final MyInvocationHandler invocationHandler = new MyInvocationHandler();
    private final Class<T> listenerInterface;
    
    @SuppressWarnings("unchecked")
    private EventHistory(Class<T> listenerInterface) {
        this.listenerInterface = listenerInterface;
        proxy = (T)Proxy.newProxyInstance(listenerInterface.getClassLoader(),
                                          new Class<?> [] { listenerInterface }, invocationHandler);
    
    }
    
    /**
     * Create a new instanceof of the EventHistory
     * @param <T> the type of the event listener
     * @param listenerInterface the event listener class
     * @return the EventHistory
     */
    public static <T> EventHistory<T> newInstance(Class<T> listenerInterface) {
        return new EventHistory<T>(listenerInterface);
    }
    
    
    private static Filter<Event> createArgFilter(int index, Object value) {
        FilterVariable<Event> var = new FilterVariable<Event>(String.format("args[%d]", index));
        FilterConstant<Event> val = new FilterConstant<Event>(value);
        return new CompareFilter<Event>(var, val, CompareFilter.Operator.EQ);
    }

    private Filter<Event> createMethodnameFilter(String name) {
        
        boolean found = false;
        for(Method m: listenerInterface.getMethods()) {
            if( m.getName().equals(name)) {
                found = true;
                break;
            }
        }
        if (!found) {
            throw new IllegalArgumentException(String.format("method %s not defined in listener interface %s", name, listenerInterface.getName()));
        }
        FilterVariable<Event> var = new FilterVariable<Event>("method");
        FilterConstant<Event> val = new FilterConstant<Event>(name);
        return new CompareFilter<Event>(var, val, CompareFilter.Operator.EQ);
    }
    
    /**
     * clear the event history
     */
    public void clear() {
        lock.lock();
        try {
            history.clear();
        } finally {
            lock.unlock();
        }
    }
    
    public int size() {
        lock.lock();
        try {
            return history.size();
        } finally {
            lock.unlock();
        }
    }
    /**
     * Get the dynamic proxy for the event listenr
     * @return the dynamic proxy
     */
    public T getProxy() {
        return proxy;
    }
    
    /**
     * Check that a event listemer method has been called
     * @param methodName the name of the event listener method
     * @param timeout  max waiting time for the event
     * @return <code>true</code> if the event method has been called
     * @throws java.lang.InterruptedException if the thread has been interrupted while waiting
     */
    public boolean hasEvent(String methodName, long timeout) throws InterruptedException {
        Filter<Event> filter = createMethodnameFilter(methodName);
        return hasEvent(filter, timeout);
    }
    
    /**
     * Check that a event listemer method with specific arugments has been called
     * @param methodName the name of the event listener method
     * @param timeout  max waiting time for the event
     * @param args     the expected arguments for the event listener method call
     * @return <code>true</code> if the event method with the arguments has been called
     * @throws java.lang.InterruptedException if the thread has been interrupted while waiting
     */
    public boolean hasEvent(String methodName, long timeout, Object ... args) throws InterruptedException {
        
        if(args == null || args.length == 0) {
            return hasEvent(methodName, timeout);
        }
        AndFilter<Event> filter = new AndFilter<Event>(args.length+ 1);
        filter.add(createMethodnameFilter(methodName));
        for(int i = 0; i < args.length; i++) {
            filter.add(createArgFilter(i, args[i]));
        }
        return hasEvent(filter, timeout);
    }
    
    private boolean hasEvent(Filter<Event> filter, long timeout) throws InterruptedException {
        long endTime = System.currentTimeMillis() + timeout;
        
        EventVariableResolver resolver = new EventVariableResolver();
        
        while (true) {
            lock.lock();
            try {
                for(Event evt: history) {
                    resolver.setEvent(evt);
                    if (filter.matches(resolver)) {
                        return true;
                    }
                }
                long rest = endTime - System.currentTimeMillis();
                if (rest > 0) {
                    gotEventCondition.await(rest, TimeUnit.MILLISECONDS);
                } else {
                    return false;
                }
            } finally {
                lock.unlock();
            }
        }
    }
    
    
    private static class Event {
        private final Method method;
        private final Object [] args;
        
        public Event(Method method, Object [] args) {
            this.method = method;
            this.args = args;
        }

        public Method getMethod() {
            return method;
        }

        public Object[] getArgs() {
            return args;
        }
        
        @Override
        public String toString() {
            return String.format("%s(%s)", method.getName(), Arrays.toString(args));
        }
        
    }
    
    private static class EventVariableResolver implements VariableResolver<Event> {
        private Event evt;
        
        public void setEvent(Event evt) {
            this.evt = evt;
        }
        public Object getValue(String name) {
            if (evt == null) {
                return null;
            }
            if (name.equals("method")) {
                return evt.getMethod().getName();
            } else if (name.startsWith("args")) {
                if(name.length() < 6) {
                    return null;
                }
                String indexStr = name.substring(6, name.length() -1);
                try {
                    int index = Integer.parseInt(indexStr);
                    
                    if (index >= 0 && index < evt.getArgs().length) {
                        return evt.getArgs()[index];
                    } else {
                        return null;
                    }
                } catch(NumberFormatException ex) {
                    return null;
                }
            } else {
                return null;
            }
        }
    }
    
    private class MyInvocationHandler implements InvocationHandler {

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.getName().equals("hashCode")) {
                return this.hashCode();
            } else if (method.getName().equals("equals")) {
                return this.equals(args[1]);
            } else {
                Event evt = new Event(method, args);
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "Got event {0}", evt.toString());
                }
                lock.lock();
                try {
                    history.add(evt);
                    gotEventCondition.signalAll();
                } finally {
                    lock.unlock();
                }
                return null;
            }
        }
    }
}
