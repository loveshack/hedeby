/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Filter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * A Mockup to check logger activity.
 * 
 * <p>This class allows to install a mockup handler in any logger of the system.
 * It can report all LogRecords logged </p>
 */
public class LoggerHandlerMockup extends Handler {

    private Level level;
    private boolean useParentHandlers;
    private Filter filter;
    private final Logger logger;
    private final List<LogRecord> list = new LinkedList<LogRecord>();

    /**
     * Constructor 
     * @param logger is the Logger to be mocked up
     */
    public LoggerHandlerMockup(Logger logger) {
        this.level = logger.getLevel();
        this.useParentHandlers = logger.getUseParentHandlers();
        this.logger = logger;
        this.filter = getFilter();
    }

    /**
     * this method activates the mockup functionality 
     * @return this instance to chain setters
     */
    public LoggerHandlerMockup installMockup() {
        //install mockup handler
        logger.addHandler(this);
        //get rid of any installed filter!
        logger.setFilter(null);
        return this;
    }

    /**
     * @param minLogLevel rise level to this level if not allready as high /higher
     * @return this instance to chain setters
     */
    public LoggerHandlerMockup minLogLevel(Level minLogLevel) {
        //adjust level
        if (level == null || level.intValue() < minLogLevel.intValue()) {
            logger.setLevel(minLogLevel);
        }
        return this;
    }

    /**
     * @param useParentHandlers suppresses the loggig cascade to the Parenthandler
     * @return this instance to chain setters
     */
    public LoggerHandlerMockup useParentHandlers(boolean useParentHandlers) {
        //block logging of parent handlers
        logger.setUseParentHandlers(useParentHandlers);
        return this;
    }

    /**
     * Reestablish the previous state of the logger
     * @return this instance to chain setters
     */
    public LoggerHandlerMockup unistallMockup() {
        logger.setUseParentHandlers(useParentHandlers);
        logger.setLevel(level);
        logger.removeHandler(this);
        if (filter != null) {
            logger.setFilter(filter);
        }
        return this;
    }

    /**
     * Not needed but is abstract in Handler
     */
    public void flush() {
    }

    /**
     * Not needed but is abstract in Handler
     * @throws SecurityException 
     */
    public void close() throws SecurityException {
    }

    /**
     * After the mockup is installed, this method is called if a record 
     * is logged 
     */
    public void publish(LogRecord record) {
        synchronized (list) {
            list.add(record);
        }
    }

    /**
     * @return the size of the list of records logged by the mockup
     */
    public int getLogRecordListSize() {
        synchronized (list) {
            return list.size();
        }
    }

    /**
     * @return a copy of the list of records logged by the mockup
     */
    public List<LogRecord> getLogRecordList() {
        synchronized (list) {
            return new ArrayList<LogRecord>(list);
        }
    }

    /**
     * Deletes all records that are logged so far
     */
    public void clearLogRecordList() {
        synchronized (list) {
            list.clear();
        }
    }
}