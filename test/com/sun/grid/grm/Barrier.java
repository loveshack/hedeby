/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class can be used to block a thread
 */
public class Barrier {

    private final static Logger log = Logger.getLogger(Barrier.class.getName());
    
    private final Lock lock = new ReentrantLock();
    private final Condition blockedCondition = lock.newCondition();
    private final Condition wakeupCondition = lock.newCondition();
    
    private final Long sleepTime;
    
    private boolean enabled = true;
    private boolean blocked;
    private boolean interrupted = false;
    private final String name;

    private Barrier(String name, Long sleepTime) {
        super();
        this.name = name;
        this.sleepTime = sleepTime;
    }
    
    private Barrier(String name) {
        this(name, null);
    }
    
    /**
     * Create a new enabled Barrier
     * @param name the name of the Barrier
     * @return the Barrier
     */
    public static Barrier newEnabledInstance(String name) {
        return new Barrier(name).enable(true);
    }

    /**
     * Create a new enabled Barrier which block the calling thread only
     * a limited time frame
     * @param name  the name of the Barrier
     * @param sleepTime the sleep time in millis
     * @return the Barrier
     */
    public static Barrier newEnabledInstance(String name, long sleepTime) {
        return new Barrier(name, sleepTime).enable(true);
    }
    
    /**
     * Create a new disable Barrier.
     * @param name the name of the Barrier
     * @return  the Barrier
     */
    public static Barrier newDisabledInstance(String name) {
        return new Barrier(name).enable(false);
    }
    
    /**
     * Create a new disabled Barrier which blocks the calling thread only
     * a limited time frame
     * @param name  the name of the Barrier
     * @param sleepTime the time frame
     * @return the Barrier
     */
    public static Barrier newDisabledInstance(String name, long sleepTime) {
        return new Barrier(name, sleepTime).enable(false);
    }
    
    /**
     * Enable/Disable the Barrier.
     * @param enabled if <code>true</code> the Barrier is enabled 
     * @return the Barrier
     */
    public Barrier enable(boolean enabled) {
        log.entering(Barrier.class.getName(), "enable", enabled);
        lock.lock();
        try {
            this.enabled = enabled;
            this.wakeupCondition.signalAll();
        } finally {
            lock.unlock();
        }
        log.exiting(Barrier.class.getName(), "enable");
        return this;
    }
    
    /**
     * Is the Barrier enabled
     * @return <code>true</code> if the Barrier is enabled
     */
    public boolean isEnabled() {
        lock.lock();
        try {
            return enabled;
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * Get the sleep time of the Barrier
     * @return the sleep time in millis
     */
    public Long getSleepTime() {
        return sleepTime;
    }
    
    /**
     * Wakeup the blocked thread (if there is any)
     */
    public void wakeup() {
        log.entering(Barrier.class.getName(), "wakeup");
        enable(false);
        log.exiting(Barrier.class.getName(), "wakeup");
    }

    /**
     * Wait until a a different thread is blocked
     * @throws java.lang.InterruptedException if the calling thread has been interrupted while waiting
     */
    public void waitUntilBlocked() throws InterruptedException {
        log.entering(Barrier.class.getName(), "waitUntilBlocked");
        lock.lock();
        try {
            while (!blocked) {
                blockedCondition.await();
            }
        } finally {
            lock.unlock();
        }
        log.exiting(Barrier.class.getName(), "waitUntilBlocked");
    }

    /**
     * Wait until the barrier is blocked (with timeout)
     * @param timeout  the timeout in milliseconds
     * @return  <tt>true</tt> if the barrier has been blocked before the timeout is reached
     * @throws java.lang.InterruptedException
     */
    public boolean waitUtilBlocked(long timeout) throws InterruptedException {
        log.entering(Barrier.class.getName(), "waitUntilBlocked", timeout);
        long endTime = System.currentTimeMillis() + timeout;
        boolean ret;
        lock.lock();
        try {
            while (!blocked) {
                if(System.currentTimeMillis() >= timeout) {
                    ret = false;
                    break;
                }
                long rest = System.currentTimeMillis() - endTime;
                blockedCondition.await(rest, TimeUnit.MILLISECONDS);
            }
            ret = true;
        } finally {
            lock.unlock();
        }
        log.exiting(Barrier.class.getName(), "waitUntilBlocked", ret);
        return ret;
    }

    public boolean isBlocked() {
        lock.lock();
        try {
            return blocked;
        } finally {
            lock.unlock();
        }
    }
    
    public void interrupt() {
        log.entering(Barrier.class.getName(), "interrupt");
        lock.lock();
        try {
            interrupted = true;
            enabled = false;
            wakeupCondition.signalAll();
        } finally {
            lock.unlock();
        }
        log.exiting(Barrier.class.getName(), "interrupt");
    }
    
    /**
     * Block the calling thread until someone disables this Barrier
     * @throws java.lang.InterruptedException if the calling thread has been interrupted while waiting
     */
    public void waitUntilWakeup() throws InterruptedException {
        log.entering(Barrier.class.getName(), "waitUntilWakeup");
        Long timeout = null;
        if(sleepTime != null) {
            timeout = System.currentTimeMillis() + sleepTime;
        }
        lock.lock();
        try {
            blocked = true;
            blockedCondition.signalAll();
            if(enabled) {
                log.log(Level.FINE, "barrier {0} is blocked", name);
                while (enabled) {
                    if(Thread.currentThread().isInterrupted()) {
                        throw new InterruptedException();
                    }
                    if (timeout == null) {
                        wakeupCondition.await();
                    } else {
                        long rest = timeout - System.currentTimeMillis();
                        if(rest > 0) {
                            wakeupCondition.await(rest, TimeUnit.MILLISECONDS);
                        } else {
                            break;
                        }
                    }
                }
                if (interrupted) {
                    throw new InterruptedException();
                }
                log.log(Level.FINE, "barrier {0} wokeup", name);
            }
        } catch(InterruptedException ex) {
            log.throwing(Barrier.class.getName(), "waitUntilWakeup", ex);
            throw ex;
        } finally {
            blocked = false;
            lock.unlock();
        }
        log.exiting(Barrier.class.getName(), "waitUntilWakeup");
    }
}
