    /*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * A simple helper/wrapper class to mockup the InputStream of the RMISocket class 
 * @see MockupRmiClientSocketFactory
 * @see MockupInputStream
 */
public class MockupRmiSocket extends RmiSocket {
    private static final long serialVersionUID = -2008100601L;

    /**
     * Constructor wrapper
     * @param socket
     */
    public MockupRmiSocket(Socket socket) {
        super(socket);
    }

    /**
     * Wrapper method to introduce the mockupInputStream into the communication process
     * @return the InputStream of the inner Socket that is wrapped in a MockupInputStream
     * @throws java.io.IOException
     */
    @Override
    public InputStream getInputStream() throws IOException {
        return new MockupInputStream(super.getInputStream());
    }
}
