    /*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class is a Wrapper that allows to mockup EOFExceptions that are thrown if the 
 * read methods are called
 * @see MockupRMIClientSocketFactory
 * @see MockupRmiSocket
 */
public class MockupInputStream extends InputStream {

    private final InputStream myStream;
    private static final AtomicBoolean forceEOFExceptionAtRead = new AtomicBoolean(false);

    /**
     * * 
     * creates a wrapper object for the InputStream in
     * @param in InputStream object to be wrapped
     */
    public MockupInputStream(InputStream in) {
        myStream = in;
    }

    /**
     * call is forwarded to the wrapped InputStream object
     * @throws IOException
     */
    @Override
    public int available() throws IOException {
        return myStream.available();
    }

    /**
     * call is forwarded to the wrapped InputStream object
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        myStream.close();
    }

    /**
     * call is forwarded to the wrapped InputStream object
     * @param readlimit
     */
    @Override
    public void mark(int readlimit) {
        myStream.mark(readlimit);
    }

    /**
     * call is forwarded to the wrapped InputStream object
     * @throws IOException
     */
    @Override
    public void reset() throws IOException {
        myStream.reset();
    }

    /**
     * call is forwarded to the wrapped InputStream object
     */
    @Override
    public boolean markSupported() {
        return myStream.markSupported();
    }

    /**
     * call is forwarded to the wrapped InputStream object
     * @param n number of skipped bytes
     * @throws IOException
     */
    @Override
    public long skip(long n) throws IOException {
        return myStream.skip(n);
    }

    /**
     * Switches on /off the mockup behavior
     * @param forceEOFExceptionAtRead the switch
     */
    public static void setForceEOFExceptionAtRead(boolean forceEOFExceptionAtRead) {
        MockupInputStream.forceEOFExceptionAtRead.set(forceEOFExceptionAtRead);
    }

    /**
     * If forceEOFExceptionAtRead switch is on throw a new EOFException else 
     * call is forwarded to the wrapped InputStream object
     * @param b
     * @return
     * @throws java.io.IOException
     */
    @Override
    public int read(byte[] b) throws IOException {
        if (forceEOFExceptionAtRead.get()) {
            throw new EOFException();
        }
        return myStream.read(b);
    }

    /**
     * If forceEOFExceptionAtRead switch is on throw a new EOFException else 
     * call is forwarded to the wrapped InputStream object
     * @param b
     * @param off
     * @param len
     * @return
     * @throws java.io.IOException
     */
    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (forceEOFExceptionAtRead.get()) {
            throw new EOFException();
        }
        return myStream.read(b, off, len);
    }

    /**
     * If forceEOFExceptionAtRead switch is on throw a new EOFException else 
     * call is forwarded to the wrapped InputStream object
     * @return 
     * @throws java.io.IOException
     */
    @Override
    public int read() throws IOException {
        if (forceEOFExceptionAtRead.get()) {
            throw new EOFException();
        }
        return myStream.read();
    }
}
