/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sparepool;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.FixedUsageSLOConfig;
import com.sun.grid.grm.config.common.MinResourceSLOConfig;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.config.common.SLOSet;
import com.sun.grid.grm.config.naming.DummyConfigurationServiceContext;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.resource.adapter.ResourceAdapterStore;
import com.sun.grid.grm.resource.adapter.impl.SimpleResourceAdapterStore;
import com.sun.grid.grm.service.impl.AbstractServiceAdapter;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManagerSetup;
import com.sun.grid.grm.service.slo.event.SLOErrorEvent;
import com.sun.grid.grm.service.slo.event.SLOManagerEventListener;
import com.sun.grid.grm.service.slo.event.SLOUpdatedEvent;
import com.sun.grid.grm.service.slo.event.UsageMapUpdatedEvent;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManager;
import java.lang.reflect.Field;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;
import javax.naming.Context;
import junit.framework.TestCase;

/**
 * This test has been implemented to ensure that issue 582 is fixed. It tests
 * that in SparePool the SLOs PermanentRequestSLO, MinResourceSLO and FixedUsageSLO
 * can be used.
 */
public class SparePoolSLOTest extends TestCase {
    
    private final static Logger log = Logger.getLogger(SparePoolSLOTest.class.getName());
    
    private ExecutionEnv env;
    private SparePoolServiceAdapterImpl sparePool;
    
    public SparePoolSLOTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        log.entering(SparePoolSLOTest.class.getName(), "setUp");
        env = DummyExecutionEnvFactory.newInstance();
        sparePool = new TestSparePoolServiceAdapterImpl(new SparePoolServiceImpl(env, "spare_pool"));
        log.exiting(SparePoolSLOTest.class.getName(), "setUp");
    }

    @Override
    protected void tearDown() throws Exception {
        log.entering(SparePoolSLOTest.class.getName(), "tearDown");
        Context ctx = env.getContext();
        if (ctx instanceof DummyConfigurationServiceContext) {
            ((DummyConfigurationServiceContext)ctx).clear();
        }
        log.exiting(SparePoolSLOTest.class.getName(), "tearDown");
    }
    
    
    private void assertSLOupdate(SLOConfig sloConfig) throws Exception {
        log.entering(SparePoolSLOTest.class.getName(), "assertSLOupdate");
        
        SLOManagerEventHistory lis = new SLOManagerEventHistory();
        
        SparePoolServiceConfig config = new SparePoolServiceConfig();
        SLOSet slos = new SLOSet();
        slos.getSlo().add(sloConfig);
        config.setSlos(slos);
        
        RunnableSLOManager sloManager = sparePool.createSLOManager();
        RunnableSLOManagerSetup setup = sparePool.createSLOManagerSetup(config);
        sloManager.setUpdateInterval(setup.getUpdateInterval());
        sloManager.setSLOs(setup.getSlos());
        sloManager.addSLOManagerListener(lis);
        sloManager.update();
        lis.assertUpdateForSLO(sloConfig, 100);
        
        log.exiting(SparePoolSLOTest.class.getName(), "calcSLOs");
    }
    
    /**
     * Check that a PermanentRequestSLO can be handled by an instance of
     * <code>SparePoolServiceImpl</code>.
     * @throws java.lang.Exception
     */
    public void testPermanentRequestSLO() throws Exception {
        PermanentRequestSLOConfig slo = new PermanentRequestSLOConfig();
        slo.setUrgency(99);
        slo.setName("permanentRequest");
        slo.setQuantity(1);
        
        assertSLOupdate(slo);
    }
    
    /**
     * Check that a MinResourceSLO can be handled by an instance of
     * <code>SparePoolServiceImpl</code>.
     * @throws java.lang.Exception
     */
    public void testMinResourceSLO() throws Exception {
        MinResourceSLOConfig slo = new MinResourceSLOConfig();
        slo.setMin(1);
        slo.setName("minRes");
        slo.setUrgency(99);
        
        assertSLOupdate(slo);
    }
    
    /**
     * Check that a FixedUsageSLO can be handled by an instance of
     * <code>SparePoolServiceImpl</code>.
     * @throws java.lang.Exception
     */
    public void testFixedUsageSLO() throws Exception {
        FixedUsageSLOConfig slo = new FixedUsageSLOConfig();
        slo.setName("fixedUsage");
        slo.setUrgency(99);
        
        assertSLOupdate(slo);
    }
    
    private SparePoolServiceConfig createConfig(SLOConfig sloConfig) {
        
        SparePoolServiceConfig ret = new SparePoolServiceConfig();
        
        SLOSet slos = new SLOSet();
        slos.getSlo().add(sloConfig);
        ret.setSlos(slos);
        
        return ret;
    }
    
    private static class SLOManagerEventHistory implements SLOManagerEventListener {

        private final BlockingQueue<SLOUpdatedEvent> updateEvents = new LinkedBlockingQueue<SLOUpdatedEvent>();
        
        public void sloUpdated(SLOUpdatedEvent event) {
            updateEvents.add(event);
        }

        public void usageMapUpdated(UsageMapUpdatedEvent event) {
            // Ignore
        }

        public void sloUpdateError(SLOErrorEvent event) {
            // Ignore
        }
        
        public void assertUpdateForSLO(SLOConfig slo, long timeout) throws InterruptedException {
            SLOUpdatedEvent evt = updateEvents.poll(timeout, TimeUnit.MILLISECONDS);
            
            if(evt == null) {
                fail("Timeout while waiting for update event of slo " + slo.getName());
            }
            assertEquals("SLOUpdate event contains wrong slo name", slo.getName(), evt.getSLOName());
        }
    }
    
    
    /**
     * This class overrides only getResources method of the SparePoolServiceAdapterImpl.
     * 
     * It makes it possible to run an SLO calculation without starting the spare_pool.
     * Normally the <code>getResource</code> method throws a <code>ServiceNotActiveException</code>
     * if the spare_pool is not started.
     */
    private static class TestSparePoolServiceAdapterImpl extends SparePoolServiceAdapterImpl {
        public TestSparePoolServiceAdapterImpl(SparePoolServiceImpl component) throws GrmException {
            super(component);
            
            try {
                Field field = AbstractServiceAdapter.class.getDeclaredField("resourceStore");
                field.setAccessible(true);
                @SuppressWarnings("unchecked")
                AtomicReference<ResourceAdapterStore> rs = (AtomicReference<ResourceAdapterStore>) field.get(this);
                rs.set(new SimpleResourceAdapterStore());
                field.setAccessible(false);
            } catch(Exception ex) {
                throw new GrmException("Can not set resourceStore", ex);
            }            
        }
    }
    
}
