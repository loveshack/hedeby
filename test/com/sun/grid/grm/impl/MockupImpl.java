/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.Barrier;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.Feature;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default implementation of most methods the <code>MockupComponent</code> interface. 
 * The different implementation of the <code>MockupComponent</code> delegates it's calls 
 * to an instance of this class.
 * @see MockupComponentImpl
 * @see MockupComponentAdapterImpl
 */
public class MockupImpl {

    private final static Logger log = Logger.getLogger(MockupImpl.class.getName());
    
    private final Barrier startComponentBarrier = Barrier.newDisabledInstance("startComponent");
    private final Barrier stopComponentBarrier = Barrier.newDisabledInstance("stopComponent");
    private final Barrier reloadComponentBarrier = Barrier.newDisabledInstance("reloadComponent");
    private final AtomicBoolean reloadRequiresRestart = new AtomicBoolean();
    private boolean exceptionInStartComponent;
    private boolean exceptionInReloadComponent;
    private boolean exceptionInStopComponent;
    private boolean exceptionInPrepareComponentStartup;
    private boolean exceptionInPrepareComponentShutdown;
    /**
     * Set up a test feature for the component
     * @param f the test feature
     */
    public void setup(Feature f) {
        switch (f) {
            case BLOCK_START_COMPONENT:
                startComponentBarrier.enable(true);
                break;
            case BLOCK_RELOAD_COMPONENT:
                reloadComponentBarrier.enable(true);
                break;
            case BLOCK_STOP_COMPONENT:
                stopComponentBarrier.enable(true);
                break;
            case RELOAD_REQUIRES_RESTART:
                reloadRequiresRestart.set(true);
                break;
            case EXCEPTION_IN_START_COMPONENT:
                exceptionInStartComponent = true;
                break;
            case EXCEPTION_IN_PREPARE_COMPONENT_START:
                exceptionInPrepareComponentStartup = true;
                break;
            case EXCEPTION_IN_PREPARE_COMPONENT_SHUTDOWN:
                exceptionInPrepareComponentShutdown = true;
                break;
            case EXCEPTION_IN_RELOAD_COMPONENT:
                exceptionInReloadComponent = true;
                break;
            case EXCEPTION_IN_STOP_COMPONENT:
                exceptionInStopComponent = true;
                break;
            default:
                throw new IllegalArgumentException("Unknown feature f");
        }
    }

    /**
     * Teardown a test feature of the component
     * @param f  the test feature
     */
    public void teardown(Feature f) {
        switch (f) {
            case BLOCK_START_COMPONENT:
                startComponentBarrier.enable(false);
                break;
            case BLOCK_RELOAD_COMPONENT:
                reloadComponentBarrier.enable(false);
                break;
            case BLOCK_STOP_COMPONENT:
                stopComponentBarrier.enable(false);
                break;
            case RELOAD_REQUIRES_RESTART:
                reloadRequiresRestart.set(false);
                break;
            case EXCEPTION_IN_PREPARE_COMPONENT_START:
                exceptionInPrepareComponentStartup = false;
                break;
            case EXCEPTION_IN_PREPARE_COMPONENT_SHUTDOWN:
                exceptionInPrepareComponentShutdown = false;
                break;
            case EXCEPTION_IN_START_COMPONENT:
                exceptionInStartComponent = false;
                break;
            case EXCEPTION_IN_RELOAD_COMPONENT:
                exceptionInReloadComponent = false;
                break;
            case EXCEPTION_IN_STOP_COMPONENT:
                exceptionInStopComponent = false;
                break;
            default:
                throw new IllegalArgumentException("Unknown feature f");
        }
    }
    
    public void waitUntil(Feature f) throws InterruptedException {
        
        
        switch(f) {
            case BLOCK_START_COMPONENT:
                startComponentBarrier.waitUntilBlocked();
                break;
            case BLOCK_STOP_COMPONENT:
                stopComponentBarrier.waitUntilBlocked();
                break;
            case BLOCK_RELOAD_COMPONENT:
                stopComponentBarrier.waitUntilBlocked();
                break;
            default:
                throw new IllegalArgumentException("Feature " + f + " does not support waitUntil");
        }
    }

    public void prepareComponentStartup(SparePoolServiceConfig config) throws GrmException {
        if(exceptionInPrepareComponentStartup) {
            throw new GrmException("Exception in component startup");
        }
    }
    
    /**
     * Start the component.
     * @param config   the configuration of the component
     * @throws com.sun.grid.grm.GrmException if the EXCEPTION_IN_START_COMPONENT feature is enabled
     */
    public void startComponent(SparePoolServiceConfig config) throws GrmException {
        try {
            log.entering(MockupImpl.class.getName(), "startComponent", config);
            if (exceptionInStartComponent) {
                throw new GrmException("Exception in startComponent");
            }
            startComponentBarrier.waitUntilWakeup();
            log.exiting(MockupImpl.class.getName(), "startComponent");
        } catch (InterruptedException ex) {
            log.log(Level.FINE, "startComponent has been interrupted", ex);
        }
    }

    public void prepareComponentShutdown(boolean fored) throws GrmException {
        if(exceptionInPrepareComponentShutdown) {
            throw new GrmException("Exception in prepare component shutdown");
        }
    }
    
    /**
     * Stop the component.
     * @param forced  <code>true</code> if in forced mode
     * @throws com.sun.grid.grm.GrmException if the EXCEPTION_IN_STOP_COMPONENT feature is enabled
     */
    public void stopComponent(boolean forced) throws GrmException {
        log.entering(MockupImpl.class.getName(), "stopComponent", forced);

        if (exceptionInStopComponent) {
            throw new GrmException("Exception in stopComponent");
        }
        // The barrier must not block in forced mode other wise
        // the stop(forced) call will not work
        if (!forced) {
            try {
                stopComponentBarrier.waitUntilWakeup();
            } catch (InterruptedException ex) {
                log.log(Level.FINE, "stopComponent has been interrupted");
            }
        }
        log.exiting(MockupImpl.class.getName(), "stopComponent");
    }

    public void prepareComponentReload(SparePoolServiceConfig config, boolean forced) throws GrmException, RestartRequiredException {
        if (reloadRequiresRestart.get()) {
            throw new RestartRequiredException();
        }
    }
    
    /**
     * Reload the component.
     * @param config the configuration of the component
     * @param forced  <code>true</code> if in forced mode
     * @throws com.sun.grid.grm.GrmException if the EXCEPTION_IN_RELOAD_COMPONENT feature is enabled
     */
    public void reloadComponent(SparePoolServiceConfig config, boolean forced) throws GrmException {
        try {
            log.entering(MockupImpl.class.getName(), "reloadComponent", forced);
            if (exceptionInReloadComponent) {
                throw new GrmException("Exception in reloadComponent");
            }

            reloadComponentBarrier.waitUntilWakeup();
        } catch (InterruptedException ex) {
            log.log(Level.FINE, "reloadComponent has been interrupted");
        }
        log.exiting(MockupImpl.class.getName(), "reloadComponent");
    }

}
