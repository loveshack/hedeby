/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.*;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.ComponentTestAdapter;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 *  Tests all possible component state transitions with a dummy component.
 */
public class ComponentStateTransitionTest extends AbstractComponentStateTransitionTester<MockupComponent> {
    
    public ComponentStateTransitionTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @Override
    protected ComponentTestAdapter<MockupComponent> createComponentTestAdapter() throws Exception {
        return new MockupComponentTestAdapter() {
            @Override
            protected MockupComponent createComponent() {
                return new MockupComponentImpl(null,"mockup");
            }
        };
    }
    
    /**
     * Check that the foreign action are finished correctly if the component
     * is stopped without forced flag
     * 
     * @throws java.lang.Exception
     */
    public void testStopWithForeignAction() throws Exception {
        
        comp.start().expect(ComponentState.STARTING, ComponentState.STARTED);
        BlockingAction a = new BlockingAction();
        
        Future<Boolean> f = comp.getComponent().submit(a);
        a.waitUntilActive();
        
        comp.stop(false).expect(ComponentState.STOPPING);
        a.wakeup();
        comp.expect(ComponentState.STOPPED);
        
        assertTrue(f.isDone());
        assertTrue(f.get());
    }
    
    public void testStopForcedWithForeignAction() throws Exception {
        comp.start().expect(ComponentState.STARTING, ComponentState.STARTED);
        BlockingAction a = new BlockingAction();
        
        Future<Boolean> f = comp.getComponent().submit(a);
        a.waitUntilActive();
        comp.stop(true).expect(ComponentState.STOPPING, ComponentState.STOPPED);
        assertFalse("Blocking action has not been interrupted by forced stop", f.get());
    }
    
    private static class BlockingAction implements Callable<Boolean> {
        
        private final Barrier barrier = Barrier.newEnabledInstance("sleeper");
        
        public Boolean call() {
            try {
                barrier.waitUntilWakeup();
                return true;
            } catch(InterruptedException ex) {
                return false;
            }
        }
        
        public void wakeup() {
            barrier.wakeup();
        }
        
        public void waitUntilActive() throws InterruptedException {
            barrier.waitUntilBlocked();
        }
    }
}
