/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.ComponentTestAdapter;
import com.sun.grid.grm.impl.Compiler.AbstractCodeGenerator;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Tests the component state transitions with ComponentContainer and a ComponentAdapter
 * and dynmic class loading.
 */
public class DynamicComponentContainerStateTransitionTest extends AbstractComponentStateTransitionTester<MockupComponent> {

    private static File baseDir;
    private static File classesDir;
    private static File classesDir1;
    
    private final static String ADAPTER_A_NAME = "TestAdapterA";
    private final static String ADAPTER_B_NAME = "TestAdapterB";

    private final static String PACKAGE_NAME = MockupComponentAdapterImpl.class.getPackage().getName();
    private final static String ADAPTER_A_FULL_QUALIFIED_NAME = String.format("%s.%s", PACKAGE_NAME, ADAPTER_A_NAME);
    private final static String ADAPTER_B_FULL_QUALIFIED_NAME = String.format("%s.%s", PACKAGE_NAME, ADAPTER_B_NAME);
    
    public DynamicComponentContainerStateTransitionTest(String name) {
        super(name);
    }
    
    @Override
    protected void setUp() throws Exception {
        // We don't complile the generated code for every test method.
        // The baseDir will be delete once the jvm is going down
        if(baseDir == null) {
            baseDir = TestUtil.getTmpDir(this);
            classesDir = new File(baseDir,"classes");
            File srcDir = new File(baseDir,"src");
            classesDir.mkdirs();
            Compiler.compile(new MyCodeGenerator(srcDir), classesDir);
            classesDir1 = new File(baseDir, "classes1");
            // Create a copy of the classes dir to simulate changes
            // in classpath
            Platform.getPlatform().copy(classesDir, classesDir1, true);
            // Platform should delete the baseDir within its shutdown handler
            Platform.getPlatform().deleteOnExit(baseDir);
        }
        // It is important that super.setUp is called at the end of this
        // method otherwise the createComponentTestAdapter is called before
        // the classedDir is initialized
        super.setUp();
    }
    

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    /**
     * This test checks that a <code>ComponentAdapter</code> is recreated if 
     * the simpleClassname of the <code>ComponentAdapter</code> has changed
     * @throws Exception 
     */
    public void testReloadLoadsNewAdapterIfClassnameChanged() throws Exception {
        
        getComp().start().expect(ComponentState.STARTING,ComponentState.STARTED);

        DynamicMockupComponentContainerImpl container = (DynamicMockupComponentContainerImpl)getComp().getComponent();
        
        assertEquals("Dynamically loaded component adapter has wrong classname",
                     ADAPTER_A_FULL_QUALIFIED_NAME, container.getAdapter().getClass().getName());
        
        container.classname(MockupComponentAdapterImpl.class.getPackage().getName() + '.' + ADAPTER_B_NAME);
       
        getComp().reload(false).expect(ComponentState.RELOADING,ComponentState.STOPPED,ComponentState.STARTING,ComponentState.STARTED);
        
        assertEquals("Dynamically reloaded component adapter has wrong classname",
                     ADAPTER_B_FULL_QUALIFIED_NAME,container.getAdapter().getClass().getName());
    }
    
    /**
     * This test checks that a <code>ComponentAdapter</code> is recreated if 
     * the classpath of the <code>ComponentAdapter</code> has changed.
     * 
     * @throws Exception
     */
    public void testReloadLoadsNewAdapterIfClasspathChanged() throws Exception {
        
        getComp().start().expect(ComponentState.STARTING,ComponentState.STARTED);

        DynamicMockupComponentContainerImpl container = (DynamicMockupComponentContainerImpl)getComp().getComponent();
        
        assertEquals("Dynamically loaded component adapter has wrong classname",
                     ADAPTER_A_FULL_QUALIFIED_NAME, container.getAdapter().getClass().getName());
        assertEquals("Dynamically loaded component adapter has wrong code source",
                     classesDir.toURI().toURL(), 
                     container.getAdapter().getClass().getProtectionDomain().getCodeSource().getLocation());

        container.classesDir(classesDir1);
        getComp().reload(false).expect(ComponentState.RELOADING,ComponentState.STOPPED,ComponentState.STARTING,ComponentState.STARTED);
        
        assertEquals("Dynamically reloaded component adapter has wrong classname",
                     ADAPTER_A_FULL_QUALIFIED_NAME,container.getAdapter().getClass().getName());
        assertEquals("Dynamically loaded component adapter has wroing code source",
                     classesDir1.toURI().toURL(), 
                     container.getAdapter().getClass().getProtectionDomain().getCodeSource().getLocation());
    }
    
    

    @Override
    protected ComponentTestAdapter<MockupComponent> createComponentTestAdapter() throws Exception {
        return new MockupComponentTestAdapter() {

            @Override
            protected MockupComponent createComponent() {
                DynamicMockupComponentContainerImpl ret = new DynamicMockupComponentContainerImpl(null, "mockup");
                ret.classesDir(classesDir);
                ret.classname(ADAPTER_A_FULL_QUALIFIED_NAME);
                return ret;
            }
        };
    }

    /**
     * This code generator creates code for two ComponentAdapter. Both generated
     * classes are a clone of <code>MockupComponentAdapterImpl</code>.
     */
    private class MyCodeGenerator extends AbstractCodeGenerator {

        public MyCodeGenerator(File srcDir) {
            super(srcDir);
        }

        public List<File> generateCode() throws IOException {
            
            List<File> ret = new ArrayList<File>(2);
            
            ret.add(generateCode(ADAPTER_A_NAME));
            ret.add(generateCode(ADAPTER_B_NAME));
            
            return ret;
        }

        private File generateCode(String simpleClassname) throws IOException {
            
            String fullQualifiedClassname = String.format("%s.%s", PACKAGE_NAME, simpleClassname);
            
            File file = getFileForClass(fullQualifiedClassname);
            
            StringBuilder sb = new StringBuilder();
            sb.append("test.".replace('.', File.separatorChar));
            sb.append(PACKAGE_NAME.replace('.', File.separatorChar));
            sb.append(File.separatorChar);
            sb.append(MockupComponentAdapterImpl.class.getSimpleName());
            sb.append(".java");
            
            File orgFile = new File(sb.toString());
            
            // MockupComponentContainerImpl
            
            Map<String,String> patterns = new HashMap<String,String>(2);
            patterns.put(MockupComponentAdapterImpl.class.getSimpleName(), simpleClassname);
            patterns.put(MockupComponentContainerImpl.class.getSimpleName(), DynamicMockupComponentContainerImpl.class.getSimpleName());
                    
                    Collections.<String,String>singletonMap(MockupComponentAdapterImpl.class.getSimpleName(), simpleClassname);
            FileUtil.replace(orgFile, file, patterns);
            return file;
        }
    }
}
