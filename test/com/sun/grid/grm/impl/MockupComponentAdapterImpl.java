/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.Feature;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 *  Default implementation for the MockupComponentAdapter
 */
public class MockupComponentAdapterImpl extends AbstractComponentAdapter<ExecutorService,SparePoolServiceConfig> implements MockupComponentAdapter {

    private final MockupImpl impl = new MockupImpl();
    
    public MockupComponentAdapterImpl(MockupComponentContainerImpl comp) {
        super(comp);
    }

    @Override
    public void prepareComponentStartup(SparePoolServiceConfig config) throws GrmException {
        impl.prepareComponentStartup(config);
    }
    
    
    public void startComponent(SparePoolServiceConfig config) throws GrmException {
        impl.startComponent(config);
    }
    
    @Override
    public void prepareComponentShutdown(boolean forced) throws GrmException {
        impl.prepareComponentShutdown(forced);
    }
    

    public void stopComponent(boolean forced) throws GrmException {
        impl.stopComponent(forced);
    }

    @Override
    public void prepareComponentReload(SparePoolServiceConfig config, boolean forced) throws GrmException, RestartRequiredException {
        impl.prepareComponentReload(config, forced);
    }
    
    public void reloadComponent(SparePoolServiceConfig config, boolean forced) throws GrmException {
        impl.reloadComponent(config, forced);
    }

    public <R> Future<R> submit(Callable<R> task) {
        return getExecutorService().submit(task);
    }

    public void setup(Feature f) {
        impl.setup(f);
    }

    public void teardown(Feature f) {
        impl.teardown(f);
    }

    public void waitUntil(Feature f) throws InterruptedException {
        impl.waitUntil(f);
    }


}
