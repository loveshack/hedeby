/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.DefaultComponentTestAdapter;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.Feature;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.FeatureHandler;

/**
 *   Provides the component adapter implementation for a <code>MockupComponent</code>.
 *   This component adapter supports all test features.
 */
abstract class MockupComponentTestAdapter extends DefaultComponentTestAdapter<MockupComponent> {

    public MockupComponentTestAdapter() {
        super();
        // This component adapter supports all available test features
        addFeatures(Feature.values());
    }

    /**
     * Adds a list of supported test features. The features are setup and
     * teardown with an instance of <code>MyFeatureHandler</code>.
     * Such <code>FeatureHandler</code>s delegates the <code>setup</code>
     * and <code>teardown</code> method to the component.
     * @param features list of supported features
     */
    private void addFeatures(Feature... features) {
        for (Feature f : features) {
            super.addFeature(f, new MyFeatureHandler(f));
        }
    }

    private class MyFeatureHandler implements FeatureHandler {

        private final Feature f;

        public MyFeatureHandler(Feature f) {
            super();
            this.f = f;
        }

        public void setup() {
            getComponent().setup(f);
        }

        public void teardown() {
            getComponent().teardown(f);
        }

        public void waitUntil() throws InterruptedException {
            // Nothing to wait
        }
    }
}
