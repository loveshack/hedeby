/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for compiling java code.
 * 
 * The compiler has common/build/classes and the jaxb jars in the classpath.
 */
final class Compiler {

    /**
     * Compile java code
     * @param cd  the code generator
     * @param targetDir  the target directory where the class files are stored
     * @throws java.lang.Exception
     */
    public static void compile(AbstractCodeGenerator cd, File targetDir) throws Exception {
        compile(cd.generateCode(), targetDir);
    }

    private static void compile(List<File> srcFiles, File targetDir) throws Exception {
        StringBuilder classpath = new StringBuilder();
        File hedebyClassesDir = new File("common" + File.separatorChar + "build" + File.separatorChar + "classes");

        if (!hedebyClassesDir.exists()) {
            throw new AssertionError(String.format("%s not found", hedebyClassesDir));
        }

        File testClassesDir = new File("build" + File.separatorChar + "test" + File.separatorChar + "classes");
        if (!testClassesDir.exists()) {
            throw new AssertionError(String.format("%s not found", testClassesDir));
        }
        
        // We need the classpath to JAXB
        classpath.append(hedebyClassesDir.getAbsolutePath());
        classpath.append(File.pathSeparatorChar);
        classpath.append(testClassesDir.getAbsolutePath());
        classpath.append(File.pathSeparatorChar);
        classpath.append(System.getProperty("jaxb.home") + File.separatorChar + "lib");
        
        List<String> argList = new ArrayList<String>(srcFiles.size() + 4);
        argList.add("-d");
        argList.add(targetDir.getAbsolutePath());
        argList.add("-classpath");
        argList.add(classpath.toString());

        for (File f : srcFiles) {
            argList.add(f.getAbsolutePath());
        }
        String[] args = new String[argList.size()];
        args = argList.toArray(args);
        File toolsJar = new File(System.getProperty("java.home") + File.separatorChar + ".." + File.separatorChar + "lib" + File.separatorChar + "tools.jar");
        if (!toolsJar.exists()) {
            throw new AssertionError(String.format("%s not found", toolsJar));
        }
        URL[] urls = {toolsJar.toURI().toURL()};
        URLClassLoader cl = new URLClassLoader(urls);
        Class<?> c = cl.loadClass("com.sun.tools.javac.Main");
        Object compiler = c.newInstance();
        Method compile = c.getMethod("compile", (new String[]{}).getClass());

        int result = ((Integer) compile.invoke(compiler, new Object[]{args})).intValue();
        if (result != 0) {
            throw new Exception("Compile returned " + result);
        }
    }

    /**
     * Helper class for CodeGenerators
     */
    public static abstract class AbstractCodeGenerator {

        private final File srcDir;

        /**
         * Create a new instance of <code>AbstractCodeGenerator</code>. 
         * @param srcDir  the source directory
         */
        public AbstractCodeGenerator(File srcDir) {
            this.srcDir = srcDir;
        }

        /**
         * Generate the code
         * @return the list of source files
         * @throws java.io.IOException if the source files could not be created
         */
        public abstract List<File> generateCode() throws IOException;
        
        /**
         * Get the file for a java class and create the package directories.
         * 
         * @param classname the full qualified name of the java class
         * @return the fiel for the java class
         * @throws java.io.IOException if the package directory for the java class
         *                        could not be created
         */
        protected File getFileForClass(String classname) throws IOException {

            StringBuilder path = new StringBuilder(classname.length() + 5);
            path.append(classname.replace('.', File.separatorChar));
            path.append(".java");

            File file = new File(srcDir, path.toString());
            File parent = file.getParentFile();
            if (!parent.exists()) {
                if (!parent.mkdirs()) {
                    throw new IOException(String.format("Can not create directory %s", parent));
                }
            }
            return file;
        }
    }
}
