/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.ComponentAdapterFactory;
import com.sun.grid.grm.ComponentContainer;
import com.sun.grid.grm.ComponentNotActiveException;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.Feature;
import com.sun.grid.grm.service.impl.MockupServiceContainer;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 */
public class AbstractMockupComponentContainerImpl extends AbstractComponentContainer<MockupComponentAdapter, ExecutorService, SparePoolServiceConfig> implements MockupComponent {

    private final List<Feature> pendingFeatureList = new LinkedList<Feature>();
    
    public AbstractMockupComponentContainerImpl(ExecutionEnv env, String name, 
            ComponentAdapterFactory<? extends ComponentContainer<MockupComponentAdapter, ExecutorService, SparePoolServiceConfig>,MockupComponentAdapter, ExecutorService, SparePoolServiceConfig> factory) {
        super(env, name, 
              ComponentExecutors.<MockupServiceContainer,SparePoolServiceConfig>newSingleThreadedExecutorServiceFactory(),
              factory);
    }

    /**
     * Submit a task into the <code>ExecutorService</code> of the component
     * @param <R>   the result type
     * @param task  the task
     * @return      the <code>Future</code> of the submitted task
     * @throws GrmException
     */
    public <R> Future<R> submit(Callable<R> task) throws GrmException {
        return getAdapter().submit(task);
    }

    /**
     * <code>AbstractComponentContainer</code> calls this method once the adapter
     * has been created.
     * 
     * <p>This calls overrides this method to setup the pending features for
     *    the test.</p>
     * 
     * @param adapter  the created adapter
     */
    @Override
    protected final void postCreateAdapter(MockupComponentAdapter oldAdapter, MockupComponentAdapter adapter) {
        super.postCreateAdapter(oldAdapter, adapter);
        for (Feature f : pendingFeatureList) {
            adapter.setup(f);
        }
    }

    /**
     * Setup a <code>Feature</code> for this test.
     * 
     * @param f  the feature
     */
    public void setup(Feature f) {
        // if the test calls setup before the start method is called
        // getAdapter() throws an IllegalStateException, because the
        // adapter can only be created in the startComponent method.
        // => Catch the IllegalStateException and store the enabled feature
        //    in pendingFeatureList. AbstractComponentContainer will call
        //    postCreateAdapter once the adapter has been created, the pending
        //    features will be setup in this method
        try {
            getAdapter().setup(f);
        } catch (ComponentNotActiveException ex) {
            pendingFeatureList.add(f);
        }
    }

    /**
     * Teardown a feature.
     * 
     * @param f  the feature
     */
    public void teardown(Feature f) {
        try {
            getAdapter().teardown(f);
        } catch (ComponentNotActiveException ex) {
            throw new IllegalStateException("Can not teardown feature " + f + " if component is not active");
        }
    }

    public void waitUntil(Feature f) throws InterruptedException {
        try {
            getAdapter().waitUntil(f);
        } catch (ComponentNotActiveException ex) {
            throw new IllegalStateException("Can not wait until feature " + f + " if component is not active");
        }
    }
    
    

    /**
     * Override this method to provoke that the component loads its configuration
     * from CS.
     * @return an empty spare pool service config
     */
    @Override
    protected SparePoolServiceConfig loadConfig() {
        return new SparePoolServiceConfig();
    }
}
