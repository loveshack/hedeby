/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester.Feature;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Default implementation of the <code>MockupComponent</code>.
 */
class MockupComponentImpl extends AbstractComponent<ExecutorService, SparePoolServiceConfig>
        implements MockupComponent {

    private final MockupImpl impl = new MockupImpl();
    
    /**
     * Create a new instance of MockupComponentImpl
     * @param env   the execution env of the component
     * @param name  the name of the component
     */
    public MockupComponentImpl(ExecutionEnv env, String name) {
        super(env, name, 
              ComponentExecutors.<MockupComponentImpl,SparePoolServiceConfig>newSingleThreadedExecutorServiceFactory());
    }

    /**
     * Set up a test feature for the component
     * @param f the test feature
     */
    public void setup(Feature f) {
        impl.setup(f);
    }

    /**
     * Teardown a test feature of the component
     * @param f  the test feature
     */
    public void teardown(Feature f) {
        impl.teardown(f);
    }

    public void waitUntil(Feature f) throws InterruptedException {
        impl.waitUntil(f);
    }
    
    

    /**
     * Prepare the component startup.
     * @param config
     * @throws com.sun.grid.grm.GrmException
     */
    @Override
    protected void prepareComponentStartup(SparePoolServiceConfig config) throws GrmException {
        impl.prepareComponentStartup(config);
    }

    
    /**
     * Start the component.
     * @param config   the configuration of the component
     * @throws com.sun.grid.grm.GrmException if the EXCEPTION_IN_START_COMPONENT feature is enabled
     */
    @Override
    protected void startComponent(SparePoolServiceConfig config) throws GrmException {
        impl.startComponent(config);
    }

    /**
     * Submit a task into the executor service
     * @param <R>   the result type of the task
     * @param task  the task
     * @return  the result of the task
     */
    public <R> Future<R> submit(Callable<R> task) {
        return getExecutorService().submit(task);
    }

    /**
     * Stop the component.
     * @param forced  <code>true</code> if in forced mode
     * @throws com.sun.grid.grm.GrmException if the EXCEPTION_IN_STOP_COMPONENT feature is enabled
     */
    @Override
    protected void stopComponent(boolean forced) throws GrmException {
        impl.stopComponent(forced);
    }

    
    @Override
    protected void prepareComponentReload(SparePoolServiceConfig config, boolean forced) throws GrmException, RestartRequiredException {
        impl.prepareComponentReload(config, forced);
    }
    
    /**
     * Reload the component.
     * @param config the configuration of the component
     * @param forced  <code>true</code> if in forced mode
     * @throws com.sun.grid.grm.GrmException if the EXCEPTION_IN_RELOAD_COMPONENT feature is enabled
     */
    @Override
    protected void reloadComponent(SparePoolServiceConfig config, boolean forced) throws GrmException {
        impl.reloadComponent(config, forced);
    }

    /**
     * Override the loadConfig, hence CS will be not contacted by the component
     * @return a empty spare pool service config
     */
    @Override
    protected SparePoolServiceConfig loadConfig() {
        return new SparePoolServiceConfig();
    }
}
