/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;

/**
 * ComponentContainer which loads dynamically the ComponentAdapter
 */
class DynamicMockupComponentContainerImpl extends AbstractMockupComponentContainerImpl {

    private AtomicReference<File> classesDir = new AtomicReference<File>();
    private AtomicReference<String> classname = new AtomicReference<String>();
    
    public DynamicMockupComponentContainerImpl(ExecutionEnv env, String name) {
        super(env, name, new MyComponentAdapterFactory());
    }
    
    public DynamicMockupComponentContainerImpl classesDir(File classesDir) {
        this.classesDir.set(classesDir);
        return this;
    }
    
    public DynamicMockupComponentContainerImpl classname(String classname) {
        this.classname.set(classname);
        return this;
    }
    
    private static class MyComponentAdapterFactory extends AbstractDynamicComponentAdapterFactory<DynamicMockupComponentContainerImpl, MockupComponentAdapter, ExecutorService, SparePoolServiceConfig> {

        @Override
        protected URL[] getClasspath(DynamicMockupComponentContainerImpl component, SparePoolServiceConfig config) throws GrmException {
            try {
                return new URL[]{component.classesDir.get().toURI().toURL()};
            } catch (MalformedURLException ex) {
                throw new GrmException("File.toURI has thrown an exception", ex);
            }
        }

        @Override
        protected String getAdapterClassname(DynamicMockupComponentContainerImpl component, SparePoolServiceConfig config) throws GrmException {
            return component.classname.get();
        }
    }
    
}
