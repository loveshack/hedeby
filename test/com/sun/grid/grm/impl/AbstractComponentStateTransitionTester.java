/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.*;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.util.InvalidStateTransistionException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Abstract base class for testing Component state transitions
 * 
 * @param <T> Type of the GrmComponent
 */
public abstract class AbstractComponentStateTransitionTester<T extends GrmComponent> extends TestCase {

    private final static Logger log = Logger.getLogger(AbstractComponentStateTransitionTester.class.getName());
    protected ComponentTester<T> comp;

    protected AbstractComponentStateTransitionTester(String testName) {
        super(testName);
    }

    protected abstract ComponentTestAdapter<T> createComponentTestAdapter() throws Exception;
    
    @Override
    protected void setUp() throws Exception {
        log.entering(getClass().getName(), "setUp");
        comp = createComponentTester(createComponentTestAdapter());
        log.exiting(getClass().getName(), "setUp");
    }
    
    protected ComponentTester<T> createComponentTester(ComponentTestAdapter<T> adapter) throws Exception {
        log.entering(getClass().getName(), "createComponentTester");
        ComponentTester<T> ret = new ComponentTester<T>(adapter);
        log.exiting(getClass().getName(), "createComponentTester", ret);
        return ret;
    }

    @Override
    protected void tearDown() throws Exception {
        log.entering(getClass().getName(), "tearDown");
        comp.destroy();
        log.exiting(getClass().getName(), "tearDown");
    }

    public void testStoppedStart() {
        log.entering(getClass().getName(), "testStoppedStart");
        comp.start().expect(ComponentState.STARTING, ComponentState.STARTED);
        log.exiting(getClass().getName(), "testStoppedStart");
    }
    
    public void testStoppedStartWithException() {
        log.entering(getClass().getName(), "testStoppedStartWithException");
        if(comp.isSupported(Feature.EXCEPTION_IN_PREPARE_COMPONENT_START)) {
            comp.setup(Feature.EXCEPTION_IN_PREPARE_COMPONENT_START)
                .start();
                
            comp.expect(GrmException.class);
        }
        log.exiting(getClass().getName(), "testStoppedStartWithException");
    }

    public void testStoppedStop() {
        log.entering(getClass().getName(), "testStoppedStop");
        comp.stop(false).expect(InvalidStateTransistionException.class);
        log.exiting(getClass().getName(), "testStoppedStop");
    }

    public void testStoppedStopForced() {
        log.entering(getClass().getName(), "testStoppedStopForced");
        comp.stop(true).expect(InvalidStateTransistionException.class);
        log.exiting(getClass().getName(), "testStoppedStopForced");
    }

    public void testStoppedReload() {
        log.entering(getClass().getName(), "testStoppedReload");
        comp.reload(false).expect(InvalidStateTransistionException.class);
        log.exiting(getClass().getName(), "testStoppedReload");
    }

    public void testStoppedReloadForced() {
        log.entering(getClass().getName(), "testStoppedReloadForced");
        comp.reload(true).expect(InvalidStateTransistionException.class);
        log.exiting(getClass().getName(), "testStoppedReloadForced");
    }

    public void testStartingStop() {
        log.entering(getClass().getName(), "testStartingStop");
        if(comp.isSupported(Feature.BLOCK_START_COMPONENT)) {
            comp.setup(Feature.BLOCK_START_COMPONENT)
                .start().expect(ComponentState.STARTING)
                .stop(false).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testStartingStop");
    }

    public void testStartingStopForced() {
        log.entering(getClass().getName(), "testStartingStopForced");
        if(comp.isSupported(Feature.BLOCK_START_COMPONENT)) {
            comp.setup(Feature.BLOCK_START_COMPONENT)
                .start().expect(ComponentState.STARTING)
                .stop(true).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testStartingStopForced");
    }

    public void testStartingStart() {
        log.entering(getClass().getName(), "testStartingStart");
        if (comp.isSupported(Feature.BLOCK_START_COMPONENT)) {
            comp.setup(Feature.BLOCK_START_COMPONENT)
                .start().expect(ComponentState.STARTING)
                .start().expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testStartingStart");
    }

    public void testStartingReload() {
        log.entering(getClass().getName(), "testStartingReload");
        
        if (comp.isSupported(Feature.BLOCK_START_COMPONENT)) {
            comp.setup(Feature.BLOCK_START_COMPONENT)
                .start().expect(ComponentState.STARTING)
                .reload(false).expect(InvalidStateTransistionException.class);
        }
        
        log.exiting(getClass().getName(), "testStartingReload");
    }

    public void testStartingReloadForced() {
        log.entering(getClass().getName(), "testStartingReloadForced");
        if (comp.isSupported(Feature.BLOCK_START_COMPONENT)) {
            comp.setup(Feature.BLOCK_START_COMPONENT)
                .start().expect(ComponentState.STARTING)
                .reload(true).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testStartingReloadForced");
    }

    public void testStartStart() {
        log.entering(getClass().getName(), "testStartStart");
        comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
            .start().expect(InvalidStateTransistionException.class);
        log.exiting(getClass().getName(), "testStartStart");
    }

    public void testStartStop() {
        log.entering(getClass().getName(), "testStartStop");
        comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
            .stop(false).expect(ComponentState.STOPPING, ComponentState.STOPPED);
        log.exiting(getClass().getName(), "testStartStop");
    }

    public void testStartStopForced() {
        log.entering(getClass().getName(), "testStartStopForced");
        comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
            .stop(true).expect(ComponentState.STOPPING, ComponentState.STOPPED);
        log.exiting(getClass().getName(), "testStartStopForced");
    }

    public void testStartReload() {
        log.entering(getClass().getName(), "testStartReload");
        comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
            .reload(false).expect(ComponentState.RELOADING, ComponentState.STARTED);
        log.exiting(getClass().getName(), "testStartReload");
    }

    public void testStartReloadForced() {
        log.entering(getClass().getName(), "testStartReloadForced");
        comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
            .reload(true).expect(ComponentState.RELOADING, ComponentState.STARTED);
        log.exiting(getClass().getName(), "testStartReloadForced");
    }
    
    public void testStartReloadRequiredRestart() {
        log.entering(getClass().getName(), "testStartReloadRequiredRestart");
        if (comp.isSupported(Feature.RELOAD_REQUIRES_RESTART)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.RELOAD_REQUIRES_RESTART)
                .reload(false).expect(ComponentState.RELOADING, ComponentState.STOPPED,ComponentState.STARTING,ComponentState.STARTED)
                .teardown(Feature.RELOAD_REQUIRES_RESTART);
        }
        log.exiting(getClass().getName(), "testStartReloadRequiredRestart");
    }

    public void testReloadingStop() {
        log.entering(getClass().getName(), "testReloadingStop");
        if (comp.isSupported(Feature.BLOCK_RELOAD_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_RELOAD_COMPONENT)
                .reload(false).expect(ComponentState.RELOADING)
                .stop(false).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testReloadingStop");
    }

    public void testReloadingStopForced() {
        log.entering(getClass().getName(), "testReloadingStopForced");
        if (comp.isSupported(Feature.BLOCK_RELOAD_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_RELOAD_COMPONENT)
                .reload(false).expect(ComponentState.RELOADING)
                .stop(true).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testReloadingStopForced");
    }

    public void testReloadingStart() {
        log.entering(getClass().getName(), "testReloadingStart");
        if (comp.isSupported(Feature.BLOCK_RELOAD_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_RELOAD_COMPONENT)
                .reload(false).expect(ComponentState.RELOADING)
                .start().expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testReloadingStart");
    }

    public void testReloadingReload() {
        log.entering(getClass().getName(), "testReloadingReload");
        if (comp.isSupported(Feature.BLOCK_RELOAD_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_RELOAD_COMPONENT)
                .reload(false).expect(ComponentState.RELOADING)
                .reload(false).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testReloadingReload");
    }

    public void testReloadingReloadForced() {
        log.entering(getClass().getName(), "testReloadingReloadForced");
        if (comp.isSupported(Feature.BLOCK_RELOAD_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_RELOAD_COMPONENT)
                .reload(false).expect(ComponentState.RELOADING)
                .reload(true).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testReloadingReloadForced");
    }

    public void testStoppingStop() {
        log.entering(getClass().getName(), "testStoppingStop");
        if (comp.isSupported(Feature.BLOCK_STOP_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_STOP_COMPONENT)
                .stop(false).expect(ComponentState.STOPPING)
                .stop(false).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testStoppingStop");
    }

    public void testStoppingStart() {
        log.entering(getClass().getName(), "testStoppingStart");
        if (comp.isSupported(Feature.BLOCK_STOP_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_STOP_COMPONENT)
                .stop(false).expect(ComponentState.STOPPING)
                .start().expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testStoppingStart");
    }

    public void testStoppingReload() {
        log.entering(getClass().getName(), "testStoppingReload");
        if (comp.isSupported(Feature.BLOCK_STOP_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_STOP_COMPONENT)
                .stop(false).expect(ComponentState.STOPPING)
                .reload(false).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testStoppingReload");
    }

    public void testStoppingReloadForced() {
        log.entering(getClass().getName(), "testStoppingReloadForced");
        if (comp.isSupported(Feature.BLOCK_STOP_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_STOP_COMPONENT)
                .stop(false).expect(ComponentState.STOPPING)
                .reload(true).expect(InvalidStateTransistionException.class);
        }
        log.exiting(getClass().getName(), "testStoppingReloadForced");
    }

    public void testStoppingStopForced() {
        log.entering(getClass().getName(), "testStoppingStopForced");
        if (comp.isSupported(Feature.BLOCK_STOP_COMPONENT)) {
            comp.start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .setup(Feature.BLOCK_STOP_COMPONENT)
                .stop(false).expect(ComponentState.STOPPING)
                .stop(true);
            comp.expect(ComponentState.STOPPED);
        }
        log.exiting(getClass().getName(), "testStoppingStopForced");
    }
    
    public void testExceptionInStartComponent() {
        log.entering(getClass().getName(), "testExceptionInStartComponent");
        if(comp.isSupported(Feature.EXCEPTION_IN_START_COMPONENT)) {
            comp.setup(Feature.EXCEPTION_IN_START_COMPONENT)
                .start().expect(ComponentState.STARTING, ComponentState.STOPPED);
        }
        log.exiting(getClass().getName(), "testExceptionInStartComponent");
    }
    
    public void testExceptionInStopComponent() {
        log.entering(getClass().getName(), "testExceptionInStopComponent");
        if(comp.isSupported(Feature.EXCEPTION_IN_STOP_COMPONENT)) {
            comp.setup(Feature.EXCEPTION_IN_STOP_COMPONENT)
                .start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .stop(false).expect(ComponentState.STOPPING, ComponentState.STOPPED);
        }
        log.exiting(getClass().getName(), "testExceptionInStopComponent");
    }
    
    public void testExceptionInReloadComponent() {
        log.entering(getClass().getName(), "testExceptionInReloadComponent");
        if(comp.isSupported(Feature.EXCEPTION_IN_RELOAD_COMPONENT)) {
            comp.setup(Feature.EXCEPTION_IN_RELOAD_COMPONENT)
                .start().expect(ComponentState.STARTING, ComponentState.STARTED)
                .reload(false).expect(ComponentState.RELOADING, ComponentState.STOPPED);
        }
        log.exiting(getClass().getName(), "testExceptionInReloadComponent");
    }
    
    protected ComponentTester<T> getComp() {
        return this.comp;
    }
    
    public enum Feature {
        BLOCK_START_COMPONENT,
        BLOCK_STOP_COMPONENT,
        BLOCK_RELOAD_COMPONENT,
        RELOAD_REQUIRES_RESTART,
        EXCEPTION_IN_START_COMPONENT,
        EXCEPTION_IN_PREPARE_COMPONENT_START,
        EXCEPTION_IN_STOP_COMPONENT,
        EXCEPTION_IN_PREPARE_COMPONENT_SHUTDOWN,
        EXCEPTION_IN_RELOAD_COMPONENT,
    }
    
    public interface FeatureHandler {
        public void setup() throws GrmException;
        public void teardown() throws GrmException;
        public void waitUntil()throws InterruptedException;
    }
    
    
    public interface ComponentTestAdapter<T extends GrmComponent> {
        public FeatureHandler getFeature(Feature feature);
        public T getComponent();
    }
    
    
    public abstract static class DefaultComponentTestAdapter<T extends GrmComponent> implements ComponentTestAdapter<T> {
        
        private final Map<Feature,FeatureHandler> featureMap = new HashMap<Feature,FeatureHandler>();
        private T comp;
        
        public DefaultComponentTestAdapter() {
        }
        
        public FeatureHandler getFeature(Feature feature) {
            return featureMap.get(feature);
        }
        
        protected void addFeature(Feature feature, FeatureHandler handler) {
            featureMap.put(feature, handler);
        }
        protected abstract T createComponent();
        
        public synchronized T getComponent() {
            if (comp == null) {
                comp = createComponent();
            }
            return comp;
        }
        
        public synchronized void clearComponent() {
            comp = null;
        }
    }
    
    public static class ComponentTester<T extends GrmComponent> implements ComponentEventListener {

        protected final Lock lock = new ReentrantLock();
        protected final Condition changedCondition = lock.newCondition();
        private final LinkedList<ComponentState> stateHistory = new LinkedList<ComponentState>();
        protected final LinkedList<Throwable> errorHistory = new LinkedList<Throwable>();
        private final ComponentTestAdapter<T> componentAdapter;

        public ComponentTester(ComponentTestAdapter<T> componentAdapter) throws GrmRemoteException {
            this.componentAdapter = componentAdapter;
            this.componentAdapter.getComponent().addComponentEventListener(this);
        }
        
        public ComponentTestAdapter<T> getAdapter() {
            return componentAdapter;
        }

        public T getComponent() {
            return componentAdapter.getComponent();
        }
        public void destroy() throws Exception {

            Field field = AbstractComponent.class.getDeclaredField("executor");

            AbstractComponent.ExecutorServiceProxy ex = null;
            field.setAccessible(true);
            try {
                ex = (AbstractComponent.ExecutorServiceProxy) field.get(componentAdapter.getComponent());
            } finally {
                field.setAccessible(false);
            }
            
            
            if (ex != null) {
                ex.shutdownNow();
            }
        }

        protected void addError(Throwable t) {
            lock.lock();
            try {
                errorHistory.add(t);
            } finally {
                lock.unlock();
            }
            log.log(Level.FINE,"Got error:" + t.getLocalizedMessage(), t);
        }

        public ComponentTester<T> start() {
            try {
                componentAdapter.getComponent().start();
            } catch (Throwable t) {
                addError(t);
            }
            return this;
        }

        public ComponentTester<T> stop(boolean forced) {
            try {
                componentAdapter.getComponent().stop(forced);
            } catch (Throwable t) {
                addError(t);
            }
            return this;
        }

        public ComponentTester<T> reload(boolean forced) {
            try {
                componentAdapter.getComponent().reload(forced);
            } catch (Throwable t) {
                addError(t);
            }
            return this;
        }

        public ComponentTester<T> expect(Class<? extends Throwable> cls) {
            lock.lock();
            try {
                if (errorHistory.isEmpty()) {
                    fail("expected error " + cls.getName() + " did not occur");
                } else if (cls.equals(errorHistory.getFirst().getClass())) {
                    errorHistory.remove();
                } else {
                    log.log(Level.WARNING, "Got unexpected error" + errorHistory.getFirst().getClass(), errorHistory.getFirst());
                    fail("Got unexpected error " + errorHistory.getFirst().getClass() + ", expected was " + cls.getName());
                }
            } finally {
                lock.unlock();
            }
            return this;
        }

        public void assertNextState(ComponentState state, long timeout) {
            long endTime = System.currentTimeMillis() + timeout;
            lock.lock();
            try {
                while (stateHistory.isEmpty()) {
                    long rest = endTime - System.currentTimeMillis();
                    if (rest <= 0) {
                        fail("component did not reach state " + state + " (timeout " + timeout + "ms)");
                    } else {
                        changedCondition.await(rest, TimeUnit.MILLISECONDS);
                    }
                }
                assertEquals(state, stateHistory.remove());
                log.log(Level.FINE, "component reached expected component state " + state);
            } catch (InterruptedException ex) {
                fail("Waiting for component state " + state + " has been interrupted");
            } finally {
                lock.unlock();
            }
        }
        public final static long TIMEOUT = 1000000L;
        
        public ComponentTester<T> expect(ComponentState... states) {
            assertEvents(TIMEOUT, states);
            return this;
        }
        

        public void assertEvents(long timeout, ComponentState... states) {
            long endTime = System.currentTimeMillis() + timeout;

            for (int i = 0; i < states.length; i++) {
                ComponentState state = states[i];
                lock.lock();
                try {
                    if (!errorHistory.isEmpty()) {
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        pw.println("component state changed events expected, but I got errors:");
                        for(Throwable t: errorHistory) {
                            t.printStackTrace(pw);
                            pw.println();
                        }
                        pw.flush();
                        fail(sw.getBuffer().toString());
                    }
                    long rest = endTime - System.currentTimeMillis();
                    if (rest <= 0) {
                        fail("component did not reach state " + state + " (timeout " + timeout + "ms)");
                    }
                    assertNextState(state, rest);
                } finally {
                    lock.unlock();
                }
            }
        }

        private void stateChanged(ComponentState state) {
            lock.lock();
            try {
                stateHistory.add(state);
                changedCondition.signalAll();
            } finally {
                lock.unlock();
            }
        }

        public void componentUnknown(ComponentStateChangedEvent event) {
            stateChanged(ComponentState.UNKNOWN);
        }

        public void componentStarting(ComponentStateChangedEvent event) {
            stateChanged(ComponentState.STARTING);
        }

        public void componentStarted(ComponentStateChangedEvent event) {
            stateChanged(ComponentState.STARTED);
        }

        public void componentStopping(ComponentStateChangedEvent event) {
            stateChanged(ComponentState.STOPPING);
        }

        public void componentStopped(ComponentStateChangedEvent event) {
            stateChanged(ComponentState.STOPPED);
        }

        public void componentReloading(ComponentStateChangedEvent event) {
            stateChanged(ComponentState.RELOADING);
        }

        public void connectionClosed() {
            log.log(Level.FINE, "Connection closed");
        }

        public void connectionFailed() {
            log.log(Level.FINE, "Connection failed");
        }

        public void eventsLost() {
            log.log(Level.FINE, "events lost");
        }

        public boolean isSupported(Feature feature) {
            return componentAdapter.getFeature(feature) != null;
        }
        public ComponentTester<T> setup(Feature feature) {
            try {
                componentAdapter.getFeature(feature).setup();
            } catch (GrmException ex) {
                addError(ex);
            }
            return this;
        }
        public ComponentTester<T> teardown(Feature feature) {
            try {
                componentAdapter.getFeature(feature).teardown();
            } catch (GrmException ex) {
                addError(ex);
            }
             return this;
        }
    }

}
