/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import com.sun.grid.grm.GrmRemoteException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The interface TestComponent is used to define TestComponent objects that are used to test the JMX Communication
 * It consists of a setter and a getter for a String member and it enforces ListenerManagement
 */
public interface TestComponent {

    public final static Logger log = Logger.getLogger(TestComponent.class.getName());

    public void setA(String a);

    public String getA();
    
    /**
     * The call of this method blocks the calling thread
     * until the <code>sleeptime</code> has elapsed
     * 
     * @param sleeptime sleeptime in millis
     * @throws java.lang.InterruptedException if the calling thread is interrupted 
     */
    public void sleep(int sleeptime) throws InterruptedException;
    
    /**
     * Wait until another thread has called the sleep method
     * @param timeout  max wait time
     * @return <code>true</code> if a thread is blocked in the sleep method
     * @throws java.lang.InterruptedException
     */
    public boolean waitUntilSleeping(int timeout) throws InterruptedException;
    
    /**
     * The call of this method wakes up a thread which has been blocked
     * in the sleep method.
     * 
     * @return <code>true</code> if the call of the method wake up a thread
     */
    public boolean wakeup();

    public void addTestComponentListener(TestComponentListener listener);

    /**
     * Remove a listener from the test component
     * @param listener the listener
     * @throws com.sun.grid.grm.GrmRemoteException should be declared for all 
     *             remote methods otherwise you get an UndeclaredThrowableException 
     */
    public void removeTestComponentListener(TestComponentListener listener) throws GrmRemoteException;

    /**
     * The implementation of the dummy interface A
     */
    public static class TestComponentImpl implements TestComponent {

        private String a;
        private int sequenceNumber;
        private final List<TestComponentListener> listeners = new LinkedList<TestComponentListener>();

        public void setA(String a) {
            int num = 0;
            synchronized (this) {
                this.a = a;
                num = sequenceNumber;
                sequenceNumber++;
            }
            for (TestComponentListener lis : listeners) {
                lis.testComponentChanged(num, a);
            }
        }

        public String getA() {
            return a;
        }
        
        private boolean sleeping;
        private boolean wokeup;
        public synchronized void sleep(int millis) throws InterruptedException {
            try {
                sleeping = true;
                this.notifyAll();
                this.wait(millis);
            } finally {
                wokeup = true;
                this.notifyAll();
            }
        }
        
        public  boolean waitUntilSleeping(int millis) throws InterruptedException {
            long endTime = System.currentTimeMillis() + millis;
            synchronized(this) {
                while(true) {
                    if (sleeping) {
                        return true;
                    }
                    if (wokeup) {
                        return false;
                    }
                    long rest = endTime - System.currentTimeMillis();
                    if (rest > 0) {
                        wait(rest);
                    } else {
                        return false;
                    }
                }
            }
        }
        

        public synchronized boolean wakeup() {
            if (sleeping && !wokeup) {
                this.notifyAll();
                return true;
            } else {
                return false;
            }
        }

        public void addTestComponentListener(TestComponentListener listener) {
            log.log(Level.FINE, "Adding TestComponentListener");
            listeners.add(listener);
        }

        public void removeTestComponentListener(TestComponentListener listener) {
            log.log(Level.FINE, "Removing TestComponentListener");
            listeners.remove(listener);
        }
    }

    @ManagedEventListener
    /**
     * This interface describes a listener to check if "a" member of TestComponent is remotely changed
     */
    public interface TestComponentListener {

        /**
         * is called if the member a is changed
         * @param sequenceNumber should be used as a incrementing counter for the amount of calls
         * @param a should be the new value of a
         */
        public void testComponentChanged(int sequenceNumber, String a);
    }

    public static class TestComponentListenerImpl implements TestComponentListener {

        private String a;

        /**
         * is called if the member a is changed
         * @param sequenceNumber should be used as a incrementing counter for the amount of calls
         * @param a should be the new value of a
         */
        public void testComponentChanged(int sequenceNumber, String a) {
            log.log(Level.FINE, "aChanged[{0}]: {1} -> {2}", new Object[]{sequenceNumber, this.a, a});
            synchronized(this){
                this.a = a;
                notifyAll();
            }
        }

        public String getA() {
            return a;
        }

        /**
         * This method lets the calling thread wait until the listener get a notification that the expected value was set.
         * @param a the value that has been set in the observed A object
         * @param timeout after this time waiting is given up and the thread is released
         * @return if expected value has been set 
         * @throws java.lang.InterruptedException
         */
        public boolean waitFor(String a, long timeout) throws InterruptedException {
            long start = System.currentTimeMillis();
            log.log(Level.FINER, "waiting max " + timeout + " seconds for " + a);
            try {
                while (true) {
                    synchronized (this) {
                        if (a.equals(this.a)) {
                            return true;
                        }
                        long rest =  (start + timeout)-System.currentTimeMillis();
                        if (rest <= 0) {
                            return false;
                        }
                        wait(rest);
                    }
                }
            } finally {
                log.log(Level.FINE, "It took " + (System.currentTimeMillis() - start) + " ms to finish to wait for state change to " + a + "!");
            }
        }
    }
}