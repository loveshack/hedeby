/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.util.Hostname;
import java.util.LinkedList;
import java.util.List;

public class ComponentWrapperTest extends CSMockupTestCase {

    private ExecutionEnv env;
    private ComponentWrapper<A> aWrapper;
    private A aProxy;

    /** Creates a new instance of ComponentWrapperTest */
    public ComponentWrapperTest(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        env = getEnv();
        setupActiveJVM("executor_vm");
        start();

        AImpl aImpl = new AImpl();
        ComponentInfo ci = new ComponentInfo(Hostname.getLocalHost(), "executor_vm", ComponentInfo.createObjectName(env, "a", A.class));
        ActiveComponent ac = ci.toActiveComponent();
        env.getContext().bind(BootstrapConstants.CS_ACTIVE_COMPONENT + "." + ci.getIdentifier(), ac);
        aWrapper = new ComponentWrapper<A>(aImpl, A.class, ci.getObjectName());
        getMBeanServer().registerMBean(aWrapper, aWrapper.getObjectName());
        aProxy = ComponentProxy.<A>newProxy(env, ci);
    }

    @Override
    protected void tearDown() throws Exception {
        getMBeanServer().unregisterMBean(aWrapper.getObjectName());
        super.tearDown();
    }

    public void testAttributes() throws Exception {

        aProxy.setA("a");
        assertEquals("a", aProxy.getA());

        aProxy.setB(true);
        assertTrue(aProxy.getB());
    }

    public void testInvoke() throws Exception {

        aProxy.a("a", true);
        assertEquals("a", aProxy.getA());
        assertTrue(aProxy.getB());

        aProxy.a("b");
        assertEquals("b", aProxy.getA());

        aProxy.a(false);
        assertFalse(aProxy.getB());

    }

    public void testEvents() throws Exception {
        EventCounter eventCounter = new EventCounter();

        aProxy.addAListener(eventCounter);

        aProxy.setA("a");
        assertTrue(eventCounter.waitForA("a"));

        aProxy.setB(true);
        assertTrue(eventCounter.waitForB(true));


        aProxy.a("b", false);
        assertTrue(eventCounter.waitForB(false));
        assertTrue(eventCounter.waitForA("b"));
    }

    public interface A {

        public void setA(String a);

        public String getA();

        public void setB(boolean b);

        public boolean getB();

        public void a(String a, boolean b);

        public void a(String a);

        public void a(boolean b);

        public void addAListener(AListener listener);

        public void removeAListener(AListener listener);
    }

    @ManagedEventListener
    public interface AListener {

        public void aChanged(String a);

        public void bChanged(boolean b);

        public void allChanged(String a, boolean b);
    }

    public class AEvent {
    }

    public static class AImpl implements A {

        private String a;
        private boolean b;

        public void setA(String a) {
            this.a = a;
            for (AListener lis : listeners) {
                lis.aChanged(a);
            }
        }

        public String getA() {
            return a;
        }

        public void setB(boolean b) {
            this.b = b;
            for (AListener lis : listeners) {
                lis.bChanged(b);
            }
        }

        public boolean getB() {
            return b;
        }

        public void a(String a, boolean b) {
            this.a = a;
            this.b = b;
            for (AListener lis : listeners) {
                lis.allChanged(a, b);
            }

        }

        public void a(String a) {
            setA(a);
        }

        public void a(boolean b) {
            setB(b);
        }
        private List<AListener> listeners = new LinkedList<AListener>();

        public void addAListener(AListener listener) {
            listeners.add(listener);
        }

        public void removeAListener(AListener listener) {
            listeners.remove(listener);
        }
    }

    class EventCounter implements AListener {

        private String a;
        private boolean b;

        public synchronized void aChanged(String a) {
            this.a = a;
            notifyAll();
        }

        public synchronized void bChanged(boolean b) {
            this.b = b;
            notifyAll();
        }

        public synchronized boolean waitForA(String a) throws InterruptedException {
            long endTime = System.currentTimeMillis() + 5000;
            while (endTime > System.currentTimeMillis() && !a.equals(this.a)) {
                wait(5000);
            }
            return a.equals(this.a);
        }

        public synchronized boolean waitForB(boolean b) throws InterruptedException {
            long endTime = System.currentTimeMillis() + 5000;
            while (endTime > System.currentTimeMillis() && b != this.b) {
                wait(5000);
            }
            return this.b == b;
        }

        public synchronized void allChanged(String a, boolean b) {
            this.a = a;
            this.b = b;
            notifyAll();
        }
    }
}
