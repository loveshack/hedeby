/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.TestUtil;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.Serializable;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.rmi.ConnectIOException;
import java.rmi.server.RMIClientSocketFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.remote.JMXConnector;
import javax.management.remote.rmi.RMIConnectorServer;

/**
 *
 */
public class ConnectorTest extends CSMockupTestCase {

    private final static Logger log = Logger.getLogger(ConnectorTest.class.getName());
    
    public ConnectorTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {


        Map<String, Object> jmxEnv = new HashMap<String, Object>(3);
        // place a MockupRmiClientSocketFactory for later communication manipulation purpose 
        jmxEnv.put(RMIConnectorServer.RMI_CLIENT_SOCKET_FACTORY_ATTRIBUTE, new BlockingRmiClientSocketFactory());

        super.setUp(jmxEnv);
        super.start();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testConnect() throws Exception {
        Connector connector = new Connector(Connector.DEFAULT_CONNECTION_TIMEOUT);
        try {
            JMXConnector jmxConn = connector.connect(getEnv(), getEnv().getCSURL());
            jmxConn.close();
        } finally {
            connector.close();
        }
    }
    
    public void testConnectionTimeout() throws Exception {
        Connector connector = new Connector(2);
        BlockingSocket.setBlocked(true);
        try {
            JMXConnector jmxConn = connector.connect(getEnv(), getEnv().getCSURL());
            try {
                fail("socket connect is blocked, connect must fail with SocketTimeoutException");
            } finally {
                jmxConn.close();
            }
        } catch(SocketTimeoutException ex) {
            TestUtil.assertMatchingError("connector.ex.timeout", Connector.BUNDLE, ex);
        } finally {
            BlockingSocket.setBlocked(false);
            connector.close();
        }
    }
    
    public void testConnectInterrupted() throws Exception {
        Connector connector = new Connector(Connector.DEFAULT_CONNECTION_TIMEOUT);
        BlockingSocket.setThrowInterruptedException(true);
        try {
            JMXConnector jmxConn = connector.connect(getEnv(), getEnv().getCSURL());
            try {
                fail("Expected InterruptedIOException during connect");
            } finally {
                jmxConn.close();
            }
        } catch(ConnectIOException ex) {
            assertEquals("Expected InterruptedIOException", ex.getCause().getClass(), InterruptedIOException.class);
            // Sometimes it happen the the InterruptedIOException comes from the socket
            // directly and not from the Connector class. In this case the message
            // is null
            // => Check the error message only if it is not null
            if (ex.getCause().getLocalizedMessage() != null) {
                TestUtil.assertMatchingError("connector.ex.interrupted", Connector.BUNDLE, ex.getCause());
            }
        } finally {
            BlockingSocket.setThrowInterruptedException(false);
            connector.close();
        }
    }

    public void testConnectRuntimeException() throws Exception {
        Connector connector = new Connector(Connector.DEFAULT_CONNECTION_TIMEOUT);
        BlockingSocket.setThrowRuntimeException(true);
        try {
            JMXConnector jmxConn = connector.connect(getEnv(), getEnv().getCSURL());
            try {
                fail("Expected RuntimeException during connect");
            } finally {
                jmxConn.close();
            }
        } catch(IOException ex) {
            assertEquals("Expected an RuntimeException", ex.getCause().getClass(), RuntimeException.class);
        } finally {
            BlockingSocket.setThrowRuntimeException(false);
            connector.close();
        }
    }
    
    public void testDefaultConnectionTimeout() throws Exception {
        assertEquals(Connector.DEFAULT_CONNECTION_TIMEOUT, Connector.getConnectionTimeoutFromSystemProperty());
    }


    private void testConnectionTimeoutFromSysProp(int value, int expectedValue) {
        String orgValue = System.getProperty(Connector.CONNECTION_TIMEOUT_SYSPROP_NAME);
        if (orgValue == null) {
            orgValue = "";
        }
        System.setProperty(Connector.CONNECTION_TIMEOUT_SYSPROP_NAME, Integer.toString(value));
        try {
            assertEquals("Expected different connection timeout", expectedValue, Connector.getConnectionTimeoutFromSystemProperty());
        } finally {
            System.setProperty(Connector.CONNECTION_TIMEOUT_SYSPROP_NAME, orgValue);
        }
    }
    
    public void testConnectionTimeoutFromSysProp_One() throws Exception {
        testConnectionTimeoutFromSysProp(1, 1);
    }
    
    public void testConnectionTimeoutFromSysProp_Zero() throws Exception {
        testConnectionTimeoutFromSysProp(0, Connector.DEFAULT_CONNECTION_TIMEOUT);
    }

    public void testConnectionTimeoutFromSysProp_Negative() throws Exception {
        testConnectionTimeoutFromSysProp(-1, Connector.DEFAULT_CONNECTION_TIMEOUT);
    }
    
    public static class BlockingRmiClientSocketFactory implements RMIClientSocketFactory, Serializable {

        private static final long serialVersionUID = -2009052601L;

                
        public Socket createSocket(String host, int port) throws IOException {
            return new BlockingSocket(host, port);
        }

    }
    
    public static class BlockingSocket extends Socket {

        private final static Lock lock = new ReentrantLock();
        private final static Condition blockedCondition = lock.newCondition();
        
        private static boolean blocked;
        private static boolean throwInterruptedException;
        private static boolean throwRuntimeException;
        
        public static boolean isThrowInterruptedException() {
            return throwInterruptedException;
        }

        public static void setThrowInterruptedException(boolean aThrowInterruptedException) {
            lock.lock();
            try {
                throwInterruptedException = aThrowInterruptedException;
                blockedCondition.signalAll();
            } finally {
                lock.unlock();
            }
        }

        public static boolean isThrowRuntimeException() {
            return throwRuntimeException;
        }

        public static void setThrowRuntimeException(boolean aThrowRuntimeException) {
            lock.lock();
            try {
                throwRuntimeException = aThrowRuntimeException;
                blockedCondition.signalAll();
            } finally {
                lock.unlock();
            }
        }
        
        public BlockingSocket(String host, int port) throws IOException {
            super(host, port);
        }
        
        private static void waitUntilReleased() throws IOException {
            lock.lock();
            try {
                if (throwInterruptedException) {
                    throw new InterruptedIOException();
                }
                if (throwRuntimeException) {
                    throw new RuntimeException();
                }
                while(blocked) {
                    try {
                        blockedCondition.await();
                    } catch (InterruptedException ex) {
                        log.log(Level.WARNING, "interrupted");
                    }
                }
            } finally {
                lock.unlock();
            }
        }

        static void setBlocked(boolean flag) {
            lock.lock();
            try {
                blocked = flag;
                blockedCondition.signalAll();
            } finally {
                lock.unlock();
            }
        }

        @Override
        public void connect(SocketAddress endpoint) throws IOException {
            waitUntilReleased();
            super.connect(endpoint);
        }

        @Override
        public void connect(SocketAddress endpoint, int timeout) throws IOException {
            waitUntilReleased();
            super.connect(endpoint, timeout);
        }

    }
}
