/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import com.sun.grid.grm.CSMockupTestCase;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.management.TestComponent.TestComponentImpl;
import com.sun.grid.grm.management.TestComponent.TestComponentListenerImpl;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ComponentProxyTest extends CSMockupTestCase {

    private final static Logger log = Logger.getLogger(ComponentProxyTest.class.getName());
    private ExecutionEnv env;
    private ComponentWrapper<TestComponent> testComponentWrapper;
    private TestComponent initialTestComponent;

    public ComponentProxyTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {

        super.setUp(null);
        env = getEnv();
        start();

        initialTestComponent = new TestComponentImpl();
        ComponentInfo ci = new ComponentInfo(env.getCSHost(), "executor_vm", ComponentInfo.createObjectName(env, "a", TestComponent.class));
        testComponentWrapper = new ComponentWrapper<TestComponent>(initialTestComponent, TestComponent.class, ci.getObjectName());
        ActiveComponent ac = ci.toActiveComponent();
        env.getContext().bind(BootstrapConstants.CS_ACTIVE_COMPONENT + "." + ci.getIdentifier(), ac);

        setupActiveJVM("executor_vm");
        getMBeanServer().registerMBean(testComponentWrapper, testComponentWrapper.getObjectName());
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    /*
     * TODO: This test is failing, once we restart the MbeanServer we can use the proxy but
     * we lose registration of listeners
     */
    public void testRestart() throws Exception {

        TestComponent receivedTestComponent = ComponentService.<TestComponent>getComponentByType(env, TestComponent.class);

        TestComponentListenerImpl testComponentListener = new TestComponentListenerImpl();

        receivedTestComponent.addTestComponentListener(testComponentListener);

        log.log(Level.FINE, "a.setA(\"a\")");
        receivedTestComponent.setA("a");

        assertEquals("a", initialTestComponent.getA());
        assertTrue("aListener did not get changed event", testComponentListener.waitFor("a", 5000));


        log.log(Level.FINE, "stop mbean server");
        getMBeanServer().unregisterMBean(testComponentWrapper.getObjectName());
        testComponentWrapper.resetEventListeners();

        resetMBeanAndJMXConnectorServer(null);

        getMBeanServer().registerMBean(testComponentWrapper, testComponentWrapper.getObjectName());
        // Use the old proxy. It must reconnect
        log.log(Level.FINE, "a.setA(\"b\")");
        receivedTestComponent.setA("b");
        //a.addAListener(aLis);
        assertEquals("b", initialTestComponent.getA());

        // Assert that the event listener registration got not lost - TODO we lost registration
        log.log(Level.FINE, "wait for b");
        assertTrue("aListener did not get changed event - we lost registration", testComponentListener.waitFor("b", 5000));

        receivedTestComponent.removeTestComponentListener(testComponentListener);
        receivedTestComponent.setA("c");
        assertFalse(testComponentListener.waitFor("c", 1000));
        getMBeanServer().unregisterMBean(testComponentWrapper.getObjectName());
    }
    
    
    /**
     * This test checks that the deadlock which has been reported with issue 643
     * does no longer occur.
     * 
     * <p>The deadlock occurred if a call of a remote method had to lookup the
     *    active_vm entry from CS while a another thread was waiting for the
     *    result of a remote method call.</p>
     * @throws java.lang.Exception
     */
    public void test_issue_643() throws Exception {
        
        final  TestComponent comp = ComponentService.<TestComponent>getComponentByType(env, TestComponent.class);
        final  int sleepTime = 10000;
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    comp.sleep(sleepTime);
                } catch (Exception ex) {
                    log.log(Level.WARNING, ex.getLocalizedMessage(), ex);
                }
            }
        };
        
        t.start();
        // A parallel call to the component must not block
        assertTrue("component must sleep now", comp.waitUntilSleeping(2000));
        
        // Thread thread t is now waiting for the end of the sleep operation
        // => clear the jvm info, the next call must lookup the jvm again
        ConnectionPool.getInstance(env).clearActiveJVMInfo();
        
        // The comp.wakeup call must wakeup the component, otherwise the
        // sleep operation ended before the wakeup operation could be executed
        // This would means that both operations has been executed seriell and
        // not parallel
        assertTrue("wakeup call must wakeup the component", comp.wakeup());
    }
    
    /**
     *  This method tests that issue 705 is fixed.
     * 
     *  Issue 705 describes the problem that an InstanceNotFoundException is thrown
     *  by ComponentProxy when removing an event listener from an MBean that does
     *  no longer exist. The exception had been thrown from
     *  <tt>ComponentProxy#createMethodInvocationHandler</tt> ComponentProxy has
     *  internally a cache for MethodInvocationHandlers. The
     *  createMethodInvocationHandler method is only called if the handler is
     *  not found in the cache. Therefore the test must clear the cache before.
     *
     *  This exception damages the internal book keeping of the event listeners in
     *  ConnectionProxy$NotificationForwarder. If the remote MBean does no longer
     *  exist the listener must be removed from the internal book keeping. No exception
     *  should be thrown.
     * 
     *  To test regression of issue 705 this test does the following steps:
     * 
     *  1. Create a ComponentProxy for a test component
     *  2. Add a listener to the test component
     *  3. Remove the MBean behind the test component from MBean server
     *  4. Clean the method invocation cache in ComponentProxy
     *  5. Clear the MBeanInfo in ComponentProxy
     *  6. Try to remove the listener from the component, must work without exception
     * @throws Exception 
     */
    public void testRemoveListenerFromRemovedComponent() throws Exception {
        log.entering(ComponentProxyTest.class.getName(), "testRemoveListenerFromRemovedComponent");
        ComponentInfo ci = ComponentService.getComponentInfoByType(env, TestComponent.class);

        ComponentProxy<TestComponent> cp = new ComponentProxy<TestComponent>(env, ci, null);
        final TestComponent comp = cp.getProxy();
        
        final TestComponentListenerImpl lis = new TestComponentListenerImpl();

        comp.addTestComponentListener(lis);
        
        getMBeanServer().unregisterMBean(testComponentWrapper.getObjectName());
        
        ComponentProxy.clearMethodInvocationHandlerCache();
        cp.clearMBeanInfo();
        
        try {
            comp.removeTestComponentListener(lis);
        } catch(GrmRemoteException ex) {
            fail("Regression of issue 705, removing the event listener from a removed mbean did not work");
        }
        log.exiting(ComponentProxyTest.class.getName(), "testRemoveListenerFromRemovedComponent");
    }
    
}
