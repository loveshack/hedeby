/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.executor.RemoteFile;
import com.sun.grid.grm.executor.impl.ShellCommand;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 */
public class ScriptingStepTest extends TestCase {

    private final static Logger log = Logger.getLogger(ScriptingStepTest.class.getName());
    
    public ScriptingStepTest(String testName) {
        super(testName);
    }

    public void testParameterReplacement() throws Exception {

        File file = new File("test/com/sun/grid/grm/gef/script.sh");

        Map<String,Object> params = new HashMap<String,Object>();
        String fooValue = "fooIsReplaced";
        params.put("FOO", fooValue);

        ShellCommand cmd = ScriptingStep.createShellCommandFromFile(file, log, params);

        assertNotNull("the shell command must have a script", cmd.getScript());

        String scriptContent = cmd.getScript().getContentAsString();

        assertTrue("The script must contain the value of parameter FOO", scriptContent.contains(fooValue));

        assertEquals("Invalid number of additional files for shell command ", 1, cmd.getFiles().size());

        RemoteFile addFile = cmd.getFiles().get(0);
        assertEquals("Invalid remote file name for included file", "include.sh", addFile.getRemoteFilename());

        // TODO: check that correct number and type of warnings are created.

    }
}
