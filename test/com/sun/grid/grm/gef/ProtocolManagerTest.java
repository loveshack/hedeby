/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.resource.impl.ResourceIdImpl;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import junit.framework.TestCase;

/**
 *
 */
public class ProtocolManagerTest extends TestCase {

    private ProtocolManager pm;
    
    public ProtocolManagerTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        File protocolDir = TestUtil.getTmpDir(this);
        if (!protocolDir.exists()) {
            if (!protocolDir.mkdirs()) {
                throw new IOException("Can not create tmp file " + protocolDir);
            }
        }
        pm = ProtocolManager.getInstance(protocolDir);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        pm.clear();
        Platform.getPlatform().removeDir(pm.getProtocolDir(), true);
    }

    private void log(GefAction action) {
            Handler hd = pm.createHandler(action, new ResourceIdImpl(0));
            hd.setLevel(Level.ALL);
            LogRecord lr = new LogRecord(Level.SEVERE, "test");
            hd.publish(lr);
            hd.close();
    }

    public void testUniqueProtocolForSameAction() throws Exception {

        ExecutorService exe = Executors.newCachedThreadPool();
        final int max = ProtocolManager.MAX_PROTOCOL_FILES;
        final GefAction action = NullAction.getInstance();

        try {

            List<Future<Void>> tasks = new ArrayList<Future<Void>>(max);
            for(int i = 0; i < max; i++) {
               tasks.add(exe.submit(new Callable<Void>() {

                    public Void call() throws Exception {
                        Handler hd = pm.createHandler(action, new ResourceIdImpl(0));
                        hd.setLevel(Level.ALL);
                        LogRecord lr = new LogRecord(Level.SEVERE, "test");
                        hd.publish(lr);
                        hd.close();
                        return null;
                    }
                }));
            }

            for(Future<Void> task: tasks) {
                task.get();
            }
        } finally {
            exe.shutdown();
            assertTrue("Executor must terminate", exe.awaitTermination(10, TimeUnit.SECONDS));
        }
        
        SortedSet<File> files = pm.getAllProtocols(action);
        assertEquals("Invalid number of protocol files in the protocol directory " + pm.getProtocolDir(), max, files.size());
    }

    
    public void testCleanupOfOutdatedProtocols() throws Exception {

        final int max = ProtocolManager.MAX_PROTOCOL_FILES;
        final GefAction action = NullAction.getInstance();
        for (int i = 0; i < max; i++) {
            log(action);
            // sleep one second => we want to be able to identify the oldest file uniquely
            Thread.sleep(1001);
        }

        SortedSet<File> files = pm.getAllProtocols(action);
        assertEquals("Max number of protocol files (" + max + ") must be reached", max, files.size());

        File oldestFile = null;
        for (File file : files) {
            if (oldestFile == null || file.lastModified() < oldestFile.lastModified()) {
                oldestFile = file;
            }
        }

        // Create a new protocol
        log(action);

        // Check that the oldest file has been deleted
        files = pm.getAllProtocols(action);
        assertEquals("Max number of protocol files (" + max + ") must still be reached", max, files.size());
        assertFalse("Oldest file has not been deleted", oldestFile.exists());

        // create some more log files without sleep ....
        for (int i = 0; i < 5; i++) {
            log(action);
            // ... and check that still no more than max protocol files are created
            files = pm.getAllProtocols(action);
            assertEquals("Number of protocol files must not increase", max, files.size());
        }
    }
}
