/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.gef;

import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.TimeInterval;
import com.sun.grid.grm.config.gef.AbstractStepConfig;
import com.sun.grid.grm.config.gef.ActionConfig;
import com.sun.grid.grm.config.gef.DefaultGefConfig;
import com.sun.grid.grm.config.gef.JavaStepConfig;
import com.sun.grid.grm.config.gef.LogLevel;
import com.sun.grid.grm.config.gef.Parameter;
import com.sun.grid.grm.config.gef.ProtocolConfig;
import com.sun.grid.grm.config.gef.ProtocolOption;
import com.sun.grid.grm.config.naming.DummyConfigurationServiceContext;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.impl.ResourcePropertyInserted;
import com.sun.grid.grm.util.TimeIntervalHelper;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import junit.framework.TestCase;

/**
 *
 */
public class ActionTest extends TestCase {


    private final static Logger log = Logger.getLogger(ActionTest.class.getName());
    public ActionTest(String testName) {
        super(testName);
    }

    private ExecutionEnv env;
    private Resource resource;
    private final static Map<String,State> stepStateMap = new HashMap<String,State>();
    private final static Map<String,Map<String,Object>> stepInputParamMap = new HashMap<String,Map<String,Object>>();
    private final static Map<String,Map<String,Object>> stepOutputParamMap = new HashMap<String,Map<String,Object>>();
    private Handler handler;
    private final static LogLevel logLevel = LogLevel.DEBUG;

    private DefaultGefConfig gefConfig;
    @Override
    protected void setUp() throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
        setUpContext(env);
        resource = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "r1");
        handler = new ConsoleHandler();
        handler.setLevel(log.getLevel() == null ? Level.OFF : log.getLevel());

        gefConfig = new DefaultGefConfig();
        ProtocolConfig pc = new ProtocolConfig();
        ProtocolOption po = new ProtocolOption();
        po.setScriptLogLevel(LogLevel.DEBUG);
        po.setStderrLogLevel(LogLevel.DEBUG);
        po.setStdoutLogLevel(LogLevel.DEBUG);
        pc.setOnError(po);
        pc.setOnSuccess(po);
        
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        clearContext(env);
        stepStateMap.clear();
        stepInputParamMap.clear();
        stepOutputParamMap.clear();
        handler.close();
        super.tearDown();
    }

    private void setUpContext(ExecutionEnv env) throws Exception{
        Context ctx = env.getContext();
        Map<String, Object> content = TestUtil.getPredefinedSystem(env);
        for (String name : content.keySet()) {
            ctx.bind(name, content.get(name));
        }

    }

    /**
     * Clear the DummyConfiguratioServiceContext memory
     * @param env ExecutionEnv from which the context is obtained
     * @throws java.lang.Exception
     */
    private void clearContext(ExecutionEnv env) throws Exception{
        Context ctx = env.getContext();
        if (ctx instanceof DummyConfigurationServiceContext) {
            ((DummyConfigurationServiceContext)ctx).clear();
        }
    }

    private void execute(ActionConfig config) throws GefException, InvalidResourcePropertiesException {
        execute(ActionFactory.newAction(env, "test", config, gefConfig));
    }

    private void execute(GefAction action) throws GefStepException, InvalidResourcePropertiesException {
        ActionExecutor exe = action.newActionExecutor(env, resource, handler, Collections.<String,Object>emptyMap());
        while(exe.hasNextStep()) {
            exe.executeNextStep();
        }
    }

    /**
     *  Simple test for a successfull step
     * @throws Exception 
     */
    public void testSingleStep() throws Exception {
        log.entering(ActionTest.class.getName(), "testSingleStep");

        Configurator conf = new Configurator()
                      .step("s1").outputParam("RESOURCE:foo", "bar");

        execute(conf.action());
        
        assertEquals("resource property foo must have value bar", "bar", resource.getProperty("foo"));
        log.exiting(ActionTest.class.getName(), "testSingleStep");
    }

    /**
     * Test a step that fails with FAILED_NO_UNDO
     * @throws java.lang.Exception
     */
    public void testFailedStepNoUndo() throws Exception {
        log.entering(ActionTest.class.getName(), "testFailedStepNoUndo");

        Configurator conf = new Configurator()
                      .step("s1")
                      .step("s2").errorParam(StepExitCode.FAILED_NO_UNDO);

        try {
            execute(conf.action());
            fail("Step s2 must fail");
        } catch(GefStepException ex) {
            assertEquals("Step s2 must fail", "s2", ex.getStepName());
            assertEquals("Invalid error code for step s2", StepExitCode.FAILED_NO_UNDO, ex.getErrorCode());
        }
        // Step s1 must be undone, the resource must not contain the resource property
        assertEquals("Invalid state for state s1", State.UNDONE, stepStateMap.get("s1"));
        assertEquals("Invalid state for state s2", State.STARTED, stepStateMap.get("s2"));
        log.exiting(ActionTest.class.getName(), "testFailedStepNoUndo");
    }

    /**
     * Test a step that fails with FAILED_NEED_UNDO
     * @throws java.lang.Exception
     */
    public void testFailedStepNeedsUndo() throws Exception {
        log.entering(ActionTest.class.getName(), "testFailedStepNeedsUndo");

        Configurator conf = new Configurator()
                      .step("s1")
                      .step("s2").errorParam(StepExitCode.FAILED_NEED_UNDO);

        try {
            execute(conf.action());
            fail("Step s2 must fail");
        } catch(GefStepException ex) {
            assertEquals("Step s2 must fail", "s2", ex.getStepName());
            assertEquals("Invalid error code for step s2", StepExitCode.FAILED_NEED_UNDO, ex.getErrorCode());
        }
        // Step s1 must be undone, the resource must not contain the resource property
        assertEquals("Invalid state for state s1", State.UNDONE, stepStateMap.get("s1"));
        assertEquals("Invalid state for state s2", State.UNDONE, stepStateMap.get("s2"));
        log.exiting(ActionTest.class.getName(), "testFailedStepNeedsUndo");
    }

    /**
     *  Test a step that fails with permanent error
     * @throws java.lang.Exception
     */
    public void testFailedStepPermanentError() throws Exception {
        log.entering(ActionTest.class.getName(), "testFailedStepPermanentError");

        Configurator conf = new Configurator()
                      .step("s1")
                      .step("s2").errorParam(StepExitCode.PERMANENT_ERROR);

        try {
            execute(conf.action());
            fail("Step s2 must fail");
        } catch(GefStepException ex) {
            assertEquals("Step s2 must fail", "s2", ex.getStepName());
            assertEquals("Invalid error code for step s2", StepExitCode.PERMANENT_ERROR, ex.getErrorCode());
        }
        // Step s1 must be undone, the resource must not contain the resource property
        assertEquals("Invalid state for state s1", State.UNDONE, stepStateMap.get("s1"));
        assertEquals("Invalid state for state s2", State.UNDONE, stepStateMap.get("s2"));
        log.exiting(ActionTest.class.getName(), "testFailedStepPermanentError");
    }

    /**
     * Tests the the successor of a failed step is not executed
     * @throws java.lang.Exception
     */
    public void testFailedStepSuccessorNotExecuted() throws Exception {
        log.entering(ActionTest.class.getName(), "testFailedStepSuccessorNotExecuted");

        Configurator conf = new Configurator()
                      .step("s1").errorParam(StepExitCode.PERMANENT_ERROR)
                      .step("s2");

        try {
            execute(conf.action());
            fail("Step s1 must fail");
        } catch(GefStepException ex) {
            assertEquals("Step s1 must fail", "s1", ex.getStepName());
            assertEquals("Invalid error code for step s1", StepExitCode.PERMANENT_ERROR, ex.getErrorCode());
        }
        // Step s1 must be undone, the resource must not contain the resource property
        assertEquals("Invalid state for state s1", State.UNDONE, stepStateMap.get("s1"));
        assertEquals("Invalid state for state s2", State.INSTANTIATED, stepStateMap.get("s2"));
        log.exiting(ActionTest.class.getName(), "testFailedStepSuccessorNotExecuted");
    }

    /**
     * Simple parameters passing test
     * @throws java.lang.Exception
     */
    public void testParameterPassing() throws Exception {
        log.entering(ActionTest.class.getName(), "testParameterPassing");

        Configurator conf = new Configurator()
                .param("para", "action")
                .step("s1").param("para", "s1")
                .step("s2").param("para", "s2");


        execute(conf.action());
        assertEquals("Invalid value for parameter para for step s1", "s1", stepInputParamMap.get("s1").get("para"));
        assertEquals("Invalid value for parameter para for step s2", "s2", stepInputParamMap.get("s2").get("para"));
        log.exiting(ActionTest.class.getName(), "testParameterPassing");
    }

    /**
     * Test that the output parameter of a step overrides the the parameter
     * from the configuration
     * @throws java.lang.Exception
     */
    public void testParameterPassingOutputOverridesConfiguration() throws Exception {
        log.entering(ActionTest.class.getName(), "testParameterPassingOutputOverridesStepInput");

        String param = "para";
        String globalParam = "global";
        
        Configurator conf = new Configurator()
                .param(param, "action")
                .param(globalParam, "global")
                .step("s1").param(param, "s1")
                .step("s2").param(param, "s2").outputParam(param, "outputOfs2").outputParam("RESOURCE:static", "true")
                .step("s3").param(param, "s3").outputParam("RESOURCE:static", "false");


        execute(conf.action());

        assertEquals("Invalid value for input parameter " + param + " for step s1", "s1", stepInputParamMap.get("s1").get(param));
        assertEquals("Invalid value for input parameter " + globalParam + " for step s1", "global", stepInputParamMap.get("s1").get(globalParam));
        assertEquals("Invalid value for input parameter " + param + " for step s2", "s2", stepInputParamMap.get("s2").get(param));
        assertEquals("Invalid value for input parameter " + globalParam + " for step s2", "global", stepInputParamMap.get("s2").get(globalParam));
        assertEquals("Invalid value for output parameter " + param + " for step s2", "outputOfs2", stepOutputParamMap.get("s2").get(param));
        assertEquals("Invalid value for input parameter " + param + " for step s3", "outputOfs2", stepInputParamMap.get("s3").get(param));
        assertEquals("Invalid value for input parameter " + globalParam + " for step s3", "global", stepInputParamMap.get("s3").get(globalParam));
        assertEquals("resource must be non static", false, resource.isStatic());
        log.exiting(ActionTest.class.getName(), "testParameterPassingOutputOverridesStepInput");
    }

    /**
     * Test that a waiting step succeeds after rerun
     * @throws java.lang.Exception
     */
    public void testWaitingStepWithSuccess() throws Exception {
        log.entering(ActionTest.class.getName(), "testWaitingStepWithSuccess");

        Configurator conf = new Configurator()
                .step("s1")
                   .repeat(2)
                   .maxTries(2);

        execute(conf.action());

        assertEquals("step s1 must be invoked twice", new Integer(2), stepOutputParamMap.get("s1").get(TestStep.INVOCATION_COUNT));
        log.exiting(ActionTest.class.getName(), "testWaitingStepWithSuccess");
    }

    /**
     * Test that a waiting step fails of maxTries is reached
     * @throws java.lang.Exception
     */
    public void testWaitingStepWithError() throws Exception {
        log.entering(ActionTest.class.getName(), "testWaitingStepWithError");
        Configurator conf = new Configurator()
                .step("s1")
                   .repeat(3)
                   .maxTries(2);

        try {
            execute(conf.action());
            fail("Step s1 must fail");
        } catch(GefException ex) {
            assertEquals("step s1 must be invoked twice", new Integer(2), stepOutputParamMap.get("s1").get(TestStep.INVOCATION_COUNT));
        }
        log.exiting(ActionTest.class.getName(), "testWaitingStepWithError");
    }

    /**
     * Test that output parameters of a step are available in the input parameters
     * to the next steps
     * @throws java.lang.Exception
     */
    public void testOutputParamsIsKnownByFollowSteps() throws Exception {

        log.entering(ActionTest.class.getName(), "testSingleStep");

        String outParName = "foo";
        String outParValue = "bar";
        
        Configurator conf = new Configurator()
                      .step("s1").outputParam(outParName, outParValue)
                      .step("s2")
                      .step("s3");

        execute(conf.action());

        assertEquals("step s2 must have the input parameters " + outParName, outParValue, stepInputParamMap.get("s2").get(outParName));
        assertEquals("step s3 must have the input parameters " + outParName, outParValue, stepInputParamMap.get("s3").get(outParName));
        
        log.exiting(ActionTest.class.getName(), "testSingleStep");
    }

    /**
     * test that the output parameters of a failed step changes the resource
     * properties
     * @throws java.lang.Exception
     */
    public void testErrorChangesResource() throws Exception {
        log.entering(ActionTest.class.getName(), "testErrorChangesResource");

        String propName = "foo";
        String propValue = "bar";
        assertNull("resource must not have a resource property " + propName, resource.getProperty(propName));

        Configurator conf = new Configurator()
                      .step("s1")
                      .step("s2").outputParam("RESOURCE:" + propName, propValue).errorParam(StepExitCode.PERMANENT_ERROR);

        try {
            execute(conf.action());
            fail("Step s2 must fail");
        } catch(GefStepException ex) {
            assertEquals("Step s2 must fail", "s2", ex.getStepName());
            assertEquals("Invalid error code for step s2", StepExitCode.PERMANENT_ERROR, ex.getErrorCode());
            assertEquals("Invalid value of resource property " + propName, propValue, resource.getProperty(propName));
            assertEquals("The gef exception must have two resource changes", 2, ex.getChanges().size());

            ResourcePropertyInserted propChange = null;
            for(ResourceChanged change: ex.getChanges()) {
                if (propName.equals(change.getName())) {
                    try {
                        propChange = (ResourcePropertyInserted)change;
                    } catch(ClassCastException ex1) {
                        fail(String.format("Expected a ResourcePropertyInserted for prop %s, but got %s",
                                propName, change.getClass().getSimpleName()));
                    }
                    break;
                }
            }
            assertNotNull("The gef execption must contain a change for property " + propName, propChange);
            assertEquals("Invalid value in change for property " + propName, propValue, propChange.getValue());
        }

        log.exiting(ActionTest.class.getName(), "testErrorChangesResource");
    }

    /**
     * Test that a step is executed if it has a filter that matches
     * against the resource.
     * @throws java.lang.Exception
     */
    public void testFilterMatching() throws Exception {
        log.entering(ActionTest.class.getName(), "testUndoFilterMatching");

        Configurator conf = new Configurator()
                .step("s1").filter("foo = null");

        execute(conf.action());
        
        assertEquals("Invalid state for state s1", State.EXECUTED, stepStateMap.get("s1"));
        log.exiting(ActionTest.class.getName(), "testUndoFilterMatching");
    }

    /**
     * Test that a step is not exected of a filter of the step is not matching
     * against the resource.
     * Test that also the undo of the step is not executed.
     * @throws java.lang.Exception
     */
    public void testFilterNotMatching() throws Exception {
        log.entering(ActionTest.class.getName(), "testUndoFilterNotMatching");

        Configurator conf = new Configurator()
                .step("s1").filter("foo != null")
                .step("s2").errorParam(StepExitCode.FAILED_NEED_UNDO);

        try {
            execute(conf.action());
            fail("Step s2 must fail");
        } catch (GefStepException ex) {
            assertEquals("Step s2 must fail", "s2", ex.getStepName());
            assertEquals("Invalid error code for step s2", StepExitCode.FAILED_NEED_UNDO, ex.getErrorCode());
        }
        // Step s1 must be not executed
        assertEquals("Invalid state for state s1", State.INSTANTIATED, stepStateMap.get("s1"));
        assertEquals("Invalid state for state s2", State.UNDONE, stepStateMap.get("s2"));
        log.exiting(ActionTest.class.getName(), "testUndoFilterNotMatching");
    }


    static class Configurator {

        private final ActionConfig ac = new ActionConfig();
        private AbstractStepConfig currentStep;

        public  Configurator() {
            ac.setLogLevel(logLevel);
        }
        Configurator step(String name) {
            JavaStepConfig s = new JavaStepConfig();
            s.setName(name);
            s.setClassname(TestStep.class.getName());
            s.setLogLevel(logLevel);

            TimeInterval delay = new TimeInterval();
            delay.setUnit(TimeIntervalHelper.SECONDS);
            delay.setValue(1);
            s.setDelay(delay);
            
            ac.getStep().add(s);
            currentStep = s;
            return this;
        }

        Configurator param(String name, String value) {
            Parameter p = new Parameter();
            p.setName(name);
            p.setValue(value);
            if (currentStep == null) {
                ac.getParam().add(p);
            } else {
                currentStep.getParam().add(p);
            }
            return this;
        }


        Configurator filter(String filter) {
            assertNotNull("current step must not be null", currentStep);
            currentStep.setFilter(filter);
            return this;
        }

        Configurator errorParam(StepExitCode errorCode) {
            return param(TestStep.ERROR_CODE, errorCode.name());
        }

        Configurator outputParam(String key, String value) {
            return param(TestStep.OUTPUT_PARAM_PREFIX + key, value);
        }

        Configurator repeat(int count) {
            param(TestStep.REPEAT_COUNTER, Integer.toString(count));
            return this;
        }

        Configurator maxTries(int count) {
            currentStep.setMaxTries(count);
            TimeInterval ti = new TimeInterval();
            ti.setUnit(TimeIntervalHelper.SECONDS);
            ti.setValue(1);
            currentStep.setDelay(ti);
            return this;
        }


        ActionConfig action() {
            return ac;
        }
        
    }

    enum State {
        INSTANTIATED,
        STARTED,
        EXECUTED,
        UNDONE
    }

    public static class TestStep implements JavaStep {

        public final static String EXCEPTION = "exception";
        public final static String OUTPUT_PARAM_PREFIX = "OUTPUT:";
        public final static String UNDO_OUTPUT_PARAM_PREFIX = "UNDO_OUTPUT:";

        public final static String REPEAT_COUNTER = "repeat";
        public final static String INVOCATION_COUNT = "invocation_count";
        
        public final static String ERROR_CODE = "ERROR_CODE";

        private final String name;

        
        public TestStep(String name) {
            this.name = name;
            stepStateMap.put(name, State.INSTANTIATED);
        }

        public String getName() {
            return name;
        }

        public StepResult execute(GefActionContext ctx, Map<String, Object> inputParams, Logger protocol) {
            stepStateMap.put(getName(), State.STARTED);
            if (inputParams.containsKey(ERROR_CODE)) {
                return new StepResult(StepExitCode.valueOf((String)inputParams.get(ERROR_CODE)));
            }
            stepInputParamMap.put(getName(), new HashMap<String,Object>(inputParams));
            Map<String,Object> outputParams = new HashMap<String,Object>();

            for(Map.Entry<String,Object> entry: inputParams.entrySet()) {
                if(entry.getKey().startsWith(OUTPUT_PARAM_PREFIX)) {
                    String key = entry.getKey().substring(OUTPUT_PARAM_PREFIX.length());
                    outputParams.put(key, entry.getValue());
                }
            }

            int invocationCount = ctx.getInvocationCount(getName());
            outputParams.put(INVOCATION_COUNT, invocationCount);
            
            log.log(Level.FINE, "Step " + getName() + " invoked " + invocationCount + " times");

            stepOutputParamMap.put(getName(), new HashMap<String,Object>(outputParams));

            String repeatStr = (String)inputParams.get(REPEAT_COUNTER);
            if(repeatStr != null) {
                int repeatCount =  Integer.parseInt(repeatStr);
                log.log(Level.FINE, "Step " + getName() + ": Repeat counter = " + repeatCount);
                if (invocationCount < repeatCount) {
                    return new StepResult(StepExitCode.REPEAT);
                }
            }

            stepStateMap.put(getName(), State.EXECUTED);

            return new StepResult(StepExitCode.SUCCESS, outputParams);
        }

        public Map<String,Object> undo(GefActionContext ctx, Map<String, Object> inputParams, Logger protocol) {

            Map<String,Object> outputParams = new HashMap<String,Object>();
            
            for(Map.Entry<String,Object> entry: inputParams.entrySet()) {
                if(entry.getKey().startsWith(OUTPUT_PARAM_PREFIX)) {
                    String key = entry.getKey().substring(OUTPUT_PARAM_PREFIX.length());
                    outputParams.put(key, entry.getValue());
                }
            }

            stepStateMap.put(getName(), State.UNDONE);

            return outputParams;
        }
    }
}
