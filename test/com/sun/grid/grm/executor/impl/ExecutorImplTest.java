/*___INFO__MARK_BEGIN__*/

/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.executor.impl;

import com.sun.grid.grm.impl.AbstractComponentStateTransitionTester;
import com.sun.grid.grm.Barrier;
import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.TestUtil;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.TimeInterval;
import com.sun.grid.grm.config.executor.ExecutorConfig;
import com.sun.grid.grm.config.naming.DummyConfigurationServiceContext;
import com.sun.grid.grm.executor.Command;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.executor.NoExitValueException;
import com.sun.grid.grm.executor.RemoteFile;
import com.sun.grid.grm.executor.Result;
import com.sun.grid.grm.ui.component.GetComponentConfigurationCommand;
import com.sun.grid.grm.ui.component.ModifyComponentConfigurationCommand;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.util.TimeIntervalHelper;
import com.sun.grid.grm.util.UnixPlatform;
import java.io.File;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 */
public class ExecutorImplTest extends AbstractComponentStateTransitionTester<Executor> {
    
    private final static Logger log = Logger.getLogger(ExecutorImplTest.class.getName());
    private ExecutionEnv env;
    
    public ExecutorImplTest(String testName) {
        super(testName);
    }        
    
    @Override
    protected void setUp() throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
        setUpContext(env);
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        // Do not stop the executor, it is done in super.tearDown
        clearContext(env);
        super.tearDown();
    }
    
    public void testMaxRuntime() throws Exception {
        
        Executor executor = getComp().getComponent();
        
        GetComponentConfigurationCommand<ExecutorConfig> gccmd = new GetComponentConfigurationCommand<ExecutorConfig>();
        gccmd.setComponentName(executor.getName());
        
        ExecutorConfig conf = env.getCommandService().execute(gccmd).getReturnValue().get(0);
        
        TimeInterval ti = new TimeInterval();
        ti.setUnit(TimeIntervalHelper.SECONDS);
        ti.setValue(3);
        conf.setMaxCommandRuntime(ti);
        
        ModifyComponentConfigurationCommand mccmd = new ModifyComponentConfigurationCommand(executor.getName(), conf);
        env.getCommandService().execute(mccmd);
        
        executor.start();
        waitUntilStarted(executor);
        
        ShellCommand cmd = new ShellCommand("sleep 10");

        Result res = executor.execute(cmd);
        try {
            res.getExitValue();
            fail("A command that exceeds the max runtime should not have a exit code");
        } catch(NoExitValueException ex) {
            // fine
        }
    }
    
    public void testComandMaxRuntime() throws Exception {
        
        Executor executor = getComp().getComponent();
        
        executor.start();
        waitUntilStarted(executor);
        
        ShellCommand cmd = new ShellCommand("sleep 10");
        cmd.setMaxRuntime(3000);
        
        Result res = executor.execute(cmd);
        try {
            res.getExitValue();
            fail("A command that exceeds the max runtime should not have a exit code");
        } catch(NoExitValueException ex) {
            // fine
        }
    }
    

    
    public void testGlobalMaxRuntimeOverrideComandMaxRuntime() throws Exception {
        
        Executor executor = getComp().getComponent();
        
        GetComponentConfigurationCommand<ExecutorConfig> gccmd = new GetComponentConfigurationCommand<ExecutorConfig>();
        gccmd.setComponentName(executor.getName());
        
        ExecutorConfig conf = env.getCommandService().execute(gccmd).getReturnValue().get(0);
        
        TimeInterval ti = new TimeInterval();
        ti.setUnit(TimeIntervalHelper.SECONDS);
        ti.setValue(3);
        conf.setMaxCommandRuntime(ti);
        
        ModifyComponentConfigurationCommand mccmd = new ModifyComponentConfigurationCommand(executor.getName(), conf);
        env.getCommandService().execute(mccmd);
        
        executor.start();
        waitUntilStarted(executor);
        
        ShellCommand cmd = new ShellCommand("sleep 10");
        cmd.setMaxRuntime(20000);
        
        Result res = executor.execute(cmd);
        try {
            res.getExitValue();
            fail("A command that exceeds the max runtime should not have a exit code");
        } catch(NoExitValueException ex) {
            // fine
        }
    }

    /**
     * This tests ensure that at least maxPoolSize tasks can run in parallel
     * in the Executor. It also test that a reload on the executor has impact
     * on the size of the thread pool
     * @throws java.lang.Exception
     */
    public void testCanRunMaxPoolSizeTasks() throws Exception {
        log.entering(ExecutorImplTest.class.getName(), "testCanRunMaxPoolSizeTasks");

        final Executor executor = getComp().getComponent();

        ExecutorConfig config = new ExecutorConfig();
        config.setCorePoolSize(1);
        config.setMaxPoolSize(1);
        env.getContext().rebind("component." + executor.getName(), config);

        long cmdTimeout = 10000L;
        executor.start();
        waitUntilStarted(executor);

        for (int maxPoolSize = 1; maxPoolSize < 4; maxPoolSize++) {
            log.log(Level.FINE, "Setting maxPoolSize to " + maxPoolSize);
            // it's enough to set the maxPoolSize, the corePoolSize from the config is ignored
            config.setMaxPoolSize(maxPoolSize);
            env.getContext().rebind("component." + executor.getName(), config);
            executor.reload(false);
            waitUntilStarted(executor);

            // the ExecutorService allows for one additional running thread
            // (maxPoolSize + 1) as in a real SDM system there is still the
            // component thread running in the thread pool.
            final int taskCount = maxPoolSize + 1;
            final CyclicBarrier barrier = new CyclicBarrier(taskCount);

            AsyncRunner ars[] = new AsyncRunner[taskCount];
            for (int i = 0; i < taskCount; i++) {
                ars[i] = new AsyncRunner(executor, new CyclicBarrierCommand(i,barrier, cmdTimeout));
                ars[i].start();
            }
            try {
                for (AsyncRunner ar : ars) {
                    assertTrue("Commands must finish", ar.waitForResult(cmdTimeout));
                    assertFalse("Command must finish without error", ar.hasError());
                }
            } finally {
                for(int i = 0; i< taskCount; i++) {
                    if (ars[i] != null && ars[i].isAlive()) {
                        ars[i].interrupt();
                    }
                }
            }
        }
  
        log.exiting(ExecutorImplTest.class.getName(), "testCanRunMaxPoolSizeTasks");
    }

    /**
     * This test submits maxPoolSize + 2 commands into the executor. Each
     * command has a cyclic barrier of party size maxPoolSize + 2. None of
     * the tasks should pass the cyclic barrier because only maxPoolSize + 1
     * commands can run in parallel in the executor.
     *
     * The maxPoolSize + 1 comes from the fact  that ExecutorImpl considers
     * that in a real SDM system one thread of the thread pool is always consumed
     * by the component thread of the executor.
     * 
     * @throws java.lang.Exception
     */
    public void testCanNotRunMoreThanMaxPoolSizeTasks() throws Exception {
        log.entering(ExecutorImplTest.class.getName(), "testCanNotRunMoreThanMaxPoolSizeTasks");
        final Executor executor = getComp().getComponent();

        ExecutorConfig config = new ExecutorConfig();
        config.setCorePoolSize(1);
        config.setMaxPoolSize(1);
        env.getContext().rebind("component." + executor.getName(), config);

        long cmdTimeout = 2000L;
        executor.start();
        waitUntilStarted(executor);

        for(int maxPoolSize=3; maxPoolSize > 0; maxPoolSize--) {
            log.log(Level.FINE, "Setting maxPoolSize to " + maxPoolSize);
            config.setMaxPoolSize(maxPoolSize);
            env.getContext().rebind("component." + executor.getName(), config);
            executor.reload(false);
            waitUntilStarted(executor);

            // ExecutorImpl sets the real pool size of the thread pool
            // to maxPoolSize + 1 because the component thread from
            // JVMImpl$ComponentLifecylce consumes always one thread in
            // real SDM system.
            // However in this test this is not the case we have an exceed
            // of one thread
            // => set the taskCount to maxPoolSize + 2
            int taskCount = maxPoolSize + 2;
            final CyclicBarrier barrier = new CyclicBarrier(taskCount);

            AsyncRunner ars[] = new AsyncRunner[taskCount];
            for (int i = 0; i < taskCount; i++) {
                ars[i] = new AsyncRunner(executor, new CyclicBarrierCommand(i, barrier, cmdTimeout));
                ars[i].start();
            }
            try {
                for (AsyncRunner ar : ars) {
                    assertTrue("Commands must finish", ar.waitForResult(5*cmdTimeout));
                    assertTrue("Command must report an error", ar.hasError());
                }
            } finally {
                for(int i = 0; i< taskCount; i++) {
                    if (ars[i] != null && ars[i].isAlive()) {
                        ars[i].interrupt();
                    }
                }
            }
        }
        log.exiting(ExecutorImplTest.class.getName(), "testCanNotRunMoreThanMaxPoolSizeTasks");
    }

    
    /**
     * This test checks that a script get the TERM signal of the max runtime is exceeded.
     * 
     * @throws java.lang.Exception
     */
    public void testScriptKilledAtMaxRuntime() throws Exception {
        
        Executor executor = getComp().getComponent();

        GetComponentConfigurationCommand<ExecutorConfig> gccmd = new GetComponentConfigurationCommand<ExecutorConfig>();
        gccmd.setComponentName(executor.getName());
        
        ExecutorConfig conf = env.getCommandService().execute(gccmd).getReturnValue().get(0);
        
        TimeInterval ti = new TimeInterval();
        ti.setUnit(TimeIntervalHelper.SECONDS);
        ti.setValue(3);
        conf.setMaxCommandRuntime(ti);
        
        ModifyComponentConfigurationCommand mccmd = new ModifyComponentConfigurationCommand(executor.getName(), conf);
        env.getCommandService().execute(mccmd);
        
        File tmpDir = TestUtil.getTmpDir(this);
        
        if (!tmpDir.exists()) {
            tmpDir.mkdirs();
        }
        try {
            File outFile = new File(tmpDir, "trap_test.out");
            
            final String termMessage = "Got TERM signal";
            final String normalExitMessage = "Normal exit";
            RemoteFile script = new RemoteFile("trap_test.sh");
            PrintWriter pw = script.createPrintWriter();

            pw.println("#!/bin/sh");
            pw.println("out_file=" + outFile.getAbsolutePath());
            pw.println("log() {");
            pw.println("   echo \"$*\" >> $out_file");
            pw.println("}");
            pw.println("terminate() {");
            pw.println("   log \"" + termMessage + "\"");
            pw.println("   exit 2");
            pw.println("}");
            pw.println("");
            pw.println("rm -f $out_file" );
            pw.println("log \"Started $$\"");
            pw.println("");
            pw.println("trap \"terminate\" TERM");
            pw.println("");
            pw.println("i=0");
            pw.println("while [ $i -lt 3600 ]; do");
            pw.println("sleep 1");
            pw.println("i=`expr $i + 1`");
            pw.println("done");
            pw.println("log \"" + normalExitMessage + "\"");
            pw.close();
            ShellCommand cmd = new ShellCommand(script);

            executor.start();
            waitUntilStarted(executor);

            Result res = executor.execute(cmd);
            try {
                res.getExitValue();
                fail("A command that exceeds the max runtime should not have a exit code");
            } catch(NoExitValueException ex) {
                String content = FileUtil.read(outFile);
                assertTrue("output file must contain message \"" + termMessage + "\"", content.contains(termMessage));
                assertTrue("output file must not contain message \"" + normalExitMessage + "\"", !content.contains(normalExitMessage));
            }
        } finally {
            Platform.getPlatform().removeDir(tmpDir, true);
        }
    }
    
    /**
     * This test checks that a script get the TERM signal of the max runtime is exceeded.
     * 
     * @throws java.lang.Exception
     */
    public void testMultipleChildsScriptKilledAtMaxRuntime() throws Exception {
        
        Executor executor = getComp().getComponent();

        GetComponentConfigurationCommand<ExecutorConfig> gccmd = new GetComponentConfigurationCommand<ExecutorConfig>();
        gccmd.setComponentName(executor.getName());
        
        ExecutorConfig conf = env.getCommandService().execute(gccmd).getReturnValue().get(0);
        
        TimeInterval ti = new TimeInterval();
        ti.setUnit(TimeIntervalHelper.SECONDS);
        ti.setValue(3);
        conf.setMaxCommandRuntime(ti);
        
        ModifyComponentConfigurationCommand mccmd = new ModifyComponentConfigurationCommand(executor.getName(), conf);
        env.getCommandService().execute(mccmd);
        
        File tmpDir = TestUtil.getTmpDir(this);
        
        if (!tmpDir.exists()) {
            tmpDir.mkdirs();
        }
        try {
            File outFile = new File(tmpDir, "trap_test.out");
            File childsOutFile = new File(tmpDir, "childs.out");

            final String termMessage = "Got TERM signal";
            final String normalExitMessage = "Normal exit";
            
            RemoteFile script2 = new RemoteFile("trap_test2.sh");
            PrintWriter pw2 = script2.createPrintWriter();

            pw2.println("#!/bin/sh");
            pw2.println("child_out_file=" + childsOutFile.getAbsolutePath());
            pw2.println("echo $$ >> $child_out_file");
            pw2.println("sleep 100 &");
            pw2.println("echo $! >> $child_out_file");
            pw2.println("sleep 100 &");
            pw2.println("echo $! >> $child_out_file");
            pw2.println("sleep 100 &");
            pw2.println("echo $! >> $child_out_file");
            pw2.println("i=0");
            pw2.println("while [ $i -lt 3600 ]; do");
            pw2.println("sleep 1");
            pw2.println("i=`expr $i + 1`");
            pw2.println("done");
            pw2.close();
            
            script2.store(tmpDir);
            
            RemoteFile script = new RemoteFile("trap_test.sh");
            PrintWriter pw = script.createPrintWriter();
            try {
                pw.println("#!/bin/sh");
                pw.println("out_file=" + outFile.getAbsolutePath());
                pw.println("child_out_file=" + childsOutFile.getAbsolutePath());
                pw.println("log() {");
                pw.println("   echo \"$*\" >> $out_file");
                pw.println("}");
                pw.println("terminate() {");
                pw.println("   log \"" + termMessage + "\"");
                pw.println("   exit 2");
                pw.println("}");
                pw.println("");
                pw.println("rm -f $out_file" );
                pw.println("log \"Started $$\"");
                pw.println("");
                pw.println("trap \"terminate\" TERM");
                pw.println("");
                pw.println("i=0");
                pw.println("ls -al | grep trap*");
                pw.println("sh "+tmpDir.getAbsolutePath()+"/trap_test2.sh &");
                pw.println("log \"pid 2 testa to $!\"");
                pw.println("sleep 100 &");
                pw.println("echo $! >> $child_out_file");
                pw.println("sleep 100 &");
                pw.println("echo $! >> $child_out_file");
                pw.println("sleep 100 &");
                pw.println("echo $! >> $child_out_file");
                pw.println("while [ $i -lt 3600 ]; do");
                pw.println("sleep 1");
                pw.println("i=`expr $i + 1`");
                pw.println("done");
                pw.println("log \"" + normalExitMessage + "\"");
            } finally {
                pw.close();
            }
            ShellCommand cmd = new ShellCommand(script);

            executor.start();
            waitUntilStarted(executor);

            Result res = executor.execute(cmd);
            try {
                res.getExitValue();
                fail("A command that exceeds the max runtime should not have a exit code");
            } catch(NoExitValueException ex) {
                String content = FileUtil.read(outFile);
                String content2 = FileUtil.read(childsOutFile);
                Thread.sleep(3000);
                for (String pid : content2.split("\n")) {
                    assertFalse("pid of child process exists !!!", UnixPlatform.getPlatform().existsProcess(Integer.parseInt(pid)));
                }
                assertTrue("output file must contain message \"" + termMessage + "\"", content.contains(termMessage));
                assertFalse("output file must not contain message \"" + normalExitMessage + "\"", content.contains(normalExitMessage));
            }
        } finally {
            Platform.getPlatform().removeDir(tmpDir, true);
        }
    }
    
    
    /**
     * This test checks that a script get the TERM signal of the max runtime is exceeded.
     * 
     * @throws java.lang.Exception
     */
    public void testScriptWithChildsKilledAtMaxRuntime() throws Exception {
        
        Executor executor = getComp().getComponent();

        GetComponentConfigurationCommand<ExecutorConfig> gccmd = new GetComponentConfigurationCommand<ExecutorConfig>();
        gccmd.setComponentName(executor.getName());
        
        ExecutorConfig conf = env.getCommandService().execute(gccmd).getReturnValue().get(0);
        
        TimeInterval ti = new TimeInterval();
        ti.setUnit(TimeIntervalHelper.SECONDS);
        ti.setValue(3);
        conf.setMaxCommandRuntime(ti);
        
        ModifyComponentConfigurationCommand mccmd = new ModifyComponentConfigurationCommand(executor.getName(), conf);
        env.getCommandService().execute(mccmd);
        
        File tmpDir = TestUtil.getTmpDir(this);
        
        if (!tmpDir.exists()) {
            tmpDir.mkdirs();
        }
        try {
            File outFile = new File(tmpDir, "trap_test.out");
            
            RemoteFile script = new RemoteFile("trap_test.sh");
            PrintWriter pw = script.createPrintWriter();

            pw.println("#!/bin/sh");            
            pw.println("out_file=" + outFile.getAbsolutePath());
            pw.println("echo $$ >> $out_file");
            pw.println("depth=1");
            pw.println("max_depth=3");
            pw.println("if [ $depth -lt  $max_depth ]; then");
            pw.println("next_depth=`expr $depth + 1`");
            pw.println("for i in 1 2; do");
            pw.println("sh "+tmpDir.getAbsolutePath()+"/child_test.sh $next_depth $max_depth &");
            pw.println("done");
            pw.println("fi");
            pw.println("sleep 3600");
            pw.close();
            
            
            RemoteFile script2 = new RemoteFile("child_test.sh");
            PrintWriter pw2 = script2.createPrintWriter();

            pw2.println("#!/bin/sh");
            pw2.println("out_file=" + outFile.getAbsolutePath());    
            pw2.println("echo $$ >> $out_file");
            pw2.println("depth=$1");
            pw2.println("max_depth=$2");
            pw2.println("if [ $depth -lt  $max_depth ]; then");
            pw2.println("next_depth=`expr $depth + 1`");
            pw2.println("for i in 1 2; do");
            pw2.println("sh "+tmpDir.getAbsolutePath()+"/child_test.sh $next_depth $max_depth &");
            pw2.println("done");
            pw2.println("fi");
            pw2.println("sleep 3600");
            pw2.close();
            
            script2.store(tmpDir);
            
            ShellCommand cmd = new ShellCommand(script);
            
            
            executor.start();
            waitUntilStarted(executor);

            Result res = executor.execute(cmd);
            try {
                res.getExitValue();
                fail("A command that exceeds the max runtime should not have a exit code");
            } catch(NoExitValueException ex) {
                String content = FileUtil.read(outFile);
                for (String pid : content.split("\n")) {
                    assertFalse("pid of child process exists !!!", UnixPlatform.getPlatform().existsProcess(Integer.parseInt(pid)));
                }
            }
        } finally {
            Platform.getPlatform().removeDir(tmpDir, true);
        }
    }

    public void testSetEnv() throws Exception {

        ExecutorImpl executor = new ExecutorImpl(env, "executor");

        executor.start();
        waitUntilStarted(executor);

        String varName = "SOME_ENV_VAR";
        String varValue = "some_env_var";

        ShellCommand cmd = new ShellCommand("echo $" + varName);

        cmd.setEnv(Collections.singletonMap(varName, varValue));
        Result res = executor.execute(cmd);

        assertEquals("Command reported error", 0, res.getExitValue());
        assertEquals("Expected no output to stderr of the command", 0, res.getErrorMessages().size());
        assertEquals("Expected only one line in stdout of the command", 1, res.getOutputMessages().size());
        assertEquals("Unexpected value in first line of command", varValue, res.getOutputMessages().get(0));
    }

    private void waitUntilStarted(Executor executor) throws Exception {
        for (int i = 0; i < 100; i++) {
            if (executor.getState().equals(ComponentState.STARTED)) {
                return;
            }
            Thread.sleep(100);
        }
        fail("Executor did not start");
    }
    
    /**
     * Method for initializing the Context. The predefined components are placed in 
     * Context so basic system operation can be done on context
     * @param env ExecutionEnv from which the context is obtained
     * @throws java.lang.Exception 
     */
    public void setUpContext(ExecutionEnv env) throws Exception{
        Context ctx = env.getContext();
        Map<String, Object> content = TestUtil.getPredefinedSystem(env);        
        for (String name : content.keySet()) {
            ctx.bind(name, content.get(name));
        }     
        
    }
    
    /**
     * Clear the DummyConfiguratioServiceContext memory
     * @param env ExecutionEnv from which the context is obtained
     * @throws java.lang.Exception 
     */
    public void clearContext(ExecutionEnv env) throws Exception{
        
        Context ctx = env.getContext();
        if (ctx instanceof DummyConfigurationServiceContext) {
            ((DummyConfigurationServiceContext)ctx).clear();
        }
    }
    
    
    @Override
    protected ComponentTestAdapter<Executor> createComponentTestAdapter() throws Exception {
        return new ExecutorAdapter();
    }
    
    class ExecutorAdapter extends DefaultComponentTestAdapter<Executor> {

        public ExecutorAdapter() {
           addFeature(Feature.BLOCK_STOP_COMPONENT, new BlockStopComponentFeatureHandler());
        }
        
        @Override
        protected Executor createComponent() {
            return new ExecutorImpl(env, "executor");
        }
        
        class BlockStopComponentFeatureHandler implements FeatureHandler {
            
            private final BlockingCommand blockingCommand = new BlockingCommand();
            
            public void setup() {

                AsyncRunner asyncRunner = new AsyncRunner(getComponent(), blockingCommand);

                asyncRunner.start();
                try {
                    blockingCommand.waitForStartup();
                } catch (InterruptedException ex) {
                    fail("Interrupt while waiting for startup of BlockingCommand");
                }
            }
            public void teardown() {
                blockingCommand.wakeup();
            }

            public void waitUntil() throws InterruptedException {
                blockingCommand.waitForStartup();
            }
        }
        
    }

    /**
     * The CyclicBarrierCommand await in the call method until a CylicBarrier
     * is reached.
     * The CyclicBarrierCommand has an timeout, if the CylicBarrier is not reached
     * in time the commands throws an exception.
     *
     * This command can be used to ensure that command can run in parallel
     * in the executor.
     *
     * @see CyclicBarrier
     */
    private static class CyclicBarrierCommand extends Command {
        private final static long serialVersionUID = -2009101601L;

        private final CyclicBarrier barrier;
        private final long timeout;
        private final int  index;
        
        public CyclicBarrierCommand(int index, CyclicBarrier barrier, long timeout) {
            this.barrier = barrier;
            this.timeout = timeout;
            this.index = index;
        }
        public Result call() throws Exception {
            try {
                log.log(Level.FINE, this.toString() + " started");
                barrier.await(timeout, TimeUnit.MILLISECONDS);
                Result ret  = new Result();
                ret.setExitValue(0);
                log.log(Level.FINE, this.toString() + " cyclic barrier reached");
                return ret;
            } catch(Exception ex) {
                log.log(Level.FINE, this.toString() + " failed");
                throw ex;
            }
        }

        @Override
        public String toString() {
            return "CyclicBarrierCommand[" + index + "]";
        }

    }
    
    private static class BlockingCommand extends Command {
        
        private final static long serialVersionUID = -2008111001L;
         
        private final Barrier barrier;
        
        public BlockingCommand() {
            this.barrier = Barrier.newEnabledInstance("sleeper");
        }
        
        public Result call() throws Exception {
            Result ret = new Result();
            try {
                barrier.waitUntilWakeup();
                ret.setExitValue(0);
            } catch(InterruptedException ex) {
                ret.setExitValue(-1);
            }
            return ret;
        }
        
        public void waitForStartup() throws InterruptedException {
            barrier.waitUntilBlocked();
        }

        public boolean waitForStartup(long timeout) throws InterruptedException {
            return barrier.waitUtilBlocked(timeout);
        }


        
        public void wakeup() {
            barrier.wakeup();
        }
        
    }
    
    private class AsyncRunner extends Thread {
        
        Result res;
        Exception error;
        
        private final Command command;
        private final Executor executor;
        
        public AsyncRunner(Executor executor, Command command) {
            this.executor = executor;
            this.command = command;
        }
        
        @Override
        public void run() {
            try {
                Result tmpRes = executor.execute(command);
                synchronized(this) {
                    this.res = tmpRes;
                    this.notifyAll();
                }
            } catch(Exception ex) {
                synchronized(this) {
                    error = ex;
                    this.notifyAll();
                }
            }
        }
        
        public synchronized boolean waitForResult(long timeout) throws InterruptedException {
            long endTime = System.currentTimeMillis() + timeout;
            while(true) {
                if(System.currentTimeMillis() >= endTime) {
                    return false;
                }
                if (res != null) {
                    return true;
                }
                if (error != null) {
                    return true;
                }
                wait(timeout);
            }
        }

        synchronized boolean hasError() {
            return error != null;
        }

        synchronized Result getResult() {
            return res;
        }
        
    }
}
