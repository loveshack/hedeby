/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
#define _CRT_SECURE_NO_DEPRECATE

// Disable warning regarding throw(int) keyword
#pragma warning( disable : 4290 )

#include <afxtempl.h>
#include <afxmt.h>
#include <userenv.h>
#include <ShlObj.h>
#include "SvcmgmtStarter.h"
#include "ServiceInstaller.h"

// constants
// name, display name and description of the service in "Services" dialog
// of control panel
static const char g_szServiceName[]        = "SvcmgmtStarter.exe";
static const char g_szServiceDisplayName[] = "SvcmgmtStarter";
static const char g_szServiceDescription[] = "Starts Haithabu ParentSvcmgmtStarter";

// internal name of the service handler
static const char g_szServiceHandlerName[] = "SvcmgmtStarterHandler";

// module static declarations
static SERVICE_STATUS_HANDLE g_hServiceStatus         = NULL;
static const char            *g_pszDefaultEnvFileName = "grm.env";
   
static enum enRunningState {
   rsStopped = 0,
   rsRunning = 1,
   rsUnknown = 2
};

// function forward declarations
static int  ChangeRunningState(enRunningState eState);
static void  WINAPI SvcmgmtStarterStart(DWORD dwArgc, LPTSTR *lpszArgv);
static DWORD WINAPI SvcmgmtStarterHandlerEx(DWORD dwControl, 
                       DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext);
static void PrintHelp(const char *pszExecutableName, const char *pszServiceName);
static int ExecSvcmgmtCmd(const char *pszCommand, const char *pszCommandLine,
                          CStringArray &arStdout, CStringArray &arStderr,
                          DWORD        &dwExitCode);
static void ReadFileToArray(const CString &strFile, 
                            CStringArray &arArray) throw(int);
static void ReadGrmDistFromRegistry(CString &strGrmDist) throw(int);
static void FillServiceStatus(SERVICE_STATUS &ServiceStatus);
static void WriteToEventLog(LPCTSTR szMessages);

/****** main() ****************************************************************
*  NAME
*     main() -- starting point of the executable
*
*  SYNOPSIS
*     int main(int argc, char *argv[]);
*
*  FUNCTION
*     A Windows Service works this way:
*     When the service is started, either automatically at boot time or manually,
*     main() gets called by the ServiceControlManager (SCM).
*
*     main() has to register the Control Dispatcher Thread to the SCM. The Control
*     Dispatcher Thread gets called by the SCM, 
*
*  INPUTS
*     int  argc    - number of command line arguments
*     char *argv[] - array of arguments
*
*  RESULT
*     int - exit status of the executable
*     0:  no errors
*     >0: see ServiceInstaller.h for all error codes
*
*  NOTES
*******************************************************************************/
int main(int argc, char *argv[])
{
   int  i;
   int  ret = 0;
   char *pszLocalSysName     = NULL;
   char *pszEnvFilePath      = NULL;
   char *pszInstallSysName   = NULL;
   char *pszUninstallSysName = NULL;
   char *pszUserName         = NULL;
   char *pszPassword         = NULL;
Sleep(20000);
   if(argc>1) {
      // Parse command line arguments
      for(i=1; i<argc; i++) {
         if(_strnicmp(argv[i], "-install", 8) == 0) {
            i++;
            if(argc > i) {
               pszInstallSysName = argv[i];
            }
         } else if(_strnicmp(argv[i], "-uninstall", 10)==0) {
            i++;
            if(argc > i) {
               pszUninstallSysName = argv[i];
            }
         } else if(_strnicmp(argv[i], "-local", 6)==0) {
            i++;
            if(argc > i) {
               pszLocalSysName = argv[i];
            }
         } else if(_strnicmp(argv[i], "-envfile", 8)==0) {
            i++;
            if(argc > i) {
               pszEnvFilePath = argv[i];
            }
         } else if(_strnicmp(argv[i], "-user", 5)==0) {
            i++;
            if(argc > i) {
               pszUserName = argv[i];
            }
         } else if(_strnicmp(argv[i], "-pass", 5)==0) {
            i++;
            if(argc > i) {
               pszPassword = argv[i];
            }
         } else {
            PrintHelp(argv[0], g_szServiceDisplayName);
            return WrongOptionsSpecified;
         }
      }

      // interpret command line arguments
      if(pszInstallSysName != NULL) {
         if(   pszUninstallSysName != NULL
            || (pszLocalSysName == NULL && pszEnvFilePath != NULL)
            || (pszLocalSysName == NULL
               && (pszUserName == NULL || pszPassword == NULL))
            || (pszLocalSysName != NULL
               && (pszUserName != NULL || pszPassword != NULL))) {
            PrintHelp(argv[0], g_szServiceDisplayName);
            ret = WrongOptionsSpecified;
         } else if(pszLocalSysName == NULL) {
            ret = InstallService(argv[0], pszInstallSysName, pszInstallSysName,
                                 g_szServiceDescription, pszUserName, pszPassword);
         } else if(pszLocalSysName != NULL) {
            ret = InstallLocally(argv[0], pszInstallSysName, pszEnvFilePath);
         }
      } else if(pszUninstallSysName != NULL) {
         if(   pszInstallSysName != NULL
            || (pszLocalSysName == NULL && pszEnvFilePath != NULL)) {
            PrintHelp(argv[0], g_szServiceDisplayName);
            ret = WrongOptionsSpecified;
         } else if(pszLocalSysName == NULL) {
            ret = UninstallService(pszUninstallSysName);
         } else if(pszLocalSysName != NULL) {
            ret = UninstallLocally(pszUninstallSysName);
         }
      } else {
         PrintHelp(argv[0], g_szServiceDisplayName);
         ret = WrongOptionsSpecified;
      }
   } else if(argc==1) {
      // Start SvcmgmtStarter Service as Win32 Service.
      // main() is called by the Windows Service Manager, main() must register
      // all service entry points to the Service Manager. Then the Service
      // Manager starts all services.
      SERVICE_TABLE_ENTRY DispatchTable[] = {
         {(LPSTR)g_szServiceName, SvcmgmtStarterStart},
         {NULL,                   NULL}};

      if(!StartServiceCtrlDispatcher(DispatchTable)) {
         ret = CantStartService;
      }
   } else {
      // Too many command line arguments, print help.
      PrintHelp(argv[0], g_szServiceDisplayName);
      ret = WrongOptionsSpecified;
   }
	return ret;
}

/****** ChangeRunningState() **************************************************
*  NAME
*     ChangeRunningState() -- Starts or stops the GRM systems.
*
*  SYNOPSIS
*     static int ChangeRunningState(enRunningState eState)
*
*  FUNCTION
*     Starts or stops the GRM systems with autostart flag.
*     It just initiates the start of each GRM system and waits blocking
*     for the end of the gstat.exe command, but this doesn't wait for the
*     end of the GRM system.
*
*  INPUTS
*     enRunningState eState - Set all GRM systems with "auto start" flag into
*                             this state
*
*  RESULT
*     int - 0: no error
*          >0: see ServiceInstaller.h for error codes.
*
*  NOTES
*******************************************************************************/
static int ChangeRunningState(enRunningState eState)
{
   CStringArray    arGconfStdout, arGconfStderr;
   CStringArray    arGstatStdout, arGstatStderr;
   CString         strCommandLine;
   int             i, ret = 0;
   DWORD           dwExitCode = -1;

   try {
      ret = ExecSvcmgmtCmd("gconf.exe", 
               "sc -a -n", arGconfStdout, arGconfStderr, dwExitCode);
      if(ret != 0) {
         //throw CantRunGconf;
      }
      for(i=0; i<arGconfStdout.GetCount(); i++) {
         strCommandLine = "--system " + arGconfStdout[i] 
                          + " change_state -c test -s ";
         if(eState == rsRunning) {
            strCommandLine += "start";
         } else {
            strCommandLine += "stop";
         }

         ret = ExecSvcmgmtCmd("gstat.exe", 
                  strCommandLine, arGstatStdout, arGstatStderr, dwExitCode);
         if(ret != 0) {
            // TODO
            // Don't stop here, just log error
         }
      }
   } catch(int retval) {
      DWORD     dwLastError;
      char      ErrorMessage[501];

//TODO: use info from arStderr or arStdout
      dwLastError = GetLastError();
      FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL,
                    dwLastError, 0, ErrorMessage, 500, NULL);
      WriteToEventLog((LPCTSTR)ErrorMessage);
      ret = retval;
   }
   return ret;
}

/****** SvcmgmtStarterStart() **************************************************
*  NAME
*     SvcmgmtStarterStart() -- starting point of the service
*
*  SYNOPSIS
*     void WINAPI SvcmgmtStarterStart(DWORD dwArgc, LPTSTR *lpszArgv)
*
*  FUNCTION
*     This is the starting point of the service that is called by the
*     service manager when the service is started.
*
*  INPUTS
*     DWORD  dwArgc    - number of arguments
*     LPTSTR *lpszArgv - argument list
*
*  RESULT
*     void - no result
*
*  NOTES
*******************************************************************************/
static void WINAPI SvcmgmtStarterStart(DWORD dwArgc, LPTSTR *lpszArgv)
{
   SERVICE_STATUS        ServiceStatus;
   DWORD                 dwStatus = 0;

   try {
      // Register at Service Control Manager
      WriteToLogFile("Starting %s.", g_szServiceDisplayName);

      g_hServiceStatus = RegisterServiceCtrlHandlerEx(g_szServiceName, 
                                 SvcmgmtStarterHandlerEx, lpszArgv[0]);
      if(g_hServiceStatus == (SERVICE_STATUS_HANDLE)0) {
         throw CantRegisterServiceCtrlHandler;
      }

      FillServiceStatus(ServiceStatus);
      if(!SetServiceStatus(g_hServiceStatus, &ServiceStatus)) {
         throw CantSetServiceStatus;
      }

      WriteToLogFile("%s started successfully.", g_szServiceDisplayName);

#ifdef DEBUG
//Sleep(20000);   // For debugging
#endif
      ChangeRunningState(rsRunning);
   } catch(int) {
      char      ErrorMessage[501];
      DWORD     dwLastError;

      dwLastError = GetLastError();
      FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL,
                    dwLastError, 0, ErrorMessage, 500, NULL);
      WriteToEventLog((LPCTSTR)ErrorMessage);

      // Stop the service
      ServiceStatus.dwCurrentState = SERVICE_STOPPED;
      SetServiceStatus(g_hServiceStatus, &ServiceStatus);
   }
}

/****** SvcmgmtStarterHandlerEx() *********************************************
*  NAME
*     SvcmgmtStarterHandlerEx() -- handler for the service that is
*                                  invoked by the service control manager
*
*  SYNOPSIS
*     DWORD WINAPI SvcmgmtStarterHandlerEx(DWORD  dwControl,
*                                          DWORD  dwEventType,
*                                          LPVOID lpEventData,
*                                          LPVOID lpContext)
*
*  FUNCTION
*     This handler is invoked by the service control manager when the service
*     is to be started, stopped or altered.
*     See HandlerEx() in Microsoft PlatformSDK documentation.
*  
*  INPUTS
*     DWORD  dwControl   - action that is to be performed
*     DWORD  dwEventType - more specific information for some actions
*     LPVOID lpEventData - more specific information for some actions
*     LPVOID lpContext   - more specific information for some actions
*
*  RESULT
*     DWORD - NO_ERROR if no errors occured, see HandlerEx() in Microsoft
*             PlatformSDK documentation for list of errors.
*
*  NOTES
*******************************************************************************/
static DWORD WINAPI SvcmgmtStarterHandlerEx(DWORD  dwControl,
                                            DWORD  dwEventType,
                                            LPVOID lpEventData,
                                            LPVOID lpContext)
{
   SERVICE_STATUS  SvcStatus;

   FillServiceStatus(SvcStatus);

   switch(dwControl) {
      case SERVICE_CONTROL_STOP:
         if(ChangeRunningState(rsStopped) == 0) {
            SvcStatus.dwCurrentState = SERVICE_STOPPED; 
            if(!SetServiceStatus(g_hServiceStatus, &SvcStatus)) { 
               WriteToEventLog("Can't stop service!");
            } 
         }
         break;

      case SERVICE_CONTROL_INTERROGATE:
         break; 

      default: 
         WriteToEventLog("Unrecognized control code!");
   }
   return NO_ERROR;
}

/****** ExecSvcmgmtCmd() *****************************************************
*  NAME
*     ExecSvcmgmtCmd() -- Executes a command (gconf or gstat)
*
*  SYNOPSIS
*  static int ExecSvcmgmtCmd(const char *pszCommand,
*                            const char *pszCommandLine,
*                            CStringArray &arStdout,
*                            CStringArray &arStderr,
*                            DWORD        &dwExitCode)
*
*  FUNCTION
*     Executes a command, redirectes stdout and stderr to the provided
*     CStringArrays and waits blocking for the end of the command.
*     A command is everything that can be executed by CreateProcess(),
*     generally an executable.
*
*  INPUTS
*     const char *pszCommand - the command to execute
*                              ("gconf.exe" or "gstat.exe")
*     const char *pszCommandLine - all arguments of the command 
*
*  OUTPUTS
*     CString &arStdout   - the redirected stdout
*     CString &arStderr   - the redirected stderr
*     DWORD   &dwExitCode - exit code of the executed command
*
*  RESULT
*     int - 0: no error
*          >0: see ServiceInstaller.h for error codes.
*
*  NOTES
*  TODO: Return exit() value of command
*******************************************************************************/
static int ExecSvcmgmtCmd(const char *pszCommand,
                          const char *pszCommandLine,
                          CStringArray &arStdout,
                          CStringArray &arStderr,
                          DWORD        &dwExitCode)
{
   STARTUPINFO           si;
   PROCESS_INFORMATION   pi;
   HANDLE                hStdout = INVALID_HANDLE_VALUE;
   HANDLE                hStderr = INVALID_HANDLE_VALUE;
   CString               strStdout, strStderr;
   CString               strWindowsSystemRoot;
   CString               strTempPath;
   CString               strGrmDist;
   CString               strCommandLine;
   int                   ret = 0;

   memset(&pi, 0, sizeof(pi));
   memset(&si, 0, sizeof(si));

   try {
      ReadGrmDistFromRegistry(strGrmDist);
      strCommandLine = strGrmDist + "\\bin\\";
      strCommandLine += pszCommand;
      strCommandLine += " ";
      strCommandLine += pszCommandLine;

      GetTempPath(MAX_PATH, strTempPath.GetBufferSetLength(MAX_PATH));
      strTempPath.ReleaseBuffer();

      GetTempFileName(strTempPath, "grm", 0, strStdout.GetBufferSetLength(MAX_PATH));
      strStdout.ReleaseBuffer();

      GetTempFileName(strTempPath, "grm", 0, strStderr.GetBufferSetLength(MAX_PATH));
      strStderr.ReleaseBuffer();

      hStdout = CreateFile(
                  strStdout,
                  GENERIC_WRITE,
                  FILE_SHARE_READ|FILE_SHARE_WRITE,
                  NULL,
                  OPEN_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_WRITE_THROUGH,
                  NULL);
      if(hStdout == INVALID_HANDLE_VALUE) {
         throw CantCreateStdoutFile;
      }
   //   SetFilePointer(hStdout, 1, NULL, FILE_END);
      SetHandleInformation(hStdout, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT);

      hStderr = CreateFile(
                  strStderr,
                  GENERIC_WRITE,
                  FILE_SHARE_READ|FILE_SHARE_WRITE,
                  NULL,
                  OPEN_ALWAYS,
                  FILE_ATTRIBUTE_NORMAL|FILE_FLAG_WRITE_THROUGH,
                  NULL);

      if(hStderr == INVALID_HANDLE_VALUE) {
         throw CantCreateStderrFile;
      }

   //   SetFilePointer(hStderr, 1, NULL, FILE_END);
      SetHandleInformation(hStderr, HANDLE_FLAG_INHERIT, HANDLE_FLAG_INHERIT);

      si.cb          = sizeof(STARTUPINFO); 
      si.dwFlags     |= STARTF_USESTDHANDLES;
      si.hStdOutput  = hStdout;
      si.hStdError   = hStderr;
      si.lpDesktop   = "WinSta0\\Default"; 
      si.wShowWindow = SW_HIDE; 

      // Call gconf
      ret = CreateProcess(
         NULL,
         (char*)LPCTSTR(strCommandLine),
         NULL,
         NULL,
         TRUE,
         CREATE_NO_WINDOW,
         NULL,
         NULL,
         &si,
         &pi);
      if(ret == 0) {
         throw CantCreateProcess;
      }

      // Wait blocking for job end
      if(pi.hProcess != INVALID_HANDLE_VALUE) {
         DWORD dwWait = WaitForSingleObjectEx(pi.hProcess, INFINITE, FALSE);

         CloseHandle(hStdout);
         CloseHandle(hStderr);
         hStdout = hStderr = INVALID_HANDLE_VALUE;

         if(dwWait==WAIT_OBJECT_0) {
            // Successfully ended - get exit code
            GetExitCodeProcess(pi.hProcess, &dwExitCode);

            // read temp files to CStringArrays
            ReadFileToArray(strStdout, arStdout);
            ReadFileToArray(strStderr, arStderr);
            ret = 0;
         } else {
            // Not successfully
         }
      }
   }
   catch(int retval) {
      DWORD     dwLastError;
      char      ErrorMessage[501];

      dwLastError = GetLastError();
      FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, 
                    dwLastError, 0, ErrorMessage, 500, NULL);
      WriteToEventLog((LPCTSTR)ErrorMessage);
      ret = retval;
   }

   // Clean up
   if(hStdout != INVALID_HANDLE_VALUE) {
      CloseHandle(hStdout);
   }
   if(hStderr != INVALID_HANDLE_VALUE) {
      CloseHandle(hStderr);
   }
   if(strStdout != "") {
      DeleteFile(strStdout);
   }
   if(strStderr != "") {
      DeleteFile(strStderr);
   }
   if(pi.hProcess != INVALID_HANDLE_VALUE) {
      CloseHandle(pi.hProcess); 
   }
   if(pi.hThread != INVALID_HANDLE_VALUE) {
      CloseHandle(pi.hThread); 
   }
   return ret;
}

/****** ReadFileToArray() ****************************************************
*  NAME
*     ReadFileToArray() -- Reads contents of a file line by line to a 
*                          CStringArray
*
*  SYNOPSIS
*     static void ReadFileToArray(const CString &strFile,
*                                 CStringArray &arArray)
*
*  FUNCTION
*     Reads a text file line by line to a CStringArray
*
*  INPUTS
*     const CString &strFile - Path and name of the file
*
*  OUTPUTS
*     CStingArray &arArray - the filled CStringArray
*
*  RESULT
*     void - no result
*     Throws an int in case of error, see ServiceInstaller.h for error codes.
*
*  NOTES
*******************************************************************************/
static void ReadFileToArray(const CString &strFile,
                            CStringArray &arArray) throw(int)
{
   HANDLE hFile = INVALID_HANDLE_VALUE;
   DWORD  dwFileSize;
   BOOL   bRet;
   char   *pPointer;
   char   *pBuffer = NULL;
   int    ret = 0;


   try {
      // Read file to buffer
      hFile = CreateFile(
               strFile,
               GENERIC_READ,
               FILE_SHARE_READ,
               NULL,
               OPEN_ALWAYS,
               FILE_ATTRIBUTE_NORMAL,
               NULL);

      if(hFile == INVALID_HANDLE_VALUE) {
         throw CantOpenFile;
      }

      dwFileSize = GetFileSize(hFile, NULL);
      if(dwFileSize > 0) {
         pBuffer = (char*)calloc((size_t)dwFileSize+1, sizeof(char));
         if(pBuffer == NULL) {
            throw CantAllocateBuffer;
         }
         bRet = ReadFile(hFile, pBuffer, dwFileSize, &dwFileSize, NULL);
         if(bRet == FALSE) {
            throw CantReadFromFile;
         }

         // Read buffer to CStringArray
         // strtok doesn't work here because of \r\n line breaks of DOS
         int i;
         BOOL bFound;

         bFound = FALSE;
         i=0;
         pPointer = pBuffer;
         while(pBuffer[i] != '\0') {
            while(pBuffer[i] != '\0') {
               if(pBuffer[i] == '\r') {
                  pBuffer[i] = '\0';
                  i++;
                  bFound = TRUE;
               } 
               if(pBuffer[i] == '\n') {
                  pBuffer[i] = '\0';
                  i++;
                  bFound = TRUE;
               }
               if(bFound == TRUE) {
                  arArray.Add(pPointer);
                  pPointer = pBuffer+i;
                  bFound = FALSE;
                  break;
               } else {
                  i++;
               }
            }
         }
      }
   } catch(int retval) {
      DWORD     dwLastError;
      char      ErrorMessage[501];

      dwLastError = GetLastError();
      FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, 
                    dwLastError, 0, ErrorMessage, 500, NULL);
      WriteToEventLog((LPCTSTR)ErrorMessage);
      ret = retval;
   }
   if(hFile != INVALID_HANDLE_VALUE) {
      CloseHandle(hFile);
   }
   if(pBuffer != NULL) {
      free(pBuffer);
   }
   if(ret != 0) {
      throw(ret);
   }
}

/****** FillServiceStatus() ****************************************************
*  NAME
*     FillServiceStatus() - Fills a SERVICE_STATUS struct with default values
*
*  SYNOPSIS
*     static void FillServiceStatus(SERVICE_STATUS &ServiceStatus)
*
*  FUNCTION
*     Fills a SERVICE_STATUS struct with default values
*
*  OUTPUTS
*     SERVICE_STATUS &ServiceStatus - gets filled
*
*  RESULT
*     void - no result
*
*  NOTES
*******************************************************************************/
static void FillServiceStatus(SERVICE_STATUS &ServiceStatus)
{
   ServiceStatus.dwServiceType             = SERVICE_WIN32_OWN_PROCESS;
   ServiceStatus.dwCurrentState            = SERVICE_RUNNING;
   ServiceStatus.dwControlsAccepted        = SERVICE_ACCEPT_SHUTDOWN
                                             |SERVICE_ACCEPT_STOP;  
   ServiceStatus.dwWin32ExitCode           = NO_ERROR;
   ServiceStatus.dwServiceSpecificExitCode = 0;
   ServiceStatus.dwCheckPoint              = 0;
   ServiceStatus.dwWaitHint                = 1000;
}

static void ReadGrmDistFromRegistry(CString &strGrmDist) throw(int)
{
   LONG     res;
   HKEY     hKey = NULL;
   DWORD    bufsize = 0;
   DWORD    dwType;
   char     szSubKey[] = "SOFTWARE\\Sun Microsystems\\grm\\svcmgmt\\grm.env";
   char     szValue[] = "GRM_DIST";
   CString  strSubKey;
   
   try {
      // Query path length
      res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, szSubKey, 0, KEY_ALL_ACCESS, &hKey);
      if(res != ERROR_SUCCESS) {
         throw CantOpenRegKey;
      }
      // Determine needed buf size
      res = RegQueryValueEx(hKey, szValue, NULL, &dwType, (BYTE*)NULL, &bufsize);
      if(res != ERROR_SUCCESS) {
         throw CantQueryRegValue;
      }
      // Alloc buffer and query path
      res = RegQueryValueEx(hKey, szValue, NULL, &dwType, 
               (BYTE*)strGrmDist.GetBufferSetLength(bufsize+1), &bufsize);
      strGrmDist.ReleaseBuffer();
      if(res != ERROR_SUCCESS) {
         throw CantQueryRegValue;
      }
      RegCloseKey(hKey);
   } catch(int retval) {
      if(hKey != NULL) {
         RegCloseKey(hKey);
      }
      throw retval;
   }
   
}

/****** PrintHelp() ***********************************************************
*  NAME
*     PrintHelp() - prints usage information to stdout
*
*  SYNOPSIS
*     void PrintHelp(const char *pszExecutableName, const char *pszServiceName)
*
*  FUNCTION
*     Prints usage information to stdout
*
*  INPUTS
*     const char* pszExecutableName - name of the executable (use argv[0])
*     const char* pszServiceName    - display name of the service
*
*  RESULT
*     void - no result
*
*  NOTES
*******************************************************************************/
static void PrintHelp(const char *pszExecutableName, const char *pszServiceName)
{
   printf("%s -install <SystemName> -user <UserName> -pass <Password>\n"
          " | -install <SystemName> -local\n"
          " | -uninstall <SystemName>\n"
          " | -uninstall <SystemName> -local\n"
          " | -help\n"
          "  -install <SystemName>    Installs %s under the given system\n"
          "                           name as a Windows service.\n"
          "  -uninstall <SystemName>  Uninstalls %s with the given system\n"
          "                           name as a Windows service.\n"
          "  -user <UserName>         Installs %s under the given user\n"
          "                           account. Must be a local user,\n"
          "                           e.g. \"MyHost\\Administrator\".\n"
          "  -pass <Password>         The password of the user account.\n"
          "  -local                   Installs %s under th given system\n"
          "                           name to the current users Startup folder.\n"
          "  -help                    Displays this help\n",
          pszExecutableName, pszServiceName, pszServiceName);
}

/****** WriteToEventLog()******************************************************
*  NAME
*     WriteToEventLog() - writes message to Windows event log
*
*  SYNOPSIS
*     static void WriteToEventLog(LPCTSTR szMessage);
*
*  FUNCTION
*     Writes a error message to the Windows event log
*
*  INPUTS
*     LPCSTSTR szMessage - the message that should be written to the event log
*
*  RESULT
*     void - no result
*
*  NOTES
*     TODO: Register events to Windows so event viewer can display full event 
*           information!
*******************************************************************************/
#define MSG_ERR_EXIST ((DWORD)0xC0000004L)
static void WriteToEventLog(LPCTSTR szMessage)
{
   HANDLE hLog;
   BOOL   bRet;
   LPCTSTR szMessages[1];

   szMessages[0] = szMessage;

   hLog = RegisterEventSource(NULL, g_szServiceDisplayName);
   if(hLog == NULL) {
      // TODO:
      // Panic! Can't access event log - what to do here?
   }

   bRet = ReportEvent(hLog,
             EVENTLOG_ERROR_TYPE,
             0,
             MSG_ERR_EXIST,
             NULL,
             1,
             0,
             szMessages,
             NULL);
   if(bRet == FALSE) {
      // TODO:
      // Panic! Can't access event log - what to do here?
   }

   DeregisterEventSource(hLog);
}


// Just for debugging purposes
/****** WriteToLogFile() *******************************************************
*  NAME
*     WriteToLogFile() -- writes a message to a log file
*
*  SYNOPSIS
*     int WriteToLogFile(const char *szMessage, ...)
*
*  FUNCTION
*     Writes a message to a log file. For debugging purposes only.
*
*  INPUTS
*     const char *szMessage - the format string of the message
*     ...                   - the parameters of the message (see printf())
*
*  RESULT
*     int - 0 if message was written to log file, 1 else
*
*  NOTES
*******************************************************************************/
int WriteToLogFile(const char *szMessage, ...)
{
   int        ret = 1;
#ifdef _DEBUG
   FILE       *fp = NULL;
   SYSTEMTIME sysTime;
   va_list    args;
   char       Buffer[4096];
   va_start(args, szMessage);

   _vsnprintf(Buffer, 4095, szMessage, args);

   fp = fopen("c:\\SvcmgmtStarter.log", "a+");
   if(fp != NULL) {
      GetLocalTime(&sysTime);
      fprintf(fp, "%02d:%02d:%02d [SGE_Helper_Service] %s\n",
         sysTime.wHour, sysTime.wMinute, sysTime.wSecond, Buffer);
      fflush(fp);
      fclose(fp);
      ret = 0;
   }
#endif 
   return ret;
}

/* Call tree, just for optimizing purposes
main
|- InstallService
|  |- GetAbsoluteBinaryPath throw(int)
| 
|- UninstallService
| 
|- InstallLocally
|  |- GetSpecialFolderPath throw(int)
|  |- GetAbsoluteBinaryPath throw(int)
| 
|- UninstallLocally
|  |- GetSpecialFolderPath throw(int)
| 
|- StartSystem
|  |- ReadEnvFileToEnvironment throw(CString)
|  |- ReadEnvironmentVariable
|  |- BuildGstatCommandLine throw(int)
|  |  |- GetSpecialFilderPath throw(int)
|  |  |- ReadEnvironmentVariable
|  |- ExecuteCommand
|     |- ReadFileToArray throw(int)
|  
|- StopSystem
|  |- ReadEnvFileToEnvironment throw(CString)
|  |- BuildGstatCommandLine throw(int)
|  |  |- GetSpecialFilderPath throw(int)
|  |  |- ReadEnvironmentVariable
|  |- ExecuteCommand
|     |- ReadFileToArray throw(int)
|
|..SvcmgmtStarterStart
|  |..SvcmgmtStarterHandlerEx
|     |- StopSystem
|        |-> siehe main->StopSystem
|  |- FillServiceStatus
|  |- GetEnvFilePathFromRegistry throw(int)
|  |- StartSystem 
|     |-> siehe main->StartSystem
|  
|- PrintHelp
*/