/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

// Error values
const int WrongOptionsSpecified          = 1;
const int CantStartService               = 2;
const int CantFindApplicationFile        = 3;
const int CantReadEnvFile                = 4;
const int CantGetBinaryPathSize          = 5;
const int CantAllocateBuffer             = 6;
const int CantGetBinaryPath              = 7;
const int CantOpenSCManager              = 8;
const int CantCreateService              = 9;
const int CantOpenService                = 10;
const int CantDeleteService              = 11;
const int CantCoInitialize               = 12;
const int CantCoCreateInstance           = 13;
const int CantSetPath                    = 14;
const int CantQueryInterface             = 15;
const int CantSave                       = 16;
const int CantGetSpecialFolderLocation   = 17;
const int CantGetPathFromIDList          = 18;
const int CantGetMalloc                  = 19;
const int CantGetStartupFolderPath       = 20;
const int CantSetArguments               = 21;
const int CantCreateProcess              = 22;
const int CantRegisterServiceCtrlHandler = 23;
const int CantSetServiceStatus           = 24;
const int CantOpenRegKey                 = 25;
const int CantQueryRegValue              = 26;
const int CantCreateStdoutFile           = 27;
const int CantCreateStderrFile           = 28;
const int CantOpenFile                   = 29;
const int CantReadFromFile               = 30;

int InstallService(const char *pszBinaryPathName, 
                   const char *pszServiceName, 
                   const char *pszDisplayName,
                   const char *pszDescription,
                   const char *pszUserName,
                   const char *pszPassword);
int UninstallService(const char *pszServiceName);

int InstallLocally(const char *pszBinaryPathName,
                   const char *pszInstallSysName,
                   const char* pszEnvFilePath);
int UninstallLocally(const char *pszServiceName);

void GetSpecialFolderPath(int nFolder,
                          CString &strStartupFolderPath) throw(int);
