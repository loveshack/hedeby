/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

Installation vom Haithabu unter Windows:
----------------------------------------
* Normal installieren
* Dann folgende Registry-Eintr�ge machen:
  - Bei Installation als 'root':
    Key HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Haithabu
  - Bei Installation als normaler User:    
    Key HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Haithabu
  Darin:
  - ValueName "DisplayName", Value "Haithabu", Type REG_SZ
  - ValueName "UninstallString", Value <Absoluter_Pfad_auf_den_Uninstaller>, Type REG_SZ

  (siehe weiter unten wie man Eintr�ge macht (oder ohne API machen kann)).


Installation vom SvcmgmtStarter:
--------------------------------

SvcmgmtStarter.exe [-install <SystemName> -user <UserName> -pass <Password>
                  | -install <SystemName> -local
                  | -uninstall <SystemName>
                  | -uninstall <SystemName> -local
                  | -help]

       -install <SystemName>    Installs SvcmgmtStarter under the given system
                                name as a Windows service.
                                If used with "-local", it installs the
                                SvcmgmtStarter locally for the current user.
       -uninstall <SystemName>  Uninstalls SvcmgmtStarter with the given system
                                name as a Windows service.
                                With "-local", uninstalls the local installation.
       -user <UserName>         Installs SvcmgmtStarter under the given user
                                account. Must be a local user,
                                e.g. "MyHost\Administrator" or ".\user_x"
       -pass <Password>         The password of the user account.
       -local                   Installs the system to the current users Startup folder.
                                For users who have not the permissions to install Windows
                                services.
       -help                    Displays this help



1a) Installation als lokaler Administrator (oder als sonstiges Mitglied der Administratoren-Gruppe):

* # SvcmgmtStarter.exe -install <SystemName> -user <UserName> -pass <Password>

  Installiert den SvcmgmtStarter Service als Win32 Service unter dem angegebenen Systemnamen,
  jede Installation erzeugt einen neuen Eintrag (eine neue Instanz) in der Windows Services Liste.

  Jede Service-Instanz erwartet im Registry-Path HKEY_LOCAL_MACHINE\SOFTWARE\Sun Microsystems\grm\svcmgmt\grm.env
  den Key "GRM_DIST", dessen Wert der volle, absolute Pfad auf die neueste Haithabu-Distribution ist.

* Einen Registry-Key schreibt man so:
  %SYSTEMROOT%\system32\reg.exe add "HKLM\SOFTWARE\Sun Microsystems\grm\svcmgmt\grm.env" /v "GRM_DIST" /t REG_SZ /d <Path_of_haithabu>
  Evtl. noch den Parameter "/f" f�r �berschreiben ohne Nachfragen
  
* Danach evtl. Service starten?
  %SYSTEMROOT\system32\net.exe start <SystemName>  

1b) Deinstallation:
* Service stoppen:
  %SYSTEMROOT\system32\net.exe stop <SystemName>
  
* # SvcmgmtStarter.exe -uninstall <SystemName>

* Key aus der Registry l�schen:
  %SYSTEMROOT%\system32\reg.exe delete "HKLM\SOFTWARE\Sun Microsystems\grm\svcmgmt\grm.env"


2a) Installation als "Dumm-User":
* Eintrag im Autostart-Folder des Users:
  SvcmgmtStarter.exe -install <SystemName> -local
  
  Der SvcmgmtStarter.exe erzeugt einen Link im Autostart-Folder des Users. Der Link zeigt
  auf das gstat.exe binary, das im Verzeichnis neben dem SvcmgmtStarter.exe liegt. (haithabu\bin).

* System starten?
  gstat.exe .....

2b)Deinstallation:
* System stoppen.
  gstat.exe ....

* Eintrag aus dem Autostart-Folder l�schen
  SvcmgmtStarter.exe -uninstall <SystemName> -local


