/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

#define _CRT_SECURE_NO_DEPRECATE

// Disable warning regarding throw(int) keyword
#pragma warning( disable : 4290 )

#include <stdio.h>
#include <afxtempl.h>
#include <afxmt.h>
#include <shtypes.h>
#include <ShlObj.h>
#include <stdio.h>
#include "ServiceInstaller.h"

// forward declarations
static void GetAbsoluteBinaryPath(const CString &strRelativeBinaryPath, CString &strAbsoluteBinaryPath) throw(int);
static void AddEventSource(const char* szAppName, const char* szAppPath) throw(int);
static void RemoveEventSource(const char* szAppName) throw(int);

/****** InstallService() ******************************************************
*  NAME
*     InstallService() -- Installs the SvcmgmtStarter Service
*
*  SYNOPSIS
*     int InstallService(const char *pszBinaryPathName, 
*                        const char *pszServiceName, 
*                        const char *pszDisplayName, 
*                        const char *pszDescription,
*                        const char *pszUserName,
*                        const char *pszPassword) 
*
*  FUNCTION
*     Installs the SvcmgmtStarter Service to the system.
*
*  INPUTS
*     const char *pszBinaryPathName - path of SvcmgmtStarter.exe
*     const char *pszServiceName    - internal name of the service
*     const char *pszDisplayName    - display name in the service control manager
*     const char *pszDescription    - description in the service control manager
*     const char *pszUserName       - User account under which the service shall run
*     const char *pszPassword       - Password for the user account
*
*
*  RESULT
*     int - did the installation succeed?
*     0:  no errors
*     >0: installation failed, see ServiceInstaller.h for error codes
*
*  NOTES
*******************************************************************************/
int InstallService(const char *pszBinaryPathName, 
                   const char *pszServiceName, 
                   const char *pszDisplayName, 
                   const char *pszDescription,
                   const char *pszUserName,
                   const char *pszPassword) 
{ 
   CString             strBinaryPath;
   SC_HANDLE           schService;
   SC_HANDLE           schSCManager;
   SERVICE_DESCRIPTION ServiceDescription;
   LPTSTR              lptstrFilePart = NULL;
   DWORD               dwBufSize = 0;
   int                 ret = 0;

   try {
      GetAbsoluteBinaryPath(pszBinaryPathName, strBinaryPath);

      schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
      if(schSCManager == NULL) {
         throw CantOpenSCManager;
      }

      schService = CreateService( 
         schSCManager,                 // SCManager database 
         pszServiceName,               // name of service 
         pszDisplayName,               // service name to display 
         SERVICE_ALL_ACCESS,           // desired access 
         SERVICE_WIN32_OWN_PROCESS,    // service type 
         SERVICE_AUTO_START,           // start type 
         SERVICE_ERROR_NORMAL,         // error control type 
         strBinaryPath,                // service's binary 
         NULL,                         // no load ordering group 
         NULL,                         // no tag identifier 
         NULL,                         // no dependencies 
         pszUserName,                  // username (NULL = LocalSystem account)
         pszPassword);                 // user's password (NULL = no password)
    
      if (schService == NULL) {
         throw CantCreateService;
      }

      AddEventSource(pszServiceName, pszBinaryPathName);

      printf("Service successfully installed.\n");

      ServiceDescription.lpDescription = (LPTSTR)pszDescription;
      ChangeServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, 
                           &ServiceDescription);
   }
   catch(int retval) {
      DWORD     dwLastError;
      char      ErrorMessage[501];

      dwLastError = GetLastError();
      FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, dwLastError, 0, 
                    ErrorMessage, 500, NULL);

      printf("Service installation failed.\n");
      printf("Error: %d: %s\n", dwLastError, ErrorMessage);
      ret = retval;
   }

   if(schService != NULL) {
      CloseServiceHandle(schService); 
   }
   if(schSCManager != NULL) {
      CloseServiceHandle(schSCManager);
   }
   return ret;
}

/****** UninstallService() ****************************************************
*  NAME
*     UninstallService() -- Uninstalls the SvcmgmtStarter Service
*
*  SYNOPSIS
*     int UninstallService(const char *pszServiceName)
*
*  FUNCTION
*     Uninstalls the SvcmgmtStarter Service from the system.
*
*  INPUTS
*     const char *pszServiceName - internal name of the service
*
*  RESULT
*     int - did the uninstallation succeed?
*     0:  no errors
*     >0: uninstallation failed, see ServiceInstaller.h for error codes
*
*  NOTES
*******************************************************************************/
int UninstallService(const char *pszServiceName) 
{ 
   SC_HANDLE schService   = NULL;
   SC_HANDLE schSCManager = NULL;
   DWORD     dwLastError;
   char      ErrorMessage[501];
   int       ret = 0;

   try {
      schSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
      if(schSCManager == NULL) {
         throw CantOpenSCManager;
      }
      schService = OpenService(schSCManager, pszServiceName, SERVICE_ALL_ACCESS);
      if(schService == NULL) {
         throw CantOpenService;
      }
      if(!DeleteService(schService)) {
         throw CantDeleteService;
      }

      RemoveEventSource(pszServiceName);

      printf("Service successfully uninstalled.\n");
   }
   catch(int retval) {
      dwLastError = GetLastError();
      FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, dwLastError, 0, ErrorMessage, 500, NULL);

      printf("Service uninstallation failed.\n");
      printf("Error: %d: %s\n", dwLastError, ErrorMessage);
      ret = retval;
   }

   if(schService != NULL) {
      CloseServiceHandle(schService); 
   }
   if(schSCManager != NULL) {
      CloseServiceHandle(schSCManager);
   }
   return ret;
}

/****** InstallLocally() ******************************************************
*  NAME
*     InstallLocally() -- Installs the SvcmgmtStarter.exe in the current
*                         users startup folder
*
*  SYNOPSIS
*     int InstallLocally(const char *pszBinaryPathName, 
*                        const char *pszInstallSysName, 
*                        const char *pszEnvFilePath) 
*
*  FUNCTION
*     Installs the SvcmgmtStarter.exe in the current users startup folder as
*     an application that is started automatically when the user logs in.
*
*  INPUTS
*     const char *pszBinaryPathName - path of SvcmgmtStarter.exe
*     const char *pszInstallSysName - Name of GRM system
*     const char *pszEnvFilePath    - Full path to grm.env file
*
*  RESULT
*     int - did the installation succeed?
*     0:  no errors
*     >0: installation failed, see ServiceInstaller.h for error codes
*
*  NOTES
*******************************************************************************/
int InstallLocally(const char *pszBinaryPathName, const char *pszInstallSysName, const char* pszEnvFilePath)
{
   HRESULT       hr;
   WCHAR         wsz[MAX_PATH]; 
   CString       strStartupPath;
   CString       strLinkName;
   CString       strBinaryName;
   CString       strBinaryPath;
   CString       strArguments;
   int           ret = 0;
   IShellLink*   psl = NULL;
   IPersistFile* ppf = NULL; 

   try {
      GetSpecialFolderPath(CSIDL_STARTUP, strStartupPath);
      GetAbsoluteBinaryPath(pszBinaryPathName, strBinaryPath);

      // determine path of lnk file
      strLinkName = strStartupPath + "\\";
      strLinkName += pszInstallSysName;
      strLinkName += _T(".lnk");

      // Create link in startup folder
      hr = CoInitialize(NULL);
      if(hr != S_OK) {
         throw CantCoInitialize;
      }

      // Get a pointer to the IShellLink interface. 
      hr = CoCreateInstance(CLSID_ShellLink, NULL, 
            CLSCTX_INPROC_SERVER, IID_IShellLink, 
            reinterpret_cast<void**>(&psl));
      if(hr !=  S_OK) {
         throw CantCoCreateInstance;
      }

      // Set the path to the shortcut target
      hr = psl->SetPath(strBinaryPath); 
      if(hr != S_OK) {
         throw CantSetPath;
      }

      strArguments = "-local ";
      strArguments += pszInstallSysName;
      if(pszEnvFilePath != NULL) {
         strArguments += " -envfile \"";
         strArguments += pszEnvFilePath;
         strArguments += "\"";
      }
      hr = psl->SetArguments(strArguments);
      if(hr != S_OK) {
         throw CantSetArguments;
      }

      // Query IShellLink for the IPersistFile interface for saving 
      //the shortcut in persistent storage. 
      hr = psl->QueryInterface(IID_IPersistFile, 
            reinterpret_cast<void**>(&ppf)); 
      if(hr != S_OK) { 
         throw CantQueryInterface;
      }

      // Ensure that the string is ANSI. 
      MultiByteToWideChar(CP_ACP, 0, strLinkName, -1, 
            wsz, MAX_PATH); 

      // Save the link by calling IPersistFile::Save. 
      hr = ppf->Save(wsz, TRUE); 
      if(hr != S_OK) {
         throw CantSave;
      }
   } catch(int retval) {
      DWORD   dwLastError;
      char    ErrorMessage[501];

      dwLastError = GetLastError();
      FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, dwLastError, 0, ErrorMessage, 500, NULL);

      printf("Intallation failed.\n");
      printf("Error: %d: %s\n", dwLastError, ErrorMessage);
      ret = retval;
   }

   if(ppf != NULL) {
      ppf->Release(); 
   }
   if(psl != NULL) {
      psl->Release(); 
   }
   if(ret != CantCoInitialize) {
      CoUninitialize();
   } 

   return ret;
}

/****** UninstallLocally() ****************************************************
*  NAME
*     UninstallLocally() -- Uninstalls the SvcmgmtStarter.exe from the current
*                           users startup folder
*
*  SYNOPSIS
*     int UninstallLocally(const char *pszInstallSysName)
*
*  FUNCTION
*     Uninstalls the SvcmgmtStarter.exe from the current users startup folder as
*     an application that is started automatically when the user logs in.
*
*  INPUTS
*     const char *pszInstallSysName - Name of GRM system
*
*  RESULT
*     int - did the installation succeed?
*     0:  no errors
*     >0: installation failed, see ServiceInstaller.h for error codes
*
*  NOTES
*******************************************************************************/
int UninstallLocally(const char *pszInstallSysName)
{
   CString       strStartupPath;
   CString       strLinkName;
   int           ret = 0;

   try {
      GetSpecialFolderPath(CSIDL_STARTUP, strStartupPath);

      // determine path of lnk file
      strLinkName = strStartupPath + "\\";
      strLinkName += pszInstallSysName;
      strLinkName += _T(".lnk");

      // remove lnk file
      DeleteFile(strLinkName);
   } catch(int retval) {
      DWORD   dwLastError;
      char    ErrorMessage[501];

      dwLastError = GetLastError();
      FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, dwLastError, 0, ErrorMessage, 500, NULL);

      printf("Intallation failed.\n");
      printf("Error: %d: %s\n", dwLastError, ErrorMessage);
      ret = retval;
   }
   return ret;
}

/****** GetSpecialFolderPath() ****************************************************
*  NAME
*     GetSpecialFolderPath() -- Gets the absolute path to the specified special
*                               Windows folder.
*
*  SYNOPSIS
*     void GetSpecialFolderPath(int nFolder, CString &strSpecialFolderPath)
*        throw(int)
*
*  FUNCTION
*     Retrieves the full, absolute path to the specified special Windows folder.
*     See SHGetSpecialFolderLocation() help for a list of the special Windows
*     folders.
*
*  INPUTS
*     int nFolder - ID of the special folder. See SHGetSpecialFolderLocation()
*                   help for a list of the special Windows folders.
*
*  OUTPUTS
*     CString &strSpecialFolderPath - the full, absolute path to the special
*                                     folder
*
*  RESULT
*     throws an int in case of error, see ServiceInstaller.h for error codes.
*
*  NOTES
*******************************************************************************/
void GetSpecialFolderPath(int nFolder, CString &strSpecialFolderPath) throw(int)
{
   HRESULT    hr;
   BOOL       res;
   char       *pszStartupPath;
   ITEMIDLIST *pidl;
   LPMALLOC   pMalloc=NULL;
   int        ret = 0;

   try {
      pszStartupPath = strSpecialFolderPath.GetBufferSetLength(_MAX_PATH);
      if(pszStartupPath == NULL) {
         throw CantAllocateBuffer;
      }
      // get path of Startup-Folder
      hr = SHGetSpecialFolderLocation(NULL, nFolder, &pidl);
      if(hr != S_OK) {
         throw CantGetSpecialFolderLocation;
      }
      res = SHGetPathFromIDList(pidl, pszStartupPath);
      if(res == FALSE) {
         throw CantGetPathFromIDList;
      }
      // clean up buffer allocated in pidl
      hr = SHGetMalloc(&pMalloc);
      if(hr != S_OK) {
         throw CantGetMalloc;
      }
   } catch(int retval) {
      ret = retval;
   }

   if(pMalloc != NULL) {
      pMalloc->Free(pidl);
      pMalloc->Release();
   }
   strSpecialFolderPath.ReleaseBuffer();

   if(ret != 0) {
      throw ret;
   }
}

/****** GetAbsoluteBinaryPath() ***************************************************
*  NAME
*     GetAbsoluteBinaryPath() -- Gets the absolute path to the specified binary
*
*  SYNOPSIS
*     static void GetAbsoluteBinaryPath(
*                    const CString &strRelativeBinaryPath,
*                    CString &strAbsoluteBinaryPath) throw(int)
*
*  FUNCTION
*     Retrieves the full, absolute path to the specified binary.
*
*  INPUTS
*     const CString &strRelativeBinaryPath - relative path to the binary
*
*  OUTPUTS
*     CString &strAbsoluteBinaryPath - absolute path to the binary
*
*  RESULT
*     throws an int in case of error, see ServiceInstaller.h for error codes.
*
*  NOTES
*******************************************************************************/
static void GetAbsoluteBinaryPath(
               const CString &strRelativeBinaryPath,
               CString &strAbsoluteBinaryPath) throw(int)
{
   DWORD      dwBufSize = 0;
   int        ret = 0;
   LPSTR      lpszAbsoluteBinaryPath = NULL;
   LPTSTR     lptstrFilePart = NULL;

   try {
      // get path of the binary
      dwBufSize = GetFullPathName(strRelativeBinaryPath, 0, NULL, &lptstrFilePart);
      if(dwBufSize == 0) {
         throw CantGetBinaryPathSize;
      }
      lpszAbsoluteBinaryPath = strAbsoluteBinaryPath.GetBufferSetLength(dwBufSize+1);
      if(lpszAbsoluteBinaryPath == NULL) {
         throw CantAllocateBuffer;
      }
      dwBufSize = GetFullPathName(strRelativeBinaryPath, dwBufSize, lpszAbsoluteBinaryPath, &lptstrFilePart);
      if(dwBufSize == 0) {
         throw CantGetBinaryPath;
      }
      strAbsoluteBinaryPath.ReleaseBuffer();
   } catch(int retval) {
      strAbsoluteBinaryPath.ReleaseBuffer();
      throw(retval);
   }
}


// TODO: Add Event Source to EventLog for proper error logging
static void AddEventSource(const char* szAppName, const char* szAppPath) throw(int)
{
/*
   HKEY  hk; 
   DWORD dwData; 
   UCHAR szBuf[500]; 

   snprintf(szBuf, 499, "SYSTEM\\CurrentControlSet\\Services\\"
                        "EventLog\\Application\\%s", szAppName);

   // Enter name of message file to app sub key in registry
   if (RegCreateKey(HKEY_LOCAL_MACHINE, szBuf, &hk)) {
      // Panic.
   }
 
   // Set the name of the message file. 
   strcpy(szBuf, szAppPath); 
 
    // Add the name to the EventMessageFile subkey. 
 
   if (RegSetValueEx(hk,             // subkey handle 
          "EventMessageFile",       // value name 
          0,                        // must be zero 
          REG_EXPAND_SZ,            // value type 
          (LPBYTE)szBuf,            // pointer to value data 
          strlen(szBuf) + 1)) {     // length of value data 
      // Panic.
      ErrorExit("Could not set the event message file."); 
   }
 
   // Set the supported event types in the TypesSupported subkey. 
   dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | 
      EVENTLOG_INFORMATION_TYPE; 
 
   if (RegSetValueEx(hk,      // subkey handle 
          "TypesSupported",   // value name 
          0,                  // must be zero 
          REG_DWORD,          // value type 
          (LPBYTE) &dwData,   // pointer to value data 
          sizeof(DWORD))) {   // length of value data 
      // Panic.
      ErrorExit("Could not set the supported types."); 
   }

   RegCloseKey(hk); 
*/
}

static void RemoveEventSource(const char* szAppName) throw(int)
{
 // TODO
}
