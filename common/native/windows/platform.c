/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

#define _CRT_SECURE_NO_DEPRECATE
#define _WIN32_WINNT 0x0501

#include <jni.h>
#include <stdio.h>
#include <process.h>   // for getpid()
#include <windows.h>
#include <tlhelp32.h>
//#include "chown.h"

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL
Java_com_sun_grid_grm_util_WindowsPlatform_NativeExecAs(JNIEnv *env,
                                                        jobject obj,
                                                        jstring strUser,
                                                        jstring strDomain,
                                                        jstring strPassword,
                                                        jstring strCommand,
                                                        jstring strEnv[],
                                                        jstring strDir)
{
   STARTUPINFO         si; 
   PROCESS_INFORMATION pi; 
   HANDLE              hToken  = INVALID_HANDLE_VALUE;
   HANDLE              hStdout = INVALID_HANDLE_VALUE;
   HANDLE              hStderr = INVALID_HANDLE_VALUE;
   BOOL                bWait = FALSE;
   BOOL                bResult;
   DWORD               dwWait;
   char                szErrorPart[1024];
   jboolean            iscopy;

   const char *pUser     = (*env)->GetStringUTFChars(env, strUser, &iscopy);
   const char *pDomain   = (*env)->GetStringUTFChars(env, strDomain, &iscopy);
   const char *pPassword = (*env)->GetStringUTFChars(env, strPassword, &iscopy);
   const char *pCommand  = (*env)->GetStringUTFChars(env, strCommand, &iscopy);
//   const char *pEnv      = (*env)->GetStringUTFChars(env, strEnv, &iscopy);
   const char *pDir      = (*env)->GetStringUTFChars(env, strDir, &iscopy);

   ZeroMemory(&si, sizeof(si));
   ZeroMemory(&pi, sizeof(pi));
   ZeroMemory(szErrorPart, sizeof(szErrorPart));

   if(!LogonUser(
         pUser,
         pDomain,
         pPassword,
         LOGON32_LOGON_INTERACTIVE,
         LOGON32_PROVIDER_DEFAULT, 
         &hToken)) {
      sprintf(szErrorPart, "LogonUser failed:");
      goto Cleanup;
   }

   si.cb          = sizeof(STARTUPINFO); 
   si.dwFlags     |= STARTF_USESTDHANDLES;
   si.hStdOutput  = hStdout;
   si.hStdError   = hStderr;
   si.lpDesktop   = "WinSta0\\Default"; 
   si.wShowWindow = SW_SHOW; 

   // Launch the process in the client's logon session.
   bResult = CreateProcessAsUser(hToken,
      NULL,
      (char*)pCommand,
      NULL,
      NULL,
      TRUE,
      NORMAL_PRIORITY_CLASS,
      NULL, // should be (char**)pEnv,
      (char*)pDir,
      &si,
      &pi);

   if(bWait == TRUE) {
      if(bResult && pi.hProcess != INVALID_HANDLE_VALUE) {
         dwWait = WaitForSingleObjectEx(pi.hProcess, INFINITE, FALSE);
         if(dwWait==WAIT_OBJECT_0) {
            // Job ended without error
         }
         CloseHandle(pi.hProcess);
      }
   }
Cleanup:
   return bResult ? 0 : 1;
}

JNIEXPORT jboolean JNICALL
Java_com_sun_grid_grm_util_WindowsPlatform_NativeRemoveDir(JNIEnv *env,
                                                           jobject obj,
                                                           jstring strDirPath,
                                                           jboolean bForce)
{
   int            iRet;
   jboolean       iscopy;
   char           *pDirBuffer;
   SHFILEOPSTRUCT sop;
   const char     *pDirPath = (*env)->GetStringUTFChars(env, strDirPath, &iscopy);
   FILEOP_FLAGS   fFlags    = FOF_NOCONFIRMATION|FOF_NOCONFIRMMKDIR
                              |FOF_NOERRORUI|FOF_SILENT;

   pDirBuffer = (char*)malloc(strlen(pDirPath)+2);
   strcpy(pDirBuffer, pDirPath);
   pDirBuffer[strlen(pDirBuffer)+1] = '\0';

   sop.hwnd                  = NULL;
   sop.wFunc                 = FO_DELETE;
   sop.pFrom                 = pDirBuffer;
   sop.pTo                   = NULL;
   sop.fFlags                = fFlags;
   sop.fAnyOperationsAborted = FALSE;
   sop.hNameMappings         = NULL;
   sop.lpszProgressTitle     = NULL;

   iRet = SHFileOperation(&sop);
   
   free(pDirBuffer);
   (*env)->ReleaseStringUTFChars(env, strDirPath, pDirPath);

   return iRet == 0;
}
/*
JNIEXPORT jstring JNICALL
Java_com_sun_grid_grm_util_WindowsPlatform_NativeChown(JNIEnv  *env,
                                                       jobject obj,
                                                       jstring strPath,
                                                       jstring strOwner,
                                                       jboolean bRecursive)
{
   jboolean   iscopy;
   char       pError[1024];
   jstring    strError;
   const char *pPath  = (*env)->GetStringUTFChars(env, strPath, &iscopy);
   const char *pOwner = (*env)->GetStringUTFChars(env, strOwner, &iscopy);

   strcpy(pError, "");
   chown(pPath, pOwner, bRecursive, pError);
   strError = (*env)->NewStringUTF(env, pError);

   return strError;
}
*/

JNIEXPORT jboolean JNICALL
Java_com_sun_grid_grm_util_WindowsPlatform_NativeCopy(JNIEnv *env,
                                                      jobject obj,
                                                      jstring strSourcePath,
                                                      jstring strTargetPath,
                                                      jboolean bRecursive)
{
   int            iRet;
   jboolean       iscopy;
   char           *pSourceBuffer;
   char           *pTargetBuffer;
   SHFILEOPSTRUCT sop;
   const char     *pSourcePath;
   const char     *pTargetPath;
   FILEOP_FLAGS   fFlags = FOF_NOCONFIRMATION|FOF_NOCONFIRMMKDIR
                           |FOF_NOERRORUI|FOF_SILENT;

   pSourcePath = (*env)->GetStringUTFChars(env, strSourcePath, &iscopy);
   pTargetPath = (*env)->GetStringUTFChars(env, strTargetPath, &iscopy);

   pSourceBuffer = (char*)malloc(strlen(pSourcePath)+2);
   strcpy(pSourceBuffer, pSourcePath);
   pSourceBuffer[strlen(pSourceBuffer)+1] = '\0';

   pTargetBuffer = (char*)malloc(strlen(pTargetPath)+2);
   strcpy(pTargetBuffer, pTargetPath);
   pTargetBuffer[strlen(pTargetBuffer)+1] = '\0';

   if (bRecursive == JNI_FALSE) {
      fFlags |= FOF_NORECURSION;
   }

   sop.hwnd                  = NULL;
   sop.wFunc                 = FO_COPY;
   sop.pFrom                 = pSourceBuffer;
   sop.pTo                   = pTargetBuffer;
   sop.fFlags                = fFlags;
   sop.fAnyOperationsAborted = FALSE;
   sop.hNameMappings         = NULL;
   sop.lpszProgressTitle     = NULL;

   iRet = SHFileOperation(&sop);
   
   free(pSourceBuffer);
   free(pTargetBuffer);

   (*env)->ReleaseStringUTFChars(env, strSourcePath, pSourcePath);
   (*env)->ReleaseStringUTFChars(env, strTargetPath, pTargetPath);

   return iRet == 0;
}

JNIEXPORT jint JNICALL
Java_com_sun_grid_grm_util_WindowsPlatform_getNativePid(JNIEnv *env,
                                                        jobject obj)
{
   return _getpid();
}

JNIEXPORT jboolean JNICALL
Java_com_sun_grid_grm_util_WindowsPlatform_isNativeSuperUser(JNIEnv *env,
                                                             jobject obj)
{
   BOOL       bRet;
   HANDLE     hProcess;
   HANDLE     hToken;
   DWORD      dwNeeded;
   TOKEN_USER token_user;

   hProcess = GetCurrentProcess();
   bRet = OpenProcessToken(hProcess, TOKEN_QUERY, &hToken);
   if(bRet) {
      bRet = GetTokenInformation(hToken, TokenUser, &token_user,
                                 sizeof(token_user), &dwNeeded);
      if(bRet) {
         bRet = IsWellKnownSid(token_user.User.Sid,
                               WinBuiltinAdministratorsSid);
      }
      CloseHandle(hToken);
   }
   CloseHandle(hProcess);

   return bRet;
}

JNIEXPORT jboolean JNICALL
Java_com_sun_grid_grm_util_WindowsPlatform_existsNativeProcess(JNIEnv *env,
                                                             jobject obj,
                                                             jint pid)
{
    HANDLE hSnapShot=CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
    if (hSnapShot==NULL) {
        return TRUE; 
    }

    PROCESSENTRY32 processInfo;
 
    PROCESSENTRY32 *pointer = &processInfo;

    pointer->dwSize=sizeof(PROCESSENTRY32);
 
    while (Process32Next(hSnapShot, &processInfo)!=FALSE) {
       if (pid==pointer->th32ProcessID) { 
          return TRUE;
       }
    }
    CloseHandle(hSnapShot);
 
    return FALSE;
}


#ifdef __cplusplus
}
#endif

