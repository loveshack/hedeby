/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

#include <stdio.h>
#include <stdlib.h>
#include <direct.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <afxtempl.h>
#include <afxmt.h>

static CString basename(const char* path);
static CString dirname(const char* path);
static CString pwd();
static CString which(const char* binary);
static BOOL    exists(const char* path);
static BOOL    directory(const char* path);
static int     eval(const char *command, const char *arguments);


/*
 * This is the 1:1 copy of the shell script gconf.
 * It doesn't look like good C++ code because it wants to be as much
 * as possible similar to the shell script!
 * This should make later changes easier.
 */
int main(int argc, char *argv[])
{
   int     ret;
   CString BASEDIR;
   CString CLASS;
   CString CMD;
   CString GRM_DIST;
   CString APPL_ARGS;
   CString JVM_ARGS;
   CString JAVA_HOME;
   CString ARCH;
   CString JAVA;

   CMD = basename(argv[0]);

   if(CMD.CompareNoCase("gconf.exe")==0 || CMD.CompareNoCase("gconf")==0) {
      CLASS = "com.sun.grid.grm.cli.GConf";
   } else if(CMD.CompareNoCase("gstat.exe")==0 || CMD.CompareNoCase("gstat")==0) {
      CLASS = "com.sun.grid.grm.cli.GStat";
   } else {
      printf("Bad command name: %s\n", argv[0]);
      exit(1);
   }

   BASEDIR = dirname(argv[0]);
   chdir(BASEDIR);
   BASEDIR = pwd();
   GRM_DIST = BASEDIR + "\\..";

   APPL_ARGS = "";
   JVM_ARGS = "";
   // parse the command args
   for(int i=1; i<argc; i++) {
      if(strcmp(argv[i], "-D")==0) {
         JVM_ARGS = JVM_ARGS + " -Xdebug -Xrunjdwp:transport=dt_socket,"
                               "server=y,suspend=y";
      } else {
         APPL_ARGS = APPL_ARGS + " \"" + argv[i] + "\"";
      }
   }

   if(JAVA_HOME == "") {
      JAVA = which("java.exe");
   } else {
      JAVA = JAVA_HOME + "\\bin\\java.exe";
   }

   if(!(exists(JAVA) && !directory(JAVA))) {
      printf("Cannot execute java %s\n", JAVA);
      exit(1);
   }

   ARCH = "win32-x86";
   SetEnvironmentVariable("ARCH", ARCH);

   /*
    * ARCH-dependent JVM_ARGS are not necessary here (see gconf shell script)
    */

   JVM_ARGS = JVM_ARGS + " -Djava.library.path=" + GRM_DIST + "\\lib\\" + ARCH;

   ret = eval(JAVA, JVM_ARGS + " -jar " + GRM_DIST + "\\lib\\grm_starter.jar " +
                        CLASS + " " + APPL_ARGS);
   return ret;
}

static CString basename(const char* path)
{
   DWORD   dwBufLength;
   CString strBaseName;
   char    *pBuf;
   int     Pos;

   // Get full path of executable
   pBuf = strBaseName.GetBufferSetLength(MAX_PATH+1);
   dwBufLength = GetModuleFileName(NULL, pBuf, MAX_PATH);
   strBaseName.ReleaseBuffer();

   // Cut filename from end of full path
   Pos = strBaseName.ReverseFind('\\');
   strBaseName = strBaseName.Mid(Pos+1);

   return strBaseName;
}

static CString dirname(const char* path)
{
   DWORD   dwBufLength;
   CString strDirName;
   char    *pBuf;
   int     Pos;

   // Get full path of executable
   pBuf = strDirName.GetBufferSetLength(MAX_PATH+1);
   dwBufLength = GetModuleFileName(NULL, pBuf, MAX_PATH);
   strDirName.ReleaseBuffer();

   // Cut filename from end of full path
   Pos = strDirName.ReverseFind('\\');
   strDirName = strDirName.Left(Pos);

   return strDirName;
}

static CString pwd()
{
   CString strPwd;
   char    *pBuf;

   pBuf = strPwd.GetBufferSetLength(MAX_PATH+1);
   getcwd(pBuf, MAX_PATH);
   strPwd.ReleaseBuffer();

   return strPwd;
}

static CString which(const char* binary)
{
   CString strWhich;
   char    *pBuf;

   pBuf = strWhich.GetBufferSetLength(MAX_PATH+1);
   _searchenv(binary, "PATH", pBuf);   
   strWhich.ReleaseBuffer();

   return strWhich;
}

static BOOL exists(const char* path)
{
   struct _stat buf;
   int          ret;
      
   ret = _stat(path, &buf);
   return ret==0;
}

static BOOL directory(const char* path)
{
   struct _stat buf;
   int          ret;

   ret = _stat(path, &buf);
   return ret==0 && (buf.st_mode & _S_IFDIR)!=0;
}

static int eval(const char *command, const char *arguments)
{
   BOOL                  bRet;
   STARTUPINFO           si;
   PROCESS_INFORMATION   pi;
   char                  *pCommandLine;

   memset(&pi, 0, sizeof(pi));
   memset(&si, 0, sizeof(si));

   si.cb = sizeof(STARTUPINFO);

   pCommandLine = strdup(arguments);

   bRet = CreateProcess(
      command,
      pCommandLine,
      NULL,
      NULL,
      FALSE,
      0,
      NULL,
      pwd(),
      &si,
      &pi);

   if(bRet == TRUE) {
      if(pi.hProcess != INVALID_HANDLE_VALUE) {
         DWORD dwWait;

         dwWait = WaitForSingleObjectEx(pi.hProcess, INFINITE, FALSE);
         if(dwWait == WAIT_OBJECT_0) {
            bRet = TRUE;
         } else {
            bRet = FALSE;
         }
      }
   }

   free(pCommandLine);

   if(pi.hThread != INVALID_HANDLE_VALUE) {
      CloseHandle(pi.hThread);
   } 
   if(pi.hProcess != INVALID_HANDLE_VALUE) {
      CloseHandle(pi.hProcess);
   } 
   return bRet?0:1;
}
