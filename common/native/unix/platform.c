/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

#include <jni.h>
#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <pwd.h>

#ifdef __cplusplus
extern "C" {
#endif

#define DEFAULT_EDITOR     "vi"

static struct termios init_set;
static int initialized = 0;

static void sge_set_def_sig_mask(sigset_t* sig_num);
static void sge_unblock_all_signals(void);
static int edit(const char* fname, uid_t myuid, gid_t mygid);
static int exec_cmd(const char* cmd);


static int get_pw_buffer_size(void);

#ifdef IRIX
#  define SGE_STAT(filename, buffer) stat64(filename, buffer)
#  define SGE_LSTAT(filename, buffer) lstat64(filename, buffer)
#  define SGE_FSTAT(filename, buffer) fstat64(filename, buffer)
#  define SGE_STRUCT_STAT struct stat64
#  define SGE_INO_T ino64_t
#  define SGE_OFF_T off64_t
#elif defined(SOLARIS)
#  define SGE_STAT(filename, buffer) stat64(filename, buffer)
#  define SGE_LSTAT(filename, buffer) lstat64(filename, buffer)
#  define SGE_FSTAT(filename, buffer) fstat64(filename, buffer)
#  define SGE_STRUCT_STAT struct stat64
#  define SGE_INO_T ino64_t
#  define SGE_OFF_T off64_t
#elif defined(INTERIX)
#  define SGE_STAT(filename, buffer) wl_stat(filename, buffer)
#  define SGE_LSTAT(filename, buffer) lstat(filename, buffer)
#  define SGE_FSTAT(filedes, buffer) fstat(filedes, buffer)
#  define SGE_STRUCT_STAT struct stat
#  define SGE_INO_T ino_t
#  define SGE_OFF_T off_t
#else
#  define SGE_STAT(filename, buffer) stat(filename, buffer)
#  define SGE_LSTAT(filename, buffer) lstat(filename, buffer)
#  define SGE_FSTAT(filename, buffer) fstat(filename, buffer)
#  define SGE_STRUCT_STAT struct stat
#  define SGE_INO_T ino_t
#  define SGE_OFF_T off_t
#endif

#ifndef MAX
#define MAX(_a,_b) ((_a)>(_b)?(_a):(_b))
#endif

static struct termios getTerminalSettings();


struct termios getTerminalSettings() {
   if(initialized == 0) {
      tcgetattr(fileno(stdin), &init_set);
      initialized = 1;
   }
   return init_set;
}


/*
 * Class:     com_sun_grid_grm_util_UnixPlatform
 * Method:    setEcho
 * Signature: (Z)V
 */
JNIEXPORT void JNICALL Java_com_sun_grid_grm_security_Password_setEcho
(JNIEnv *env, jclass password, jboolean flag) {
   
   struct termios new_set = getTerminalSettings();
   
   if(flag) {
      tcsetattr(fileno(stdin), TCSAFLUSH, &init_set);
   } else {
      new_set.c_lflag &= ~ECHO;
      tcsetattr(fileno(stdin), TCSAFLUSH, &new_set);
   }
}

JNIEXPORT void JNICALL Java_com_sun_grid_grm_util_UnixPlatform_initTerminal(JNIEnv *env, jclass aClass) {
   getTerminalSettings();
}

JNIEXPORT void JNICALL Java_com_sun_grid_grm_util_UnixPlatform_resetTerminal(JNIEnv *env, jclass aClass) {
   tcsetattr(fileno(stdin), TCSAFLUSH, &init_set);
}

JNIEXPORT jint JNICALL Java_com_sun_grid_grm_util_UnixPlatform_getNativePid(JNIEnv *env, jobject obj) {
	return getpid();
}
JNIEXPORT jint JNICALL Java_com_sun_grid_grm_util_UnixPlatform_getNativeUid(JNIEnv *env, jobject obj) {
	return getuid();
}
JNIEXPORT jint JNICALL Java_com_sun_grid_grm_util_UnixPlatform_killNativeProcess(JNIEnv *env, jobject obj, jint pid) {
	return kill(pid, SIGKILL);
}

JNIEXPORT jint JNICALL Java_com_sun_grid_grm_util_UnixPlatform_terminateNativeProcess(JNIEnv *env, jobject obj, jint pid) {
	return kill(pid, SIGTERM);
}

JNIEXPORT jint JNICALL Java_com_sun_grid_grm_util_UnixPlatform_executeNativeScript(JNIEnv *env, jobject obj, jstring command) {
        jint ret =0;
        const char *cmd = (*env)->GetStringUTFChars(env, command, 0);
	ret = exec_cmd(cmd);
        (*env)->ReleaseStringUTFChars(env, command, cmd);
        return ret;

}

int exec_cmd(const char* cmd) {
    /*
     * Use thread safe popen to execute the command, and wait for return of the command
     */
    FILE *p;
    if ((p = popen(cmd, "w")) == NULL) {
        return (-1);
    }
    return (pclose(p));
}


JNIEXPORT jint JNICALL Java_com_sun_grid_grm_util_UnixPlatform_nativeEdit(JNIEnv *env, jobject obj, jstring path_obj) {

   const char* path;
   jint ret = 0;
   
   /* Initialize the terminal settings
      Shutdown handler of the calling method will reset then terminal setting
   */
   getTerminalSettings();
   
   path = (*env)->GetStringUTFChars(env, path_obj, 0);
   
   ret = edit(path, geteuid(), getegid());

   (*env)->ReleaseStringUTFChars(env, path_obj, path);
   
   return ret;
}

int edit(const char* fname, uid_t myuid, gid_t mygid) {

   SGE_STRUCT_STAT before, after;
   pid_t pid;
   int status;
   int ws;

   if (fname == NULL) {
      return -1;
   }

   if (SGE_STAT(fname, &before)) {
      return -1;
   }

   pid = fork();
   if (pid) {
      while (ws != pid) {
         ws = waitpid(pid, &status, 0);
         if (WIFEXITED(status)) {
            if (WEXITSTATUS(status) != 0) {
               return -1;
            }
            else {
               if (SGE_STAT(fname, &after)) {
                  return -1;
               }
               if ((before.st_mtime != after.st_mtime) ||
                    (before.st_size != after.st_size)) {
                  return 0;
               }
               else {
                  /* file is unchanged; inform caller */
                  return 1;
               }
            }
         }
#ifndef WIN32  /* signals b18 */
         if (WIFSIGNALED(status)) {
            return -1;
         }
#endif
      }
   } else {
      const char *cp = NULL;

      sge_set_def_sig_mask(NULL);
      sge_unblock_all_signals();
      setuid(getuid());
      setgid(getgid());

      cp = getenv("EDITOR");
      if (!cp || strlen(cp) == 0)
         cp = DEFAULT_EDITOR;

      execlp(cp, cp, fname, (char *) 0);
      exit(1);
   }
}

void sge_set_def_sig_mask(sigset_t* sig_num)
{
   int i = 1;
   struct sigaction sig_vec;

   while (i < NSIG) {
      /*
       * never set default handler for
       * SIGKILL and SIGSTOP
       */
      if ((i == SIGKILL) || (i == SIGSTOP)) {
         i++;
         continue;
      }

      /*
       * on HPUX don't set default handler for
       * _SIGRESERVE and SIGDIL
       */
#if defined(HPUX)
      if ((i == _SIGRESERVE) || (i == SIGDIL)) {
         i++;
         continue;
      }
#endif

      /*
       * never set default handler for signals set
       * in sig_num if not NULL
       */
      if (sig_num != NULL && sigismember(sig_num, i)) {
         i++;
         continue;
      }

      errno = 0;
      sigemptyset(&sig_vec.sa_mask);
      sig_vec.sa_flags = 0;
      sig_vec.sa_handler = 0;
      sig_vec.sa_handler = SIG_DFL;
      if (sigaction(i, &sig_vec, NULL)) {
#if 0
         /* Should we log a error message here */
         if (err_func) {
            char err_str[256];
            snprintf(err_str, 256, MSG_PROC_SIGACTIONFAILED_IS, i, strerror(errno));
            err_func(err_str);
         }
#endif
      }
      i++;
   }
}

void sge_unblock_all_signals(void)
{
   sigset_t sigmask;
   /* unblock all signals */
   /* without this we depend on shell to unblock the signals */
   /* result is that SIGXCPU was not delivered with several shells */
   sigemptyset(&sigmask);
   sigprocmask(SIG_SETMASK, &sigmask, NULL);
}

/**
 *   Get the UID of the owner a file
 *
 *   @param  env the JNI env
 *   @param  obj the this pointer
 *   @param  path_obj path to the file
 *   @return the UID of the onwer of the file or -1 if an error occurred
 */
JNIEXPORT jint JNICALL Java_com_sun_grid_grm_util_UnixPlatform_getUID(JNIEnv *env, jobject obj, jstring path_obj) {
   const char* path;
   SGE_STRUCT_STAT st;
   jint ret = -1;
   path = (*env)->GetStringUTFChars(env, path_obj, 0);
   if (path != NULL && SGE_STAT(path, &st) == 0) {
       ret = st.st_uid;
   }
   (*env)->ReleaseStringUTFChars(env, path_obj, path);
   return ret;
}

#ifndef MAX_NIS_RETRIES
#  define MAX_NIS_RETRIES 10
#endif    

/**
 *  Get the name of a user by its id
 *  @param env the JNI env
 *  @param obj the this pointer
 *  @param uid  the uid of user
 *  @return  The name of the user or NULL on error
 */
JNIEXPORT jstring JNICALL Java_com_sun_grid_grm_util_UnixPlatform_uid2User(JNIEnv *env, jobject obj, jint uid) {
   struct  passwd *pw = NULL;
   struct  passwd pwentry;
   int     size;
   char    *pw_buffer;
   int     retries = MAX_NIS_RETRIES;
   jstring ret = NULL;
   size = get_pw_buffer_size();
   pw_buffer = malloc(size);

   /* max retries that are made resolving user name */
   while (getpwuid_r(uid, &pwentry, pw_buffer, size, &pw) != 0 || !pw) {
      if (!retries--) {
         free(pw_buffer);
         return NULL;
      }
      sleep(1);
   }
   if (pw != NULL) {
      ret = (*env)->NewStringUTF(env, pw->pw_name);
   }
   free(pw_buffer);
   return ret;
}

/**
 *  Determines the buffer size for getpwuid_r
 */
static int get_pw_buffer_size(void)
{
   enum { buf_size = 20480 };  /* default is 20 KB */
   
   int sz = buf_size;

#ifdef _SC_GETPW_R_SIZE_MAX
   if ((sz = (int)sysconf(_SC_GETPW_R_SIZE_MAX)) == -1) {
      sz = buf_size;
   } else {
      sz = MAX(sz, buf_size);
   }
#endif
   return sz;
}

#ifdef __cplusplus
}
#endif
