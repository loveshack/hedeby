/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 * 
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 * 
 *  Sun Microsystems Inc., March, 2001
 * 
 * 
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 * 
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 * 
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 * 
 *   Copyright: 2008 by Sun Microsystems, Inc.
 * 
 *   All Rights Reserved.
 * 
 ************************************************************************/
/*___INFO__MARK_END__*/

#ifdef ADD_COPYRIGHT
#  include "copyright.h"
#endif

#if !(ADD_COPYRIGHT || ADD_SUN_COPYRIGHT)
const char SFLN_ELN[] = "\n\
   Grid Engine is based on code donated by Sun Microsystems.\n\
   The copyright is owned by Sun Microsystems and other contributors.\n\
   It has been made available to the open source community under the SISSL license.\n\
   For further information and the latest news visit: @fBhttp://gridengine.sunsource.net\n\n";

#endif

#ifndef ADD_SUN_COPYRIGHT

const char SISSL[] = "\n\
The Contents of this file are made available subject to the terms of\n\
the Sun Industry Standards Source License Version 1.2\n\
\n\
Sun Microsystems Inc., March, 2001\n\
\n\
\n\
Sun Industry Standards Source License Version 1.2\n\
=================================================\n\
The contents of this file are subject to the Sun Industry Standards\n\
Source License Version 1.2 (the \"License\"); You may not use this file\n\
except in compliance with the License. You may obtain a copy of the\n\
License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html\n\
\n\
Software provided under this License is provided on an \"AS IS\" basis,\n\
WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,\n\
WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,\n\
MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.\n\
See the License for the specific provisions governing your rights and\n\
obligations concerning the Software.\n\
\n\
The Initial Developer of the Original Code is: Sun Microsystems, Inc.\n\
\n\
Copyright: 2008 by Sun Microsystems, Inc.\n\
\n\
All Rights Reserved.\n"; 

#endif /* ADD_SUN_COPYRIGHT */
