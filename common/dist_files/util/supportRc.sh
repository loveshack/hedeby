#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2008 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__

# Installation/Uninstallation script for Rc Scripts support.
# This script should not be used as standalone script. It is used by
# SDM administrative command: sdmadm to install/uninstall autostart
# of system feature

#---------------------------------------------------------------------------
# Install Rc script support on host
InstallRcScript()
{  
   # If system is Linux Standard Base (LSB) compliant, use the install_initd utility
   if [ "$RC_FILE" = lsb ]; then
      echo cp $SDM_STARTUP_FILE $RC_PREFIX/$STARTUP_FILE_NAME
      echo /usr/lib/lsb/install_initd $RC_PREFIX/$STARTUP_FILE_NAME
      Execute cp $SDM_STARTUP_FILE $RC_PREFIX/$STARTUP_FILE_NAME
      Execute /usr/lib/lsb/install_initd $RC_PREFIX/$STARTUP_FILE_NAME
      # Several old Red Hat releases do not create proper startup links from LSB conform
      # scripts. So we need to check if the proper links were created.
      # See RedHat: https://bugzilla.redhat.com/bugzilla/long_list.cgi?buglist=106193
      if [ -f "/etc/redhat-release" -o -f "/etc/fedora-release" ]; then
         # According to Red Hat documentation all rcX.d directories are in /etc/rc.d
         # we hope this will never change for Red Hat
         RCD_PREFIX="/etc/rc.d"
         for runlevel in 0 1 2 3 4 5 6; do
            # check for a corrupted startup link
            if [ -L "$RCD_PREFIX/rc$runlevel.d/S-1$STARTUP_FILE_NAME" ]; then
               Execute rm -f $RCD_PREFIX/rc$runlevel.d/S-1$STARTUP_FILE_NAME
               # create new correct startup link
               if [ $runlevel -eq 3 -o $runlevel -eq 5 ]; then
                  Execute rm -f $RCD_PREFIX/rc$runlevel.d/$S99NAME
                  Execute ln -s $RC_PREFIX/$STARTUP_FILE_NAME $RCD_PREFIX/rc$runlevel.d/$S99NAME
               fi
            fi
            # check for a corrupted shutdown link
            if [ -L "$RCD_PREFIX/rc$runlevel.d/K-1$STARTUP_FILE_NAME" ]; then
               Execute rm -f $RCD_PREFIX/rc$runlevel.d/K-1$STARTUP_FILE_NAME
               Execute rm -f $RCD_PREFIX/rc$runlevel.d/$K02NAME
               # create new correct shutdown link
               if [ $runlevel -eq 0 -o $runlevel -eq 1 -o $runlevel -eq 2 -o $runlevel -eq 6 ]; then
                  Execute rm -f $RCD_PREFIX/rc$runlevel.d/$K02NAME
                  Execute ln -s $RC_PREFIX/$STARTUP_FILE_NAME $RCD_PREFIX/rc$runlevel.d/$K02NAME
               fi
            fi
         done
      fi
   # If we have System V we need to put the startup script to $RC_PREFIX/init.d
   # and make a link in $RC_PREFIX/rc2.d to $RC_PREFIX/init.d
   elif [ "$RC_FILE" = "sysv_rc" ]; then
      Execute rm -f $RC_PREFIX/$RC_DIR/$S99NAME
      Execute rm -f $RC_PREFIX/$RC_DIR/$K02NAME
      Execute cp $SDM_STARTUP_FILE $RC_PREFIX/init.d/$STARTUP_FILE_NAME
      Execute chmod a+x $RC_PREFIX/init.d/$STARTUP_FILE_NAME
      Execute ln -s $RC_PREFIX/init.d/$STARTUP_FILE_NAME $RC_PREFIX/$RC_DIR/$S99NAME
      Execute ln -s $RC_PREFIX/init.d/$STARTUP_FILE_NAME $RC_PREFIX/$RC_DIR/$K02NAME

      # runlevel management in Linux is different -
      # each runlevel contains full set of links
      # RedHat uses runlevel 5 and SUSE runlevel 3 for xdm
      # RedHat uses runlevel 3 for full networked mode
      # Suse uses runlevel 2 for full networked mode
      # we already installed the script in level 3
      SDM_ARCH=`$SDM_DIST/util/arch`
      case $SDM_ARCH in
      lx2?-*)
         runlevel=`grep "^id:.:initdefault:"  /etc/inittab | cut -f2 -d:`
         if [ "$runlevel" = 2 -o  "$runlevel" = 5 ]; then
            Execute rm -f $RC_PREFIX/rc${runlevel}.d/$S99NAME
            Execute rm -f $RC_PREFIX/rc${runlevel}.d/$K02NAME
            Execute ln -s $RC_PREFIX/init.d/$STARTUP_FILE_NAME $RC_PREFIX/rc${runlevel}.d/$S99NAME
            Execute ln -s $RC_PREFIX/init.d/$STARTUP_FILE_NAME $RC_PREFIX/rc${runlevel}.d/$K02NAME
         fi
         ;;
       esac

   elif [ "$RC_FILE" = "insserv-linux" ]; then
      echo  cp $SDM_STARTUP_FILE $RC_PREFIX/$STARTUP_FILE_NAME
      echo /sbin/insserv $RC_PREFIX/$STARTUP_FILE_NAME
      Execute cp $SDM_STARTUP_FILE $RC_PREFIX/$STARTUP_FILE_NAME
      /sbin/insserv $RC_PREFIX/$STARTUP_FILE_NAME
   elif [ "$RC_FILE" = "update-rc.d" ]; then
      # let Debian install scripts according to defaults
      echo  cp $SDM_STARTUP_FILE $RC_PREFIX/$STARTUP_FILE_NAME
      echo /usr/sbin/update-rc.d $STARTUP_FILE_NAME
      Execute cp $SDM_STARTUP_FILE $RC_PREFIX/$STARTUP_FILE_NAME
      /usr/sbin/update-rc.d $STARTUP_FILE_NAME defaults 99 02
   elif [ "$RC_FILE" = "freebsd" ]; then
      echo  cp $SDM_STARTUP_FILE $RC_PREFIX/sdm${RC_SUFFIX}
      Execute cp $SDM_STARTUP_FILE $RC_PREFIX/sdm${RC_SUFFIX}
   else
      # if this is not System V we simple add the call to the
      # startup script to RC_FILE

      # Start-up script already installed?
      #------------------------------------
      grep $STARTUP_FILE_NAME $RC_FILE > /dev/null 2>&1
      status=$?
      if [ $status != 0 ]; then
         cat $RC_FILE | sed -e "s/exit 0//g" > $RC_FILE.new.1 2>/dev/null
         cp $RC_FILE $RC_FILE.save_sdm
         cp $RC_FILE.new.1 $RC_FILE
         # Add the procedure
         #------------------
         $ECHO "" >> $RC_FILE
         $ECHO "# SDM start up" >> $RC_FILE
         $ECHO "#-$LINE---------" >> $RC_FILE
         $ECHO $SDM_STARTUP_FILE >> $RC_FILE
         $ECHO "exit 0" >> $RC_FILE
         rm $RC_FILE.new.1
      else
         echo "SDM startup in RC_FILE: $RC_FILE already found."        
      fi
   fi
}

#---------------------------------------------------------------------------
# Remove Rc script support from the host
RemoveRcScript()
{
   # If system is Linux Standard Base (LSB) compliant, use the install_initd utility
   if [ "$RC_FILE" = lsb ]; then
      echo /usr/lib/lsb/remove_initd $RC_PREFIX/$STARTUP_FILE_NAME
      Execute /usr/lib/lsb/remove_initd $RC_PREFIX/$STARTUP_FILE_NAME
      # Several old Red Hat releases do not create/remove startup links from LSB conform
      # scripts. So we need to check if the links were deleted.
      # See RedHat: https://bugzilla.redhat.com/bugzilla/long_list.cgi?buglist=106193
      if [ -f "/etc/redhat-release" -o -f "/etc/fedora-release" ]; then
         RCD_PREFIX="/etc/rc.d"
         # Are all startup links correctly removed?
         for runlevel in 3 5; do
            if [ -L "$RCD_PREFIX/rc$runlevel.d/$S99NAME" ]; then
               Execute rm -f $RCD_PREFIX/rc$runlevel.d/$S99NAME
            fi
         done
         # Are all shutdown links correctly removed?
         for runlevel in 0 1 2 6; do
            if [ -L "$RCD_PREFIX/rc$runlevel.d/$K02NAME" ]; then
               Execute rm -f $RCD_PREFIX/rc$runlevel.d/$K02NAME
            fi
         done
      fi
   # If we have System V we need to put the startup script to $RC_PREFIX/init.d
   # and make a link in $RC_PREFIX/rc2.d to $RC_PREFIX/init.d
   elif [ "$RC_FILE" = "sysv_rc" ]; then
      Execute rm -f $RC_PREFIX/$RC_DIR/$S99NAME
      Execute rm -f $RC_PREFIX/$RC_DIR/$K02NAME
      Execute rm -f $RC_PREFIX/init.d/$STARTUP_FILE_NAME

      # runlevel management in Linux is different -
      # each runlevel contains full set of links
      # RedHat uses runlevel 5 and SUSE runlevel 3 for xdm
      # RedHat uses runlevel 3 for full networked mode
      # Suse uses runlevel 2 for full networked mode
      # we already installed the script in level 3
      SDM_ARCH=`$SDM_DIST/util/arch`
      case $SDM_ARCH in
      lx2?-*)
         runlevel=`grep "^id:.:initdefault:"  /etc/inittab | cut -f2 -d:`
         if [ "$runlevel" = 2 -o  "$runlevel" = 5 ]; then
            Execute rm -f $RC_PREFIX/rc${runlevel}.d/$S99NAME
            Execute rm -f $RC_PREFIX/rc${runlevel}.d/$K02NAME
         fi
         ;;
       esac

   elif [ "$RC_FILE" = "insserv-linux" ]; then
      echo  rm $SDM_STARTUP_FILE $RC_PREFIX/$STARTUP_FILE_NAME
      echo /sbin/insserv -r $RC_PREFIX/$STARTUP_FILE_NAME
      Execute rm $SDM_STARTUP_FILE $RC_PREFIX/$STARTUP_FILE_NAME
      /sbin/insserv -r $RC_PREFIX/$STARTUP_FILE_NAME
   elif [ "$RC_FILE" = "freebsd" ]; then
      echo  rm $SDM_STARTUP_FILE $RC_PREFIX/sdm${RC_SUFFIX}
      Execute rm $SDM_STARTUP_FILE $RC_PREFIX/sdm${RC_SUFFIX}
   else
      # if this is not System V we simple remove the call from the
      # RC_FILE

      # Start-up script already installed?
      #------------------------------------
      grep $STARTUP_FILE_NAME $RC_FILE > /dev/null 2>&1
      status=$?
      if [ $status = 0 ]; then
         mv $RC_FILE.sdm_uninst.3 $RC_FILE.sdm_uninst.4
         mv $RC_FILE.sdm_uninst.2 $RC_FILE.sdm_uninst.3
         mv $RC_FILE.sdm_uninst.1 $RC_FILE.sdm_uninst.2
         mv $RC_FILE.sdm_uninst $RC_FILE.sdm_uninst.1

         cat $RC_FILE | sed -e "s/# SDM start up//g" | sed -e "s/$SDM_STARTUP_FILE//g"  > $RC_FILE.new.1 2>/dev/null
         cp $RC_FILE $RC_FILE.sdm_uninst
         cp $RC_FILE.new.1 $RC_FILE
         
         rm $RC_FILE.new.1
      fi
   fi
}

#---------------------------------------------------------------------------
# Show the usage of the command
USAGE()
{
   echo "Usage: supportRc <command>"
   echo ""
   echo "Commands:"
   echo "   install   [sdm_dist_dir]   ... install SDM Rc Scripts support"
   echo "   uninstall [sdm_dist_dir]   ... uninstall SDM Rc Scripts support"
   echo "   help            ... this help"
   echo
}


#---------------------------------------------------------------------------
# Main routine
# PARAMS: $command 
# return a stdout output
SUPPORTRC()
{   
  
   cmd="$1"
   args="$#"
   if [ "$args" -gt 2 -o "$args" -lt 1 ]; then
      USAGE
      exit 1
   fi

   if [ "$args" -eq 1 -a "$cmd" != "help" ]; then
      USAGE
      exit 1 
   fi

   if [ "$cmd" != "install" -a "$cmd" != "uninstall" ]; then
      USAGE
      exit 1   
   fi

   SDM_DIST="$2"
   if [ ! -f $SDM_DIST/util/arch_variables ]; then
      echo
      echo ERROR: Missing shell script \"$SDM_DIST/util/arch_variables\".
      echo
      exit 1
   fi

   . $SDM_DIST/util/arch_variables

   STARTUP_FILE_NAME="sdmsvc"
   SDM_STARTUP_FILE="$SDM_DIST/util/templates/sdmsvc"
   S99NAME="S99sdmsvc"
   K02NAME="K02sdmsvc"

   if [ "$cmd" = "install" ]; then
      InstallRcScript
   else
      RemoveRcScript
   fi
   return 0
}

SUPPORTRC $@
