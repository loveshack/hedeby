#!/bin/sh
#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2008 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__
#
# Solaris Service Management Facility (SMF) support script
#
# This script is used by SDM to check whether the SMF is supported on host
# and also for installation/uninstallation of SMF services.
# This script should not be invoked independently, it is used by sdmadm command.
# Exit codes:
# 0 - success
# 1 - error
# 2 - error SMF not supported

 
#Setting paths for svcadm, svccfg 
SVCADM="/usr/sbin/svcadm"
SVCCFG="/usr/sbin/svccfg"


#check whether smf is available
hasSMF=0
if [ -f /lib/svc/share/smf_include.sh ]; then 
   . /lib/svc/share/smf_include.sh
   smf_present
   hasSMF="$?"
   if [ $hasSMF -ne 0 ]; then
      hasSMF=0
   else
      hasSMF=1
   fi
else
   hasSMF=0
fi

#---------------------------------------------------------------------------
# Show the usage of the standalone command
SMFusage()
{
   echo "Usage: sdm_smf <command>"
   echo ""
   echo "Commands:"
   echo "   register   [descriptor file]   ... register SDM jvm as SMF service"
   echo "   unregister [service name]   ... unregister SDM jvm SMF service"
   echo "   supported       ... check if the SMF can be used on current host"
   echo "   help            ... this help"
   echo
}

#-------------------------------------------------------------------------
# SMFImportService: Import service descriptor
#
SMFImportService() 
{
   file="$1"
   SMFValidateContent $file
   if [ "$?" != 0 ]; then
      echo "Service descriptor is corrupted, exiting!!!"
      exit 1
   fi
   #Strangly svccfg import does not return non-zero exit code if there was 
   #an error such as permission denied, so just wait for some output
   res=`$SVCCFG import $file 2>&1 | head -1`
   if [ -n "$res" ]; then
      echo $res
      echo "Importing service was not successful!!!"
      exit 1
   fi
}

#-------------------------------------------------------------------------
# SMFValidateContent: Validate service descriptor
#
SMFValidateContent()
{
   return `$SVCCFG validate "$1"`
}


#---------------------------------------------------------------------------
# Register SMF service
# return Registered messages or nothing
SMFRegister()
{  

   if [ ! -f "$1" ]; then 
        echo "File: $1 not found"
        exit 1
   fi
   SMFImportService $1 
   return 0
}


#-------------------------------------------------------------------------
# SMFHaltAndDeleteService: Stops the service and deletes it
# arg1 ... SMF service name
SMFHaltAndDeleteService()
{
   service_name="$1"
   $INFOTEXT "$1 is going down ...., please wait!"
   $SVCADM disable -s "$service_name"
   if [ "$?" -ne 0 ]; then
      #Maybe maintenance state we need request disable without admin
      #doing it manually
      $SVCADM disable "$service_name"
      if [ "$?" -ne 0 ]; then
         echo "Could not disable the service \"$service_name\"! Exiting"
         exit 1
      fi   
   fi
   sleep 1
   $INFOTEXT "$1 is down!"
   $SVCCFG delete "$service_name"
   if [ "$?" -ne 0 ]; then
      echo "Could not delete the service \"$service_name\"! Exiting"
      exit 1
   fi
   echo "Unregistered $service_name"
}

#---------------------------------------------------------------------------
# Unregister SMF service
# return registered or not to stdout
SMFUnregister()
{   
   SMFHaltAndDeleteService $1  
   return 0
}


#---------------------------------------------------------------------------
# Main routine
# PARAMS: $command 
# return a stdout output
SMF()
{   
  
   cmd="$1"
   if [ "$#" -gt 2 -o "$cmd" = "-h" -o "$cmd" = "help"  -o "$cmd" = "--help" -o "$#" -lt 1 ]; then
      SMFusage
      exit 1
   fi

   if [ "$#" -eq 1 -a "$cmd" != "support" ]; then
      SMFusage
      exit 1 
   fi

   if [ "$#" -eq 2 -a "$cmd" != "register" -a "$cmd" != "unregister" ]; then
      SMFusage
      exit 1   
   fi

   #We return if we don't have SMF
   if [ "$hasSMF" = 0 ]; then
      echo "SMF not supported"
      exit 2
   fi
   opt="$2"

   if [ "$cmd" = "support" ]; then
        #check already done
        echo "SMF supported"
        return 0
   elif [ "$cmd" = "register" ]; then
      SMFRegister $opt
   elif [ "$cmd" = "unregister" ]; then
      SMFUnregister $opt
   else
      SMFusage
      exit 1
   fi
   return 0
}

SMF $@
