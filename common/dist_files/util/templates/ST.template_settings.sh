#!/bin/sh

#
#
# Sun Service Domain Manager service tag management script
#
#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2001 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__


PRODUCT_URN="urn:uuid:77c0b5f7-42fd-11dd-acd4-080020a9ed93"
PRODUCT_NAME="Service Domain Manager 1.0"
PRODUCT_VENDOR="Sun Microsystems"
PRODUCT_PARENT_URN="urn:uuid:739079eb-80c5-4ab0-acf7-0e8c31251190"
PRODUCT_PARENT_NAME="Grid Engine"
PRODUCT_VERSION="1.0u5"
PRODUCT_SOURCE="SDM_maintrunk"

