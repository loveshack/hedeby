<?xml version="1.0" encoding="UTF-8"?>
<!--
___INFO__MARK_BEGIN__
**************************************************************************

  The Contents of this file are made available subject to the terms of
  the Sun Industry Standards Source License Version 1.2

  Sun Microsystems Inc., March, 2001


  Sun Industry Standards Source License Version 1.2
  =================================================
  The contents of this file are subject to the Sun Industry Standards
  Source License Version 1.2 (the "License"); You may not use this file
  except in compliance with the License. You may obtain a copy of the
  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html

  Software provided under this License is provided on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
  See the License for the specific provisions governing your rights and
  obligations concerning the Software.

  The Initial Developer of the Original Code is: Sun Microsystems, Inc.

  Copyright: 2009 by Sun Microsystems, Inc.

  All Rights Reserved.

**************************************************************************
___INFO__MARK_END__
-->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
           xmlns:jxb="http://java.sun.com/xml/ns/jaxb"
           xmlns:common="http://hedeby.sunsource.net/hedeby-common"
           targetNamespace="http://hedeby.sunsource.net/hedeby-gef"
           xmlns="http://hedeby.sunsource.net/hedeby-gef"
           elementFormDefault="qualified"
           jxb:version="2.0">
    
    <xs:annotation>
        <xs:appinfo>
            <jxb:schemaBindings>
                <jxb:package name="com.sun.grid.grm.config.gef">
                    <jxb:javadoc>&lt;body&gt;
                        Directory for the Hedeby General Exectution Framework
                    &lt;/body&gt;</jxb:javadoc>
                </jxb:package>
            </jxb:schemaBindings>
        </xs:appinfo>
    </xs:annotation>
    
    <xs:import namespace="http://hedeby.sunsource.net/hedeby-common" schemaLocation="hedeby-common.xsd"/>               

    <xs:element name="action" type="ActionConfig"/>
    <xs:element name="geftest" type="DefaultGefConfig">
        <xs:annotation><xs:documentation>
            The geftest element is needed from 'sdmadm gef' command from 
            sdm-test.jar. The parameter -f specifies a xml file containing a
            geftest root element.
        </xs:documentation></xs:annotation>
    </xs:element>
    
    <xs:complexType name="ActionConfig">
        <xs:annotation>
            <xs:documentation>
                Configuration of an action in GEF
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="protocol" type="ProtocolConfig" minOccurs="0" maxOccurs="1"/>
            <xs:element name="param" type="Parameter" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="step" type="AbstractStepConfig" minOccurs="1" maxOccurs="unbounded">
                <xs:annotation><xs:documentation>
                    Each action must contain one or more steps, otherwise the resource will
                    stay in UNASSIGNING state when invoking the action.
                </xs:documentation></xs:annotation>
            </xs:element>

        </xs:sequence>
        <xs:attribute name="logLevel" type="LogLevel" use="optional" default="WARNING"/>
        <xs:attribute name="executeAs" type="xs:string" use="optional"/>
    </xs:complexType>

    <xs:complexType name="AbstractStepConfig" abstract="true">
        <xs:annotation>
            <xs:documentation>
                Configuration of a step of a GEF action
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="param" type="Parameter" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="filter" type="xs:string" minOccurs="0" maxOccurs="1"/>
            <xs:element name="delay" type="common:TimeInterval" minOccurs="0" maxOccurs="1"/>
        </xs:sequence>
        <xs:attribute name="name" type="xs:string" use="required"/>
        <xs:attribute name="isolationLevel" type="xs:string" use="optional"/>
        <xs:attribute name="logLevel" type="LogLevel" use="optional"/>
        <xs:attribute name="maxTries" type="xs:int" use="optional"/>
    </xs:complexType>

    <xs:complexType name="ScriptingStepConfig">
        <xs:complexContent>
            <xs:extension base="AbstractStepConfig">
                <xs:sequence>
                    <xs:element name="script" type="Script" minOccurs="1" maxOccurs="1"/>
                    <xs:element name="undo" type="Script" minOccurs="0" maxOccurs="1"/>
                </xs:sequence>
                <xs:attribute name="executeOn" type="ExecuteOn" use="optional" default="service_host"/>
                <xs:attribute name="executeAs" type="xs:string" use="optional"/>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:simpleType name="ExecuteOn">
        <xs:annotation><xs:documentation>
                Target where a script should be executed
        </xs:documentation></xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="service_host"/>
            <xs:enumeration value="resource_host"/>
        </xs:restriction>
    </xs:simpleType>


    <xs:complexType name="ProtocolConfig">
        <xs:sequence>
            <xs:element name="onSuccess" type="ProtocolOption" minOccurs="1" maxOccurs="1"/>
            <xs:element name="onError" type="ProtocolOption" minOccurs="1" maxOccurs="1"/>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="ProtocolOption">
        <xs:attribute name="scriptLogLevel" type="LogLevel" use="optional" default="DEBUG"/>
        <xs:attribute name="stdoutLogLevel" type="LogLevel" use="optional" default="DEBUG"/>
        <xs:attribute name="stderrLogLevel" type="LogLevel" use="optional" default="WARNING"/>
    </xs:complexType>

    <xs:complexType name="Script">
         <xs:sequence>
             <xs:element name="file" type="xs:string" minOccurs="1" maxOccurs="1"/>
             <xs:element name="timeout" type="common:TimeInterval" minOccurs="0" maxOccurs="1"/>
         </xs:sequence>
    </xs:complexType>

    <xs:complexType name="JavaStepConfig">
        <xs:complexContent>
            <xs:extension base="AbstractStepConfig">
                <xs:sequence>
                    <xs:element name="classpath" type="common:Path" minOccurs="0" maxOccurs="1"/>
                    <xs:element name="classname" type="xs:string" minOccurs="1" maxOccurs="1"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>


    <xs:complexType name="Parameter">
        <xs:attribute name="name" use="required"/>
        <xs:attribute name="value" use="required"/>
    </xs:complexType>

    <xs:complexType name="AbstractGefConfig" abstract="true">
        <xs:sequence>
            <xs:element name="protocol" type="ProtocolConfig" minOccurs="1" maxOccurs="1"/>
            <xs:element name="param" type="Parameter" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="executeAs" type="xs:string" use="optional" default="root"/>
    </xs:complexType>

    <xs:complexType name="DefaultGefConfig">
        <xs:complexContent>
            <xs:extension base="AbstractGefConfig">
                <xs:sequence>
                    <xs:element name="action" type="ActionConfig" minOccurs="1" maxOccurs="1"/>
                </xs:sequence>
            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

    <xs:simpleType name="LogLevel">
        <xs:restriction base="xs:string">
            <xs:enumeration value="DEBUG"/>
            <xs:enumeration value="INFO"/>
            <xs:enumeration value="WARNING"/>
            <xs:enumeration value="ERROR"/>
            <xs:enumeration value="OFF"/>
        </xs:restriction>
    </xs:simpleType>

    
</xs:schema>
