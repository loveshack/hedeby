/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.impl.RestartRequiredException;
import java.util.concurrent.ExecutorService;

/**
 * A component adapter provides the functionality of a component, however it
 * it does not implement the functionality of the compoent.
 * 
 * <p>Implementing subclasses can be used within an <code>AbstractComponentContainer</code>.
 * The container class forward all calls to the adapter.</p>
 * 
 * @param <E> The type of <code>ExecutorService</code> provided by the owning component
 * @param <C> The type of configuration of the owning component
 */
public interface ComponentAdapter<E extends ExecutorService, C> {

    /**
     * Prepare the component start.
     * @param config   the new configuration of the component
     * @throws com.sun.grid.grm.GrmException if the reload can not be executed
     */
    public void prepareComponentStartup(C config) throws GrmException;
    
    /**
     *  Start the component.
     * @param config  the configuration of the component
     * @throws GrmException  if the component could not be started
     */
    public void startComponent(C config) throws GrmException;
    
    
    /**
     * Prepare the shutdown of the component.
     * 
     * @param forced if <code>true</code> shutdown in forced mode
     */
    public void prepareComponentShutdown(boolean forced) throws GrmException;
    
    /**
     * Stop the component.
     * @param forced  stop the component forced
     * @throws com.sun.grid.grm.GrmException if the component could not be stopped
     */
    public void stopComponent(boolean forced) throws GrmException;
    
    /**
     * Prepare the component reload.
     * @param config   the new configuration of the component
     * @param forced   if <code>true</code> perform the reload forced
     * @throws com.sun.grid.grm.GrmException if the reload can not be executed
     * @throws com.sun.grid.grm.impl.RestartRequiredException if incompatible 
     *                 changes in the configuration requires a restart of the component
     */
    public void prepareComponentReload(C config, boolean forced) throws GrmException, RestartRequiredException;
    
    /**
     * Reinit the component with a new configuration.
     * @param config the new configuration
     * @param forced <code>true</code> if the reload should be made in forced mode
     * @throws com.sun.grid.grm.GrmException if the configuration could not be reloaded
     */
    public void reloadComponent(C config, boolean forced) throws GrmException;
    
    /**
     * Get the <code>ExecutorService</code> of the owning component.
     * @return the <code>ExecutorService</code> of the owning component.
     */
    public E getExecutorService();
    
    /**
     * Get the <code>ExecutionEnv</code> of the owning component.
     * @return the <code>ExecutionEnv</code> of the owning component
     */
    public ExecutionEnv getExecutionEnv();
    
    /**
     * Get the name of the owning component.
     * @return the name of the owning component
     */
    public String getName();
    
    /**
     * Get the configuration of the owning component.
     * 
     * @return the configuration of the owning component
     * @throws com.sun.grid.grm.GrmException  if the configuration could not read
     */
    public C getConfig() throws GrmException;
}
