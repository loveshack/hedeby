/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.reporting;

/**
 *
 * This enumeration describes types of notifications.
 */
public enum ReporterNotificationType {    
    
    //service was added to system
    SERVICE_ADDED,
    //service was removed
    SERVICE_REMOVED,
    //service is starting
    SERVICE_STARTING,
    //service is running
    SERVICE_RUNNING,
    //service is in the unknown state
    SERVICE_UNKNOWN,
    //service is going down
    SERVICE_GOING_DOWN,
    //service is in the error state
    SERVICE_ERROR,
    //service is shutdown
    SERVICE_SHUTDOWN,
    //service is asking for resource
    RESOURCE_REQUEST,
    //resource is about to be added to system
    RESOURCE_ADD,
    //resource was added to the system
    RESOURCE_ADDED,
    //resource is removing from the system
    RESOURCE_REMOVE,
    //resource was removed from the system
    RESOURCE_REMOVED,
    //resource was rejected by service
    RESOURCE_REJECTED,
    //resource is in error state
    RESOURCE_ERROR,
    //resource is reseting
    RESOURCE_RESET,
    //properties of the resource have been changed
    RESOURCE_PROPERTIES_CHANGED,
    //request queued in resource provider
    REQUEST_QUEUED,
    //request is pending in resource provider for processing
    REQUEST_PENDING,
    //request is processing in resource provider
    REQUEST_PROCESS,
    //request was processed by resource provider
    REQUEST_PROCESSED,
    //object was added to configuration service
    CS_OBJECT_ADDED,
    //object was removed from configuration service
    CS_OBJECT_REMOVED,
    //object was changed in configuration service
    CS_OBJECT_CHANGED
}
