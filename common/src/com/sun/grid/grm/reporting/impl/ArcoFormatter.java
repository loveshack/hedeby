/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.reporting.impl;

import com.sun.grid.grm.util.ThreadLocalStringBuffer;
import java.io.PrintWriter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 *
 * Formatter for logging in Arco format.
 */
public class ArcoFormatter extends Formatter {
   
    private static final String DELIMITER = ":";

    /** All character in the QUOTE_CHARS array will be quoted with a preceeding '\' */
    private  final static char[] QUOTE_CHARS = {'\\', ':', '\n', '\r'};

    /*
     * This method takes a LogRecord object and formats it to the Arco format style.
     * @param record record to format
     * @return the formatted String object in Arco format
     */
    @Override
    public String format(LogRecord record) {
        ThreadLocalStringBuffer sb = ThreadLocalStringBuffer.aquire();
        try {
            PrintWriter pw = sb.getPrintWriter();
            Object [] params = record.getParameters();
            if (params != null && params.length > 0) {
                if (params[0] != null) {
                    quote(params[0].toString(), pw);
                }
                for(int i = 1; i < params.length; i++) {
                    pw.print(DELIMITER);
                    // Omit null parameters, they will end up in empty columns
                    if (params[i] != null) {
                        quote(params[i].toString(), pw);
                    }
                }
            }
            pw.println();
            pw.flush();
            return sb.toString();
        } finally {
            sb.release();
        }
    }
    
    static void quote(String str, PrintWriter pw) {
        for(int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            for(char uc: QUOTE_CHARS) {
                if(uc == c) {
                    pw.print('\\');
                    break;
                }
            }
            pw.print(c);
        }
    }
}
