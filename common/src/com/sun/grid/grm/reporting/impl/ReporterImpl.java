/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.reporting.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.reporter.ReporterConfig;
import com.sun.grid.grm.impl.AbstractComponent;
import com.sun.grid.grm.impl.ComponentExecutors;
import com.sun.grid.grm.reporting.Reporter;
import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.reporting.ReportData;
import com.sun.grid.grm.reporting.filter.RowFactory;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.impl.SimpleServiceStore;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.Filter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class implements component reporter.
 */
public class ReporterImpl extends AbstractComponent<ExecutorService, ReporterConfig> implements Reporter {

    private static final String BUNDLE = "com.sun.grid.grm.reporting.impl.reporter";
    private static final int REPORT_PAGE_SIZE = 2000;
    private static final Logger log = Logger.getLogger(ReporterImpl.class.getName(), BUNDLE);
    /** lock for state changes */
    private final Lock stateLock = new ReentrantLock();
    private final ServiceStore<Service> services = new SimpleServiceStore<Service>();
    private ReporterServiceManager serviceManager = null;
    private String name;
    private AtomicLong clientSequenceNumber = new AtomicLong();
    private final Map<Long, Request> requestMap = new HashMap<Long, Request>();

    /**
     * Create reporter instance
     * @param env
     * @param name 
     * @throws com.sun.grid.grm.GrmException
     */
    public ReporterImpl(ExecutionEnv env, String name) throws GrmException {
        super(env, name,
                ComponentExecutors.<ReporterImpl, ReporterConfig>newSingleThreadedExecutorServiceFactory());
        this.name = name;
    }

    /**
     * Start the reporter component
     * @throws com.sun.grid.grm.GrmException
     */
    @Override
    protected void startComponent(ReporterConfig config) throws GrmException {
        log.entering(ReporterImpl.class.getName(), "startComponent");

        stateLock.lock();
        try {
            if (serviceManager == null) {
                serviceManager = new ReporterServiceManager(env, services, config, name);
            } else {
                serviceManager.setConfig(config);
            }
            serviceManager.start();
        } finally {
            stateLock.unlock();
        }
        if (log.isLoggable(Level.INFO)) {
            log.log(Level.INFO, "rep.start.started");
        }

        log.exiting(ReporterImpl.class.getName(), "startComponent");
    }

    /**
     * Stop the reporter component
     * @param forced determine if start was forced
     * @throws com.sun.grid.grm.GrmException
     */
    @Override
    protected void stopComponent(boolean forced) throws GrmException {
        log.entering(ReporterImpl.class.getName(), "stop", forced);

        stateLock.lock();
        try {
            serviceManager.stop();
        } finally {
            stateLock.unlock();
        }

        if (log.isLoggable(Level.INFO)) {
            log.log(Level.INFO, "rep.stopped");
        }

        log.exiting(ReporterImpl.class.getName(), "stop");
    }

    /**
     * Perform the reload of the reporter component
     * @param forced determine if reload was forced
     * @throws com.sun.grid.grm.GrmException
     */
    @Override
    protected void reloadComponent(ReporterConfig config, boolean forced) throws GrmException {
        log.entering(ReporterImpl.class.getName(), "reloadComponent", forced);
        stateLock.lock();
        try {
            stopComponent(forced);
            startComponent(config);
        } finally {
            stateLock.unlock();
        }
        log.exiting(ReporterImpl.class.getName(), "reloadComponent", forced);
    }

    public Report createReport(Filter<Report> filter) throws GrmException {
        if (filter == null) {
            throw new GrmException(I18NManager.formatMessage("rep.null.filter", BUNDLE));
        }

        Request req = null;

        long id = clientSequenceNumber.getAndIncrement();
        ReportingDataReader reader = serviceManager.createReader();
        ReportImpl ret =  new ReportImpl(id, RowFactory.DEFAULT_COLUMNS, REPORT_PAGE_SIZE);
        req = new Request(ret, reader, filter);
        synchronized (requestMap) {
            requestMap.put(id, req);
        }
        ret.setInitialData(req.read(REPORT_PAGE_SIZE));
        cleanupRequest();
        return ret;
    }



    /**
     * Gets the data from the reporter component
     * @param id of the client
     * @param max the max number of resources
     * @return reporter data
     * @throws com.sun.grid.grm.GrmException
     */
    public ReportData getData(long id, int max) throws GrmException {
        Request req = null;

        synchronized (requestMap) {
            req = requestMap.get(id);
        }

        if (req == null) {
            throw new GrmException("Unknown id");
        }
        cleanupRequest();
        return req.read(max);
    }

    private void cleanupRequest() {
        // Cleanup the timeout requests
        long timeoutValue = 60000;
        synchronized (requestMap) {
            ArrayList<Long> ids = new ArrayList<Long>(requestMap.keySet());
            for (long tmpId : ids) {
                Request tmpReq = requestMap.get(tmpId);
                if (tmpReq.getLastReadTime() + timeoutValue < System.currentTimeMillis()) {
                    tmpReq.close();
                    requestMap.remove(tmpId);
                }
            }
        }
    }

    static class Request {

        private final Filter<Report> filter;
        private final ReportingDataReader reader;
        private final AtomicLong lastReadTime = new AtomicLong();
        private final Report report;

        public Request(Report report, ReportingDataReader reader, Filter<Report> filter) {
            this.report = report;
            this.filter = filter;
            this.reader = reader;
        }

        public long getLastReadTime() {
            return lastReadTime.get();
        }

        public void close() {
            reader.close();
        }

        public ReportData read(int maxCount) throws GrmException {
            try {
                ReportingVariableResolver resolver = new ReportingVariableResolver(report);
                lastReadTime.set(System.currentTimeMillis());
                List<Object[]> rows = new ArrayList<Object[]>(maxCount);
                ArrayList<String> line = new ArrayList<String>(20);
                while (rows.size() < maxCount) {
                    line.clear();
                    if (reader.readLine(line)) {
                        return new ReportDataImpl(rows, false);
                    }
                    try {
                        Object[] row = RowFactory.newRow(report, line);
                        resolver.setRowObject(row);
                        if (filter.matches(resolver)) {
                            rows.add(row);
                        }
                    } catch(GrmException ex) {
                        log.log(Level.WARNING, I18NManager.formatMessage("rep.invalidLine", BUNDLE, ex.getLocalizedMessage(), line), ex);
                    }
                }
                return new ReportDataImpl(rows, true);
            } catch (IOException ex) {
                throw new GrmException("I/O error", ex);
            }
        }
    }
}
