/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.reporting.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.reporting.ReportData;
import com.sun.grid.grm.reporting.Reporter;
import com.sun.grid.grm.reporting.RowIterator;
import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * This is the reporter data object that is send to cli client.
 */
class ReportImpl implements Serializable, Report {
    
    /**
     * The serial version UID
     */
    private static final long serialVersionUID = -2009080801;
    
    private final long id;
    private final String [] columns;
    private ReportData initialData;
    private int pageSize;
    
    /**
     * This constructor creates an reporter data object
     * @param id id of the client
     * @param columns array of column names
     * @param pageSize size of a page of data
     */
    public ReportImpl(long id, String [] columns, int pageSize) {
        this.columns = columns;
        this.id = id;
        this.pageSize = pageSize;
    }

    public long getId() {
        return id;
    }

    public void setInitialData(ReportData data) {
        this.initialData = data;
    }

    public int getColumnCount() {
        return columns.length;
    }

    public String getColumnName(int index) {
        return columns[index];
    }

    public int getIndexForColumn(String name) {
        for(int i = 0; i < columns.length; i++) {
            if (columns[i].equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public RowIterator iterator() {
        if (initialData == null) {
            return new MyRowIterator();
        } else {
            return new MyRowIterator(initialData);
        }
    }

    private class MyRowIterator implements RowIterator {

        private boolean hasMore;
        private Iterator<Object[]> iter;

        public MyRowIterator() {
            iter = new Iterator<Object[]> () {

                public boolean hasNext() {
                    return false;
                }

                public Object[] next() {
                    throw new NoSuchElementException();
                }

                public void remove() {
                    throw new NoSuchElementException();
                }
            };
        }
        public MyRowIterator(ReportData data) {
            iter = data.getRows().iterator();
            hasMore = data.hasMore();
        }

        public boolean hasNext(Reporter reporter) throws GrmException {
            if (iter.hasNext()) {
                return true;
            }
            if (!hasMore) {
                return false;
            }
            ReportData data = reporter.getData(id, pageSize);
            hasMore = data.hasMore();
            iter = data.getRows().iterator();
            return iter.hasNext();
        }

        public Object[] next(Reporter reporter) throws GrmException {
            return iter.next();
        }
    }
}