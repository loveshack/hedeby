/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.reporting.impl;

import com.sun.grid.grm.util.I18NManager;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  This class is responsible for parsing the tokens out of a reporting file.
 *
 * <p><b>Note:</b> This class is not thread safe.</p>
 */
public class RowReader {

    private final static String BUNDLE = "com.sun.grid.grm.reporting.impl.reporter";

    private final static Logger log = Logger.getLogger(RowReader.class.getName(), BUNDLE);
    
    private final Reader rd;
    private boolean eofReached = false;
    private int putBackChar = -1;
    private final TokenWithContent tc = new TokenWithContent();
    
    private enum Token {
        STRING,
        COLON,
        EOL,
        EOF;
    }

    /**
     * Create a new RowReader
     * @param rd  the reader
     */
    public RowReader(Reader rd) {
        this.rd = rd;
    }

    /**
     * Close the RowReader
     * @throws java.io.IOException
     */
    public void close() throws IOException {
        eofReached = true;
        rd.close();
    }

    /**
     * Read a line from the reader and parse the tokens.
     * Store the tokens of the line in <tt>tokens</tt>
     *
     * <p>For performance reasons the list of tokens is not a return
     * value. The caller can allocate the list once and it can be reused.
     * 
     * @param tokens the list where the tokens are stored
     * @return <tt>true</tt> if EOF is reached.
     *         In this case no tokens have been stored in <tt>tokens</tt>
     * @throws java.io.IOException on any io error
     */
    boolean readLine(List<String> tokens) throws IOException {
        log.entering(RowReader.class.getName(), "readLine");
        boolean ret = readLineNoLog(tokens);
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "rowread.ret", tokens.toString());
        }
        log.exiting(RowReader.class.getName(), "readLine", ret);
        return ret;
    }


    private int nextChar() throws IOException {
        int c;
        if (putBackChar != -1) {
            c = putBackChar;
            putBackChar = -1;
        } else if (eofReached) {
            c = -1;
        } else {
            c = rd.read();
            if (c < 0) {
                eofReached = true;
            }
        }
        return c;
    }

    private static String quote(String str) {
        StringBuilder ret  = new StringBuilder(str.length() * 2);
        for(int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            switch(c) {
                case '\n': ret.append("\\n"); break;
                case '\r': ret.append("\\r"); break;
                case '\t': ret.append("\\t"); break;
                default: ret.append(c);
            }
        }
        return ret.toString();
    }

    private Token scan() throws IOException {
        Token ret = scanNoLog();
        if (log.isLoggable(Level.FINE)) {
            log.fine(I18NManager.formatMessage("rowread.scan", BUNDLE, ret, quote(tc.getContent())));
        }
        return ret;
    }
    
    private Token scanNoLog() throws IOException {
        int c = nextChar();
        switch (c) {
            case ':':
                return tc.set(Token.COLON, ":");
            case '\r': // Ignore
                return scan();
            case '\n':
                return tc.set(Token.EOL, "\n");
            case -1:
                return tc.set(Token.EOF, "EOF");
            default:
                tc.set(Token.STRING, "");
                while (true) {
                    switch (c) {
                        case '\n':
                            putBackChar = c;
                            return tc.token;
                        case -1:
                            return tc.token;
                        case '\r': // Ingore \r
                            break;
                        case '\\':
                            c = nextChar();
                            if (c == -1) {
                                // Backslash at the end of the file
                                // => add a single blackslash
                                tc.append('\\');
                            } else {
                                tc.append((char) c);
                            }
                            break;
                        case ':':
                            putBackChar = c;
                            return tc.token;
                        default:
                            tc.append((char) c);
                    }
                    c = nextChar();
                }
        }
    }

    private boolean scan(Token ... tokens) throws IOException {
        Token t = scan();
        for(Token token: tokens) {
           if (t == token) {
               return true;
           }
        }
        return false;
    }

    private boolean readLineNoLog(List<String> tokens) throws IOException {

        if (scan(Token.EOF)) {
            return true;
        }
        while(true) {
            switch(tc.token) {
                case EOF: return false;
                case EOL: return false;
                case COLON: // empty column
                    tokens.add("");
                    if (scan(Token.EOF, Token.EOL)) {
                        tokens.add("");
                        return false;
                    }
                    break;
                case STRING: // column with content
                    tokens.add(tc.getContent());
                    if (scan(Token.COLON)) {
                        if (scan(Token.EOF, Token.EOL)) {
                            tokens.add("");
                            return false;
                        }
                    }
                    break;
                default:  throw new IllegalStateException("Unpexected token" + tc.token);
            }
        }
    }

    private static class TokenWithContent {

        private Token token;
        private StringBuilder content = new StringBuilder(50);

        public Token set(Token t, String c) {
            token = t;
            content.setLength(0);
            content.append(c);
            return t;
        }

        public Token set(Token t, char c) {
            token = t;
            content.setLength(0);
            content.append(c);
            return t;
        }

        public void append(char c) {
            content.append(c);
        }

        /**
         * @return the content
         */
        public String getContent() {
            return content.toString();
        }
    }
}
