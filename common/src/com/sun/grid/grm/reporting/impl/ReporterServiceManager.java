/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.reporting.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectAddedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectChangedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectRemovedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventListener;
import com.sun.grid.grm.config.reporter.ReporterConfig;
import com.sun.grid.grm.reporting.ReporterNotificationType;
import com.sun.grid.grm.reporting.filter.RowFactory;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.resource.management.AbstractManagementEvent;
import com.sun.grid.grm.resource.management.ManagementEventListener;
import com.sun.grid.grm.resource.management.ProcessRequestEvent;
import com.sun.grid.grm.resource.management.RequestPendingEvent;
import com.sun.grid.grm.resource.management.RequestProcessedEvent;
import com.sun.grid.grm.resource.management.RequestQueuedEvent;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import com.sun.grid.grm.service.impl.AbstractServiceManager;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * This class implements service manager for reporter component that handle notification from services.
 */
public class ReporterServiceManager extends AbstractServiceManager<Service> {

    private static final String BUNDLE = "com.sun.grid.grm.reporting.impl.reporter";
    private static final Logger log = Logger.getLogger(ReporterServiceManager.class.getName(), BUNDLE);
    private final ServiceEventListener serviceEventListener;
    private final File spoolDir;
    private ResourceProvider rp = null;
    private boolean showRPEvents;
    private boolean showRPMEvents;
    private boolean showCSEvents;
    private final ConfigurationServiceEventListener configServiceEventListener;
    private final Lock csLock = new ReentrantLock();
    private final RotateFileHandler handler;

    /**
     * A constructor. 
     * 
     * @param env Execution environment
     * @param serviceStore Instance of ServiceStore, can not be null.
     * @param config reporter configuration
     * @param name name of the reporter
     * @throws com.sun.grid.grm.GrmException in any problem during
     * the initial communication with CS
     */
    public ReporterServiceManager(ExecutionEnv env, ServiceStore<Service> serviceStore, ReporterConfig config, String name) throws GrmException {
        super(env, serviceStore);

        configServiceEventListener = new REPConfigurationServiceEventListener();
        serviceEventListener = new REPServiceEventListener();
        spoolDir = PathUtil.getSpoolDirForComponent(env, name);
        if (!spoolDir.exists()) {
            spoolDir.mkdirs();
        }

        try {
            handler = new RotateFileHandler(spoolDir, config.getFilePattern(), config.getFileSize(), config.getFileCount(), config.isFileAppend());
            handler.setFormatter(new ArcoFormatter());
            handler.setLevel(Level.parse(config.getLevel()));
        } catch (IOException ex) {
            throw new GrmException("rep.file.exception", ex, BUNDLE, ex.getLocalizedMessage());
        }
        setConfig(config);
    }

    /**
     * Factory method that creates a service for the given name.
     * 
     * @param serviceName name of the service
     * @return service instance
     * @throws com.sun.grid.grm.service.InvalidServiceException when service 
     * manager can not create an object for the given name
     */
    protected Service createManagedService(String serviceName)
            throws InvalidServiceException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "createManagedService");
        }
        Service service = null;
        try {
            service = ComponentService.getComponentByNameAndType(env, serviceName, Service.class);
        } catch (Exception e) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "rep.cms.cs", e);
            }
        }
        if (service == null) {
            InvalidServiceException ex = new InvalidServiceException("rep.error.null_svc", BUNDLE);
            if (log.isLoggable(Level.FINER)) {
                log.throwing(ReporterServiceManager.class.getName(), "createManagedService", ex);
            }
            throw ex;
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ReporterServiceManager.class.getName(), "createManagedService", service);
        }
        return service;
    }

    /**
     * Updates logging configuration.
     * 
     * @param config reported configuration
     */
    public void setConfig(ReporterConfig config) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "setConfig", config);
        }
        this.showRPEvents = config.isShowResourceProviderEvents();
        this.showRPMEvents = config.isShowManagementEvents();
        this.showCSEvents = config.isShowCSEvents();
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ReporterServiceManager.class.getName(), "setConfig");
        }
    }

    /**
     * Starts the service manager.
     */
    @Override
    public void start() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "start");
        }

        csLock.lock();
        try {
            if (!isStarted()) {
                super.start();
                cs.addConfigurationServiceEventListener(configServiceEventListener);
                enableRP();
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "rep.astarted");
                }
            }
        } finally {
            csLock.unlock();
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(ReporterServiceManager.class.getName(), "start");
        }
    }

    /**
     * Stops the service manager.
     */
    @Override
    public void stop() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "stop");
        }

        csLock.lock();
        try {
            if (isStarted()) {
                super.stop();
                disableRP();
                cs.removeConfigurationServiceEventListener(configServiceEventListener);
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "rep.nstarted");
                }
            }
        } finally {
            csLock.unlock();
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(ReporterServiceManager.class.getName(), "stop");
        }
    }

    /**
     * Retrieves a proxy for the service and adds it to the list
     * of managed services.
     *
     * @param service the service to report about
     * @throws InvalidServiceException thrown when active component record
     * for a service can not be retrieved
     * @return true if service was added
     */
    @Override
    public boolean addService(Service service) throws InvalidServiceException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "addService", service);
        }
        boolean service_added = false;
        csLock.lock();
        try {
            service_added = super.addService(service);
            String name = null;
            try {
                name = service.getName();
            } catch (GrmRemoteException grexa) {
                if (log.isLoggable(Level.SEVERE)) {
                    log.log(Level.SEVERE, "rep.remoteexception", grexa);
                }
            }
            if ((name == null || name.length() == 0) && log.isLoggable(Level.SEVERE)) {
                log.log(Level.SEVERE, "rep.error.null_svc_name");
            }
            if (service_added) {
                addServiceReport(name);
                try {
                    service.addServiceEventListener(serviceEventListener);
                } catch (GrmRemoteException grexa) {
                    if (log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "rep.remoteexception", grexa);
                    }
                }
            }
        } finally {
            csLock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ReporterServiceManager.class.getName(), "addService", service_added);
        }
        return service_added;
    }

    /**
     * Removes a service from the reporter service manager.  This method has
     * no direct effect on the information stored in CS or on the removed service.
     *
     * @param name of the service to remove
     */
    @Override
    public boolean removeService(String name) throws UnknownServiceException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "removeService", name);
        }
        boolean service_removed = false;
        csLock.lock();
        try {
            Service service = getService(name);

            if (service == null) {
                UnknownServiceException e = new UnknownServiceException("rep.error.unknownservice", BUNDLE, name);
                if (log.isLoggable(Level.FINER)) {
                    log.throwing(ReporterServiceManager.class.getName(), "removeService", e);
                }
                throw e;
            }

            try {
                service.removeServiceEventListener(serviceEventListener);
            } catch (GrmRemoteException grexa) {
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "rep.remoteexception", grexa);
                }
            }
            service_removed = super.removeService(name);

            if (service_removed) {
                removeServiceReport(name);
            }
        } finally {
            csLock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ReporterServiceManager.class.getName(), "removeService", service_removed);
        }
        return service_removed;
    }

    /* helper method to log an add service action */
    private void addServiceReport(String name) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "addServiceReport");
        }
        logServiceAction(System.currentTimeMillis(), ReporterNotificationType.SERVICE_ADDED, name,
                I18NManager.formatMessage("rep.not.service.added", BUNDLE, name));
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ReporterServiceManager.class.getName(), "addServiceReport");
        }
    }

    /* helper method to log an remove service action */
    private void removeServiceReport(String name) {
        log.entering(ReporterServiceManager.class.getName(), "removeServiceReport");
        logServiceAction(System.currentTimeMillis(), ReporterNotificationType.SERVICE_REMOVED, name,
                I18NManager.formatMessage("rep.not.service.removed", BUNDLE, name));
        log.exiting(ReporterServiceManager.class.getName(), "removeServiceReport");
    }

    /* helper method for setting up a RP reporting */
    private void enableRP() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "enableRP");
        }
        csLock.lock();
        try {
            if (rp == null) {
                try {
                    rp = ComponentService.getComponentByType(env, ResourceProvider.class);
                } catch (GrmException grexa) {
                    if (log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "rep.erp.norp", grexa);
                    }
                }
                if (rp != null) {
                    if (showRPEvents) {
                        try {
                            rp.addResourceProviderEventListener(new REPResourceProviderEventListener());
                        } catch (GrmRemoteException grexa) {
                            if (log.isLoggable(Level.WARNING)) {
                                log.log(Level.WARNING, "rep.erp.norpel", grexa);
                            }
                        }
                    }
                    if (showRPMEvents) {
                        try {
                            rp.addManagementEventListener(new REPManagementEventListener());
                        } catch (GrmRemoteException grexa) {
                            if (log.isLoggable(Level.WARNING)) {
                                log.log(Level.WARNING, "rep.erp.nomel", grexa);
                            }
                        }
                    }
                }
            }
        } finally {
            csLock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ReporterServiceManager.class.getName(), "enableRP");
        }
    }

    /* helper method for setting up a RP reporting */
    private void disableRP() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "disableRP");
        }
        csLock.lock();
        try {
            if (rp != null) {
                if (showRPEvents) {
                    try {
                        rp.removeResourceProviderEventListener(new REPResourceProviderEventListener());
                    } catch (GrmRemoteException grexa) {
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, "rep.drp.norpel", grexa);
                        }
                    }
                }
                if (showRPMEvents) {
                    try {
                        rp.removeManagementEventListener(new REPManagementEventListener());
                    } catch (GrmRemoteException grexa) {
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, "rep.drp.nomel", grexa);
                        }
                    }
                }
            }
        } finally {
            csLock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ReporterServiceManager.class.getName(), "disableRP");
        }
    }

    /**
     * Create a reader for the data
     * @return the reader for the data
     * @throws GrmException if the reader could not be created
     */
    public ReportingDataReader createReader() throws GrmException {
        try {
            return handler.createReader();
        } catch (IOException ex) {
            throw new GrmException("Could not create reporting reader:" + ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Private class responsible for listening to the service events and 
     * triggering the appropriate action.
     * 
     */
    private class REPServiceEventListener implements ServiceEventListener {

        public void resourceRequest(ResourceRequestEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
            }
        }

        public void addResource(AddResourceEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_ADD, event));
            }
        }

        public void resourceAdded(ResourceAddedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_ADDED, event));
            }
        }

        public void removeResource(RemoveResourceEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_REMOVE, event));
            }
        }

        public void resourceRemoved(ResourceRemovedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_REMOVED, event));
            }
        }

        public void resourceRejected(ResourceRejectedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_REJECTED, event));
            }
        }

        public void resourceError(ResourceErrorEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_ERROR, event));
            }
        }

        public void resourceReset(ResourceResetEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_RESET, event));
            }
        }

        public void resourceChanged(ResourceChangedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_PROPERTIES_CHANGED, event));
            }
        }

        public void serviceStarting(ServiceStateChangedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
            }
        }

        public void serviceRunning(ServiceStateChangedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
            }
        }

        public void serviceUnknown(ServiceStateChangedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
            }
        }

        public void serviceShutdown(ServiceStateChangedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
            }
        }

        public void serviceError(ServiceStateChangedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
            }
        }

        public void serviceStopped(ServiceStateChangedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
            }
        }
    }

    private class REPResourceProviderEventListener implements ResourceProviderEventListener {

        public void addResource(AddResourceEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_ADD, event));
            }
        }

        public void resourceAdded(ResourceAddedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_ADDED, event));
            }
        }

        public void removeResource(RemoveResourceEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_REMOVE, event));
            }
        }

        public void resourceRemoved(ResourceRemovedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_REMOVED, event));
            }
        }

        public void resourceError(ResourceErrorEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_ERROR, event));
            }
        }

        public void resourceReset(ResourceResetEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_RESET, event));
            }
        }

        public void resourceChanged(ResourceChangedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_PROPERTIES_CHANGED, event));
            }
        }

        public void resourceRejected(ResourceRejectedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(ReporterNotificationType.RESOURCE_REJECTED, event));
            }
        }
    }

    private class REPManagementEventListener implements ManagementEventListener {

        public void requestQueued(RequestQueuedEvent event) {
            if (isStarted()) {
                logManagementEvent(event, ReporterNotificationType.REQUEST_QUEUED,
                        event.getNeeds(), event.getServiceName());
            }
        }

        public void requestPending(RequestPendingEvent event) {
            if (isStarted()) {
                logManagementEvent(event, ReporterNotificationType.REQUEST_PENDING,
                        event.getNeeds(), event.getServiceName());
            }
        }

        public void processRequest(ProcessRequestEvent event) {
            if (isStarted()) {
                logManagementEvent(event, ReporterNotificationType.REQUEST_PROCESS,
                        event.getNeeds(), event.getServiceName());
            }
        }

        public void requestProcessed(RequestProcessedEvent event) {
            if (isStarted()) {
                logManagementEvent(event, ReporterNotificationType.REQUEST_PROCESSED,
                        event.getRemainingNeeds(), event.getServiceName());
            }
        }
    }

    private class REPConfigurationServiceEventListener implements ConfigurationServiceEventListener {

        public void configurationObjectAdded(ConfigurationObjectAddedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
                enableRP();
            }
        }

        public void configurationObjectRemoved(ConfigurationObjectRemovedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
                enableRP();
            }
        }

        public void configurationObjectChanged(ConfigurationObjectChangedEvent event) {
            if (isStarted()) {
                log(RowFactory.createLogRecord(event));
                enableRP();
            }
        }
    }

    private void log(LogRecord lr) {
        handler.publish(lr);
    }

    private void logManagementEvent(AbstractManagementEvent event, ReporterNotificationType notType, List<Need> needs, String service) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "logManagementEvent", new Object[]{event, notType});
        }
        log(RowFactory.createLogRecord(notType, event, service, RowFactory.formatNeeds(needs)));
        log.exiting(ReporterServiceManager.class.getName(), "logManagementEvent");
    }

    private void logServiceAction(long timestamp, ReporterNotificationType notType, String service, String message) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ReporterServiceManager.class.getName(), "logServiceAction", new Object[]{notType, service, message});
        }
        log(RowFactory.createLogRecord(timestamp, notType, service, message));
        log.exiting(ReporterServiceManager.class.getName(), "logServiceAction");
    }

}
