/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.reporting.impl;

import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.FilterVariable;
import com.sun.grid.grm.util.filter.OrFilter;
import com.sun.grid.grm.util.filter.VariableResolver;
import java.util.Date;

/**
 * Get the data from the reporting data rows
 */
public class ReportingVariableResolver implements VariableResolver<Report> {
    
    /**
     * variable name for the time
     */
    public final static FilterVariable<Report> TIME = new FilterVariable<Report>("time");
    /**
     * variable name fo the event type
     */
    public final static FilterVariable<Report> TYPE = new FilterVariable<Report>("type");

    /**
     * variable for the resource id
     */
    public final static FilterVariable<Report> RESOURCE_ID = new FilterVariable<Report>("resource_id");

    /**
     * variable for the resource name
     */
    public final static FilterVariable<Report> RESOURCE_NAME = new FilterVariable<Report>("resource_name");

    /**
     * variable for the service name
     */
    public final static FilterVariable<Report> SERVICE = new FilterVariable<Report>("service");

    /**
     * variable for the description
     */
    public final static FilterVariable<Report> DESCRIPTION = new FilterVariable<Report>("description");

    private Object[] obj;

    private final Report report;

    public ReportingVariableResolver(Report report) {
        this.report = report;
    }
    
    /**
     * Sets the current row
     * @param obj
     */
    public void setRowObject(Object[] obj) {
        this.obj = obj;
    }
    
    /**
     * Gets value for the given column
     * @param name column name
     * @return value for this column
     */
    public Object getValue(String name) {
        int index = report.getIndexForColumn(name);
        if (index >= 0 && index < obj.length) {
            return obj[index];
        }
        return null;
    }
    
    /**
     * Create a filter for the start time.
     * @param startTime 
     * @return filter
     */
    public static Filter<Report> createStartimeFilter(Date startTime) {
        return new CompareFilter<Report>(TIME, new FilterConstant<Report>(startTime), CompareFilter.Operator.GE);
    }
    
    /**
     * Create a filter for the end time.
     * @param stopTime 
     * @return filter
     */
    public static Filter<Report> createEndtimeFilter(Date stopTime) {
        return new CompareFilter<Report>(TIME, new FilterConstant<Report>(stopTime), CompareFilter.Operator.LE);
    }
    
    /**
     * Create a filter that matches if the resource name or the resource
     * id matches against <tt>resource</tt>.
     *
     * @param resource   the resource
     * @return filter    the filter
     */
    public static Filter<Report> createResourceFilter(String resource) {
        OrFilter<Report> ret = new OrFilter<Report>(2);
        FilterConstant<Report> op1 = new FilterConstant<Report>(resource);
        ret.add(new CompareFilter<Report>(RESOURCE_NAME, op1, CompareFilter.Operator.EQ));
        ret.add(new CompareFilter<Report>(RESOURCE_ID, op1, CompareFilter.Operator.EQ));
        return ret;
    }
    
    /**
     * Create compare filter.
     * @param service 
     * @return filter
     */
    public static Filter<Report> createServiceFilter(String service) {
        return new CompareFilter<Report>(SERVICE, new FilterConstant<Report>(service), CompareFilter.Operator.EQ);
    }
    
    /**
     * Create compare filter.
     * @param type 
     * @return filter
     */
    public static Filter<Report> createEventTypeFilter(String type) {
        return new CompareFilter<Report>(TYPE, new FilterConstant<Report>(type), CompareFilter.Operator.EQ);
    }    
}