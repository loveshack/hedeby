/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.reporting.impl;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.FileHandler;

/**
 * This class sets the correct file to read by reporter.
 */
class RotateFileHandler extends FileHandler {

    private final File dir;
    private final String pattern;
    private final Set<ReportingDataReaderImpl> readers;
    private final int maxFileCount;

    /**
     * Create file handler for reporter
     * @param dir reporter spool directory
     * @param pattern of the reporter file name
     * @param limit of the file size
     * @param count of the reporter files
     * @param append determine if data should appear in the file
     * @throws java.io.IOException thrown when there was porblems with the file reading
     * @throws java.lang.SecurityException thrown when security manager exists and if
     *             the caller does not have <tt>LoggingPermission("control")
     */
    RotateFileHandler(File dir, String pattern, int limit, int count, boolean append)
            throws IOException, SecurityException {
        super(new File(dir, pattern).toString(), limit, count, append);
        this.dir = dir;
        this.pattern = pattern;
        this.maxFileCount = count;
        readers = new HashSet<ReportingDataReaderImpl>();
    }

    @Override
    protected synchronized void setOutputStream(OutputStream out) throws SecurityException {
        if (readers != null) {
            for (ReportingDataReaderImpl reader : readers) {
                reader.rotate();
            }
        }
        super.setOutputStream(out);
    }

    /**
     * Create a reader for the data.
     * 
     * The caller must close the reader otherwise we will have a memory leak
     * @return the reader
     * @throws java.io.IOException if the reader could not be created
     */
    synchronized ReportingDataReader createReader() throws IOException {
        ReportingDataReaderImpl ret = new ReportingDataReaderImpl();
        readers.add(ret);
        return ret;
    }

    private class ReportingDataReaderImpl implements ReportingDataReader {

        private RowReader rd;
        private int currentFileIndex;

        public ReportingDataReaderImpl() throws IOException {
            synchronized (RotateFileHandler.this) {
                currentFileIndex = -1;
                for (int i = maxFileCount - 1; i >= 0; i--) {
                    File f = new File(dir, pattern.replace("%g", String.valueOf(i)));
                    if (f.exists()) {
                        currentFileIndex = i;
                        break;
                    }
                }
                if (currentFileIndex >= 0) {
                    rd = new RowReader(new FileReader(new File(dir, pattern.replace("%g", String.valueOf(currentFileIndex)))));
                }
            }
        }

        void rotate() {
            synchronized (RotateFileHandler.this) {
                // The rotate increments the index of the files
                // => adjust the current file index
                currentFileIndex++;
                if (currentFileIndex >= maxFileCount) {
                    currentFileIndex = -1;
                }
            }
        }

        /**
         * Read the line from a reporting file
         * @return line
         * @throws java.io.IOException when there are problems while reading
         */
        public boolean readLine(List<String> tokens) throws IOException {
            if (rd == null) {
                return true;
            }
            boolean eof = rd.readLine(tokens);
            if (eof) {
                try {
                    rd.close();
                } catch (IOException ex) {
                    // Ignore
                }
                rd = null;
                synchronized (RotateFileHandler.this) {
                    if (currentFileIndex > 0) {
                        currentFileIndex--;
                        rd = new RowReader(new FileReader(new File(dir, pattern.replace("%g", String.valueOf(currentFileIndex)))));
                    }
                }
                if (rd != null) {
                    eof = rd.readLine(tokens);
                }
            }
            return eof;
        }

        /**
         * Close the stream.
         */
        public void close() {
            synchronized (RotateFileHandler.this) {
                readers.remove(this);
                currentFileIndex = -1;
            }
            if (rd != null) {
                try {
                    rd.close();
                } catch (IOException ex) {
                    // ignore
                }
            }
        }
    }
}
