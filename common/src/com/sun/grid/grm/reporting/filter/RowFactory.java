/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.reporting.filter;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.config.naming.event.AbstractConfigurationServiceEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectAddedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectChangedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectRemovedEvent;
import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.reporting.ReporterNotificationType;
import com.sun.grid.grm.reporting.impl.ReportingVariableResolver;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.management.AbstractManagementEvent;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.event.AbstractServiceChangedResourceEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import com.sun.grid.grm.util.I18NManager;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Factory class for rows of the reporter
 */
public abstract class RowFactory {

    final static String BUNDLE = "com.sun.grid.grm.reporting.filter.messages";

    /**
     * An array of names of the default columns of a row:
     * <ul>
     *   <li>time</li>
     *   <li>type</li>
     *   <li>service</li>
     *   <li>resource name</li>
     *   <li>resource id</li>
     *   <li>description</li>
     * </ul>
     */
    public final static String [] DEFAULT_COLUMNS = {
        ReportingVariableResolver.TIME.getName(),
        ReportingVariableResolver.TYPE.getName(),
        ReportingVariableResolver.SERVICE.getName(),
        ReportingVariableResolver.RESOURCE_NAME.getName(),
        ReportingVariableResolver.RESOURCE_ID.getName(),
        ReportingVariableResolver.DESCRIPTION.getName()
    };


    /**
     * Create a new row.
     * @param report the report to which the row belongs
     * @param tokens the raw tokens that are converted into a row
     * @return the new row
     * @throws com.sun.grid.grm.GrmException if the column count of the report
     *         does not match the number of tokens or the time column cannot be
     *         parsed.
     */
    public static Object[] newRow(Report report, List<String> tokens) throws GrmException {
        int colCount = report.getColumnCount();
        if (tokens.size() != colCount) {
            throw new GrmException("as.ex.tokenCount", BUNDLE, colCount, tokens.size());
        }
        Object[] ret = new Object[colCount];

        for(int i = 0; i < colCount; i++) {
            if (report.getColumnName(i).equals(ReportingVariableResolver.TIME.getName())) {
                ret[i] = parseDate(tokens.get(i));
            } else {
                ret[i] = getNullIfEmpty(tokens.get(i));
            }
        }
        return ret;
    }

    private static String getNullIfEmpty(String token) {
        if (token == null || token.length() == 0) {
            return null;
        } else {
            return token;
        }
    }


    private static Date parseDate(String token) throws GrmException {
        try {
            long ts = Long.parseLong(token);
            return new Date(ts);
        } catch(NumberFormatException ex) {
            throw new GrmException("ar.ex.ts", ex, BUNDLE, token);
        }
    }

    /**
     * Create a log record with log level SEVERE.
     * @param timeStamp time stamp for the log record
     * @param notificationType the type of notification
     * @param service the affected service
     * @param resource the affected resource (or null)
     * @param notDesc the description
     * @return the created <tt>LogRecord</tt>
     */
    public static LogRecord createLogRecord(long timeStamp,
            ReporterNotificationType notificationType, String service, Resource resource, String notDesc) {

         LogRecord ret = new LogRecord(Level.SEVERE, "");

         Object[] params = new Object [DEFAULT_COLUMNS.length];
         for(int i = 0; i < DEFAULT_COLUMNS.length; i++) {
            if (ReportingVariableResolver.TIME.getName().equals(DEFAULT_COLUMNS[i])) {
                params[i] = timeStamp;
            } else if (ReportingVariableResolver.TYPE.getName().equals(DEFAULT_COLUMNS[i])) {
                params[i] = notificationType;
            } else if (ReportingVariableResolver.SERVICE.getName().equals(DEFAULT_COLUMNS[i])) {
                params[i] = service;
            } else if (ReportingVariableResolver.RESOURCE_ID.getName().equals(DEFAULT_COLUMNS[i])) {
                params[i] = resource == null ? null : resource.getId().getId();
            } else if (ReportingVariableResolver.RESOURCE_NAME.getName().equals(DEFAULT_COLUMNS[i])) {
                params[i] = resource == null ? null : resource.getName();
            } else if (ReportingVariableResolver.DESCRIPTION.getName().equals(DEFAULT_COLUMNS[i])) {
                params[i] = notDesc;
            }
         }
         ret.setParameters(params);
         return ret;
    }
    
    /**
     * Create a log record with log level SEVERE.
     * @param timeStamp time stamp for the log record
     * @param notificationType the type of notification
     * @param service the affected service
     * @param notDesc the description
     * @return the created <tt>LogRecord</tt>
     */
    public static LogRecord createLogRecord(long timeStamp,
            ReporterNotificationType notificationType, String service, String notDesc) {
        return createLogRecord(timeStamp, notificationType, service, null, notDesc);
    }

    /**
     * Create a log record from a <tt>ResourceRequestEvent</tt>. The description
     * contains the SLO name and the needs of the resource request.
     * @param event the resource request event
     * @return the created <tt>LogRecord</tt>
     */
    public static LogRecord createLogRecord(ResourceRequestEvent event) {
        return createLogRecord(event.getTimestamp(),
                               ReporterNotificationType.RESOURCE_REQUEST,
                               event.getServiceName(),
                               String.format("SLO: %s, %s", event.getSLOName(), formatNeeds(event.getNeeds())));
    }

    /**
     * get the string represenation of a need list
     * @param needs  the need list
     * @return the stirng representation
     */
    public static String formatNeeds(List<Need> needs) {
        switch (needs.size()) {
            case 0:
                return I18NManager.formatMessage("nr.noNeed", BUNDLE);
            case 1:
                return formatNeed(needs.get(0));
            default:
                StringBuilder sb = new StringBuilder();
                Iterator<Need> iter = needs.iterator();
                if (iter.hasNext()) {
                    sb.append("[");
                    sb.append(formatNeed(iter.next()));
                    sb.append("]");
                    while (iter.hasNext()) {
                        sb.append(",[");
                        sb.append(formatNeed(iter.next()));
                        sb.append("]");
                    }
                }
                return sb.toString();
        }
    }

    private static String formatNeed(Need need) {
        return I18NManager.formatMessage("nr.need", BUNDLE, need.getQuantity(), need.getUrgency().getLevel(),
                need.getResourceFilter() == null ? "" : need.getResourceFilter().toString());
    }


    /**
     * Create a log record from a <tt>AbstractConfigurationServiceEvent</tt>.
     * @param event the event
     * @return the created <tt>LogRecord</tt>
     */
    public static LogRecord createLogRecord(AbstractConfigurationServiceEvent event) {

        final String msg;
        final String path = event.getContextPath();
        final String name = path + "." + event.getName();
        if (BootstrapConstants.CS_ACTIVE_COMPONENT.equals(path)
           || BootstrapConstants.CS_ACTIVE_JVM.equals(path)  ) {
            msg = I18NManager.formatMessage("notRow.configWithHost", BUNDLE, name, event.getHost());
        } else {
            msg = I18NManager.formatMessage("notRow.config", BUNDLE, name);
        }
        return createLogRecord(event.getTimestamp(),
                               getType(event),
                               event.getCSName(),
                               msg);
    }

    private static ReporterNotificationType getType(AbstractConfigurationServiceEvent event) {
        if (event instanceof ConfigurationObjectAddedEvent) {
            return ReporterNotificationType.CS_OBJECT_ADDED;
        }
        if (event instanceof ConfigurationObjectChangedEvent) {
            return ReporterNotificationType.CS_OBJECT_CHANGED;
        }
        if (event instanceof ConfigurationObjectRemovedEvent) {
            return ReporterNotificationType.CS_OBJECT_REMOVED;
        }
        throw new IllegalStateException("Unsupported AbstractConfigurationServiceEvent " + event.getClass());
    }


    /**
     * Create a log record from an <tt>AbstractManagementEvent</tt>.
     * @param notType the notification type
     * @param event the <tt>AbstractManagementEvent</tt>
     * @param service the affected service
     * @param message the description
     * @return the created <tt>LogRecord</tt>
     */
    public static LogRecord createLogRecord(ReporterNotificationType notType, AbstractManagementEvent event, String service, String message) {
        return createLogRecord(event.getTimestamp(),
                              notType,
                              service,
                              message);
    }

    /**
     * Create a log record from an <tt>AbstractServiceChangedResourceEvent</tt>.
     * If the event is a <tt>ResourceChangedEvent</tt>, the changed properties
     * are contained in the log record. Otherwise the event message is included.
     * @param notType the notification type
     * @param event the <tt>AbstractServiceChangedResourceEvent</tt>
     * @return the created <tt>LogRecord</tt>
     */
    public static LogRecord createLogRecord(ReporterNotificationType notType, AbstractServiceChangedResourceEvent event) {

        String notDesc;
        if (event instanceof ResourceChangedEvent) {
            ResourceChangedEvent rce = (ResourceChangedEvent) event;
            notDesc = rce.getProperties().toString();
        } else {
            notDesc = event.getMessage();
        }
        return createLogRecord(event.getTimestamp(),
                               notType,
                               event.getServiceName(),
                               event.getResource(),
                               notDesc);
    }
    /**
     * Create a log record from a <tt>ServiceStateChangedEvent</tt>
     * @param event the event
     * @return the created <tt>LogRecord</tt>
     */
    public static LogRecord createLogRecord(ServiceStateChangedEvent event) {
        return createLogRecord(event.getTimestamp(),
                               getType(event),
                               event.getServiceName(),
                               null,
                               null);
    }

    private static ReporterNotificationType getType(ServiceStateChangedEvent event) {
        switch(event.getNewState()) {
            case ERROR: return ReporterNotificationType.SERVICE_ERROR;
            case RUNNING: return ReporterNotificationType.SERVICE_RUNNING;
            case SHUTDOWN: return ReporterNotificationType.SERVICE_GOING_DOWN;
            case STARTING: return ReporterNotificationType.SERVICE_STARTING;
            case STOPPED: return ReporterNotificationType.SERVICE_SHUTDOWN;
            case UNKNOWN: return ReporterNotificationType.SERVICE_UNKNOWN;
            default: throw new IllegalStateException("Unknown state " + event.getNewState());
        }
    }


}
