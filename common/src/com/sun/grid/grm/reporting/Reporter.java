/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.reporting;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmSingleton;
import com.sun.grid.grm.management.Manageable;
import com.sun.grid.grm.util.filter.Filter;

/**
 * The reporting component that creates history for Hedeby
 * and is used to obtain history data.
 * 
 */
@Manageable
public interface Reporter extends GrmComponent, GrmSingleton{

    /**
     * Create a new report
     * @param filter the filter
     * @return created report
     * @throws GrmException if the report could not be created
     */
    Report createReport(Filter<Report> filter) throws GrmException;
    
    /**
     * Gets the data from the reporter
     * @param id of the client
     * @param max maximum number of rows
     * @return the data from the reporter for the given client id.
     * @throws com.sun.grid.grm.GrmException
     */
    ReportData getData(long id, int max) throws GrmException;
}
