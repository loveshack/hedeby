/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import java.util.concurrent.ExecutorService;

/**
 * Factory for a <code>ComponentAdapter</code>.
 * 
 * <p>Instances of <code>ComponentAdapterFactory</code> are used from instances of
 *    <code>AbstractComponentContainer</code> to create the <code>ComponentAdapter</code>
 *    for the component container.</p>
 * 
 * @param <T>   The type of the <code>ComponentContainer</code>
 * @param <A>   The type of the created <code>ComponentAdapter</code>
 * @param <E>   The type of the <code>ExecutorService</code> provided by the component
 * @param <C>   Type type of the configuration of the component
 */
public interface ComponentAdapterFactory<T extends ComponentContainer<A,E,C>,A extends ComponentAdapter<E,C>,E extends ExecutorService,C> {
    
    /**
     * Create the adapter for a component
     * @param component the owning component container
     * @param config the configuration of the component
     * @return the <code>ComponentAdapter</code>
     * @throws GrmException if the <code>ComponentAdapter</code> could not be created
     */
    public A createAdapter(T component, C config) throws GrmException;

    /**
     * Has the implementation of the component adapter changed due to 
     * configuration changes.
     * 
     * @param component  the owning component container
     * @param adapter    the component adapter
     * @param config     the current configuration of the component
     * @return <code>true</code> of the implemenation has changed
     * @throws GrmException if the configuration is invalid
     */
    public boolean hasImplementationChanged(T component, A adapter, C config) throws GrmException;
}
