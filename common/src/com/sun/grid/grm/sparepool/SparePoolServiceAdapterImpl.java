/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.sparepool;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.ResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.adapter.ResourceAdapterStore;
import com.sun.grid.grm.resource.adapter.impl.DefaultResourceAdapter;
import com.sun.grid.grm.resource.adapter.impl.DefaultResourceAdapterFileStore;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManagerSetup;
import com.sun.grid.grm.service.slo.SLO;
import com.sun.grid.grm.service.slo.SLOFactory;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.impl.AbstractServiceAdapter;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

/**
 * This is the implementation of a Spare Pool ServiceAdapter.
 * The SparePoolServiceAdapterImpl class represents an internal pool of resources which are not
 * currently being used. A SparePool presents a constant need for resources to
 * the resource provider at a specified urgency level and period.
 *
 */
public class SparePoolServiceAdapterImpl extends AbstractServiceAdapter<ExecutorService,SparePoolServiceConfig,ResourceId,DefaultResourceAdapter> {

    /** lock for accessing resources */
    private static final String BUNDLE = "com.sun.grid.grm.sparepool.messages";
    /** lock for accessing resources */
    private static final Logger log = Logger.getLogger(SparePoolServiceAdapterImpl.class.getName(),
            BUNDLE);

    
    private final static long DEFAULT_SLO_UPATE_INTERVAL = 60000;
    
    /**
     * Creates an instance of SparePool.
     * @param component the owning component
     * @throws com.sun.grid.grm.GrmException in case any error occurs
     */
    public SparePoolServiceAdapterImpl(SparePoolServiceImpl component)
            throws GrmException {
        super(component);
    }

    @Override
    protected ResourceFactory<DefaultResourceAdapter> createResourceFactory() {
        return new DefaultResourceFactory<DefaultResourceAdapter>();
    }

    
    /**
     * Create the SLOManager of the spare pool.
     * @return an RunnableSLOManager which makes every minute and SLO run
     */
    @Override
    protected RunnableSLOManager createSLOManager() {
        return  new RunnableSLOManager(getName(), this, DEFAULT_SLO_UPATE_INTERVAL);
    }

    /**
     * All resources of the SparePool should be freed very fast. 
     * To be sure we allow an timeout of one minute
     * @return one minute in milliseconds
     */
    @Override
    protected long getFreeResourcesTimeout() {
        return 60000;
    }

    /**
     * Creates the setup for the SLOManager
     * @param config the configuration of the SparePool
     * @return the setup for the SLOManager
     * @throws com.sun.grid.grm.GrmException if the configuration is invalid
     */
    @Override
    protected RunnableSLOManagerSetup createSLOManagerSetup(SparePoolServiceConfig config) throws GrmException {
        List<SLO> slos = createSLOs(config);
        return new RunnableSLOManagerSetup(DEFAULT_SLO_UPATE_INTERVAL, slos);
    }

    /**
     * The SparePool uses an DefaultResourceAdapterFileStore. It spools
     * into the spooling directory if the component
     * @return a DefaultResourceAdapterFileStore
     */
    @Override
    protected ResourceAdapterStore<ResourceId,DefaultResourceAdapter> createResourceAdapterStore(SparePoolServiceConfig config) {
        File spoolDir = PathUtil.getSpoolDirForComponent(getExecutionEnv(), getName());
        return new DefaultResourceAdapterFileStore(spoolDir);
    }
    

    /**
     * No action required for starting the SparePool service.
     * @param config not used
     * @return null => AbstractServiceAdapter will not perform a merge with the Resources
     *                 from the ResourceAdapterStore
     */
    @Override
    protected Map<ResourceId,Object> doStartService(SparePoolServiceConfig config) {
        // No special action necessary
        return null;
    }

    /**
     * No action required for starting the SparePool service.
     * @param config not used
     * @param forced not used
     * @return null => AbstractServiceAdapter will not perform a merge with the Resources
     *                 from the ResourceAdapterStore
     */
    @Override
    protected Map<ResourceId,Object> doReloadService(SparePoolServiceConfig config, boolean forced) {
        // No special action necessary
        return null;
    }

    /*
     * If the SparePool is stopped the resulting service state is STOPPED.
     * 
     *  @param isFreeResources is not used
     *  @return Always STOPPED
     */
    @Override
    public ServiceState doStopService(boolean freeResources) throws GrmException {
        log.entering(SparePoolServiceAdapterImpl.class.getName(), "stopService", freeResources);
        ServiceState ret = ServiceState.STOPPED;
        log.exiting(SparePoolServiceAdapterImpl.class.getName(), "stopService", ret);
        return ret;
    }

    private List<SLO> createSLOs(SparePoolServiceConfig aConfig)
            throws GrmException {
        log.entering(SparePoolServiceAdapterImpl.class.getName(), "createSLOs");
        
        if (!aConfig.isSetSlos()) {
            return Collections.<SLO>emptyList();
        }

        List<SLOConfig> sloConfigs = aConfig.getSlos().getSlo();
        List<SLO> ret = new ArrayList<SLO>(sloConfigs.size());
        SLOFactory fac = SLOFactory.getDefault();

        for (SLOConfig sloc : sloConfigs) {
            SLO slo = null;

            try {
                slo = fac.createSLO(sloc);
            } catch (GrmException ex) {
                throw new GrmException("sparepool.ex.slo", ex, BUNDLE,
                        sloc.getName(), ex.getLocalizedMessage());
            }

            if (slo == null) {
                throw new GrmException("sparepool.ex.uslo", BUNDLE, sloc.getName(),
                        sloc.getClass().getSimpleName());
            } else {
                ret.add(slo);
            }
        }

        log.exiting(SparePoolServiceAdapterImpl.class.getName(), "createSLOs", ret);
        return ret;
    }
}
