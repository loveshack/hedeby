/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * RemoteEvent.java
 *
 * Created on April 12, 2006, 11:49 PM
 *
 */

package com.sun.grid.grm.rmi;

import java.io.Serializable;
import java.util.EventObject;

/**
 * The RemoteEvent class is a wrapper around Notification that allows for the
 * serialization of the source property.  The source property as stored by the
 * EventObject (parent of Notification) is transient.  The RemoteEvent class
 * has the capacity to store Serializable source objects so that they can be
 * sent along with the event.
 *
 */
public class RemoteEvent extends EventObject {
    /**
     * This object is used to indicate that no source should be set for this
     * event.  This most likely will mean that the EventSource.fireEvent()
     * method will replace this reference with an appropriate reference to the
     * JMX wrapper, which in turn means that the JMX framework will replace
     * the wrapper reference with the wrapper's JMX object name.
     */
    public static final Object NO_SOURCE = new Object();
    private final Serializable source;
    private static final long serialVersionUID = -2007092301L;

    /**
     * Creates a new instance of RemoteEvent
     * @param source the id of the event source
     * @see com.sun.grid.grm.bootstrap.ComponentInfo
     */
    public RemoteEvent(Object source) {
        super(source);

      /* Because the source object of the EventObject class will not be
       * serialized, we store a second copy that will. */
        if (source instanceof Serializable) {
            this.source = (Serializable)source;
        } else {
            this.source = null;
        }
    }

    /* Docs inherited from EventObject. */
    @Override
    public Object getSource() {
        Object retValue;

        if (this.source != null) {
            retValue = this.source;
        } else {
            retValue = super.getSource();
        }

        return retValue;
    }
}
