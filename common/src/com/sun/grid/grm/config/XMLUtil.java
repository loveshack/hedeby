/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.config;

import com.sun.grid.grm.util.DependencyTree;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ConfigPackage;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.Module;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.validate.GrmValidationException;
import com.sun.grid.grm.validate.ValidatorService;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.helpers.ValidationEventImpl;
import javax.xml.bind.helpers.ValidationEventLocatorImpl;
import javax.xml.bind.util.ValidationEventCollector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Helper class which hides JAXB usage for storing/loading objects to/from XML files
 * How to store-load example with hedeby configuration objects
 * Example:
 * <pre>
 *   File tmp = new File("test.xml");
 *   ExecutorConfig executor = ExecutorConfig();
 *   executor.setCorePoolSize(50);
 *   executor.setIdleTimeout(30);    
 *   XMLUtil.store(executor, tmp);
 *   ExecutorConfig executor1 = (ExecutorConfig)XMLUtil.load(tmp);
 * </pre>
 */
public class XMLUtil {

    /**
     * Bundle which containes i18n strings
     */
    public static final String BUNDLE = "com.sun.grid.grm.config.configuration";
    private final static Logger log = Logger.getLogger(XMLUtil.class.getName(), BUNDLE);
    
    private static NamespacePrefixMapperImpl namespacePrefixMapper = null;
    private static JAXBContext context = null;
    private static Schema      schema  = null;
    
    private static final Map<Class, CacheElem> cache = new HashMap<Class, CacheElem>();

    /** Creates a new instance of XMLUtil */
    protected XMLUtil() {
    }

    /**
     * Store the object into XML to the hedeby distribution in the preferences.
     *
     * @param object Object that should be stored
     * @param filePath      File to which object should be stored
     * @throws com.sun.grid.grm.config.GrmXMLException when storing operation fails
     */
    static public void store(Object object, File filePath) throws GrmXMLException {

        try {
            CacheElem elem = getCacheElem(object);

            JAXBElement jaxbe = elem.createJAXBElement(object);

            createMarshaller().marshal(jaxbe, new FileWriter(filePath));

        } catch (IOException ex) {
            throw new GrmXMLException(ex.getLocalizedMessage(), ex);
        } catch(JAXBException ex) {
            throw createError(ex);
        }
    }

    /**
     * Write a object into a writer.
     *
     * @param object the object
     * @param writer the writer
     * @throws GrmXMLException when storing operation fails
     */
    public static void write(Object object, Writer writer) throws GrmXMLException {
        CacheElem elem = getCacheElem(object);

        JAXBElement jaxbe = elem.createJAXBElement(object);
        try {
            createMarshaller().marshal(jaxbe, writer);
        } catch(JAXBException ex) {
            throw createError(ex);
        }
    }

    private static GrmXMLException createError(JAXBException ex) {
        Throwable t = findRealCause(ex);
        if (t.getLocalizedMessage() != null) {
            return new GrmXMLException(t.getLocalizedMessage(), t);
        } else {
            return new GrmXMLException("xmle", t, BUNDLE);
        }
    }


    private static Throwable findRealCause(JAXBException ex) {

        Throwable t = ex;
        if(ex.getLinkedException() != null) {
            t = ex.getLinkedException();
        }
        if (t.getLocalizedMessage() == null) {
            // The message is only null if an I/O error occured
            // Search for an IOException in the exception hierachy
            for(Throwable tmp = t; tmp != null; tmp = tmp.getCause()) {
                if (tmp instanceof IOException) {
                    t = tmp;
                    break;
                }
            }
        }
        return t;
    }

    /**
     * Write a object into a OutputStream.
     *
     * @param object the object
     * @param out the OutputStream
     * @throws GrmXMLException if the write operation fails
     */
    public static void write(Object object, OutputStream out) throws GrmXMLException {
        CacheElem elem = getCacheElem(object);

        JAXBElement jaxbe = elem.createJAXBElement(object);
        try {
            createMarshaller().marshal(jaxbe, out);
        } catch (JAXBException ex) {
            throw createError(ex);
        }

    }

    /**
     * Unmarshals object from XML load from provided path.
     *     
     * @param filePath      File from which object should be read
     * @return object that was read from source XML
     * @throws GrmXMLException when loading operation fails
     */
    static public Object load(File filePath) throws GrmXMLException {
        return loadAndValidate(filePath, (ValidationEventCollector) null);
    }

     /**
     * Unmarshals object from XML load from provided InputStream.
     *
     * @param inputStream    Stream from which object should be read
     * @return object that was read from source XML
     * @throws GrmXMLException when loading operation fails
     */
    static public Object load(InputStream inputStream) throws GrmXMLException {
        return loadAndValidate(inputStream, (ValidationEventCollector) null);
    }


     /**
     * Unmarshals object from XML and provide validation errors
     * @param inputStream    Stream from which object should be read
     * @param collector   collector for the validation errors
     * @throws GrmXMLException when loading operation fails
     * @return object that was read from source XML
     */
    static private Object loadAndValidate(InputStream inputStream, ValidationEventCollector collector) throws GrmXMLException {

        try {
            Unmarshaller um = createUnmarshaller();
            if(collector != null) {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                factory.setNamespaceAware(true);
                factory.setSchema(getSchema());
                DocumentBuilder builder = factory.newDocumentBuilder();

                builder.setErrorHandler(new ValidationErrorHandler(collector));

                Document doc = builder.parse(inputStream);
                JAXBElement elem = (JAXBElement) um.unmarshal(doc);
                return elem.getValue();
            } else {
                JAXBElement elem = (JAXBElement) um.unmarshal(inputStream);
                return elem.getValue();
            }

        } catch(JAXBException ex) {
            throw createError(ex);
        } catch(SAXException ex) {
            throw new GrmXMLException("config.ex.loadAndValidate", ex, BUNDLE, ex.getLocalizedMessage());
        } catch(Exception ex) {
            throw new GrmXMLException(ex.getLocalizedMessage(), ex);
        }
    }


    /**
     * Unmarshals object from XML and provide validation errors
     * @param filePath    File from which object should be read
     * @param collector   collector for the validation errors
     * @throws GrmXMLException when loading operation fails
     * @return object that was read from source XML
     */
    static private Object loadAndValidate(File filePath, ValidationEventCollector collector) throws GrmXMLException {

        try {
            Unmarshaller um = createUnmarshaller();
            if(collector != null) {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                factory.setNamespaceAware(true);
                factory.setSchema(getSchema());
                DocumentBuilder builder = factory.newDocumentBuilder();

                builder.setErrorHandler(new ValidationErrorHandler(collector));

                Document doc = builder.parse(filePath);
                JAXBElement elem = (JAXBElement) um.unmarshal(doc);
                return elem.getValue();
            } else {
                JAXBElement elem = (JAXBElement) um.unmarshal(filePath);
                return elem.getValue();
            }

        } catch(JAXBException ex) {
            throw createError(ex);
        } catch(SAXException ex) {
            throw new GrmXMLException("config.ex.loadAndValidate", ex, BUNDLE, filePath, ex.getLocalizedMessage());
        } catch(Exception ex) {
            throw new GrmXMLException(ex.getLocalizedMessage(), ex);
        }
    }

    
    private static class ValidationErrorHandler implements ErrorHandler {

        private final ValidationEventCollector collector;

        public ValidationErrorHandler(ValidationEventCollector collector) {
            this.collector = collector;
        }

        private ValidationEvent createEvent(int severity, SAXParseException ex) {
            return new ValidationEventImpl(severity, ex.getLocalizedMessage(), new ValidationEventLocatorImpl(ex));
        }
        public void warning(SAXParseException ex) throws SAXException {
            collector.handleEvent(createEvent(ValidationEvent.WARNING, ex));
        }

        public void error(SAXParseException ex) throws SAXException {
            collector.handleEvent(createEvent(ValidationEvent.ERROR, ex));
        }

        public void fatalError(SAXParseException ex) throws SAXException {
            collector.handleEvent(createEvent(ValidationEvent.FATAL_ERROR, ex));
        }
        
    }

    /**
     * Unmarshals object from XML and provide validation errors
     * @param filePath   File from which object should be read
     * @throws GrmXMLException if the xml object could not be loaded 
     * @throws com.sun.grid.grm.validate.GrmValidationException if the xml object is not valid
     * @return object that was read from source XML
     */
    static public Object loadAndValidate(File filePath) throws GrmXMLException, GrmValidationException {

        ValidationEventCollector collector = new ValidationEventCollector();
        Object ret = loadAndValidate(filePath, collector);
        if (collector.hasEvents()) {
            List<String> errors = new LinkedList<String>();
            MessageFormat fmt = new MessageFormat(ResourceBundle.getBundle(BUNDLE).getString("validateEvent"));
            Object[] args = new Object[4];
            for (ValidationEvent evt : collector.getEvents()) {
                args[0] = evt.getSeverity();
                args[1] = evt.getLocator().getLineNumber();
                args[2] = evt.getLocator().getColumnNumber();
                args[3] = evt.getMessage();
                errors.add(fmt.format(args));
            }
            throw new GrmValidationException(errors);
        }
        return ret;
    }

    private static Modules.ModuleListener moduleListener;

    /**Helper methods for getting Context, Marshaller, Unmarshaller*/
    private synchronized static JAXBContext getJAXBContext() throws GrmXMLException {
        if (context == null) {
            if (moduleListener == null) {
                // Register a ModuleListener
                moduleListener = new Modules.ModuleListener() {

                    public void moduleAdded(Module module) {
                        synchronized (XMLUtil.class) {
                            context = null;
                            namespacePrefixMapper = null;
                            cache.clear();
                        }
                    }

                    public void moduleRemoved(Module module) {
                        synchronized (XMLUtil.class) {
                            context = null;
                            namespacePrefixMapper = null;
                            cache.clear();
                        }
                    }
                };
                Modules.addModuleListener(moduleListener);
            }

            StringBuilder packages = new StringBuilder();

            for (Module module : Modules.getModules()) {
                for (ConfigPackage pkg : module.getConfigPackages()) {
                    packages.append(':');
                    packages.append(pkg.getPackage());
                }
            }
            try {
                context = JAXBContext.newInstance(packages.toString());
            } catch(JAXBException ex) {
                throw createError(ex);
            }
            namespacePrefixMapper = new NamespacePrefixMapperImpl();
        }
        return context;
    }

    
    private static synchronized Marshaller createMarshaller() throws GrmXMLException {
        try {
            Marshaller marshaller = getJAXBContext().createMarshaller();
            marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", namespacePrefixMapper);
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            return marshaller;
        } catch(JAXBException ex) {
            throw createError(ex);
        }
    }

    private static Unmarshaller createUnmarshaller() throws GrmXMLException {
        try {
            return getJAXBContext().createUnmarshaller();
        } catch(JAXBException ex) {
            throw createError(ex);
        }
    }

    private static CacheElem getCacheElem(Object object) throws GrmXMLException {
        synchronized (cache) {
            CacheElem ret = cache.get(object.getClass());
            if (ret == null) {
                ret = createCacheElem(object.getClass());
                cache.put(object.getClass(), ret);
            }
            return ret;
        }
    }

    private static CacheElem createCacheElem(Class<?> clazz) throws GrmXMLException {
        for (Class tmpClass = clazz; tmpClass != null; tmpClass = tmpClass.getSuperclass()) {
            CacheElem ret = createCacheElemInternal(tmpClass);
            if (ret != null) {
                return ret;
            }
        }
        throw new GrmXMLException("config.exception.no.object.factory", BUNDLE, clazz.getName());
    }

    private static CacheElem createCacheElemInternal(Class<?> clazz) {
        String packageName = clazz.getPackage().getName();

        StringBuilder buf = new StringBuilder(packageName.length() +
                ".ObjectFactory".length());

        buf.append(packageName);
        buf.append(".ObjectFactory");
        try {
            Class factoryClass = Class.forName(buf.toString());
            if (Modifier.isPublic(factoryClass.getModifiers())) {
                for (Method method : factoryClass.getMethods()) {
                    if (Modifier.isPublic(method.getModifiers()) && method.getName().startsWith("create") && method.getReturnType().equals(JAXBElement.class)) {
                        Class<?>[] paramTypes = method.getParameterTypes();
                        if (paramTypes != null && paramTypes.length == 1 && paramTypes[0].isAssignableFrom(clazz)) {
                            return new CacheElem(factoryClass.newInstance(), method);
                        }
                    }
                }
            }
            return null;
        } catch (ClassNotFoundException ex) {
            return null;
        } catch (InstantiationException ex) {
            return null;
        } catch (IllegalAccessException ex) {
            return null;
        }
    }

    private static synchronized Schema getSchema() throws GrmXMLException {
        if(schema == null) {
            schema = createSchema();
        }
        return schema;
    }

    private static Schema createSchema() throws GrmXMLException {
        try {
            DependencyTree<ConfigPackage> depTree = new DependencyTree<ConfigPackage>();

            for (Module module : Modules.getModules()) {
                for (ConfigPackage cp : module.getConfigPackages()) {
                    depTree.add(cp.getNamespacePrefix(), cp, cp.getDependencies());
                }
            }

            Source[] sources = new Source[depTree.size()];
            int i = 0;
            for (ConfigPackage cp : depTree) {
                URL url = cp.getLocalURL();
                if (url == null) {
                    throw new GrmXMLException("config.ex.schema.noURL", BUNDLE, cp.getNamespace());
                }
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "config.schema.load", new Object[]{cp.getNamespace(), cp.getLocalURL()});
                }
                try {
                    sources[i++] = new StreamSource(cp.getLocalURL().openStream());
                } catch (IOException ex) {
                    throw new GrmXMLException("config.ex.schema.IO", ex, BUNDLE, cp.getNamespace(), url);
                }
            }

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            return schemaFactory.newSchema(sources);
        } catch (SAXException ex) {
            throw new GrmXMLException("config.ex.schema.parse", ex, BUNDLE);
        }
    }
    
    /**
     *
     * Class that creating objects to store in XMLUtil cache
     */
    private static class CacheElem {

        private final Object factory;
        private final Method createMethod;

        public CacheElem(Object factory, Method createMethod) {
            this.factory = factory;
            this.createMethod = createMethod;
        }

        public Object getFactory() {
            return this.factory;
        }

        public Method getCreateMethod() {
            return this.createMethod;
        }

        /*
         * This method create a JAXBElement object out of given object
         * @param object object to be transformed to JAXBElement object
         * @return JAXBElement object
         */
        public JAXBElement createJAXBElement(Object object) throws GrmXMLException {
            JAXBElement element = null;
            try {
                element = (JAXBElement) getCreateMethod().invoke(getFactory(), object);
            } catch (IllegalArgumentException ex) {
                throw new GrmXMLException("config.exception.property.illegal.argument", ex, BUNDLE);
            } catch (InvocationTargetException ex) {
                throw new GrmXMLException(ex.getLocalizedMessage(), ex);
            } catch (IllegalAccessException ex) {
                throw new GrmXMLException("config.exception.property.no.access", ex, BUNDLE);
            }
            return element;
        }
    }

    /**
     *  parseMethod for xml string. 
     *
     *  see also in util/xml/bindling.xml
     *
     *  @param str the string
     * @return the trimmed string
     */
    public static String parseString(String str) {
        if (str == null) {
            return null;
        } else {
            return str.trim();
        }
    }
    

    /**
     * Open a configuration in the editor
     * @param env     the execution env
     * @param obj     the configuration obj
     * @param prefix  the prefix for the tmp filename
     * @return  the edited configuration of <code>null</code> if the configuration did not changed
     * @throws com.sun.grid.grm.GrmException  on any unexpected error (I/O error)
     */
    @SuppressWarnings("unchecked")
    public static <T> T edit(ExecutionEnv env, T obj, String prefix) throws GrmException {
        
        File tmpFile = null;
        try {
            try {
                tmpFile = File.createTempFile(prefix, "tmp");
                tmpFile.deleteOnExit();
                XMLUtil.store(obj, tmpFile);
            } catch(IOException ex) {
                throw new GrmException("XMLUtil.ex.store", ex, BUNDLE, ex.getLocalizedMessage());
            }

            if (Platform.getPlatform().edit(tmpFile)) {
                @SuppressWarnings("unchecked")
                Object newConf = XMLUtil.loadAndValidate(tmpFile);
                
                ValidatorService.validateChain(env, newConf, obj);
                return (T)newConf;
            } else {
                return null;
            }
        } finally {
            if(tmpFile != null) {
                tmpFile.delete();
            }
        }
        
    }
}
