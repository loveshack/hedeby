/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.config.naming.event;

import com.sun.grid.grm.management.ManagedEventListener;
import java.util.EventListener;

/**
 *
 * Interface that describes the events that come from CS.
 */
@ManagedEventListener
public interface ConfigurationServiceEventListener extends EventListener {
    
    /*
     * Configuration Service informs listeners that new configuration object was stored.
     * @param event object event
     */ 
    public void configurationObjectAdded(ConfigurationObjectAddedEvent event);
    
    /*
     * Configuration Service informs listeners that configuration object was removed.
     * @param event object event
     */ 
    public void configurationObjectRemoved(ConfigurationObjectRemovedEvent event);
    
    /*
     * Configuration Service informs listeners that configuration object was changed.
     * @param event object event
     */ 
    public void configurationObjectChanged(ConfigurationObjectChangedEvent event);
}
