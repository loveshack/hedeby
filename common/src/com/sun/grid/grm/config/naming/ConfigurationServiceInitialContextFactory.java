/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.config.naming;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.spi.InitialContextFactory;

/**
 * This Factory products an initial Context
 * 
 * To get a valid Context you have to provide proper values in Enviroment Properties
 * 
 * You have to provide following values:
 *
 * Context.INITIAL_CONTEXT_FACTORY - fully qualified class name of the Context Factory. This class name.
 * Context.PROVIDER_URL - url to the shared directory for Hedeby instance and there cs directory.
 *
 * Here is an code example how to get a initial Context:
 * <pre>
 * Hashtable&lt;String, Object&gt; env = new Hashtable&lt;String, Object&gt;();
 * env.put(Context.INITIAL_CONTEXT_FACTORY,
 *         "com.sun.grid.grm.config.naming.ConfigurationServiceInitialContextFactory");
 * env.put(Context.PROVIDER_URL, 
 *         csDir.toURL());
 * 
 * InitialContext = new InitialContext(env);
 * </pre>
 */
public class ConfigurationServiceInitialContextFactory implements InitialContextFactory{

    public Context getInitialContext(Hashtable<?, ?> env) throws NamingException {
        return new ConfigurationServiceContext(env);
    }    
}
