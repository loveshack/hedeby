/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.config.naming.event;

import java.io.Serializable;

/**
 *
 * Abstract clas for Configuration Sevice Events notifications.
 */
public class AbstractConfigurationServiceEvent implements Serializable, Comparable<AbstractConfigurationServiceEvent> {
    
    /**
     * The serial version UID
     */
    private static final long serialVersionUID = 2008080701;
    private final long timestamp;
    private final long sequenceNumber;
    private final String csName;
    private final String message;
    private final String name;
    private final String host;
    private final String contextPath;
    private final String type;
    /**
     * Creates a new instance of AbstractServiceEvent
     * @param aSequenceNumber the sequence number for the event
     * @param aCSName the CS name
     * @param aMessage the message
     * @param aName  name of the configuration object
     * @param aHost  hostname of the configuration object
     * @param aContextPath the context path
     * @param aType the type of the event
     */
    public AbstractConfigurationServiceEvent(long aSequenceNumber, String aCSName, String aMessage, String aName, String aHost, String aContextPath, String aType) {
        if(aCSName == null) {
            throw new NullPointerException();
        }
        timestamp = System.currentTimeMillis();
        sequenceNumber = aSequenceNumber;
        csName = aCSName;
        message= aMessage;
        name = aName;
        host = aHost;
        contextPath = aContextPath;
        type = aType;
    } 
    
    public int compareTo(AbstractConfigurationServiceEvent o) {
        int ret = 0;
        if ((ret = csName.compareTo(o.getCSName())) != 0) {
            return ret;
        }
        if ((ret = message.compareTo(o.getMessage())) != 0) {
            return ret;
        }
        if ((ret = name.compareTo(o.getName())) != 0) {
            return ret;
        }
        if ((ret = host.compareTo(o.getHost())) != 0) {
            return ret;
        }
        if ((ret = contextPath.compareTo(o.getContextPath())) != 0) {
            return ret;
        }
        if ((ret = type.compareTo(o.getType())) != 0) {
            return ret;
        }
        if (sequenceNumber < o.sequenceNumber) {
            ret = -1;
        } else if (sequenceNumber > o.sequenceNumber) {
            ret = 1;
        } else {
            ret = 0;
        }
        return ret;
    }
    
    /**
     * get the sequence number of the event
     * @return the sequence number
     */
    public long getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * get the name of the cs
     * @return the name of the cs
     */
    public String getCSName() {
        return csName;
    }
    
    /**
     * get the name of the configuration object
     * @return the name of the configuration object
     */
    public String getName() {
        return name;
    }

    /**
     * get the host of the configuration object
     * @return the host of the configuration object
     */
    public String getHost() {
        return host;
    }
    
    /**
     * get the context path to the object
     * @return the context path to the object
     */
    public String getContextPath() {
        return contextPath;
    }
    
    /**
     * get the message that describes event
     * @return the message that describes event
     */
    public String getMessage() {
        return message;
    }
    
    /**
     * get the type of the object that was added to the cs
     * @return the type of the added object
     */
    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractConfigurationServiceEvent other = (AbstractConfigurationServiceEvent) obj;
        if (this.sequenceNumber != other.sequenceNumber) {
            return false;
        }
        if ((this.csName == null) ? (other.csName != null) : !this.csName.equals(other.csName)) {
            return false;
        }
        if ((this.message == null) ? (other.message != null) : !this.message.equals(other.message)) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.host == null) ? (other.host != null) : !this.host.equals(other.host)) {
            return false;
        }
        if ((this.contextPath == null) ? (other.contextPath != null) : !this.contextPath.equals(other.contextPath)) {
            return false;
        }
        if ((this.type == null) ? (other.type != null) : !this.type.equals(other.type)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.sequenceNumber ^ (this.sequenceNumber >>> 32));
        hash = 59 * hash + (this.csName != null ? this.csName.hashCode() : 0);
        hash = 59 * hash + (this.message != null ? this.message.hashCode() : 0);
        hash = 59 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 59 * hash + (this.host != null ? this.host.hashCode() : 0);
        hash = 59 * hash + (this.contextPath != null ? this.contextPath.hashCode() : 0);
        hash = 59 * hash + (this.type != null ? this.type.hashCode() : 0);
        return hash;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }
}
