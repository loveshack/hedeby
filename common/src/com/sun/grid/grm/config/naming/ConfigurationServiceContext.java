/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.config.naming;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.config.GrmXMLException;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventListener;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventSupport;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.reporting.Reporter;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import javax.management.ObjectName;
import javax.naming.Binding;
import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameClassPair;
import javax.naming.NameNotFoundException;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.NotContextException;

/**
 * Configuration Service Context that is used to store and load configurations
 * objects for Hedeby.
 *
 * Here is an explenation how to get subcontext and entries.
 * This is a hierarchical Context, so the structure is that there is a root Context
 * which can contain subContext or entries. 
 *
 * The structure for this Context is following:
 *
 * 
 *          
 *            myHedeby - hedeby instance (root Context)
 *                       |
 *                       | - global - global config object
 *                       | - security - security config object
 *                       | - component - context with components config objects
 *                       |              | 
 *                       |              | - rp - resource provider config object
 *                       |              | - sc - service container confug object
 *                       |              ...
 *                       | - active_components - context with active components objects
 *                                             |
 *                                             | - rp - resource provider object
 *                                             | - sc - service container object
 *
 * Examples:
 *
 * ctx.list(""); - this will list instances of Hedeby;
 * result:
 * global.xml
 * security.xml
 * component
 * active_components
 *
 * ctx.list("component"); - this will return NamingEnumeration object with the names of the components configs
 * result:
 * rp.xml
 * sc.xml
 *
 * ctx.listBindings("active_components"); - this will return the NamingEnumeration object
 * with the name and assosiated object for active copmonents.
 * result:
 * rp, ActiveComponent
 * sc, ActiveComponent
 *
 * Object obj = ctx.lookup("component.rp"); - this will get the rp config entry
 *
 * ctx.bind("active_components.sa", object) - this will bind (store the file in fs) sa object in
 * component subcontext
 */
public class ConfigurationServiceContext implements Context {

    protected Hashtable<?, ?> myEnv;
    protected File contextPath;
    private static final ConfigurationServiceParser myParser = new ConfigurationServiceParser();
    private static final String BUNDLE = "com.sun.grid.grm.config.naming.messages";
    private static final String XML_SUFFIX = ".xml";
    private static final ConfigurationServiceEventSupport listeners = new ConfigurationServiceEventSupport();
    /*
     * This private Context creates a hierarchical structure of Contexts
     * @param env Context Enviroment variable
     * @param level keep the level number of the context
     * @param path is a path to directory related to this context on file system
     * @throw NamingException when the Context directory doesnt exists, its not a directory or when
     * URL exception was thrown
     */

    private ConfigurationServiceContext(Hashtable<?, ?> env,
            File path)
            throws NamingException {
        this.myEnv = (Hashtable<?, ?>) env.clone();

        if (path == null) {
            contextPath = new File(((URL) env.get(Context.PROVIDER_URL)).getFile());
        } else {
            contextPath = path;
        }

        FileCache.CachedFile file = FileCache.getInstance(contextPath);
        //Check if this is directory
        if (!(file.getPath().isDirectory())) {
            throw new NamingException(
                    I18NManager.formatMessage("context.not.found",
                    BUNDLE,
                    contextPath));
        }

    }

    /*
     * Constructor that is used by Context Factory to create an initial Context 
     * @param env Context Enviroment variable
     * @throw NamingException when the Context directory doesnt exists, its not a directory or when
     * URL exception was thrown
     */
    public ConfigurationServiceContext(Hashtable<?, ?> env) throws NamingException {
        this(env,
                null);
    }

    protected Context cloneContext() throws NamingException {
        return new ConfigurationServiceContext(myEnv);
    }

    /*
     * This method returns object from a given Context or the Context itself
     * using JAXB from file system.
     * @param name name of a object or Context as Name type
     * @return object or a Context
     * @throw NamingException if object or Context was not found
     */
    public Object lookup(Name name) throws NamingException {
        if (name.isEmpty()) {
            return cloneContext();
        }
        Name nm = getMyComponents(name);
        String tempName = nm.get(0);
        String atom = getContextPath(tempName);

        if (nm.size() == 1) {
            File newPath = new File(atom);

            //get representation of file
            FileCache.CachedFile af = FileCache.getInstance(newPath);

            //check if representation points to directory
            if (af.getPath().isDirectory()) {
                ConfigurationServiceContext ctx = new ConfigurationServiceContext(myEnv, newPath);
                return ctx;
            }
            File configFile = new File(atom + XML_SUFFIX);
            //get file representation for leaf
            af = FileCache.getInstance(configFile);

            //check if file really exists
            if (af.getPath().exists()) {
                try {
                    //read file content
                    Object object = af.read();
                    return object;

                } catch (GrmXMLException ex) {
                    NamingException ne = new NamingException(I18NManager.formatMessage("context.jaxb.exception.load", BUNDLE, af.getPath().getName()));
                    ne.initCause(ex);
                    throw ne;
                } catch (IOException ex) {
                    NamingException ne = new NamingException(I18NManager.formatMessage("context.jaxb.exception.load", BUNDLE, af.getPath().getName()));
                    ne.initCause(ex);
                    throw ne;
                }
            } else {
                throw new NameNotFoundException(I18NManager.formatMessage("context.name.not.found", BUNDLE, atom));
            }
        } else {
            File newFile = new File(atom);

            //get representation for file
            FileCache.CachedFile af = FileCache.getInstance(newFile);

            //it has to be directory - subcontext expected
            if (af.getPath().isDirectory()) {
                ConfigurationServiceContext ctx = new ConfigurationServiceContext(myEnv, af.getPath());
                Object object = ctx.lookup(nm.getSuffix(1));

                return object;
            } else {
                throw new NameNotFoundException(I18NManager.formatMessage("context.name.not.found", BUNDLE, atom));
            }
        }
    }

    /*
     * This method returns object from a given Context or the Context itself
     * using JAXB from file system.
     * @param string name of a object or Context as String type
     * @return object or a Context
     * @throw NamingException if object or Context was not found
     */
    public Object lookup(String string) throws NamingException {
        return lookup(new CompositeName(string));
    }

    /*
     * This method bind a object with the given name and then store it
     * using JAXB in the file system.
     * @param name name for a object to bind as Name type.
     * @param object ab object to bind.
     * @throw NamingException if an error when binding an object
     */
    public void bind(Name name, Object object) throws NamingException {
        if (name.isEmpty()) {
            throw new InvalidNameException(
                    I18NManager.formatMessage("context.cant.bind.empty",
                    BUNDLE));
        }
        Name nm = getMyComponents(name);
        String tempName = nm.get(0);
        String atom = getContextPath(tempName);

        if (nm.size() == 1) {
            String atomFile = atom + XML_SUFFIX;
            File newFile = new File(atomFile);

            //get representation for file
            FileCache.CachedFile af = FileCache.getInstance(newFile);

            //if exists - error occured
            if (af.getPath().exists()) {
                throw new NameAlreadyBoundException(
                        I18NManager.formatMessage("context.use.rebind",
                        BUNDLE,
                        atomFile));
            }
            //create file with content
            try {
                af.write(object);
            } catch (GrmXMLException ex) {
                NamingException ne = new NamingException(
                        I18NManager.formatMessage("context.jaxb.exception.store",
                        BUNDLE,
                        atomFile));
                ne.initCause(ex);
                throw ne;
            } catch (IOException ex) {
                NamingException ne = new NamingException(I18NManager.formatMessage("context.jaxb.exception.store",
                        BUNDLE,
                        af.getPath().getName()));
                ne.initCause(ex);
                throw ne;
            }
            listeners.fireConfigurationObjectAdded(resolveName(tempName), getObjectHost(object, contextPath.getName()), contextPath.getName(),
                    I18NManager.formatMessage("cs_context.objectadded", BUNDLE, tempName, contextPath.getName()),
                    getObjectType(object, contextPath.getName()));
        } else {
            try {
                FileCache.CachedFile af = FileCache.getInstance(contextPath);
                af.createDir(tempName);
            } catch (IOException ex) {
                throw new NamingException(I18NManager.formatMessage("context.not_created", BUNDLE, contextPath.getAbsolutePath()));
            }

            ConfigurationServiceContext ctx =
                    (ConfigurationServiceContext) this.lookup(tempName);
            ctx.bind(nm.getSuffix(1),
                    object);

        }
    }
    /*
     * This method bind an object with the given name and then store it
     * using JAXB in the file system.
     * @param name name for a object to bind as String type.
     * @param object ab object to bind.
     * @throw NamingException if an error when binding an object
     */

    public void bind(String string, Object object) throws NamingException {
        bind(new CompositeName(string),
                object);
    }

    /*
     * This method re-bind an object with the given name in the file system. If
     * there is no object with that name than its created. 
     * @param name name for a object to re-bind as Name type.
     * @param object ab object to re-bind.
     * @throw NamingException if an error when re-binding an object
     */
    public void rebind(Name name, Object object) throws NamingException {
        if (name.isEmpty()) {
            throw new InvalidNameException(
                    I18NManager.formatMessage("context.cant.bind.empty",
                    BUNDLE));
        }
        Name nm = getMyComponents(name);
        String tempName = nm.get(0);
        String atom = getContextPath(tempName);

        if (nm.size() == 1) {
            String atomFile = atom + XML_SUFFIX;
            File newPath = new File(atomFile);

            //get file representation
            FileCache.CachedFile af = FileCache.getInstance(newPath);

            try {
                //write content to file
                af.write(object);
            } catch (GrmXMLException ex) {
                NamingException ne = new NamingException(
                        I18NManager.formatMessage("context.jaxb.exception.store",
                        BUNDLE,
                        atom));
                ne.initCause(ex);
                throw ne;
            } catch (IOException ex) {
                NamingException ne = new NamingException(I18NManager.formatMessage("context.jaxb.exception.store",
                        BUNDLE,
                        af.getPath().getName()));
                ne.initCause(ex);
                throw ne;
            }
            listeners.fireConfigurationObjectChanged(resolveName(tempName), getObjectHost(object, contextPath.getName()), contextPath.getName(),
                    I18NManager.formatMessage("cs_context.objectchanged", BUNDLE, tempName, contextPath.getName()),
                    getObjectType(object, contextPath.getName()));
        } else {

            FileCache.CachedFile af = FileCache.getInstance(new File(atom));

            //check if really directory
            if (af.getPath().isDirectory()) {
                ConfigurationServiceContext ctx =
                        (ConfigurationServiceContext) this.lookup(tempName);
                ctx.rebind(nm.getSuffix(1),
                        object);

            } else {
                throw new NotContextException(
                        I18NManager.formatMessage("context.doesnt.name.context",
                        BUNDLE,
                        atom));
            }
        }
    }

    /*
     * This method re-bind an object with the given name in the file system. If
     * there is no object with that name than its created. 
     * @param string name for a object to re-bind as String type.
     * @param object - object to re-bind.
     * @throw NamingException if an error when re-binding an object
     */
    public void rebind(String string, Object object) throws NamingException {
        rebind(new CompositeName(string), object);
    }

    /*
     * This method unbind a object with the given name and then remove it
     * from the file system.
     * @param name name for a object to unbind as Name type.
     * @throw NamingException if an error when binding an object
     */
    public void unbind(Name name) throws NamingException {
        if (name.isEmpty()) {
            throw new InvalidNameException(
                    I18NManager.formatMessage("context.cant.unbind.empty",
                    BUNDLE));
        }
        Name nm = getMyComponents(name);
        String tempName = nm.get(0);
        String atom = getContextPath(tempName);

        if (nm.size() == 1) {
            String atomFile = atom;
            File file = new File(contextPath, atomFile);

            FileCache.CachedFile af = FileCache.getInstance(file);

            //unbind on context is not allowed
            if (af.getPath().isDirectory()) {
                NamingException ne = new NamingException(
                        I18NManager.formatMessage("context.cannot.unbind.file",
                        BUNDLE,
                        af.getPath().getAbsolutePath()));
                throw ne;
            } else {
                atomFile = atom + XML_SUFFIX;
                file = new File(atomFile);

                af = FileCache.getInstance(file);

                //check if file really exists
                if (!af.getPath().exists()) {
                    throw new NameNotFoundException(
                            I18NManager.formatMessage("context.name.not.found",
                            BUNDLE,
                            atomFile));
                }
            }
            String[] list = af.getPath().list();
            if (!af.getPath().isDirectory() || (list != null && list.length == 0)) {
                Object object = lookup(name);
                try {
                    //remove file
                    af.delete();
                } catch (IOException ex) {
                    NamingException ne = new NamingException(
                            I18NManager.formatMessage("context.cannot.unbind.file",
                            BUNDLE,
                            atom));
                    throw ne;
                }
                listeners.fireConfigurationObjectRemoved(resolveName(tempName), getObjectHost(object, contextPath.getName()), contextPath.getName(),
                        I18NManager.formatMessage("cs_context.objectremoved", BUNDLE, tempName, contextPath.getName()),
                        getObjectType(object, contextPath.getName()));
            } else {
                NamingException ne = new NamingException(
                        I18NManager.formatMessage("context.cannot.unbind.file",
                        BUNDLE,
                        atom));
                throw ne;
            }
        } else {
            File file = new File(atom);
            FileCache.CachedFile af = FileCache.getInstance(file);

            //check if really directory
            if (!af.getPath().isDirectory()) {
                throw new NotContextException(atom +
                        I18NManager.formatMessage("context.doesnt.name.context",
                        BUNDLE));
            }
            ConfigurationServiceContext ctx = null;
            ctx = new ConfigurationServiceContext(myEnv, new File(atom));
            ctx.unbind(nm.getSuffix(1));
        }
    }

    /*
     * This method unbind a object with the given name and then remove it
     * from the file system.
     * @param name name for a object to unbind as String type.
     * @throw NamingException if an error when binding an object
     */
    public void unbind(String string) throws NamingException {
        unbind(new CompositeName(string));
    }

    public void rename(Name name, Name name0) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public void rename(String string, String string0) throws NamingException {
        throw new UnsupportedOperationException();
    }

    /* This method return the key names for an object stored in a given Context.
     * @param name Context name to list of Name type.
     * @return NamingEnumeration<NameClassPair> object with object names that are
     * stored in given Context
     * @throw NamingException when given name cannot be listed.
     */
    public NamingEnumeration<NameClassPair> list(Name name) throws NamingException {
        if (name.isEmpty()) {
            List<FileCache.CachedFile> list;
            try {
                FileCache.CachedFile af = FileCache.getInstance(this.contextPath);
                list = af.list();
            } catch (IOException ex) {
                throw new NameNotFoundException(I18NManager.formatMessage("context.filecache.error.elem", BUNDLE, this.contextPath.getAbsolutePath()));
            }
            Set<String> ret = new HashSet<String>(list.size());
            for (FileCache.CachedFile a : list) {
                ret.add(a.getPath().getPath());
            }
            return new ListOfNames(ret);
        }

        Object target = lookup(name);
        if (target instanceof ConfigurationServiceContext) {
            return ((ConfigurationServiceContext) target).list("");
        }
        throw new NotContextException(
                I18NManager.formatMessage("context.cannot.be.listed",
                BUNDLE,
                name));
    }

    /* This method return the key names for an object stored in a given Context.
     * @param name Context name to list of String type.
     * @return NamingEnumeration<NameClassPair> object with object names that are
     * stored in given Context
     * @throw NamingException when given name cannot be listed.
     */
    public NamingEnumeration<NameClassPair> list(String name) throws NamingException {
        return list(new CompositeName(name));
    }

    /* This method return the bindings stored in a given Context.
     * @param name Context name to list bindings from of Name type.
     * @return NamingEnumeration<Binding> object with objects that are
     * stored in given Context
     * @throw NamingException when given name cannot be listed.
     */
    public NamingEnumeration<Binding> listBindings(Name name) throws NamingException {
        if (name.isEmpty()) {
            List<FileCache.CachedFile> list;
            try {
                FileCache.CachedFile af = FileCache.getInstance(this.contextPath);
                list = af.list();
            } catch (IOException ex) {
                throw new NameNotFoundException(I18NManager.formatMessage("context.filecache.error.elem", BUNDLE, this.contextPath.getAbsolutePath()));
            }
            Set<String> ret = new HashSet<String>(list.size());
            for (FileCache.CachedFile a : list) {
                ret.add(a.getPath().getPath());
            }
            return new ListOfBindings(ret);
        }

        Object target = lookup(name);
        if (target instanceof ConfigurationServiceContext) {
            return ((ConfigurationServiceContext) target).listBindings("");
        }
        throw new NotContextException(
                I18NManager.formatMessage("context.cannot.be.listed",
                BUNDLE,
                name));
    }

    /* This method return the bindings stored in a given Context.
     * @param name Context name to list bindings from of String type.
     * @return NamingEnumeration<Binding> object with objects that are
     * stored in given Context
     * @throw NamingException when given name cannot be listed.
     */
    public NamingEnumeration<Binding> listBindings(String name) throws NamingException {
        return listBindings(new CompositeName(name));
    }

    public void destroySubcontext(Name name) throws NamingException {
    }

    public void destroySubcontext(String string) throws NamingException {
    }

    public Context createSubcontext(Name name) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public Context createSubcontext(String string) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public Object lookupLink(Name name) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public Object lookupLink(String string) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public NameParser getNameParser(Name name) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public NameParser getNameParser(String string) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public Name composeName(Name name, Name name0) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public String composeName(String string, String string0) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public Object addToEnvironment(String string, Object object) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public Object removeFromEnvironment(String string) throws NamingException {
        throw new UnsupportedOperationException();
    }

    public Hashtable<?, ?> getEnvironment() throws NamingException {
        throw new UnsupportedOperationException();
    }

    public void close() throws NamingException {
    }

    public String getNameInNamespace() throws NamingException {
        throw new UnsupportedOperationException();
    }

    /*
     * This method parse the name that is separated by comas to atoms.
     */
    protected Name getMyComponents(Name name) throws NamingException {
        if (name instanceof CompositeName) {
            if (name.size() > 1) {
                throw new InvalidNameException(
                        I18NManager.formatMessage("context.too.much.components",
                        BUNDLE,
                        name.toString()));
            }
            return myParser.parse(name.get(0));
        } else {
            return name;
        }
    }

    private String getContextPath(String atomicName) {
        StringBuilder sb = new StringBuilder(contextPath.getPath());
        sb.append(File.separatorChar);
        sb.append(atomicName);
        return sb.toString();
    }

    /*
     * Helper class
     */
    class BaseListOfNames {

        protected Iterator<String> iterator;

        BaseListOfNames(Set<String> names) {
            /* a new set has to be created - w/o that there is a risk
             * of concurrent modification 
             */
            this.iterator = new HashSet<String>(names).iterator();
        }

        public boolean hasMoreElements() {
            try {
                return hasMore();
            } catch (NamingException e) {
                return false;
            }
        }

        public boolean hasMore() throws NamingException {
            return iterator.hasNext();
        }

        public void close() {
        }
    }

    /*
     * Helper class for enumerating names
     */
    class ListOfNames extends BaseListOfNames implements NamingEnumeration<NameClassPair> {

        ListOfNames(Set<String> names) {
            super(names);
        }

        public NameClassPair next() throws NamingException {
            String name = iterator.next();
            String relName = getRelativeName(name);
            String className;
            try {

                FileCache.CachedFile af = FileCache.getInstance(new File(name));
                if (af.getPath().isDirectory()) {
                    className = ConfigurationServiceContext.class.getName();
                } else {
                    className = af.read().getClass().getName();
                }
            } catch (GrmXMLException ex) {
                NamingException ne = new NamingException(I18NManager.formatMessage("context.jaxb.exception.load", BUNDLE, name));
                ne.initCause(ex);
                throw ne;
            } catch (IOException ex) {
                throw new NameNotFoundException(I18NManager.formatMessage("context.filecache.error.elem", BUNDLE, name));
            }

            return new NameClassPair(relName,
                    className);
        }

        public NameClassPair nextElement() {
            try {
                return next();
            } catch (NamingException e) {
                NoSuchElementException ne = new NoSuchElementException();
                ne.initCause(e);
                throw ne;
            }
        }
    }

    private static String getRelativeName(String absName) {
        int start = absName.lastIndexOf(File.separatorChar);
        if (start < 0) {
            start = 0;
        } else {
            start++;
        }
        int end = 0;
        // Strip the xml suffix        
        if (absName.endsWith(".xml") || absName.endsWith(".XML")) {
            end = absName.length() - 4;
        } else {
            end = absName.length();
        }
        return absName.substring(start, end);
    }
    /*
     * Helper class for enumerating bindings
     */

    class ListOfBindings extends BaseListOfNames implements NamingEnumeration<Binding> {

        ListOfBindings(Set<String> names) {
            super(names);
        }

        public Binding next() throws NamingException {
            String name = iterator.next();
            String relName = getRelativeName(name);
            Object object = null;
            FileCache.CachedFile af = FileCache.getInstance(new File(name));
            if (af.getPath().isDirectory()) {
                object = new ConfigurationServiceContext(myEnv, af.getPath());
            } else {
                try {
                    object = af.read();
                } catch (GrmXMLException ex) {
                    NamingException ne = new NamingException(I18NManager.formatMessage("context.jaxb.exception.load", BUNDLE, af.getPath().getName()));
                    ne.initCause(ex);
                    throw ne;
                } catch (IOException ex) {
                    NamingException ne = new NamingException(I18NManager.formatMessage("context.jaxb.exception.load", BUNDLE, af.getPath().getName()));
                    ne.initCause(ex);
                    throw ne;
                }
            }
            return new Binding(relName, object);
        }

        public Binding nextElement() {
            try {
                return next();
            } catch (NamingException ex) {
                NoSuchElementException ne = new NoSuchElementException();
                ne.initCause(ex);
                throw ne;
            }
        }
    }

    public static void addConfigurationServiceEventListener(ConfigurationServiceEventListener listener) {
        listeners.addConfigurationServiceEventListener(listener);
    }

    public static void removeConfigurationServiceEventListener(ConfigurationServiceEventListener listener) {
        listeners.addConfigurationServiceEventListener(listener);
    }

    private String getObjectType(Object object, String context) {
        if (context.equals(BootstrapConstants.CS_ACTIVE_COMPONENT)) {
            if (object instanceof ActiveComponent) {
                String mBeanName = ((ActiveComponent) object).getMBeanName();
                try {
                    ObjectName on = new ObjectName(mBeanName);
                    return on.getKeyProperty("type");
                } catch (Exception ex) {
                    if (mBeanName.endsWith(Service.class.getName())) {
                        return Service.class.getName();
                    } else if (mBeanName.endsWith(Reporter.class.getName())) {
                        return Reporter.class.getName();
                    } else if (mBeanName.endsWith(ResourceProvider.class.getName())) {
                        return ResourceProvider.class.getName();
                    } else if (mBeanName.endsWith(Executor.class.getName())) {
                        return Executor.class.getName();
                    } else {
                        return GrmComponent.class.getName();
                    }
                }
            }
        }

        if (context.equals(BootstrapConstants.CS_ACTIVE_JVM)) {
            if (object instanceof ActiveJvm) {
                return JVM.class.getName();
            }
        }
        return "Other";
    }

    private String getObjectHost(Object object, String context) {
        if (context.equals(BootstrapConstants.CS_ACTIVE_COMPONENT)) {
            if (object instanceof ActiveComponent) {
                return ((ActiveComponent) object).getHost();
            }
        }
        if (context.equals(BootstrapConstants.CS_ACTIVE_JVM)) {
            if (object instanceof ActiveJvm) {
                return ((ActiveJvm) object).getHost();
            }
        }
        return "unknown";
    }

    private String resolveName(String name) {
        if (name.contains("@")) {
            return name.substring(0, name.indexOf("@"));
        }
        return name;
    }
}
