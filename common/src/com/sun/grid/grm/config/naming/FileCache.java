/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.config.naming;

import com.sun.grid.grm.config.GrmXMLException;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Caching mechanism for directory and file contents. Thread-safe.
 */
public class FileCache {

    private final static Map<File, CachedFile> instances = new WeakHashMap<File, CachedFile>();
    private final static String BUNDLE = "com.sun.grid.grm.config.naming.messages";

    /**
     * Method for obtaining the instance representing File.
     * @param path - File for which instance should be obtained
    
     * @return <code>FileCache.CachedFile</code> instance - a <code>FileCache.CachedFile</code>
     *          object that is thread-safe
     */
    public static synchronized CachedFile getInstance(File path) {
        CachedFile ret = null;
        ret = instances.get(path);
        if (ret == null) {
            ret = new CachedFile(path);
            instances.put(path, ret);
        }
        return ret;
    }

    /**
     * Object representing cached file
     */
    public static class CachedFile {

        private WeakReference cachedObjRef;
        private long lastUpdate;
        private final File path;
        private List<CachedFile> children;

        /**
         * Getter for path of this object
         * @return File object that is represented by this
         */
        public File getPath() {
            return path;
        }

        private CachedFile(File path) {
            this.path = path;
        }

        /**
         * Read the content of file. Operation is performed using XMLUtil tool.
         *
         * @return Object representing the content of file
         * @throws GrmXMLException when operation of reading fails
         * @throws IOException when the operation will be performed on instance representing directory
         */
        public synchronized Object read() throws GrmXMLException, IOException {
            if (path.isDirectory()) {
                throw new IOException(I18NManager.formatMessage("filecache.error.file_operation",
                        BUNDLE,
                        path.getAbsolutePath()));
            }
            /* reference to cached object must be kept out of 'if' block, 
             * otherwise we risk that it is immediately GC */
            Object cachedObj = null;
            if (cachedObjRef == null) {
                cachedObj = XMLUtil.load(path);
                cachedObjRef = new WeakReference<Object>(cachedObj);
                lastUpdate = path.lastModified();
            } else {
                cachedObj = cachedObjRef.get();
                if ((cachedObj == null) || lastUpdate < path.lastModified()) {
                    cachedObjRef = null; /* help GC */
                    cachedObj = XMLUtil.load(path);
                    cachedObjRef = new WeakReference<Object>(cachedObj);
                    lastUpdate = path.lastModified();
                }
            }            
            return cachedObj;
        }

        /**
         * Write a content to file. Operation performed using XMLUtil tool.
         *
         * @param obj representing content of the file
         * @throws GrmXMLException when operation of writting fails
         * @throws IOException when the operation will be performed on instance representing directory
         */
        public synchronized void write(Object obj) throws GrmXMLException, IOException {

            if (path.isDirectory()) {
                throw new IOException(I18NManager.formatMessage("filecache.error.file_operation",
                        BUNDLE,
                        path.getAbsolutePath()));
            }
            XMLUtil.store(obj, path);
            cachedObjRef = new WeakReference<Object>(obj);
            lastUpdate = path.lastModified();
        }

        /**
         * Delete cached file
         * @throws IOException if the operation fails
         */
        public synchronized void delete() throws IOException {
            if (!path.isDirectory()) {
                if (path.exists() && !path.isDirectory() && !getPath().delete()) {
                    throw new IOException(I18NManager.formatMessage("filecache.error.cannot_del_file",
                            BUNDLE,
                            path.getAbsolutePath()));
                }
            } else {
                for (CachedFile file : list()) {
                    file.delete();
                }
                children = Collections.<CachedFile>emptyList();
                if (path.exists() && !getPath().delete()) {
                    throw new IOException(I18NManager.formatMessage("filecache.error.cannot_del_dir",
                            BUNDLE,
                            path.getAbsolutePath()));
                }
            }
            lastUpdate = 0;
        }

        /**
         * Create new file with a given content in the directory represented by the current instance
         * @param name - string representing the name of new file
         * @param content - Object representing content for new file
         * @return FileCache.CachedFile instance of created file
         * @throws IOException when operation of creating file fails
         * @throws GrmXMLException - when the operation of writing the content to file fails
         */
        public synchronized CachedFile create(String name, Object content) throws IOException, GrmXMLException {
            if (!this.getPath().isDirectory()) {
                throw new IOException(I18NManager.formatMessage("filecache.error.dir_operation",
                        BUNDLE,
                        getPath().getAbsolutePath()));
            }

            int index = name.lastIndexOf(File.separatorChar);
            if (index > 0) {
                String dirname = name.substring(0, index);
                CachedFile dir = createDir(dirname);
                return dir.create(name.substring(index + 1, name.length()), content);
            } else {
                File file = new File(getPath(), name);
                CachedFile cachedFile = getInstance(file);
                cachedFile.write(content);
                return cachedFile;
            }
        }

        /**
         * Create new sub-directory in the directory represented by the current instance
         * @param name - string representing the name of new sub-directory
         * @return FileCache.CachedDirectory instance of created directory
         * @throws IOException when operation of creating directory fails
         */
        public synchronized CachedFile createDir(String name) throws IOException {

            if (!this.getPath().isDirectory()) {
                throw new IOException(I18NManager.formatMessage("filecache.error.dir_operation",
                        BUNDLE,
                        getPath().getAbsolutePath()));
            }

            int index = name.indexOf(File.separatorChar);
            if (index > 0) {
                String dirname = name.substring(0, index);
                File dir = new File(getPath(), dirname);

                if (!dir.exists() && !dir.mkdir()) {
                    throw new IOException(I18NManager.formatMessage("filecache.error.cannot_create_dir",
                            BUNDLE,
                            dir.getAbsolutePath()));
                }
                if (!dir.isDirectory()) {
                    throw new IOException(I18NManager.formatMessage("filecache.error.not_dir",
                            BUNDLE,
                            dir.getAbsolutePath()));
                }
                CachedFile cachedDir = getInstance(dir);
                return cachedDir.createDir(name.substring(index + 1, name.length()));
            } else {
                File dir = new File(getPath(), name);

                if (!dir.exists() && !dir.mkdir()) {
                    throw new IOException(I18NManager.formatMessage("filecache.error.cannot_create_dir",
                            BUNDLE,
                            dir.getAbsolutePath()));
                }
                return getInstance(dir);
            }
        }

        /**
         * Gets the list of all elements in the directory
         * @return list of AbstractCachedFile objects that represent the content of the directory
         * @throws IOException when retrieving information fails
         */
        public synchronized List<CachedFile> list() throws IOException {

            if (!path.isDirectory()) {
                throw new IOException(I18NManager.formatMessage("filecache.error.dir_operation",
                        BUNDLE,
                        getPath().getAbsolutePath()));
            }


            if (children == null || lastUpdate < path.lastModified()) {
                if (!path.exists()) {
                    children = Collections.<CachedFile>emptyList();
                } else {
                    File files[] = getPath().listFiles();
                    if (files == null || files.length == 0) {
                        children = Collections.<CachedFile>emptyList();
                    } else {
                        children = new ArrayList<CachedFile>(files.length);
                        for (File file : files) {
                            children.add(getInstance(file));
                        }
                        children = Collections.unmodifiableList(children);
                    }
                }
            }
            return children;
        }
    }
}

