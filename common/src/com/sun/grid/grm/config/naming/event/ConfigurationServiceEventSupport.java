/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.config.naming.event;

import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.util.EventListenerSupport;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Helper class for CS events.
 */
public class ConfigurationServiceEventSupport {
    
    private final static String BUNDLE = "com.sun.grid.grm.config.naming.event.messages";
    private final static Logger log = Logger.getLogger(ConfigurationServiceEventSupport.class.getName(), BUNDLE);
    private final EventListenerSupport<ConfigurationServiceEventListener> listeners;
    private final AtomicLong eventSequenceNumber = new AtomicLong();
    private static final String csName = BootstrapConstants.CS_ID;
    
    public ConfigurationServiceEventSupport() {
        listeners = EventListenerSupport.<ConfigurationServiceEventListener>newSynchronInstance(ConfigurationServiceEventListener.class);        
    }
    
    /**
     * Register a <code>ConfigurationServiceEventListener</code>.
     *
     * @param eventListener the Configuration Service listener
     */
    public void addConfigurationServiceEventListener(ConfigurationServiceEventListener eventListener) {
        listeners.addListener(eventListener);
    }

    /**
     * Deregister a <code>ConfigurationServiceEventListener</code>.
     *
     * @param eventListener the Configuration Service listener
     */
    public void removeConfigurationServiceEventListener(ConfigurationServiceEventListener eventListener) {
        listeners.removeListener(eventListener);
    }
    
    /**
     * Fire a ConfigurationObjectAddedEvent event.
     * @param name name of the configuration object that was added
     * @param host hostname of the configuration object that was added
     * @param contextPath name of the context that configuration object was added to
     * @param message message for the monitoring
     * @param type the type of the added object
     */
    public void fireConfigurationObjectAdded(String name, String host, String contextPath, String message, String type) {

        ConfigurationObjectAddedEvent evt = new ConfigurationObjectAddedEvent(eventSequenceNumber.incrementAndGet(),
                csName, message, name, host, contextPath, type);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "cs_esupp.fireevent", evt);
        }

        listeners.getProxy().configurationObjectAdded(evt);

    }
    
    /**
     * Fire a ConfigurationObjectRemovedEvent event.
     * @param name name of the configuration object that was removed
     * @param host hostname of the configuration object that was removed
     * @param contextPath name of the context that configuration object was removed from
     * @param message message for the monitoring
     * @param type the type of the removed object
     */
    public void fireConfigurationObjectRemoved(String name, String host, String contextPath, String message, String type) {

        ConfigurationObjectRemovedEvent evt = new ConfigurationObjectRemovedEvent(eventSequenceNumber.incrementAndGet(),
                csName, message, name, host, contextPath, type);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "cs_esupp.fireevent", evt);
        }

        listeners.getProxy().configurationObjectRemoved(evt);

    }
    
    /**
     * Fire a ConfigurationObjectChangedEvent event.
     * @param name name of the configuration object that was changed
     * @param host hostname of the configuration object that was changed
     * @param contextPath name of the context that configuration object was changed in
     * @param message message for the monitoring
     * @param type the type of the changed object
     */
    public void fireConfigurationObjectChanged(String name, String host, String contextPath, String message, String type) {

        ConfigurationObjectChangedEvent evt = new ConfigurationObjectChangedEvent(eventSequenceNumber.incrementAndGet(),
                csName, message, name, host, contextPath, type);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "cs_esupp.fireevent", evt);
        }
   
        listeners.getProxy().configurationObjectChanged(evt);

    }
}
