/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.ComponentAdapter;
import com.sun.grid.grm.ComponentAdapterFactory;
import com.sun.grid.grm.ComponentContainer;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.GrmClassLoaderFactory;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The <code>AbstractDynamicComponentAdapterFactory</code> can be used for 
 * <code>ComponentAdapter</code> that requires extra elements in the classpath.
 * 
 * <p>Subclasses must implement the <code>getClasspath</code> (provides the extended
 *    classpath  for the <code>ComponentAdapter</code> and the <code>getAdapterClassname</code>
 *    (provides the full qualified classname of the <code>ComponentAdapter</code>) methods.</p>
 * 
 * <p>The <code>createAdapter</code> constructs with the provided classpath a new <code>ClassLoader</code>
 *    an instantiates via reflection the <code>ComponentAdapter</code>.</p> 
 * 
 * <p>The <code>ComponentAdapter</code> class must</p>
 * 
 * <ul>
 *    <li>be an instance of A</li>
 *    <li>provide a public constructor in the form:
 * <pre>
 *      public &lt;classname&gt;(AbstractComponent&lt;E,C&gt; component)
 * </pre>
 *    </li>
 * </ul>
 * 
 * @param <T> The type of the owning <code>ComponentContainer</code>
 * @param <A> The type of the <code>ComponentAdapter</code>
 * @param <E> The type of the <code>ExecutorService</code>
 * @param <C> Thy type of the configuration of the component
 */
public abstract class AbstractDynamicComponentAdapterFactory<T extends ComponentContainer<A,E,C>,A extends ComponentAdapter<E,C>,E extends ExecutorService,C> 
        implements ComponentAdapterFactory<T,A,E,C> {

    private final static String BUNDLE = "com.sun.grid.grm.impl.messages";
    
    private final static Logger log = Logger.getLogger(AbstractDynamicComponentAdapterFactory.class.getName());
    
    
    /**
     * Create a component adapter.
     * 
     * @param component the component of the adapter
     * @param config  the configuration of the component
     * @return the new component adapter
     * @throws com.sun.grid.grm.GrmException if the component adapter could not be created
     */
    @SuppressWarnings("unchecked")
    public final A createAdapter(T component, C config) throws GrmException {
        A newDelegate = null;
        
        URL[] classpath = getClasspath(component, config);
        String classname = getAdapterClassname(component, config);
        
        ClassLoader adapterClassLoader = GrmClassLoaderFactory.getInstance(classpath, getClass().getClassLoader());

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "adcaf.load", new Object[]{classname, GrmClassLoaderFactory.toString(adapterClassLoader)});
        }

        Class cls;
        try {
            cls = Class.forName(classname, true, adapterClassLoader);
        } catch (ClassNotFoundException ex) {
            throw new GrmException("adcaf.ex.classNotFound", ex, BUNDLE, classname);
        }

        Constructor cons = null;
        
        for(Constructor tmpCons: cls.getConstructors()) {
            Class<?> []paramTypes = tmpCons.getParameterTypes();
            if (paramTypes.length == 1 && paramTypes[0].isAssignableFrom(component.getClass())) {
                cons = tmpCons;
                break;
            }
        }
        if (cons == null) {
            throw new GrmException("adcaf.ex.noConstructor", BUNDLE, cls.getName());
        }
        
        try {
            newDelegate = (A) cons.newInstance(new Object[]{component});
        } catch (IllegalArgumentException ex) {
            throw new GrmException("adcaf.ex.noConstructor", ex, BUNDLE, cls.getName());
        } catch (IllegalAccessException ex) {
            throw new GrmException("adcaf.ex.noConstructor", ex, BUNDLE, cls.getName());
        } catch (InstantiationException ex) {
            throw new GrmException("adcaf.ex", ex, BUNDLE, ex.getLocalizedMessage());
        } catch (InvocationTargetException ex) {
            throw new GrmException("adcaf.ex", ex.getTargetException(), BUNDLE, ex.getTargetException().getLocalizedMessage());
        }

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "adcaf.loaded", newDelegate.toString());
        }
        return newDelegate;
    }

    /**
     * Has the implementation of the adapter changed since the last call of 
     * createAdapater
     * @param component the owning component
     * @param adapter  the adapter
     * @param config   the configuration of the component
     * @return   <code>true</code> of the adapter implementation has changed
     * @throws com.sun.grid.grm.GrmException if classpath of classname of for the adapter could
     *                                       not be determined
     */
    public final boolean hasImplementationChanged(T component, A adapter, C config) throws GrmException {

        if (adapter == null) {
            return true;
        }
        String classname = getAdapterClassname(component, config);
        if (!adapter.getClass().getName().equals(classname)) {
            return true;
        }
        
        ClassLoader cl = adapter.getClass().getClassLoader();
        if (!(cl instanceof URLClassLoader)) {
            // Can it be that the adapter has not been created with this
            // factory. 
            // => Just to be sure return true, the Container will recreate the adapter
            return true;
        }
        
        URL[] orgClasspath = ((URLClassLoader)cl).getURLs();
        URL[] classpath = getClasspath(component, config);
        if (orgClasspath == null) {
            return true;
        } else if (orgClasspath.length != classpath.length) {
            return true;
        } else {
            boolean ret = false;
            for (int i = 0; i < orgClasspath.length; i++) {
                if (!orgClasspath[i].equals(classpath[i])) {
                    ret = true;
                    break;
                }
            }
            return ret;
        }
    }
    
    /**
     * Get the classpath for the component adapter.
     *
     * @param component The owning component
     * @param config The configuration of the component
     * @throws com.sun.grid.grm.GrmException if the classpath can not be constructed, the
     *          component will go into STOPPED newState
     * @return an array with the URLs of all classpath elements
     */
    protected abstract URL[] getClasspath(T component, C config) throws GrmException;

    /**
     * Get the classname of the component adapter
     *
     * @param component the owning component
     * @param config    the configuration of the component
     * @throws com.sun.grid.grm.GrmException if the classname can not be constructed,
     * @return the classname of the component delegate
     */
    protected abstract String getAdapterClassname(T component, C config) throws GrmException;
    

}
