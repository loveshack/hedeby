/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/**
 * Helper class for creating ExecutorServiceFactory instances
 */
public final class ComponentExecutors {
    
    /**
     * Create a new <code>ExecutorServiceFactory</code> for a <code>CachedThreadPool</code>.
     * @param <T>  the type of the component
     * @param <C>  the type of the configuration of the component
     * @return the <code>ExecutorServiceFactory</code>
     */
    public static <T extends AbstractComponent<ExecutorService,C>,C> ExecutorServiceFactory<T,ExecutorService,C> 
            newCachedThreadPoolExecutorServiceFactory() {
        return new CachedThreadPoolExecutorServiceFactory<T,C>();
    }
    
    /**
     * Create a new <code>ExecutorServiceFactory</code> for a <code>FixedThreadPool</code>.
     * @param <T>  the type of the component
     * @param <C>  the type of the configuration of the component
     * @param nThreads number of fixed threads
     * @return the <code>ExecutorServiceFactory</code>
     */
    public static <T extends AbstractComponent<ExecutorService,C>,C> ExecutorServiceFactory<T,ExecutorService,C> 
            newFixedThreadPoolExecutorServiceFactory(int nThreads) {
        return new FixedThreadPoolExecutorServiceFactory<T,C>(nThreads);
    }
    
    /**
     * Create a new <code>ExecutorServiceFactory</code> for a <code>SingleThreadedExecutorService</code>.
     * @param <T>  the type of the component
     * @param <C>  the type of the configuration of the component
     * @return the <code>ExecutorServiceFactory</code>
     */
    public static <T extends AbstractComponent<ExecutorService,C>,C> ExecutorServiceFactory<T,ExecutorService,C> 
            newSingleThreadedExecutorServiceFactory() {
        return new SingleThreadedExecutorServiceFactory<T,C>();
    }
    
    /**
     * Create a new <code>ExecutorServiceFactory</code> for a <code>ScheduledThreadPoolExecutorService</code>.
     * @param <T>  the type of the component
     * @param <C>  the type of the configuration of the component
     * @param corePoolSize the core pool size of the ExecutorService
     * @return the <code>ExecutorServiceFactory</code>
     */
    public static <T extends AbstractComponent<ScheduledExecutorService,C>,C> ExecutorServiceFactory<T,ScheduledExecutorService,C> 
            newScheduledThreadPoolExecutorServiceFactory(int corePoolSize) {
        return new ScheduledThreadPoolExecutorServiceFactory<T,C>(corePoolSize);
    }
    
    private static class SingleThreadedExecutorServiceFactory<T extends AbstractComponent<ExecutorService, C>, C>
            implements ExecutorServiceFactory<T, ExecutorService, C> {

        public ExecutorService createExecutor(T comp) {
            return Executors.newSingleThreadExecutor(createDefaultThreadFactory(comp.getName()));
        }

        public Class<? extends ExecutorService> getType() {
            return ExecutorService.class;
        }
    }
    
    private static class CachedThreadPoolExecutorServiceFactory<T extends AbstractComponent<ExecutorService, C>, C>
            implements ExecutorServiceFactory<T, ExecutorService, C> {

        public ExecutorService createExecutor(T comp) {
            return Executors.newCachedThreadPool(createDefaultThreadFactory(comp.getName()));
        }

        public Class<? extends ExecutorService> getType() {
            return ExecutorService.class;
        }
    }
    
    private static ThreadFactory createDefaultThreadFactory(final String componentName) {
        return new ThreadFactory() {

            public Thread newThread(Runnable r) {
                return new Thread(r, String.format("component %s", componentName));
            }
        };
    }
    
    private static class FixedThreadPoolExecutorServiceFactory<T extends AbstractComponent<ExecutorService, C>, C>
            implements ExecutorServiceFactory<T, ExecutorService, C> {

        private final int nThreads;
        
        public FixedThreadPoolExecutorServiceFactory(int nThreads) {
            this.nThreads = nThreads;
        }
        public ExecutorService createExecutor(T comp) {
            return Executors.newFixedThreadPool(nThreads,createDefaultThreadFactory(comp.getName()));
        }

        public Class<? extends ExecutorService> getType() {
            return ExecutorService.class;
        }
    }
    
    private static class  ScheduledThreadPoolExecutorServiceFactory<T extends AbstractComponent<ScheduledExecutorService, C>, C>
            implements ExecutorServiceFactory<T, ScheduledExecutorService, C> {

        private final int corePoolSize;
        
        public ScheduledThreadPoolExecutorServiceFactory(int corePoolSize) {
            this.corePoolSize = corePoolSize;
        }
        public ScheduledExecutorService createExecutor(T comp) {
            return Executors.newScheduledThreadPool(corePoolSize);
        }

        public Class<? extends ExecutorService> getType() {
            return ScheduledExecutorService.class;
        }
    }
    
}
