/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.impl;

import com.sun.grid.grm.*;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

/**
 * Abstract base class for component adapters.
 * 
 * @param <E> The type of <code>ExecutorService</code> used by the <code>ComponentAdapter</code>.
 * @param <C> Tye type of the configuration used by the <code>ComponentAdapter</code>.
 */
public abstract class AbstractComponentAdapter<E extends ExecutorService, C> implements ComponentAdapter<E,C> {

    private final AbstractComponentContainer<? extends ComponentAdapter<E,C>,E,C> component;
    
    /**
     * Create a new <code>AbstractComponentAdapter</code>.
     * 
     * @param component the owning component
     */
    protected AbstractComponentAdapter(AbstractComponentContainer<? extends ComponentAdapter<E,C>,E,C> component) {
        this.component = component;
    }

    /**
     * Get the owning component
     * @return the owning component
     */
    protected ComponentContainer<? extends ComponentAdapter<E,C>,E,C> getComponent() {
        return component;
    }
    
    /**
     * Get the <code>ExecutorService</code> of the owning component.
     * @return the <code>ExecutorService</code>
     */
    public E getExecutorService() throws RejectedExecutionException {
        return component.getExecutorService();
    }

    /**
     * Get the <code>ExecutionEnv</code> of the owning component
     * @return the <code>ExecutionEnv</code>
     */
    public ExecutionEnv getExecutionEnv() {
        return component.getExecutionEnv();
    }
    
    /**
     * Get the name of the owning component.
     * @return the name of th owning component
     */
    public String getName() {
        return component.getName();
    }
    
    /**
     * Get the configuration of the owning component.
     * @return the configuration of the owning compoent
     * @throws com.sun.grid.grm.GrmException if the configuration could not be loaded
     */
    public C getConfig() throws GrmException {
        return component.getConfig();
    }
    
    /**
     * Prepare the component start
     * @param config   the new configuration of the component
     * @throws com.sun.grid.grm.GrmException is not used for this method
     */
    public void prepareComponentStartup(C config) throws GrmException {
        // Per default no special action necessary to prepare the startup
    }
    
    /**
     * Prepare the component reload
     * @param config   the new configuration of the component
     * @param forced   if <code>true</code> perform the reload forced
     * @throws com.sun.grid.grm.GrmException is not used for this method
     * @throws com.sun.grid.grm.impl.RestartRequiredException is not used for this method
     */
    public void prepareComponentReload(C config, boolean forced) throws GrmException, RestartRequiredException {
        // Per default no special action necessary to prepare the reload
    }
    
    /**
     * Prepare the component shutdown
     * @param forced   if <code>true</code> perform the reload forced
     * @throws com.sun.grid.grm.GrmException is not used for this method
     */
    public void prepareComponentShutdown(boolean forced) throws GrmException {
        // Per default no special action necessary to prepare the shutdown
    }
    
    /**
     * Get the state of the owning component
     * @return the state of the owning component
     */
    protected ComponentState getState() {
        return this.component.getState();
    }
    
    /**
     * Check that the component is active
     * @throws com.sun.grid.grm.ComponentNotActiveException if the component is not active
     */
    protected void checkActive() throws ComponentNotActiveException {
        if (getState() != ComponentState.STARTED) {
            throw new ComponentNotActiveException(getName());
        }
    }
    
    
}
