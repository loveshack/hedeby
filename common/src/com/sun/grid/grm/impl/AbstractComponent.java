/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.*;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentEventSupport;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.InvalidStateTransistionException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>This class provides the state machine for managing state transitions of 
 *    Hedeby component.</p>
 * 
 * <h1>Component State Model</h1>
 * 
 * <pre>
 *                            +------------------+
 *                            |                  |
 *                            V                  |
 *              +--------&gt; STOPPED -------+      |
 *              |             ^           |      |
 *              |             |           |      |
 *              |         on error      start   on error
 *              |             |           |      |
 *              |             |           V      |
 *    +-----&gt;STOPPING      RELOADING   STARTING -+
 *    |       |   ^             ^           |
 *    |       |   |             |           |
 *  stop(forced)  stop          reload      start
 *    +-------+   |             |           |
 *                +--------  STARTED &lt;------+
 * </pre>
 * 
 * <p>In the above picture the words in capital letters are <code>ComponentState</code>s.
 *    The words in lowercase letters are either method calls (stop, reload, start) or
 *    results of method calls (e.g. on error).<br>
 *    The state model has intermediate state (STOPPING, RELOADING and STARTING), they
 *    have in common that the follow up non intermediate state is reached with out user
 *    intervention.<br>
 *    The non intermediate states STOPPPED and STARTED are reached as a result of a state
 *    transition. The state transition is triggered via an extern method calls (start, stop,
 *    reload).<br>
 *    The initial state of a <code>AbstractComponent</code> is the STOPPED state.
 * </p>
 * <p>The component state model is implemented according to state design pattern. The
 *    component states are represented by instances of <code>StateHandler</code>. Each
 *    <code>StateHandler</code> implements the methods <code>start</code>, <code>stop</code>
 *    and <code>reload</code>.If a component does not allow a specific state transition 
 *    the implementing <code>StateHandler</code> throws an <code>InvalidStateTransitionException</code>.
 *    If a state transition is allowed the <code>StateHandler</code> triggers the transition.
 * </p>
 * 
 * <h1>ExecutorService</h1>
 * 
 * <p>The <code>AbstractComponent</code> creates at component startup an <code>ExecutorService</code>. 
 * This <code>ExecutorService</code> is used to perform the asynchronous parts of the state transitions.
 * Additionally subclasses can use it to submit also asynchronous tasks. At component shutdown
 * the <code>ExecutorService</code> is also shutdown. With a forced stop of the component the
 * <code>ExecutorService</code> is stopped with <code>ExecutorService#shutdownAll</code>.<br>
 * Subclasses can access the <code>ExecutorService</code> via the method <code>getExecutorService</code>,
 * however they get only a dynamic proxy. This proxy forbids the shutdown of the <code>ExecutorService</code>.</p>
 * 
 * <H1>Thread safeness</H1>
 * 
 * <p>Any state transition has a synchronous part (preparation) and an asynchronous part. The complete synchronous 
 *    part is protected by a global lock. The asynchronous part is running without protection.</p>
 * 
 * <p>Before triggering  a state transition the <code>AbstractComponent</code> sets a global lock. While preparing
 *    the state transitions (<code>prepareComponentStartup</code>, <code>prepareComponentReload</code>, 
 *    <code>prepareComponentShutdown</code>) the global lock is kept locked until the intermediate state
 *    of the state transition is reached (STARTING, STOPPING, RELOADING).</br>
 *    The follow up state transition from the ING state to the final state is done unattended. The
 *    active state handler must ensure that no other state transition is started meanwhile. The corresponding
 *    method must throw an {@link InvalidStateTransistionException}.
 * </p>
 * 
 * @param <E> The type of the ExecutorService
 * @param <C> The type of configuration
 */
public abstract class AbstractComponent<E extends ExecutorService, C> implements GrmComponent {

    /**
     * Bundle which contains i18n strings
     */
    private static final String BUNDLE = "com.sun.grid.grm.impl.messages";
    
    /** the logger for this class */
    private final static Logger log = Logger.getLogger(AbstractComponent.class.getName(), BUNDLE);
    
    /**
     * The state handler handles all possible state transitions for the current
     * state
     */
    private final AtomicReference<StateHandler> stateHandler;
    
    /**
     * The ExecutorService which is executing the asynchronous action.
     */
    private final ExecutorServiceProxy<E, C> executor;
    
    /** The execution env */
    protected final ExecutionEnv env;
    
    /** support for component events */
    protected final ComponentEventSupport cmpEventSupport;
    /**
     * stores the name of the component.
     */
    private String name;
    /**
     * host on which the component runs. Used for events.
     */
    private final Hostname host;
    /**
     *  Guards the stopThread and executor
     */
    private final Lock lock = new ReentrantLock();
    /**
     * This thread executes the stopping of the component
     */
    private StoppingThread stopThread;
    /**
     * Reference to the current configuration
     * of the component.
     */
    private final AtomicReference<C> config = new AtomicReference<C>();

    /** 
     * Creates a new instance of AbstractComponent.
     *
     * @param env   the execution of of the Hedeby system
     * @param name  the name for the component
     * @param executorServiceFactory Factory for the ExecutorService
     */
    protected AbstractComponent(ExecutionEnv env, String name, ExecutorServiceFactory<? extends AbstractComponent<E,C>,E, C> executorServiceFactory) {
        this.env = env;
        this.name = name;
        this.host = Hostname.getLocalHost();
        this.cmpEventSupport = createEventSupport(name, host);
        this.stateHandler = new AtomicReference<StateHandler>(stoppedStateHandler);
        this.executor = new ExecutorServiceProxy<E, C>(executorServiceFactory);
    }

    /**
     * Get the configuration of the component.
     *
     * @throws com.sun.grid.grm.GrmException if the configuration was not found
     * @return the configuration
     */
    public C getConfig() throws GrmException {
        log.entering(AbstractComponent.class.getName(), "getConfig");
        if (config.get() == null) {
            config.set(loadConfig());
        }
        C ret = config.get();
        log.exiting(AbstractComponent.class.getName(), "getConfig", ret);
        return ret;
    }
    
    /**
     * Allows the subclass to provide a different implementation of the 
     * ComponentEventSupport
     * 
     * @param componentName the name of the component
     * @param hostname   the hostname where the component is living
     * @return the ComponentEventSupport
     */
    protected ComponentEventSupport createEventSupport(String componentName, Hostname hostname) {
        return ComponentEventSupport.newInstance(componentName, hostname);
    }
    
    /**
     * Get the ComponentEventSupport element for this component
     * @return the ComponentEventSupport
     */
    protected ComponentEventSupport getComponentEventSupport() {
        return cmpEventSupport;
    }

    /**
     * Load the configuration of the component
     * 
     * <p>This method load the configuration of a component from CS. Junit tests
     *    can override this method to provide an alternative configuration loading
     *    mechanism.</p>
     * 
     * @return the configuration
     * @throws com.sun.grid.grm.GrmException if the configuration could not be loaded
     */
    protected C loadConfig() throws GrmException {
        log.entering(AbstractComponent.class.getName(), "loadConfig");
        
        GetConfigurationCommand<C> cmd = new GetConfigurationCommand<C>(name);
        Result<C> r = env.getCommandService().<C>execute(cmd);
        
        log.exiting(AbstractComponent.class.getName(), "loadConfig", r.getReturnValue());
        return r.getReturnValue();
    }

    /**
     * Returns the name of a component.
     *
     * @return component name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the name of host on which runs the component.
     *
     * @return host name
     */
    public Hostname getHostname() throws GrmRemoteException {
        return host;
    }

    /**
     * Return the execution environment for this component.
     *
     * @return the execution environment
     */
    public ExecutionEnv getExecutionEnv() {
        return env;
    }

    /**
     * Returns the current component state.
     * @return Current state.
     */
    public ComponentState getState() {
        log.entering(AbstractComponent.class.getName(), "getState");
        ComponentState ret = stateHandler.get().getState();
        log.exiting(AbstractComponent.class.getName(), "getState", ret);
        return ret;
    }

    /**
     * Register a <code>ComponentEventListener</code>.
     *
     * @param listener  the listener
     */
    public void addComponentEventListener(ComponentEventListener listener) {
        log.entering(AbstractComponent.class.getName(), "addComponentEventListener", listener);
        
        cmpEventSupport.addComponentEventListener(listener);
        
        log.exiting(AbstractComponent.class.getName(), "addComponentEventListener");
    }

    /**
     * Deregister a <code>ComponentEventListener</code>.
     *
     * @param listener  the listener
     */
    public void removeComponentEventListener(ComponentEventListener listener) {
        log.entering(AbstractComponent.class.getName(), "removeComponentEventListener", listener);
        
        cmpEventSupport.removeComponentEventListener(listener);
        
        log.exiting(AbstractComponent.class.getName(), "removeComponentEventListener");
    }

    /**
     * Start the component.
     * 
     * @throws com.sun.grid.grm.GrmException if the startup of the component failed
     */
    public final void start() throws GrmException {
        log.entering(AbstractComponent.class.getName(), "start");
        
        lock();
        try {
            stateHandler.get().start();
        } finally {
            unlock();
        }
        
        log.exiting(AbstractComponent.class.getName(), "start");
    }

    /**
     * Stop the component.
     * 
     * @param forced if <code>true</code> the component will be stopped in forced mode.
     *               This means that all active action running in the <code>ExecutorService</code>
     *               will be interrupted.
     * 
     * @throws com.sun.grid.grm.GrmException if the component could not be stopped
     */
    public final void stop(boolean forced) throws GrmException {
        log.entering(AbstractComponent.class.getName(), "stop", forced);

        lock();
        try {
            stateHandler.get().stop(forced);
        } finally {
            unlock();
        }
        
        log.exiting(AbstractComponent.class.getName(), "stop");
    }

    /**
     * Reload the configuration and apply the chances to the component.
     * @param forced  has currently no impact.
     * 
     * @throws com.sun.grid.grm.GrmException if the configuration could not be reload
     */
    public final void reload(boolean forced) throws GrmException {
        log.entering(AbstractComponent.class.getName(), "reload", forced);
        
        lock();
        try {
            stateHandler.get().reload(forced);
        } finally {
            unlock();
        }
        
        log.exiting(AbstractComponent.class.getName(), "reload");
    }
    
    /**
     * Lock the component.
     * 
     * <p>This method sets the global lock for the component. The call must
     *    call <code>unlock</code> to release the lock.</p>
     */
    protected final void lock() {
        lock.lock();
    }
    
    /**
     * Unlock the component
     */
    protected final void unlock() {
        lock.unlock();
    }

    /**
     * Prepare the component startup. 
     * 
     * <p>This method is called before the state transition from STOPPED to
     *    STARTING is performed. Subclasses can override this method to perform additional
     *    checks for proving that startup is possible.</p>
     * 
     * @param config the configuration of the component
     * @throws com.sun.grid.grm.GrmException if the component startup is not possible
     *             If this exception is thrown the state transition from STOPPED to
     *             STARTING is not performed
     */
    protected void prepareComponentStartup(C config) throws GrmException {
        // The default implemenation is empty
    }

    /**
     * Perform the concrete actions for starting up the component.
     * 
     * <p>Subclasses implements in this method the necessary code for starting
     *    the component.</p>
     * 
     * @param config  the configuration of the component
     * @throws com.sun.grid.grm.GrmException The component could not be started. If
     *             this exception is thrown the component will go into STOPPED state.
     */
    protected abstract void startComponent(C config) throws GrmException;


    /**
     * Prepare the shutdown of the component.
     * 
     * <p>This method is called before the state transition from STARTED to STOPPING
     *    is performed. Subclasses can override this method to perform additional actions
     *    before the component shutdown is triggered.</p>
     * 
     * @param forced if <code>true</code> the component will be stopped in forced mode.
     * @throws com.sun.grid.grm.GrmException if the preparations for the component shutdown failed
     *                          If this method is called, the state transition from STARTED to STOPPING
     *                          is not performed
     */
    protected void prepareComponentShutdown(boolean forced) throws GrmException {
        // The default implementation is empty
    }
    
    /**
     * Perform the concrete actions for stopping the component.
     * 
     * <p>Subclasses implements in this method the necessary code for starting
     *    the component.</p>
     * 
     * @param forced  If <code>true</code> the component is stopped in forced mode.
     *                The implementing subclass should minimize code for shutdown
     *                the component in forced mode
     * @throws com.sun.grid.grm.GrmException The component could not be stopped
     */
    protected abstract void stopComponent(boolean forced) throws GrmException;

    /**
     * Prepare the reload of the component.
     * 
     * <p>This method is called before performing the state transition from STARTED to
     *    RELOADING. Subclasses can override this method to perform additinal action
     *    before the reload of a component is triggered.</p>
     * 
     * @param config the new configuration of the component
     * @param forced if <code>true</code> force the reload
     * @throws com.sun.grid.grm.GrmException if the reload can not be performed, the state transition
     *                                    from STARTED to RELOADING is not performed
     * @throws RestartRequiredException if the configuration can only become active with a
     *                                  restart of the component
     */
    protected void prepareComponentReload(C config, boolean forced) throws GrmException, RestartRequiredException {
        // The default implementation is empty
    }

    /**
     * Perform the concrete actions to apply the new configuration to the 
     * component.
     * 
     * @param config  the new configuration
     * @param forced  forced mode, has currently no impact
     * @throws com.sun.grid.grm.GrmException The configuration could not be applied to 
     *                                       the component
     */
    protected abstract void reloadComponent(C config, boolean forced) throws GrmException;

    /**
     * Get the ExecutorService.
     * 
     * <p><b>Attention:</b> The returned instance can only be used for submitting new
     * tasks or for monitoring the executor. Any call of
     * <code>shutdown</code> or <code>shutdownNow</code> will end up in a 
     * <code>UnsupportedOperationException</code>.</p>
     * 
     * <p>The life cycle of the return executor is bound to the life cycle of the
     *    component. If the component is stopped the executor will shutdown. If
     *    the component is stopped forced, the executor will be stopped with
     *    <code>shutdownNow</code> (all running actions will be interrupted).
     * 
     * @return the <code>ExecutorService</code> of the component
     */
    public final E getExecutorService() {
        return executor.getProxy();
    }

    /**
     * A <code>StateHandler</code> handles all transitions for a specific state.
     */
    private interface StateHandler {

        /**
         * Get the component state represented by this <code>StateHandler</code>.
         * @return the component state
         */
        ComponentState getState();

        /**
         * Trigger state transition from the current state to the STARTED state.
         * 
         * @throws com.sun.grid.grm.GrmException if the state transition failed due to configuration problems
         * @throws com.sun.grid.grm.util.InvalidStateTransistionException if the state transition from
         *             the current state to STARTED is not allowed
         */
        void start() throws InvalidStateTransistionException, GrmException;

        /**
         * Trigger state transition from the current state to the STOPPED state.
         * 
         * @throws com.sun.grid.grm.GrmException if the state transition failed due to configuration problems
         * @throws com.sun.grid.grm.util.InvalidStateTransistionException if the state transition from
         *             the current state to STOPPED is not allowed
         */
        void stop(boolean forced) throws GrmException, InvalidStateTransistionException;

        /**
         * Trigger state transition from the current state to the RELOADING state.
         * 
         * @param forced has currently no impact
         * @throws com.sun.grid.grm.GrmException if the reload failed
         * @throws com.sun.grid.grm.util.InvalidStateTransistionException if the state transition from
         *             the current state to RELOADING is not allowed
         */
        void reload(boolean forced) throws GrmException;
    }

    /**
     * Intermediate state handler performs the state transition
     * without external trigger.
     * 
     * <p>All -ING</p> states are represented by an <code>IntermediateStateHandler</code>.
     */
    private interface IntermediateStateHandler extends StateHandler {

        /**
         * Prepare the state transition. This method implements the synchronous
         * part of a state transition.
         * 
         * @throws com.sun.grid.grm.GrmException if the state transition can
         *           not be performed
         */
        void prepare() throws GrmException, RestartRequiredException;

        /**
         * Perform the asynchronous part of the state transition
         * @return the follow state
         */
        StateHandler performTransition();
    }

    /**
     * There exists also state transitions which must be executed completely
     * synchronous. Such state transitions implement <code>SynchronIntermediateStateHandler</code>. 
     */
    private interface SynchronIntermediateStateHandler extends IntermediateStateHandler {
    }

    /**
     * notify all registered component listeners that the component state
     * has changed.
     * 
     * @param oldState  state handler representing the old component state
     * @param newState  state handler representing the new component state
     */
    private void fireStateChanged(StateHandler oldState, StateHandler newState) {
        if (!oldState.getState().equals(newState.getState())) {
            cmpEventSupport.fireComponentStateChangedEvent(newState.getState());
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "ac.stateChanged", new Object[]{getName(), oldState.getState(), newState.getState()});
            }
        }
    }

    /**
     * Performs the asynchronous part of a start transition.
     * @param newState the handler of the state transition
     */
    private void performTransition(final IntermediateStateHandler newState) {
        log.entering(AbstractComponent.class.getName(), "performTransition", newState);
        Runnable r = new Runnable() {

            public void run() {
                StateHandler followState = newState.performTransition();

                stateHandler.set(followState);

                fireStateChanged(newState, followState);

                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "ac.trans.finished",
                            new Object[]{getName(), newState, followState});
                }
            }
        };
        
        if (newState instanceof SynchronIntermediateStateHandler) {
            r.run();
        } else {
            executor.submit(r);
        }
        log.exiting(AbstractComponent.class.getName(), "performTransition");
    }
    
    /**
     * This method perform the synchronous part of a state transition and triggers
     * the asynchrous part.
     * @param oldState  the handler of the old component state
     * @param newState  the handler of the new component state
     * @throws com.sun.grid.grm.util.InvalidStateTransistionException if the state transition is not valid
     * @throws com.sun.grid.grm.GrmException if the state transition is not possible
     */
    private void changeState(final StateHandler oldState, final IntermediateStateHandler newState)
            throws InvalidStateTransistionException, GrmException {
        if(log.isLoggable(Level.FINER)) {
            log.entering(AbstractComponent.class.getName(), "changeState", new Object [] { oldState, newState });
        }
        
        try {
            // The prepare is always executed synchronously
            newState.prepare();
            stateHandler.set(newState);            
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "ac.changeState", new Object[]{getName(), oldState, newState});
            }
            fireStateChanged(oldState, newState);

            performTransition(newState);

        } catch (RestartRequiredException ex) {
            // It can happen that the reload of a component is not possible
            // (e.g the class path of the ComponentAdapter has changed)
            // The <code>RestartRequiredException</code> indicates exactly this
            // situation. 
            // => Perform an automatic restart of the component
            log.log(Level.WARNING, "ac.startRequired", getName());
            // => We can use the StoppingStateHandler to perform the state transition
            //    RELOADING -> STOPPED -> STARTING -> STARTED
            IntermediateStateHandler restartStateHandler =
                    new StoppingStateHandler(newState.getState(), false).restart(true);
            
            try {
                restartStateHandler.prepare();
                stateHandler.set(restartStateHandler);
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "ac.changeState", new Object[]{getName(), oldState, newState});
                }
                fireStateChanged(oldState, newState);
                performTransition(restartStateHandler);
            } catch (RestartRequiredException ex1) {
                throw new IllegalStateException("RestartStateHandler must not throw RestartRequiredException", ex1);
            }
        }

        log.exiting(AbstractComponent.class.getName(), "changeState");
    }
    
    /**
     * StateHandler for the component state STOPPED
     */
    private final StateHandler stoppedStateHandler = new StateHandler() {

        public ComponentState getState() {
            return ComponentState.STOPPED;
        }

        public void start() throws GrmException {
            changeState(this, startingStateHandler);
        }

        public void stop(boolean forced) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException(getState(), ComponentState.STOPPING, "ac.ex.stop.stop", BUNDLE);
        }

        public void reload(boolean forced) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException(getState(), ComponentState.RELOADING
                    , "ac.ex.stop.reload", BUNDLE);
        }

        @Override
        public String toString() {
            return String.format("stoppedStateHandler[%s]", getState());
        }
    };
    
    /**
     * StateHandler for the component state STARTING
     */
    private final IntermediateStateHandler startingStateHandler = new IntermediateStateHandler() {

        public ComponentState getState() {
            return ComponentState.STARTING;
        }

        public void start() throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException(getState(), ComponentState.STARTING, "ac.ex.starting.start", BUNDLE);
        }

        public void stop(boolean forced) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException(getState(), ComponentState.STOPPING, "ac.ex.starting.stop", BUNDLE);
        }

        public void reload(boolean forced) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException(getState(), ComponentState.RELOADING, "ac.ex.starting.reload", BUNDLE);
        }

        public void prepare() throws GrmException {

            // Reset the configuration, the next getConfig call will load it
            config.set(null);
            
            AbstractComponent.this.prepareComponentStartup(getConfig());

            if (stopThread != null) {
                if (stopThread.isAlive()) {
                    stopThread.interrupt();
                }
                stopThread = null;
            }
            executor.createExecutor(AbstractComponent.this);
        }

        public StateHandler performTransition() {
            try {
                startComponent(config.get());
                return startedStateHandler;
            } catch (GrmException ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("ac.ex.start", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
                // The executor has been already started 
                // => We must trigger the stop procedure
                return new StoppingStateHandler(ComponentState.STARTING, false).performTransition();
            } catch (Throwable ex) {
                // We must catch Throwable to get all this nasty NoClassDefFoundError or IllegalAccessError
                log.log(Level.WARNING, I18NManager.formatMessage("ac.ex.start.int", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
                // The executor has been already started 
                // => We must trigger the stop procedure
                return new StoppingStateHandler(ComponentState.STARTING, false).performTransition();
            }
        }
        
        @Override
        public String toString() {
            return String.format("startingStateHandler[%s]", getState());
        }
        
    };
    
    /**
     * StateHandler for the component state STARTED
     */
    private final StateHandler startedStateHandler = new StateHandler() {

        public ComponentState getState() {
            return ComponentState.STARTED;
        }

        public void start() throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException(getState(), ComponentState.STARTING, "ac.ex.started.start", BUNDLE);
        }

        public void stop(boolean forced) throws GrmException {
            changeState(this, new StoppingStateHandler(forced));
        }

        public void reload(boolean forced) throws GrmException {
            changeState(this, new ReloadingStateHandler(forced));
        }
        
        @Override
        public String toString() {
            return String.format("startedStateHandler[%s]", getState());
        }
        
    };

    /**
     * StateHandler for the component state STOPPING
     * 
     * <p>This StateHandler can also be used to restart the component completely (set
     *    the restart property to true. The component will perform the following state
     *    transitions:</p>
     * <pre>
     *    &lt;orgState&gt; -> STOPPED -> STARTING -> STARTED (or STOPPED if an error occured)
     * </pre>
     * <p><code>&lt;orgState&gt;</code> is set via the constructor of this class.</p>
     * 
     * <p>The <code>StoppingStateHandler</code> is an <code>SynchronIntermediateStateHandler</code>,
     *    this means the <code>performTransition</code> is called directly and not asynchron via
     *    the <code>ExecutorService</code>.<br>
     *    The reason for being a <code>SynchronIntermediateStateHandler</code> is that the
     *    <code>performTransition</code> triggers the shutdown of the <code>ExecutorService</code>.
     *    It can not be done if the performTransition method is invoked within a thread of the
     *    <code>ExecutorService</code>.
     * </p>
     * <p>The <code>performTransition</code> method returns as followup <code>StateHandler</code> a
     *    reference to <code>this</code>. This means after <code>performTransition</code> returns
     *    the component stays in STOPPING state. The state transtion from STOPPING to state is performed in
     *    the <code>StoppingThread</code>.
     * </p>
     * 
     * <h2>Forced stop of component is in STOPPING state</h2>
     * 
     * <p>The <code>stop</code> method if this <code>StateHandler</code> allows that a forced
     *    stop of a component is done if the component is already in STOPPING state. This
     *    can be helpful if a task running in the <code>ExecutorService</code> does not stop
     *    working with a normal shutdown.
     * </p>
     */
    private class StoppingStateHandler implements SynchronIntermediateStateHandler {

        private final AtomicBoolean forced = new AtomicBoolean();
        private final ComponentState state;
        private final AtomicBoolean restart = new AtomicBoolean();

        public StoppingStateHandler(boolean forced) {
            this(ComponentState.STOPPING, forced);
        }

        public StoppingStateHandler(ComponentState state, boolean forced) {
            this.forced.set(forced);
            this.state = state;
        }

        /**
         * Set the restart property of the <code>StoppingStateHandler</code>.
         * 
         * @param restart  the value of the restart property
         * @return reference to this
         */
        public StoppingStateHandler restart(boolean restart) {
            this.restart.set(restart);
            return this;
        }

        public ComponentState getState() {
            return state;
        }

        public void start() throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException(getState(), ComponentState.STARTING, "ac.ex.stopping.start", BUNDLE);
        }

        public void stop(boolean forced) throws InvalidStateTransistionException {
            if (forced) {
                if (this.forced.get()) {
                    throw new InvalidStateTransistionException(getState(), ComponentState.STOPPING, "ac.ex.stopping.stop.forced", BUNDLE);
                } else {
                    this.forced.set(true);
                    performTransition();
                }
            } else {
                throw new InvalidStateTransistionException(getState(), ComponentState.STOPPING, "ac.ex.stopping.stop", BUNDLE);
            }
        }

        public void reload(boolean forced) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException(getState(), ComponentState.RELOADING, "ac.ex.stopping.reload", BUNDLE);
        }

        public void prepare() throws GrmException {
            prepareComponentShutdown(forced.get());
        }

        public StateHandler performTransition() {
            
            // We must not lock the lock, because StoppingStateHandler is a
            // SynchronIntermediateStateHandler
            // This means that the performTransition method is directly called
            // for AbstractComponent#performTransition
            // => the lock is already set
            StoppingThread localStoppingThread = null;
            if (stopThread == null) {
                localStoppingThread = new StoppingThread(getName(), this);
                stopThread = localStoppingThread;
                stopThread.restart(restart.get());
            }
            if (forced.get()) {
                stopThread.forceStop();
            }
            if (localStoppingThread != null) {
                stopThread.start();
            }
            // We stay in stopping state
            // The StoppingThread will set the state transition from STOPPING
            // to STOPPED
            // This allows the user a "stop(forced)" after a stop() call
            return this;
        }
        
        @Override
        public String toString() {
            return String.format("StoppingStateHandler[%s]", getState());
        }
        
    }
    
    /**
     * This <code>StateHandler</code> handles the RELOADING state.
     */
    private class ReloadingStateHandler implements IntermediateStateHandler {

        private final boolean forced;

        public ReloadingStateHandler(boolean forced) {
            this.forced = forced;
        }

        public ComponentState getState() {
            return ComponentState.RELOADING;
        }

        public void start() throws GrmException {
            throw new InvalidStateTransistionException(getState(), ComponentState.STARTING, "ac.ex.reloading.start", BUNDLE);
        }

        public void stop(boolean forced) throws GrmException {
            throw new InvalidStateTransistionException(getState(), ComponentState.STOPPED, "ac.ex.reloading.stop", BUNDLE);
        }

        public void reload(boolean forced) throws GrmException {
            throw new InvalidStateTransistionException(getState(), ComponentState.RELOADING, "ac.ex.reloading.reload", BUNDLE);
        }

        public void prepare() throws GrmException, RestartRequiredException {
            // Reset the config, the next call of getConfig will reload it
            config.set(null);
            AbstractComponent.this.prepareComponentReload(getConfig(), forced);
        }

        public StateHandler performTransition() {
            try {
                config.set(null);
                reloadComponent(getConfig(), forced);
                return startedStateHandler;
            } catch (GrmException ex) {
                log.log(Level.SEVERE, I18NManager.formatMessage("ac.reload.ex", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
                // Reload failed, stop the component
                return new StoppingStateHandler(ComponentState.RELOADING, false).performTransition();
            } catch (Throwable ex) {
                // We must catch Throwable to get all this nasty NoClassDefFoundError or IllegalAccessError
                log.log(Level.SEVERE, I18NManager.formatMessage("ac.reload.ex.int", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
                // Reload failed, stop the component
                return new StoppingStateHandler(ComponentState.RELOADING, false).performTransition();
            }
        }
        
        @Override
        public String toString() {
            return String.format("ReloadingStateHandler[%s]", getState());
        }
    }

    /**
     * The <code>StoppingThread</code> is responsible for stopping the component.
     * It is started from the <code>StoppingStateHandler</code>.
     * 
     * <p>The run method will wait until the <code>ExecutorService</code> is
     *    terminated.</p>
     * 
     * <p>If the <code>restart</code> property of the <code>StoppingThread</code> is
     *    set after successful shutdown the component will be restarted.</p>
     * 
     * <p>If a normal shutdown is in progress an upgrade to a forced shutdown is possible.
     *    For this purpose the <code>forceStop</code> method must be called.</p>
     */
    private class StoppingThread extends Thread {

        private final AtomicBoolean forced = new AtomicBoolean();
        private final StoppingStateHandler orgStateHandler;
        private final AtomicBoolean restart = new AtomicBoolean();

        private StoppingThread(String name, StoppingStateHandler orgStateHandler) {
            super(name + "[Stopping]");
            this.orgStateHandler = orgStateHandler;
        }

        public void forceStop() {
            this.forced.set(true);
        }

        public StoppingThread restart(boolean restart) {
            this.restart.set(restart);
            return this;
        }
        
        
        private class StopComponentRunnable implements Runnable {

            private final Lock lock = new ReentrantLock();
            private final Condition startupCondition = lock.newCondition();
            private boolean started;
            
            /**
             * This method returns true once the stop component action is active
             * @param timeout the timeout for waiting for the startup of the action
             * @return <code>true</code> if the stop component action is active
             * @throws java.lang.InterruptedException if the calling thread has been interrupted
             */
            public  boolean waitForStartup(long timeout) throws InterruptedException {
                lock.lock();
                try {
                    if(!started) {
                        startupCondition.await(timeout, TimeUnit.MILLISECONDS);
                    }
                    return started;
                } finally {
                    lock.unlock();
                }
            }
            
            public void run() {
                log.log(Level.FINE, "ac.stoppingThread.stopComponent", AbstractComponent.this.getName());
                lock.lock();
                try {
                    started = true;
                    startupCondition.signalAll();
                } finally {
                    lock.unlock();
                }
                log.log(Level.FINE, "ac.stoppingThread.stopComponent", AbstractComponent.this.getName());
                try {
                    stopComponent(false);
                } catch (GrmException ex) {
                    log.log(Level.SEVERE, I18NManager.formatMessage("ac.shutdown.ex", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
                } catch (Throwable ex) {
                    // We must catch Throwable to get all this nasty NoClassDefFoundError or IllegalAccessError
                    log.log(Level.SEVERE, I18NManager.formatMessage("ac.shutdown.ex.int", BUNDLE, getName(),ex.getLocalizedMessage()), ex);
                } 
            }
        }
        
        @Override
        public void run() {
            try {
                
                log.log(Level.FINE, "ac.stoppingThread.active", AbstractComponent.this.getName());
                
                boolean shutdownCalled = false;
                boolean shutdownNowCalled = false;
                boolean forcedStoppedComponentCalled = false;

                long lastLogTimestamp = 0;

                if (!forced.get()) {
                    try {
                        // The stopComponent action must be called from a Runnable in the ExecutorService
                        // Otherwise no forced stop is not possible if stopComponent blocks
                        StopComponentRunnable r = new StopComponentRunnable();
                        executor.submit(r);
                        log.log(Level.FINER, "ac.stoppingThread.wsca", AbstractComponent.this.getName());
                        // Wait until the StopComponentRunnable is active
                        while (true) {
                            if (r.waitForStartup(100)) {
                                log.log(Level.FINER, "ac.stoppingThread.scas", AbstractComponent.this.getName());
                                break;
                            }
                            if (forced.get()) {
                                // If meanwhile a forced shutdown is triggered the stop component action 
                                // will not start
                                // => give up
                                log.log(Level.FINER, "ac.stoppingThread.scaa", AbstractComponent.this.getName());
                                break;
                            }
                        }
                    } catch(Exception t) {
                        log.log(Level.WARNING, 
                                I18NManager.formatMessage("ac.stoppingThread.ex.stopComponent", BUNDLE,  AbstractComponent.this.getName(), t.getLocalizedMessage()),
                                t);
                    }
                }                
                
                int tryCounter = 0;
                while (!executor.isTerminated()) {
                    if (forced.get()) {
                        if (forcedStoppedComponentCalled == false) {
                            log.log(Level.FINE, "ac.stoppingThread.stopComponentForced", AbstractComponent.this.getName());
                            stopComponent(true);
                            forcedStoppedComponentCalled = true;
                        }
                        // We use here the hammering method
                        // If the ExecutorService does not stop it can be that
                        // some thread is hanging in an while(!Thread.isInterrupted) loop
                        log.log(Level.FINE, "ac.shutdown.forced", AbstractComponent.this.getName());
                        executor.shutdownNow();
                        shutdownNowCalled = true;
                    } else {
                        if (shutdownCalled == false) {
                            log.log(Level.FINE, "ac.shutdown", AbstractComponent.this.getName());
                            executor.shutdown();
                            shutdownCalled = true;
                        }
                    }
                    
                    // Write every ten seconds a log message
                    if(lastLogTimestamp - System.currentTimeMillis() > 10000L) {
                        Level level = Level.FINE;
                        if(tryCounter > 5000) {
                            level = Level.WARNING;
                        }  else if (tryCounter > 10000) {
                            level = Level.SEVERE;
                        }
                        log.log(level,"ac.waitForShutdown", getName());
                        lastLogTimestamp = System.currentTimeMillis();
                    }
                    
                    // Wait only 100ms for the shutdown of the component because 
                    // a forced stop call should become active ASAP.
                    executor.awaitTermination(100, TimeUnit.MILLISECONDS);
                    tryCounter++;
                }
            } catch (InterruptedException ex) {
                log.log(Level.FINE, "ac.shutdown.int", getName());
            } catch (GrmException ex) {
                log.log(Level.SEVERE, I18NManager.formatMessage("ac.shutdown.ex", BUNDLE, AbstractComponent.this.getName(),ex.getLocalizedMessage()), ex);
            } catch (Throwable ex) {
                // We must catch Throwable to get all this nasty NoClassDefFoundError or IllegalAccessError
                log.log(Level.SEVERE, I18NManager.formatMessage("ac.shutdown.ex.int", BUNDLE, AbstractComponent.this.getName(),ex.getLocalizedMessage()), ex);
            } finally {
                lock();
                try {
                    stateHandler.set(stoppedStateHandler);
                    fireStateChanged(orgStateHandler, stoppedStateHandler);
                    if (restart.get()) {
                        try {
                            stoppedStateHandler.start();
                        } catch (InvalidStateTransistionException ex) {
                            // Should not happen since state transition from stopped to starting is allowed
                            // This is an illegal state, however we can not thow an IllegalStateException here
                            log.log(Level.SEVERE, "ac.stop.strange", AbstractComponent.this.getName());
                        } catch (GrmException ex) {
                            log.log(Level.WARNING, I18NManager.formatMessage("ac.stop.restart.failed", BUNDLE, AbstractComponent.this.getName(), ex.getLocalizedMessage()), ex);
                        }
                    }
                } finally {
                    unlock();
                }
                log.log(Level.FINE, "ac.shutdown.finished", getName());
            }
        }
    }

    /**
     * The <code>ExecutorServiceProxy</code> provides a dynamic proxy to the <code>ExecutorService</code>
     * of the component. It is further used to synchronize the access to the <code>ExecutorService</code>
     * for internal usage.
     * @param <E>  the type of <code>ExecutorService</code>
     * @param <C>  the type of configuration of the component
     */
    static class ExecutorServiceProxy<E extends ExecutorService, C> implements InvocationHandler {

        private final ExecutorServiceFactory<AbstractComponent<E,C>,E,C> factory;
        private final AtomicReference<E> executor;
        private final E proxy;
        
        /**
         * Create a new instance of the ExecutorServiceProxy
         * @param factory the factory for the ExecutorService
         */
        @SuppressWarnings("unchecked")
        private ExecutorServiceProxy(ExecutorServiceFactory<? extends AbstractComponent<E,C>,E, C> factory) {
            this.executor = new AtomicReference<E>();
            this.factory = (ExecutorServiceFactory<AbstractComponent<E, C>, E, C>)factory;
            proxy = (E) Proxy.newProxyInstance(factory.getClass().getClassLoader(),
                    new Class<?>[]{factory.getType()}, this);
        }

        /**
         * Create a new executor.
         * 
         * @param component  the component owning the executor
         * @throws GrmException if the executor could not be created
         */
        public void createExecutor(AbstractComponent<E,C> component) throws GrmException {
            executor.set(factory.createExecutor(component));
        }

        /**
         * Get a proxy to the executor.
         * 
         * <p>The returned proxy is not allowed to shutdown the executor.</p>
         * @return the proxy to the executor
         */
        public E getProxy() {
            return proxy;
        }

        /**
         * Invoke an executor in on the executor
         * @param proxy    the proxy
         * @param method   the method
         * @param args     the arguments for the method call
         * @return  the return value
         * @throws java.lang.Throwable
         */
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            E exe = executor.get();
            if (exe == null) {
                throw new RejectedExecutionException("Executor is not active");
            }
            if (method.getName().startsWith("shutdown")) {
                throw new UnsupportedOperationException("shuting down the executor is not allowed");
            }
            return method.invoke(exe, args);
        }

        List<Runnable> shutdownNow() {
            E exe = executor.get();
            if (exe == null) {
                return Collections.<Runnable>emptyList();
            }
            return exe.shutdownNow();
        }

        private Future<?> submit(Runnable task) {
            E exe = executor.get();
            if (exe == null) {
                throw new RejectedExecutionException("executor is not active");
            }
            return exe.submit(task);
        }

        private void shutdown() {
            E exe = executor.get();
            if (exe != null) {
                exe.shutdown();
            }
        }

        private boolean isTerminated() {
            E exe = executor.get();
            if (exe == null) {
                return true;
            }
            return exe.isTerminated();
        }

        private boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
            E exe = executor.get();
            if (exe == null) {
                return true;
            }
            return exe.awaitTermination(timeout, unit);
        }
    }

}
