/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.*;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

/**
 *  <p>An <code>AbstractComponentContainer</code> creates on demand via the
 *  <code>ComponentAdapterFactory</code> a <code>ComponentAdapter</code>.</p>
 * 
 *  <p>The functionality of the component is implemented in the <code>ComponentAdapter</code>,
 *     the <code>AbstractComponentContainer</code> forwards all calls to the 
 *     <code>ComponentAdapter</code>.
 * 
 *  <H1>Thread saveness</H1>
 * 
 *  <p>
 *  This class is thread safe. Its reference to the adapter is protected by a <code>AtomicReference</code>.
 *  All <code>prepare...</code> methods assumes that they are called within the global transition lock
 *  of the component</code>.
 *  </p>
 * 
 *  @see   AbstractComponent#lock() 
 * 
 *  @param <C>  The type of configuration of the component
 *  @param <E>  The type of the <code>ExecutorService</code> provided by the component
 *  @param <T>  The type of the <code>ComponentAdapter</code> which implements the functionality
 */
public abstract class AbstractComponentContainer<T extends ComponentAdapter<E,C>, E extends ExecutorService, C> 
        extends AbstractComponent<E,C>  implements ComponentContainer<T, E, C> {

    static final String BUNDLE = "com.sun.grid.grm.impl.messages";
    
    private static final Logger log = Logger.getLogger(AbstractComponentContainer.class.getName(), BUNDLE);

    /** This factory is used to create the component adapter */
    private final ComponentAdapterFactory<ComponentContainer<T,E,C>,T,E,C> componentAdapterFactory;
    
    /** the component adapter */
    private AtomicReference<T> adapter = new AtomicReference<T>();
    
    /**
     * Creates a new instance of GrmComponentDelegator.
     *
     * @param env                    the execution environment for the component
     * @param name                   name of the component
     * @param executorServiceFactory factory for the <code>ExecutorService</code>
     * @param componentAdapterFactory factory for the component adapter
     */
    @SuppressWarnings("unchecked")
    public AbstractComponentContainer(ExecutionEnv env, String name, 
                                 ExecutorServiceFactory<? extends AbstractComponent<E,C>,E,C> executorServiceFactory,
                                 ComponentAdapterFactory<? extends ComponentContainer<T,E,C>,T,E,C> componentAdapterFactory) {
        super(env, name, executorServiceFactory);
        this.componentAdapterFactory = (ComponentAdapterFactory<ComponentContainer<T,E,C>,T,E,C>)componentAdapterFactory;
    }

    /**
     * Get the adapter of the component.
     *
     * @return the component delegate
     * @throws ComponentNotActiveException if the component is not active
     */
    public final T getAdapter() throws ComponentNotActiveException {
        log.entering(getClass().getName(), "getAdapter");
        T ret = adapter.get();
        if (ret == null) {
            throw new ComponentNotActiveException(getName());
        }
        log.exiting(getClass().getName(), "getAdapter", ret);
        return ret;
    }

    /**
     * This method is called after a new adapter has been created.
     * 
     * @param oldAdapter  reference to the old adapter (can be null)
     * @param newAdapter  reference to the newly created adapter
     */
    protected void postCreateAdapter(T oldAdapter, T newAdapter) {
        // Empty default implementation
    }

    /**
     * With the call of <code>prepareComponentStartup</code> the new component adapter is created
     * and the <code>prepareComponentStartup</code> if the component adapter is called.
     * 
     * <p>It is assumed that the caller of this method has the global transition
     *    lock.</p>
     * 
     * @param config the configuration of the component
     * @throws com.sun.grid.grm.GrmException if the component adapter could not be created
     *                or if the <code>prepareComponentStartup</code> of the <code>ComponentAdapter</code>
     *                threw an exception.
     */
    @Override
    protected final void prepareComponentStartup(C config) throws GrmException {
        T newAdapter = componentAdapterFactory.createAdapter(this,config);
        postCreateAdapter(adapter.get(), newAdapter);
        newAdapter.prepareComponentStartup(config);
        adapter.set(newAdapter);
    }

    /**
     * Start the component.
     * 
     * <p>Delegates the call to the <code>ComponentAdapter</code>.</p>
     * 
     * @see ComponentAdapter#startComponent(java.lang.Object) 
     * @param config the configuration of the component
     * @throws com.sun.grid.grm.GrmException if the startup of the component failed
     */
    protected final void startComponent(C config) throws GrmException {
        getAdapter().startComponent(config);
    }

    /**
     * Prepare the the component shutdown.
     * 
     * <p>Calls simply <code>ComponentAdater#prepareComponentShutdown</code>.</p>
     * 
     * <p>It is assumed that the caller of this method has the global transition
     *    lock.</p>
     * 
     * @see ComponentAdapter#prepareComponentShutdown(boolean) 
     * @param forced  if <code>true</code> shutdown the component in forced mode
     * @throws com.sun.grid.grm.GrmException of the shutdown failed
     */
    @Override
    protected final void prepareComponentShutdown(boolean forced) throws GrmException {
        getAdapter().prepareComponentShutdown(forced);
    }
    

    /**
     * Calls the <code>stopComponent</code> of the <code>ComponentAdapter</code>.
     * 
     * @param forced  if <code>true</code> shutdown the component in forced mode
     * @throws com.sun.grid.grm.GrmException if the shutdown failed
     */
    protected final void stopComponent(boolean forced) throws GrmException {
        getAdapter().stopComponent(forced);
    }

    /**
     * Prepare the reload of the component.
     * 
     * <p>Calls the <code>prepareComponentReload</code> of the <code>ComponentAdapter</code>.</p>
     * 
     * @param config the new configuration of the component
     * @param forced  if <code>true</code> reload the component in forced mode
     * @throws com.sun.grid.grm.GrmException if the startup of the component failed
     * @throws com.sun.grid.grm.impl.RestartRequiredException if <code>ComponentAdapterFactory</code> signalized that the 
     *                     implementation of the <code>ComponentAdapter</code> has changed with the new configuration
     */
    @Override
    protected final void prepareComponentReload(C config, boolean forced) throws GrmException, RestartRequiredException {
        if (componentAdapterFactory.hasImplementationChanged(this, adapter.get(), config)) {
            throw new RestartRequiredException();
        }
        getAdapter().prepareComponentReload(config, forced);
    }

    /**
     * Perform the reload of the component.
     * 
     * @param config  the new configuration of the component
     * @param forced  if <code>true</code> reload the component in forced mode
     * @throws com.sun.grid.grm.GrmException if the reload failed
     */
    protected final void reloadComponent(C config, boolean forced) throws GrmException {
        getAdapter().reloadComponent(config, forced);
    }
}
