/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.impl;

import com.sun.grid.grm.GrmException;
import java.util.concurrent.ExecutorService;

/**
 * Factory class for creating the <code>ExecutorService<code> for a component.
 * 
 * @param <T>  The type of component
 * @param <E>  The type of <code>ExecutorService</code> of the component
 * @param <C>  The type of config of the component
 */
public interface ExecutorServiceFactory<T extends AbstractComponent<E,C>,E extends ExecutorService, C> {

    /**
     * Get the type of the executor service
     * 
     * <p>Unfortunately java has no possibility to find out what class behindes E is.</p>
     * @return the type of the executor service
     */
    public Class<? extends ExecutorService> getType();

    /**
     * Create the <code>ExecutorService</code> for a component
     * @param component  the component
     * @return the <code>ExecutorService</code>
     * @throws GrmException if the <code>ExecutorService</code> could not be created
     */
    public E createExecutor(T component) throws GrmException;
}
