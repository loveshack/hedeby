/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service;

import com.sun.grid.grm.GrmException;
import java.util.ResourceBundle;

/**
 * This exception indicates that a Service is not active and is not able to
 * perform certain tasks.
 */
public class ServiceNotActiveException extends GrmException {
    private static final String BUNDLE = "com.sun.grid.grm.service.messages";
    private final static long serialVersionUID = -2007080101L;
    
    /**
     * Creates a new instance of <code>ServiceNotActiveException</code> without detailed message.
     */
    public ServiceNotActiveException() {
        this(ResourceBundle.getBundle(BUNDLE).getString("snae.notActiveDef"));
    
    }

    /**
     * Constructs an instance of <code>ServiceNotActiveException</code> with the specified detailed message.
     * 
     * @param msg the detail message.
     */
    public ServiceNotActiveException(String msg) {
        super(msg);
    }
    
    /**
     * Constructs an instance of <code>ServiceNotActiveException</code> with the specified
     * internationalized detailed message.
     * @param msg the detailed message.
     * @param cause the source Throwable
     * @param bundleName the name of the resource bundle
     * @param params the parameters to the message
     */
    public ServiceNotActiveException(String msg, Throwable cause, String bundleName, Object... params) {
        super(msg, bundleName, params, cause);
    }
    
    /**
     * Constructs an instance of <code>ServiceNotActiveException</code> with
     * the specified detailed message.
     * 
     * @param msg    the message
     * @param cause  the cause
     */
    public ServiceNotActiveException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
