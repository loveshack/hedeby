/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service;

import java.io.Serializable;
import java.util.ResourceBundle;

/**
 * The Usage class is a representation of a service container's need for a
 * resource.  A Usage object may be referred to as a &quot;usage&quot;,
 * &quot;urgency&quot;, &quot;level&quot;, or &quot;quantifier&quot;.
 */
public class Usage implements Serializable, Comparable<Usage>, Cloneable {
    
    private static final String BUNDLE = "com.sun.grid.grm.service.messages";
    
    private static final long serialVersionUID = -2007101301L;
    public static final Usage MAX_VALUE = new Usage(Integer.MAX_VALUE);
    public static final Usage MIN_VALUE = new Usage(0);
    private int level = 0;

    /**
     * Creates a new instance of Usage
     * @param level - the usage level
     */
    public Usage(int level) {
        if(level < 0) {
            throw new IllegalArgumentException("level must be positive number");
        }
        this.level = level;
    }

    /**
     * Gets the usage level.
     * @return the usage level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Compares this Usage object to another Usage object.
     * @param object - another Usage object
     * @return whether this Usage object is the same as the Usage object
     */
    @Override
    public boolean equals(Object object) {
        if (object instanceof Usage) {
            return this.level == ((Usage)object).level;
        } else {
            return false;
        }
    }

    /**
     * Compares this Usage object to another Usage object.
     * @param usage another Usage object
     * @return &gt;0 if this Usage object's level is higher<br>
     * 0 if both levels are equal<br>
     * &lt;0 if this Usage object's level is lower
     */
    public int compareTo(Usage usage) {
        return this.level - usage.level;
    }

    /**
     * Returns a hash code for this object.
     * @return a hash code for this object
     */
    @Override
    public int hashCode() {
        return level;
    }
    
    /**
     * Returns a String representation of this object.
     * @return a String representation of this object
     */
    @Override
    public String toString() {
        if(level == MAX_VALUE.getLevel()) {
            return ResourceBundle.getBundle(BUNDLE).getString("u.inf");
        }
        return Integer.toString(level);
    }

    /**
     * Creates and returns a copy of this object.
     * 
     * @return a clone of this instance.
     * @see java.lang.Cloneable
     */
    @Override
    public Object clone() {
        Usage retValue = null;

        try {
            retValue = (Usage)super.clone();
        } catch (CloneNotSupportedException e) {
            /* This exception cannot happen, but the compiler doesn't know
             * that. */
            assert false : "Clone operation failed on Usage object";
        }        
        
        return retValue;
    }
}
