/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.management.Manageable;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.UnknownResourceException;

import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * This interface describes all methods of a Service from the Hedeby view.
 * 
 * <p><code>ServiceAdapter</code> and <code>ServiceContainer</code> implements 
 *    this interface.</p>
 * 
 * <p>The name <code>ServiceBase</code> is not a good choice for this interface,
 *    Naming it directly <code>Service</code> would be a better solution.
 *    However renaming is for compatibility reasons not possible.</p>
 */
@Manageable
public interface ServiceBase {

    /**
     * returns the current service state
     *
     * @return the state the service is in
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public ServiceState getServiceState() throws GrmRemoteException;
    
    /**
     * Get the states of the SLOs of this service.
     * 
     * @return list of slo states
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public List<SLOState> getSLOStates() throws GrmRemoteException;
    
    /**
     * Get the SLOStates of this service. Include only the resource information
     * of those resources matching a <code>resourceFitler</code>
     * @param resourceFilter  the resource filter
     * @return the SLOState
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException;


    /**
     * This call will take a filter to search matching resources
     * @param filter the filter
     * @return list with the matching resources
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    List<Resource> getResources(Filter<Resource> filter)
            throws ServiceNotActiveException, GrmRemoteException;
    /**
     * Get all resources of the service
     * @return list of resources
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    List<Resource> getResources() throws ServiceNotActiveException, GrmRemoteException;

    /**
     *
     * @param resourceId
     * @return the resource
     * @throws com.sun.grid.grm.resource.UnknownResourceException
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public Resource getResource(ResourceId resourceId)
            throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException;

    /**
     * Removes a resource from a Service. This is a non-blocking call and can take a
     * long time. The call will be notified upon the success if it is registered as an
     * Event Listener.
     * The call will wait until the resource to remove is no longer used. Once that is
     * the case, it will remove the resource from the service and return it to the call
     * via a notification.
     *
     * @param resourceId  the id of the resource which should be removed
     * @param descr descriptor if the operation
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not found
     * @throws com.sun.grid.grm.resource.InvalidResourceException if resource is not valid for remove
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr)
            throws UnknownResourceException, InvalidResourceException, ServiceNotActiveException, GrmRemoteException;

    /**
     * Reset a resource state. The reset signals to the service that all steps
     * were taken to bring the resource to operational status and
     * that the service can start to use the resource again.
     *
     * Upon a successful reset, the notification is sent to registered listeners.
     *
     * @param resource The resource to reset
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not found
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    void resetResource(ResourceId resource)
            throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException;        

    /**
     * Adds a resource to the service. This call will not block and return right away.
     * If a Resource Event Listener is registered the listener will be informed once
     * the resource was added or if an error occured.
     *
     * @param resource The resource object that will be added to the service
     * @throws com.sun.grid.grm.resource.InvalidResourceException
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    void addResource(Resource resource)
            throws InvalidResourceException, ServiceNotActiveException, GrmRemoteException;
    
    
    /**
     * Adds a resource to the service. This call will not block and return right away.
     * If a Resource Event Listener is registered the listener will be informed once
     * the resource was added or if an error occurred.
     * If the resource was added in response to an SLO request the SLO name is provided
     * @param resource The resource object that will be added to the service
     * @param sloName if the resource is added in response of a SLO request it contains the SLO name
     * otherwise it contains null
     * @throws com.sun.grid.grm.resource.InvalidResourceException
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    void addResource(Resource resource,String sloName)
            throws InvalidResourceException, ServiceNotActiveException, GrmRemoteException;


    /**
     * Create a new resource and assign it
     * @param type       the type of the resource
     * @param properties the properties of the resource
     * @throws ServiceNotActiveException if the service is not active
     * @throws InvalidResourceException If the resource type is not supported
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the resource properties are not valid
     * @throws com.sun.grid.grm.GrmRemoteException  communication error
     * @throws com.sun.grid.grm.GrmException if the resource could not be created
     * @return the resource
     */
    Resource addNewResource(ResourceType type, Map<String,Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException;
    
    /**
     * Triggers an operation that modifies a resource (its properties). It depends
     * on a service whether the operation actively change the underlying resource
     * (if the service has tools to do that) or just passively updates the resource properties.
     *
     * Active change of the resource can be for example changing the size of the virtual memory
     * of the host resource (the service can ask the resource to change the virtual
     * memory size to the new value and if successful it can update the resource properties).
     *
     * Passive change of the resource can be for example changing the size of the physical memory
     * of the host resource (the amount of physical memory has to changed by manual
     * intervention of the administrator).
     *
     * @param resource a resource to be modified
     * @param operations that will modify resource properties and attributes
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not found
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException of the properties are not valid
     */
    void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations)
            throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException , InvalidResourcePropertiesException;

    /**
     * Triggers an operation that modifies a resource (its properties). It depends
     * on a service whether the operation actively change the underlying resource
     * (if the service has tools to do that) or just passively updates the resource properties.
     *
     * Active change of the resource can be for example changing the size of the virtual memory
     * of the host resource (the service can ask the resource to change the virtual
     * memory size to the new value and if successful it can update the resource properties).
     *
     * Passive change of the resource can be for example changing the size of the physical memory
     * of the host resource (the amount of physical memory has to changed by manual
     * intervention of the administrator).
     *
     * @param resource a resource to be modified
     * @param operation that will modify resource properties and attributes
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not found
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException of the properties are not valid
     */
    void modifyResource(ResourceId resource, ResourceChangeOperation operation)
            throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException , InvalidResourcePropertiesException;
    
    /**
     * Get a snapshot of the current state of the service
     * @return the snapshot of the current state of the service
     * @throws com.sun.grid.grm.GrmRemoteException
     * @throws ServiceNotActiveException if the service is not active
     */
    ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException;
}
