/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.event;

/**
 * Adapter class for a ServicEventListener. Provider a empty default
 * implementatoin for each ServiceEventListener method
 */
public class ServiceEventAdapter implements ServiceEventListener {

    /**
     * Empty implementation
     * @param event
     */
    public void resourceRequest(ResourceRequestEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void addResource(AddResourceEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void resourceAdded(ResourceAddedEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void removeResource(RemoveResourceEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void resourceRemoved(ResourceRemovedEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void resourceRejected(ResourceRejectedEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void resourceError(ResourceErrorEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void resourceReset(ResourceResetEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void resourceChanged(ResourceChangedEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void serviceStarting(ServiceStateChangedEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void serviceRunning(ServiceStateChangedEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void serviceUnknown(ServiceStateChangedEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void serviceShutdown(ServiceStateChangedEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void serviceError(ServiceStateChangedEvent event) {
    }

    /**
     * Empty implementation
     * @param event
     */
    public void serviceStopped(ServiceStateChangedEvent event) {
    }

}
