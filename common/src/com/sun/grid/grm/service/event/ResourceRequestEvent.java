/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.event;

import com.sun.grid.grm.service.Need;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Service uses instances of this class to report resource requests 
 * to <code>ServiceListener</code>s.
 */
public class ResourceRequestEvent extends AbstractServiceEvent implements Serializable {

    /**
     * the serial version UID.
     */
    private static final long serialVersionUID = 2007100401;
    private final String slo;
    private final List<Need> needs;

    /**
     * Creates a new instance of ResourceRequestEvent.
     * @param sequenceNumber the event sequence number
     * @param serviceName name of the service
     * @param SLOName name of the SLO that triggered the request
     * @param aNeedList list of resource needs
     */
    public ResourceRequestEvent(long sequenceNumber, String serviceName, String SLOName, Collection<Need> aNeedList) {
        super(sequenceNumber, serviceName);
        slo = SLOName;
        needs = Collections.unmodifiableList(new ArrayList<Need>(aNeedList));
    }

    /**
     *  Get the resource needs.
     *
     *  @return unmodifiable list of resource needs
     */
    public List<Need> getNeeds() {
        return needs;
    }

    /**
     *  Get the name of SLO that triggered th request.
     *
     *  @return string representation of the SLO name
     */
    public String getSLOName() {
        return slo;
    }

    /**
     *  Get human readable string representation of this event
     *
     *  @return human readable string representation
     */
    @Override
    public String toString() {
        return String.format("[%d, %s, resource requests: %s, %s]", getSequenceNumber(), getServiceName(), slo, needs.toString());
    }
}
