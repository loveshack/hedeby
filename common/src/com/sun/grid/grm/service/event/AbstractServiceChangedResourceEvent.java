/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.event;

import com.sun.grid.grm.resource.Resource;
import java.io.Serializable;

/**
 * Abstract base class for all events where a service reports modifications 
 * on a resource.
 */
public abstract class AbstractServiceChangedResourceEvent extends AbstractServiceEvent implements Serializable {
    
    /**
     * the serial version UID
     */
    private static final long serialVersionUID = 2007100401;
    
    private final Resource   resource;
    private final String     message;
    
    /**
     * Creates a new instance of AbstractServiceChangedResourceEvent
     * 
     * @param sequenceNumber the sequence number
     * @param serviceName name of the servce
     * @param aResource the snapshot of the resource
     */
    public AbstractServiceChangedResourceEvent(long sequenceNumber, String serviceName, Resource aResource) {
        this(sequenceNumber, serviceName, aResource, null);
    }

    /**
     * Creates a new instance of AbstractServiceChangedResourceEvent
     * 
     * @param sequenceNumber the sequence number
     * @param serviceName the name of the service
     * @param aResource snapshot of the resource
     * @param aMessage the message for monitoring
     */
    public AbstractServiceChangedResourceEvent(long sequenceNumber, String serviceName, Resource aResource, String aMessage) {
        super(sequenceNumber, serviceName);
        resource = aResource;
        message = aMessage;
    }
    
    /**
     * get the snapshot of the resource.
     *
     * @return the resource
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * get the message for monitoring.
     * @return the message for monitoring
     */
    public String getMessage() {
        return message;
    }
    
}
