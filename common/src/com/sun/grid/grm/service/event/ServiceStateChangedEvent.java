/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.event;

import com.sun.grid.grm.service.ServiceState;
import java.io.Serializable;

/**
 * Services uses instances of this class to report service state changes.
 */
public class ServiceStateChangedEvent extends AbstractServiceEvent implements Serializable {
    
    /**
     * the serial version UID.
     */
    private static final long serialVersionUID = 2007100401;
    
    private final ServiceState newState;
    
    /**
     * Creates a new instance of ServiceStateChangedEvent.
     *
     * @param sequenceNumber the event sequence number
     * @param serviceName the name of the service
     * @param aNewState the new state
     */
    public ServiceStateChangedEvent(long sequenceNumber, String serviceName, ServiceState aNewState) {
        super(sequenceNumber, serviceName);
        newState = aNewState;
    }

    /**
     * Get the new state of the resource.
     *
     * @return the new state of the resource
     */
    public ServiceState getNewState() {
        return newState;
    }
    
    /**
     *  Get human readable string representation of this event
     *
     *  @return human readable string representation
     */
    @Override
    public String toString() {
        return String.format("[%d, %s, state changed: %s]", getSequenceNumber(), getServiceName(), newState);
    }
    
    
}
