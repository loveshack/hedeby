/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.event;

import com.sun.grid.grm.event.ComponentEventSupport;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.util.EventListenerSupport;
import com.sun.grid.grm.util.Hostname;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The ServiceEventSupport is used by the Services to fire service events.
 * 
 * <h2>Thread safeness</h2>
 * 
 * This class guarantees that the sequence number of the fired events reflects
 * the order of events.
 */
public class ServiceEventSupport extends ComponentEventSupport {

    private final static String BUNDLE = "com.sun.grid.grm.service.event.messages";
    private static Logger log = Logger.getLogger(ServiceEventSupport.class.getName(), BUNDLE);
    private final EventListenerSupport<ServiceEventListener> listeners;
    private final String serviceName;

    /**
     * Create a new instance of ServiceEventSupport.
     * 
     * @param serviceName name of the service
     * @param synchron if true, the message delivery is synchronous
     */
    private ServiceEventSupport(String serviceName, Hostname hostname) {
        super(serviceName, hostname);
        if (serviceName == null) {
            throw new IllegalArgumentException("service name must not be null");
        }
        this.serviceName = serviceName;
        this.listeners = EventListenerSupport.<ServiceEventListener>newInstance(ServiceEventListener.class, getEventProcessor());
        setLogPrefix("Service");
    }    

    /**
     * Create a new instance of ServiceEventSupport
     * @param serviceName name of the service
     * @param hostname the host name where the service is living
     * @return instance of ServiceEventSupport
     */
    public static ServiceEventSupport newInstance(String serviceName, Hostname hostname) {
        return new ServiceEventSupport(serviceName, hostname);
    }

    /**
     * Register a <code>ServiceEventListener</code>.
     *
     * @param eventListener the service listener
     */
    public void addServiceEventListener(ServiceEventListener eventListener) {
        listeners.addListener(eventListener);
    }

    /**
     * Deregister a <code>ServiceEventListener</code>.
     *
     * @param eventListener the service listener
     */
    public void removeServiceEventListener(ServiceEventListener eventListener) {
        listeners.removeListener(eventListener);
    }

    /**
     * Send a resource request event
     * @param SLOName SLO that triggered the request
     * @param needs list of resource needs
     */
    public void fireResourceRequest(String SLOName, Collection<Need> needs) {
        lock();
        try {
            ResourceRequestEvent evt = new ResourceRequestEvent(createSequenceNumberForEvent(),
                    serviceName, SLOName, needs);

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }

            listeners.getProxy().resourceRequest(evt);
        } finally {
            unlock();
        }
    }

    /**
     * Send a resource request event for a single need.
     * @param SLOName SLO that triggered the request
     * @param need the need
     */
    public void fireResourceRequest(String SLOName, Need need) {
        fireResourceRequest(SLOName, Collections.singletonList(need));
    }

    /**
     * Fire a add resource event.
     * @param resource the resource which will be added
     * @param message message for the for monitoring
     */
    public void fireAddResource(Resource resource, String message) {
        lock();
        try {
            AddResourceEvent evt = new AddResourceEvent(createSequenceNumberForEvent(),
                    serviceName, resource, message);

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }

            listeners.getProxy().addResource(evt);
        } finally {
            unlock();
        }
    }

    /**
     * Fire a resource added event.
     * @param resource the resource which has been added
     * @param message the message for monitoring
     */
    public void fireResourceAdded(Resource resource, String message) {
        lock();
        try {
            ResourceAddedEvent evt = new ResourceAddedEvent(createSequenceNumberForEvent(),
                    serviceName, resource, message);

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }
            listeners.getProxy().resourceAdded(evt);
        } finally {
            unlock();
        }
    }

    /**
     * Fire a remove resource event
     * @param resource The resource which will be removed
     * @param message The message for monitoring
     */
    public void fireRemoveResource(Resource resource, String message) {
        lock();
        try {
            RemoveResourceEvent evt = new RemoveResourceEvent(createSequenceNumberForEvent(),
                    serviceName, resource, message);

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }

            listeners.getProxy().removeResource(evt);
        } finally {
            unlock();
        }
    }

    /**
     * Fire a resource removed event.
     * @param resource The resource which has been removed
     * @param message The message for monitoring
     */
    public void fireResourceRemoved(Resource resource, String message) {
        lock();
        try {
            ResourceRemovedEvent evt = new ResourceRemovedEvent(createSequenceNumberForEvent(),
                    serviceName, resource, message);
            
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }

            listeners.getProxy().resourceRemoved(evt);
        } finally {
            unlock();
        }
    }

    /**
     * Fire a Resource Rejected Event
     * @param resource The resource which has been rejected
     * @param message message for monitoring
     */
    public void fireResourceRejected(Resource resource, String message) {
        lock();
        try {
            ResourceRejectedEvent evt = new ResourceRejectedEvent(createSequenceNumberForEvent(),
                    serviceName, resource, message);

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }
            listeners.getProxy().resourceRejected(evt);
        } finally {
            unlock();
        }
    }

    /**
     * Fire a resource error event.
     * @param resource the error resource 
     * @param message the message for monitoring
     */
    public void fireResourceError(Resource resource, String message) {
        lock();
        try {
            ResourceErrorEvent evt = new ResourceErrorEvent(createSequenceNumberForEvent(),
                    serviceName, resource, message);

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }

            listeners.getProxy().resourceError(evt);
        } finally {
            unlock();
        }
    }

    /**
     * Fire a resource reset event
     * @param resource the resource which has been reset
     * @param message The message for monitoring
     */
    public void fireResourceReset(Resource resource, String message) {
        lock();
        try {
            ResourceResetEvent evt = new ResourceResetEvent(createSequenceNumberForEvent(),
                    serviceName, resource, message);

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }

            listeners.getProxy().resourceReset(evt);
        } finally {
            unlock();
        }
    }

    /**
     * Fire a resource properties changed event
     * @param resource the resource
     * @param changes the changed properties and attributes of the resource
     * @param message The message for monitoring
     */
    public void fireResourceChanged(Resource resource, Collection<ResourceChanged> changes, String message) {
        lock();
        try {
            ResourceChangedEvent evt = new ResourceChangedEvent(createSequenceNumberForEvent(),
                    serviceName, resource, changes, message);

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }

            listeners.getProxy().resourceChanged(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The service adapter tries to connect to the real service. If it has
     * full control over the service it tries to start the service.
     */
    public void fireServiceStarting() {
        lock();
        try {
            ServiceStateChangedEvent evt = new ServiceStateChangedEvent(createSequenceNumberForEvent(), serviceName, ServiceState.STARTING);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }
            listeners.getProxy().serviceStarting(evt);
        } finally {
            unlock();
        }
    }

    /**
     *  The service adapter has established a connection to the real service
     *  Service is working.
     */
    public void fireServiceRunning() {
        lock();
        try {
            ServiceStateChangedEvent evt = new ServiceStateChangedEvent(createSequenceNumberForEvent(), serviceName, ServiceState.RUNNING);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }
            listeners.getProxy().serviceRunning(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The service adapter does not kown in what state the real service
     * is.
     */
    public void fireServiceUnknown() {
        lock();
        try {
            ServiceStateChangedEvent evt = new ServiceStateChangedEvent(createSequenceNumberForEvent(), serviceName, ServiceState.UNKNOWN);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }
            listeners.getProxy().serviceUnknown(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The service adapter is shuting down the service. If it has full
     * control over the service it now stopping the real service
     */
    public void fireServiceShutdown() {
        lock();
        try {
            ServiceStateChangedEvent evt = new ServiceStateChangedEvent(createSequenceNumberForEvent(), serviceName, ServiceState.SHUTDOWN);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }
            listeners.getProxy().serviceShutdown(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The service adapter has a connection to the service, but the
     * service is not working
     */
    public void fireServiceError() {
        lock();
        try {
            ServiceStateChangedEvent evt = new ServiceStateChangedEvent(createSequenceNumberForEvent(), serviceName, ServiceState.ERROR);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }
            listeners.getProxy().serviceError(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The service adapter has shutdown the service. The service adapter
     * will only shutdown the service it is has full control over it.
     */
    public void fireServiceStopped() {
        lock();
        try {
            ServiceStateChangedEvent evt = new ServiceStateChangedEvent(createSequenceNumberForEvent(), serviceName, ServiceState.STOPPED);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { getLogPrefix(), serviceName, evt });
            }
            listeners.getProxy().serviceStopped(evt);
        } finally {
            unlock();
        }
    }

}
