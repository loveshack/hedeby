/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.event;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

/**
 * A service uses instances of this class to report changes in the
 * resource properties and attributes to <code>ServiceListener</code>s.
 */
public class ResourceChangedEvent extends AbstractServiceChangedResourceEvent implements Serializable {

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 2007112501;
    private final Collection<ResourceChanged> diffProperties;
    
    /**
     * Creates a new instance of ResourceChangedEvent.
     * @param sequenceNumber the sequence number of the event
     * @param serviceName the name of the service
     * @param resource   snapshot of the resource
     * @param changed      set of changed properties
     * @param message    message for the monitoring purposes
     */
    public ResourceChangedEvent(long sequenceNumber, String serviceName, Resource resource, Collection<ResourceChanged> changed, String message) {
        super(sequenceNumber, serviceName, resource, message);
        // To make it really immutable we must copy the properties
        this.diffProperties = Collections.<ResourceChanged>unmodifiableCollection(changed);
    }
    
    /**
     *  Get human readable string representation of this event
     *
     *  @return human readable string representation
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ResourceChanged rc: diffProperties) {
            sb.append(rc.getName());
            sb.append("=");
            sb.append(rc.getValue().toString());
            sb.append(", ");
        }
        return String.format("[%d, %s, properties of resource %s changed: %s]", getSequenceNumber(), getServiceName(), getResource(), sb.toString());
    }

    /**
     * get the snapshot of the set of resources that were changed.
     *
     * @return the changed resource properties
     */
    public Collection<ResourceChanged> getProperties() {
        return diffProperties;
    }
    
}
