/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.event;

import com.sun.grid.grm.resource.Resource;
import java.io.Serializable;

/**
 * With Instances of this class a service signalizes that an error occured on a resource
 * The service can not longer handle the resource. The Administrator must have a look on 
 * the resource.
 */
public class ResourceErrorEvent extends AbstractServiceChangedResourceEvent implements Serializable {
    
    /**
     * the serial version UID.
     */
    private static final long serialVersionUID = 2007100401;
    
    /**
     * Creates a new instance of ResourceErrorEvent.
     *
     * @param sequenceNumber sequence number of the service event
     * @param serviceName    name of the service
     * @param resource       snapshot of the resource
     * @param message        message for the state transition
     */
    public ResourceErrorEvent(long sequenceNumber, String serviceName, Resource resource, String message) {
        super(sequenceNumber, serviceName, resource, message);
    }
    
    /**
     *  Get human readable string representation of this event
     *
     *  @return human readable string representation
     */
    @Override
    public String toString() {
        return String.format("[%d, %s, resource %s has error: %s]", getSequenceNumber(), getServiceName(), getResource(), getMessage());
    }
    
    
}
