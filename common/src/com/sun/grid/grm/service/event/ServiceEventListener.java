/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.event;

import com.sun.grid.grm.management.ManagedEventListener;
import java.util.EventListener;

/**
 * The ServiceListener will be used by the Service to inform about state changes
 * in the Service. These "events" will be used by the ResourceProvider.
 *
 */
@ManagedEventListener
public interface ServiceEventListener extends EventListener {

    /**
     * When ever the Service evaluates its SLOs, it will create a need 
     * event. This event can contain Need objects for each SLO, that is not 
     * meet. It can also be an empty collection identifying the ResourceProvider 
     * that there are currently no further needs.
     *
     * @param event  the event object which contains the need list
     */
    void resourceRequest(ResourceRequestEvent event);
    
    
    /**
     *  The service informs the listeners that is starting to add a resource.
     *
     *  @param event  the event object
     */
    void addResource(AddResourceEvent event);
    
    /**
     *  The service informs the listeners that a resource has been added.
     *
     *  @param event  the event object
     */
    void resourceAdded(ResourceAddedEvent event);
    
    /**
     *  The service informs the listeners that a resource is added to the service
     *
     *  @param event  the event object
     */
    void removeResource(RemoveResourceEvent event);
    
    /**
     *  The service informs the listeners that has been removed from the service.
     *
     *  @param event  the event object
     */
    void resourceRemoved(ResourceRemovedEvent event);
    
    /**
     *  The service informs the listeners that a resource can not be handled by 
     *  the service.
     *
     *  <p>It is ensured that the resource has not been touched by the service</p>
     *
     *  @param event  the event object
     */
    void resourceRejected(ResourceRejectedEvent event);
    
    /**
     *  The service informs the listener that an unexpected error has been occurred
     *  on a resource.
     *
     *  <p>This error can not be recovered. Administrator has to look on the resource
     *  and clean it up.</p>
     *
     *  @param event  the event object
     */
    void resourceError(ResourceErrorEvent event);
    
    /**
     *  The service reports that a resoruce has been reseted. The service will
     *  continue to use the resource.
     *
     *  @param event  the event object
     */
    void resourceReset(ResourceResetEvent event);
    
    
    /**
     * The service reports changes in the resource properties with this method
     *
     *  @param event  the event object
     */
    void resourceChanged(ResourceChangedEvent event);
    

    /**
     * The service adpater tries to connect to the real service. If it has
     * full control over the service it tries to start the service.
     * 
     *  @param event  the event object
     */
    void serviceStarting(ServiceStateChangedEvent event);


    /**
     *  The service adapter has established a connection to the real service
     *  Service is working.
     * 
     * @param event the event object
     */
    void serviceRunning(ServiceStateChangedEvent event);
    
    /**
     * The service adapter does not kown in what state the real service
     * is.
     * 
     * @param event the event object
     */
    void serviceUnknown(ServiceStateChangedEvent event);
    
    /**
     * The service adapter is shutting down the service. If it has full
     * control over the service it is now stopping the real service.
     * 
     * @param event the event object
     */
    void serviceShutdown(ServiceStateChangedEvent event);
    
    /**
     * The service adapter has a connection to the service, but the
     * service is not working
     * 
     * @param event the event object
     */
    void serviceError(ServiceStateChangedEvent event);
    
    /**
     * The service adapter has shutdown the service - the service is stopped. 
     * The service adapter will only shutdown the service it is has full control 
     * over it.
     * 
     * @param event
     */
    void serviceStopped(ServiceStateChangedEvent event);
    
}
