/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service;

import java.util.List;
import java.util.Set;

/**
 * The interface specifying the operations for storage of services.
 * 
 * Implementing classes can decide whether they use in-memory only storage
 * of service or a persistent storage.
 * 
 * @param <T> implementing class type of stored services
 */
public interface ServiceStore<T extends Service> {

    /**
     * Removes all services.
     * 
     * @throws com.sun.grid.grm.service.ServiceStoreException 
     */
    public void clear() throws ServiceStoreException;

    /**
     * Adds a service. ServiceName is used as a key.
     * 
     * @param service service to be added
     * @return an old service with the same name as added object or null if such
     * service was not stored
     * @throws com.sun.grid.grm.service.ServiceStoreException if operation fails
     */
    public T addService(T service) throws ServiceStoreException;

    /**
     * Removes a service.
     * 
     * @param serviceName name of a service to be removed
     * @return the removed service
     * @throws com.sun.grid.grm.service.UnknownServiceException if service with 
     * such name was not found
     * @throws com.sun.grid.grm.service.ServiceStoreException  if operation fails
     */
    public T removeService(String serviceName) throws UnknownServiceException, ServiceStoreException;

    /**
     * Gets a set of names of all stored services.
     * 
     * @return the set of service names
     * @throws com.sun.grid.grm.service.ServiceStoreException if operation fails
     */
    public Set<String> getServiceNames() throws ServiceStoreException;

    /**
     * Gets a service with the specified name.
     * 
     * @param serviceName the name of the service to look for
     * @return the service with specified name
     * @throws com.sun.grid.grm.service.UnknownServiceException if service with 
     * such name was not found
     * @throws com.sun.grid.grm.service.ServiceStoreException  if operation fails
     */
    public T getService(String serviceName) throws UnknownServiceException, ServiceStoreException;

    /**
     * Gets all stored services.
     * @return list of service.
     * @throws com.sun.grid.grm.service.ServiceStoreException  if operation fails
     */
    public List<T> getServices() throws ServiceStoreException;
}
