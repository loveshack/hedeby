/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service;

import com.sun.grid.grm.ComponentAdapter;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.service.slo.SLOManager;
import java.util.concurrent.ExecutorService;

/**
 * Basic interface for a <code>ServiceAdapter</code>
 * @param <E>  The type of <code>ExecutorService</code> for the <code>ServiceAdpater</code>
 * @param <C>  The type of configuration for the <code>ServiceAdapter</code>
 */
public interface ServiceAdapter<E extends ExecutorService, C> extends ComponentAdapter<E,C>, ServiceBase {
    
    /**
     * Starts up the service, if it is not running yet
     *
     * @param config The configuration of the service
     * @throws com.sun.grid.grm.GrmException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void startService(C config) throws GrmException, GrmRemoteException;
    
    /**
     * Stops the service, if it is running. In case that
     * the service is stopped, one can specify to free the
     * resources with it. If the service is not running,
     * the isFreeResources has not effect.
     * 
     * @param freeResources if true, frees all none static resources
     * @throws com.sun.grid.grm.GrmException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void stopService(boolean freeResources) throws GrmException, GrmRemoteException;
    
    
    /**
     * Get the SLOManager of the service
     * @return the SLOManager
     * @throws ServiceNotActiveException if the service is not active
     */
    public SLOManager getSLOManager() throws ServiceNotActiveException;
    
    

}
