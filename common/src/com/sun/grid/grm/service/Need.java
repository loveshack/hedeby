/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import java.io.Serializable;

/**
 * The Need class is a representation of a resource need.  It is composed of a
 * resource description, a need quantifier, and a quantity.
 */
public class Need implements Serializable, Cloneable {

    private static final long serialVersionUID = -2008012401L;
    
    /**
     * The EMPTY_NEED is used to signalize the ResourceProvider the
     * no further resources are needed
     */
    public final static Need EMPTY_NEED = new Need(Usage.MIN_VALUE, 0);
    /* The resource description. */
    private final Filter<Resource> resourceFilter;
    /* The need quantifier. */
    private final Usage urgency;
    /* The resource quantity. */
    private final int quantity;
    
    /**
     * Creates a new instance with a quantity of 1 and without a request filter
     * @param urgency the urgency
     */
    public Need(Usage urgency) {
        this(null, urgency);
    }

    /**
     * Creates a new instance with a quantity of 1.
     * @param resourceFilter filter for the needed resources
     * @param urgency the need quantifier
     * @throws java.lang.NullPointerException if <code>urgency<(/code> is <code>null</code>
     */
    public Need(Filter<Resource> resourceFilter, Usage urgency) {
        this(resourceFilter, urgency, 1);
    }

    /**
     * Creates a new instance without a request filter
     * @param urgency the need quantifier
     * @param quantity the resource quantity
     * @throws java.lang.NullPointerException if <code>urgency<(/code> is <code>null</code>
     */
    public Need(Usage urgency, int quantity) {
        this(null, urgency, quantity);
    }

    /**
     * Creates a new instance of Need
     * @param resourceFilter filter for the needed resources
     * @param urgency the need quantifier
     * @param quantity the resource quantity
     * @throws java.lang.NullPointerException if <code>urgency<(/code> is <code>null</code>
     */
    public Need(Filter<Resource> resourceFilter, Usage urgency, int quantity) {
        if (urgency == null) {
            throw new NullPointerException("urgency must not be null");
        }
        if (quantity < 0) {
            throw new IllegalArgumentException("Quantity must be non-negative number");
        }
        this.resourceFilter = resourceFilter;
        this.urgency = urgency;
        this.quantity = quantity;
    }

    /**
     * Indicates whether any other object is "equal to" this object.
     *
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object is equal as the obj
     *          argument; <code>false</code> otherwise.
     * @see     #hashCode()
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Need other = (Need) obj;

        if (!equalsDisregardingQuantity(other)) {
            return false;
        }
        if (this.quantity != other.quantity) {
            return false;
        }
        return true;
    }

    /**
     * Compares only the resource filter and the urgency of the need with
     * another need, quantity is not regarded.
     * @param other  the other need
     * @return <tt>true</tt> if resource filter and urgency are equal
     */
    public boolean equalsDisregardingQuantity(Need other) {
        if (other == null) {
            return false;
        }
        if (this.resourceFilter != other.resourceFilter && (this.resourceFilter == null || !this.resourceFilter.equals(other.resourceFilter))) {
            return false;
        }
        if (this.urgency != other.urgency && (this.urgency == null || !this.urgency.equals(other.urgency))) {
            return false;
        }
        return true;
    }

    /**
     * Get the hashcode of this object.
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.resourceFilter != null ? this.resourceFilter.hashCode() : 0);
        hash = 97 * hash + (this.urgency != null ? this.urgency.hashCode() : 0);
        hash = 97 * hash + this.quantity;
        return hash;
    }

    /**
     * Get the resource description.
     * @return the resource description
     */
    public Filter<Resource> getResourceFilter() {
        if (resourceFilter == null) {
            return ConstantFilter.<Resource>alwaysMatching();
        } else {
            return resourceFilter;
        }
    }

    /**
     * Get the need quantifier.
     * @return the need quantifier
     */
    public Usage getUrgency() {
        return urgency;
    }

    /**
     * Get the resource quantity.
     * @return the resource quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Returns a string representation of the object.
     * 
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return String.format("Need{%s:%d:%d}", resourceFilter, urgency.getLevel(), quantity);
    }

    /**
     * Creates and returns a copy of this object.
     * @return a clone of this instance.
     * @see java.lang.Cloneable
     */
    @Override
    public Need clone() {
        return new Need(resourceFilter == null ? null : resourceFilter.clone(),
                (Usage) urgency.clone(),
                quantity);

    }
}
