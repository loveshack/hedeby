/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service;

import com.sun.grid.grm.service.descriptor.ResourcePurgeDescriptor;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.descriptor.ResourceRemovalFromSystemDescriptor;

/**
 * Empty tag interface for the descriptor of a resource removal operation. 
 * 
 * <p>Instances of this interface can be used to transport parameter for a 
 *    resource remove operation to a service adapter.</p>
 * 
 * <p>The default Hedeby implementation knows three classes which implement this
 * interface:</p>
 * 
 * <table>
 *   <tr><th>Class</th><th>Description</th></tr>
 *   <tr><td><code>ResourceReassignmentDescriptor</code></td>
 *       <td>
 *          is used whenever the ResourceProvider decides that a resource
 *          should be moved from one Service to another.
 *       </td>
 *    </tr>
 *    <tr><td><code>ResourceRemovalFromSystemDescriptor</code></td>
 *        <td>
 *          is used whenever a resource is going to be removed from the SDM system.
 *        </td>
 *    </tr>
 *    <tr><td><code>ResourcePurgeDescriptor</code></td>
 *        <td>
 *          is used whenever a resource is purged from the SDM system. Purging a
 *          resource means that the Service should <b>NOT</b> do the usual
 *          checks before unassigning (is resource static? is resource state
 *          correct?) and should <b>NOT</b> do any actions to uninstall
 *          the resource (like e.g. uninstalling an execd).
 *        </td>
 *    </tr>
 *  </table>
 *   
 *    @see ResourceReassignmentDescriptor
 *    @see ResourceRemovalFromSystemDescriptor
 *    @see ResourcePurgeDescriptor
 */
public interface ResourceRemovalDescriptor {
    
}
