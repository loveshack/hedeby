/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.descriptor;

import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.util.I18NManager;
import java.io.Serializable;

/**
 * This <code>ResourcePurgeDescriptor</code> can be used to signal to
 * a service adapter that a resource will be purged from the system after it 
 * has been release by the service.
 * 
 * <p>Purging a resource means that the service should <b>NOT</b> do the usual
 * checks before unassigning (is resource static? is resource state correct?)
 * and should <b>NOT</b> do any actions to uninstall the resource (like e.g.
 * uninstalling an execd).
 */
public class ResourcePurgeDescriptor implements ResourceRemovalDescriptor, Serializable {
    
    private final static long serialVersionUID = -200910160L;
    private static final String BUNDLE = "com.sun.grid.grm.service.descriptor.service_descriptor";
    
    private final static ResourcePurgeDescriptor INSTANCE = new ResourcePurgeDescriptor();
    
    private ResourcePurgeDescriptor() {
        // empty
    }

    /**
     * Get the singleton instance of the ResourcePurgeDescriptor.
     * @return the singleton instance of the ResourcePurgeDescriptor
     */
    public static ResourcePurgeDescriptor getInstance() {
        return INSTANCE;
    }

    @Override
    public String toString() {
        return I18NManager.formatMessage("purge.toString", BUNDLE);
    }
}
