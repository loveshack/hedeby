/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.descriptor;

import com.sun.grid.grm.util.I18NManager;
import java.io.Serializable;

/**
 *  Instances of this class are used to signalize an service adapter that a 
 *  resource will be assigned to a different service after the service has released
 *  the resource
 */
public class ResourceReassignmentDescriptor extends AbstractForcedResourceRemovalDescriptor 
    implements Serializable {
    
    private final static long serialVersionUID = -200903100L;
    private static final String BUNDLE = "com.sun.grid.grm.service.descriptor.service_descriptor";

    /**
     * The default instance of the ResourceReassignmentDescriptor
     * If the {@link ServiceBase#removeResource}
     * is called with this instance the resource should be removed unforced.
     */
    public final static ResourceReassignmentDescriptor INSTANCE =
              new ResourceReassignmentDescriptor(false);
    
    /**
     * The forced instance of of the ResourceReassignmentDescriptor.
     * If the {@link ServiceBase#removeResource}
     * is called with this instance the resource should be removed forced.
     */
    public final static ResourceReassignmentDescriptor FORCED_INSTANCE =
             new ResourceReassignmentDescriptor(true);
    
    /**
     * Get the instance of the ResourceReassignmentDescriptor
     * @param forced forced of unforced instance
     * @return the instance of the ResourceReassignmentDescriptor
     */
    public static ResourceReassignmentDescriptor getInstance(boolean forced) {
        return forced ? FORCED_INSTANCE: INSTANCE;
    }
    
    private  ResourceReassignmentDescriptor(boolean forced) {
        super(forced);
    }
    
    @Override
    public String toString() {
        return isForced() ? I18NManager.formatMessage("reassignment.forced.toString", BUNDLE)
                : I18NManager.formatMessage("reassignment.toString", BUNDLE);
    }
}
