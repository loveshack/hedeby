/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service;

import java.util.List;
import java.util.Set;

/**
 * Service manager defines the operation which are needed for "registering/unregistering"
 * the service to be managed by Resource Provider or Reporter.
 * 
 * Implementing class should decide how to deal with communication with remote
 * services, how to store the services, rely events etc.
 * 
 * The interface is not supposed to be used by administrator, it should be used
 * exclusively by ResourceProvider/Reporter internal mechinsm.
 *
 * @param <T> implementing class type of managed services
 */
public interface ServiceManager<T extends Service> {

    /**
     * Adds a service to the resource provider. 
     *
     * @param service service to be managed
     * @return true if service was addded
     * @throws com.sun.grid.grm.service.InvalidServiceException if service can not
     * be added
     */
    boolean addService(T service) throws InvalidServiceException;

    /**
     * Gets a service. 
     *
     * @param name name of the service
     * @return the service instance (can be null)
     * @throws com.sun.grid.grm.service.UnknownServiceException if the service
     * with specified name was not found
     */
    T getService(String name) throws UnknownServiceException;

    /**
     * Removes a service from the resource provider.  This method has no
     * direct effect on the removed service.
     * 
     * @param name name of the service
     * @return true is service was removed
     * @throws UnknownServiceException thrown if the named service does not exist
     */
    boolean removeService(String name) throws UnknownServiceException;

    /**
     * Gets a list of configured services.
     * 
     * @return a list of configured services
     */
    List<T> getServices();

    /**
     * Gets a set of configured service names.
     * 
     * @return a list of configured service names
     */
    Set<String> getServiceNames();

    /**
     * Starts the service manager.
     */
    public void start();

    /**
     * Stops the service manager.
     */
    public void stop();
}
