/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.event;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.util.EventListenerSupport;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Helper class for firing the SLO manager events.
 */
public class SLOManagerEventSupport {
    private final static String BUNDLE = "com.sun.grid.grm.service.slo.event.messages";
    private static Logger log = Logger.getLogger(SLOManagerEventSupport.class.getName(),
            BUNDLE);
    private final EventListenerSupport<SLOManagerEventListener> listeners;
    private final AtomicLong eventSequenceNumber;

    public SLOManagerEventSupport() {
        listeners = EventListenerSupport.<SLOManagerEventListener>newSynchronInstance(SLOManagerEventListener.class);
        eventSequenceNumber = new AtomicLong();
    }

    /**
     * Register a <code>SLOManagerEventListener</code>.
     *
     * @param eventListener the management listener
     */
    public void addSLOManagerEventListener(
        SLOManagerEventListener eventListener) {
        listeners.addListener(eventListener);
    }

    /**
     * Deregister a <code>SLOManagerEventListener</code>.
     *
     * @param eventListener the management listener
     */
    public void removeSLOManagerEventListener(
        SLOManagerEventListener eventListener) {
        listeners.removeListener(eventListener);
    }

    /**
     * Fire a slo updated event.
     * @param slo the slo name that triggered the event
     * @param needs the list of needs that was created by slo
     */
    public void fireSLOUpdated(String slo, List<Need> needs) {
        SLOUpdatedEvent evt = new SLOUpdatedEvent(eventSequenceNumber.incrementAndGet(),
                slo, needs);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().sloUpdated(evt);
    }

    /**
     * Fire a resource added event.
     * @param e exception that occured during the update
     */
    public void fireSLOError(GrmException e) {
        SLOErrorEvent evt = new SLOErrorEvent(eventSequenceNumber.incrementAndGet(),
                e);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().sloUpdateError(evt);
    }

    /**
     * Fire a remove resource event
     * @param aUsageMap usage map of resources
     */
    public void fireUsageMapUpdated(Map<Resource, Usage> aUsageMap) {
        UsageMapUpdatedEvent evt = new UsageMapUpdatedEvent(eventSequenceNumber.incrementAndGet(),
                aUsageMap);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().usageMapUpdated(evt);
    }
}
