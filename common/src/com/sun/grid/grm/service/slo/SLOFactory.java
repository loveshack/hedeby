/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo;

import com.sun.grid.grm.config.common.FixedUsageSLOConfig;
import com.sun.grid.grm.config.common.MinResourceSLOConfig;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.service.slo.impl.FixedUsageSLO;
import com.sun.grid.grm.service.slo.impl.MinResourceSLO;
import com.sun.grid.grm.service.slo.impl.PermanentRequestSLO;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Factory class for SLOs.
 *
 * Default SLOFactory supports the following SLOs:
 *
 * <ul>
 *     <li>FixedUsageSLO</li>
 *     <li>MinResourceSLO</li>
 *     <li>PermanentRequestSLO</li>
 * </ul>
 */
public class SLOFactory {
    private final static String BUNDLE = "com.sun.grid.grm.service.slo.messages";
    
    static final int DEFAULT_CAPACITY = 16;
    
    private static SLOFactory defaultInstance;

    private final Map<Class<? extends SLOConfig>,Class<? extends SLO>> sloMap;
        
    private final SLOFactory parent;
    
    /**
     *  Create an empty SLOFactory.
     */
    public SLOFactory() {
        this(DEFAULT_CAPACITY);
    }
    
    /**
     *  Create an empty SLOFactory with a parent factory.
     *
     *  @param parent the parent factory
     */
    public SLOFactory(SLOFactory parent) {
        this(DEFAULT_CAPACITY, parent);
    }
    
    /**
     * Creates a SLOFactory with a defined capacity.
     *
     * @param defaultCapacity the default capacity of the SLOFactory
     */
    public SLOFactory(int defaultCapacity) {
        this(defaultCapacity, null);
    }

    
    /**
     * Create a SLOFactory with a default capacity and a parent SLOFactory.
     *
     * @param defaultCapacity the default capacity
     * @param aParent  the parent factory
     */
    public SLOFactory(int defaultCapacity, SLOFactory aParent) {
        parent = aParent;
        sloMap = new HashMap<Class<? extends SLOConfig>,Class<? extends SLO>>(defaultCapacity);
    }
    
    /**
     * Get the default SLOFactory for a Hedeby system.
     * @return the default SLOFactory
     */
    public static synchronized SLOFactory getDefault() {
        if(defaultInstance == null) {
            defaultInstance = new SLOFactory();
            defaultInstance.registerSLO(FixedUsageSLOConfig.class, FixedUsageSLO.class);
            defaultInstance.registerSLO(MinResourceSLOConfig.class, MinResourceSLO.class);
            defaultInstance.registerSLO(PermanentRequestSLOConfig.class, PermanentRequestSLO.class);
        }
        return defaultInstance;
    }
    
    
    /**
     * Register a new SLO.
     *
     * @param configClass the configuration class of the SLO
     * @param implClass  the implementation class of the SLO
     */
    protected void registerSLO(Class<? extends SLOConfig> configClass, Class<? extends SLO> implClass) {
        sloMap.put(configClass, implClass);
    }
    
    /**
     * Create a new instanceof of a SLO.
     * @param config   configuration of the SLO
     * @throws com.sun.grid.grm.service.slo.SLOException if the SLO could not be created
     * @return the SLO
     */
    public SLO createSLO(SLOConfig config) throws SLOException {
        Class<? extends SLO> clazz = sloMap.get(config.getClass());
        SLO ret = null;
        if(clazz != null) {
            try {
                Constructor<? extends SLO> cons = clazz.getConstructor(config.getClass());
                ret = cons.newInstance(config);
            } catch (InvocationTargetException ex) {
                throw new SLOException("slo.error", ex.getTargetException(), BUNDLE, config.getName(), config.getClass());
            } catch (Exception ex) {
                throw new SLOException("slo.error", ex, BUNDLE, config.getName(), config.getClass());
            }
        } else if (parent != null) {
            ret = parent.createSLO(config);
        }
        return ret;
    }
}
