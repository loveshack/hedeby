/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.service.slo.SLOContextFactory;
import com.sun.grid.grm.util.TimeIntervalHelper;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  Default implementation of a SLOManager that is ready to be used as a thread.
 *
 */
public class RunnableSLOManager extends DefaultSLOManager implements Runnable {

    private final static String BUNDLE = "com.sun.grid.grm.service.slo.impl.messages";
    private final static Logger log = Logger.getLogger(RunnableSLOManager.class.getName(), BUNDLE);
    
    
    /** update interval in millis */
    private long     updateInterval;
    
    /**
     * This flag signals that the run method is active
     */
    private boolean  isActive;
    
    /**
     * This flag signals that a SLO run should be executed despite
     * the wait time has not elapsed.
     * @see #triggerUpdate() 
     */
    private boolean  updateTriggered;
    
    /**
     * This flag signals that the run method should exit.
     * @see #shutdown()
     */
    private boolean  gotShutdown;
    
    
    /**
     * Create a new RunnableSLOManager which executes an SLO update every 5 minutes.
     * 
     * @param serviceName The name of the service (for logging)
     * @param sloContextFactory  the SLOContext factory
     */
    public RunnableSLOManager(String serviceName, SLOContextFactory sloContextFactory) {
        this(serviceName, sloContextFactory, 5 * 60 * 1000);
    }
    
    /**
     * Create a new RunnableSLOManager which executes the SLO update in the specified
     * update interval.
     * 
     * @param serviceName The name of the service (for logging)
     * @param sloContextFactory  the SLOContext factory
     * @param updateInterval the update interval in millis
     */
    public RunnableSLOManager(String serviceName, SLOContextFactory sloContextFactory, long updateInterval) {
        super(serviceName, sloContextFactory);
        this.updateInterval = updateInterval;
    }

    /**
     * Trigger a new run of the SLOManager.
     * 
     * <p>If an SLO calculation is in progress the call of this method has no
     * effect.</p>
     */
    public synchronized void triggerUpdate() {
        updateTriggered = true;
        notify();
    }
    
    /**
     * Modify the update interval for the SLO runs.
     * 
     * @param value  the update interval in millis
     */
    public synchronized void setUpdateInterval(long value) {
        this.updateInterval = value;
    }
    
    /**
     * Shutdown the current SLO manager thread. An ongoing SLO calculation
     * is interrupted.
     */
    public void shutdown() {
        log.entering("RunnableSLOManager", "shutdown");
        interrupt();
        synchronized(this) {
            if (isActive) {
                gotShutdown = true;
                notify();
            }
        }        
        log.exiting("RunnableSLOManager", "shutdown");
    }

    /**
     *   Implementing the Runnable interface makes it possible to
     *   start it in a Thread
     */
    public void run() {
        log.entering("RunnableSLOManager", "run");
        synchronized(this) {
            isActive = true;
            gotShutdown = false;
            updateTriggered = false;
        }
        try {
            while (true) {
                if (Thread.interrupted()) {
                    throw new InterruptedException();
                }
                // Run the update before waiting
                // Otherwise we can have a big delay for the initial
                // SLO run
                update();
                synchronized(this) {
                    // Wait until the slo update interval is reached
                    if(gotShutdown == true) {
                        break;
                    }
                    if(updateTriggered == false) {
                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "rslo.wait", new Object [] { getServiceName(), TimeIntervalHelper.toString(updateInterval) });
                        }
                        wait(updateInterval);
                    }
                    updateTriggered = false;
                    
                    if(gotShutdown == true) {
                        break;
                    }
                }
            }
            if (log.isLoggable(Level.FINE)) {
                if (gotShutdown) {
                    log.log(Level.FINE, "rslo.shutdown", getServiceName());
                }
            }
        } catch (InterruptedException ex) {
            log.log(Level.FINE, "rslo.interrupted", getServiceName());
        } finally {
            synchronized(this) {
                isActive = false;
                gotShutdown = false;
                updateTriggered = false;
            }
        }
        log.exiting("RunnableSLOManager", "run");
    }
    
}
