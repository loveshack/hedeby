/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.slo.SLOState;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Default implementation of the SLOState.
 */
public class DefaultSLOState implements SLOState, Serializable {

    private final static long serialVersionUID = -2007121701L;
    
    private final Map<Resource, Usage> usageMap;
    private final List<Need> needs;
    private final String name;

    /**
     * Create a new <code>DefaultSLOState</code>
     * @param sloName the name of the SLO
     * @param aUsageMap the usage map
     * @param aNeeds the produced needs
     */
    public DefaultSLOState(String sloName, Map<Resource, Usage> aUsageMap, List<Need> aNeeds) {
        name = sloName;
        usageMap = Collections.unmodifiableMap(new HashMap<Resource, Usage>(aUsageMap));
        needs = Collections.unmodifiableList(new ArrayList<Need>(aNeeds));
    }
    
    /**
     * Create new SLOState which has no needs. 
     * @param sloName the name of the SLO
     * @return the SLOState without needs
     */
    public static SLOState createSLOStateWithNoNeed(String sloName) {
        return new DefaultSLOState(sloName, 
                                   Collections.<Resource,Usage>emptyMap(), 
                                   Collections.singletonList(Need.EMPTY_NEED));
    }

    /**
     * Get the usage map
     * @return the usage map (unmodifiable map)
     */
    public Map<Resource, Usage> getUsageMap() {
        return usageMap;
    }

    /**
     * Get the needs.
     * @return list of needs (unmodifiable list)
     */
    public List<Need> getNeeds() {
        return needs;
    }

    /**
     * Get the SLO name
     * @return the slo name
     */
    public String getSloName() {
        return name;
    }
    
    /**
     * has the slo needs
     * @return <code>true</code> if the slo has any need
     */
    public boolean hasNeeds() {
        for(Need need: needs) {
            if(need.getQuantity() > 0) {
                return true;
            }
        }
        return false;
    }
    
}
