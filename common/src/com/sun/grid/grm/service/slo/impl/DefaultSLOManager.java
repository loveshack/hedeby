/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.slo.InvalidatedSLOContextException;
import com.sun.grid.grm.service.slo.SLO;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.SLOContextFactory;
import com.sun.grid.grm.service.slo.SLOManager;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.service.slo.event.SLOManagerEventListener;
import com.sun.grid.grm.service.slo.event.SLOManagerEventSupport;

import com.sun.grid.grm.util.I18NManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  Default implementation of a SLOManager.
 *
 */
public class DefaultSLOManager implements SLOManager {

    private final static String BUNDLE = "com.sun.grid.grm.service.slo.impl.messages";
    private final static Logger log = Logger.getLogger(DefaultSLOManager.class.getName(), BUNDLE);

    /** 
     * The state of the DefaultSLOManager signals that an SLO calculation is
     * on going or not. The following state transitions are possible.
     * 
     *      IDLE < ------+
     *       |           |
     *     update   update finished
     *       |           |
     *       v           |
     *     RUNNING  -----+
     *       |           |
     * interruptSLOcalc  |
     *       |           |
     *       v           |    
     *     INTERRUPTED   |
     *       +-----------+
     * 
     */
    private enum State {
        IDLE,        // No SLO calculation is ongoing
        RUNNING,     // SLO calculation is ongoing
        INTERRUPTED  // Ongoing SLO calculation has been interrrupted
    }
    
    /**
     * If this flag is set to false the SLOManager does not produce any SLO events.
     * 
     * @see #disable() 
     * @see #enable() 
     */
    private boolean isEnabled = true;
    
    /** Mainly used for logging */
    private final String serviceName;
    
    private final SLOManagerEventSupport evtSupport = new SLOManagerEventSupport();
    
    private final Lock lock = new ReentrantLock();
    private final Condition sloRunFinishedCondition = lock.newCondition();
    private final Map<String, SLOState> sloStateMap = new HashMap<String, SLOState>();
    private final SLOContextFactory sloContextFactory;
    private SLOContext sloContext;
    private State state = DefaultSLOManager.State.IDLE;
    protected List<SLO> slos = Collections.<SLO>emptyList();

    /**
     * Create a new DefaultSLOManager
     * @param serviceName The name of the service (for logging)
     * @param sloContextFactory the SLOContext factory
     */
    public DefaultSLOManager(String serviceName, SLOContextFactory sloContextFactory) {
        this.sloContextFactory = sloContextFactory;
        this.serviceName = serviceName;
    }

    /**
     * Add a SLO listener
     * @param lis  the listener
     */
    public void addSLOManagerListener(SLOManagerEventListener lis) {
        log.entering(DefaultSLOManager.class.getName(), "addSLOManagerListener", lis);
        evtSupport.addSLOManagerEventListener(lis);
        log.exiting(DefaultSLOManager.class.getName(), "addSLOManagerListener");
    }

    /**
     * Remove a SLO listener
     * @param lis  the listener
     */
    public void removeSLOManagerListener(SLOManagerEventListener lis) {
        log.entering(DefaultSLOManager.class.getName(), "removeSLOManagerListener", lis);
        evtSupport.removeSLOManagerEventListener(lis);
        log.exiting(DefaultSLOManager.class.getName(), "removeSLOManagerListener");
    }

    /**
     * set the list of managed SLOs.
     *
     * @param sloList  list of SLOs
     */
    public void setSLOs(List<SLO> sloList) {
        log.entering(DefaultSLOManager.class.getName(), "setSLOs", sloList);
        lock.lock();
        try {
            slos = new ArrayList<SLO>(sloList);
        } finally {
            lock.unlock();
        }
        log.exiting(DefaultSLOManager.class.getName(), "setSLOs");
    }

    /**
     * Get the states of the SLOs
     * @return list with the states of the SLOS
     */
    public List<SLOState> getSLOStates() {
        log.entering(DefaultSLOManager.class.getName(), "getSLOStates");
        List<SLOState> ret = null;
        lock.lock();
        try {
            ret = new ArrayList<SLOState>(sloStateMap.values());
        } finally {
            lock.unlock();
        }
        log.exiting(DefaultSLOManager.class.getName(), "getSLOStates", ret);
        return ret;
    }

    /**
     *  Determine if this SLOManager manages a specific SLO type.
     *
     *  @param  type the type
     *  @return <code>true</code> if in the SLO list is a slo which
     *             is assignable from <code>type</code>
     */
    public boolean hasSLO(Class<? extends SLO> type) {
        log.entering(DefaultSLOManager.class.getName(), "hasSLO", type);
        List<SLO> tmpSlos = null;
        boolean ret = false;
        lock.lock();
        try {
            tmpSlos = slos;
        } finally {
            lock.unlock();
        }

        for (SLO slo : tmpSlos) {
            if (type.isAssignableFrom(slo.getClass())) {
                ret = true;
                break;
            }
        }
        log.exiting(DefaultSLOManager.class.getName(), "hasSLO", ret);
        return ret;
    }

    /**
     * interrupt the current SLO calculation
     * 
     * @return <code>true</code> if the call has interrupted the SLO update run
     */
    private boolean interruptSLOcalc() {
        log.entering(DefaultSLOManager.class.getName(), "interruptSLOcalc");
        boolean ret = false;
        SLOContext tmpCtx = null;
        lock.lock();
        try {
            switch (state) {
                case INTERRUPTED: // Fall through
                case IDLE:
                    ret = false;
                    break;
                case RUNNING:
                    state = State.INTERRUPTED;
                    ret = true;
                    break;
                default:
                    throw new IllegalStateException("Unknown state " + state);
            }
            tmpCtx = sloContext;
        } finally {
            lock.unlock();
        }
        if (tmpCtx != null) {
            tmpCtx.invalidate();
        }
        log.exiting(DefaultSLOManager.class.getName(), "interruptSLOCalc", ret);
        return ret;
    }

    /**
     * Get the name of the owning service (mainly used for logging)
     * @return
     */
    protected String getServiceName() {
        return serviceName;
    }
    
    /**
     * Interrupt the current SLO calculation and wait for the until it is finished
     * @throws java.lang.InterruptedException if the calling thread has been interrupted
     */
    private void interruptSLOCalcAndWait() throws InterruptedException {
        interruptSLOcalc();
        waitForSLORunFinished();
    }

    /**
     * Wait until the current SLO run is finished
     * @throws java.lang.InterruptedException if the calling thread has been interrupted
     */
    private void waitForSLORunFinished() throws InterruptedException {
        lock.lock();
        try {
            while (state != State.IDLE) {
                sloRunFinishedCondition.await();
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * This method performs a regular SLO calculation (SLOManager has at least
     * one SLO).
     * 
     * The calculation is divided into two blocks.
     * 
     * Block 1: Call the update method of each SLO. This block can be interrupted
     *          if the SLOContext is invalidated
     * 
     * Block 2: Deliver the SLO events. This block can not be interrupted
     * 
     * @param tmpSlos  the list of SLOs
     * @param mySloContext  the SLOContext for the calculation
     * @throws com.sun.grid.grm.GrmException if the SLOContext has been invalidated or an SLO#update
     *                      method has thrown an Exception
     */
    private void performSLOCalculation(List<SLO> tmpSlos, SLOContext mySloContext) throws GrmException {
        Map<Resource, Usage> usageMap = new HashMap<Resource, Usage>();
        Map<String, SLOState> tmpSloStateMap = new HashMap<String, SLOState>(tmpSlos.size());
        List<SLOState> cancelRequests = new LinkedList<SLOState>();

        for (SLO slo : tmpSlos) {
            mySloContext.validate();
            SLOState sloState = slo.update(mySloContext);

            if (usageMap.isEmpty()) {
                usageMap.putAll(sloState.getUsageMap());
            } else {
                for (Map.Entry<Resource, Usage> entry : sloState.getUsageMap().entrySet()) {
                    Usage oldUsage = usageMap.get(entry.getKey());

                    if ((oldUsage == null) || (oldUsage.getLevel() < entry.getValue().getLevel())) {
                        usageMap.put(entry.getKey(), entry.getValue());
                    }
                }
            }
            tmpSloStateMap.put(slo.getName(), sloState);
        }

        // Beyond this point this point the SLO calculation can not
        // be interrupted
        lock.lock();
        try {
            for (SLOState pendingSLOState : sloStateMap.values()) {
                SLOState newSLOState = tmpSloStateMap.get(pendingSLOState.getSloName());
                // If we had pending need create a empty need and report it
                if (newSLOState == null && pendingSLOState.hasNeeds()) {
                    cancelRequests.add(DefaultSLOState.createSLOStateWithNoNeed(pendingSLOState.getSloName()));
                }
            }
            sloStateMap.clear();
            for (SLOState sloState : tmpSloStateMap.values()) {
                sloStateMap.put(sloState.getSloName(), sloState);
            }
        } finally {
            lock.unlock();
        }
        
        for (SLOState sloState : tmpSloStateMap.values()) {
            evtSupport.fireSLOUpdated(sloState.getSloName(), sloState.getNeeds());
        }
        sendCancelRequests(cancelRequests);
        evtSupport.fireUsageMapUpdated(usageMap);
    }
    
    /**
     * This method produces the cancel request for all SLOStates which have been
     * created by the last SLO run.
     * 
     * This method is only called if the SLOManager has no SLOs configured!
     */
    private void sendCancelRequestsForLastSLOStates() {
        // If we have pending resource request we have to send cancel requests
        List<SLOState> cancelRequests = null;
        lock.lock();
        try {
            if (sloStateMap.size() > 0) {
                cancelRequests = new ArrayList<SLOState>(sloStateMap.size());
                for (SLOState pendingSLOState : sloStateMap.values()) {
                    if (pendingSLOState.hasNeeds()) {
                        cancelRequests.add(DefaultSLOState.createSLOStateWithNoNeed(pendingSLOState.getSloName()));
                    }
                }
                sloStateMap.clear();
            }
        } finally {
            lock.unlock();
        }
        if (cancelRequests != null) {
            sendCancelRequests(cancelRequests);
            evtSupport.fireUsageMapUpdated(Collections.<Resource, Usage>emptyMap());
        }
    }
    
    private void sendCancelRequests(List<SLOState> cancelSLOStates) {
        for (SLOState sloState : cancelSLOStates) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "cancelRequest", new Object[]{serviceName, sloState.getSloName()});
            }
            evtSupport.fireSLOUpdated(sloState.getSloName(), sloState.getNeeds());
        }
    }
    
    /**
     * Updates each SLO.
     * 
     * while this method is active the state of the SLOManager is RUNNING. If
     * <code>interruptedSLOcalc</code> has been called while this method is invoked
     * the state of the SLOManager is INTERRUPTED. 
     * This method sets finally the state back to IDLE.
     */
    public final void update() {
        log.entering(DefaultSLOManager.class.getName(), "update");
        try {
            List<SLO> tmpSlos = null;
            lock.lock();
            try {
                if (!isEnabled) {
                    log.log(Level.FINE, "update.disabled", serviceName);
                    log.exiting(DefaultSLOManager.class.getName(), "update");
                    return;
                }
                tmpSlos = slos;
            } finally {
                lock.unlock();
            }

            interruptSLOcalc();
            
            lock.lock();
            try {
                if (!isEnabled) {
                    log.log(Level.FINE, "update.disabled", serviceName);
                    log.exiting(DefaultSLOManager.class.getName(), "update");
                    return;
                }
                waitForSLORunFinished();
                // current is now the thread which will
                // perform the next slo calculation
                state = State.RUNNING;
            } catch (InterruptedException ex) {
                throw new InvalidatedSLOContextException(ex);
            } finally {
                lock.unlock();
            }

            if ((tmpSlos != null) && !tmpSlos.isEmpty()) {
                // creation of slo context must not run in synchronized block
                SLOContext mySloContext = sloContextFactory.createSLOContext();

                // If the SLO run has been interrupted during the creation
                // of the SLOContext the stateBeforeSettingNewSLOContext will
                // have the value INTERRUPTED
                State stateBeforeSettingNewSLOContext;
                lock.lock();
                try {
                    stateBeforeSettingNewSLOContext = state;
                    sloContext = mySloContext;
                } finally {
                    lock.unlock();
                }
                if (stateBeforeSettingNewSLOContext != State.RUNNING) {
                    // Interrupt during startup phase occured
                    // => Invalid the new SLOContext
                    mySloContext.invalidate();
                }
                performSLOCalculation(tmpSlos, sloContext);
            } else {
                log.log(Level.FINE, "noSLOs", serviceName);
                sendCancelRequestsForLastSLOStates();
            }
        } catch (GrmException ex) {
            evtSupport.fireSLOError(ex);
        } catch (Exception ex) {
            evtSupport.fireSLOError(new GrmException("ex.slo", ex, BUNDLE, ex.getLocalizedMessage()));
        } finally {
            lock.lock();
            try {
                sloContext = null;
                state = State.IDLE;
                sloRunFinishedCondition.signalAll();
            } finally {
                lock.unlock();
            }
        }
        log.exiting(DefaultSLOManager.class.getName(), "update");
    }

    /**
     * Enable the SLOManager
     */
    public void enable() {
        lock.lock();
        try {
            isEnabled = true;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Disable the SLOManager
     */
    public void disable() {
        log.entering(DefaultSLOManager.class.getName(), "disable");
        lock.lock();
        try {
            isEnabled = false;
            interruptSLOCalcAndWait();
        } catch (InterruptedException ex) {
            log.log(Level.SEVERE, I18NManager.formatMessage("iterr", BUNDLE, serviceName), ex);
        } finally {
            lock.unlock();
        }
        log.exiting(DefaultSLOManager.class.getName(), "disable");
    }
    
    @SuppressWarnings("deprecation")
    public boolean interrupt() {
        return interruptSLOcalc();
    }

    @SuppressWarnings("deprecation")
    public void interruptAndWait() throws InterruptedException {
        interruptSLOCalcAndWait();
    }
}
