/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.slo;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.filter.Filter;
import java.util.List;

/**
 * The SLOContext provides the information for a SLO run.
 * 
 * <p>A <code>SLOContext</code> can be invalidated. This is a signal for the 
 *    <code>SLOManager</code> data of this SLOContext are not longer valid. The
 *    SLO calculation can be stopped.</p>
 */
public interface SLOContext {

    /**
     *   Get the list of resources which should be considered for the SLO run
     *   @return list of resources
     *   @throws InvalidatedSLOContextException if the SLOContext has been invalidated
     */
    public List<Resource> getResources() throws InvalidatedSLOContextException;
    
    /**
     *   Get the list of resources matching against the <code>filter</code>
     *   which should be considered for the SLO run.
     *    
     *   @param filter   the resource filter
     *   @return list of resource
     *   @throws InvalidatedSLOContextException if the SLOContext has been invalidated
     */
    public List<Resource> getResources(Filter<Resource> filter) throws InvalidatedSLOContextException;
    
    /**
     * invalidate the SLOContext
     * @return <code>true</code> if the call has invalidated the SLOContext or <code>false</code>
     *         if the SLOContext has already been invalidated
     */
    public boolean invalidate();
    
    /**
     * Determine if the SLOContext is valid
     * @return <code>true</code> if the SLOContext is valid
     */
    public boolean isValid();
    
    /**
     *  Validate the SLOContext
     *   @throws InvalidatedSLOContextException if the SLOContext has been invalidated
     */
    public void validate() throws InvalidatedSLOContextException;
    
}
