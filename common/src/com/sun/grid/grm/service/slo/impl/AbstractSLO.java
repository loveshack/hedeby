/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.slo.SLO;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterException;
import com.sun.grid.grm.util.filter.FilterHelper;


/**
 * Abstract base class for SLO
 */
public abstract class AbstractSLO implements SLO {
    private final SLOConfig config;
    private final Usage urgency;
    private final Usage usage;
    private final Filter<Resource> requestFilter;
    private final Filter<Resource> resourceFilter;
    
    /**
     * Creates a new instance of AbstractSLO.
     *
     * @param aConfig   the configuration of the SLO
     * @throws com.sun.grid.grm.util.filter.FilterException if the resourceFilter is not parseable
     */
    public AbstractSLO(SLOConfig aConfig) throws FilterException {
        config = aConfig;
        urgency = new Usage(aConfig.getUrgency());

        if (aConfig.isSetUsage()) {
            usage = new Usage(aConfig.getUsage());
        } else {
            usage = new Usage(aConfig.getUrgency());
        }
        if (aConfig.isSetRequest()) {
            requestFilter = FilterHelper.parse(aConfig.getRequest());
        } else {
            requestFilter = ConstantFilter.<Resource>alwaysMatching();
        }
        if (aConfig.isSetResourceFilter()) {
            resourceFilter = FilterHelper.parse(aConfig.getResourceFilter());
        } else {
            resourceFilter = ConstantFilter.<Resource>alwaysMatching();
        }
    }

    /**
     * Get the name of the SLO.
     *
     * @return the name of the SLO
     */
    public String getName() {
        return config.getName();
    }

    /**
     * Get the configuration of the SLO
     * @return the configuration of the SLO
     */
    protected SLOConfig getConfig() {
        return config;
    }

    /**
     * Get the urgency of the SLO.
     * @return the urgency of the SLO
     */
    public Usage getUrgency() {
        return urgency;
    }

    /**
     * Get the usage that the SLO assigns to resources
     * @return the usage of the SLO
     */
    public Usage getUsage() {
        return usage;
    }
    /**
     * Get the request filter of this SLO
     * @return the request filter
     */
    public Filter<Resource> getRequestFilter() {
        return requestFilter;
    }
    
    /**
     * Get the resource filter of this SLO
     * @return the request filter
     */
    public Filter<Resource> getResourceFilter() {
        return resourceFilter;
    }
    
    /**
     * Create a need for this SLO out of the request configuration.
     *
     * @param quantity quantity for the need
     * @return the need
     */
    protected Need createNeed(int quantity) {
        return new Need(requestFilter, getUrgency(), quantity);
    }

    @Override
    public String toString() {
        return String.format("SLO[%s,req=%s,resFilter=%s]", getName(), getRequestFilter(), getRequestFilter());
    }
}
