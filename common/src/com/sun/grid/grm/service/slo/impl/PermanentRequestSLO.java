/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This SLO is never satisfied with resources that matches a provided resource filter.
 *
 * <p>Need is produced everytime an update is called.</p>
 * <p>The usage map of this SLO contains <code>min</code> assigned resource. They all have
 *    as usage the urgency of the SLO.</p>
 *
 */
public class PermanentRequestSLO extends AbstractSLO {

    /**
     * Creates a new instance of PermanentRequestSLO.
     *
     * @param aConfig   the configuration
     * @throws com.sun.grid.grm.util.filter.FilterException if the resource filter is not correct
     */
    public PermanentRequestSLO(PermanentRequestSLOConfig aConfig) throws FilterException {
        super(aConfig);
    }

    private int getQuantity() {
        return ((PermanentRequestSLOConfig)getConfig()).getQuantity();
    }
    /**
     * Update the SLO.
     *
     * @param  ctx the SLOContext
     * @throws com.sun.grid.grm.GrmException if the update of the SLO failed
     * @return the SLO state
     */
    public SLOState update(SLOContext ctx) throws GrmException {
        List<Resource> resources = ctx.getResources();
        Map<Resource, Usage> usageMap = new HashMap<Resource, Usage>(resources.size());

        Filter<Resource> filter = getResourceFilter();
        ResourceVariableResolver filterSource = new ResourceVariableResolver();

        for (Resource resource : resources) {
            filterSource.setResource(resource);
            if (filter.matches(filterSource)) {
                usageMap.put(resource, getUsage());
            }
            ctx.validate();
        }

        List<Need> needs = Collections.singletonList(createNeed(getQuantity()));

        return new DefaultSLOState(getName(), usageMap, needs);
    }
}
