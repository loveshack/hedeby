/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.service.slo.InvalidatedSLOContextException;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.util.filter.Filter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

/**
 * Default implementation of the SLOContext
 */
public class DefaultSLOContext implements SLOContext {

    private final static Logger log = Logger.getLogger(DefaultSLOContext.class.getName());
    private List<Resource> resources;
    private final AtomicBoolean isValid;
    
    /**
     * Create a new DefaultSLOContext
     * @param resources snapshot of the resources
     */
    public DefaultSLOContext(List<Resource> resources) {
        log.entering(DefaultSLOContext.class.getName(), "<init>");
        this.resources = Collections.unmodifiableList(new ArrayList<Resource>(resources));
        isValid = new AtomicBoolean(true);
        log.exiting(DefaultSLOContext.class.getName(), "<init>");
    }
    
    /**
     *   Get the list of resources which should be considered for the SLO run
     *   @return list of resources
     *   @throws InvalidatedSLOContextException if the SLOContext has been invalidated
     */
    public List<Resource> getResources() throws InvalidatedSLOContextException {
        log.entering(DefaultSLOContext.class.getName(), "getResources");
        validate();
        log.exiting(DefaultSLOContext.class.getName(), "resources");
        return resources;
    }

    /**
     *   Get the list of resources matching against the <code>filter</code>
     *   which should be considered for the SLO run.
     *    
     *   @param filter   the resource filter
     *   @return list of resource
     *   @throws InvalidatedSLOContextException if the SLOContext has been invalidated
     */
    public List<Resource> getResources(Filter<Resource> filter) throws InvalidatedSLOContextException {
        log.entering(DefaultSLOContext.class.getName(), "getResources", filter);
        validate();
        List<Resource> ret = new LinkedList<Resource>();
        ResourceVariableResolver resolver = new ResourceVariableResolver();
        for(Resource r: resources) {
            resolver.setResource(r);
            if(filter.matches(resolver)) {
                ret.add(r);
            }
        }
        log.exiting(DefaultSLOContext.class.getName(), "getResources", ret);
        return ret;
    }

    /**
     * invalidate the SLOContext
     * @return <code>true</code> if the call has invalidated the SLOContext or <code>false</code>
     *         if the SLOContext has already been invalidated
     */
    public boolean invalidate() {
        log.entering(DefaultSLOContext.class.getName(), "invalidate");
        boolean ret = isValid.getAndSet(false);
        log.exiting(DefaultSLOContext.class.getName(), "invalidate", ret);
        return ret;
    }

    /**
     * Determine if the SLOContext is valid
     * @return <code>true</code> if the SLOContext is valid
     */
    public boolean isValid() {
        log.entering(DefaultSLOContext.class.getName(), "isValid");
        boolean ret = isValid.get();
        log.exiting(DefaultSLOContext.class.getName(), "isValid", ret);
        return ret;
    }

    /**
     *  Validate the SLOContext
     *   @throws InvalidatedSLOContextException if the SLOContext has been invalidated
     */
    public void validate() throws InvalidatedSLOContextException {
        log.entering(DefaultSLOContext.class.getName(), "validate");
        if(!isValid()) {
            InvalidatedSLOContextException ex = new InvalidatedSLOContextException();
            log.throwing(DefaultSLOContext.class.getName(), "validate", ex);
            throw ex;
        }
        log.exiting(DefaultSLOContext.class.getName(), "validate");
    }

}
