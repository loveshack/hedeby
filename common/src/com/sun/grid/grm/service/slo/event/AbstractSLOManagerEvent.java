/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.event;

import java.io.Serializable;


/**
 * AbstractSLOManagerEvent provides default implementation for operations that
 * are common to all slo events.
 */
public abstract class AbstractSLOManagerEvent implements Serializable,
    Comparable<AbstractSLOManagerEvent> {
    /**
     * The serial version UID
     */
    private static final long serialVersionUID = -2007111601L;
    private final long sequenceNumber;
    private transient int hash;

    /**
     * Creates a new instance of AbstractSLOManagerEvent
     * @param aSequenceNumber the sequence number for the event
     */
    public AbstractSLOManagerEvent(long aSequenceNumber) {
        sequenceNumber = aSequenceNumber;
    }

    /**
     * get the sequence number of the event
     * @return the sequence number
     */
    public long getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Compare with another slo event.
     *
     * @param o the other slo event
     * @return  a negative integer, zero, or a positive integer as this object is
     *          less than, equal to, or greater than the specified object.
     */
    public int compareTo(AbstractSLOManagerEvent o) {
        int ret = 0;

        if (ret == 0) {
            if (sequenceNumber < o.sequenceNumber) {
                ret = -1;
            } else if (sequenceNumber > o.sequenceNumber) {
                ret = 1;
            } else {
                ret = 0;
            }
        }

        return ret;
    }

    /**
     *  test if this is equal to a object.
     *
     *  @param obj the object
     *  @return <code>true</code> if this is equal to <code>object</code>
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AbstractSLOManagerEvent) {
            AbstractSLOManagerEvent e = (AbstractSLOManagerEvent) obj;

            return (sequenceNumber == e.sequenceNumber);
        }

        return false;
    }

    /**
     *  Get the hashcode of this object
     *
     *  @return the hashcode
     */
    @Override
    public int hashCode() {
        int h = hash;

        if (h == 0) {
            h = (int) (sequenceNumber ^ (sequenceNumber >>> 32));
            hash = h;
        }

        return h;
    }
}
