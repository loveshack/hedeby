/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo;

import com.sun.grid.grm.service.slo.event.SLOManagerEventListener;

import java.util.List;


/**
 *  Interface for a SLOManager
 */
public interface SLOManager {
    /**
     * Set the list of SLOs which are managed by this
     * SLOManager
     *
     * @param slos  list of SLOs
     */
    public void setSLOs(List<SLO> slos);

    /**
     * update the SLOs
     */
    public void update();

    /**
     * Interrupt the current SLO calculation
     * @return <code>true</code> if the call has interrupt a SLO run
     */
    public boolean interrupt();
    
    /**
     * Interrupt the current SLO calculation and wait for current run to be finished.
     * @deprecated Use {@link #enable()} and {@link #disable()} to enable/disable slo manager instead to interrupt slo calculation
     */
    @Deprecated
    public void interruptAndWait() throws InterruptedException;
    
    /**
     * Enable the SLOManager
     * 
     * The next call of the update method can produce SLO events
     */
    public void enable();
    
    /**
     * Disable the SLOManager
     * 
     * SLOManager no longer produces SLO events
     */
    public void disable();
    
    /**
     *  Determine if this SLOManager manages a specific SLO type.
     *
     *  @param  type the type
     *  @return <code>true</code> if in the SLO list is a slo which
     *             is assignable from <code>type</code>
     */
    public boolean hasSLO(Class<?extends SLO> type);
    
    /**
     * Get the states of the SLOs
     * @return list with the states of the SLOS
     */
    public List<SLOState> getSLOStates();    

    /**
     * Add a SLOManagerEventListener
     * @param lis  the listener
     */
    public void addSLOManagerListener(SLOManagerEventListener lis);

    /**
     * Add a SLOManagerEventListener
     * @param lis  the listener
     */
    public void removeSLOManagerListener(SLOManagerEventListener lis);
}
