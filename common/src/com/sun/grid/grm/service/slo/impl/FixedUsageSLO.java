/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.slo.impl;

import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.common.FixedUsageSLOConfig;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.slo.InvalidatedSLOContextException;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterException;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.SLOState;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  This SLO is usable for all kind of services. It gives each resource
 *  of the service a fixed usage. It will never produce any <code>Need</code>.
 * 
 *  <p>It is possible to filter the resources</p>
 */
public class FixedUsageSLO extends AbstractSLO {
    
    /**
     * Creates a new instance of FixedUsageSLO.
     * 
     * @param config  configuration of the SLO
     * @throws com.sun.grid.grm.util.filter.FilterException if the resource filter in 
     *                     <code>config</code> is invalid
     */
    public FixedUsageSLO(FixedUsageSLOConfig config) throws FilterException {
        super(config);
    }

    /**
     * Update this SLO.
     * 
     * @param  ctx the SLOContext
     * @return the state of the SLO
     * @throws com.sun.grid.grm.service.slo.InvalidatedSLOContextException of the ctx has been invalidated 
     */
    public SLOState update(SLOContext ctx) throws InvalidatedSLOContextException {
        List<Resource> resources = ctx.getResources();
        Map<Resource,Usage> usageMap = new HashMap<Resource,Usage>(resources.size());
        
        Filter<Resource> requestFilter = getResourceFilter();
        ResourceVariableResolver filterSource = new ResourceVariableResolver();

        for (Resource resource : resources) {
            filterSource.setResource(resource);
            if (requestFilter.matches(filterSource)) {
                usageMap.put(resource, getUsage());
            }
            ctx.validate();
        }
        return new DefaultSLOState(getName(), usageMap, Collections.<Need>emptyList());
    }
    
}
