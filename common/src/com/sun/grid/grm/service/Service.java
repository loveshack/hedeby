/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.GrmSingleton;
import com.sun.grid.grm.management.Manageable;
import com.sun.grid.grm.service.event.ServiceEventListener;


/**
 * This interface describes all method which musts be implemented
 * by a Hedeby component that represents a service.
 * 
 * <p>The name <code>Service</code> is not a good choice for this interface,
 *    Naming it directly <code>ServiceComponent</code> would be a better solution.
 *    However renaming is for compatibility reasons not possible.</p>
 * 
 */
@Manageable
public interface Service extends GrmComponent, GrmSingleton, ServiceBase {
    
    /**
     * Starts up the service, if it is not running yet
     *
     * @throws com.sun.grid.grm.GrmException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void startService() throws GrmException, GrmRemoteException;

    /**
     * Stops the service, if it is running. In case that
     * the service is stopped, one can specify to free the
     * resources with it. If the service is not running,
     * the isFreeResources has not effect.
     *
     * @param isFreeResources if true, frees all none static resources
     * @throws com.sun.grid.grm.GrmException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void stopService(boolean isFreeResources) throws GrmException, GrmRemoteException;
    
    /**
     * Registers an event listener to receive status reports and task finish
     * notifications.
     *
     * @param eventListener Registers the Service Container as an event listener
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    void addServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException;

    /**
     * Removes an event listener.
     *
     * @param eventListener EventListener to remove
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    void removeServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException;

}
