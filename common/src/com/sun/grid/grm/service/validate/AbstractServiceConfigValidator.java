/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.validate;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.AbstractServiceConfig;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.validate.GrmValidationException;
import com.sun.grid.grm.validate.Validation;
import com.sun.grid.grm.validate.Validator;
import com.sun.grid.grm.validate.ValidatorService;
import java.util.LinkedList;
import java.util.List;

/**
 * Validator for the configuration of services
 */
@Validation(type=AbstractServiceConfig.class)
public class AbstractServiceConfigValidator implements Validator<AbstractServiceConfig> {

    private final static String BUNDLE = "com.sun.grid.grm.service.validate.messages";
    
    public void validate(ExecutionEnv env, AbstractServiceConfig obj, AbstractServiceConfig old) throws GrmValidationException {
        
        List<String> errors = new LinkedList<String>();
        
        
        if (obj.getSlos() != null) {
            // Validate all SLOs
            for(SLOConfig slo: obj.getSlos().getSlo()) {
                try {
                    ValidatorService.validateChain(env, slo);
                } catch(GrmValidationException ex) {
                    for(String err: ex.getErrorMessages()) {
                        errors.add(I18NManager.formatMessage("AbstractServiceConfigValidator.slo", BUNDLE, slo.getName(), err));
                    }
                } catch(GrmException ex) {
                    String msg = ex.getLocalizedMessage();
                    if(msg == null || msg.length() == 0) {
                        msg = ex.getClass().getName();
                    }
                    errors.add(I18NManager.formatMessage("AbstractServiceConfigValidator.slo", BUNDLE, slo.getName(), msg));
                }
            }
        } 
        
        if(errors.size() > 0) {
            throw new GrmValidationException(errors);
        }
        
    }

}
