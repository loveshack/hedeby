/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.util.I18NManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The ResourceExecutor executes instances of ResourceAction.
 * 
 * <p>This class provides for each resource id a queue. Submitted actions are stored
 *    in the queue.</p>
 * <p>For each queue a worker is submitted to a ExecutorService (cached thread pool). The
 *    worker processes the pending actions for one resource</p>
 *
 * @param <K> type of the key of a resource
 */
class ResourceActionExecutor<K> {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.service";
    private final static Logger log = Logger.getLogger(ResourceActionExecutor.class.getName(), BUNDLE);
    private final String serviceName;
    private final ExecutorService executor;
    private final List<ResourceAction> activeActions = new LinkedList<ResourceAction>();
    private final Map<K, Queue<ResourceAction>> pendingActionMap = new HashMap<K, Queue<ResourceAction>>();
    private final Lock actionLock = new ReentrantLock();

    /**
     * Create a new instance of ResourceActionExecutor
     * @param serviceName The service name will part of the thread created by the ExecutorService
     *                    All logging messages contains the service name
     * @param cl  The context ClassLoader for the threads of the ExecutorService
     */
    public ResourceActionExecutor(final String serviceName, final ClassLoader cl) {
        this.serviceName = serviceName;
        this.executor = Executors.newCachedThreadPool(new ThreadFactory() {

            private final AtomicInteger instanceCount = new AtomicInteger();

            public Thread newThread(Runnable r) {
                Thread ret = new Thread(r, String.format("%s[resource%d]", serviceName, instanceCount.getAndIncrement()));
                ret.setContextClassLoader(cl);
                return ret;
            }
        });
    }

    /**
     * Stop the ResourceActionExecutor.
     * 
     * <p>This method performs the following steps</p>
     * <ul>
     *     <li>Shutdown the ExecutorService</li>
     *     <li>Cancel all pending actions by calling <code>ResourceAction#cancel</code></li>
     *     <li>Wait until all active action (running in the ExecutorService)
     * </ul>
     * @throws java.lang.InterruptedException  if waiting for the active actions has been interrupted
     */
    public void stop() throws InterruptedException {
        log.entering(ResourceActionExecutor.class.getName(), "stop");

        if (!executor.isTerminated()) {
            //no shutdown now as executor may do some remote calls are not interruptable at all (see issue 644)
            executor.shutdown();
            // Cancel all pending actions
            List<ResourceAction> cancelMap = new ArrayList<ResourceAction>();
            actionLock.lock();
            try {
                for (Queue<ResourceAction> queue : pendingActionMap.values()) {
                    for (ResourceAction action : queue) {
                        cancelMap.add(action);
                    }
                }
                pendingActionMap.clear();
            } finally {
                actionLock.unlock();
            }
            for (ResourceAction action : cancelMap) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "rae.cancel", new Object[]{serviceName, action});
                }
                action.cancel();
            }

            // Wait until all active actions are finished
            while (true) {
                if (executor.isTerminated()) {
                    break;
                }
                if (log.isLoggable(Level.FINE)) {
                    for (ResourceAction action : getActiveActions()) {
                        log.log(Level.FINE, "rae.await", new Object[]{serviceName, action});
                    }
                }
                executor.awaitTermination(10, TimeUnit.SECONDS);
            }
        }
        log.log(Level.FINE, "rae.stopped", serviceName);
        log.exiting(ResourceActionExecutor.class.getName(), "stop");
    }

    /**
     * Get the list of active actions
     * @return the list of active actions
     */
    public List<ResourceAction> getActiveActions() {
        log.entering(ResourceActionExecutor.class.getName(), "getActiveActions");
        List<ResourceAction> ret;
        actionLock.lock();
        try {
            ret = new ArrayList<ResourceAction>(activeActions);
        } finally {
            actionLock.unlock();
        }
        log.exiting(ResourceActionExecutor.class.getName(), "getActiveActions", ret);
        return ret;
    }

    /**
     * Submit a resource action
     * @param action the resource action
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the ResourceActionExecutor has been stopped
     *               or the underlying ExecutorService does not accept further tasks
     */
    public void submit(ResourceAction<?,K> action) throws ServiceNotActiveException {
        log.entering(ResourceActionExecutor.class.getName(), "submit", action);
        actionLock.lock();
        try {
            if (executor.isShutdown()) {
                throw new ServiceNotActiveException();
            } else {
                Queue<ResourceAction> queue = pendingActionMap.get(action.getResourceKey());
                if (queue == null) {
                    // We found no queue for this resource
                    // Start a new worker for the new queue
                    try {
                        executor.submit(new Worker(action.getResourceKey()));
                    } catch (RejectedExecutionException ex) {
                        throw new ServiceNotActiveException();
                    }
                    // Add the new queue
                    queue = new LinkedList<ResourceAction>();
                    pendingActionMap.put(action.getResourceKey(), queue);
                }
                queue.add(action);
            }
        } finally {
            actionLock.unlock();
        }
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "rae.submit", new Object[]{serviceName, action});
        }
        log.exiting(ResourceActionExecutor.class.getName(), "submit", action);
    }

    /**
     *  The Worker executes all ResourceAction for it's Resource id.
     *  If the queue is empty it removes the queue from the pendingActionMap.
     */
    private class Worker implements Runnable {

        private final K resourceKey;

        public Worker(K resourceKey) {
            this.resourceKey = resourceKey;
        }

        public void run() {
            log.entering(Worker.class.getName(), "run");
            while (true) {
                ResourceAction action = null;
                actionLock.lock();
                try {
                    if (Thread.currentThread().isInterrupted()) {
                        // Store the queue in a temp variable
                        // Outside of the log the unprocessed actions will be canceld
                        break;
                    }
                    Queue<ResourceAction> queue = pendingActionMap.get(resourceKey);
                    if (queue == null) {
                        break;
                    }
                    action = queue.poll();
                    if (action == null) {
                        pendingActionMap.remove(resourceKey);
                        break;
                    } else {
                        activeActions.add(action);
                    }
                } finally {
                    actionLock.unlock();
                }
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "rae.execute", new Object[]{serviceName, action});
                }
                long startTime = System.currentTimeMillis();
                try {
                    action.execute();
                } catch (Exception ex) {
                    log.log(Level.WARNING,
                            I18NManager.formatMessage("rae.action.err", BUNDLE, serviceName, action, ex.getLocalizedMessage()),
                            ex);
                } finally {
                    if (log.isLoggable(Level.FINE)) {
                        long duration = System.currentTimeMillis() - startTime;
                        log.log(Level.FINE, "rae.action.finished",
                                new Object[]{serviceName, action, duration});
                    }
                    actionLock.lock();
                    try {
                        activeActions.remove(action);
                    } finally {
                        actionLock.unlock();
                    }
                }
            }
            log.exiting(Worker.class.getName(), "run");
        }
    }
}
