/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.*;
import com.sun.grid.grm.config.naming.event.*;
import com.sun.grid.grm.service.*;
import com.sun.grid.grm.ui.ConfigurationService;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service manager defines the operation which are needed for "registering/unregistering"
 * the service to be managed by Resource Provider or Reporter.
 * 
 * Implementing class should decide how to deal with communication with remote
 * services, how to store the services etc.
 * 
 * The interface is not supposed to be used by administrator, it should be used
 * exclusively by ResourceProvider/Reporter internal mechanism.
 * 
 * @param <T> implementing class type of managed services
 */
public abstract class AbstractServiceManager<T extends Service> implements ServiceManager<T> {

    protected final ExecutionEnv env;
    private static final String BUNDLE = "com.sun.grid.grm.service.impl.service";
    private static final Logger log = Logger.getLogger(AbstractServiceManager.class.getName(), BUNDLE);
    private final ServiceStore<T> services;
    protected final ConfigurationService cs;
    private final Lock lock = new ReentrantLock();
    private final ConfigurationServiceEventListener csEventListener;
    private boolean started;

    /**
     * A default constructor.
     *
     * @param env Execution enviroment
     * @param serviceStore Instance of ServiceStore, can not be null.
     * @throws com.sun.grid.grm.GrmException in any problem during
     * the initial communication with CS
     */
    public AbstractServiceManager(ExecutionEnv env, ServiceStore<T> serviceStore)
            throws GrmException {
        if (serviceStore == null) {
            throw new IllegalArgumentException("Service store must not be null");
        }
        if (env == null) {
            throw new IllegalArgumentException("Execution environment must not be null");
        } else {
            this.env = env;
            services = serviceStore;
            cs = ComponentService.getCS(env);
            csEventListener = new SMConfigurationServiceEventListener();
            started = false;
        }
    }

    /**
     * Adds a service to a store of managed services. If called directly, one
     * should be sure that service object is supported by the service manager.
     * 
     * By default, the method is never called directly - service manager is
     * capable of handling all system situations and take care of all services
     * stored in CS.
     *
     * @param service managed service
     * @throws InvalidServiceException thrown when service object can not be
     * managed by the service manager
     * @return true if service was added
     */
    public boolean addService(T service) throws InvalidServiceException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "addService", service);
        }
        boolean service_added = false;
        lock.lock();
        try {
            String serviceName;
            try {
                serviceName = service.getName();
            } catch (GrmRemoteException grexa) {
                InvalidServiceException ex = new InvalidServiceException("asm.error.svc_name_prob", BUNDLE, new Object[0]);
                ex.initCause(grexa);
                if (log.isLoggable(Level.FINER)) {
                    log.throwing(AbstractServiceManager.class.getName(), "addService", ex);
                }
                throw ex;
            }
            if (serviceName == null || serviceName.length() == 0) {
                InvalidServiceException ex = new InvalidServiceException("asm.error.null_svc_name", BUNDLE, new Object[0]);
                if (log.isLoggable(Level.FINER)) {
                    log.throwing(AbstractServiceManager.class.getName(), "addService", ex);
                }
                throw ex;
            }
            if (getServiceNames().contains(serviceName)) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "asm.as.exist", serviceName);
                }
                service_added = false;
            } else {
                try {
                    services.addService(service);
                    service_added = true;
                } catch (ServiceStoreException sse) {
                    if (log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "asm.storeexception", sse);
                    }
                }
            }
            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "asm.as.ok", serviceName);
            }
        } finally {
            lock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "addService", service_added);
        }
        return service_added;
    }

    /**
     * Removes a service from the service manager.  This method has
     * no direct effect on the information stored in CS or on the removed service.
     *
     * @param serviceName name of the service
     */
    public boolean removeService(String serviceName) throws UnknownServiceException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "removeService", serviceName);
        }
        boolean service_removed = false;
        lock.lock();
        try {
            T service = null;
            try {
                service = services.getService(serviceName);
            } catch (ServiceStoreException sse) {
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "asm.storeexception", sse);
                }
            }
            if (service == null) {
                UnknownServiceException e = new UnknownServiceException("asm.error.unknownservice", BUNDLE, new Object[]{
                            serviceName
                        });
                if (log.isLoggable(Level.FINER)) {
                    log.throwing(AbstractServiceManager.class.getName(), "removeService", e);
                }
                throw e;
            }
            try {
                services.removeService(serviceName);
                service_removed = true;
            } catch (ServiceStoreException sse) {
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "asm.storeexception", sse);
                }
            }
        } finally {
            lock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "removeService", service_removed);
        }
        return service_removed;
    }

    /**
     * Gets a service.
     *
     * @param serviceName name of the service
     * @return the instance of service
     * @throws com.sun.grid.grm.service.UnknownServiceException if the service
     * with specified name was not found
     */
    public T getService(String serviceName) throws UnknownServiceException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "getService", serviceName);
        }
        T service = null;
        lock.lock();
        try {
            service = services.getService(serviceName);
        } catch (ServiceStoreException sse) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "asm.storeexception", sse);
            }
        } finally {
            lock.unlock();
        }
        if (service == null) {
            UnknownServiceException e = new UnknownServiceException("asm.error.unknownservice", BUNDLE, new Object[]{
                        serviceName
                    });
            if (log.isLoggable(Level.FINER)) {
                log.throwing(AbstractServiceManager.class.getName(), "getService", e);
            }
            throw e;
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "getService", service);
        }
        return service;
    }

    /**
     * Gets a set of managed service names.
     *
     * @return a set of managed service names
     */
    public Set<String> getServiceNames() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "getServiceNames");
        }
        Set<String> ret = Collections.<String>emptySet();
        lock.lock();
        try {
            ret = new HashSet<String>(services.getServiceNames());
        } catch (ServiceStoreException sse) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "asm.storeexception", sse);
            }
        } finally {
            lock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "getServiceNames", ret);
        }
        return ret;
    }

    /**
     * Gets a list of managed services.
     *
     * @return a list of managed services
     */
    public List<T> getServices() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "getServices");
        }
        List<T> ret = Collections.<T>emptyList();
        lock.lock();
        try {
            ret = services.getServices();
        } catch (ServiceStoreException sse) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "asm.storeexception", sse);
            }
        } finally {
            lock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "getServices", ret);
        }
        return ret;
    }

    /* helper method for syncing active services - method relies on a 
     * createManagedService(String ...) factory method
     */
    private void getServicesFromCS() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "getServicesFromCS");
        }
        List<ComponentInfo> cis = Collections.<ComponentInfo>emptyList();
        try {
            cis = ComponentService.getComponentInfosByType(env, Service.class);
        } catch (Exception e) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "asm.gsfcs.cs", e);
            }
        }
        for (ComponentInfo ci : cis) {
            String serviceName = ci.getName();
            if (serviceName == null || serviceName.length() == 0) {
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "asm.error.null_svc_name");
                }
            } else {
                try {
                    addService(createManagedService(serviceName));
                } catch (InvalidServiceException ex) {
                    if (log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "asm.gsfcs.add", ex);
                    }
                }
            }
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "getServicesFromCS");
        }
    }

    /**
     * Factory method that know what object type has to be created out of
     * service name.
     * 
     * @param s name of the service
     * @return service instance
     * @throws com.sun.grid.grm.service.InvalidServiceException when service 
     * manager can not create an object for the given name
     */
    protected abstract T createManagedService(String s) throws InvalidServiceException;

    /** 
     * helper method for clearing the managed services 
     * 
     * @return true if all services were removed
     */
    private boolean clear() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "clear");
        }
        Set<String> list = getServiceNames();
        boolean all_removed = true;
        for (String service : list) {
            try {
                all_removed = all_removed && removeService(service);
            } catch (UnknownServiceException use) {
                if (log.isLoggable(Level.FINEST)) {
                    log.log(Level.FINEST, "asm.c.us", use);
                }
            }
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "clear", all_removed);
        }
        return all_removed;
    }

    /**
     * Starts the service manager.
     */
    public void start() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "start");
        }
        lock.lock();
        try {
            if (!isStarted()) {
                started = true;
                if (!clear()) {
                    if (log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "asm.start.notcleared");
                    }
                }
                cs.addConfigurationServiceEventListener(csEventListener);
                getServicesFromCS();
            } else if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "asm.astarted");
            }
        } finally {
            lock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "start");
        }
        return;
    }

    /**
     * Stops the service manager.
     */
    public void stop() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "stop");
        }
        lock.lock();
        try {
            if (isStarted()) {
                started = false;
                cs.removeConfigurationServiceEventListener(csEventListener);
                if (!clear()) {
                    if (log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "asm.stop.notcleared");
                    }
                }
            } else if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "asm.nstarted");
            }
        } finally {
            lock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "stop");
        }
        return;
    }

    /** 
     * helper method for finding out a state of service manager
     * 
     * @return true if sm is started
     */
    protected boolean isStarted() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceManager.class.getName(), "isStarted");
        }
        boolean ret = false;
        lock.lock();
        try {
            ret = started;
        } finally {
            lock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractServiceManager.class.getName(), "isStarted", ret);
        }
        return ret;
    }

    /* helper class taking care of CS events */
    private class SMConfigurationServiceEventListener
            implements ConfigurationServiceEventListener {

        public void configurationObjectAdded(ConfigurationObjectAddedEvent event) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "asm.event.coa", event);
            }
            if (isStarted() && event.getType().equals(Service.class.getName())) {
                try {
                    addService(createManagedService(event.getName()));
                } catch (InvalidServiceException ex) {
                    if (log.isLoggable(Level.SEVERE)) {
                        log.log(Level.SEVERE, "asm.event.coa.adderr", new Object[]{event, ex});
                    }
                }
            }
        }

        public void configurationObjectRemoved(ConfigurationObjectRemovedEvent event) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "asm.event.cor", event);
            }
            if (isStarted() && event.getType().equals(Service.class.getName())) {
                try {
                    removeService(event.getName());
                } catch (UnknownServiceException ex) {
                    if (log.isLoggable(Level.SEVERE)) {
                        log.log(Level.SEVERE, "asm.event.cor.remerr", new Object[]{event, ex});
                    }
                }
            }
        }

        public void configurationObjectChanged(ConfigurationObjectChangedEvent event) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "asm.event.coc", event);
            }
            if (isStarted() && event.getType().equals(Service.class.getName())) {
                lock.lock();
                try {
                    removeService(event.getName());
                    addService(createManagedService(event.getName()));
                } catch (GrmException ex) {
                    if (log.isLoggable(Level.SEVERE)) {
                        log.log(Level.SEVERE, "asm.event.coc.cherr", new Object[]{event, ex});
                    }
                } finally {
                    lock.unlock();
                }
            }
        }
    }
}
