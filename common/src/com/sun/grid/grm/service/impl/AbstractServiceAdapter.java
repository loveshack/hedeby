/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.service.slo.impl.RunnableSLOManagerSetup;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.config.common.AbstractServiceConfig;
import com.sun.grid.grm.impl.AbstractComponentAdapter;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceFactory;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.resource.adapter.ResourceAdapterStore;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.adapter.InvalidResourceStateException;
import com.sun.grid.grm.resource.adapter.MergeResult;
import com.sun.grid.grm.resource.adapter.RAOperation;
import com.sun.grid.grm.resource.adapter.RAOperationException;
import com.sun.grid.grm.resource.adapter.ResourceTransition;
import com.sun.grid.grm.resource.adapter.impl.AddResourceOPR;
import com.sun.grid.grm.resource.adapter.impl.DefaultResourceTransition;
import com.sun.grid.grm.service.*;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.service.slo.impl.DefaultSLOState;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.resource.impl.ResourceAnnotationUpdateOperation;
import com.sun.grid.grm.resource.impl.SetAmbiguousResourceOperation;
import com.sun.grid.grm.resource.impl.UpdateResourceUsageOperation;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.descriptor.ResourceRemovalFromSystemDescriptor;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.SLOContextFactory;
import com.sun.grid.grm.service.slo.SLOManager;
import com.sun.grid.grm.service.slo.event.SLOErrorEvent;
import com.sun.grid.grm.service.slo.event.SLOManagerEventListener;
import com.sun.grid.grm.service.slo.event.SLOUpdatedEvent;
import com.sun.grid.grm.service.slo.event.UsageMapUpdatedEvent;
import com.sun.grid.grm.service.slo.impl.DefaultSLOContext;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManager;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.InvalidStateTransistionException;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Abstract base class for <code>ServiceAdapter</code>.
 * 
 * <p>This class provides an implementation of the life cycle of the <code>ServiceAdapter</code>. 
 *    Subclasses must implement only <code>doStartService</code>, <code>doReloadService</code> and
 *    <code>doStopService</code> method to getAll a correct handling of the service life cycle.</p>
 * 
 * <h2>Implemented service life cycle</h2>
 * 
 * The service life cycle has been implemented as documented in the spec. Here
 * is the state diagram for a service:
 * 
 * <img src="http://hedeby.sunsource.net/doc/spec/0.4/images/Overview/CommonConcept/service_state_transitions.png"/>
 * 
 * The life cycle of a component is related to the life cycle a the service. If the component
 * is started the service is also started. If the component is stopped the service
 * is also stopped. 
 * 
 * <h2>Note</h2>
 * 
 * <p>This class does not provide a full featured API for a <code>ServiceAdapter</code>.
 *    A lot of things like managing SLOs, managing resource persistence must be done
 *    by the implementation of the concrete service adapter. Many improvements here
 *    are possible.</p>
 * 
 * @param <E> Type of <code>ExecutorService</code> for the service adapter
 * @param <C> The type of the configuration
 * @param <K> The type of the key of a resource
 * @param <RA> Type the of the ResourceAdapter
 */
public abstract class AbstractServiceAdapter<E extends ExecutorService, C extends AbstractServiceConfig, K, RA extends ResourceAdapter<K>> extends AbstractComponentAdapter<E, C> implements ServiceAdapter<E, C>, SLOContextFactory {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.service";
    private final static Logger log = Logger.getLogger(AbstractServiceAdapter.class.getName(), BUNDLE);
    private final AtomicReference<StateHandler<K, C, RA>> stateHandler = new AtomicReference<StateHandler<K, C, RA>>();
    private final AtomicReference<ResourceActionExecutor<K>> resourceActionExecutor = new AtomicReference<ResourceActionExecutor<K>>();
    private final AtomicReference<ResourceAdapterStore<K, RA>> resourceStore = new AtomicReference<ResourceAdapterStore<K, RA>>();
    private final AtomicReference<ResourceFactory<RA>> resourceFactory = new AtomicReference<ResourceFactory<RA>>();
    private final SLOManagerTask sloManagerTask;
    /**
     * Guards changes on the component state, the service state, the resources
     * and the resource requests
     * It must be locked whenever an element which is part of a service
     * snapshot is changed.
     */
    private final Lock lock = new ReentrantLock();
    /**
     * This map contains the last send resource request for each SLO. The
     * content of the map is included in a snapshot
     */
    private final Map<String, Collection<Need>> resourceRequestMap = new HashMap<String, Collection<Need>>();

    /**
     * Creates a new instance of AbstractServiceAdapter.
     *
     * @param component the owning component
     */
    protected AbstractServiceAdapter(AbstractServiceContainer<? extends ServiceAdapter<E, C>, E, C> component) {
        super(component);
        stateHandler.set(new UnknownStateHandler(ServiceState.UNKNOWN));
        sloManagerTask = new SLOManagerTask();
    }

    // ---- Public method ------------------------------------------------------
    /**
     * Get the state of the service
     * @return the state of the service
     */
    public final ServiceState getServiceState() {
        return stateHandler.get().getState();
    }

    /**
     * Get the states of the SLOs of this service.
     * 
     * @return list of slo states
     */
    public final List<SLOState> getSLOStates() throws GrmRemoteException {
        log.entering(AbstractServiceAdapter.class.getName(), "getSLOStates");

        List<SLOState> ret = getSLOStates(ConstantFilter.<Resource>alwaysMatching());

        log.exiting(AbstractServiceAdapter.class.getName(), "getSLOStates", ret);
        return ret;
    }

    /**
     * Get the SLO manager of this service adapter
     * @return the SLO manager if this service adapter
     */
    public final SLOManager getSLOManager() {
        return sloManagerTask.getSLOManager();
    }

    /**
     * Get the SLOStates of this service. Include only the resource information
     * of those resources matching a <code>resourceFilter</code>.
     * 
     * <p>This method takes a snapshot from the <code>SLOState</code> of the
     *    <code>SLOManager</code> and a snapshot of the <code>Resources</code>
     *    of the service. If there are <code>SLOState</code>s of obsolete 
     *    <code>Resources</code> are removed.</p>
     *
     * @param resourceFilter  the resource filter
     * @return the SLOState
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public final List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
        log.entering(AbstractServiceAdapter.class.getName(), "getSLOStates");

        List<SLOState> ret = null;
        SLOManager tmpSloManager = null;
        List<Resource> resources = null;
        try {
            lock.lock();
            try {
                tmpSloManager = getSLOManager();
                resources = getResources();
            } finally {
                lock.unlock();
            }

            // It is possible that the usage map of the slo manager contains
            // usage for resources which has been removed
            // => Remove these resources from the usage map
            ret = new ArrayList<SLOState>();

            Map<Resource, Usage> newUsageMap = new HashMap<Resource, Usage>();
            ResourceVariableResolver resolver = new ResourceVariableResolver();

            for (SLOState aState : tmpSloManager.getSLOStates()) {
                newUsageMap.putAll(aState.getUsageMap());
                for (Map.Entry<Resource, Usage> entry : aState.getUsageMap().entrySet()) {
                    if (!resources.contains(entry.getKey())) {
                        newUsageMap.remove(entry.getKey());
                        continue;
                    }
                    resolver.setResource(entry.getKey());
                    if (!resourceFilter.matches(resolver)) {
                        newUsageMap.remove(entry.getKey());
                        continue;
                    }
                }
                ret.add(new DefaultSLOState(aState.getSloName(), newUsageMap, aState.getNeeds()));
                newUsageMap.clear();
            }
        } catch (ServiceNotActiveException ex) {
            ret = Collections.<SLOState>emptyList();
        }

        log.exiting(AbstractServiceAdapter.class.getName(), "getSLOStates", ret);
        return ret;
    }

    /**
     * Get all resources which matches a filter.
     *
     * <p>This method takes a snapshot of all existing resources of the service
     *    and apply the <code>filter</code> on the snapshot</p>.
     * 
     * @param  filter the filter
     * @return list of resources
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws com.sun.grid.grm.GrmRemoteException
     */
    public final List<Resource> getResources(Filter<Resource> filter) throws ServiceNotActiveException, GrmRemoteException {
        log.entering(AbstractServiceAdapter.class.getName(), "getResources", filter);

        List<Resource> matched = new LinkedList<Resource>();
        ResourceVariableResolver resolver = new ResourceVariableResolver();

        List<Resource> resources = getResources();
        for (Resource r : resources) {
            resolver.setResource(r);
            if (filter.matches(resolver)) {
                matched.add(r);
            }
        }
        log.exiting(AbstractServiceAdapter.class.getName(), "getResources", matched);
        return matched;
    }

    /**
     * Create a default SLOContext
     * @return return theSLOContext 
     * @throws com.sun.grid.grm.GrmException if the resource can not be evaluated
     */
    public SLOContext createSLOContext() throws GrmException {
        // We call directly getResources from the resource store,
        // because we don't want the ServiceNotActiveException thrown by
        // getResources() method if the service is in starting phase
        return new DefaultSLOContext(new ArrayList<Resource>(getResourceStore().getResources()));
    }

    /**
     * Prepares the startup of the component.
     * 
     * <p>This method is invoked before the component state transition 
     *    from STOPPED to STARTING is performed.</p>
     * <p>If the service is in a state that the component startup is not
     *    possible this method throws and InvalidStateTransitionException.</p>
     * 
     * @param config the new configuration of the component
     * @throws com.sun.grid.grm.GrmException if the component can not be started
     */
    @Override
    public final void prepareComponentStartup(C config) throws GrmException {
        log.entering(AbstractServiceAdapter.class.getName(), "prepareComponentStartup");
        stateHandler.get().prepareComponentStartup(config);
        log.exiting(AbstractServiceAdapter.class.getName(), "prepareComponentStartup");
    }

    /**
     * Perform the component startup.
     * 
     * @param config the configuration of the component
     * @throws com.sun.grid.grm.GrmException if the component startup failed
     */
    public final void startComponent(C config) throws GrmException {
        log.entering(AbstractServiceAdapter.class.getName(), "startComponent");
        stateHandler.get().startComponent(config);
        log.exiting(AbstractServiceAdapter.class.getName(), "startComponent");
    }

    /**
     * Prepare the component shutdown.
     * 
     * <p>This method is invoked before the component state changes from
     *    STARTED to STOPPING.</p>
     * <p>In this method it is checked whether the component could be stopped.
     *    It throws an InvalidateStateTransitionException if the <code>forced</code>
     *    parameter is false and the service state is STARTING or STOPPING.</p>
     * <p>If the <code>forced</code> parameter is <code>true</code> and the 
     *    service is in STARTING or STOPPING state it allows the component
     *    shutdown, because the shutdown of the executor will interrupt the
     *    service startup or service shutdown.</p>
     * 
     * @param forced  if <code>true</code> the component shutdown is performed in forced mode
     * @throws com.sun.grid.grm.GrmException if the shutdown of the component is not possible
     */
    @Override
    public final void prepareComponentShutdown(boolean forced) throws GrmException {
        log.entering(AbstractServiceAdapter.class.getName(), "prepareComponentShutdown");
        stateHandler.get().prepareComponentShutdown(forced);
        log.exiting(AbstractServiceAdapter.class.getName(), "prepareComponentShutdown");
    }

    /**
     * Stop the component.
     * 
     * @param forced if <code>true</code> shutdown the component in forced mode
     * 
     * @throws com.sun.grid.grm.GrmException if the component could not be stopped
     */
    public final void stopComponent(boolean forced) throws GrmException {
        log.entering(AbstractServiceAdapter.class.getName(), "stopComponent", forced);
        stateHandler.get().stopComponent(forced);
        log.exiting(AbstractServiceAdapter.class.getName(), "stopComponent");
    }

    /**
     * Prepare the reload of the component.
     * 
     * @param config  the configuration of the component
     * @param forced  reload in forced mode
     * @throws com.sun.grid.grm.GrmException if the reload of the component is not possible
     */
    @Override
    public final void prepareComponentReload(C config, boolean forced) throws GrmException {
        log.entering(AbstractServiceAdapter.class.getName(), "prepareComponentReload", forced);
        stateHandler.get().prepareComponentReload(config, forced);
        log.exiting(AbstractServiceAdapter.class.getName(), "prepareComponentReload");
    }

    /**
     * Reload the component.
     * 
     * @param config the new configuration of the component
     * @param forced if <code>true</code> reload in forced mode
     * @throws com.sun.grid.grm.GrmException if the reload was not possible
     */
    public final void reloadComponent(C config, boolean forced) throws GrmException {
        log.entering(AbstractServiceAdapter.class.getName(), "reloadComponent", forced);
        stateHandler.get().reloadComponent(config, forced);
        log.exiting(AbstractServiceAdapter.class.getName(), "reloadComponent");
    }

    /**
     * Start the service.
     * @param config  the configuration of the service
     * @throws com.sun.grid.grm.GrmException if the startup failed
     */
    public final void startService(final C config) throws GrmException {
        log.entering(AbstractServiceAdapter.class.getName(), "startService");
        stateHandler.get().startService(config);
        log.exiting(AbstractServiceAdapter.class.getName(), "startService");
    }

    /**
     * Stop the service.
     * 
     * @param freeResources if <code>true</code> free all non static resources.
     * @throws com.sun.grid.grm.GrmException if the service could not be stopped
     */
    public final void stopService(boolean freeResources) throws GrmException {
        log.entering(AbstractServiceAdapter.class.getName(), "stopService", freeResources);
        stateHandler.get().stopService(freeResources);
        log.exiting(AbstractServiceAdapter.class.getName(), "stopService");
    }

    /**
     * This interface provides all methods which are part of a service state transition.
     * 
     * @param <C> the type of the component configuration
     */
    private interface StateHandler<K, C, RA extends ResourceAdapter> {

        /**
         * Get the <code>ServiceState</code> represented by this <code>StateHandler</code>.
         * @return the <code>ServiceState</code>
         */
        ServiceState getState();

        /**
         * This method implements the synchronous part of the state transition
         * from the current state to STARTING.
         * 
         * <p>The implementing <code>StateHandler</code> has the possibility to
         *    make a veto against the state transition by throwing a </code>GrmException</code>.
         * @param config the configuration of the service
         * @throws com.sun.grid.grm.GrmException if the state transition is not possible
         */
        public void prepareComponentStartup(C config) throws GrmException;

        /**
         * This method is called from the <code>startComponent</code> method of
         * the <code>AbstractServiceAdapter</code>. It is invoked if a component
         * should start. 
         * 
         * <p>If the <code>StateHandler</code> does not support the transition to STARTING
         * it throws an <code>InvalidStateTransitionException</code>.</br>
         * If the transition is supported, but not possible (e.g. due to an invalid
         * configuration) it throws an <code>GrmException</code>.
         * </p>
         * @param config
         * @throws com.sun.grid.grm.GrmException
         */
        public void startComponent(C config) throws GrmException;

        /**
         * This method implements the synchronous part of the state transition
         * from the current state to SHUTDOWN.
         * 
         * <p>The implementing <code>StateHandler</code> has the possibility to
         *    make a veto against the state transition by throwing a </code>GrmException</code>.
         * 
         * @param forced <code>true</code> if the shutdown is forced
         * @throws com.sun.grid.grm.GrmException if the shutdown is not possible
         */
        public void prepareComponentShutdown(boolean forced) throws GrmException;

        /**
         * This method triggers the state transition from the current state
         * to SHUTDOWN. It is invoked when the component starts up.
         * 
         * @param forced <code>true</code> if the shutdown is forced
         * @throws com.sun.grid.grm.GrmException if the shutdown is not possible
         */
        public void stopComponent(boolean forced) throws GrmException;

        /**
         * This method triggers the state transition from the current state
         * to SHUTDOWN. It is invoked from the {@link  AbstractServiceAdapter#startService(java.lang.Object) }
         * method. 
         * @param config the configuration of the service
         * @throws com.sun.grid.grm.GrmException if the startup of the service is not possible
         */
        public void startService(C config) throws GrmException;

        /**
         * This method triggers the state transition from the current state
         * to SHUTDOWN. It is invoked from the {@link AbstractServiceAdapter#stopService(boolean) }
         * method.
         * 
         * @param freeResources free non static resources
         * @throws com.sun.grid.grm.GrmException if the shutdown is not possible
         */
        public void stopService(boolean freeResources) throws GrmException;

        /**
         * This method implement the synchronous part of the state state transition
         * from the current state to RELOADING.
         * 
         * @param config the config of the service
         * @param forced if <code>true</code> the reload is forced
         * @throws com.sun.grid.grm.GrmException if the reload is not possible
         */
        public void prepareComponentReload(C config, boolean forced) throws GrmException;

        /**
         * This method triggers the state transition from the current state to
         * RELOADING. It is invoked from the {@link AbstractServiceAdapter#reloadComponent(java.lang.Object, boolean) ) }
         * method. 
         * @param config the configuration of the service
         * @param forced if <code>true</code> the reload is forced
         * @throws com.sun.grid.grm.GrmException if the reload is not possible
         */
        public void reloadComponent(C config, boolean forced) throws GrmException;

        /**
         * This method implements the state transition from the current state to
         * UNKNOWN. The reconnectAction is used to try a reconnect to the real
         * service.
         * @param reconnectAction the reconnect action
         * @throws com.sun.grid.grm.GrmException if the state transition to UNKNOWN is not possible
         */
        public void reconnect(Callable<Map<K, Object>> reconnectAction) throws GrmException;

        /**
         * Add a resource
         * @param resource the resource
         * @param sloName the name of the requesting SLO
         * @throws com.sun.grid.grm.service.ServiceNotActiveException if the serivce is not active
         * @throws com.sun.grid.grm.resource.InvalidResourceException if the resource has not been accepted by the service
         */
        public void addResource(Resource resource, String sloName) throws ServiceNotActiveException, InvalidResourceException;

        /**
         * Create a new resource and assign it
         * @param type       the type of the resource
         * @param properties the properties of the resource
         * @throws ServiceNotActiveException if the service is not active
         * @throws InvalidResourceException If the resource type is not supported
         * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the resource properties are not valid
         * @throws com.sun.grid.grm.GrmRemoteException  communication error
         * @throws com.sun.grid.grm.GrmException if the resource could not be created
         * @return the resource
         */
        public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException;

        /**
         * Remove a resource
         * @param resourceId the id of the resource which should be removed
         * @param descr the descriptor of the resource removal
         * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
         * @throws com.sun.grid.grm.resource.InvalidResourceException if the resource can not be removed
         * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
         */
        public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, InvalidResourceException, ServiceNotActiveException;

        /**
         * Reset a resource
         * @param resource  the resource
         * @throws com.sun.grid.grm.resource.UnknownResourceException  if the resource is unknown
         * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
         */
        public void resetResource(ResourceId resource) throws UnknownResourceException, ServiceNotActiveException;

        /**
         * Modify the properties of a resource
         * @param resource   the resource
         * @param operations the operation
         * @throws com.sun.grid.grm.resource.UnknownResourceException  if the resource is unknown
         * @throws com.sun.grid.grm.service.ServiceNotActiveException  if the service is not active
         * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation would create an invalid resource
         */
        public void modifyResourceProperties(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, InvalidResourcePropertiesException;

        /**
         * Set the ambiguous flag of a resource
         * @param resource   the resource
         * @param ambiguous  the new value of the ambiguous flag
         * @param annotation the annotation for the resource
         * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
         * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
         */
        public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws ServiceNotActiveException, UnknownResourceException;

        /**
         * Process an update resource operation.
         * 
         * @param resourceKey   the key of the resource
         * @param operation  the operation
         * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
         * @throws UnknownResourceException if the resource is unknown
         */
        public void processUpdateResourceOperation(K resourceKey, RAOperation operation)
                throws UnknownResourceException, ServiceNotActiveException, RAOperationException;

        /**
         * Submit a batch of resource operations
         *
         * @param resourceKey   the key of the resource
         * @param operations  the iterator over the operations
         * @param name       name of the batch operation
         * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
         */
        public void submitUpdateResourceOperations(K resourceKey, String name, Iterable<RAOperation> operations)
                throws ServiceNotActiveException;

        /**
         * Process a resource operation that update an resource. If the resource
         * does not exists in the resource store it adds it before invoking
         * the operation.
         * 
         * @param resourceKey   the key of the resource
         * @param internalRepresentation  the internal representation of the resource
         * @param operation  the resource operation
         * @throws InvalidResourceException if the operation would produce an invalid resource
         * @throws com.sun.grid.grm.service.ServiceNotActiveException
         */
        public void processUpdateOrCreateResourceOperation(K resourceKey, Object internalRepresentation, RAOperation operation)
                throws InvalidResourceException, ServiceNotActiveException, RAOperationException;
    }

    /**
     * Represents the result of a service state transition.
     * 
     * @param <C> the type configuration of the service
     */
    private static class TransitionResult<K, C, RA extends ResourceAdapter> {

        private final StateHandler<K, C, RA> followState;
        private final GrmException error;

        public TransitionResult(StateHandler<K, C, RA> followState, GrmException error) {
            this.followState = followState;
            this.error = error;
        }

        public TransitionResult(StateHandler<K, C, RA> followState) {
            this(followState, null);
        }

        /**
         * Get the follow state of the state transition
         * @return the follow state
         */
        public StateHandler<K, C, RA> getFollowState() {
            return followState;
        }

        /**
         * Get the error produced by the state transition
         * @return the error
         */
        public GrmException getError() {
            return error;
        }

        @Override
        public String toString() {
            if (error != null) {
                return String.format("TransitionResult[state=%s,error=%s]", followState, error.getLocalizedMessage());
            } else {
                return String.format("TransitionResult[state=%s]", followState);
            }
        }
    }

    /**
     * This class implements all method of a StateHandler which throws an ServiceNotActiveException if
     * the service is not active
     */
    private abstract class ServiceNotActiveStateHandler implements StateHandler<K, C, RA> {

        public final void addResource(Resource resource, String sloName) throws ServiceNotActiveException, InvalidResourceException {
            throw new ServiceNotActiveException();
        }

        public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException {
            throw new ServiceNotActiveException();
        }

        public final void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws ServiceNotActiveException {
            throw new ServiceNotActiveException();
        }

        public final void resetResource(ResourceId resource) throws ServiceNotActiveException {
            throw new ServiceNotActiveException();
        }

        public final void modifyResourceProperties(ResourceId resource, Collection<ResourceChangeOperation> operations) throws ServiceNotActiveException {
            throw new ServiceNotActiveException();
        }

        public void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws ServiceNotActiveException {
            throw new ServiceNotActiveException();
        }

        public void processResourceOperation(Resource resource, RAOperation operation) throws ServiceNotActiveException {
            throw new ServiceNotActiveException();
        }

        public void processUpdateResourceOperation(K resourceKey, RAOperation operation) throws UnknownResourceException, ServiceNotActiveException, RAOperationException {
            throw new ServiceNotActiveException();
        }

        public void processUpdateOrCreateResourceOperation(K resourceKey, Object internalRepresentation, RAOperation operation) throws InvalidResourceException, ServiceNotActiveException, RAOperationException {
            throw new ServiceNotActiveException();
        }

        public void submitUpdateResourceOperations(K resourceKey, String name, Iterable<RAOperation> operations) throws ServiceNotActiveException {
            throw new ServiceNotActiveException();
        }


    }

    private abstract class IntermediateStateHandler<C> extends ServiceNotActiveStateHandler {

        private final AtomicBoolean synchron = new AtomicBoolean();

        public IntermediateStateHandler<C> synchron() {
            this.synchron.set(true);
            return this;
        }

        public IntermediateStateHandler<C> asynchron() {
            this.synchron.set(false);
            return this;
        }

        /**
         * All transition triggered by stopService and startService are running
         * asynchronous, all other state transitions are running synchronous (triggered
         * by startComponent, stopComponent, reloadComponent).
         * @return <code>true</code> if the state transition should be executed synchronous
         */
        public boolean isSynchron() {
            return synchron.get();
        }

        /**
         * This method implements the necessary actions to perform the transition
         * from the intermediate state (-ING state) to the follow state (non -ING state)
         * 
         * @return the result of the transition. If the transition produced an error
         *         the exception will be stored in the error property of the result
         *         The followState property of the result contains the stateHandler
         *         which represent the follow state of the transition
         */
        protected abstract TransitionResult<K, C, RA> performTransition();
    }

    /**
     * The <code>UnknownStateHandler</code> handles all service state transitions
     * if the service state is unknown.
     */
    private final class UnknownStateHandler extends ServiceNotActiveStateHandler {

        private final ServiceState serviceState;

        public UnknownStateHandler(ServiceState serviceState) {
            this.serviceState = serviceState;
        }

        public ServiceState getState() {
            return serviceState;
        }

        public void prepareComponentStartup(C config) throws GrmException {
            // Startup is possible 
        }

        public void startComponent(C config) throws GrmException {
            changeServiceState(this, new StartingStateHandler(config).synchron());
        }

        public void startService(C config) throws GrmException {
            changeServiceState(this, new StartingStateHandler(config).asynchron());
        }

        public void stopService(boolean freeResources) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException("asa.ex.unknown.stop", BUNDLE);
        }

        public void prepareComponentShutdown(boolean forced) {
            // Ignore it, service is already in UNKNOWN state
        }

        public void stopComponent(boolean forced) {
            // Ignore it, service is already in UNKNOWN state
        }

        public void prepareComponentReload(C config, boolean forced) {
            // Ignore it, service is not running, with the next startup the config becomes
            // active
        }

        public void reloadComponent(C config, boolean forced) {
            // Ignore it, service is not running, with the next startup the config becomes
            // active
        }

        public void reconnect(Callable<Map<K, Object>> reconnectAction) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException("asa.ex.unknown.reconnect", BUNDLE);
        }

        @Override
        public String toString() {
            return String.format("UnknownStateHandler[%s]", getState());
        }
    }

    /**
     * This class handles all state transition for the phase where the startup
     * of the service is in progress.
     * It allows no state transition until the startup is finished or failed.
     */
    private class StartingStateHandler extends IntermediateStateHandler<C> {

        private final C config;

        public StartingStateHandler(C config) {
            this.config = config;
        }

        public TransitionResult<K, C, RA> performTransition() {
            log.entering(StartingStateHandler.class.getName(), "performTransition");

            TransitionResult<K, C, RA> ret = null;

            ServiceElements se = null;
            try {
                se = initServiceElements("asa.ex.starting.failed", config);
            } catch (GrmException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN), ex);
                return ret;
            }

            Map<K, Object> internalResources = null;
            try {
                internalResources = doStartService(config);
            } catch (GrmException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.ERROR),
                        new GrmException("asa.ex.starting.failed", ex, BUNDLE, ex.getLocalizedMessage()));
                log.exiting(StartingStateHandler.class.getName(), "performTransition", ret);
                return ret;
            } catch (Throwable ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.ERROR),
                        new GrmException("asa.ex.starting.interal", ex, BUNDLE, ex.getLocalizedMessage()));
                log.exiting(StartingStateHandler.class.getName(), "performTransition", ret);
                return ret;
            }

            MergeResult<K, RA> mergeResult = null;
            try {
                mergeResult = mergeResources(se.getResourceStore(), se.getResourceFactory(), internalResources);
            } catch (ResourceStoreException ex) {
                ServiceState newState = null;
                try {
                    newState = doStopService(false);
                } catch (GrmException ex1) {
                    log.log(Level.SEVERE,
                            I18NManager.formatMessage("asa.starting.doStop.failed", getName(), ex1.getLocalizedMessage()),
                            ex1);
                }
                return new TransitionResult<K, C, RA>(new UnknownStateHandler(newState),
                        new GrmException("asa.ex.starting.failed", ex, BUNDLE, ex.getLocalizedMessage()));
            }


            try {
                commitStartup(se, mergeResult);
            } catch (ServiceNotActiveException ex) {
                ServiceState newState = null;
                try {
                    newState = doStopService(false);
                } catch (GrmException ex1) {
                    log.log(Level.WARNING,
                            I18NManager.formatMessage("asa.starting.doStop.failed", BUNDLE, getName(), ex1.getLocalizedMessage()),
                            ex1);
                }
                return new TransitionResult<K, C, RA>(new UnknownStateHandler(newState),
                        new GrmException("asa.ex.starting.failed", ex, BUNDLE, ex.getLocalizedMessage()));
            }
            ret = new TransitionResult<K, C, RA>(new RunningStateHandler());
            log.exiting(StartingStateHandler.class.getName(), "performTransition", ret);
            return ret;
        }

        public ServiceState getState() {
            return ServiceState.STARTING;
        }

        public void startService(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.starting.start", BUNDLE);
        }

        public void prepareComponentStartup(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.starting.start", BUNDLE);
        }

        public void startComponent(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.starting.start", BUNDLE);
        }

        public void stopService(boolean freeResources) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.starting.stop", BUNDLE);
        }

        public void prepareComponentShutdown(boolean forced) throws GrmException {
            if (!forced) {
                throw new InvalidStateTransistionException("asa.ex.starting.stop", BUNDLE);
            }
        }

        public void stopComponent(boolean forced) throws GrmException {
            log.entering(StartingStateHandler.class.getName(), "stopComponent", forced);
            if (forced) {
                try {
                    destroyServiceElements();
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, I18NManager.formatMessage("asa.se.destroy.failed", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
                } finally {
                    doStopService(forced);
                }
            } else {
                throw new InvalidStateTransistionException("asa.ex.starting.stop", BUNDLE);
            }
            log.exiting(StartingStateHandler.class.getName(), "stopComponent");
        }

        public void prepareComponentReload(C config, boolean forced) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.starting.reload", BUNDLE);
        }

        public void reloadComponent(C config, boolean forced) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.starting.reload", BUNDLE);
        }

        public void reconnect(Callable<Map<K, Object>> reconnectAction) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException("asa.ex.starting.reconnect", BUNDLE);
        }

        @Override
        public String toString() {
            return String.format("StartingStateHandler[%s]", getState());
        }
    }

    /**
     * This class handles all state transitions of the service is in RUNNING state.
     * It allows shutdown, reload and the reconnect to real service.
     */
    private class RunningStateHandler implements StateHandler<K, C, RA> {

        public ServiceState getState() {
            return ServiceState.RUNNING;
        }

        public void startService(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.running.start", BUNDLE);
        }

        public void prepareComponentStartup(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.running.start", BUNDLE);
        }

        public void startComponent(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.running.start", BUNDLE);
        }

        public void stopService(boolean freeResources) throws GrmException {
            changeServiceState(this, new ShutdownStateHandler(freeResources).asynchron());
        }

        public void prepareComponentShutdown(boolean forced) throws GrmException {
            // Shutdown is possible
        }

        public void stopComponent(boolean forced) throws GrmException {
            log.entering(RunningStateHandler.class.getName(), "stopComponent", forced);
            changeServiceState(this, new ShutdownStateHandler(false).synchron());
            log.exiting(RunningStateHandler.class.getName(), "stopComponent");
        }

        public void prepareComponentReload(C config, boolean forced) throws GrmException {
            // Reload is possible
        }

        public void reloadComponent(C config, boolean forced) throws GrmException {
            changeServiceState(this, new PreReloadingStateHandler(config, forced).synchron());
        }

        public void reconnect(Callable<Map<K, Object>> reconnectAction) throws GrmException {
            changeServiceState(this, new ReconnectStateHandler(reconnectAction).asynchron());
        }

        @Override
        public String toString() {
            return String.format("RunningStateHandler[%s]", getState());
        }

        public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmException {

            ResourceFactory<RA> resFac = resourceFactory.get();

            if (resFac == null) {
                throw new ServiceNotActiveException();
            }

            Resource res = resFac.createResource(getExecutionEnv(), type, properties);

            addResource(res, null);

            return res.clone();
        }

        public final void addResource(final Resource resource, final String slo) throws InvalidResourceException, ServiceNotActiveException {

            ResourceId resId = resource.getId();
            try {
                RA ra = getResourceStore().createResourceAdapter(resource);

                ResourceTransition transition = null;
                try {
                    transition = ra.prepareInstall();
                } catch (InvalidResourceStateException ex) {
                    throw new InvalidResourceException(ex.getLocalizedMessage(), ex);
                }
                sloManagerTask.disable();
                try {
                    lock.lock();
                    try {
                        // Check again that the resource adapter still does not exist
                        try {
                            ra = getResourceStore().getByKey(ra.getKey());
                            throw new InvalidResourceException("asa.ar.exist", BUNDLE, resId, ra.getKey());
                        } catch (UnknownResourceException ex) {
                            // Fine resource is unknown => commit the transition
                            commitResourceTransition(ra, transition);
                            // Resource is now prepared for installation, submit the InstallAction
                            getResourceActionExecutor().submit(new InstallAction(ra.getKey(), resId, transition, slo));
                        }
                    } finally {
                        lock.unlock();
                    }
                } finally {
                    sloManagerTask.enable(false);
                }
            } catch (ResourceStoreException ex) {
                handleResourceStoreException(ex);
                throw new ServiceNotActiveException();
            }
        }

        public final void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, ServiceNotActiveException, InvalidResourceException {
            try {
                RA ra = getResourceStore().get(resourceId);
                ResourceTransition transition = ra.prepareUninstall(descr);
                sloManagerTask.disable();
                try {
                    lock.lock();
                    try {
                        commitResourceTransition(ra, transition);
                        getResourceActionExecutor().submit(new UninstallAction(ra.getKey(), resourceId, descr, transition));
                    } finally {
                        lock.unlock();
                    }
                } finally {
                    sloManagerTask.enable(false);
                }
            } catch (ResourceStoreException ex) {
                handleResourceStoreException(ex);
                throw new ServiceNotActiveException();
            }
        }

        public final void resetResource(final ResourceId resId) throws UnknownResourceException, ServiceNotActiveException {
            try {
                RA ra = getResourceStore().get(resId);
                ResourceTransition transition = ra.prepareReset();
                sloManagerTask.disable();
                try {
                    lock.lock();
                    try {
                        commitResourceTransition(ra, transition);
                        getResourceActionExecutor().submit(new ResetAction(ra.getKey(), resId, transition));
                    } finally {
                        lock.unlock();
                    }
                } finally {
                    sloManagerTask.enable(false);
                }
            } catch (ResourceStoreException ex) {
                handleResourceStoreException(ex);
                throw new ServiceNotActiveException();
            }
        }

        public void modifyResourceProperties(ResourceId resId, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, InvalidResourcePropertiesException, ServiceNotActiveException {
            try {
                RA ra = getResourceStore().get(resId);
                Collection<ResourceChanged> changes = ra.modifyResource(operations);
                if (changes != null && !changes.isEmpty()) {
                    for(ResourceChanged c: changes) {
                        if(c.hasChangedResource()) {
                            sloManagerTask.disable();
                            try {
                                lock.lock();
                                try {
                                    getResourceStore().store(ra);
                                    getServiceEventSupport().fireResourceChanged(ra.getResource(), changes, "");
                                } finally {
                                    lock.unlock();
                                }
                            } finally {
                                sloManagerTask.enable(true);
                            }
                            break;
                        }
                    }
                }
            } catch (ResourceStoreException ex) {
                handleResourceStoreException(ex);
                throw new ServiceNotActiveException();
            }
        }

        public void setAmbiguous(ResourceId resId, boolean ambiguous, String annotation) throws UnknownResourceException, ServiceNotActiveException {
            ArrayList<ResourceChangeOperation> ops = new ArrayList<ResourceChangeOperation>(2);
            ops.add(new SetAmbiguousResourceOperation(Boolean.valueOf(ambiguous)));
            ops.add(new ResourceAnnotationUpdateOperation(annotation));

            try {
                modifyResource(resId, ops);
            } catch (InvalidResourcePropertiesException ex) {
                // Should not happen, setAmbigious works always
                throw new IllegalStateException("setAmbigious of resource " + resId + " did not work:" + ex.getLocalizedMessage(), ex);
            }
        }

        public void processUpdateResourceOperation(K resourceKey, RAOperation operation) throws UnknownResourceException, ServiceNotActiveException, RAOperationException {
            doProcessUpdateResourceOperation(resourceKey, operation);
        }

        public void submitUpdateResourceOperations(K resourceKey, String name, Iterable<RAOperation> operations) throws  ServiceNotActiveException {
            doSubmitUpdateResourceOperations(resourceKey, name, operations);
        }



        public void processUpdateOrCreateResourceOperation(K resourceKey, Object internalRepresentation, RAOperation operation) throws InvalidResourceException, ServiceNotActiveException, RAOperationException {
            doProcessUpdateOrCreateResourceOperation(resourceKey, internalRepresentation, operation);
        }
    }

    /**
     * This class handles all state transitions if the shutdown of the service
     * is in progress.
     * It allows no state transition until shutdown is finished or failed.
     * The follow up state depends on the result of the <code>doStopService</code>
     * method.
     */
    private class ShutdownStateHandler extends IntermediateStateHandler<C> {

        private final boolean freeResources;

        public ShutdownStateHandler(boolean freeResources) {
            this.freeResources = freeResources;
        }

        public TransitionResult<K, C, RA> performTransition() {
            log.entering(ShutdownStateHandler.class.getName(), "performTransition");

            TransitionResult<K, C, RA> ret = null;

            try {
                if (freeResources) {
                    // We don't want additional SLO calculation
                    sloManagerTask.shutdown();
                    freeResources();
                }

                destroyServiceElements();
            } catch (InterruptedException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN),
                        new GrmException("asa.ex.shutdown.failed", ex, BUNDLE, ex.getLocalizedMessage()));
                log.exiting(ShutdownStateHandler.class.getName(), "performTransition", ret);
                return ret;
            }

            ServiceState state = null;
            try {
                state = doStopService(freeResources);
            } catch (GrmException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.ERROR),
                        new GrmException("asa.ex.shutdown.failed", ex, BUNDLE, ex.getLocalizedMessage()));
                log.exiting(ShutdownStateHandler.class.getName(), "performTransition", ret);
                return ret;
            } catch (Throwable ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.ERROR),
                        new GrmException("asa.ex.shutdown.internal", ex, BUNDLE, ex.getLocalizedMessage()));
                log.exiting(ShutdownStateHandler.class.getName(), "performTransition", ret);
                return ret;
            }

            // We allow only ERROR, STOPPED and UNKNOWN
            switch (state) {
                case ERROR:
                case STOPPED:
                case UNKNOWN:
                    ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(state));
                    break;
                default:
                    log.log(Level.SEVERE, "asa.ivss",
                            new Object[]{getName(), getClass().getName(), state, ServiceState.UNKNOWN});
                    ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN));
            }
            log.exiting(ShutdownStateHandler.class.getName(), "performTransition", ret);
            return ret;
        }

        @Override
        public void processUpdateOrCreateResourceOperation(K resourceKey, Object internalRepresentation, RAOperation operation) throws InvalidResourceException, ServiceNotActiveException, RAOperationException {
            doProcessUpdateOrCreateResourceOperation(resourceKey, internalRepresentation, operation);
        }

        @Override
        public void processUpdateResourceOperation(K resourceKey, RAOperation operation) throws UnknownResourceException, ServiceNotActiveException, RAOperationException {
            doProcessUpdateResourceOperation(resourceKey, operation);
        }

        public ServiceState getState() {
            return ServiceState.SHUTDOWN;
        }

        public void startService(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.shutdown.start", BUNDLE);
        }

        public void prepareComponentStartup(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.shutdown.start", BUNDLE);
        }

        public void startComponent(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.shutdown.start", BUNDLE);
        }

        public void stopService(boolean freeResources) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.shutdown.stop", BUNDLE);
        }

        public void prepareComponentShutdown(boolean forced) throws GrmException {
            if (!forced) {
                throw new InvalidStateTransistionException("asa.ex.shutdown.stop", BUNDLE);
            }
        }

        public void stopComponent(boolean forced) throws GrmException {
            log.entering(ShutdownStateHandler.class.getName(), "stopComponent", forced);
            if (forced) {
                try {
                    destroyServiceElements();
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, I18NManager.formatMessage("asa.se.destroy.failed", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
                } finally {
                    doStopService(forced);
                }
            } else {
                throw new InvalidStateTransistionException("asa.ex.shutdown.stop", BUNDLE);
            }
            log.exiting(ShutdownStateHandler.class.getName(), "stopComponent");
        }

        public void prepareComponentReload(C config, boolean forced) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.shutdown.reload", BUNDLE);
        }

        public void reloadComponent(C config, boolean forced) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.shutdown.reload", BUNDLE);
        }

        public void reconnect(Callable<Map<K, Object>> reconnectAction) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException("asa.ex.shutdown.reconnect", BUNDLE);
        }

        @Override
        public String toString() {
            return String.format("ShutdownStateHandler[%s]", getState());
        }
    }

    /**
     * This class handles all state transitions if the service is in reloading
     * state. It allows no state transition until reloading is finished or failed.
     */
    private class ReloadingStateHandler extends IntermediateStateHandler<C> {

        final C config;
        final boolean forced;

        public ReloadingStateHandler(C config, boolean forced) {
            this.config = config;
            this.forced = forced;
        }

        public TransitionResult<K, C, RA> performTransition() {
            log.entering(ReloadingStateHandler.class.getName(), "performTransition");
            TransitionResult<K, C, RA> ret = null;

            //service elements are destroyed by the preloading state handler

            ServiceElements se = null;
            try {
                se = initServiceElements("asa.ex.reloading.failed", config);
            } catch (GrmException ex) {
                log.log(Level.WARNING, "asa.reloading.doStop", getName());
                ServiceState newState;
                try {
                    newState = doStopService(false);
                } catch (Exception ex1) {
                    log.log(Level.SEVERE,
                            I18NManager.formatMessage("asa.reloading.doStop.failed", BUNDLE, getName(), ex1.getLocalizedMessage()),
                            ex1);
                    newState = ServiceState.ERROR;
                }
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(newState), ex);
                return ret;
            }

            Map<K, Object> internalResources = null;
            try {
                internalResources = doReloadService(config, forced);
            } catch (GrmException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN),
                        new GrmException("asa.ex.reloading.failed", ex, BUNDLE, ex.getLocalizedMessage()));
                log.exiting(ReloadingStateHandler.class.getName(), "performTransition", ret);
                return ret;
            } catch (Throwable ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN),
                        new GrmException("asa.ex.reloading.internal", ex, BUNDLE, ex.getLocalizedMessage()));
                log.exiting(ReloadingStateHandler.class.getName(), "performTransition", ret);
                return ret;
            }

            MergeResult<K, RA> mergeResult = null;
            try {
                mergeResult = mergeResources(se.getResourceStore(), se.getResourceFactory(), internalResources);
            } catch (ResourceStoreException ex) {
                log.log(Level.WARNING, "asa.reloading.merge.failed", getName());
                ServiceState newState = null;
                try {
                    newState = doStopService(false);
                } catch (GrmException ex1) {
                    log.log(Level.SEVERE,
                            I18NManager.formatMessage("asa.reloading.doStop.failed", BUNDLE, getName(), ex1.getLocalizedMessage()),
                            ex1);
                    newState = ServiceState.ERROR;
                }
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(newState),
                        new GrmException("asa.ex.reloading.failed", ex, BUNDLE, ex.getLocalizedMessage()));
                log.exiting(ReloadingStateHandler.class.getName(), "performTransition", ret);
                return ret;
            }

            try {
                commitStartup(se, mergeResult);
            } catch (ServiceNotActiveException ex) {
                log.log(Level.SEVERE, "asa.reloading.commit.failed", getName());
                ServiceState newState = null;
                try {
                    newState = doStopService(false);
                } catch (GrmException ex1) {
                    log.log(Level.SEVERE,
                            I18NManager.formatMessage("asa.reloading.doStop.failed", BUNDLE, getName(), ex1.getLocalizedMessage()),
                            ex1);
                    newState = ServiceState.ERROR;
                }
                return new TransitionResult<K, C, RA>(new UnknownStateHandler(newState),
                        new GrmException("asa.ex.reloading.failed", ex, BUNDLE, ex.getLocalizedMessage()));
            }

            ret = new TransitionResult<K, C, RA>(new RunningStateHandler());

            log.exiting(ReloadingStateHandler.class.getName(), "performTransition", ret);
            return ret;
        }

        public ServiceState getState() {
            return ServiceState.UNKNOWN;
        }

        public void startService(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.reloading.start", BUNDLE);
        }

        public void prepareComponentStartup(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.reloading.start", BUNDLE);
        }

        public void startComponent(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.reloading.start", BUNDLE);
        }

        public void stopService(boolean freeResources) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.reloading.stop", BUNDLE);
        }

        public void prepareComponentShutdown(boolean forced) throws GrmException {
            if (!forced) {
                throw new InvalidStateTransistionException("asa.ex.reloading.stop", BUNDLE);
            }
        }

        public void stopComponent(boolean forced) throws GrmException {
            log.entering(ReloadingStateHandler.class.getName(), "stopComponent", forced);
            if (forced) {
                try {
                    destroyServiceElements();
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, I18NManager.formatMessage("asa.se.destroy.failed", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
                } finally {
                    doStopService(forced);
                }
            } else {
                throw new InvalidStateTransistionException("asa.ex.reloading.stop", BUNDLE);
            }
            log.entering(ReloadingStateHandler.class.getName(), "stopComponent", forced);
        }

        public void prepareComponentReload(C config, boolean forced) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.reloading.reload", BUNDLE);
        }

        public void reloadComponent(C config, boolean forced) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.reloading.reload", BUNDLE);
        }

        public void reconnect(Callable<Map<K, Object>> reconnectAction) throws InvalidStateTransistionException {
            throw new InvalidStateTransistionException("asa.ex.reloading.reconnect", BUNDLE);
        }

        @Override
        public String toString() {
            return String.format("ReloadingStateHandler[%s]", getState());
        }
    }

    /**
     * This class handles all state transitions if the service is in pre reloading
     * state. While in this state the service is in state SHUTDOWN. 
     * The state handler is finished after the service elements are destroyed
     * and will change then to ReloadingStateHandler
     */
    private class PreReloadingStateHandler extends ReloadingStateHandler {

        public PreReloadingStateHandler(C config, boolean forced) {
            super(config, forced);
        }

        @Override
        public TransitionResult<K, C, RA> performTransition() {
            log.entering(PreReloadingStateHandler.class.getName(), "performTransition");

            TransitionResult<K, C, RA> ret = null;
            // we will only destroy service elements. The rest is done in the reloading state handler
            try {
                destroyServiceElements();
            } catch (InterruptedException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN),
                        new GrmException("asa.ex.reloading.failed", ex, BUNDLE, ex.getLocalizedMessage()));
                log.exiting(ReloadingStateHandler.class.getName(), "performTransition", ret);
                return ret;
            }

            ret = new TransitionResult<K, C, RA>(new ReloadingStateHandler(config, forced).synchron());
            log.exiting(PreReloadingStateHandler.class.getName(), "performTransition", ret);
            return ret;
        }

        @Override
        public ServiceState getState() {
            return ServiceState.SHUTDOWN; //this state allows to safely finish ongoing resource actions
        }
        //**All other methods are inerited from Reloading state handler
    }

    /**
     * This class handles all state transitions while a reconnect to the 
     * real service is in progress.
     * If the ReconnectStateHandler is active only shutdown and reload of the service
     * is possible
     */
    private class ReconnectStateHandler extends IntermediateStateHandler<C> {

        private final Callable<Map<K, Object>> reconnectAction;
        private AtomicReference<Future<Map<K, Object>>> future = new AtomicReference<Future<Map<K, Object>>>();

        public ReconnectStateHandler(Callable<Map<K, Object>> reconnectAction) {
            this.reconnectAction = reconnectAction;
        }

        public TransitionResult<K, C, RA> performTransition() {
            log.entering(ReconnectStateHandler.class.getName(), "performTransition");
            Future<Map<K, Object>> tmpFuture = null;
            TransitionResult<K, C, RA> ret = null;

            tmpFuture = getExecutorService().submit(reconnectAction);
            future.set(tmpFuture);
            log.log(Level.FINE, "asa.reconnect.started", getName());
            try {
                Map<K, Object> resources = tmpFuture.get();
                if (resources != null) {

                    MergeResult<K, RA> mr = mergeResources(getResourceStore(), resourceFactory.get(), resources);
                    fireEventsFromMergeResult(mr);

                    log.log(Level.FINE, "asa.reconnect.success", getName());
                    return new TransitionResult<K, C, RA>(new RunningStateHandler());
                } else {
                    log.log(Level.FINE, "asa.reconnect.failed", getName());
                    ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN));
                }
            } catch (ServiceNotActiveException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN),
                        new GrmException("asa.ex.reconnect.failed", ex, BUNDLE, ex.getCause().getLocalizedMessage()));
            } catch (ResourceStoreException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN),
                        new GrmException("asa.ex.reconnect.failed", ex, BUNDLE, ex.getCause().getLocalizedMessage()));
            } catch (InterruptedException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN),
                        new GrmException("asa.ex.reconnect.interrupt", ex, BUNDLE));
            } catch (ExecutionException ex) {
                ret = new TransitionResult<K, C, RA>(new UnknownStateHandler(ServiceState.UNKNOWN),
                        new GrmException("asa.ex.reconnect.failed", ex, BUNDLE, ex.getCause().getLocalizedMessage()));
            }
            log.exiting(ReconnectStateHandler.class.getName(), "performTransition", ret);
            return ret;
        }

        private void cancel() {
            Future tmpFuture = future.getAndSet(null);
            if (tmpFuture != null) {
                log.log(Level.FINE, "asa.reconnect.cancel", getName());
                tmpFuture.cancel(true);
            }
        }

        public ServiceState getState() {
            return ServiceState.UNKNOWN;
        }

        public void startService(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.reconnect.start", BUNDLE);
        }

        public void prepareComponentStartup(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.reconnect.start", BUNDLE);
        }

        public void startComponent(C config) throws GrmException {
            throw new InvalidStateTransistionException("asa.ex.reconnect.start", BUNDLE);
        }

        public void stopService(boolean freeResources) throws GrmException {
            cancel();
            changeServiceState(this, new ShutdownStateHandler(freeResources).asynchron());
        }

        public void prepareComponentShutdown(boolean forced) throws GrmException {
            // Shutdown is possible
        }

        public void stopComponent(boolean forced) throws GrmException {
            cancel();
            changeServiceState(this, new ShutdownStateHandler(false).synchron());
        }

        public void prepareComponentReload(C config, boolean forced) throws GrmException {
            // Reload is possible
        }

        public void reloadComponent(C config, boolean forced) throws GrmException {
            cancel();
            changeServiceState(this, new PreReloadingStateHandler(config, forced).synchron());
        }

        public void reconnect(Callable<Map<K, Object>> reconnectAction) throws InvalidStateTransistionException {
            // Reconnect is already running, ignore it
        }

        @Override
        public String toString() {
            return String.format("ReconnectStateHandler[%s]", getState());
        }
    }

    /**
     * Get a snapshot of the current state of the service
     * @return the snapshot
     * @throws com.sun.grid.grm.service.ServiceNotActiveException the current state of the service
     */
    public final ServiceSnapshot getSnapshot() throws ServiceNotActiveException {
        // making a snapshot should work as long ans the resource store is 
        // available
        // => do not need a method in the StateHandler
        return createSnapshot();
    }

    /**
     * Add a resource to the service
     * @param resource  the resource
     * @throws com.sun.grid.grm.resource.InvalidResourceException if the resource is not accepted
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public final void addResource(Resource resource) throws InvalidResourceException, ServiceNotActiveException {
        addResource(resource, null);
    }

    /**
     * Add a resource to the service
     * @param resource  the resource
     * @param sloName   name of the requesting SLO 
     * @throws com.sun.grid.grm.resource.InvalidResourceException if the resource is not accepted
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public final void addResource(Resource resource, String sloName) throws InvalidResourceException, ServiceNotActiveException {
        stateHandler.get().addResource(resource, sloName);
    }

    /**
     * Create a new resource and assign it
     * @param type       the type of the resource
     * @param properties the properties of the resource
     * @throws InvalidResourceException If the resource type is not supported
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the resource properties are not valid
     * @throws com.sun.grid.grm.GrmRemoteException  communication error
     * @throws com.sun.grid.grm.GrmException if the resource could not be created
     * @return the resource
     */
    public final Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
        return stateHandler.get().addNewResource(type, properties);
    }

    /**
     * Remove a resource from the service
     * @param resourceId  the resource
     * @param descr  the descriptor of the resource remove operation
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
     * @throws com.sun.grid.grm.resource.InvalidResourceException if the resource can not be removed
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public final void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, InvalidResourceException, ServiceNotActiveException {
        stateHandler.get().removeResource(resourceId, descr);
    }

    /**
     * Reset a resource.
     * @param resource the resource
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public final void resetResource(ResourceId resource) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
        stateHandler.get().resetResource(resource);
    }

    /**
     * Modify a resource
     * @param resource    the resource
     * @param operations  the operation
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation would create an invalid resource
     */
    public final void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, InvalidResourcePropertiesException {
        stateHandler.get().modifyResourceProperties(resource, operations);
    }

    /**
     * Modify a resource
     * @param resource    the resource
     * @param operation  the operation
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation would create an invalid resource
     */
    public final void modifyResource(ResourceId resource, ResourceChangeOperation operation) throws UnknownResourceException, ServiceNotActiveException, InvalidResourcePropertiesException {
        stateHandler.get().modifyResourceProperties(resource, Collections.singleton(operation));
    }


    /**
     * Set the ambiguous flag of a resource
     * @param resource    the resource
     * @param ambiguous   the new value of the ambiguous flag
     * @param annotation  the annotation
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public final void setAmbiguous(ResourceId resource, boolean ambiguous, String annotation) throws ServiceNotActiveException, UnknownResourceException {
        stateHandler.get().setAmbiguous(resource, ambiguous, annotation);
    }

    /**
     * Get a resource.
     * @param resourceId the id of the resource
     * @return the resource
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public final Resource getResource(ResourceId resourceId) throws UnknownResourceException, ServiceNotActiveException {
        try {
            return getResourceStore().get(resourceId).getResource();
        } catch (ResourceStoreException ex) {
            handleResourceStoreException(ex);
            throw new ServiceNotActiveException();
        }
    }

    /**
     * Get all resources of the service
     * @return the resources
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public final List<Resource> getResources() throws ServiceNotActiveException {
        try {
            return new ArrayList<Resource>(getResourceStore().getResources());
        } catch (ResourceStoreException ex) {
            handleResourceStoreException(ex);
            throw new ServiceNotActiveException();
        }
    }

    /**
     * Get a resource by its key from the resource store.
     * @param resourceKey key of the resource
     * @return the resource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws com.sun.grid.grm.resource.ResourceStoreException if resource store is corrupt
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource was not found in resource store
     */
    protected final Resource getResourceByKey(K resourceKey) throws ServiceNotActiveException, ResourceStoreException, UnknownResourceException {
        return getResourceStore().getByKey(resourceKey).getResource();
    }

    /**
     * Get the amount of resources of the service
     * @return amount of resources in the service
     * @throws ServiceNotActiveException
     */
    public final int getResourceAmount() throws ServiceNotActiveException {
        return getResourceStore().size();
    }

    // ----- Protected Methods -------------------------------------------------
    /**
     * Subclasses should use this method to signalize that the connection to the
     * real service has been lost. 
     * 
     * <p>The service will go into UNKNOWN state and the <code>reconnectAction</code>
     *    is submitted into the <code>ExecutorService</code> of the component.</p>
     * 
     * @param reconnectAction This <code>Callable</code> is used to try a reconnect to
     *                        the real service. It should run until the connection is
     *                        established or until it is interrupted. If the boolean result
     *                        of the <code>Callable</code> is <code>true</code> it is assumed
     *                        that the connection has been successfully established. Any
     *                        <code>Exception</code> in the reconnectAction is caught, the
     *                        service will stay in UNKNOWN state
     * @throws com.sun.grid.grm.GrmException if the reconnect was not possible
     */
    protected void reconnect(Callable<Map<K, Object>> reconnectAction) throws GrmException {
        log.entering(AbstractServiceAdapter.class.getName(), "reconnect");
        stateHandler.get().reconnect(reconnectAction);
        log.exiting(AbstractServiceAdapter.class.getName(), "reconnect");
    }

    /**
     * Get the ServiceEventSupport element for the service adapter
     * @return the ServiceEventSupport
     */
    protected final ServiceEventSupport getServiceEventSupport() {
        return ((AbstractServiceContainer) getComponent()).getServiceEventSupport();
    }

    /**
     * Subclasses performs the necessary actions for starting the service
     * in this method.
     * 
     * @param config the configuration of the service
     * @return If null is returned the AbstractServiceAdapter assumes that
     *         the implementing class has no glue what resources the real service 
     *         has. In this case it simply uses the ResourceAdapters stored in 
     *         the ResourceAdapterStore.
     *         if a non null values is returned the AbstractServiceAdapter merges the
     *         returned Set with the resources in the ResourceAdapterStore and generates
     *         the outstanding events for the missing/added resources
     * @throws com.sun.grid.grm.GrmException if the startup failed
     */
    protected abstract Map<K, Object> doStartService(C config) throws GrmException;

    /**
     * Subclasses performs in this method the necessary actions for stopping the service.
     *
     * @param forced if <code>true</code>  if the service should the stopped in forced mode (
     *                      this means during the disable no expensive operations should be executed)
     * @return The resulting <code>ServiceState</code> (UNKNOWN,  STOPPED or ERROR)
     * @throws com.sun.grid.grm.GrmException if the service could not be stopped
     */
    protected abstract ServiceState doStopService(boolean forced) throws GrmException;

    /**
     * Subclasses performs in this method the necessary actions to reload the service
     * @param config the new configuration of the service 
     * @param forced if <code>true</code> force the reload
     * @return If null is returned the AbstractServiceAdapter assumes that
     *         the implementing class has no glue what resources the real service 
     *         has. In this case it simply uses the ResourceAdapters stored in 
     *         the ResourceAdapterStore.
     *         if a non null values is returned the AbstractServiceAdapter merges the
     *         returned Set with the resources in the ResourceAdapterStore and generates
     *         the outstanding events for the missing/added resources
     * @throws com.sun.grid.grm.GrmException if the reload failed
     */
    protected abstract Map<K, Object> doReloadService(C config, boolean forced) throws GrmException;

    /**
     * Process an update resource operation.
     * 
     * Subclasses can use this method to perform additional operations on a resource. If
     * the resource does not exists, it is created.
     * 
     * The call of this method block until the resource action has been executed
     * <h2>Attention:</h2>
     * 
     * The AbstractServiceAdapter executes resource action for a resource sequential.
     * If a resource operation blocks the complete service adapter can be blocked.
     * 
     * @param resourceKey the key of the resource in the resource store
     * @param operation   the operation
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws UnknownResourceException if the resource is unknown
     * @throws RAOperationException If the operation failed
     */
    protected final void processUpdateResourceOperation(K resourceKey, RAOperation operation)
            throws UnknownResourceException, ServiceNotActiveException, RAOperationException {
        stateHandler.get().processUpdateResourceOperation(resourceKey, operation);
    }

    /**
     * Submit a patch of resource operations
     *
     * Subclasses can use this method to perform additional operations on a resource.
     *
     * The AbstractServiceAdapter submits on resource action for all operations into
     * the resource action executor. 
     *
     * @param resourceKey the key of the resource in the resource store
     * @param operations   the iterator over the operations
     * @param name        the name of the batch operation
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    protected final void submitUpdateResourceOperations(K resourceKey, String name, Iterable<RAOperation> operations)
                    throws ServiceNotActiveException {
        stateHandler.get().submitUpdateResourceOperations(resourceKey, name, operations);
    }

    /**
     * Process a resource operation that update an resource. If the resource
     * does not exists in the resource store it adds it before invoking
     * the operation.
     * 
     * @param resourceKey  the key of the resource in the resource store
     * @param internalRepresentation  the internal representation of the resource
     * @param operation  the resource operation
     * @throws InvalidResourceException if the operation would produce an invalid resource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws RAOperationException If the operation failed
     */
    protected void processUpdateOrCreateResourceOperation(K resourceKey, Object internalRepresentation, RAOperation operation)
            throws InvalidResourceException, ServiceNotActiveException, RAOperationException {
        stateHandler.get().processUpdateOrCreateResourceOperation(resourceKey, internalRepresentation, operation);
    }

    /**
     * Get the timeout for waiting until all resources are freed.
     * 
     * <p>If the service is stopped with the freeResource flag this action will be blocked
     * until all possible resources are freed or until the freeResourcesTimeout has
     * elapsed.</p>
     * 
     * @return the timeout in milliseconds
     */
    protected abstract long getFreeResourcesTimeout();

    /**
     * Create the ResourceAdapterStore for this service
     * @return the ResourceAdapterStore
     */
    protected abstract ResourceAdapterStore<K, RA> createResourceAdapterStore(C config);

    /**
     * Create the SLOManager for this service
     * @return the SLO manager
     */
    protected abstract RunnableSLOManager createSLOManager();


    /**
     * Creates a new resource factory.
     * @return
     */
    protected abstract ResourceFactory<RA> createResourceFactory();

    /**
     * Create from the configuration of the service the setup for the SLO manager.
     * 
     * @param config the configuration for the service
     * @return the setup of the SLOManager
     * @throws com.sun.grid.grm.GrmException if the configuration is invalid
     */
    protected abstract RunnableSLOManagerSetup createSLOManagerSetup(C config) throws GrmException;

    /**
     * Check that the service is active.
     * 
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    @SuppressWarnings("unchecked")
    protected final void checkServiceActive() throws ServiceNotActiveException {
        if (((AbstractServiceContainer) getComponent()).getServiceState() != ServiceState.RUNNING) {
            throw new ServiceNotActiveException();
        }
    }

    protected ResourceAdapterStore<K, RA> getResourceStore() throws ServiceNotActiveException {
        ResourceAdapterStore<K, RA> ret = resourceStore.get();
        if (ret == null) {
            throw new ServiceNotActiveException();
        }
        return ret;
    }
    
    // ----- Private Methods ---------------------------------------------------
    private ResourceActionExecutor<K> getResourceActionExecutor() throws ServiceNotActiveException {
        ResourceActionExecutor<K> ret = resourceActionExecutor.get();
        if (ret == null) {
            throw new ServiceNotActiveException();
        }
        return ret;
    }

    private void destroyServiceElements() throws InterruptedException {
        try {
            sloManagerTask.shutdown();
        } finally {
            try {
                ResourceActionExecutor re = resourceActionExecutor.getAndSet(null);
                if (re != null) {
                    re.stop();
                }
            } finally {
                resourceStore.set(null);
            }
        }
        log.log(Level.FINE, "asa.se.destroyed", getName());
    }

    private ServiceElements initServiceElements(String message, C config) throws GrmException {

        ResourceFactory<RA> rf = createResourceFactory();

        log.log(Level.FINE, "asa.se.openRS", getName());
        ResourceAdapterStore<K, RA> tmpResourceStore = null;
        try {
            tmpResourceStore = createResourceAdapterStore(config);
            tmpResourceStore.load();

        } catch (ResourceStoreException ex) {
            throw new GrmException(message, ex, BUNDLE, ex.getLocalizedMessage());
        }

        log.log(Level.FINE, "asa.se.setupSLO", getName());
        RunnableSLOManagerSetup sloSetup = null;
        try {
            sloSetup = createSLOManagerSetup(config);
        } catch (GrmException ex) {
            throw new GrmException(message, ex, BUNDLE, ex.getLocalizedMessage());
        }

        log.log(Level.FINE, "asa.se.createRE", getName());
        ResourceActionExecutor<K> tmpResourceActionExecutor = new ResourceActionExecutor<K>(getName(), getClass().getClassLoader());

        return new ServiceElements(tmpResourceStore, tmpResourceActionExecutor, sloSetup, rf);
    }

    private void fireEventsFromMergeResult(MergeResult<K, RA> mergeResult) {

        lock.lock();
        try {
            // Send events about removed resources
            for (ResourceAdapter ra : mergeResult.getRemoved()) {
                Resource r = ra.getResource();
                getServiceEventSupport().fireRemoveResource(r, I18NManager.formatMessage("asa.merge.removed", BUNDLE));
                getServiceEventSupport().fireResourceRemoved(r, I18NManager.formatMessage("asa.merge.removed", BUNDLE));
            }

            // Send events about added resources
            for (ResourceAdapter ra : mergeResult.getAdded()) {
                Resource r = ra.getResource();
                getServiceEventSupport().fireAddResource(r, I18NManager.formatMessage("asa.merge.added", BUNDLE));
                switch(r.getState()) {
                    case ASSIGNED:
                        getServiceEventSupport().fireResourceAdded(r, I18NManager.formatMessage("asa.merge.added", BUNDLE));
                        break;
                    case ERROR:
                        getServiceEventSupport().fireResourceError(r, "");
                        break;
                    default:
                        // Ignore
                }
                
            }
        } finally {
            lock.unlock();
        }
    }

    private void commitStartup(ServiceElements se, MergeResult<K, RA> mergeResult) throws ServiceNotActiveException {
        log.entering(AbstractServiceAdapter.class.getName(), "commitStartup");

        lock.lock();
        try {
            resourceFactory.set(se.getResourceFactory());
            resourceActionExecutor.set(se.getResourceActionExecutor());
            resourceStore.set(se.getResourceStore());
            fireEventsFromMergeResult(mergeResult);
        } finally {
            lock.unlock();
        }

        // Finally activate the SLO manager
        sloManagerTask.reconfigure(se.getSLOManagerSetup());
        sloManagerTask.start();

        log.exiting(AbstractServiceAdapter.class.getName(), "commitStartup");
    }

    private MergeResult<K, RA> mergeResources(ResourceAdapterStore<K, RA> tmpResourceStore, ResourceFactory<RA> resFac, Map<K, Object> internalResources)
            throws ResourceStoreException {
        log.log(Level.FINE, "asa.merge", getName());
        MergeResult<K, RA> ret = null;
        if (internalResources == null) {
            // ServiceAdapter has no clue what resource the real service has
            ret = new MergeResult<K, RA>(Collections.<RA>emptySet(), Collections.<RA>emptySet());
        } else {

            Set<K> addedKeys = new HashSet<K>(internalResources.keySet());
            Set<RA> removed = new HashSet<RA>();

            for (RA rc : tmpResourceStore.getResourceAdapters()) {
                if (!addedKeys.remove(rc.getKey())) {
                    removed.add(rc);
                }
            }

            Set<RA> added = new HashSet<RA>(addedKeys.size());
            for (K key : addedKeys) {
                try {
                    RA ra = resFac.createResourceAdapter(getExecutionEnv(), internalResources.get(key));
                    added.add(ra);
                } catch (GrmException ex) {
                    throw new ResourceStoreException("asa.ex.cr", ex, BUNDLE, key, ex.getLocalizedMessage());
                }
            }

            for (RA ra : added) {
                tmpResourceStore.store(ra);
            }

            for (RA rc : removed) {
                try {
                    tmpResourceStore.remove(rc.getId(),ResourceRemovalFromSystemDescriptor.getInstance(true));
                } catch (UnknownResourceException ex) {
                    throw new IllegalStateException("Resource " + rc.getId() + " must be in the storage");
                }
            }

            ret = new MergeResult<K, RA>(added, removed);
        }
        if (log.isLoggable(Level.FINER)) {
            log.log(Level.FINER, "asa.merged", new Object[]{getName(), ret.toString()});
        }
        return ret;
    }

    private void freeResources() {
        log.entering(AbstractServiceAdapter.class.getName(), "freeResources");
        try {
            // Try to free all non static resource
            List<UninstallAction> uninstallAction = new LinkedList<UninstallAction>();

            ResourcesFreedBarrier waitForResourceRemoved = new ResourcesFreedBarrier(getName());
            ServiceEventSupport evtSup = getServiceEventSupport();
            evtSup.addServiceEventListener(waitForResourceRemoved);
            try {
                lock.lock();
                try {
                    for (RA ra : getResourceStore().getResourceAdaptersToFree()) {
                        ResourceTransition transition;
                        try {
                            transition = ra.prepareUninstall(ResourceReassignmentDescriptor.INSTANCE);
                            commitResourceTransition(ra, transition);
                            UninstallAction action = new UninstallAction(ra.getKey(), ra.getId(), ResourceReassignmentDescriptor.INSTANCE, transition);
                            waitForResourceRemoved.addResource(action.getResourceId());
                            try {
                                getResourceActionExecutor().submit(action);
                                uninstallAction.add(action);
                            } catch (ServiceNotActiveException ex) {
                                waitForResourceRemoved.removeResource(action.getResourceId());
                                throw ex;
                            }
                        } catch (InvalidResourceStateException ex) {
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE,
                                        I18NManager.formatMessage("asa.freeRes.notFreed", BUNDLE, getName(), ra.getId(), ex.getLocalizedMessage()),
                                        ex);
                            }
                        }
                    }
                } finally {
                    lock.unlock();
                }
                for (UninstallAction action : uninstallAction) {
                    try {
                        action.getResult();
                    } catch (ExecutionException ex) {
                        log.log(Level.WARNING,
                                I18NManager.formatMessage("asa.freeRes.notFreed",
                                BUNDLE, getName(), action.getResourceId(),
                                ex.getCause().getLocalizedMessage()),
                                ex);
                    } catch (CancellationException ex) {
                        log.log(Level.WARNING,
                                I18NManager.formatMessage("asa.freeRes.notFreed",
                                BUNDLE, getName(), action.getResourceId(),
                                ex.getLocalizedMessage()),
                                ex);
                    }
                }
                waitForResourceRemoved.waitForAllEvents(getFreeResourcesTimeout(), TimeUnit.MILLISECONDS);
            } finally {
                evtSup.removeServiceEventListener(waitForResourceRemoved);
            }
        } catch (InterruptedException ex) {
            log.log(Level.WARNING, "asa.transitionFinished", getName());
        } catch (ServiceNotActiveException ex) {
            // Should not happen, because destroyServiceElements has not been called
            log.log(Level.WARNING, I18NManager.formatMessage("asa.freeRes.error", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
        } catch (ResourceStoreException ex) {
            log.log(Level.WARNING, I18NManager.formatMessage("asa.freeRes.error", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
        }
        log.exiting(AbstractServiceAdapter.class.getName(), "freeResources");
    }

    private void handleResourceStoreException(ResourceStoreException ex) {
        log.log(Level.SEVERE,
                I18NManager.formatMessage("asa.rspanic", BUNDLE, getName(), ex.getLocalizedMessage()),
                ex);
        try {
            getComponent().stop(false);
        } catch (GrmException ex1) {
            log.log(Level.SEVERE,
                    I18NManager.formatMessage("asa.rspanic.sdf", BUNDLE, getName(), ex1.getLocalizedMessage()),
                    ex1);
        }
    }

    private void changeServiceState(StateHandler<K, C, RA> oldState, final IntermediateStateHandler<C> newState)
            throws GrmException {
        // newestState is needed to get a reference to the last IntermediateStateHandler that is cycled through
        final AtomicReference<IntermediateStateHandler<C>> newestState = new AtomicReference<IntermediateStateHandler<C>>(newState);
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceAdapter.class.getName(), "changeState",
                    new Object[]{oldState, newestState.get()});
        }

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "asa.triggerTrans", new Object[]{getName(), oldState, newestState.get(), newestState.get().isSynchron()});
        }
        lock.lock();
        try {
            stateHandler.set(newestState.get());
            fireStateChanged(oldState, newestState.get());
        } finally {
            lock.unlock();
        }

        Callable<TransitionResult<K, C, RA>> r = new Callable<TransitionResult<K, C, RA>>() {

            @SuppressWarnings("unchecked") // for the (IntermediateStateHandler<C>) cast 
            public TransitionResult<K, C, RA> call() {
                TransitionResult<K, C, RA> res = null;
                boolean loop = false;
                /* This loop will cycle to a sequence of intermediate state handlers 
                 * like PreReloadingStateHandler ==> ReloadingStateHandler
                 */
                do {
                    res = newestState.get().performTransition();

                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "asa.transitionFinished",
                                new Object[]{getName(), newestState.get(), res.getFollowState()});
                    }

                    lock.lock();
                    try {
                        stateHandler.set(res.getFollowState());
                        fireStateChanged(newestState.get(), res.getFollowState());
                    } finally {
                        lock.unlock();
                    }
                    if (res.getError() != null) {
                        log.log(Level.SEVERE, res.getError().getLocalizedMessage(), res.getError());
                    }
                    //if there are two intermediate state handler chained, cycle through them and finish with the last
                    if (res.getFollowState() instanceof IntermediateStateHandler) {
                        newestState.set((IntermediateStateHandler<C>) res.getFollowState());
                        loop = true;
                    } else {
                        loop = false;//this avoids endless loop
                    }
                } while (loop);
                return res;
            }
        };

        try {
            if (newestState.get().isSynchron()) {
                TransitionResult<K, C, RA> res = null;
                // Note: It is important that the transition itself runs in
                //       the ExecutorService because the adapter can rely in
                //       on a special context class loader which has been setup
                //       in the ExecutorServiceFactory.

                Future<TransitionResult<K, C, RA>> f = null;
                try {
                    f = getExecutorService().submit(r);
                } catch (Exception ex) {
                    // We are in shutdown phase, run it directly
                    res = r.call();
                }

                if (f != null) {
                    res = f.get();
                }

                if (res.getError() != null) {
                    throw res.getError();
                }
            } else {
                getExecutorService().submit(r);
            }
        } catch (Throwable ex) {
            throw new GrmException("asa.ex.unexpected", ex, BUNDLE, getName(), oldState, newestState.get(), ex.getLocalizedMessage());
        }

        log.exiting(AbstractServiceAdapter.class.getName(), "changeState");
    }

    private void commitResourceTransition(ResourceAdapter ra, ResourceTransition transition) throws ResourceStoreException, ServiceNotActiveException {
        if (transition.commit(ra, getResourceStore(), getServiceEventSupport())) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "asa.commit.res",
                        new Object[]{getName(), ra.getId(), transition.toString()});
            }
        }
    }

    private void fireStateChanged(StateHandler<K, C, RA> oldState, StateHandler<K, C, RA> newState) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractServiceAdapter.class.getName(), "fireStateChanged", new Object[]{oldState, newState});
        }
        if (!oldState.getState().equals(newState.getState())) {
            switch (newState.getState()) {
                case STARTING:
                    getServiceEventSupport().fireServiceStarting();
                    break;
                case RUNNING:
                    getServiceEventSupport().fireServiceRunning();
                    break;
                case SHUTDOWN:
                    getServiceEventSupport().fireServiceShutdown();
                    break;
                case STOPPED:
                    getServiceEventSupport().fireServiceStopped();
                    break;
                case ERROR:
                    getServiceEventSupport().fireServiceError();
                    break;
                case UNKNOWN:
                    getServiceEventSupport().fireServiceUnknown();
                    break;
                default:
                    throw new IllegalStateException("Unknown service state " + newState);
            }
        }
        log.exiting(AbstractServiceAdapter.class.getName(), "fireStateChanged");
    }

    public ServiceSnapshot createSnapshot() throws ServiceNotActiveException {
        ServiceSnapshot ret = null;
        try {
            lock.lock();
            try {
                ret = new DefaultServiceSnapshot(getServiceEventSupport().getSequenceNumberOfNextEvent(),
                        AbstractServiceAdapter.this.getState(),
                        getServiceState(),
                        getResourceStore().getResources(),
                        resourceRequestMap);
            } finally {
                lock.unlock();
            }
        } catch (ResourceStoreException ex) {
            handleResourceStoreException(ex);
            throw new ServiceNotActiveException();
        }
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "asa.snapshot", new Object[]{getName(), ret});
        }
        return ret;
    }

    private void doProcessUpdateOrCreateResourceOperation(K resourceKey, Object internalRepresentation, RAOperation operation) throws ServiceNotActiveException, InvalidResourceException, RAOperationException {
        log.entering(AbstractServiceAdapter.class.getName(), "doProcessUpdateOrCreateResourceOperation");

        if (resourceKey == null) {
            throw new NullPointerException("resourceKey must not be null");
        }
        if (operation == null) {
            throw new NullPointerException("operation must not be null");
        }
        ProcessUpdateOrCreateResourceOperationAction action = new ProcessUpdateOrCreateResourceOperationAction(resourceKey, internalRepresentation, operation);
        getResourceActionExecutor().submit(action);

        try {
            action.getResult();
        } catch (InterruptedException ex) {
            throw new ServiceNotActiveException();
        } catch (CancellationException ex) {
            throw new ServiceNotActiveException();
        } catch (ExecutionException ex) {
            if (ex.getCause() instanceof ServiceNotActiveException) {
                throw (ServiceNotActiveException) ex.getCause();
            } else if (ex.getCause() instanceof InvalidResourceException) {
                throw (InvalidResourceException) ex.getCause();
            } else if (ex.getCause() instanceof ResourceStoreException) {
                handleResourceStoreException((ResourceStoreException) ex.getCause());
                throw new ServiceNotActiveException();
            } else if (ex.getCause() instanceof RAOperationException) {
                throw (RAOperationException) ex.getCause();
            } else {
                throw new InvalidResourceException("asa.ex.ra.failed", ex, BUNDLE, ex.getLocalizedMessage());
            }
        }
        log.exiting(AbstractServiceAdapter.class.getName(), "doProcessUpdateOrCreateResourceOperation");
    }

    private void doProcessUpdateResourceOperation(K resourceKey, RAOperation operation) throws ServiceNotActiveException, RAOperationException, UnknownResourceException {
        log.entering(AbstractServiceAdapter.class.getName(), "doProcessUpdateResourceOperation");
        ProcessUpdateResourceOperationAction action = new ProcessUpdateResourceOperationAction(resourceKey, operation);
        getResourceActionExecutor().submit(action);

        try {
            action.getResult();
        } catch (InterruptedException ex) {
            throw new ServiceNotActiveException();
        } catch (CancellationException ex) {
            throw new ServiceNotActiveException();
        } catch (ExecutionException ex) {
            if (ex.getCause() instanceof ServiceNotActiveException) {
                throw (ServiceNotActiveException) ex.getCause();
            } else if (ex.getCause() instanceof UnknownResourceException) {
                throw (UnknownResourceException) ex.getCause();
            } else if (ex.getCause() instanceof ResourceStoreException) {
                handleResourceStoreException((ResourceStoreException) ex.getCause());
                throw new ServiceNotActiveException();
            } else if (ex.getCause() instanceof RAOperationException) {
                throw (RAOperationException) ex.getCause();
            } else {
                throw new RAOperationException("asa.ex.ra.failed", ex, BUNDLE, ex.getLocalizedMessage());
            }
        }
        log.exiting(AbstractServiceAdapter.class.getName(), "doProcessUpdateResourceOperation");
    }

    private void doSubmitUpdateResourceOperations(K resourceKey, String operationName, Iterable<RAOperation> operations) throws ServiceNotActiveException {
        BatchProcessUpdateResourceOperationAction action = new BatchProcessUpdateResourceOperationAction(resourceKey, operationName, operations);
        getResourceActionExecutor().submit(action);
    }


    // ---- Inner classes ------------------------------------------------------
    /**
     * Abstract implementation of a a ResourceAction which is part of a 
     * resource transition
     * @param <R> the result type of the ResourceAction
     */
    private abstract class AbstractTransitionResourceAction<R> extends AbstractResourceAction<R, K> {

        private final ResourceTransition transition;
        private final ResourceId resourceId;

        public AbstractTransitionResourceAction(K resourceKey, ResourceId resId, String actionName, ResourceTransition transition) {
            super(resourceKey, String.format("%s(trans=%d)", actionName, transition.getId()));
            this.transition = transition;
            this.resourceId = resId;
        }

        public ResourceTransition getTransition() {
            return transition;
        }

        public ResourceId getResourceId() {
            return resourceId;
        }
    }

    /**
     * This action performs the installation of a Resource over a ResourceAdapter
     */
    private class InstallAction extends AbstractTransitionResourceAction<Void> {

        private final String sloName;

        public InstallAction(K resourceKey, ResourceId resId, ResourceTransition transition, String sloName) {
            super(resourceKey, resId, "Install", transition);
            this.sloName = sloName;
        }

        public Void doExecute() {
            try {
                RA ra = getResourceStore().get(getResourceId());
                ResourceTransition transition = ra.install(getTransition());
                sloManagerTask.disable();
                try {
                    commitResourceTransition(ra, transition);
                } finally {
                    sloManagerTask.enable(true);
                }
            } catch (UnknownResourceException ex) {
                log.log(Level.WARNING, "asa.install.removed", new Object[]{getName(), getResourceId()});
            } catch (ServiceNotActiveException ex) {
                log.log(Level.WARNING, "asa.install.notActive", new Object[]{getName(), getResourceId()});
            } catch (InvalidResourceStateException ex) {
                log.log(Level.WARNING,
                        I18NManager.formatMessage("asa.install.error", BUNDLE, getName(), getResourceId(), ex.getLocalizedMessage()),
                        ex);
            } catch (ResourceStoreException ex) {
                log.log(Level.SEVERE,
                        I18NManager.formatMessage("asa.install.error", BUNDLE, getName(), getResourceId(), ex.getLocalizedMessage()),
                        ex);
            }
            return null;
        }
    }

    /**
     * This action perform the uninstall of a resource with the ResourceAdapter
     */
    private class UninstallAction extends AbstractTransitionResourceAction<Void> {

        private final ResourceRemovalDescriptor descr;

        public UninstallAction(K resourceKey, ResourceId resourceId, ResourceRemovalDescriptor descr, ResourceTransition transition) {
            super(resourceKey, resourceId, "Uninstall", transition);
            this.descr = descr;
        }

        public Void doExecute() {
            try {
                RA ra = getResourceStore().get(getResourceId());
                ResourceTransition transition = ra.uninstall(getTransition(), descr);
                sloManagerTask.disable();
                try {
                    commitResourceTransition(ra, transition);
                } finally {
                    sloManagerTask.enable(true);
                }
            } catch (UnknownResourceException ex) {
                log.log(Level.WARNING, "asa.uninstall.removed", new Object[]{getName(), getResourceId()});
            } catch (ServiceNotActiveException ex) {
                log.log(Level.WARNING, "asa.uninstall.notActive", new Object[]{getName(), getResourceId()});
            } catch (InvalidResourceStateException ex) {
                log.log(Level.WARNING,
                        I18NManager.formatMessage("asa.uninstall.error", BUNDLE, getName(), getResourceId(), ex.getLocalizedMessage()),
                        ex);
            } catch (ResourceStoreException ex) {
                log.log(Level.WARNING,
                        I18NManager.formatMessage("asa.uninstall.error", BUNDLE, getName(), getResourceId(), ex.getLocalizedMessage()),
                        ex);
            }
            return null;
        }
    }

    /**
     * This action performs the reset of a resource with the ResourceAdapter
     */
    private class ResetAction extends AbstractTransitionResourceAction<Void> {

        public ResetAction(K resourceKey, ResourceId resId, ResourceTransition transition) {
            super(resourceKey, resId, "Reset", transition);
        }

        public Void doExecute() {
            try {
                RA ra = getResourceStore().get(getResourceId());
                ResourceTransition transition = ra.reset(getTransition());
                sloManagerTask.disable();
                try {
                    commitResourceTransition(ra, transition);
                } finally {
                    sloManagerTask.enable(true);
                }
            } catch (UnknownResourceException ex) {
                log.log(Level.WARNING, "asa.reset.removed", new Object[]{getName(), getResourceId()});
            } catch (ServiceNotActiveException ex) {
                log.log(Level.WARNING, "asa.reset.notActive", new Object[]{getName(), getResourceId()});
            } catch (InvalidResourceStateException ex) {
                log.log(Level.WARNING,
                        I18NManager.formatMessage("asa.reset.error", BUNDLE, getName(), getResourceId(), ex.getLocalizedMessage()),
                        ex);
            } catch (ResourceStoreException ex) {
                log.log(Level.WARNING,
                        I18NManager.formatMessage("asa.reset.error", BUNDLE, getName(), getResourceId(), ex.getLocalizedMessage()),
                        ex);
            }
            return null;
        }
    }

    /**
     * This action is used to update a resource. If the resource has no ResourceAdapter
     * in the resource store, it is created automatically.
     */
    private class ProcessUpdateOrCreateResourceOperationAction extends AbstractResourceAction<Void, K> {

        private final RAOperation operation;
        private final Object internalRepresentation;

        public ProcessUpdateOrCreateResourceOperationAction(K resourceKey, Object internalRepresentation, RAOperation operation) {
            super(resourceKey, operation.getOperationName());
            this.operation = operation;
            this.internalRepresentation = internalRepresentation;
        }

        @Override
        protected Void doExecute() throws ServiceNotActiveException, InvalidResourceException, ResourceStoreException, RAOperationException, InvalidResourcePropertiesException, GrmException {
            RA ra = null;
            ResourceTransition transition = null;
            sloManagerTask.disable();
            try {
                lock.lock();
                try {
                    ra = getResourceStore().getByKey(getResourceKey());
                } catch (UnknownResourceException ex) {
                    // Resource is unknown, it's an auto discovered resource
                    ra = resourceFactory.get().createResourceAdapter(getExecutionEnv(), internalRepresentation);
                    AddResourceOPR opr = new AddResourceOPR(ra.getResource(), I18NManager.formatMessage("asa.autodiscovered", BUNDLE));
                    DefaultResourceTransition t = new DefaultResourceTransition(0, opr);
                    commitResourceTransition(ra, t);
                } finally {
                    lock.unlock();
                }
                transition = operation.execute(ra);
                if (transition.hasModifiedResource()) {
                    lock.lock();
                    try {
                        commitResourceTransition(ra, transition);
                    } finally {
                        lock.unlock();
                    }
                }
            } finally {
                sloManagerTask.enable(transition != null ? transition.hasModifiedResource() : false);
            }
            return null;
        }
    }

    /**
     * This action is used to update an existing resource
     */
    private class ProcessUpdateResourceOperationAction extends AbstractResourceAction<Void, K> {

        private final RAOperation operation;

        public ProcessUpdateResourceOperationAction(K resourceKey, RAOperation operation) {
            super(resourceKey, operation.getOperationName());
            this.operation = operation;
        }

        @Override
        protected Void doExecute() throws ServiceNotActiveException, InvalidResourceException, ResourceStoreException, RAOperationException, UnknownResourceException {
            RA ra = getResourceStore().getByKey(getResourceKey());
            ResourceTransition transition = operation.execute(ra);
            if (transition.hasModifiedResource()) {
                sloManagerTask.disable();
                try {
                    lock.lock();
                    try {
                        commitResourceTransition(ra, transition);
                    } finally {
                        lock.unlock();
                    }
                } finally {
                    sloManagerTask.enable(true);
                }
            }
            return null;
        }
    }

    private class BatchProcessUpdateResourceOperationAction extends AbstractResourceAction<Void, K> {

        private final Iterable<RAOperation> operations;
        
        public BatchProcessUpdateResourceOperationAction(K resourceKey, String name, Iterable<RAOperation> operations) {
            super(resourceKey, name);
            this.operations = operations;
        }

        @Override
        protected Void doExecute() throws ServiceNotActiveException, UnknownResourceException, ResourceStoreException, RAOperationException {
            RA ra = getResourceStore().getByKey(getResourceKey());

            for(RAOperation operation: operations) {
                ResourceTransition transition = operation.execute(ra);
                lock.lock();
                try {
                    commitResourceTransition(ra, transition);
                } finally {
                    lock.unlock();
                }
            }
            return null;
        }

        
    }

    // ---- Inner classes for SLO handling -------------------------------------
    /**
     * This class encapsulates all actions in the SLOManager
     * 
     * <p>It provides it's own single threaded ExecutorService which is used to
     * run the SLO calculation.</p>
     * 
     * <h2>Disable/Enable SLO processing</h2>
     * 
     * <p>The SLO processing can be disabled with the <code>disable</code> method.
     *    If the SLO processing is disabled the SLOManager is stopped
     *    and the SLO listener ignore the incoming events from the SLOManager.
     *    The disable method stores all disabling threads. Once all disabling 
     *    threads have called the <code>enable</code> method the SLO processing 
     *    will continue.</p>
     * 
     * <h2>Thread safeness</h2>
     * 
     * It has it's only lock (the sloLock), hence modifications on the SLOManager
     * must no be triggered within the global lock
     */
    private class SLOManagerTask {

        private final RunnableSLOManager sloManager;
        private final SLOListener sloListener;
        private final Lock sloLock = new ReentrantLock();
        private final Set<Thread> disablingThreads = new HashSet<Thread>();
        //private ExecutorService executor;
        private Future sloFuture;

        public SLOManagerTask() {
            sloManager = createSLOManager();
            sloListener = new SLOListener();
            sloManager.addSLOManagerListener(sloListener);
        }

        /**
         * Disable the SLO processing.
         * 
         * <p>Interrupts an ongoing SLO calculation and stops the processing
         *    of incoming events from the SLOManger</p>
         * <p>This method stores all threads which has called this method
         *    in the <code>disablingThreads</code>.</p>
         */
        public void disable() {
            boolean isStoppingThread;
            sloLock.lock();
            try {
                sloListener.setIngoreEvents(true);
                isStoppingThread = disablingThreads.isEmpty();
                disablingThreads.add(Thread.currentThread());
            } finally {
                sloLock.unlock();
            }
            if (isStoppingThread) {
                disableSLOManager();
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "asa.slo.disable",
                            new Object[]{getName(), Thread.currentThread().getName()});
                }
            }
        }

        private boolean triggerUpdate;
        
        /**
         * Enable the SLO processing.
         * 
         * <p>This method removes the current thread from the <code>disablingThreads</code>
         *    set. If this set is empty it re allows the processing of SLOManager events
         *    and triggers an new SLO run</p>.
         */
        public void enable(boolean triggerUpdate) {
            boolean enablingThread = false;
            boolean doTriggerUpdate = false;
            sloLock.lock();
            try {
                disablingThreads.remove(Thread.currentThread());
                this.triggerUpdate |= triggerUpdate;
                if (disablingThreads.isEmpty()) {
                    sloListener.setIngoreEvents(false);
                    enablingThread = true;
                    doTriggerUpdate = this.triggerUpdate;
                    this.triggerUpdate = false;
                }
            } finally {
                sloLock.unlock();
            }
            if (enablingThread) {
                enableSLOManager();
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "asa.slo.enable",
                            new Object[]{getName(), Thread.currentThread().getName()});
                }
                if (doTriggerUpdate) {
                    sloManager.triggerUpdate();
                }
            }
        }
        private void disableSLOManager() {
            log.log(Level.FINE, "asa.slo.disableManager", getName());
            sloManager.disable();
        }

        /**
         * This helper method enables the SLOManager
         * Further calls of the SLOManager#update method
         * will perform an SLO run
         * 
         * In addition it clears the resourceRequestMap, so that the
         * after restarting the SLOManager at least on resource request
         * for each SLO is sent to the ServiceEventListeners
         */
        private void enableSLOManager() {
            lock.lock();
            try {
                resourceRequestMap.clear();
            } finally {
                lock.unlock();
            }
            log.log(Level.FINE, "asa.slo.enableManager", getName());
            sloManager.enable();
        }

        /**
         * Get the SLO manager
         * @return the SLO manager
         */
        public RunnableSLOManager getSLOManager() {
            return sloManager;
        }

        /**
         * Reconfigure the SLOManager
         * @param setup  the setup for the SLOManager
         */
        public void reconfigure(RunnableSLOManagerSetup setup) {
            sloManager.setUpdateInterval(setup.getUpdateInterval());
            sloManager.setSLOs(setup.getSlos());
        }

        public void start() throws ServiceNotActiveException {
            // Call first startSLOManager
            // This call changes the interal state of the SLO manager
            // to IDLE, the next call of update method will execute an SLO run
            enableSLOManager();
            sloLock.lock();
            try {
                sloListener.setIngoreEvents(false);
                sloFuture = getExecutorService().submit(sloManager);
            } catch (Exception ex) {
                throw new ServiceNotActiveException();
            } finally {
                sloLock.unlock();
            }
        }

        public void shutdown() {
            log.log(Level.FINE, "asa.slo.shutdownManager", getName());
            sloManager.shutdown();

            Future future = null;
            sloLock.lock();
            try {
                future = sloFuture;
                sloFuture = null;
            } finally {
                sloLock.unlock();
            }
            if (future != null) {
                future.cancel(true);
                if (!future.isDone()) {
                    int count = 0;
                    try {
                        while (!future.isDone()) {
                            Level level;
                            if (count > 12) {
                                level = Level.SEVERE;
                            } else if (count > 6) {
                                level = Level.WARNING;
                            } else {
                                level = Level.FINE;
                            }
                            if (log.isLoggable(level)) {
                                log.log(level, "asa.slo.await", getName());
                            }
                            try {
                                future.get(10, TimeUnit.SECONDS);
                            } catch (TimeoutException ex) {
                                continue;
                            }
                        }
                    } catch (ExecutionException ex) {
                        log.log(Level.WARNING, "Service {0}: Error in SLO manager: {1}", new Object[]{getName(), ex.getLocalizedMessage()});
                    } catch (InterruptedException ex) {
                        log.log(Level.WARNING, "asa.slo.shutdown.int", getName());
                    }
                }
            }
        }
    }

    private class SLOListener implements SLOManagerEventListener {

        private AtomicBoolean ignoreEvents = new AtomicBoolean();

        public void setIngoreEvents(boolean ignoreEvents) {
            this.ignoreEvents.set(ignoreEvents);
        }

        /**
         *  Called from DefaultSLOManager if the update is finished
         *
         *  @param state the new SLOState
         */
        public void sloUpdated(SLOUpdatedEvent event) {
            if (ignoreEvents.get()) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "asa.slo.ignore",
                            new Object[]{getName(), event});
                }
            } else {
                lock.lock();
                try {
                    boolean send = false;
                    Collection<Need> currentNeeds = resourceRequestMap.get(event.getSLOName());
                    if (currentNeeds == null) {
                        send = true;
                    } else if (currentNeeds.size() != event.getNeeds().size()) {
                        send = true;
                    } else {
                        for (Need need : currentNeeds) {
                            if (!event.getNeeds().contains(need)) {
                                send = true;
                                break;
                            }
                        }
                    }
                    // The send = true turns off the optimization that service
                    // sends only ne resource request if something has changed
                    // RP works much better if this optimization is turned off
                    send = true;
                    if (send) {
                        getServiceEventSupport().fireResourceRequest(event.getSLOName(), event.getNeeds());
                        resourceRequestMap.put(event.getSLOName(), event.getNeeds());
                    } else {
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "asa.slo.noRR", new Object[]{getName(), event.getSLOName()});
                        }
                    }
                } finally {
                    lock.unlock();
                }
            }
        }

        public void usageMapUpdated(UsageMapUpdatedEvent event) {
            if (log.isLoggable(Level.FINER)) {
                log.entering(AbstractServiceAdapter.class.getName(), "usageMapUpdated", event);
            }

            if (ignoreEvents.get()) {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "asa.slo.ignore", new Object[]{getName(), event});
                    log.exiting(AbstractServiceAdapter.class.getName(), "usageMapUpdated");
                }
                return;
            }
            try {
                Collection<RA> ras = getResourceStore().getResourceAdapters();

                Map<ResourceId, Usage> usage = new HashMap<ResourceId, Usage>(ras.size());
                for (RA ra : ras) {
                    usage.put(ra.getId(), Usage.MIN_VALUE);
                }
                for (Map.Entry<Resource, Usage> entry : event.getUsageMap().entrySet()) {
                    usage.put(entry.getKey().getId(), entry.getValue());
                }
                lock.lock();
                try {
                    if (ignoreEvents.get()) {
                        log.log(Level.FINER, "asa.slo.ignore", new Object[]{getName(), event});
                    } else {
                        for (Map.Entry<ResourceId, Usage> entry : usage.entrySet()) {
                            RA ra;
                            try {
                                ra = getResourceStore().get(entry.getKey());
                                UpdateResourceUsageOperation op = new UpdateResourceUsageOperation(entry.getValue());
                                ResourceChanged change = ra.modifyResource(op);
                                if (change.hasChangedResource()) {
                                    getResourceStore().store(ra);
                                    getServiceEventSupport().fireResourceChanged(ra.getResource(), Collections.singleton(change), I18NManager.formatMessage("asa.slo.usageUpdated", BUNDLE));
                                }
                            } catch (InvalidResourcePropertiesException ex) {
                                log.log(Level.SEVERE,
                                        I18NManager.formatMessage("asa.slo.usageUpdate.error", BUNDLE, getName(), entry.getKey(), ex.getLocalizedMessage()),
                                        ex);
                            } catch (UnknownResourceException ex) {
                                log.log(Level.FINE, "asa.slo.usageUpdate.removed",
                                        new Object[]{getName(), entry.getKey()});
                            }
                        }
                    }
                } finally {
                    lock.unlock();
                }
            } catch (ServiceNotActiveException ex) {
                log.log(Level.WARNING, "asa.slo.updateUsage.notActive", getName());
            } catch (ResourceStoreException ex) {
                log.log(Level.SEVERE,
                        I18NManager.formatMessage("asa.slo.usageUpdate.error.rs", BUNDLE, getName(), ex.getLocalizedMessage()),
                        ex);
            }

            if (log.isLoggable(Level.FINER)) {
                log.exiting(AbstractServiceAdapter.class.getName(), "usageMapUpdated");
            }
        }

        /**
         *  Called from DefaultSLOManager if update failed
         *
         *  @param ex  the error
         */
        public void sloUpdateError(SLOErrorEvent event) {
            LogRecord lr = new LogRecord(Level.WARNING, event.getError().getLocalizedMessage());
            lr.setThrown(event.getError());
            log.log(lr);
        }
    }

    /**
     * Placeholder class for all elements which are created during the service startup
     */
    private class ServiceElements {

        private final ResourceAdapterStore<K, RA> resourceStore;
        private final ResourceActionExecutor<K> resourceActionExecutor;
        private final RunnableSLOManagerSetup sloSetup;
        private final ResourceFactory<RA> resourceFactory;

        public ServiceElements(ResourceAdapterStore<K, RA> resourceStore,
                ResourceActionExecutor<K> resourceActionExecutor, RunnableSLOManagerSetup sloSetup, ResourceFactory<RA> resourceFactory) {
            this.resourceStore = resourceStore;
            this.resourceActionExecutor = resourceActionExecutor;
            this.sloSetup = sloSetup;
            this.resourceFactory = resourceFactory;
        }

        public ResourceAdapterStore<K, RA> getResourceStore() {
            return resourceStore;
        }

        public ResourceActionExecutor<K> getResourceActionExecutor() {
            return resourceActionExecutor;
        }

        public RunnableSLOManagerSetup getSLOManagerSetup() {
            return sloSetup;
        }

        public ResourceFactory<RA> getResourceFactory() {
            return resourceFactory;
        }
    }
}
