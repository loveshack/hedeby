/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.GrmComponentBase;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.*;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.service.slo.SLOManager;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.service.slo.impl.DefaultSLOState;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.SLOContextFactory;
import com.sun.grid.grm.service.slo.impl.DefaultSLOContext;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.InvalidStateTransistionException;
import com.sun.grid.grm.util.StateMachine;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract base class for service
 * @param <C> the type of the component configuration
 * @deprecated Please use {@link AbstractServiceContainer} and {@link AbstractServiceAdapter}
*/
public abstract class AbstractService<C extends ComponentConfig> extends GrmComponentBase<C> implements Service, SLOContextFactory {
    
    private final static String BUNDLE = "com.sun.grid.grm.service.impl.service";
    
    private final static Logger log = Logger.getLogger(AbstractService.class.getName(), BUNDLE);
    
    private final static StateMachine<ServiceState> STATE_MACHINE = new StateMachine<ServiceState>();
    
    private static Logger log() {
        return Logger.getLogger(AbstractService.class.getName(), BUNDLE);
    }
    static {
        // First parameter is the state, the following parameters defines the
        // allowed state transitions
        STATE_MACHINE.addState(ServiceState.UNKNOWN, ServiceState.STARTING, ServiceState.ERROR, ServiceState.SHUTDOWN);
        STATE_MACHINE.addState(ServiceState.STARTING, ServiceState.RUNNING, ServiceState.ERROR, ServiceState.UNKNOWN);
        STATE_MACHINE.addState(ServiceState.RUNNING, ServiceState.SHUTDOWN, ServiceState.ERROR, ServiceState.UNKNOWN);
        STATE_MACHINE.addState(ServiceState.STOPPED, ServiceState.STARTING, ServiceState.ERROR, ServiceState.UNKNOWN);
        STATE_MACHINE.addState(ServiceState.SHUTDOWN, ServiceState.STOPPED, ServiceState.ERROR, ServiceState.UNKNOWN);
        STATE_MACHINE.addState(ServiceState.ERROR, ServiceState.UNKNOWN, ServiceState.STARTING);
    }
    
    private final StateMachine<ServiceState>.Instance state = STATE_MACHINE.newInstance(ServiceState.UNKNOWN);
    
    protected final ServiceEventSupport evtSupport;
    
    /**
     * Creates a new instance of AbstractService.
     *
     * @param env    the execution environment of the service
     * @param name   the component name of the service
     */
    protected AbstractService(ExecutionEnv env, String name) {
        super(env, name);
        evtSupport = ServiceEventSupport.newInstance(name, Hostname.getLocalHost());
    }
    
    /**
     * Get the state of the service
     * @return the state of the service
     */
    public ServiceState getServiceState() {
        return state.getState();
    }        
    
    /**
     * Set the state of the service
     * @param newState the new state of the service
     * @throws com.sun.grid.grm.util.InvalidStateTransistionException if the state transition
     *         from the current state to <code>newState</code> is not valid
     */
    protected void setServiceState(ServiceState newState) throws InvalidStateTransistionException {
        ServiceState oldState = state.setState(newState);
        if(!oldState.equals(newState)) {
            if(log().isLoggable(Level.FINE)) {
                log().log(Level.FINE, "AbstractService.stateChanged", new Object [] { getName(), oldState, newState });
            }
            switch(newState) {
            case STARTING:
                evtSupport.fireServiceStarting();
                break;
            case RUNNING:
                evtSupport.fireServiceRunning();
                break;
            case SHUTDOWN:
                evtSupport.fireServiceShutdown();
                break;
            case STOPPED:
                evtSupport.fireServiceStopped();
                break;
            case ERROR:
                evtSupport.fireServiceError();
                break;
            case UNKNOWN:
                evtSupport.fireServiceUnknown();
                break;
            default:
                throw new IllegalStateException("Unknown service state " + newState);
            }
        }
    }

    /**
     * Register a <code>ServiceEventListener</code>.
     *
     * @param eventListener the service listener
     */
    public void addServiceEventListener(ServiceEventListener eventListener) {
        evtSupport.addServiceEventListener(eventListener);
    }

    /**
     * Deregister a <code>ServiceEventListener</code>.
     *
     * @param eventListener the service listener
     */
    public void removeServiceEventListener(ServiceEventListener eventListener) {
        evtSupport.removeServiceEventListener(eventListener);
    }
    
    /**
     * Get the SLO manager for this service
     * @return the SLO manager;
     */
    protected abstract SLOManager getSLOManager();
    
    /**
     * Get the states of the SLOs of this service.
     * 
     * @return list of slo states
     */
    public List<SLOState> getSLOStates() throws GrmRemoteException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractService.class.getName(), "getSLOStates");
        }
        List<SLOState> ret = getSLOStates(ConstantFilter.<Resource>alwaysMatching());
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractService.class.getName(), "getSLOStates", ret);
        }
        return ret;
    }
    
    /**
     * Get the SLOStates of this service. Include only the resource information
     * of those resources matching a <code>resourceFitler</code>
     * @param resourceFilter  the resource filter
     * @return the SLOState
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractService.class.getName(), "getSLOStates");
        }
        List<SLOState> ret = null;
        try {

            SLOManager sloManager = getSLOManager();

            if (sloManager == null) {
                return Collections.<SLOState>emptyList();
            }

            // It is possible that the usage map of the slo manager contains
            // usage for resources which has been removed
            // => Remove these resources from the usage map
            List<Resource> resources = getResources();
            ret = new ArrayList<SLOState>();

            Map<Resource, Usage> newUsageMap = new HashMap<Resource, Usage>();
            ResourceVariableResolver resolver = new ResourceVariableResolver();
            
            for (SLOState aState : sloManager.getSLOStates()) {
                newUsageMap.putAll(aState.getUsageMap());
                for (Map.Entry<Resource, Usage> entry : aState.getUsageMap().entrySet()) {
                    if (!resources.contains(entry.getKey())) {
                        newUsageMap.remove(entry.getKey());
                        continue;
                    }
                    resolver.setResource(entry.getKey()); 
                    if(!resourceFilter.matches(resolver)) {
                        newUsageMap.remove(entry.getKey());
                        continue;
                    }
                }
                ret.add(new DefaultSLOState(aState.getSloName(), newUsageMap, aState.getNeeds()));
                newUsageMap.clear();
            }
        } catch (ServiceNotActiveException ex) {
            ret = Collections.<SLOState>emptyList();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractService.class.getName(), "getSLOStates", ret);
        }
        return ret;
    }
    
    /**
     * Get all resources which matches a filter.
     *
     * It is not thread safe.
     *
     * @param  filter the filter
     * @return list of resources
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws com.sun.grid.grm.GrmRemoteException
     */
    public List<Resource> getResources(Filter<Resource> filter) throws ServiceNotActiveException, GrmRemoteException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(AbstractService.class.getName(), "getResources", filter);
        }
        List<Resource> matched = new LinkedList<Resource>();
        ResourceVariableResolver resolver = new ResourceVariableResolver();
        
        List<Resource> resources = getResources();
        for (Resource r : resources) {
            resolver.setResource(r);
            if (filter.matches(resolver)) {
                matched.add(r);
            }
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(AbstractService.class.getName(), "getResources", matched);
        }
        return matched;
    }

    /**
     * Create a default SLOContext
     * @return return theSLOContext 
     * @throws com.sun.grid.grm.GrmException if the resource can not be evaluated
     */
    public SLOContext createSLOContext() throws GrmException {
        return new DefaultSLOContext(getResources());
    }
}
