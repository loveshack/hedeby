/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.service.event.AbstractServiceChangedResourceEvent;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import com.sun.grid.grm.service.event.ServiceEventListener;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The waitForAllEvents method of this class blocks the service sends out one of the following
 * events to for all registered resources:
 *
 * <p>ResourcError, ResourceRejected, ResourceRemoved, ResourceReset, AddResource
 * or ResourceAdded.</p>
 * 
 * <p>The instance of this class must be registered as ServiceEventListener.</p>
 *
 */
class ResourcesFreedBarrier extends ServiceEventAdapter {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.service";
    private final static Logger log = Logger.getLogger(ResourcesFreedBarrier.class.getName(), BUNDLE);
    
    private final Lock lock = new ReentrantLock();
    private final Condition cond = lock.newCondition();
    private final Set<ResourceId> resources = new HashSet<ResourceId>();
    private final String serviceName;

    /**
     * Create a new ResourcesFreedBarrier.
     * @param serviceName the name of the service (only used for logging)
     */
    public ResourcesFreedBarrier(String serviceName) {
        super();
        this.serviceName = serviceName;
    }

    /**
     * Add a resource which should be freed
     * @param id the id of the resource
     */
    public void addResource(ResourceId id) {
        lock.lock();
        try {
            resources.add(id);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Remove a resource which should be freed
     * @param id the id of the resource
     */
    public void removeResource(ResourceId id) {
        lock.lock();
        try {
            resources.remove(id);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Until the events for all resources has been received.
     * @param timeout  max wait time
     * @param unit     the time unit for the max wait time
     * @return true if all events have been received
     * @throws java.lang.InterruptedException if waiting for the events has been interrupted
     */
    public boolean waitForAllEvents(long timeout, TimeUnit unit) throws InterruptedException {

        int remaining = 0;
        lock.lock();
        try {
            if (!resources.isEmpty()) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "rfb.wait", new Object[]{serviceName, resources.size()});
                }
                cond.await(timeout, unit);
                remaining = resources.size();
            }
        } finally {
            lock.unlock();
        }
        if (remaining == 0) {
            log.log(Level.FINE, "rfb.all", serviceName);
            return true;
        } else {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "rfb.remaining", new Object[]{serviceName, remaining});
            }
            return false;
        }
    }

    private void processEvent(AbstractServiceChangedResourceEvent evt) {
        ResourceId resId = evt.getResource().getId();
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "rfb.got", new Object[]{serviceName, evt});
        }
        lock.lock();
        try {
            resources.remove(resId);
            if (resources.isEmpty()) {
                cond.signalAll();
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * EventListener method
     * @param event
     * @see ServiceEventListener#addResource(com.sun.grid.grm.service.event.AddResourceEvent) 
     */
    @Override
    public void addResource(AddResourceEvent event) {
        processEvent(event);
    }

    /**
     * EventListener method
     * @param event
     * @see ServiceEventListener#resourceAdded(com.sun.grid.grm.service.event.ResourceAddedEvent) 
     */
    @Override
    public void resourceAdded(ResourceAddedEvent event) {
        processEvent(event);
    }

    /**
     * EventListener method
     * @param event
     * @see ServiceEventListener#resourceError(com.sun.grid.grm.service.event.ResourceErrorEvent) 
     */
    @Override
    public void resourceError(ResourceErrorEvent event) {
        processEvent(event);
    }

    /**
     * EventListener method
     * @param event
     * @see ServiceEventListener#resourceRejected(com.sun.grid.grm.service.event.ResourceRejectedEvent) 
     */
    @Override
    public void resourceRejected(ResourceRejectedEvent event) {
        processEvent(event);
    }

    /**
     * EventListener method
     * @param event
     * @see ServiceEventListener#resourceRemoved(com.sun.grid.grm.service.event.ResourceRemovedEvent) 
     */
    @Override
    public void resourceRemoved(ResourceRemovedEvent event) {
        processEvent(event);
    }

    /**
     * EventListener method
     * @param event
     * @see ServiceEventListener#resourceReset(com.sun.grid.grm.service.event.ResourceResetEvent) 
     */
    @Override
    public void resourceReset(ResourceResetEvent event) {
        processEvent(event);
    }
}
