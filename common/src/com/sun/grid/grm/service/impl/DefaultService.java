/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.adapter.impl.DefaultResourceAdapter;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * The DefaultService is the service that is used by ResourceProvider to
 * hold UNASSIGNED resources.
 */
public class DefaultService implements Service {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.service";
    private final static ResourceFactory<DefaultResourceAdapter> RESOURCE_FACTORY = new DefaultResourceFactory<DefaultResourceAdapter>();
    private final ExecutionEnv env;
    private final String name;
    private final ServiceEventSupport evtSupport;
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final ResourceStore resourceStore;
    
    private ServiceState state = ServiceState.UNKNOWN;

    /**
     * Create a new instance of DefaultService
     * @param env   the execution env
     * @param name  the name of the service
     * @param resourceStore the resource store
     */
    public DefaultService(ExecutionEnv env, String name, ResourceStore resourceStore) {
        this.env = env;
        this.name = name;
        this.resourceStore = resourceStore;
        this.evtSupport = ServiceEventSupport.newInstance(name, Hostname.getLocalHost());
    }

    /**
     * Start the service
     */
    public void startService() {
        lock.writeLock().lock();
        try {
            state = ServiceState.STARTING;
            evtSupport.fireServiceStarting();
            state = ServiceState.RUNNING;
            evtSupport.fireServiceRunning();
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Stop the service
     * @param isFreeResources not used
     */
    public void stopService(boolean isFreeResources) {
        lock.writeLock().lock();
        try {
            state = ServiceState.SHUTDOWN;
            evtSupport.fireServiceShutdown();
            state = ServiceState.STOPPED;
            evtSupport.fireServiceStopped();
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Add a service event listener
     * @param eventListener the service event listener
     */
    public void addServiceEventListener(ServiceEventListener eventListener) {
        evtSupport.addServiceEventListener(eventListener);
    }

    /**
     * remove a service event listener
     * @param eventListener the service event listener
     */
    public void removeServiceEventListener(ServiceEventListener eventListener) {
        evtSupport.removeServiceEventListener(eventListener);
    }

    /**
     *  The call of this method has no effect. The Default service does not
     *  support the component specific methods
     */
    public void start() {
        // Empty implementation
    }

    /**
     *  The call of this method has no effect. The Default service does not
     *  support the component specific methods
     */
    public void stop(boolean isForced) throws GrmException {
        // Empty implementation
    }

    /**
     *  The call of this method has no effect. The Default service does not
     *  support the component specific methods
     */
    public void reload(boolean isForced) {
        // Empty implementation
    }

    /**
     *  The call of this method has no effect. The Default service does not
     *  support the component specific methods
     * @return always STARTED
     */
    public ComponentState getState() {
        return ComponentState.STARTED;
    }

    /**
     *  The call of this method has no effect. The Default service does not
     *  support the component specific methods
     */
    public void addComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
    }

    /**
     *  The call of this method has no effect. The Default service does not
     *  support the component specific methods
     */
    public void removeComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
    }

    /**
     * Get the name of the default service
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * get the hostname where the default service lives
     * @return the hostname
     */
    public Hostname getHostname() {
        return Hostname.getLocalHost();
    }

    /**
     * get the service state of the defautl service
     * @return the service state
     */
    public ServiceState getServiceState() {
        lock.readLock().lock();
        try {
            return state;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * the default service has not SLOs. This method returns always an empty list
     * @return an empty list
     */
    public List<SLOState> getSLOStates() {
        return Collections.<SLOState>emptyList();
    }

    /**
     * the default service has not SLOs. This method returns always an empty list
     * @return an empty list
     */
    public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) {
        return Collections.<SLOState>emptyList();
    }

    /**
     * get the resource that matches a filter
     * @param filter  the filter
     * @return the list of resources
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public List<Resource> getResources(Filter<Resource> filter) throws ServiceNotActiveException {
        lock.readLock().lock();
        try {
            return resourceStore.getResources(filter);
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     *   get all resources of the default service
     *   @return list with all resources
     */
    public List<Resource> getResources() throws ServiceNotActiveException {
        lock.readLock().lock();
        try {
            return resourceStore.getResources();
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * get a resoruce by its id.
     * @param resourceId the resource id
     * @return the resource
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource has not been found
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public Resource getResource(ResourceId resourceId) throws UnknownResourceException, ServiceNotActiveException {
        lock.readLock().lock();
        try {
            return resourceStore.getResource(resourceId);
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * remove a resource from the service
     * @param resourceId the resource id
     * @param descr the reason 
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource has not been found
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, ServiceNotActiveException {
        lock.readLock().lock();
        try {
            Resource res = resourceStore.remove(resourceId);
            evtSupport.fireRemoveResource(res, "");
            evtSupport.fireResourceRemoved(res, "");
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * The call of this method has not effect. The default service has not mean
     * for reseting a resource
     * @param resource the id of the resource
     */
    public void resetResource(ResourceId resource) {
        // Empty implementation
    }

    /**
     * add a resource to the default service
     * @param resource the resource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws InvalidResourceException if a resource with the same id is owned by the service
     */
    public void addResource(Resource resource) throws ServiceNotActiveException, InvalidResourceException {
        lock.writeLock().lock();
        try {
            resource.setState(Resource.State.UNASSIGNED);
            evtSupport.fireAddResource(resource, "");
            resource.setUsage(Usage.MIN_VALUE);
            Resource orgRes = resourceStore.add(resource);
            if(orgRes != null) {
                resourceStore.add(orgRes);
                throw new InvalidResourceException("ds.resourcealreadyexists.error", BUNDLE, orgRes.getId().getId(), getName());
            }
            evtSupport.fireResourceAdded(resource, "");
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * add a resource to the default service
     *
     * @param resource  the resource
     * @param sloName   the name of the SLO that requested the resource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws InvalidResourceException if a resource with the same id is owned by the service
     */
    public void addResource(Resource resource, String sloName) throws ServiceNotActiveException, InvalidResourceException {
        addResource(resource);
    }

    /**
     * add a new resource
     * @param type   the type of the resource
     * @param properties the resource properties
     * @return clone of the new resource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException  if the service is not active
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the resource properties are not valid
     * @throws com.sun.grid.grm.GrmException if the resource could not be created
     */
    public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourcePropertiesException, GrmException {
        Resource res = RESOURCE_FACTORY.createResource(env, type, properties);
        addResource(res);
        return res.clone();
    }

    /**
     * Modify a resource
     * @param resource    the resource
     * @param operations  the resource operation
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation leads to an invalid resource
     */
    public void modifyResource(ResourceId resource, ResourceChangeOperation operations) throws UnknownResourceException, ServiceNotActiveException, InvalidResourcePropertiesException {
        lock.readLock().lock();
        try {
            Resource res = resourceStore.getResource(resource);
            ResourceChanged changes = res.modify(operations);
            if (changes.hasChangedResource()) {
                evtSupport.fireResourceChanged(res, Collections.singleton(changes), "");
            }
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Modify a resource
     * @param resource    the resource
     * @param operations  the resource operation
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is unknown
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation leads to an invalid resource
     */
    public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, InvalidResourcePropertiesException {
        lock.readLock().lock();
        try {
            Resource res = resourceStore.getResource(resource);
            Collection<ResourceChanged> changes = res.modify(operations);

            for(ResourceChanged change: changes) {
                if (change.hasChangedResource()) {
                    evtSupport.fireResourceChanged(res, changes, "");
                    break;
                }
            }
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * get a snapshot of the service
     * @return the snapshot of the service
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    public ServiceSnapshot getSnapshot() throws ServiceNotActiveException {
        lock.writeLock().lock();
        try {
            return new DefaultServiceSnapshot(evtSupport.getSequenceNumberOfNextEvent(), getState(), getServiceState(),
                    new HashSet<Resource>(resourceStore.getResources()), Collections.<String, Collection<Need>>emptyMap());
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        } finally {
            lock.writeLock().unlock();
        }
    }
}
