/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/**
 * Interface for a action which can be executed with the ResourceActionExecutor
 * @param <R>  The result of the action
 * @param <K>  The type of key of the resource
 */
interface ResourceAction<R,K> {

    public enum State {
        /* Action has not been started */
        PENDING,
        /* Action currently executed by the ResourceActionExecutor */
        RUNNING,
        /* Action has been canceled */
        CANCELED,
        /* The ResourceActionExecutor has finished this action */
        FINISHED
    }
    
    /**
     * Get the state of the action
     * @return the state of the action
     */
    public State getState();
    
    /**
     * Get the key of the resource which will be modified by this action
     * @return the id of the resource
     */
    public K getResourceKey();
    
    /**
     * Execute the action
     */
    public void execute();
 
    /**
     * Cancel the action
     */
    public void cancel();
    
    /**
     * Get the name of the action.
     * @return the name of the action
     */
    public String getActionName();
    
    /**
     * Get the result of the action. This method should block
     * until the action is finished or it has been canceled
     * @return the result of the action
     * @throws java.lang.InterruptedException if the calling thread has been interrupted while waiting for the result
     * @throws java.util.concurrent.ExecutionException if the action produced an error
     * @throws java.util.concurrent.CancellationException if the action has been canceled
     */
    public R getResult() throws InterruptedException, ExecutionException, CancellationException;
}
