/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.util.I18NManager;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class provides the implementation of the life cycle of a ResourceAction,
 * the concrete action must be implemented in the abstract method <code>doExecute</code>.
 * 
 * @param <R> The type of the result of the action
 */
abstract class AbstractResourceAction<R,K> implements ResourceAction<R,K> {
    private final static String BUNDLE = "com.sun.grid.grm.service.impl.service";
    private final Logger log = Logger.getLogger(AbstractResourceAction.class.getName(), BUNDLE);
    private final K resourceKey;
    private final Lock lock = new ReentrantLock();
    private final Condition stateChangedCondition = lock.newCondition();
    private Throwable error;
    private R result;
    private final String actionName;
    private final AtomicReference<State> state = new AtomicReference<State>();

    /**
     * Create a new instance of AbstractResourceAction
     * @param resId  the resource id
     * @param actionName the name of the action
     */
    protected AbstractResourceAction(K resourceKey, String actionName) {
        this.resourceKey = resourceKey;
        state.set(State.PENDING);
        this.actionName = actionName;
    }

    /**
     * Get the current state of the action.
     * @return the current state of the action
     */
    public final State getState() {
        return state.get();
    }

    /**
     * Get the name of the action
     * @return the name of the action
     */
    public final String getActionName() {
        return actionName;
    }

    /**
     * Get id of the resource which is processed by this action
     * @return the id of the resource
     */
    public final K getResourceKey() {
        return resourceKey;
    }

    /**
     * Execute the ResourceAction
     */
    public final void execute() {
        log.entering(AbstractResourceAction.class.getName(), "execute");
        lock.lock();
        try {
            state.set(State.RUNNING);
            stateChangedCondition.signalAll();
        } finally {
            lock.unlock();
        }
        log.log(Level.FINE, "ra.started", toString());
        
        Throwable myError = null;
        R myResult = null;
        try {
            myResult = doExecute();
        } catch (Throwable ex) {
            myError = ex;
        } finally {
            lock.lock();
            try {
                error = myError;
                result = myResult;
                state.set(State.FINISHED);
                stateChangedCondition.signalAll();
            } finally {
                lock.unlock();
            }
            log.log(Level.FINE, "ra.finished", toString());
        }
        log.exiting(AbstractResourceAction.class.getName(), "execute");
    }

    /**
     * Cancel the resource action.
     * 
     * Thread which are blocked in the getResult method will be waked up.
     */
    public void cancel() {
        log.entering(AbstractResourceAction.class.getName(), "cancel");
        lock.lock();
        try {
            state.set(State.CANCELED);
            stateChangedCondition.signalAll();
        } finally {
            lock.unlock();
        }
        log.log(Level.FINE, "ResourceAction {0} canceled", toString());
        log.exiting(AbstractResourceAction.class.getName(), "cancel");
    }

    /**
     * Get the result of this action
     * @return the result
     * @throws java.lang.InterruptedException  if the current thread has been interrupted while waiting for the result
     * @throws java.util.concurrent.ExecutionException if the execution of the action thrown an exception (it's the cause
     *                   of the ExecutionException)
     * @throws java.util.concurrent.CancellationException if the cancel method of the action has called
     */
    public R getResult() throws InterruptedException, ExecutionException, CancellationException {
        log.entering(AbstractResourceAction.class.getName(), "getResult");
        R ret = null;
        log.log(Level.FINE, "ra.wait", toString());
        try {
            lock.lock();
            try {
                outer:
                while (true) {
                    if (Thread.currentThread().isInterrupted()) {
                        throw new InterruptedException();
                    }
                    switch (state.get()) {
                        case CANCELED:
                            throw new CancellationException();
                        case FINISHED:
                            if (error != null) {
                                throw new ExecutionException(error);
                            }
                            ret = result;
                            break outer;
                        case RUNNING:
                        case PENDING:
                            stateChangedCondition.await();
                    }
                }
            } finally {
                lock.unlock();
            }
        } catch(CancellationException ex) {
            log.log(Level.FINE,"ra.canceled", toString());
            throw ex;
        } catch(ExecutionException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE,I18NManager.formatMessage("ra.error", BUNDLE, toString(), ex.getLocalizedMessage()), ex);
            }
            throw ex;
        }
        
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE,"ra.haveResult", new Object [] { toString(), ret });
        }
        
        log.exiting(AbstractResourceAction.class.getName(), "getResult");
        return ret;
    }

    /**
     * Sub class must implement the action in this method
     * @return the result of the action
     * @throws java.lang.Exception if the action produced an error
     */
    protected abstract R doExecute() throws Exception;

    @Override
    public String toString() {
        return String.format("[name=%s,res=%s]", getActionName(), getResourceKey());
    }
}
