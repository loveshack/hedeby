/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.config.naming.ConfigurationObjectNotFoundException;
import com.sun.grid.grm.event.AbstractComponentEvent;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.AbstractServiceEvent;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.service.event.ServiceStateChangedEvent;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.impl.AbstractResourceChanged;
import com.sun.grid.grm.resource.impl.FileResourceStore;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.event.AbstractServiceChangedResourceEvent;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.util.filter.Filter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Caching proxy of service class for use with Resource provider. It listens for
 * every kind of service events and maintains the up-to-date local copy of
 * service's resources.
 *
 * The persistence mechanism of the resources is not implemented (should be done
 * on the service side if service needs it).
 */
public class ServiceCachingProxy implements Service {

    private static final String BUNDLE = "com.sun.grid.grm.service.impl.service";
    private static final Logger log = Logger.getLogger(ServiceCachingProxy.class.getName(), BUNDLE);
    private final ResourceStore visible_resources;
    private final Service service;
    private final String serviceName;
    private final Hostname hostname;
    private final ServiceEventListener serviceEventListener = new SCPServiceEventListener();
    private final ComponentEventListener componentEventListener = new SCPComponentEventListener();
    protected final ServiceEventSupport svcEventForwarder;
    private final Lock actionLock = new ReentrantLock();
    private final AtomicLong nextExpectedEventSequenceNumber = new AtomicLong(-1);
    private static final ThreadGroup scpThreadGroup = new ThreadGroup("service caching proxy");
    private static String actionExecutorName = "ACTION_EXECUTOR";
    ScheduledExecutorService actionExecutor;
    private boolean isDestroy = false;
    private Future<Void> scheduledRefresh;
    final Map<Action, Future<Void>> scheduledActions = new HashMap<Action, Future<Void>>();
    private final AtomicReference<ServiceState> cachedServiceState = new AtomicReference<ServiceState>(ServiceState.UNKNOWN);
    private final AtomicReference<ComponentState> cachedComponentState = new AtomicReference<ComponentState>(ComponentState.UNKNOWN);
    private final ExecutionEnv enviro;
    /**
     * Contains all resource request which has been forwarded. The FullRefresh must
     * update this map. For obsolete SLO cancel request must be forwarded.
     */
    private final Map<String, Collection<Need>> cachedResourceRequestMap = new HashMap<String, Collection<Need>>();

    /**
     * Package private constructor for testing purposes. It does not trigger
     * immediate refresh.
     *
     * @param env execution environment
     * @param s the target service
     * @param rm resource manager instance for tracking the uniqueness of managed resources
     * @throws com.sun.grid.grm.service.InvalidServiceException if the service name is not correct
     * or if the service in not active
     */
    ServiceCachingProxy(ExecutionEnv env, Service s, ResourceManager rm, boolean performFullRefreshImmediately) throws InvalidServiceException {
        log.entering(ServiceCachingProxy.class.getName(), "<init>");

        isDestroy = false;

        if (s == null) {
            InvalidServiceException ise = new InvalidServiceException("scp.error.null_svc", BUNDLE);
            log.throwing(ServiceCachingProxy.class.getName(), "<init>", ise);
            throw ise;
        }

        try {
            serviceName = s.getName();
        } catch (GrmRemoteException grexa) {
            InvalidServiceException ex = new InvalidServiceException("scp.error.svc_name_prob", grexa, BUNDLE);
            log.throwing(ServiceCachingProxy.class.getName(), "<init>", ex);
            throw ex;
        }
        if (serviceName == null || serviceName.length() == 0) {
            InvalidServiceException ise = new InvalidServiceException("scp.error.null_svc_name", BUNDLE, serviceName);
            log.throwing(ServiceCachingProxy.class.getName(), "<init>", ise);
            throw ise;
        }

        if (env == null) {
            InvalidServiceException ise = new InvalidServiceException("scp.error.null_env", BUNDLE, serviceName);
            log.throwing(ServiceCachingProxy.class.getName(), "<init>", ise);
            throw ise;
        }

        enviro = env;

        try {
            visible_resources = new FileResourceStore(PathUtil.getRPSpoolDirForComponent(enviro, serviceName));
            visible_resources.loadResources();
        } catch (ResourceStoreException rse) {
            InvalidServiceException ise = new InvalidServiceException("scp.error.cache_spool_err", BUNDLE);
            ise.initCause(rse);
            log.throwing(ServiceCachingProxy.class.getName(), "<init>", ise);
            throw ise;
        }

        service = s;
        try {
            hostname = s.getHostname();
        } catch (GrmRemoteException grexa) {
            InvalidServiceException ex = new InvalidServiceException("scp.error.svc_host_prob", grexa, BUNDLE);
            log.throwing(ServiceCachingProxy.class.getName(), "<init>", ex);
            throw ex;
        }
        if (hostname == null) {
            InvalidServiceException ise = new InvalidServiceException("scp.error.null_host", BUNDLE);
            log.throwing(ServiceCachingProxy.class.getName(), "<init>", ise);
            throw ise;
        }

        try {
            service.addServiceEventListener(serviceEventListener);
        } catch (Exception e) {
            InvalidServiceException ex = new InvalidServiceException("scp.error.not_grm_svc", e, BUNDLE, serviceName);
            log.throwing(ServiceCachingProxy.class.getName(), "<init>", ex);
            throw ex;
        }

        try {
            service.addComponentEventListener(componentEventListener);
        } catch (Exception e) {
            try {
                service.removeServiceEventListener(serviceEventListener);
            } catch (GrmRemoteException grexa) {
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.error.rmsel", new Object[]{serviceName, grexa});
                }
            }
            InvalidServiceException ex = new InvalidServiceException("scp.error.not_grm_svc", e, BUNDLE, serviceName);
            log.throwing(ServiceCachingProxy.class.getName(), "<init>", ex);
            throw ex;
        }

        svcEventForwarder = ServiceEventSupport.newInstance(serviceName, hostname);
        svcEventForwarder.setLogPrefix("SCP");

        /* start the action processing and make a full refresh first */
        if (performFullRefreshImmediately) {
            fullRefresh();
        }

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "scp.info.proxyinitok", serviceName);
        }
        log.exiting(ServiceCachingProxy.class.getName(), "<init>");
    }

    /**
     * Default constructor which constructs a service proxy pointing to
     * the real service (with the given id).
     *
     * The constructor initializes proxy for a service, registers listeners and
     * perform initial synchronization of the resource cache.
     *
     * As this is the caching proxy (not virtual proxy), the service object has
     * to be alive (stored amongst the active components in the CS) for init
     * to be successful.
     *
     * @param env execution environment
     * @param s the target service
     * @param rm resource manager instance for tracking the uniqueness of managed resources
     * @throws com.sun.grid.grm.service.InvalidServiceException if the service name is not correct
     * or if the service in not active
     */
    public ServiceCachingProxy(ExecutionEnv env, Service s, ResourceManager rm) throws InvalidServiceException {
        this(env, s, rm, true);
    }

    /**
     * Releases and cleans the connection stuff to remote service, performs cache
     * cleanup is needed. Should be called when SCP is removed from ServiceManager.
     */
    public void destroy() {
        log.entering(ServiceCachingProxy.class.getName(), "destroy");

        isDestroy = true;
        stopActionProcessing();

        try {
            this.service.removeServiceEventListener(serviceEventListener);
        } catch (GrmRemoteException grexa) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "scp.error.rmsel", new Object[]{serviceName, grexa});
            }
        }

        try {
            this.service.removeComponentEventListener(componentEventListener);
        } catch (GrmRemoteException grexa) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "scp.error.rmcel", new Object[]{serviceName, grexa});
            }
        }

        GetConfigurationCommand<ComponentConfig> gcc = new GetConfigurationCommand<ComponentConfig>();
        /* name of the scp spool dir is the same as service name and thus the same
         * as the service's component's config name */
        gcc.setConfigName(serviceName);
        ComponentConfig cc = null;
        try {
            /* if no component configuration with service's name is found, it means
             * that during SCP lifetime an original service configuration was removed
             * and we need to remove SCP spool dir. ideally, command execution
             * has to throw ConfigurationObjectNotFoundException, but to be sure,
             * check against null value also */
            cc = enviro.getCommandService().execute(gcc).getReturnValue();
            if (cc != null) {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "scp.d.cosi", serviceName);
                }
            } else {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "scp.d.conf", serviceName);
                }
                removeSpoolDir();
            }
        } catch (ConfigurationObjectNotFoundException conf) {
            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "scp.d.conf", serviceName);
            }
            removeSpoolDir();
        } catch (GrmException ex) {
            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "scp.d.cogrexa", new Object[]{serviceName, ex});
            }
        }

        log.exiting(ServiceCachingProxy.class.getName(), "destroy");
    }

    /* helper method for SCP spool dir removal with necessary logging */
    private void removeSpoolDir() {
        log.entering(ServiceCachingProxy.class.getName(), "removeSpoolDir");
        try {
            Platform.getPlatform().removeDir(PathUtil.getRPSpoolDirForComponent(enviro, serviceName), true);
            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "scp.rsd.scp_rem_ok", serviceName);
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, "scp.rsd.scp_rem_fail", new Object[]{serviceName, ex});
        }
        log.exiting(ServiceCachingProxy.class.getName(), "removeSpoolDir");
    }

    /**
     * Gets the cached component state. Cached state information is updated upon
     * a component state event or during the full refresh.
     *
     * @return the state of the target component.
     */
    public ComponentState getState() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ServiceCachingProxy.class.getName(), "getState");
        }
        ComponentState ret = cachedComponentState.get();
        log.exiting(ServiceCachingProxy.class.getName(), "getState", ret);
        return ret;
    }

    /**
     * Get the resource store of the service caching proxy
     *
     * Package private for testing
     *
     * p><b>Warning:</b> This method must only be used for junit testing</p>
     * @return the resource store
     */
    final ResourceStore getResourceStore() {
        return visible_resources;
    }

    /**
     * Helper method for setting the current component state.
     *
     * Only STARTING/STOPPING events are recreated (they signal
     * begin of the related operation).
     *
     * package private for testing
     * @param state current state.
     */
    final void setState(ComponentState newState) {
        ComponentState oldState = cachedComponentState.getAndSet(newState);
        if (!newState.equals(oldState)) {
            switch (newState) {
                case STARTING:
                    svcEventForwarder.fireComponentStarting();
                    break;
                case STARTED:
                    if (!oldState.equals(ComponentState.STARTING) &&
                            !oldState.equals(ComponentState.RELOADING)) {
                        svcEventForwarder.fireComponentStarting();
                    }
                    svcEventForwarder.fireComponentStarted();
                    break;
                case STOPPING:
                    svcEventForwarder.fireComponentStopping();
                    break;
                case STOPPED:
                    if (!oldState.equals(ComponentState.STOPPING)) {
                        svcEventForwarder.fireComponentStopping();
                    }
                    svcEventForwarder.fireComponentStopped();
                    break;
                case RELOADING:
                    svcEventForwarder.fireComponentReloading();
                    break;
                case UNKNOWN:
                    svcEventForwarder.fireComponentUnknown();
                    break;
                default:
                    throw new IllegalStateException("Unknown component state " + newState);
            }
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.scs.cs", new Object[]{serviceName, newState});
            }
        }
    }

    /**
     * Gets the cached service state. Cached state information is updated upon
     * a service state event or during the full refresh.
     *
     * @return the state of the target service.
     */
    public ServiceState getServiceState() {
        log.entering(ServiceCachingProxy.class.getName(), "getServiceState");
        ServiceState ret = cachedServiceState.get();
        log.exiting(ServiceCachingProxy.class.getName(), "getServiceState", ret);
        return ret;
    }

    /**
     * Helper method for setting the current service state.
     *
     * Only STARTING/SHUTDOWN events are recreated (they signal
     * begin of the related operation).
     *
     * Package private for tests
     * @param newState the new state of the service
     */
    void setServiceState(ServiceState newState) {
        ServiceState oldState = cachedServiceState.getAndSet(newState);
        if (!newState.equals(oldState)) {
            switch (newState) {
                case STARTING:
                    svcEventForwarder.fireServiceStarting();
                    break;
                case RUNNING:
                    if (!oldState.equals(ServiceState.STARTING)) {
                        svcEventForwarder.fireServiceStarting();
                    }
                    svcEventForwarder.fireServiceRunning();
                    break;
                case SHUTDOWN:
                    svcEventForwarder.fireServiceShutdown();
                    break;
                case STOPPED:
                    if (!oldState.equals(ServiceState.SHUTDOWN)) {
                        svcEventForwarder.fireServiceShutdown();
                    }
                    svcEventForwarder.fireServiceStopped();
                    break;
                case ERROR:
                    svcEventForwarder.fireServiceError();
                    break;
                case UNKNOWN:
                    svcEventForwarder.fireServiceUnknown();
                    break;
                default:
                    throw new IllegalStateException("Unknown service state " + newState);
            }
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.sss.cs", new Object[]{serviceName, newState});
            }
        }
    }


    /* helper method that checks whether the service is active */
    private void isServiceActive() throws ServiceNotActiveException {
        log.entering(ServiceCachingProxy.class.getName(), "isServiceActive");
        if (!ServiceState.RUNNING.equals(cachedServiceState.get())) {
            ServiceNotActiveException ex = new ServiceNotActiveException();
            log.throwing(ServiceCachingProxy.class.getName(), "isServiceActive", ex);
            throw ex;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "isServiceActive");
    }

    /**
     * Gets the slo state of the service.
     * @return the slo state
     */
    public List<SLOState> getSLOStates() {
        try {
            return service.getSLOStates();
        } catch (GrmRemoteException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            return Collections.<SLOState>emptyList();
        }
    }

    /**
     * Gets the slo state of the service. Include only the usage information
     * if those resoruces matching the resourceFilter
     * @param  resourceFilter the resourceFilter
     * @return the slo states
     */
    public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) {
        try {
            return service.getSLOStates(resourceFilter);
        } catch (GrmRemoteException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            return Collections.<SLOState>emptyList();
        }
    }

    /**
     * Triggers the start service operation on the target service.
     *
     * @throws com.sun.grid.grm.GrmException if the call to the target
     * service fails
     */
    public void startService() throws GrmException {
        log.entering(ServiceCachingProxy.class.getName(), "startService");
        try {
            service.startService();
        } catch (GrmRemoteException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            GrmException grexa = new GrmException(ex.getLocalizedMessage());
            log.throwing(ServiceCachingProxy.class.getName(), "startService", grexa);
            throw grexa;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "startService");
    }

    /**
     * Triggers the stop service operation on the target service.
     *
     * @throws com.sun.grid.grm.GrmException if the call to the target
     * service fails
     */
    public void stopService(boolean isFreeResources) throws GrmException {
        log.entering(ServiceCachingProxy.class.getName(), "stopService", isFreeResources);
        try {
            service.stopService(isFreeResources);
        } catch (GrmRemoteException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            GrmException grexa = new GrmException(ex.getLocalizedMessage());
            log.throwing(ServiceCachingProxy.class.getName(), "stopService", grexa);
            throw grexa;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "stopService");
    }

    /**
     * Registers the service event listener. Events coming from the
     * target service are relayed to the listener if the SCP determines that
     * event is not doubled or obsolete. SCP will send additional events, if
     * it detects that cached information is out of sync.
     *
     * @param eventListener the listener to add
     */
    public void addServiceEventListener(ServiceEventListener eventListener) {
        log.entering(ServiceCachingProxy.class.getName(), "addServiceEventListener", eventListener);
        svcEventForwarder.addServiceEventListener(eventListener);
        log.exiting(ServiceCachingProxy.class.getName(), "addServiceEventListener");
    }

    /**
     * Unregisters the service event listener.
     *
     * @param eventListener the listener to remove
     */
    public void removeServiceEventListener(ServiceEventListener eventListener) {
        log.entering(ServiceCachingProxy.class.getName(), "removeServiceEventListener", eventListener);
        svcEventForwarder.removeServiceEventListener(eventListener);
        log.exiting(ServiceCachingProxy.class.getName(), "removeServiceEventListener");
    }

    /**
     * Get all resources which matches a filter
     * @param  filter the filter
     * @return list of resources
     */
    public List<Resource> getResources(Filter<Resource> filter) throws ServiceNotActiveException {
        log.entering(ServiceCachingProxy.class.getName(), "getResources", filter);

        List<Resource> matched = Collections.<Resource>emptyList();
        try {
            isServiceActive();
            matched = visible_resources.getResources(filter);
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        }
        log.exiting(ServiceCachingProxy.class.getName(), "getResources", matched);
        return matched;
    }

    /**
     * Returns the cached list of services resources.
     *
     * @return the list of resources
     */
    public List<Resource> getResources() throws ServiceNotActiveException {
        log.entering(ServiceCachingProxy.class.getName(), "getResources");
        List<Resource> ret = Collections.<Resource>emptyList();
        try {
            isServiceActive();
            ret = visible_resources.getResources();
        } catch (ResourceStoreException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.storeexception", new Object[]{serviceName, ex});
            }
        }
        log.exiting(ServiceCachingProxy.class.getName(), "getResources", ret);
        return ret;
    }

    /**
     * Returns the resource with the given Id.
     *
     * @param resourceId id of the resource
     * @return the resource if found or null if the underlying store has a problem
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is not found
     */
    public Resource getResource(ResourceId resourceId) throws UnknownResourceException, ServiceNotActiveException {
        log.entering(ServiceCachingProxy.class.getName(), "getResource", resourceId);
        Resource ret = null;
        try {
            isServiceActive();
            ret = visible_resources.getResource(resourceId);
        } catch (ResourceStoreException ex) {
            throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
        }
        log.exiting(ServiceCachingProxy.class.getName(), "getResource", ret);
        return ret;
    }

    /**
     * Triggers the removeResource operation on the target service.
     *
     * @param resourceId id of the resource which should be removed
     * @param descr the descriptor of the resource remove operation
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the service does not contain the resource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the target service is not active
     */
    public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, InvalidResourceException, ServiceNotActiveException {
        log.entering(ServiceCachingProxy.class.getName(), "removeResource", descr);

        try {
            isServiceActive();
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.removeResource", new Object[]{serviceName, resourceId, descr});
            }
            service.removeResource(resourceId, descr);
        } catch (GrmRemoteException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            // service appears to be active, but remote call failed. do full refresh
            fullRefresh();
            ServiceNotActiveException snae = new ServiceNotActiveException(ex.getLocalizedMessage());
            log.throwing(ServiceCachingProxy.class.getName(), "removeResource", snae);
            throw snae;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "removeResource");
    }

    /**
     * Triggers the resetResource operation on the target service.
     *
     * @param resource the resource to be reset
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the service does not contain the resource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the target service is not active
     */
    public void resetResource(ResourceId resource) throws UnknownResourceException, ServiceNotActiveException {
        log.entering(ServiceCachingProxy.class.getName(), "resetResource", resource);
        try {
            service.resetResource(resource);
        } catch (GrmRemoteException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            // service appears to be active, but remote call failed. do full refresh
            fullRefresh();
            ServiceNotActiveException snae = new ServiceNotActiveException(ex.getLocalizedMessage());
            log.throwing(ServiceCachingProxy.class.getName(), "resetResource", snae);
            throw snae;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "resetResource");
    }

    /**
     * Triggers the modifyResource operation on the target service.
     *
     * @param resource the resource to be modified
     * @param operation to be done on resource properties and attributes
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the service does not contain the resource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the target service is not active
     */
    public void modifyResource(ResourceId resource, ResourceChangeOperation operation) throws UnknownResourceException, ServiceNotActiveException, InvalidResourcePropertiesException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ServiceCachingProxy.class.getName(), "modifyResource", new Object[]{resource, operation});
        }
        try {
            service.modifyResource(resource, operation);
        } catch (GrmRemoteException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            // service appears to be active, but remote call failed. do full refresh
            fullRefresh();
            ServiceNotActiveException snae = new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
            log.throwing(ServiceCachingProxy.class.getName(), "modifyResource", snae);
            throw snae;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "modifyResource");
    }

    /**
     * Triggers the modifyResource operation on the target service.
     *
     * @param resource the resource to be modified
     * @param operations to be done on resource properties and attributes
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the service does not contain the resource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the target service is not active
     */
    public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, InvalidResourcePropertiesException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ServiceCachingProxy.class.getName(), "modifyResource", new Object[]{resource, operations});
        }
        try {
            service.modifyResource(resource, operations);
        } catch (GrmRemoteException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            // service appears to be active, but remote call failed. do full refresh
            fullRefresh();
            ServiceNotActiveException snae = new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
            log.throwing(ServiceCachingProxy.class.getName(), "modifyResource", snae);
            throw snae;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "modifyResource");
    }

    /**
     * Triggers the addResource operation on the target service.
     * @param resource the resource to be added
     * @throws com.sun.grid.grm.resource.InvalidResourceException if the target service considers the resource as invalid
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the target service is not active
     */
    public void addResource(Resource resource) throws InvalidResourceException, ServiceNotActiveException {
        addResource(resource, null);
    }

    /**
     * Create a new resource and assign it
     * @param type       the type of the resource
     * @param properties the properties of the resource
     * @throws ServiceNotActiveException if the service is not active
     * @throws InvalidResourceException If the resource type is not supported
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the resource properties are not valid
     * @throws com.sun.grid.grm.GrmRemoteException  communication error
     * @throws com.sun.grid.grm.GrmException if the resource could not be created
     * @return the resource
     */
    public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
        return service.addNewResource(type, properties);
    }

    /**
     * Triggers the addResource operation on the target service.
     * @param resource the resource to be added
     * @param sloName if the resource is added because of a SLO request it contains the SLO name
     * @throws com.sun.grid.grm.resource.InvalidResourceException if the target service considers the resource as invalid
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the target service is not active
     */
    public void addResource(Resource resource, String sloName) throws InvalidResourceException, ServiceNotActiveException {
        log.entering(ServiceCachingProxy.class.getName(), "addResource", resource);
        try {
            if(log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.addResource", new Object [] { serviceName, resource, sloName });
            }
            service.addResource(resource, sloName);
        } catch (GrmRemoteException ex) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, I18NManager.formatMessage("scp.grexa.ar", BUNDLE, serviceName, resource, ex.getLocalizedMessage()), ex);
            }
            // service appears to be active, but remote call failed. do full refresh
            fullRefresh();
            ServiceNotActiveException snae = new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
            log.throwing(ServiceCachingProxy.class.getName(), "addResource", snae);
            throw snae;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "addResource");
    }

    /**
     * Triggers the start (component) operation on the target service.
     *
     * @throws com.sun.grid.grm.GrmException if the call to the target
     * service fails
     */
    public void start() throws GrmException {
        log.entering(ServiceCachingProxy.class.getName(), "start");
        try {
            service.start();
        } catch (GrmException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            log.throwing(ServiceCachingProxy.class.getName(), "start", ex);
            throw ex;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "start");
    }

    /**
     * Triggers the stop (component) operation on the target service.
     *
     * @throws com.sun.grid.grm.GrmException if the call to the target
     * service fails
     */
    public void stop(boolean isForced) throws GrmException {
        log.entering(ServiceCachingProxy.class.getName(), "stop", isForced);
        try {
            service.stop(isForced);
        } catch (GrmException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            log.throwing(ServiceCachingProxy.class.getName(), "stop", ex);
            throw ex;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "stop");
    }

    /**
     * Triggers the reload (component) operation on the target service.
     *
     * @throws com.sun.grid.grm.GrmException if the call to the target
     * service fails
     */
    public void reload(boolean isForced) throws GrmException {
        log.entering(ServiceCachingProxy.class.getName(), "reload", isForced);
        try {
            service.reload(isForced);
        } catch (GrmException ex) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
            }
            // we do not know what went wrong, rather do full refresh
            fullRefresh();
            log.throwing(ServiceCachingProxy.class.getName(), "reload", ex);
            throw ex;
        }
        log.exiting(ServiceCachingProxy.class.getName(), "reload");
    }

    /**
     * Registers the component event listener. Events coming from the
     * target service are relayed to the listener if the SCP determines that
     * event is not doubled or obsolete. SCP will send additional events, if
     * it detects that cached information is out of sync.
     *
     * @param componentEventListener the listener to add
     */
    public void addComponentEventListener(ComponentEventListener componentEventListener) {
        log.entering(ServiceCachingProxy.class.getName(), "addComponentEventListener", componentEventListener);
        svcEventForwarder.addComponentEventListener(componentEventListener);
        log.exiting(ServiceCachingProxy.class.getName(), "addComponentEventListener");
    }

    /**
     * Unregisters the component event listener.
     *
     * @param componentEventListener the listener to remove
     */
    public void removeComponentEventListener(ComponentEventListener componentEventListener) {
        log.entering(ServiceCachingProxy.class.getName(), "removeComponentEventListener", componentEventListener);
        svcEventForwarder.removeComponentEventListener(componentEventListener);
        log.exiting(ServiceCachingProxy.class.getName(), "removeComponentEventListener");
    }

    /**
     * Gets the service name.
     *
     * @return the service name
     */
    public String getName() {
        log.entering(ServiceCachingProxy.class.getName(), "getName");
        log.exiting(ServiceCachingProxy.class.getName(), "getName", serviceName);
        return serviceName;
    }

    /**
     * Gets the name of host on which runs the service.
     *
     * @return the host name
     */
    public Hostname getHostname() {
        log.entering(ServiceCachingProxy.class.getName(), "getHostname");
        log.exiting(ServiceCachingProxy.class.getName(), "getHostname", hostname);
        return hostname;
    }

    /**
     * Inner class for listening to the events coming from the target service
     * and relaying them to all registered listeners.
     */
    private class SCPServiceEventListener implements ServiceEventListener {

        /* propagate the event */
        public void resourceRequest(ResourceRequestEvent event) {
            addServiceAction(event, new ResourceRequestAction(event));
        }

        /* refresh the cached resource and propagate the event */
        public void addResource(AddResourceEvent event) {
            addServiceAction(event, new AddResourceAction(event));
        }

        /* refresh the cached resources and propagate the event */
        public void resourceAdded(ResourceAddedEvent event) {
            addServiceAction(event, new ResourceAddedAction(event));
        }

        /* refresh the cached resource and propagate the event */
        public void removeResource(RemoveResourceEvent event) {
            addServiceAction(event, new RemoveResourceAction(event));
        }

        /* refresh the cached resources and propagate the event */
        public void resourceRemoved(ResourceRemovedEvent event) {
            addServiceAction(event, new ResourceRemovedAction(event));
        }

        /* refresh the cached resource and propagate the event */
        public void resourceRejected(ResourceRejectedEvent event) {
            addServiceAction(event, new ResourceRejectedAction(event));
        }

        /* refresh the cached resource and propagate the event */
        public void resourceError(ResourceErrorEvent event) {
            addServiceAction(event, new ResourceErrorAction(event));
        }

        /* refresh the cached resource and propagate the event */
        public void resourceReset(ResourceResetEvent event) {
            addServiceAction(event, new ResourceResetAction(event));
        }

        /* refresh the cached resource and propagate the event */
        public void resourceChanged(ResourceChangedEvent event) {
            addServiceAction(event, new ResourceChangedAction(event));
        }

        /* update the cached service state */
        public void serviceStarting(ServiceStateChangedEvent event) {
            addServiceAction(event, new ServiceStateChangedAction(event));
        }

        /* perform full refresh */
        public void serviceRunning(ServiceStateChangedEvent event) {
            addServiceAction(event, new ServiceRUNNINGAction(event));
        }

        /* update the cached service state */
        public void serviceUnknown(ServiceStateChangedEvent event) {
            addServiceAction(event, new ServiceNOT_ACTIVEAction(event));
        }

        /* update the cached service state */
        public void serviceShutdown(ServiceStateChangedEvent event) {
            addServiceAction(event, new ServiceStateChangedAction(event));
        }

        /* update the cached service state */
        public void serviceError(ServiceStateChangedEvent event) {
            addServiceAction(event, new ServiceNOT_ACTIVEAction(event));
        }

        /* update the cached service state */
        public void serviceStopped(ServiceStateChangedEvent event) {
            addServiceAction(event, new ServiceNOT_ACTIVEAction(event));
        }
    }

    /**
     * Inner class for listening to the events coming from the target service
     * and relaying them to all registered listeners.
     */
    private class SCPComponentEventListener implements ComponentEventListener {

        /* update the cached component state */
        public void componentUnknown(ComponentStateChangedEvent event) {
            addComponentAction(event, new ComponentStateChangedAction(event));
        }

        /* update the cached component state */
        public void componentStarting(ComponentStateChangedEvent event) {
            addComponentAction(event, new ComponentStateChangedAction(event));
        }

        /* update the cached component state */
        public void componentStarted(ComponentStateChangedEvent event) {
            addComponentAction(event, new ComponentStateChangedAction(event));
        }

        /* update the cached component state */
        public void componentStopping(ComponentStateChangedEvent event) {
            addComponentAction(event, new ComponentStateChangedAction(event));
        }

        /* update the cached component state */
        public void componentStopped(ComponentStateChangedEvent event) {
            addComponentAction(event, new ComponentStateChangedAction(event));
        }

        /* update the cached component state */
        public void componentReloading(ComponentStateChangedEvent event) {
            addComponentAction(event, new ComponentStateChangedAction(event));
        }

        public void connectionClosed() {
            log.entering(ServiceCachingProxy.class.getName(), "connectionClosed");
            fullRefresh();
            log.exiting(ServiceCachingProxy.class.getName(), "addComponentAction");
        }

        public void connectionFailed() {
            log.entering(ServiceCachingProxy.class.getName(), "connectionFailed");
            fullRefresh();
            log.exiting(ServiceCachingProxy.class.getName(), "connectionFailed");
        }

        public void eventsLost() {
            log.entering(ServiceCachingProxy.class.getName(), "eventsLost");
            fullRefresh();
            log.exiting(ServiceCachingProxy.class.getName(), "eventsLost");
        }
    }

    /* helper method for submitting an action triggered by a service event */
    private void addServiceAction(AbstractServiceEvent e, Action action) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ServiceCachingProxy.class.getName(), "addServiceAction", new Object[]{e, action});
        }
        actionLock.lock();
        try {
            if (startActionProcessing()) {
                scheduledActions.put(action, actionExecutor.submit(action));
            }
        } finally {
            actionLock.unlock();
        }
        log.exiting(ServiceCachingProxy.class.getName(), "addServiceAction");
    }

    /**
     * <p>This method checks whether there are any active or pending actions
     *    in the Service Caching Proxy</p>
     * 
     * <p>This method is used only the junit test to check the actions processing
     *    if SCP has been finished</p>
     * @return <code>true</code> if there is an active or pending action
     */
    boolean hasActiveActions() {
        actionLock.lock();
        try {
            if ((actionExecutor == null) || actionExecutor.isTerminated()) {
                return false;
            }

            if (scheduledRefresh != null && !scheduledRefresh.isDone()) {
                return true;
            }
            if (scheduledActions.isEmpty()) {
                return false;
            }

            for (Future f : scheduledActions.values()) {
                if (!(f.isDone() || f.isCancelled())) {
                    return true;
                }
            }
            return false;
        } finally {
            actionLock.unlock();
        }
    }

    /* helper method for submitting an action triggered by a component event */
    private void addComponentAction(AbstractComponentEvent e, Action action) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ServiceCachingProxy.class.getName(), "addComponentAction", new Object[]{e, action});
        }
        actionLock.lock();
        try {
            if (startActionProcessing()) {
                scheduledActions.put(action, actionExecutor.submit(action));
            }
        } finally {
            actionLock.unlock();
        }
        log.exiting(ServiceCachingProxy.class.getName(), "addComponentAction");
    }

    /**
     * helper method for submitting a full refresh action.
     *
     * Package private for testing
     */
    void fullRefresh() {
        log.entering(ServiceCachingProxy.class.getName(), "fullRefresh");
        actionLock.lock();
        try {
            if (startActionProcessing()) {
                if ((scheduledRefresh != null) && (!scheduledRefresh.isDone())) {
                    /* do not care if cancellation fails - in worst case, full refresh is executed twice */
                    scheduledRefresh.cancel(false);
                }
                scheduledRefresh = actionExecutor.submit(new RefreshAction());
            }
        } finally {
            actionLock.unlock();
        }
        log.exiting(ServiceCachingProxy.class.getName(), "fullRefresh");
    }

    /**
     * Wait until the scheduled refresh is finished.
     * 
     * This method is only used in junit test.
     * 
     * @param timeout max waiting time in milliseconds
     * @throws java.lang.InterruptedException  if the calling thread has been interrupted
     * @throws java.util.concurrent.TimeoutException    if the timeout elapsed
     * @throws java.util.concurrent.ExecutionException  if the scheduled refresh produced an error
     */
    void awaitEndOfFullRefresh(long timeout) throws InterruptedException, TimeoutException, ExecutionException {
        Future future = null;
        actionLock.lock();
        try {
            future = scheduledRefresh;
        } finally {
            actionLock.unlock();
        }
        if (future != null) {
            future.get(timeout, TimeUnit.MILLISECONDS);
        }
    }

    /**
     * Helper class for processing the service and component events.
     */
    abstract class Action implements Callable<Void> {

        /**
         * All 'unexpected' exceptions are consumed and logged by {@link Action#run()}  method.
         *
         * @throws java.lang.Exception if unexpected error occurs.
         */
        public abstract void execute() throws Exception;

        public final Void call() {
            try {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.action.exe", new Object[]{getName(), this.toString()});
                }
                execute();
            } catch (Exception ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "scp.a.unexpected");
                    lr.setParameters(new Object[]{getName(), this, ex});
                    lr.setThrown(ex);
                    lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                    log.log(lr);
                }
            } finally {
                actionLock.lock();
                try {
                    scheduledActions.remove(this);
                } finally {
                    actionLock.unlock();
                }
            }
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.action.fini", new Object[]{getName(), this.toString()});
            }
            return null;
        }
    }

    /**
     * Abstract base class of all actions which has been triggered by a ServiceEvent
     * or a ComponentEvent.
     */
    private abstract class AbstractEventTriggeredAction extends Action {

        /**
         * Get the sequence number of the event
         * @return the sequence number
         */
        protected abstract long getEventSequenceNumber();

        @Override
        public final void execute() throws Exception {
            long seqNo = getEventSequenceNumber();
            long expSeqNo = nextExpectedEventSequenceNumber.get();
            if (expSeqNo == -1) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.event.ignore.initial", new Object[]{getName(), this.toString()});
                }
            } else if (seqNo == 0 && expSeqNo > 0) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.event.ingore.restart",
                            new Object[]{getName(), this.toString(), seqNo, expSeqNo});
                }
                // A new life cylce of the service has started
                fullRefresh();
            } else if (seqNo < expSeqNo) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.event.ingore.outdated",
                            new Object[]{getName(), this.toString(), seqNo, expSeqNo});
                }
            } else if (seqNo > expSeqNo) {
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.event.ingore.unexpected",
                            new Object[]{getName(), this.toString(), seqNo, expSeqNo});
                }
                fullRefresh();
            } else {
                nextExpectedEventSequenceNumber.incrementAndGet();
                executeAction();
            }
        }

        protected abstract void executeAction() throws Exception;
    }

    /**
     * Abstract base class of all actions triggered by a service event
     * @param <T> the type of the event
     */
    private abstract class AbstractServiceEventAction<T extends AbstractServiceEvent> extends AbstractEventTriggeredAction {

        protected final T event;

        protected AbstractServiceEventAction(T event) {
            if (event == null) {
                throw new IllegalArgumentException("Event must not be null!");
            }
            this.event = event;
        }

        protected long getEventSequenceNumber() {
            return event.getSequenceNumber();
        }

        @Override
        public final String toString() {
            return String.format("[%s:ev=%s]", getClass().getSimpleName(), event);
        }
    }

    /**
     * Helper class for processing the service events that deals with single resource.
     */
    private abstract class SingleResourceAction<T extends AbstractServiceChangedResourceEvent> extends AbstractServiceEventAction<T> {

        final protected Resource resource;
        final protected T e;

        SingleResourceAction(T event) {
            super(event);
            if (event.getResource() == null) {
                throw new IllegalArgumentException("Event's resource must not be null!");
            }
            this.resource = event.getResource().clone();
            this.e = event;
        }
    }

    /* helper class - encapsulates processing of ResourceRequestEvent */
    private class ResourceRequestAction extends AbstractServiceEventAction<ResourceRequestEvent> {

        ResourceRequestAction(ResourceRequestEvent event) {
            super(event);
        }

        @Override
        protected void executeAction() throws Exception {
            String sloName = new String(event.getSLOName());
            ArrayList<Need> needz = new ArrayList<Need>(event.getNeeds().size());
            for (Need n : event.getNeeds()) {
                needz.add(n.clone());
            }
            svcEventForwarder.fireResourceRequest(sloName, needz);
            cachedResourceRequestMap.put(sloName, needz);
        }
    }

    /**
     * AddResourceAction action synchronizes the resource information
     * using the received AddResourceEvent.
     *
     * It follows the rules:
     *
     * 0. the resource id is registered
     * 1. if resource is already cached, update the resource and fire related events.
     * 2. if resource is not cached, resource is added to cache and fire related events.
     *
     * cached       state of cached     remote resource diff    forwarded events
     * no           -                   -                       AddResourceEvent
     * yes          ASSIGNING           no                      -
     * yes          ASSIGNING           yes                     ResourceChangedEvent
     * yes          ASSIGNED            no                      AddResourceEvent
     * yes          ASSIGNED            yes                     AddResourceEvent, ResourceChangedEvent
     * yes          ERROR               no                      AddResourceEvent
     * yes          ERROR               yes                     AddResourceEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      AddResourceEvent
     * yes          UNASSIGNING         yes                     AddResourceEvent, ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) AddResourceEvent
     * yes          UNASSIGNED          yes                     (bug) AddResourceEvent, ResourceChangedEvent
     *
     */
    private class AddResourceAction extends SingleResourceAction<AddResourceEvent> {

        AddResourceAction(AddResourceEvent e) {
            super(e);
        }

        @Override
        protected void executeAction() throws Exception {
            if (!ServiceState.RUNNING.equals(getServiceState())) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sna", new Object[]{serviceName, e});
                }
                return;
            }

            Resource r = this.resource.clone();
            Resource cached = null;
            Collection<ResourceChanged> patch = Collections.<ResourceChanged>emptyList();
            Resource.State cachedState = null;

            try {
                cached = visible_resources.getResource(r.getId());
            } catch (UnknownResourceException ure) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.enc", new Object[]{serviceName, r});
                }
            }

            if (cached == null) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nc", new Object[]{serviceName, r});
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.c", new Object[]{serviceName, r});
                }
                /* store state of cached resource to know which event has to be fired */
                cachedState = cached.getState();

                /* track changes between cached and remote resource */
                patch = AbstractResourceChanged.getChanges(cached, r);
            }

            /* cache the resource and be sure that resource is in
             * ASSIGNING state - it HAS to be as the event is AddResource */
            r.setState(Resource.State.ASSIGNING);
            visible_resources.add(r);

            /* if resource is already cached:
             * 1. we may have lost several events, but that is
             * discovered by isServiceEventNext which also schedules fullRefresh
             * 2. resource was synchronized using fullRefresh().
             *
             * anyway, service owns the resource so we will send respective events.
             */
            if (Resource.State.ASSIGNING.equals(cachedState)) {
                /* no need to send event, cached resource is in the same
                 * state as remote resource */
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "scp.s.senf", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ASSIGNED.equals(cachedState)) {
                /* forward add event */
                svcEventForwarder.fireAddResource(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ERROR.equals(cachedState)) {
                /* forward add event */
                svcEventForwarder.fireAddResource(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNING.equals(cachedState)) {
                /* forward add event */
                svcEventForwarder.fireAddResource(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNED.equals(cachedState)) {
                /* if this happens, we have bug in implementation */
                /* treat as ERROR state, forward add event */
                svcEventForwarder.fireAddResource(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.s.cu", new Object[]{serviceName, cached});
                }
            } else {
                /* forward add event */
                svcEventForwarder.fireAddResource(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nyc", new Object[]{serviceName, r});
                }
            }

            /* if remote resource differs from cached one, fire changed event */
            if (!patch.isEmpty()) {
                svcEventForwarder.fireResourceChanged(r.clone(), patch, I18NManager.formatMessage("scp.s.rc.msg", BUNDLE));
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.rc", new Object[]{serviceName, r});
                }
            }
        }
    }

    /**
     * ResourceAddedAction action synchronizes the resource information
     * using the received ResourceAddedEvent.
     *
     * It follows the rules:
     *
     * 0. the resource id is registered
     * 1. if resource is already cached, update the resource and fire related events.
     * 2. if resource is not cached, resource is added to cache and fire related events.
     *
     * cached       state of cached     remote resource diff    forwarded events
     * no           -                   -                       AddResourceEvent, ResourceAddedEvent
     * yes          ASSIGNING           no                      ResourceAddedEvent
     * yes          ASSIGNING           yes                     ResourceAddedEvent, ResourceChangedEvent
     * yes          ASSIGNED            no                      -
     * yes          ASSIGNED            yes                     ResourceAddedEvent, ResourceChangedEvent
     * yes          ERROR               no                      ResourceResetEvent
     * yes          ERROR               yes                     ResourceResetEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      AddResourceEvent, ResourceAddedEvent
     * yes          UNASSIGNING         yes                     AddResourceEvent, ResourceAddedEvent, ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) ResourceResetEvent,
     * yes          UNASSIGNED          yes                     (bug) ResourceResetEvent, ResourceChangedEvent
     *
     */
    private class AbstractResourceAddedAction<T extends AbstractServiceChangedResourceEvent> extends SingleResourceAction<T> {

        AbstractResourceAddedAction(T e) {
            super(e);
        }

        @Override
        protected final void executeAction() throws Exception {
            if (!ServiceState.RUNNING.equals(cachedServiceState.get())) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sna", new Object[]{serviceName, e});
                }
                return;
            }

            Resource r = this.resource.clone();
            Resource cached = null;
            Collection<ResourceChanged> patch = Collections.<ResourceChanged>emptyList();
            Resource.State cachedState = null;

            try {
                cached = visible_resources.getResource(r.getId());
            } catch (UnknownResourceException ure) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.enc", new Object[]{serviceName, r});
                }
            }

            if (cached == null) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nc", new Object[]{serviceName, r});
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.c", new Object[]{serviceName, r});
                }
                /* store state of cached resource to know which event has to be fired */
                cachedState = cached.getState();

                /* if resource is not in ASSIGNING state, we have lost some events.
                 * full refresh will handle it - no need to trigger it immediately,
                 * it will be triggered automatically by isServiceEventNext method */
                if (!Resource.State.ASSIGNING.equals(cached.getState())) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "scp.raa.ning", new Object[]{serviceName, r});
                    }

                }
                /* track changes between cached and remote resource */
                patch = AbstractResourceChanged.getChanges(cached, r);
            }

            /* cache the resource and be sure that resource is in
             * ASSIGNED state - it HAS to be as the event is ResourceAdded */
            r.setState(Resource.State.ASSIGNED);
            visible_resources.add(r);

            /* if resource is cached but in different state than ASSIGNING:
             * 1. we may have lost several events, but that is
             * discovered by isServiceEventNext which also schedules fullRefresh
             * 2. resource was synchronized using fullRefresh().
             *
             * anyway, service owns the resource so we will send respective events.
             *
             * if resource is not cached, fire also add event.
             */
            if (Resource.State.ASSIGNING.equals(cachedState)) {
                /* forward added event */
                if (e instanceof ResourceResetEvent) {
                    svcEventForwarder.fireResourceReset(r.clone(), e.getMessage());
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                    }
                } else if (e instanceof ResourceAddedEvent) {
                    svcEventForwarder.fireResourceAdded(r.clone(), e.getMessage());
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                    }
                } else {
                    throw new IllegalStateException("AbstractResourceAddedAction can only handle ResourceAddedEvent or ResourceResetEvent, but got " + e.getClass().getSimpleName());
                }
            } else if (Resource.State.ASSIGNED.equals(cachedState)) {
                /* no need to send event, cached resource is in the same
                 * state as remote resource */
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "scp.s.senf", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ERROR.equals(cachedState)) {
                /* forward reset event */
                svcEventForwarder.fireResourceReset(r.clone(), I18NManager.formatMessage("scp.s.rs.msg", BUNDLE));
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNING.equals(cachedState)) {
                /* forward add and added event */
                Resource clone = r.clone();
                clone.setState(Resource.State.ASSIGNING);
                svcEventForwarder.fireAddResource(clone, I18NManager.formatMessage("scp.s.a.msg", BUNDLE));
                clone.setState(Resource.State.ASSIGNED);
                svcEventForwarder.fireResourceAdded(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNED.equals(cachedState)) {
                /* if this happens, we have bug in implementation */
                /* treat as ERROR state, forward add and added event */
                Resource clone = r.clone();
                clone.setState(Resource.State.ASSIGNING);
                svcEventForwarder.fireAddResource(clone, I18NManager.formatMessage("scp.s.a.msg", BUNDLE));
                clone.setState(Resource.State.ASSIGNED);
                svcEventForwarder.fireResourceAdded(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.s.cu", new Object[]{serviceName, cached});
                }
            } else {
                /* forward add and added event */
                Resource clone = r.clone();
                clone.setState(Resource.State.ASSIGNING);
                svcEventForwarder.fireAddResource(clone, I18NManager.formatMessage("scp.s.a.msg", BUNDLE));
                clone.setState(Resource.State.ASSIGNED);
                svcEventForwarder.fireResourceAdded(clone, I18NManager.formatMessage("scp.ra.ad.msg", BUNDLE));
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nyc", new Object[]{serviceName, r});
                }
            }

            /* if remote resource differs from cached one, fire changed event */
            if (!patch.isEmpty()) {
                svcEventForwarder.fireResourceChanged(r.clone(), patch, I18NManager.formatMessage("scp.s.rc.msg", BUNDLE));
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.rc", new Object[]{serviceName, r});
                }
            }
        }
    }

    private class ResourceAddedAction extends AbstractResourceAddedAction<ResourceAddedEvent> {

        ResourceAddedAction(ResourceAddedEvent e) {
            super(e);
        }
    }

    /**
     * RemoveResourceAction action action synchronizes the resource information
     * using the received RemoveResourceEvent.
     *
     * It follows the rules:
     *
     * 0. the resource id is registered
     * 1. if resource is already cached, update the resource and fire related events.
     * 2. if resource is not cached, resource is added to cache and fire related events.
     *
     * cached       state of cached     remote resource diff    forwarded events
     * no           -                   -                       AddResourceEvent, ResourceAddedEvent, RemoveResourceEvent
     * yes          ASSIGNING           no                      ResourceAddedEvent, RemoveResourceEvent
     * yes          ASSIGNING           yes                     ResourceAddedEvent, RemoveResourceEvent, ResourceChangedEvent
     * yes          ASSIGNED            no                      RemoveResourceEvent
     * yes          ASSIGNED            yes                     RemoveResourceEvent, ResourceChangedEvent
     * yes          ERROR               no                      RemoveResourceEvent
     * yes          ERROR               yes                     RemoveResourceEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      -
     * yes          UNASSIGNING         yes                     ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) RemoveResourceEvent
     * yes          UNASSIGNED          yes                     (bug) RemoveResourceEvent, ResourceChangedEvent
     *
     */
    private class RemoveResourceAction extends SingleResourceAction<RemoveResourceEvent> {

        RemoveResourceAction(RemoveResourceEvent e) {
            super(e);
        }

        @Override
        protected void executeAction() throws Exception {
            ServiceState ss = cachedServiceState.get();
            if (!ServiceState.RUNNING.equals(ss) && !ServiceState.SHUTDOWN.equals(ss)) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sna", new Object[]{serviceName, e});
                }
                return;
            }

            Resource r = this.resource.clone();
            Resource cached = null;
            Collection<ResourceChanged> patch = Collections.<ResourceChanged>emptyList();
            Resource.State cachedState = null;

            try {
                cached = visible_resources.getResource(r.getId());
            } catch (UnknownResourceException ure) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.enc", new Object[]{serviceName, r});
                }
            }

            if (cached == null) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nc", new Object[]{serviceName, r});
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.c", new Object[]{serviceName, r});
                }
                /* store state of cached resource to know which event has to be fired */
                cachedState = cached.getState();
                /* track changes between cached and remote resource */
                patch = AbstractResourceChanged.getChanges(cached, r);
            }

            /* cache the resource and be sure that resource is in
             * UNASIGNING state - it HAS to be as the event is RemoveResource */
            r.setState(Resource.State.UNASSIGNING);
            visible_resources.add(r);

            /* if resource is cached but in different state than ASSIGNED:
             * 1. we may have lost several events, but that is
             * discovered by isServiceEventNext which also schedules fullRefresh
             * 2. resource was synchronized using fullRefresh().
             *
             * anyway, service STILL owns the resource so we will send respective events.
             */
            if (Resource.State.ASSIGNING.equals(cachedState)) {
                /* Forwared added and remove events */
                Resource clone = r.clone();
                clone.setState(Resource.State.ASSIGNED);
                svcEventForwarder.fireResourceAdded(clone, I18NManager.formatMessage("scp.s.ad.msg", BUNDLE));
                clone.setState(Resource.State.UNASSIGNING);
                svcEventForwarder.fireRemoveResource(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ASSIGNED.equals(cachedState)) {
                /* forward remove event */
                svcEventForwarder.fireRemoveResource(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ERROR.equals(cachedState)) {
                /* forward remove event */
                svcEventForwarder.fireRemoveResource(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNING.equals(cachedState)) {
                /* nothing needs to be done */
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "scp.ra.senf", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNED.equals(cachedState)) {
                /* if this happens, we have bug in implementation */
                /* treat as ERROR state, forward remove event */
                svcEventForwarder.fireRemoveResource(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.s.cu", new Object[]{serviceName, cached});
                }
            } else {
                /* forward add, added and remove events */
                Resource clone = r.clone();
                clone.setState(Resource.State.ASSIGNING);
                svcEventForwarder.fireAddResource(clone, I18NManager.formatMessage("scp.ra.a.msg", BUNDLE));
                clone.setState(Resource.State.ASSIGNED);
                svcEventForwarder.fireResourceAdded(clone, I18NManager.formatMessage("scp.ra.ad.msg", BUNDLE));
                clone.setState(Resource.State.UNASSIGNING);
                svcEventForwarder.fireRemoveResource(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nyc", new Object[]{serviceName, r});
                }
            }

            /* if remote resource differs from cached one, fire changed event */
            if (!patch.isEmpty()) {
                svcEventForwarder.fireResourceChanged(r.clone(), patch, I18NManager.formatMessage("scp.s.rc.msg", BUNDLE));
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.rc", new Object[]{serviceName, r});
                }
            }
        }
    }

    /**
     * ResourceRemovedAction action action synchronizes the resource information
     * using the received ResourceRemovedEvent.
     *
     * It follows the rules:
     *
     * 0. the resource id is unregistered
     * 1. if resource is cached, remove the resource and fire related events.
     * 2. if resource is not cached, log the resource properties and to nothing.
     *
     * cached       state of cached     remote resource diff    forwarded events
     * no           -                   -                       - (log resource with its properties)
     * yes          ASSIGNING           -                       ResourceAddedEvent, RemoveResourceEvent, ResourceRemovedEvent
     * yes          ASSIGNED            -                       RemoveResourceEvent, ResourceRemovedEvent
     * yes          ERROR               -                       RemoveResourceEvent, ResourceRemovedEvent
     * yes          UNASSIGNING         -                       -
     * yes          UNASSIGNED          -                       (bug) RemoveResourceEvent, ResourceRemovedEvent
     *
     */
    private class ResourceRemovedAction extends SingleResourceAction<ResourceRemovedEvent> {

        ResourceRemovedAction(ResourceRemovedEvent e) {
            super(e);
        }

        @Override
        protected void executeAction() throws Exception {
            ServiceState ss = cachedServiceState.get();
            if (!ServiceState.RUNNING.equals(ss) && !ServiceState.SHUTDOWN.equals(ss)) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sna", new Object[]{serviceName, e});
                }
                return;
            }

            Resource r = this.resource.clone();
            Resource cached = null;
            Resource.State cachedState = null;

            try {
                /* remove the cached resource */
                cached = visible_resources.remove(r.getId());
            } catch (UnknownResourceException ure) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.enc", new Object[]{serviceName, r});
                }
            }

            if (cached == null) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nc", new Object[]{serviceName, r});
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.c", new Object[]{serviceName, r});
                }
                /* store state of cached resource to know which event has to be fired */
                cachedState = cached.getState();
            }

            /* if resource is cached but in different state than UNASSIGNING:
             * 1. we may have lost several events, but that is
             * discovered by isServiceEventNext which also schedules fullRefresh
             * 2. resource was synchronized using fullRefresh().
             *
             * in both cases, we will forward all state transition events.
             *
             * if resource is not cached:
             * 3. resource was removed during full refresh, in that case event was fired and we do not have to anyting
             * 4. adding of resource to service bypassed SCP (otherwise resource would be cached) and removed event
             * was fired before full refresh has occured.
             *
             * if we decide to fire removed event, we risk double assigned
             * resource (we have no chance to find out whether 3 or 4 happened),
             * so we will rather save whole resource information to log file instead
             * of firing removed event (but we risk losing of resource for all)
             *
             * TODO if we introduce in the future binary data properties, it'd
             * be better to store the resource snapshot to filesystem or to db.
             */
            if(!r.getState().equals(Resource.State.UNASSIGNED)) {
                log.log(Level.WARNING, "scp.rem.invalidResourceState",
                        new Object [] {serviceName, r, r.getState(), Resource.State.UNASSIGNED});
            }


            if (Resource.State.ASSIGNING.equals(cachedState)) {
                /* Forward added, remove and removed events */
                Resource clone = r.clone();
                clone.setState(Resource.State.ASSIGNED);
                svcEventForwarder.fireResourceAdded(clone, I18NManager.formatMessage("scp.s.ad.msg", BUNDLE));
                clone.setState(Resource.State.UNASSIGNING);
                svcEventForwarder.fireRemoveResource(clone, I18NManager.formatMessage("scp.s.u.msg", BUNDLE));
                clone.setState(Resource.State.UNASSIGNED);
                svcEventForwarder.fireResourceRemoved(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ASSIGNED.equals(cachedState)) {
                /* forward remove and removed event */
                Resource clone = r.clone();
                clone.setState(Resource.State.UNASSIGNING);
                svcEventForwarder.fireRemoveResource(clone, I18NManager.formatMessage("scp.s.u.msg", BUNDLE));
                clone.setState(Resource.State.UNASSIGNED);
                svcEventForwarder.fireResourceRemoved(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ERROR.equals(cachedState)) {
                /* forward remove and removed event */
                Resource clone = r.clone();
                clone.setState(Resource.State.UNASSIGNING);
                svcEventForwarder.fireRemoveResource(clone, I18NManager.formatMessage("scp.s.u.msg", BUNDLE));
                r.setState(Resource.State.UNASSIGNED);
                svcEventForwarder.fireResourceRemoved(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNING.equals(cachedState)) {
                /* forward removed event */
                Resource clone = r.clone();
                clone.setState(Resource.State.UNASSIGNED);
                svcEventForwarder.fireResourceRemoved(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNED.equals(cachedState)) {
                /* if this happens, we have bug in implementation */
                /* treat as ERROR state, forward remove and removed event */
                Resource clone = r.clone();
                clone.setState(Resource.State.UNASSIGNING);
                svcEventForwarder.fireRemoveResource(clone, I18NManager.formatMessage("scp.s.u.msg", BUNDLE));
                r.setState(Resource.State.UNASSIGNED);
                svcEventForwarder.fireResourceRemoved(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.s.cu", new Object[]{serviceName, cached});
                }
            } else {
                /* resource is not cached do not forward event, just log resource */
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.s.armd", new Object[]{serviceName, r, r.getProperties()});
                }
            }
        }
    }

    /**
     * ResourceRejectedAction action action synchronizes the resource information
     * using the received ResourceRejectedEvent.
     *
     * It follows the rules:
     *
     * 0. the resource id is unregistered
     * 1. if resource is cached, remove the resource and fire related event.
     * 2. if resource is not cached, do nothing.
     *
     * cached       state of cached     remote resource diff    forwarded events
     * no           -                   -                       -
     * yes          ASSIGNING           -                       ResourceRejectedEvent
     * yes          ASSIGNED            -                       ResourceRejectedEvent
     * yes          ERROR               -                       ResourceRejectedEvent
     * yes          UNASSIGNING         -                       ResourceRejectedEvent
     * yes          UNASSIGNED          -                       (bug) ResourceRejectedEvent
     *
     */
    private class ResourceRejectedAction extends SingleResourceAction<ResourceRejectedEvent> {

        ResourceRejectedAction(ResourceRejectedEvent e) {
            super(e);
        }

        @Override
        protected void executeAction() throws Exception {
            if (!ServiceState.RUNNING.equals(cachedServiceState.get())) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sna", new Object[]{serviceName, e});
                }
                return;
            }

            Resource r = this.resource.clone();
            Resource cached = null;
            Resource.State cachedState = null;

            try {
                /* remove the cached resource */
                cached = visible_resources.remove(r.getId());
            } catch (UnknownResourceException ure) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.enc", new Object[]{serviceName, r});
                }
            }

            if (cached == null) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nc", new Object[]{serviceName, r});
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.c", new Object[]{serviceName, r});
                }
                cachedState = cached.getState();
            }

            /* if resource is cached but in different state than ASSIGNING:
             * 1. we may have lost several events, but that is
             * discovered by isServiceEventNext which also schedules fullRefresh
             * 2. resource was synchronized using fullRefresh().
             *
             * in both cases, we will forward just rejected event.
             *
             * if resource is not cached:
             * 3. resource was removed during full refresh, in that case event was fired and we do not have to anyting
             * 4. adding of resource to service bypassed SCP (otherwise resource would be cached) and rejected event
             * was fired before full refresh has occured.
             *
             * if we decide to fire removed event, we risk double assigned
             * resource (we have no chance to find out whether 3 or 4 happened),
             * so we will rather save whole resource information to log file instead
             * of firing rejected event (but we risk losing of resource for all)
             *
             * TODO if we introduce in the future binary data properties, it'd
             * be better to store the resource snapshot to filesystem or to db.
             */
            if(!r.getState().equals(Resource.State.ASSIGNING)) {
                log.log(Level.WARNING, "scp.rej.invalidResourceState",
                        new Object [] {serviceName, r, r.getState(), Resource.State.ASSIGNING});
            }

            Resource clone = r.clone();
            r.setState(Resource.State.UNASSIGNED);
            if (Resource.State.ASSIGNING.equals(cachedState)) {
                /* fire rejected event */
                svcEventForwarder.fireResourceRejected(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ASSIGNED.equals(cachedState)) {
                /* fire rejected event */
                svcEventForwarder.fireResourceRejected(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ERROR.equals(cachedState)) {
                /* fire rejected event */
                svcEventForwarder.fireResourceRejected(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNING.equals(cachedState)) {
                /* fire rejected event */
                svcEventForwarder.fireResourceRejected(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNED.equals(cachedState)) {
                /* if this happens, we have bug in implementation */
                /* anyway, fire a rejected event */
                svcEventForwarder.fireResourceRejected(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.s.cu", new Object[]{serviceName, cached});
                }
            } else {
                /* do not forward the event - it could lead to double assigned resource */
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.s.arjd", new Object[]{serviceName, r, r.getProperties()});
                }
            }
        }
    }

    /**
     * ResourceErrorAction action synchronizes the resource information
     * using the received ResourceErrorEvent.
     *
     * It follows the rules:
     *
     * 0. the resource id is registered
     * 1. if resource is already cached, update the resource and fire related events.
     * 2. if resource is not cached, resource is added to cache and fire related events.
     *
     * cached       state of cached     remote resource diff    forwarded events
     * no           -                   -                       AddResourceEvent, ResourceAddedEvent
     * yes          ASSIGNING           no                      ResourceAddedEvent
     * yes          ASSIGNING           yes                     ResourceAddedEvent, ResourceChangedEvent
     * yes          ASSIGNED            no                      -
     * yes          ASSIGNED            yes                     ResourceAddedEvent, ResourceChangedEvent
     * yes          ERROR               no                      ResourceResetEvent
     * yes          ERROR               yes                     ResourceResetEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      AddResourceEvent, ResourceAddedEvent
     * yes          UNASSIGNING         yes                     AddResourceEvent, ResourceAddedEvent, ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) ResourceResetEvent,
     * yes          UNASSIGNED          yes                     (bug) ResourceResetEvent, ResourceChangedEvent
     *
     */
    private class ResourceErrorAction extends SingleResourceAction<ResourceErrorEvent> {

        ResourceErrorAction(ResourceErrorEvent e) {
            super(e);
        }

        @Override
        protected void executeAction() throws Exception {
            if (!ServiceState.RUNNING.equals(cachedServiceState.get())) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sna", new Object[]{serviceName, e});
                }
                return;
            }

            Resource r = this.resource.clone();
            Resource cached = null;
            Collection<ResourceChanged> patch = Collections.<ResourceChanged>emptyList();
            Resource.State cachedState = null;

            try {
                cached = visible_resources.getResource(r.getId());
            } catch (UnknownResourceException ure) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.enc", new Object[]{serviceName, r});
                }
            }

            if (cached == null) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nc", new Object[]{serviceName, r});
                }
            /* if this happens, we have lost some event and full refresh will handle it - no
             * need to trigger full refresh immediately, it will be triggered automatically */
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.c", new Object[]{serviceName, r});
                }
                /* store state of cached resource to know which event has to be fired */
                cachedState = cached.getState();

                /* track changes between cached and remote resource */
                patch = AbstractResourceChanged.getChanges(cached, r);
            }

            /* update the cached resource - set state to ERROR as event is ResourceError */
            r.setState(Resource.State.ERROR);
            visible_resources.add(r);

            /* if resource is cached but in different state than ASSIGNING:
             * 1. we may have lost several events, but that is
             * discovered by isServiceEventNext which also schedules fullRefresh
             * 2. resource was synchronized using fullRefresh().
             *
             * anyway, service owns the resource so we will send respective events.
             *
             * if resource is not yet cached, fired add event also.
             */
            if (Resource.State.ASSIGNING.equals(cachedState)) {
                /* fire error event */
                svcEventForwarder.fireResourceError(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ASSIGNED.equals(cachedState)) {
                /* forward error event */
                svcEventForwarder.fireResourceError(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.ERROR.equals(cachedState)) {
                /* nothing needs to be done */
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "scp.s.senf", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNING.equals(cachedState)) {
                /* forward error event */
                svcEventForwarder.fireResourceError(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
            } else if (Resource.State.UNASSIGNED.equals(cachedState)) {
                /* if this happens, we have bug in implementation */
                /* it is not suitable to treat it as error state, so fire error event */
                svcEventForwarder.fireResourceError(r.clone(), e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sef", new Object[]{serviceName, cachedState, r});
                }
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "scp.s.cu", new Object[]{serviceName, cached});
                }
            } else {
                /* forward add and error event */
                Resource clone = r.clone();
                clone.setState(Resource.State.ASSIGNING);
                svcEventForwarder.fireAddResource(clone, I18NManager.formatMessage("scp.s.a.msg", BUNDLE));
                clone.setState(Resource.State.ERROR);
                svcEventForwarder.fireResourceError(clone, e.getMessage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nyc", new Object[]{serviceName, r});
                }
            }

            /* if remote resource differs from cached one, fire changed event */
            if (!patch.isEmpty()) {
                svcEventForwarder.fireResourceChanged(r.clone(), patch, I18NManager.formatMessage("scp.s.rc.msg", BUNDLE));
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.rc", new Object[]{serviceName, r});
                }
            }
        }
    }

    /**
     * ResourceResetAction action synchronizes the resource information
     * using the received ResourceResetEvent.
     *
     * It follows the rules:
     *
     * 0. the resource id is registered
     * 1. if resource is already cached, update the resource and fire related events.
     * 2. if resource is not cached, resource is added to cache and fire related events.
     *
     * cached       state of cached     remote resource diff    forwarded events
     * no           -                   -                       -
     * yes          ASSIGNING           no                      -
     * yes          ASSIGNING           yes                     ResourceChangedEvent
     * yes          ASSIGNED            no                      -
     * yes          ASSIGNED            yes                     ResourceChangedEvent
     * yes          ERROR               no                      ResourceResetEvent
     * yes          ERROR               yes                     ResourceResetEvent, ResourceChangedEvent
     * yes          UNASSIGNING         no                      -
     * yes          UNASSIGNING         yes                     ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) ResourceResetEvent
     * yes          UNASSIGNED          yes                     (bug) ResourceResetEvent, ResourceChangedEvent
     *
     */
    private class ResourceResetAction extends AbstractResourceAddedAction<ResourceResetEvent> {

        ResourceResetAction(ResourceResetEvent e) {
            super(e);
        }
    }

    /**
     * ResourceChangedAction action synchronizes the resource information
     * using the received ResourceChangedEvent.
     *
     * ResourceChangedEvent is not bound to any resource state, so we will keeep
     * cached state of resource and notfire any state change related events
     * (normally scheduled fullRefresh will discover state change and fire events
     * if needed).
     *
     * It follows the rules:
     *
     * 0. the resource id is registered
     * 1. if resource is already cached, update the resource and fire related events.
     * 2. if resource is not cached, schedule full refresh
     *
     * cached       state of cached     remote resource diff    forwarded events
     * no           -                   -                       -
     * yes          ASSIGNING           no                      -
     * yes          ASSIGNING           yes                     ResourceChangedEvent
     * yes          ASSIGNED            no                      -
     * yes          ASSIGNED            yes                     ResourceChangedEvent
     * yes          ERROR               no                      -
     * yes          ERROR               yes                     ResourceChangedEvent
     * yes          UNASSIGNING         no                      -
     * yes          UNASSIGNING         yes                     ResourceChangedEvent
     * yes          UNASSIGNED          no                      (bug) -
     * yes          UNASSIGNED          yes                     (bug) ResourceChangedEvent
     *
     */
    private class ResourceChangedAction extends SingleResourceAction<ResourceChangedEvent> {

        ResourceChangedAction(ResourceChangedEvent e) {
            super(e);
        }

        @Override
        protected void executeAction() throws Exception {
            if (!ServiceState.RUNNING.equals(cachedServiceState.get())) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.sna", new Object[]{serviceName, e});
                }
                return;
            }

            Resource r = this.resource.clone();
            Resource cached = null;
            Collection<ResourceChanged> patch = Collections.<ResourceChanged>emptyList();
            Resource.State cachedState = null;

            try {
                cached = visible_resources.getResource(r.getId());
            } catch (UnknownResourceException ure) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.enc", new Object[]{serviceName, r});
                }
            }

            if (cached == null) {
                fullRefresh();
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.nc", new Object[]{serviceName, r});
                }
            } else {
                /* store state of cached resource */
                cachedState = cached.getState();

                /* track changes between cached and remote resource */
                patch = AbstractResourceChanged.getChanges(cached, r);

                /* update the cached resource - but keep cached state of resource
                 * as we will inform only about properties changed - if state is changed,
                 * normally scheduled fullRefresh will find out */
                r.setState(cachedState);
                visible_resources.add(r);
            }

            if (Resource.State.ASSIGNING.equals(cachedState)) {
                // defer to patch check
            } else if (Resource.State.ASSIGNED.equals(cachedState)) {
                // defer to patch check
            } else if (Resource.State.ERROR.equals(cachedState)) {
                // defer to patch check
            } else if (Resource.State.UNASSIGNING.equals(cachedState)) {
                // defer to patch check
            } else if (Resource.State.UNASSIGNED.equals(cachedState)) {
                // defer to patch check
            } else {
                /* do nothing */
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "scp.s.ch.nyc", new Object[]{serviceName, r});
                }
            }

            /* if remote resource differs from cached one, fire changed event */
            if (!patch.isEmpty()) {
                svcEventForwarder.fireResourceChanged(r.clone(), patch, I18NManager.formatMessage("scp.s.rc.msg", BUNDLE));
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.s.rc", new Object[]{serviceName, r});
                }
            }
        }
    }

    /**
     * helper class - encapsulates service state update to STARTING, SHUTDOWN
     */
    private class ServiceStateChangedAction extends AbstractServiceEventAction<ServiceStateChangedEvent> {

        ServiceStateChangedAction(ServiceStateChangedEvent e) {
            super(e);
        }

        @Override
        protected void executeAction() throws Exception {
            setServiceState(event.getNewState());
        }
    }

    /**
     * helper class - encapsulates service state updated to STOPPED, UNKNOWN, ERROR
     */
    private class ServiceNOT_ACTIVEAction extends AbstractServiceEventAction<ServiceStateChangedEvent> {

        ServiceNOT_ACTIVEAction(ServiceStateChangedEvent e) {
            super(e);
        }

        @Override
        protected void executeAction() throws Exception {
            setServiceState(event.getNewState());
        }
    }

    /**
     * helper class - encapsulates service state updated to RUNNING
     */
    private class ServiceRUNNINGAction extends AbstractServiceEventAction<ServiceStateChangedEvent> {

        ServiceRUNNINGAction(ServiceStateChangedEvent e) {
            super(e);
        }

        @Override
        protected void executeAction() throws Exception {
            /* full refresh have to handle eash state */
            Action a = new RefreshAction();
            a.execute();
        }
    }

    /**
     * helper class - encapsulates component state update.
     *
     * it is not needed to create special action for each component state event
     * (only if we want to have better logging) - as resources can be synchronized
     * only if also service is started.
     *
     */
    private final class ComponentStateChangedAction extends AbstractEventTriggeredAction {

        private final ComponentStateChangedEvent event;

        ComponentStateChangedAction(ComponentStateChangedEvent e) {
            event = e;
        }

        @Override
        protected long getEventSequenceNumber() {
            return event.getSequenceNumber();
        }

        @Override
        protected void executeAction() throws Exception {
            setState(event.getNewState());
        }

        @Override
        public final String toString() {
            return String.format("[%s:ev=%s]", getClass().getSimpleName(), event);
        }
    }

    /**
     * RefreshAction action action performs full synchronization of data between
     * remote service and service caching proxy. Following items are synchronized:
     * - component state
     * - service state
     * - resource information
     *
     * Resource information kept by remote service is always prefered over local cache.
     *
     * Below is description of what events have to be sent - column headers are descriptive.
     *
     * cached   cached state    remote  remote state    remote diff     comment                     forwarded events
     * no       -               yes     ASSIGNING       -               -                           AddResourceEvent
     * no       -               yes     ASSIGNED        -               -                           AddResourceEvent, ResourceAddedEvent
     * no       -               yes     UNASSIGNING     -               -                           AddResourceEvent, ResourceAddedEvent, RemoveResourceEvent
     * no       -               yes     UNASSIGNED      -               treat as remote ERROR state (bug) AddResourceEvent, ResourceErrorEvent
     * no       -               yes     ERROR           -               -                           AddResourceEvent, ResourceErrorEvent
     * yes      ASSIGNING       yes     ASSIGNING       no              -                           -
     * yes      ASSIGNING       yes     ASSIGNING       yes             -                           ResourceChangedEvent
     * yes      ASSIGNING       yes     ASSIGNED        no              -                           ResourceAddedEvent
     * yes      ASSIGNING       yes     ASSIGNED        yes             -                           ResourceChangedEvent, ResourceAddedEvent
     * yes      ASSIGNING       yes     UNASSIGNING     no              -                           ResourceAddedEvent, RemoveResourceEvent
     * yes      ASSIGNING       yes     UNASSIGNING     yes             -                           ResourceChangedEvent, ResourceAddedEvent, RemoveResourceEvent
     * yes      ASSIGNING       yes     UNASSIGNED      no              treat as remote ERROR state (bug) ResourceErrorEvent
     * yes      ASSIGNING       yes     UNASSIGNED      yes             treat as remote ERROR state (bug) ResourceChangedEvent, ResourceErrorEvent
     * yes      ASSIGNING       yes     ERROR           no              -                           ResourceErrorEvent
     * yes      ASSIGNING       yes     ERROR           yes             -                           ResourceChangedEvent, ResourceErrorEvent
     * yes      ASSIGNING       no      -               -               -                           ResourceAddedEvent, RemoveResourceEvent, ResourceRemovedEvent
     * yes      ASSIGNED        yes     ASSIGNING       no              -                           AddResourceEvent
     * yes      ASSIGNED        yes     ASSIGNING       yes             -                           AddResourceEvent, ResourceChangedEvent
     * yes      ASSIGNED        yes     ASSIGNED        no              -                           -
     * yes      ASSIGNED        yes     ASSIGNED        yes             -                           ResourceChangedEvent
     * yes      ASSIGNED        yes     UNASSIGNING     no              -                           RemoveResourceEvent
     * yes      ASSIGNED        yes     UNASSIGNING     yes             -                           ResourceChangedEvent, RemoveResourceEvent
     * yes      ASSIGNED        yes     UNASSIGNED      no              treat as remote ERROR state (bug) ResourceErrorEvent
     * yes      ASSIGNED        yes     UNASSIGNED      yes             treat as remote ERROR state (bug) ResourceChangedEvent, ResourceErrorEvent
     * yes      ASSIGNED        yes     ERROR           no              -                           ResourceErrorEvent
     * yes      ASSIGNED        yes     ERROR           yes             -                           ResourceChangedEvent, ResourceErrorEvent
     * yes      ASSIGNED        no      -               -               -                           RemoveResourceEvent, ResourceRemovedEvent
     * yes      UNASSIGNING     yes     ASSIGNING       no              -                           AddResourceEvent
     * yes      UNASSIGNING     yes     ASSIGNING       yes             -                           AddResourceEvent, ResourceChangedEvent
     * yes      UNASSIGNING     yes     ASSIGNED        no              -                           ResourceAddedEvent
     * yes      UNASSIGNING     yes     ASSIGNED        yes             -                           ResourceChangedEvent, ResourceAddedEvent
     * yes      UNASSIGNING     yes     UNASSIGNING     no              -                           -
     * yes      UNASSIGNING     yes     UNASSIGNING     yes             -                           ResourceChangedEvent
     * yes      UNASSIGNING     yes     UNASSIGNED      no              treat as remote ERROR state (bug) ResourceErrorEvent
     * yes      UNASSIGNING     yes     UNASSIGNED      yes             treat as remote ERROR state (bug) ResourceChangedEvent, ResourceErrorEvent
     * yes      UNASSIGNING     yes     ERROR           no              -                           ResourceErrorEvent
     * yes      UNASSIGNING     yes     ERROR           yes             -                           ResourceChangedEvent, ResourceErrorEvent
     * yes      UNASSIGNING     no      -               -               -                           ResourceRemovedEvent
     * yes      UNASSIGNED      yes     ASSIGNING       no              treat as cached ERROR state AddResourceEvent
     * yes      UNASSIGNED      yes     ASSIGNING       yes             treat as cached ERROR state AddResourceEvent, ResourceChangedEvent
     * yes      UNASSIGNED      yes     ASSIGNED        no              treat as cached ERROR state ResourceResetEvent
     * yes      UNASSIGNED      yes     ASSIGNED        yes             treat as cached ERROR state ResourceChangedEvent, ResourceResetEvent
     * yes      UNASSIGNED      yes     UNASSIGNING     no              treat as cached ERROR state RemoveResourceEvent
     * yes      UNASSIGNED      yes     UNASSIGNING     yes             treat as cached ERROR state RemoveResourceEvent, ResourceChangedEvent
     * yes      UNASSIGNED      yes     UNASSIGNED      no              treat as r/c ERROR state    (bug) -
     * yes      UNASSIGNED      yes     UNASSIGNED      yes             treat as r/c ERROR state    (bug) ResourceChangedEvent
     * yes      UNASSIGNED      yes     ERROR           no              treat as cached ERROR state -
     * yes      UNASSIGNED      yes     ERROR           yes             treat as cached ERROR state ResourceChangedEvent
     * yes      UNASSIGNED      no      -               -               treat as cached ERROR state RemoveResourceEvent, ResourceRemovedEvent
     * yes      ERROR           yes     ASSIGNING       no              -                           AddResourceEvent
     * yes      ERROR           yes     ASSIGNING       yes             -                           AddResourceEvent, ResourceChangedEvent
     * yes      ERROR           yes     ASSIGNED        no              -                           ResourceResetEvent
     * yes      ERROR           yes     ASSIGNED        yes             -                           ResourceChangedEvent, ResourceResetEvent
     * yes      ERROR           yes     UNASSIGNING     no              -                           RemoveResourceEvent
     * yes      ERROR           yes     UNASSIGNING     yes             -                           RemoveResourceEvent, ResourceChangedEvent
     * yes      ERROR           yes     UNASSIGNED      no              treat as remtoe ERROR state (bug) -
     * yes      ERROR           yes     UNASSIGNED      yes             treat as remote ERROR state (bug) ResourceChangedEvent
     * yes      ERROR           yes     ERROR           no              -                           -
     * yes      ERROR           yes     ERROR           yes             -                           ResourceChangedEvent
     * yes      ERROR           no      -               -               -                           RemoveResourceEvent, ResourceRemovedEvent
     */
    private final class RefreshAction extends Action {

        /**
         * performs full synchronization - catching exception on couple of places
         * is needed, as we need to perform update in "consistent way" (not to
         * leave unregistered resources if service is stopped, unknown, error etc.).
         * 
         * before syncing with remote service occurs, all already queued event actiones are
         * cleared - they contain information that will be synced using the full refresh, thus 
         * their execution is superfluous and may be safely removed to save cpu time.
         * 
         * @throws java.lang.Exception
         */
        public void execute() throws Exception {
            actionLock.lock();
            try {
                Iterator<Entry<Action, Future<Void>>> iterator = scheduledActions.entrySet().iterator();
                while (iterator.hasNext()) {
                    Entry<Action, Future<Void>> entry = iterator.next();
                    if (entry.getKey() != this) {
                        entry.getValue().cancel(false);
                        iterator.remove();
                    }
                }
            } finally {
                actionLock.unlock();
            }
            refreshService();
        }

        private void refreshService() {
            log.entering(getClass().getName(), "refreshService");
            Collection<Resource> toAdd = new HashSet<Resource>();
            Collection<Resource> added = new LinkedList<Resource>();

            /* map of old resource - new resource pairs */
            Map<Resource, Resource> toUpdate = new HashMap<Resource, Resource>();

            Collection<Resource> toRemove = new HashSet<Resource>();
            Collection<Resource> removed = new LinkedList<Resource>();

            boolean isServiceActive = false;
            ServiceSnapshot serviceSnapshot = null;
            try {
                serviceSnapshot = service.getSnapshot();
                ServiceState ss = serviceSnapshot.getServiceState();
                // consider the service as active also during shutdown
                isServiceActive = ss.equals(ServiceState.RUNNING) || ss.equals(ServiceState.SHUTDOWN);
            } catch (ServiceNotActiveException ex) {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "scp.ra.sna", new Object[]{serviceName, ex});
                }
                serviceSnapshot = new DefaultServiceSnapshot(0, ComponentState.UNKNOWN, ServiceState.UNKNOWN,
                        Collections.<Resource>emptySet(),
                        Collections.<String, Collection<Need>>emptyMap());
                isServiceActive = false;
            } catch (GrmRemoteException ex) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.grexa", new Object[]{serviceName, ex});
                }
                serviceSnapshot = new DefaultServiceSnapshot(0, ComponentState.UNKNOWN, ServiceState.UNKNOWN,
                        Collections.<Resource>emptySet(),
                        Collections.<String, Collection<Need>>emptyMap());
                isServiceActive = false;
            }

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.fullrefresh.event",
                        new Object[]{getName(), serviceSnapshot.getNextEventSequenceNumber()});
            }
            nextExpectedEventSequenceNumber.set(serviceSnapshot.getNextEventSequenceNumber());
            setServiceState(serviceSnapshot.getServiceState());
            setState(serviceSnapshot.getComponentState());

            Map<ResourceId, Resource> current = new HashMap<ResourceId, Resource>();

            if (isServiceActive) {
                for (final Resource r : serviceSnapshot.getResources()) {
                    current.put(r.getId(), r);
                }
            }

            Map<ResourceId, Resource> cachedMap = new HashMap<ResourceId, Resource>();
            try {
                for (final Resource r : visible_resources.getResources()) {
                    cachedMap.put(r.getId(), r);
                }
            } catch (ResourceStoreException rse) {
                log.log(Level.SEVERE, "scp.ra.vr.get", new Object[]{serviceName, rse});
            }

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.ra.ca", new Object[]{serviceName, cachedMap});
                log.log(Level.FINE, "scp.ra.po", new Object[]{serviceName, current});
            }

            for (Map.Entry<ResourceId, Resource> entry : current.entrySet()) {
                Resource.State cs = entry.getValue().getState();
                if (Resource.State.UNASSIGNED.equals(cs)) {
                    if (log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "scp.ra.ta.bs", new Object[]{serviceName, entry.getValue(), cs});
                    }
                    /* remote service is buggy, so set cached resource to ERROR state to prevent problems */
                    entry.getValue().setState(Resource.State.ERROR);
                }
                if (!cachedMap.containsKey(entry.getKey())) {
                    toAdd.add(entry.getValue());
                } else {
                    toUpdate.put(cachedMap.get(entry.getKey()), entry.getValue());
                }
            }
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.ra.ta", new Object[]{serviceName, toAdd});
            }

            for (Map.Entry<ResourceId, Resource> entry : cachedMap.entrySet()) {
                if (!current.containsKey(entry.getKey())) {
                    toRemove.add(entry.getValue());
                }
            }
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "scp.ra.tr", new Object[]{serviceName, toRemove});
            }

            for (Resource r : toAdd) {
                try {
                    visible_resources.add(r);
                    added.add(r);
                } catch (ResourceStoreException rse) {
                    log.log(Level.SEVERE, "scp.ra.ar.fail", new Object[]{serviceName, r});
                }
            }

            for (Map.Entry<Resource, Resource> entry : toUpdate.entrySet()) {
                try {
                    visible_resources.add(entry.getValue());
                } catch (ResourceStoreException rse) {
                    log.log(Level.SEVERE, "scp.ra.ur.fail", new Object[]{serviceName, entry.getValue()});
                }
            }

            /* remove resources ONLY if service is in a RUNNING or SHUTDOWN
            (for shutdown resource -fr) state */
            for (Resource r : toRemove) {
                try {
                    if (isServiceActive) {
                        visible_resources.remove(r.getId());
                    }
                    removed.add(r);
                } catch (ResourceStoreException rse) {
                    log.log(Level.SEVERE, "scp.ra.rr.fail", new Object[]{serviceName, r});
                } catch (UnknownResourceException use) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "scp.ra.rr.ur", new Object[]{serviceName, r});
                    }
                }
            }
            /* unregister resource IDs - even if service is not in RUNNING state to make ids usable for other services */
            for (Resource r : removed) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.ra.ur", new Object[]{serviceName, r});
                }
            }

            /* no need to check if service is in running state - if it is not, added set is empty */
            for (Resource r : added) {
                switch (r.getState()) {
                    case ASSIGNING:
                        svcEventForwarder.fireAddResource(r.clone(), I18NManager.formatMessage("scp.ra.a.msg", BUNDLE));
                        break;
                    case ASSIGNED:
                        r.setState(Resource.State.ASSIGNING);
                        svcEventForwarder.fireAddResource(r.clone(), I18NManager.formatMessage("scp.ra.a.msg", BUNDLE));
                        r.setState(Resource.State.ASSIGNED);
                        svcEventForwarder.fireResourceAdded(r.clone(), I18NManager.formatMessage("scp.ra.ad.msg", BUNDLE));
                        break;
                    case ERROR:
                        r.setState(Resource.State.ASSIGNING);
                        svcEventForwarder.fireAddResource(r.clone(), I18NManager.formatMessage("scp.ra.a.msg", BUNDLE));
                        r.setState(Resource.State.ERROR);
                        svcEventForwarder.fireResourceError(r.clone(), I18NManager.formatMessage("scp.ra.e.msg", BUNDLE));
                        break;
                    case UNASSIGNING:
                        r.setState(Resource.State.ASSIGNING);
                        svcEventForwarder.fireAddResource(r.clone(), I18NManager.formatMessage("scp.ra.a.msg", BUNDLE));
                        r.setState(Resource.State.ASSIGNED);
                        svcEventForwarder.fireResourceAdded(r.clone(), I18NManager.formatMessage("scp.ra.ad.msg", BUNDLE));
                        r.setState(Resource.State.UNASSIGNING);
                        svcEventForwarder.fireRemoveResource(r.clone(), I18NManager.formatMessage("scp.ra.u.msg", BUNDLE));
                        break;
                    default:
                        /* this can not happen - if resource state was none of the above (is UNASSIGNED, INPROCESS),
                         * then we should set the resource state to ERROR. if we reached this line, it is bug */
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, "scp.ra.a.bs", new Object[]{serviceName, r, r.getState()});
                        }
                        break;
                }
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.ra.ad", new Object[]{serviceName, r});
                }
            }

            /* remove resources from cache only is service is running or shutdown - otherwise we risk losing of
             * resource information 
             * The resource can be in shutdown state if during a stop and free resources operation
             * an event gots lost. In this case we allow also that resources are removed
             */
            if (isServiceActive) {
                for (Resource r : removed) {
                    if (!r.isStatic()) {
                        switch (r.getState()) {
                            case UNASSIGNING:
                                r.setState(Resource.State.UNASSIGNED);
                                svcEventForwarder.fireResourceRemoved(r.clone(), I18NManager.formatMessage("scp.ra.rd.msg", BUNDLE));
                                break;
                            case ASSIGNING:
                                r.setState(Resource.State.ASSIGNED);
                                svcEventForwarder.fireResourceAdded(r.clone(), I18NManager.formatMessage("scp.ra.ad.msg", BUNDLE));
                                r.setState(Resource.State.UNASSIGNING);
                                svcEventForwarder.fireRemoveResource(r.clone(), I18NManager.formatMessage("scp.ra.r.msg", BUNDLE));
                                r.setState(Resource.State.UNASSIGNED);
                                svcEventForwarder.fireResourceRemoved(r.clone(), I18NManager.formatMessage("scp.ra.rd.msg", BUNDLE));
                                break;
                            default:
                                r.setState(Resource.State.UNASSIGNING);
                                svcEventForwarder.fireRemoveResource(r.clone(), I18NManager.formatMessage("scp.ra.r.msg", BUNDLE));
                                r.setState(Resource.State.UNASSIGNED);
                                svcEventForwarder.fireResourceRemoved(r.clone(), I18NManager.formatMessage("scp.ra.rd.msg", BUNDLE));
                                break;
                        }
                    } else {
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, "scp.ra.static_disappear", new Object[]{serviceName, r, r.getProperties()});
                        }
                    }

                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "scp.ra.rd", new Object[]{serviceName, r});
                    }
                }

                // -------------------------------------------------------------
                // Update the resource request information
                // -------------------------------------------------------------
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "scp.fullrefresh.rr.cached", new Object[]{serviceName, cachedResourceRequestMap});
                    log.log(Level.FINE, "scp.fullrefresh.rr.real", new Object[]{serviceName, serviceSnapshot.getResourceRequests()});
                }
                // If the cached resource request map has entries for obsolete SLOs
                // we send a cancel resource request
                for (Map.Entry<String, Collection<Need>> entry : cachedResourceRequestMap.entrySet()) {
                    if (!serviceSnapshot.getResourceRequests().containsKey(entry.getKey())) {
                        // Send cancel resource request
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.fullrefresh.rr.cancel",
                                    new Object[]{serviceName, entry.getKey()});
                        }
                        svcEventForwarder.fireResourceRequest(entry.getKey(), Need.EMPTY_NEED);

                    }
                }
                cachedResourceRequestMap.clear();

                // Renew the still valid resource requests
                for (Map.Entry<String, Collection<Need>> entry : serviceSnapshot.getResourceRequests().entrySet()) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "scp.fullrefresh.rr.renew",
                                new Object[]{serviceName, entry.getKey(), entry.getValue()});
                    }
                    svcEventForwarder.fireResourceRequest(entry.getKey(), entry.getValue());
                    cachedResourceRequestMap.put(entry.getKey(), entry.getValue());
                }

            }

            /* no need to check if service is in running state - if it is not, toUpdate set is empty */
            for (Map.Entry<Resource, Resource> entry : toUpdate.entrySet()) {
                Resource cached = entry.getKey();
                Resource remote = entry.getValue();
                if (Resource.State.ASSIGNING.equals(remote.getState())) {
                    if (Resource.State.ASSIGNING.equals(cached.getState())) {
                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "scp.ra.senf", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.ASSIGNED.equals(cached.getState())) {
                        svcEventForwarder.fireAddResource(remote.clone(), I18NManager.formatMessage("scp.ra.a.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.a", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.ERROR.equals(cached.getState())) {
                        svcEventForwarder.fireAddResource(remote.clone(), I18NManager.formatMessage("scp.ra.a.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.a", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.UNASSIGNING.equals(cached.getState())) {
                        svcEventForwarder.fireAddResource(remote.clone(), I18NManager.formatMessage("scp.ra.a.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.a", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.UNASSIGNED.equals(cached.getState())) {
                        /* if this happens, we have bug in implementation */
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, "scp.ra.cu", new Object[]{serviceName, cached});
                        }
                    } else {
                        /* we should not have reached this section - cached resource that has to be updated is either
                         * null or in some weird state, provide warning message */
                        if (log.isLoggable(Level.WARNING)) {
                            if (cached != null) {
                                log.log(Level.WARNING, "scp.ra.cs", new Object[]{serviceName, cached});
                            } else {
                                log.log(Level.WARNING, "scp.ra.cn", serviceName);
                            }
                        }
                    }
                } else if (Resource.State.ASSIGNED.equals(remote.getState())) {
                    if (Resource.State.ASSIGNING.equals(cached.getState())) {
                        svcEventForwarder.fireResourceAdded(remote.clone(), I18NManager.formatMessage("scp.ra.ad.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.ad", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.ASSIGNED.equals(cached.getState())) {
                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "scp.ra.senf", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.ERROR.equals(cached.getState())) {
                        svcEventForwarder.fireResourceReset(remote.clone(), I18NManager.formatMessage("scp.ra.rs.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.rs", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.UNASSIGNING.equals(cached.getState())) {
                        svcEventForwarder.fireAddResource(remote.clone(), I18NManager.formatMessage("scp.ra.a.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.a", new Object[]{serviceName, remote});
                        }
                        svcEventForwarder.fireResourceAdded(remote.clone(), I18NManager.formatMessage("scp.ra.ad.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.ad", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.UNASSIGNED.equals(cached.getState())) {
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, "scp.ra.cu", new Object[]{serviceName, cached});
                        }
                    } else {
                        /* we should not have reached this section - cached resource that has to be updated is either
                         * null or in some weird state, provide warning message */
                        if (log.isLoggable(Level.WARNING)) {
                            if (cached != null) {
                                log.log(Level.WARNING, "scp.ra.cs", new Object[]{serviceName, cached});
                            } else {
                                log.log(Level.WARNING, "scp.ra.cn", serviceName);
                            }
                        }
                    }
                } else if (Resource.State.ERROR.equals(remote.getState())) {
                    if (Resource.State.ASSIGNING.equals(cached.getState())) {
                        svcEventForwarder.fireResourceError(remote.clone(), I18NManager.formatMessage("scp.ra.e.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.e", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.ASSIGNED.equals(cached.getState())) {
                        svcEventForwarder.fireResourceError(remote.clone(), I18NManager.formatMessage("scp.ra.e.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.e", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.ERROR.equals(cached.getState())) {
                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "scp.ra.senf", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.UNASSIGNING.equals(cached.getState())) {
                        svcEventForwarder.fireResourceError(remote.clone(), I18NManager.formatMessage("scp.ra.e.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.e", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.UNASSIGNED.equals(cached.getState())) {
                        /* if this happens, we have bug in implementation */
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, "scp.ra.cu", new Object[]{serviceName, cached});
                        }
                    } else {
                        /* we should not have reached this section - cached resource that has to be updated is either
                         * null or in some weird state, provide warning message */
                        if (log.isLoggable(Level.WARNING)) {
                            if (cached != null) {
                                log.log(Level.WARNING, "scp.ra.cs", new Object[]{serviceName, cached});
                            } else {
                                log.log(Level.WARNING, "scp.ra.cn", serviceName);
                            }
                        }
                    }
                } else if (Resource.State.UNASSIGNING.equals(remote.getState())) {
                    if (Resource.State.ASSIGNING.equals(cached.getState())) {
                        svcEventForwarder.fireResourceAdded(remote.clone(), I18NManager.formatMessage("scp.ra.ad.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.ad", new Object[]{serviceName, remote});
                        }
                        svcEventForwarder.fireRemoveResource(remote.clone(), I18NManager.formatMessage("scp.ra.r.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.r", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.ASSIGNED.equals(cached.getState())) {
                        svcEventForwarder.fireRemoveResource(remote.clone(), I18NManager.formatMessage("scp.ra.r.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.r", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.ERROR.equals(cached.getState())) {
                        svcEventForwarder.fireRemoveResource(remote.clone(), I18NManager.formatMessage("scp.ra.r.msg", BUNDLE));
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "scp.ra.r", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.UNASSIGNING.equals(cached.getState())) {
                        /* nothing needs to be done */
                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "scp.ra.senf", new Object[]{serviceName, remote});
                        }
                    } else if (Resource.State.UNASSIGNED.equals(cached.getState())) {
                        /* if this happens, we have bug in implementation */
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, "scp.ra.cu", new Object[]{serviceName, cached});
                        }
                    } else {
                        /* we should not have reached this section - cached resource that has to be updated is either
                         * null or in some weird state, provide warning message */
                        if (log.isLoggable(Level.WARNING)) {
                            if (cached != null) {
                                log.log(Level.WARNING, "scp.ra.cs", new Object[]{serviceName, cached});
                            } else {
                                log.log(Level.WARNING, "scp.ra.cn", serviceName);
                            }
                        }
                    }
                } else if (Resource.State.UNASSIGNED.equals(remote.getState())) {
                    /* if this happens, we have bug in implementation */
                    if (log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "scp.ra.ru", new Object[]{serviceName, remote});
                    }
                } else {
                    /* we should not have reached this section - remote resource that has to be updated is either
                     * null or in some weird state, provide warning message */
                    if (log.isLoggable(Level.WARNING)) {
                        if (remote != null) {
                            log.log(Level.WARNING, "scp.ra.rs", new Object[]{serviceName, remote});
                        } else {
                            log.log(Level.WARNING, "scp.ra.rn", serviceName);
                        }
                    }
                }
                Collection<ResourceChanged> patch = AbstractResourceChanged.getChanges(cached, remote);
                if (!patch.isEmpty()) {
                    svcEventForwarder.fireResourceChanged(remote.clone(), patch, I18NManager.formatMessage("scp.ra.rcp.msg", BUNDLE));
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "scp.ra.rcp", new Object[]{serviceName, remote});
                    }
                }
            }

            log.exiting(getClass().getName(), "refreshService");
        }

        @Override
        public final String toString() {
            return String.format("[%s:service=%s]", getClass().getSimpleName(), serviceName);
        }
    }

    /**
     * Starts the action processing if it is not started and if SCP is not
     * in process of "destroying" itself.
     *
     * @return true if processing is started, false otherwise
     */
    private boolean startActionProcessing() {
        log.entering(ServiceCachingProxy.class.getName(), "startActionProcessing");
        boolean isstarted = false;
        actionLock.lock();
        try {
            if (isDestroy) {
                isstarted = false;
            } else {
                if ((actionExecutor == null) || actionExecutor.isTerminated()) {
                    actionExecutor = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {

                        public Thread newThread(Runnable r) {
                            return new Thread(scpThreadGroup, r, actionExecutorName + ":" + serviceName);
                        }
                    });
                }
                isstarted = true;
            }
        } finally {
            actionLock.unlock();
        }
        log.exiting(ServiceCachingProxy.class.getName(), "startActionProcessing", isstarted);
        return isstarted;
    }

    /**
     * Stops the action processing.
     */
    private void stopActionProcessing() {
        log.entering(ServiceCachingProxy.class.getName(), "stopActionProcessing");

        ScheduledExecutorService tmpExe = null;
        actionLock.lock();
        try {
            tmpExe = actionExecutor;
        } finally {
            actionLock.unlock();
        }

        try {
            if (tmpExe != null) {
                tmpExe.shutdownNow();
                long startTime = System.currentTimeMillis();
                while (!tmpExe.isTerminated()) {
                    log.log(Level.FINE, "scp.ap.sd.await", getName());
                    if (tmpExe.awaitTermination(10, TimeUnit.SECONDS)) {
                        break;
                    } else {
                        List<Action> actions = null;
                        actionLock.lock();
                        try {
                            actions = new ArrayList<Action>(scheduledActions.keySet());
                        } finally {
                            actionLock.unlock();
                        }
                        Level level = Level.FINE;
                        if (System.currentTimeMillis() - startTime > 60000) {
                            level = Level.WARNING;
                        }
                        if (log.isLoggable(level)) {
                            log.log(level, "scp.ap.sd.stillactive",
                                    new Object[]{getName(), actions.toString()});
                        }
                    }
                }
            }
        } catch (InterruptedException ex) {
            log.log(Level.SEVERE, "scp.ap.sd.interrupted", ex);
        } finally {
            actionLock.lock();
            try {
                actionExecutor = null;
            } finally {
                actionLock.unlock();
            }
        }
        log.exiting(ServiceCachingProxy.class.getName(), "stopActionProcessing");
    }

    @Override
    public String toString() {
        return String.format("SCP[ %s ]", serviceName);
    }

    /**
     * Call the getSnapshot method if the remote service
     * @return the snapshot
     * @throws com.sun.grid.grm.GrmRemoteException
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     */
    public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
        return this.service.getSnapshot();
    }
}
