/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.GrmComponentDelegate;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.util.EventForwarder;
import com.sun.grid.grm.util.filter.Filter;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Delegate base class for all services which needs a private classloader.
 * @param <K> Service Object class
 * @deprecated Please use {@link DefaultServiceContainer} and {@link AbstractServiceAdapter}
 */
public abstract class ServiceDelegate<K extends ComponentConfig> extends GrmComponentDelegate<Service, K>
   implements Service {
    
    private static final String BUNDLE = "com.sun.grid.grm.service.impl.service";
    
    private final static Logger log = Logger.getLogger(ServiceDelegate.class.getName(), BUNDLE);
    
    /**
     * The event forwarder forwards any service event from the delegate to the registered
     * ServiceEventListeners
     * We can not add/remove the eventListener directly to the delegate
     * because the registered listener must survive a reload of the delegate
     */
    private final EventForwarder<ServiceEventListener> eventForwarder = EventForwarder.<ServiceEventListener>newSynchronInstance(ServiceEventListener.class);
    
    /**
     * Creates a new instance of ServiceDelegate.
     *
     * @param env        the excution env
     * @param name       name of the component
     */
    public ServiceDelegate(ExecutionEnv env, String name) {
        super(env, name);
    }
    
    /**
     * Start the service.
     *
     * @throws com.sun.grid.grm.GrmException if the startup of the service failed
     */
    public final void startService() throws GrmException {
        getDelegate().startService();
    }

    /**
     * Stop the service
     *
     * @param isFreeResources  should the resource be freed
     * @throws com.sun.grid.grm.GrmException if the shutdown of the service failed
     */
    public final void stopService(boolean isFreeResources) throws GrmException {
        getDelegate().stopService(isFreeResources);
    }

    /**
     * Get all resources which matches a filter
     * @param  filter the filter
     * @return list of resources
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws com.sun.grid.grm.GrmRemoteException
     */
    public List<Resource> getResources(Filter<Resource> filter) throws ServiceNotActiveException, GrmRemoteException {
        return getDelegate().getResources(filter);
    }
    

    /**
     * Remove a resource.
     *
     * @param resourceId  the id of the resource which should be removed
     * @param descr the descriptor of the resource remove operation
     *
     * @throws com.sun.grid.grm.resource.UnknownResourceException
     * @throws com.sun.grid.grm.resource.InvalidResourceException
     * @throws com.sun.grid.grm.service.ServiceNotActiveException 
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public final void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourceException {
        getDelegate().removeResource(resourceId, descr);
    }
    
    /**
     * Resets a resource.
     *
     * @param resource   id of the resource
     *
     * @throws com.sun.grid.grm.resource.UnknownResourceException 
     * @throws com.sun.grid.grm.service.ServiceNotActiveException 
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public final void resetResource(ResourceId resource) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
        getDelegate().resetResource(resource);
    }
    
    /**
     * Modifies a resource.
     *
     * @param resource   id of the resource
     * @param operations to be done on resource properties and attributes
     * @throws com.sun.grid.grm.resource.UnknownResourceException
     * @throws com.sun.grid.grm.service.ServiceNotActiveException 
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public final void modifyResource(ResourceId resource, ResourceChangeOperation operations) 
            throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
        getDelegate().modifyResource(resource, operations);
    }
    
    /**
     * Add a resource to the service.
     *
     * @param resource  the resource
     * @throws com.sun.grid.grm.resource.InvalidResourceException 
     * @throws com.sun.grid.grm.service.ServiceNotActiveException 
     */
    public final void addResource(Resource resource) throws InvalidResourceException, ServiceNotActiveException {
        try {
            getDelegate().addResource(resource);
        } catch (GrmException e) {
            ServiceNotActiveException ex = new ServiceNotActiveException();
            ex.initCause(e);
            throw ex;
        }
    }

    /**
     * Get the state of the service.
     *
     * @return the state of the service
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public final ServiceState getServiceState() throws GrmRemoteException {
        return getDelegate().getServiceState();
    }
    
    /**
     * Gets the slo state of the service.
     * @return the slo state
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public final List<SLOState> getSLOStates() throws GrmRemoteException {
        return getDelegate().getSLOStates();
    }
    
    /**
     * Get the SLOStates of this service. Include only the resource information
     * of those resources matching a <code>resourceFitler</code>
     * @param resourceFilter  the resource filter
     * @return the SLOState
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public final List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
        return getDelegate().getSLOStates(resourceFilter);
    }

    /**
     * Register a service listener.
     *
     * @param eventListener  the listener
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public final void addServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException {
        eventForwarder.addListener(eventListener);
    }

    /**
     * Deregister a service listener.
     *
     * @param eventListener  the listener
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public final void removeServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException {
        eventForwarder.removeListener(eventListener);
    }

    /**
     * This method reregisters the event forwarder in the new loaded delegate
     * @param newDelegate the new delegate
     * @param oldDelegate the old delegate
     */
    @Override
    protected final void delegateLoaded(Service newDelegate, Service oldDelegate) {
        /**
         * Do not forget to call the implementation of the parent classes
         */
        super.delegateLoaded(newDelegate, oldDelegate);
        
        /**
         * If old delegate is not null the event forwarder is already register
         * => Remove the event forwarder from service event listeners of the delegate
         */
        if (oldDelegate != null) {
            try {
                oldDelegate.removeServiceEventListener(eventForwarder.getProxy());
            } catch (GrmRemoteException ex) {
                // The GrmRemoteException should not happen because the delegate is a local object
                LogRecord lr = new LogRecord(Level.SEVERE, "serviceDelegate.ex.unregisterListener");
                lr.setParameters(new Object[]{getName(), ex.getLocalizedMessage()});
                lr.setThrown(ex);
                lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                log.log(lr);
            }
        }
        
        /**
         * Register the event forwarder at the new delegate
         */
        try {
            newDelegate.addServiceEventListener(eventForwarder.getProxy());
        } catch (GrmRemoteException ex) {
            // The GrmRemoteException should not happen because the delegate is a local object
            LogRecord lr = new LogRecord(Level.SEVERE, "serviceDelegate.ex.registerListener");
            lr.setParameters( new Object [] { getName(), ex.getLocalizedMessage() });
            lr.setThrown(ex);
            lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
            log.log(lr);
        }
    }
    
    
    
    /**
     * Get all resources of this service.
     *
     * @throws com.sun.grid.grm.service.ServiceNotActiveException 
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     * @return all resources of this service
     */
    public final List<Resource> getResources() throws ServiceNotActiveException, GrmRemoteException {
        return getDelegate().getResources();
    }

    /**
     * Get a resource by it's id
     * @param resourceId the resource id
     * @throws com.sun.grid.grm.resource.UnknownResourceException 
     * @throws com.sun.grid.grm.service.ServiceNotActiveException 
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     * @return the resource
     */
    public final Resource getResource(ResourceId resourceId) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
        return getDelegate().getResource(resourceId);
    }
    
}
