/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.resource.impl.*;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceStoreException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.util.I18NManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class holds the repository of services for the service manager and other
 * managers used by ResourceProvider.
 * 
 * The SimpleServiceStore is the default implementation of ServiceStore and does
 * not implement any persistence - all information is kept in-memory only because
 * persistent information about the serices is maintained by configuration 
 * service.
 * 
 * @param <T> implementing class type of stored services
 */
public class SimpleServiceStore<T extends Service> implements ServiceStore<T> {

    private Map<String, T> storage;
    private static final String BUNDLE = "com.sun.grid.grm.service.impl.service";
    private static final Logger log = Logger.getLogger(SimpleServiceStore.class.getName(), BUNDLE);
    private final ReentrantLock serviceLock = new ReentrantLock();

    /**
     * Creates a new instance of SimpleServiceStore
     */
    public SimpleServiceStore() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(SimpleServiceStore.class.getName(), "<init>");
        }
        this.storage = new ConcurrentHashMap<String, T>();
        if (log.isLoggable(Level.FINER)) {
            log.exiting(SimpleServiceStore.class.getName(), "<init>");
        }
    }

    /**
     * Adds a service. ServiceName is used as a key.
     * 
     * @param service service to be added
     * @return an old service with the same name as added object or null if such
     * service was not stored
     * @throws com.sun.grid.grm.service.ServiceStoreException if operation fails
     */
    public T addService(T service) throws ServiceStoreException {
        String sname;
        try {
            sname = service.getName();
        } catch (Exception ex) {
            ServiceStoreException rse = new ServiceStoreException(ex.getLocalizedMessage());
            rse.initCause(ex);
            throw rse;
        }
        if (log.isLoggable(Level.FINER)) {
            log.entering(SimpleServiceStore.class.getName(), "addService", sname);
        }
        serviceLock.lock();
        T ret = null;
        try {
            ret = storage.put(sname, service);
        } catch (Exception e) {
            ServiceStoreException rse = new ServiceStoreException(e.getLocalizedMessage());
            rse.initCause(e);
            throw rse;
        } finally {
            serviceLock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(SimpleServiceStore.class.getName(), "addService");
        }
        return ret;
    }

    /**
     * Removes a service.
     * 
     * @param serviceName name of a service to be removed
     * @return the removed service
     * @throws com.sun.grid.grm.service.UnknownServiceException if service with 
     * such name was not found
     * @throws com.sun.grid.grm.service.ServiceStoreException  if operation fails
     */
    public T removeService(String serviceName) throws UnknownServiceException, ServiceStoreException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(SimpleServiceStore.class.getName(), "removeService", serviceName);
        }
        serviceLock.lock();
        T retValue;
        try {
            retValue = storage.remove(serviceName);
        } catch (Exception e) {
            ServiceStoreException rse = new ServiceStoreException(e.getLocalizedMessage());
            rse.initCause(e);
            throw rse;
        } finally {
            serviceLock.unlock();
        }

        if (retValue == null) {
            UnknownServiceException use = new UnknownServiceException(I18NManager.formatMessage(
                    "sss.error.unknownservice", BUNDLE, serviceName));

            if (log.isLoggable(Level.FINER)) {
                log.throwing(SimpleResourceStore.class.getName(), "removeService", use);
            }

            throw use;
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(SimpleServiceStore.class.getName(), "removeService", retValue);
        }
        return retValue;
    }

    /**
     * Gets a set of names of all stored services.
     * 
     * @return the set of service names
     */
    public Set<String> getServiceNames() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(SimpleServiceStore.class.getName(), "getServiceIds");
        }
        serviceLock.lock();
        Set<String> names;
        try {
            names = storage.keySet();
        } finally {
            serviceLock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(SimpleServiceStore.class.getName(), "getServiceIds", names);
        }
        return names;
    }

    /**
     * Gets a service with the specified name.
     * 
     * @param serviceName the name of the service to look for
     * @return the service with specified name
     * @throws com.sun.grid.grm.service.UnknownServiceException if service with 
     * such name was not found
     * @throws com.sun.grid.grm.service.ServiceStoreException  if operation fails
     */
    public T getService(String serviceName) throws UnknownServiceException, ServiceStoreException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(SimpleServiceStore.class.getName(), "getService");
        }
        serviceLock.lock();
        T match;
        try {
            match = storage.get(serviceName);
        } catch (Exception e) {
            ServiceStoreException rse = new ServiceStoreException(e.getLocalizedMessage());
            rse.initCause(e);
            throw rse;
        } finally {
            serviceLock.unlock();
        }

        if (match == null) {
            UnknownServiceException use = new UnknownServiceException(I18NManager.formatMessage(
                    "sss.error.unknownservice", BUNDLE, serviceName));

            if (log.isLoggable(Level.FINER)) {
                log.throwing(SimpleResourceStore.class.getName(), "getService", use);
            }

            throw use;
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(SimpleServiceStore.class.getName(), "getService", match);
        }
        return match;
    }

    /**
     * Gets all stored services.
     * @return list of service.
     */
    public List<T> getServices() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(SimpleServiceStore.class.getName(), "getServices");
        }
        serviceLock.lock();
        List<T> matches;
        try {
            matches = new ArrayList<T>(storage.values());
        } finally {
            serviceLock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(SimpleServiceStore.class.getName(), "getServices", matches);
        }
        return matches;
    }

    /**
     * Removes all services.
     * 
     */
    public void clear() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(SimpleServiceStore.class.getName(), "clear");
        }
        serviceLock.lock();
        try {
            storage.clear();
        } finally {
            serviceLock.unlock();
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(SimpleServiceStore.class.getName(), "clear");
        }
    }
}
