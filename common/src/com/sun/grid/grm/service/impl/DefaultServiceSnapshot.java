/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *  Default implementation of a ServiceSnapshot
 */
public class DefaultServiceSnapshot implements ServiceSnapshot, Serializable {

    private final static long serialVersionUID = -200902030L;
    
    private final long eventId;
    private final List<Resource> resources;
    private final ComponentState componentState;
    private final ServiceState serviceState;
    private final Map<String, Collection<Need>> resourceRequests;
    
    /**
     * Create a new instance of DefaultServiceSnapshot.
     * @param eventId  the sequence number of the next service event
     * @param compState the current component state
     * @param serviceState the current service state
     * @param res list of resources of the service
     * @param resourceRequests map with the SLO name as key and the current needs list of the SLO as value
     */
    public DefaultServiceSnapshot(long eventId, ComponentState compState, ServiceState serviceState, 
            Set<Resource> res, Map<String, Collection<Need>> resourceRequests) {
        this.eventId = eventId;
        this.componentState = compState;
        this.serviceState = serviceState;
        List<Resource> tmpRes = new ArrayList<Resource>(res);
        this.resources = Collections.<Resource>unmodifiableList(tmpRes);

        Map<String,Collection<Need>> tmpRR = new HashMap<String,Collection<Need>>(resourceRequests.size());
        for(Map.Entry<String,Collection<Need>> entry: resourceRequests.entrySet()) {
            tmpRR.put(entry.getKey(), new ArrayList<Need>(entry.getValue()));
        }
        
        this.resourceRequests = Collections.unmodifiableMap(tmpRR);
    }
    
    /**
     * The the sequence number of the next event.
     * @return the sequence number of the next event
     */
    public long getNextEventSequenceNumber() {
        return eventId;
    }

    /**
     * Get the list of resources of the service. 
     * @return unmodifiable list of resources
     */
    public List<Resource> getResources() {
        return resources;
    }

    /**
     * Get the service state.
     * @return the service state
     */
    public ServiceState getServiceState() {
        return serviceState;
    }

    /**
     * Get the component state.
     * @return the component state
     */
    public ComponentState getComponentState() {
        return componentState;
    }

    /**
     * Get the resource request map.
     * 
     * @return unmodifiable map with the SLO name as key and the need list of the
     *         SLO as value
     */
    public Map<String, Collection<Need>> getResourceRequests() {
        return resourceRequests;
    }
    
    @Override
    public String toString() {
        return String.format("[DefaultServiceSnapshot:cs=%s,ss=%s,ev=%d]",
                             componentState, serviceState, eventId);
    }
    
}
