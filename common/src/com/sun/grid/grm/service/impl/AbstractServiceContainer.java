/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl;

import com.sun.grid.grm.ComponentAdapterFactory;
import com.sun.grid.grm.ComponentNotActiveException;
import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventSupport;
import com.sun.grid.grm.impl.AbstractComponentContainer;
import com.sun.grid.grm.impl.ExecutorServiceFactory;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.ServiceAdapter;
import com.sun.grid.grm.service.ServiceContainer;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

/**
 * Default implementation of a <code>ServiceContainer</code>.
 * 
 * <p>This class delegates all method calls defined in <code>Service</code> to
 *   the <code>ServiceAdapter</code>.</p>
 * 
 * @param <A>  the type of the service adapter
 * @param <E>  the type of the <code>ExecutorService</code>
 * @param <C>  the  type of the configuration
 */
public class AbstractServiceContainer<A extends ServiceAdapter<E,C>,E extends ExecutorService,C>
        extends AbstractComponentContainer<A,E,C> implements ServiceContainer<A,E,C> {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.service";
    
    private final Logger log = Logger.getLogger(AbstractServiceContainer.class.getName(), BUNDLE);
    
    /**
     * Create a new instance of of <code>AbstractServiceContainer</code>
     * @param env  the execution env of the component
     * @param name the name of the component
     * @param executorServiceFactory  factory for creating the <code>ExecutorService</code> of the component
     * @param componentAdapterFactory factory for creating the service adapter
     */
    public AbstractServiceContainer(ExecutionEnv env, String name,
                                   ExecutorServiceFactory<? extends AbstractServiceContainer<A,E,C>,E,C> executorServiceFactory,
                                   ComponentAdapterFactory<? extends AbstractServiceContainer<A,E,C>,A,E,C> componentAdapterFactory) {
        super(env, name, executorServiceFactory, componentAdapterFactory);
    }

    /**
     * Service always need a ServiceEventSupport element
     * @param componentName the name of the component
     * @param hostname      the host where the component is living
     * @return the ServiceEventSupport
     */
    @Override
    protected final ComponentEventSupport createEventSupport(String componentName, Hostname hostname) {
        return ServiceEventSupport.newInstance(componentName, hostname);
    }
    
    /**
     * Get the ServiceEventSupport for this service
     * @return the ServiceEventSupport
     */
    public ServiceEventSupport getServiceEventSupport() {
        return (ServiceEventSupport)getComponentEventSupport();
    }

    /**
     * Starts up the service, if it is not running yet
     *
     * @throws com.sun.grid.grm.GrmException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void startService() throws GrmException, GrmRemoteException {
        log.entering(AbstractServiceContainer.class.getName(), "startService");
        
        lock();
        try {
            // issue 651: Check that the component is in STARTED state, otherwise starting
            //            the service is not possible
            if (ComponentState.STARTED.equals(getState())) {
                getAdapter().startService(getConfig());
            } else {
                throw new ComponentNotActiveException(getName());
            }
        } finally {
            unlock();
        }
        
        log.exiting(AbstractServiceContainer.class.getName(), "startService");
    }

    /**
     * Stops the service, if it is running. In case that
     * the service is stopped, one can specify to free the
     * resources with it. If the service is not running,
     * the isFreeResources has not effect.
     *
     * @param freeResources if true, frees all none static resources
     * @throws com.sun.grid.grm.GrmException if the shutdown of the service is not possible
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void stopService(boolean freeResources) throws GrmException, GrmRemoteException {
        log.entering(AbstractServiceContainer.class.getName(), "stopService", freeResources);
        
        lock();
        try {
            // issue 651: Check that the component is in STARTED state, otherwise stopping
            //            the service is not possible
            if (ComponentState.STARTED.equals(getState())) {
                getAdapter().stopService(freeResources);
            } else {
                throw new ComponentNotActiveException(getName());
            }
        } finally {
            unlock();
        }
        
        log.exiting(AbstractServiceContainer.class.getName(), "stopService");
    }

    /**
     * returns the current service state
     *
     * @return the state the service is in
     */
    public ServiceState getServiceState() {
        try {
            return getAdapter().getServiceState();
        } catch (GrmRemoteException ex) {
            return ServiceState.UNKNOWN;
        } catch (ComponentNotActiveException ex) {
            return ServiceState.UNKNOWN;
        }
    }

    /**
     * Get the states of the SLOs of this service.
     * 
     * @return list of slo states or an empty list of the service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public List<SLOState> getSLOStates() throws GrmRemoteException {
        try {
            return getAdapter().getSLOStates();
        } catch (ComponentNotActiveException ex) {
            return Collections.<SLOState>emptyList();
        }
    }
    
    /**
     * Get the SLOStates of this service. Include only the resource information
     * of those resources matching a <code>resourceFilter</code>
     * @param resourceFilter  the resource filter
     * @return the list of  SLOStates, or an empty list if the service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
        try {
            return getAdapter().getSLOStates(resourceFilter);
        } catch (ComponentNotActiveException ex) {
            return Collections.<SLOState>emptyList();
        }
    }

    /**
     * Register an event listener to receive status reports and task finish
     * notifications.
     *
     * @param eventListener the event listener
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void addServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException {
        getServiceEventSupport().addServiceEventListener(eventListener);
    }

    /**
     * Removes an event listener.
     *
     * @param eventListener EventListener to remove
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void removeServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException {
        getServiceEventSupport().removeServiceEventListener(eventListener);
    }

    /**
     * Get the service adapter of this service container
     * @return the service adapter
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    protected final A getServiceAdapter() throws ServiceNotActiveException {
        try {
            return getAdapter();
        } catch (ComponentNotActiveException ex) {
            throw new ServiceNotActiveException();
        }
    }
    
    /**
     * Get the Resources matching a filter.
     * 
     * @param filter the Resource filter
     * @return the matching Resources
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem
     */
    public List<Resource> getResources(Filter<Resource> filter) throws ServiceNotActiveException, GrmRemoteException {
        return getServiceAdapter().getResources(filter);
    }

    /**
     * Get the all Resources of the Service.
     * 
     * @return list with all Resources
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem
     */
    public List<Resource> getResources() throws ServiceNotActiveException, GrmRemoteException {
        return getServiceAdapter().getResources();
    }

    /**
     * Get a Resource.
     * 
     * @param resourceId the id of the Resource
     * @return the Resource
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the Resource is not known
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the Service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any communication problem
     */
    public Resource getResource(ResourceId resourceId) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
        return getServiceAdapter().getResource(resourceId);
    }

    /**
     * Remove a Resource
     * @param resourceId  the id of the resource which should be removed
     * @param descr      the descriptor of the remove resource operation
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not found
     * @throws com.sun.grid.grm.resource.InvalidResourceException if resource is not valid for remove
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if service is ot active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, InvalidResourceException, ServiceNotActiveException, GrmRemoteException {
        getServiceAdapter().removeResource(resourceId, descr);
    }

    /**
     * Reset a resource state. The reset signals to the service that all steps
     * were taken to bring the resource to operational status and
     * that the service can start to use the resource again.
     *
     * Upon a successful reset, the notification is sent to registered listeners.
     *
     * @param resource The resource to reset
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not found
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void resetResource(ResourceId resource) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
        getServiceAdapter().resetResource(resource);
    }

    /**
     * Adds a resource to the service. This call will not block and return right away.
     * If a Resource Event Listener is registered the listener will be informed once
     * the resoure was added or if an error occured.
     *
     * @param resource The resource object that will be added to the service
     * @throws com.sun.grid.grm.resource.InvalidResourceException
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void addResource(Resource resource) throws InvalidResourceException, ServiceNotActiveException, GrmRemoteException {
        getServiceAdapter().addResource(resource);
    }

    /**
     * Adds a resource to the service. This call will not block and return right away.
     * If a Resource Event Listener is registered the listener will be informed once
     * the resource was added or if an error occured.
     * If the resource was added in response to an SLO request the SLO name is provided
     * @param resource The resource object that will be added to the service
     * @param sloName if the resource is added because of a SLO request it contains the SLO name
     * @throws com.sun.grid.grm.resource.InvalidResourceException
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void addResource(Resource resource,String sloName) throws InvalidResourceException, ServiceNotActiveException, GrmRemoteException {
        getServiceAdapter().addResource(resource,sloName);
    }

    /**
     * Add a new resource to the service
     * @param type        the resource type
     * @param properties the resource properties
     * @return
     * @throws com.sun.grid.grm.service.ServiceNotActiveException  if the service is not active
     * @throws com.sun.grid.grm.resource.InvalidResourceException  if the service did not accept the resource
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the resource properties are not valid
     * @throws com.sun.grid.grm.GrmRemoteException  communication error
     * @throws com.sun.grid.grm.GrmException  if the resource could not be created (reason unknown)
     */
    public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
        return getServiceAdapter().addNewResource(type, properties);
    }

    
    
    /**
     * Triggers an operation that modifies a resource (its properties). It depends
     * on a service whether the operation actively change the underlying resource
     * (if the service has tools to do that) or just passively updates the resource properties.
     *
     * Active change of the resource can be for example changing the size of the virtual memory
     * of the host resource (the service can ask the resource to change the virtual
     * memory size to the new value and if successful it can update the resource properties).
     *
     * Passive change of the resource can be for example changing the size of the physical memory
     * of the host resource (the amount of physical memory has to changed by manual
     * intervention of the administrator).
     *
     * @param resource a resource to be modified
     * @param operations that will modify resource properties and attributes
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not found
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException of the properties are not valid
     */
    public void modifyResource(ResourceId resource, ResourceChangeOperation operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
        getServiceAdapter().modifyResource(resource, operations);
    }

    /**
     * Triggers an operation that modifies a resource (its properties). It depends
     * on a service whether the operation actively change the underlying resource
     * (if the service has tools to do that) or just passively updates the resource properties.
     *
     * Active change of the resource can be for example changing the size of the virtual memory
     * of the host resource (the service can ask the resource to change the virtual
     * memory size to the new value and if successful it can update the resource properties).
     *
     * Passive change of the resource can be for example changing the size of the physical memory
     * of the host resource (the amount of physical memory has to changed by manual
     * intervention of the administrator).
     *
     * @param resource a resource to be modified
     * @param operations that will modify resource properties and attributes
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not found
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if service is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException of the properties are not valid
     */
    public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
        getServiceAdapter().modifyResource(resource, operations);
    }


    public final ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
        lock();
        try {
            return getServiceAdapter().getSnapshot();
        } finally {
            unlock();
        }
    }

    
}
