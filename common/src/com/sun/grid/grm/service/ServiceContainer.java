/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service;

import com.sun.grid.grm.ComponentContainer;
import java.util.concurrent.ExecutorService;

/**
 * This class provides the basic interface for a <code>ServiceContainer</code>.
 * 
 * <p>A <code>ServiceContainer</code> is a <code>ComponentContainer</code> which provides
 *    also all method necessary for a <code>Service</code>.
 * 
 * @param <A>  The type of the <code>ServiceAdapter</code>
 * @param <E>  The type of the <code>ExecutorService</code>
 * @param <C>  The type of configuration
 */
public interface ServiceContainer<A extends ServiceAdapter<E, C>, E extends ExecutorService, C> 
        extends ComponentContainer<A,E,C>, Service {

}
