/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.resource.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * A ServiceSnapshot describes the state of a Service at the time where
 * the snapshot has been made.
 * 
 * <p>The snapshot includes the following information:</p>
 * <ul>
 *    <li>The sequence number of the next event</li>
 *    <li>The component state of the service</li>
 *    <li>The service state of the service</li>
 *    <li>The list of resources</li>
 *    <li>The pending resource requests of the service</li>
 * </ul>
 */
public interface ServiceSnapshot {

    /**
     * Get the sequence number of the next event
     * @return the sequence number of the next event
     */
    public long getNextEventSequenceNumber();
    
    /**
     * Get the list of resources
     * @return the list of resources
     */
    public List<Resource> getResources();
    
    /**
     * Get the service state
     * @return the service state
     */
    public ServiceState getServiceState(); 
    
    /**
     * Get the component state
     * @return the component state
     */
    public ComponentState getComponentState();
    
    /**
     * Get the resource requests
     * @return the resource requests
     */
    public Map<String,Collection<Need>> getResourceRequests();
}
