/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.event.ComponentEventAdapter;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentEventSupport;
import com.sun.grid.grm.event.ComponentStateChangedEvent;
import com.sun.grid.grm.impl.AbstractComponent;
import com.sun.grid.grm.impl.AbstractComponentContainer;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.util.GrmClassLoaderFactory;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 *  The <code>GrmComponentDelegate</code> support delegation of a grm component
 *  to a implementation class which run under a different class loader.
 *
 *  All components which requires additional classpath elements can extend this
 *  class. They have to implement the methods <code>getClasspath</code> and
 *  <code>getDelegateClassname</code>.
 *
 *  <p> The loading of the delegate is thread safe.</p>
 * 
 *  @param <T>  type of the component
 *  @param <C>  type of the component configuration
 *  @deprecated Please use {@link  AbstractComponent} or {@link  AbstractComponentContainer}
 */
public abstract class GrmComponentDelegate<T extends GrmComponent, C extends ComponentConfig> implements GrmComponent {

    static final String BUNDLE = "com.sun.grid.grm.messages";
    private static final Logger log = Logger.getLogger(GrmComponentDelegate.class.getName(), BUNDLE);
    /**
     *  The componentListener is the only listener that is registered in the delegate. It has the job
     *  to forward all events to an eventForwarder
     */
    private final MyComponentListener componentListener = new MyComponentListener();
    private final ComponentEventSupport cmpEventSupport;
    /** Guards the delegate and newState */
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    /** the execution env of the Hedeby system */
    private final ExecutionEnv env;
    /** the name of the component */
    private final String name;
    /** the name of the component */
    private final Hostname hostname;
    /** the component delegate */
    private T delegate;
    /** The newState of the component. The GrmComponentDelegate class stores the newState
    of component in this member variable. The MyComponentListener is responsible
    for keeping the newState of the delegate in sync with this newState variable.
    If the delegate could not be created for some reason this newState is set to
    STOPPED. */
    private ComponentState state = ComponentState.UNKNOWN;
    /** the classpath used for constructing the current delegate */
    private URL[] activeClasspath;

    /**
     * Creates a new instance of GrmComponentDelegate.
     *
     * @param env          the execution environment for the component
     * @param name         name of the component
     */
    public GrmComponentDelegate(ExecutionEnv env, String name) {
        this.env = env;
        this.name = name;
        this.hostname = Hostname.getLocalHost();
        this.cmpEventSupport = ComponentEventSupport.newInstance(name, hostname);
    }

    /**
     * Get the delegate of the component.
     *
     * This method calls at the first call <code>createDelegate</code>
     * to create the component delegate
     *
     * @return the component delegate
     */
    public final T getDelegate() {
        log.entering(getClass().getSimpleName(), "getDelegate");
        T ret = null;
        lock.readLock().lock();
        try {
            if (delegate == null) {
                throw new IllegalStateException(I18NManager.formatMessage("componentDelegate.error.notLoaded", BUNDLE));
            }
            ret = delegate;
        } finally {
            lock.readLock().unlock();
        }
        log.exiting(getClass().getSimpleName(), "getDelegate", ret);
        return ret;
    }

    /**
     * Get the classpath for the component delegate.
     *
     * @throws com.sun.grid.grm.GrmException if the classpath can not be constructed, the
     *          component will go into STOPPED newState
     * @return an array with the URLs of all classpath elements
     */
    protected abstract URL[] getClasspath() throws GrmException;

    /**
     * Get the classname of the component delegate
     *
     * @throws com.sun.grid.grm.GrmException if the classname can not be constructed,
     *              the component will go into STOPPED newState
     * @return the classname of the component delegate
     */
    protected abstract String getDelegateClassname() throws GrmException;

    /**
     * Get concrete class of the delegate.
     *
     * @return the delegate class
     */
    public Class<?> getDelegateClass() {
        return getDelegate().getClass();
    }

    /**
     * Has the implementation (classpath or classname) of the delegate changed
     * @param classname  the name of the delegate
     * @param classpath  the classpath of the delegate
     * @return <code>true</code> if the implementation has changed
     */
    private boolean hasImplementationChanged(String classname, URL[] classpath) {

        boolean ret = false;
        lock.readLock().lock();
        try {
            if (delegate == null) {
                return true;
            }
            if (!delegate.getClass().getName().equals(classname)) {
                return true;
            }
            if (activeClasspath == null) {
                ret = true;
            } else if (activeClasspath.length != classpath.length) {
                ret = true;
            } else {
                for (int i = 0; i < activeClasspath.length; i++) {
                    if (!activeClasspath[i].equals(classpath[i])) {
                        ret = true;
                        break;
                    }
                }
            }
        } finally {
            lock.readLock().unlock();
        }
        return ret;
    }

    /**
     * This method creates the component delegate.
     *
     * <p>First out of the classpath a new <code>ClassLoader</code> is created. This
     * <code>ClassLoader</code> is used find the class of the component delegate.</p>
     *
     * It is expected that the component delegate class has a public constructor in
     * with the signature <code>(ExecutionEnv env, String name, String configName)</code>.
     * This constructor is used to create an instance of the component delegate.
     *
     * @throws com.sun.grid.grm.GrmException if the component delegate can not be constructed
     *
     * @param classpath the classpath element
     * @param classname name of the class
     * @see #getDelegateClassname
     * @see #getClasspath
     */
    @SuppressWarnings(value = "unchecked")
    private T loadDelegate(String classname, URL[] classpath) throws GrmException {

        T oldDelegate = null;
        T newDelegate = null;
        lock.writeLock().lock();
        try {
            ClassLoader delegateClassLoader = GrmClassLoaderFactory.getInstance(classpath, getClass().getClassLoader());

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "componentDelegate.load", new Object[]{classname, GrmClassLoaderFactory.toString(delegateClassLoader)});
            }

            Class cls;
            try {
                cls = Class.forName(classname, true, delegateClassLoader);
            } catch (ClassNotFoundException ex) {
                throw new GrmException("componentDelegate.error.classNotFound", ex, BUNDLE, classname);
            }

            Constructor cons;
            try {
                cons = cls.getConstructor(new Class[]{ExecutionEnv.class, String.class});
            } catch (NoSuchMethodException ex) {
                throw new GrmException("componentDelegate.error.noConstructor", ex, BUNDLE, cls.getName());
            }
            try {
                newDelegate = (T) cons.newInstance(new Object[]{getExecutionEnv(), getName()});
                this.activeClasspath = classpath;
                oldDelegate = this.delegate;
                this.delegate = newDelegate;
                delegateLoaded(newDelegate, oldDelegate);
            } catch (IllegalArgumentException ex) {
                throw new GrmException("componentDelegate.error.noConstructor", ex, BUNDLE, cls.getName());
            } catch (IllegalAccessException ex) {
                throw new GrmException("componentDelegate.error.noConstructor", ex, BUNDLE, cls.getName());
            } catch (InstantiationException ex) {
                throw new GrmException("componentDelegate.error", ex, BUNDLE, ex.getLocalizedMessage());
            } catch (InvocationTargetException ex) {
                throw new GrmException("componentDelegate.error", ex.getTargetException(), BUNDLE, ex.getTargetException().getLocalizedMessage());
            }

        } finally {
            lock.writeLock().unlock();
        }
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "componentDelegate.loaded", newDelegate.toString());
        }
        return newDelegate;
    }

    /**
     * This method is called after successfully loading of the delegate. 
     * 
     * <p>
     * Subclasses can override this method, however they must call the implementation of the parent
     * class.</p>
     * 
     * <p><b>Attention!!</B> This method is called within a locked code. Subclasses overriding this method 
     *    <p>must not</b> call any method from <code>GrmComponentDelegate</code> otherwise a deadlock can occur</p>.
     * 
     * @param newDelegate   the new delegate
     * @param oldDelegate   the old delegate (can be null)
     */
    protected void delegateLoaded(T newDelegate, T oldDelegate) {
        try {
            newDelegate.addComponentEventListener(componentListener);
        } catch (GrmRemoteException ex) {
            // The GrmRemoteException should not happen because the delegate is a local object
            LogRecord lr = new LogRecord(Level.SEVERE, "componentDelegate.ex.registerComponentEventListener");
            lr.setParameters(new Object[]{getName(), ex.getLocalizedMessage()});
            lr.setThrown(ex);
            lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
            log.log(lr);
        }
        if (oldDelegate != null) {
            try {
                oldDelegate.removeComponentEventListener(componentListener);
            } catch (GrmRemoteException ex) {
                // The GrmRemoteException should not happen because the delegate is a local object
                LogRecord lr = new LogRecord(Level.SEVERE, "componentDelegate.ex.unregisterComponentEventListener");
                lr.setParameters(new Object[]{getName(), ex.getLocalizedMessage()});
                lr.setThrown(ex);
                lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                log.log(lr);
            }
        }
    }

    /**
     * Get the configuration of the component.
     *
     * @throws com.sun.grid.grm.GrmException if the configuration was not found
     * @return the configuration
     */
    @SuppressWarnings(value = "unchecked")
    protected C getConfig() throws GrmException {
        GetConfigurationCommand<C> cmd = new GetConfigurationCommand<C>(name);
        return env.getCommandService().<C>execute(cmd).getReturnValue();
    }

    /**
     * Triggers a newState transition from STOPPED to STARTED.
     * 
     * <p>If the implementation of the delegate object has changed (classpath
     *    or classname) a new delegate object is created before calling the
     *    <code>start</code> method of the delegate.</p>
     * 
     * @throws com.sun.grid.grm.GrmException if the delegate could not be created or if the start method of the delegate
     *            has thrown an Exception. The component state will be STOPPED
     */
    public final void start() throws GrmException {
        try {
            URL[] classpath = getClasspath();
            String classname = getDelegateClassname();
            if (hasImplementationChanged(classname, classpath)) {
                loadDelegate(classname, classpath);
            }
            getDelegate().start();
        } catch (GrmException ex) {
            setState(ComponentState.STOPPED);
            throw ex;
        } catch (Exception ex) {
            setState(ComponentState.STOPPED);
            throw new GrmException("componentDelegate.error.start.unknown", ex, BUNDLE, getName(), ex.getLocalizedMessage());
        } catch (NoClassDefFoundError ex) {
            // It can happen that classpath of the delegate seems to be correct,
            // however some classes are missing
            // => set component newState to stopped and throw a exception
            setState(ComponentState.STOPPED);
            throw new GrmException("componentDelegate.error.start.missingclass", ex, BUNDLE, getName(), ex.getLocalizedMessage());
        }
    }

    /**
     * Triggers a newState transition from STARTED to STOPPING.
     *
     * @param isForced Force the newState transition.
     * @throws com.sun.grid.grm.GrmException
     */
    public final void stop(boolean isForced) throws GrmException {
        getDelegate().stop(isForced);
    }

    /**
     * Triggers a newState transition from STARTED to RELOADING.
     *
     * <p>If the implementation of the delegate (classpath of classname) has been
     *    changed first the <code>stop</code> method if the delegate will be called
     *    and finally a new delegate is created by calling the <code>start</code> method./<p>
     * 
     * @param isForced Force the newState transition.
     * @throws com.sun.grid.grm.GrmException when an error happend. It can
     * also be a ReloadNotSupportedException, when the Service does not support the
     * reload.
     */
    public final void reload(boolean isForced) throws GrmException {
        try {
            URL[] classpath = getClasspath();
            String classname = getDelegateClassname();
            if (hasImplementationChanged(classname, classpath)) {
                stop(isForced);
                start();
            } else {
                getDelegate().reload(isForced);
            }
        } catch (GrmException ex) {
            setState(ComponentState.STOPPED);
            throw ex;
        } catch (Exception ex) {
            setState(ComponentState.STOPPED);
            throw new GrmException("componentDelegate.error.reload.unknown", ex, BUNDLE, getName(), ex.getLocalizedMessage());
        }
    }

    /**
     * Returns the current component newState.
     *
     * @return Current newState.
     */
    public final ComponentState getState() throws GrmRemoteException {
        lock.readLock().lock();
        try {
            return state;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get the execution env of the component
     * @return the execution env
     */
    public final ExecutionEnv getExecutionEnv() {
        return env;
    }

    /**
     * Get the name of the component
     * @return the name of the component
     */
    public final String getName() {
        return name;
    }

    /**
     * Get the name of host on which runs the component
     * @return hostname
     */
    public final Hostname getHostname() {
        return hostname;
    }

    /**
     * Remove a newState change listener from the component.
     *
     * @param listener
     */
    public void removeComponentEventListener(ComponentEventListener listener) throws GrmRemoteException {
        cmpEventSupport.removeComponentEventListener(listener);
    }

    /**
     * Add a newState change listener to the component.
     *
     * @param listener 
     */
    public void addComponentEventListener(ComponentEventListener listener) throws GrmRemoteException {
        // We can not add the componentEventListener directly to the delegate
        // because the registered listener must survive a reload of the delegate
        cmpEventSupport.addComponentEventListener(listener);
    }

    private void setState(ComponentState newState) {
        boolean changed = false;
        ComponentState oldState = null;
        lock.writeLock().lock();
        try {
            if (!this.state.equals(newState)) {
                this.state = newState;
                changed = true;
            }
        } finally {
            lock.writeLock().unlock();
        }
        if (changed) {
            cmpEventSupport.fireComponentStateChangedEvent(newState);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "stateChanged", new Object[]{getName(), oldState, newState});
            }
        }
    }

    private class MyComponentListener extends ComponentEventAdapter {

        @Override
        public void componentUnknown(ComponentStateChangedEvent event) {
            setState(ComponentState.UNKNOWN);
        }

        @Override
        public void componentStarting(ComponentStateChangedEvent event) {
            setState(ComponentState.STARTING);
        }

        @Override
        public void componentStarted(ComponentStateChangedEvent event) {
            setState(ComponentState.STARTED);
        }

        @Override
        public void componentStopping(ComponentStateChangedEvent event) {
            setState(ComponentState.STOPPING);
        }

        @Override
        public void componentStopped(ComponentStateChangedEvent event) {
            setState(ComponentState.STOPPED);
        }

        @Override
        public void componentReloading(ComponentStateChangedEvent event) {
            setState(ComponentState.RELOADING);
        }
    }
}
