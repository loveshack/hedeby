/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

/**
 * Following enum describes possible states of a Component or
 * Subcomponent. ComponentState transitions can be triggered by invoking
 * one of the methods contained in this interface. Automatic state
 * transitions are following:<P>
 *
 *      STOPPED -> STARTING -> STARTED<P>
 *      STARTED -> STOPPING -> STOPPED<P>
 *      STOPPING -> STOPPED<P>
 *      STARTED -> RELOADING -> STARTED<P>
 *
 * They will be triggered when the corresponding action has been
 * finished completely.<P>
 * All method calles will block until the end of the execution or
 * return as soon as an error happens. A component which produced
 * an error will be in stated STOPPED
 */
public enum ComponentState {

    /**
     * Component is starting. It does not serve any functionality.
     */
    STARTING,
    /**
     * Component has been started. Services are provided.
     */
    STARTED,
    /**
     * Component is shutting down. It does not serve any functionality.
     */
    STOPPING,
    /**
     * Component has been stopped. It does not serve any functionality.
     */
    STOPPED,
    /**
     * Component is in process of reloading the configuration. 
     * Incoming requests are not queued.
     */
    RELOADING,
    /**
     * Component is in an unknown state, most likely because it cannot be
     * reached.
     */
    UNKNOWN
}
