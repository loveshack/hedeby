/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * DoubleKeyedHashMap.java
 *
 * Created on September 22, 2006, 12:25 PM
 *
 */

package com.sun.grid.grm.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * This class represents a mapping with two keys.  Internally, it is implemented
 * as a map of maps.  When an entry using the first key is first added to the
 * map, a sub-map will be created for that key.  Subsequent entries using the
 * same first key will be added to that same map.  When the last entry using a
 * particular first key is removed, the map associated with that first key is
 * also removed.
 * <p>The normal, single-keyed get/put/remove methods function normally, with
 * the key being a K1 and the value being a Map<K2,V>.  This class also provides
 * double-keyed get/put/remove methods that take two keys, of types K1 and K2,
 * and operate on a V.  The containsValue() method is overriden to only test for
 * the presence of the given value in a sub-map.</p>
 */
public class DoubleKeyedHashMap<K1,K2,V> extends HashMap<K1,Map<K2,V>> {
    
    private final static long serialVersionUID = -20080101801L;
    /**
     * Constructs an empty <tt>DoubleKeyedHashMap</tt> with the default initial
     * capacity (16) and the default load factor (0.75).
     */
    public DoubleKeyedHashMap() {
        super();
    }

    /**
     * Constructs an empty <tt>DoubleKeyedHashMap</tt> with the specified
     * initial capacity and the default load factor (0.75).
     *
     * @param  initialCapacity the initial capacity.
     * @throws IllegalArgumentException if the initial capacity is negative.
     */
    public DoubleKeyedHashMap(int initialCapacity) {
        super(initialCapacity);
    }

    /**
     * Constructs an empty <tt>DoubleKeyedHashMap</tt> with the specified
     * initial capacity and load factor.
     *
     * @param  initialCapacity The initial capacity.
     * @param  loadFactor      The load factor.
     * @throws IllegalArgumentException if the initial capacity is negative
     *         or the load factor is nonpositive.
     */
    public DoubleKeyedHashMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    /**
     * Returns the value to which the specified keys are mapped in this identity
     * hash map, or <tt>null</tt> if the map contains no mapping for these keys.
     * A return value of <tt>null</tt> does not <i>necessarily</i> indicate
     * that the map contains no mapping for the keys; it is also possible that
     * the map explicitly maps the keys to <tt>null</tt>. The
     * <tt>containsKey</tt> method may be used to distinguish these two cases.
     *
     * @param   key1 the first key whose associated value is to be returned.
     * @param   key2 the second key whose associated value is to be returned.
     * @return  the value to which this map maps the specified keys, or
     *          <tt>null</tt> if the map contains no mapping for these keys.
     * @see #put
     */
    public V get(Object key1, Object key2) {
        V ret = null;

        Map<K2,V> map = get(key1);

        if (map != null) {
            ret = map.get(key2);
        }

        return ret;
    }

    /**
     * Associates the specified value with the specified keys in this map.
     * If the map previously contained a mapping for these keys, the old
     * value is replaced.
     *
     * @param key1 first key with which the specified value is to be associated.
     * @param key2 second key with which the specified value is to be
     * associated.
     * @param value value to be associated with the specified keys.
     * @return previous value associated with specified keys, or <tt>null</tt>
     *	       if there was no mapping for key1 and key2.  A <tt>null</tt>
     *         return can also indicate that the map previously associated
     *	       <tt>null</tt> with the specified keys.
     */
    public V put(K1 key1, K2 key2, V value) {
        V ret = null;
        Map<K2,V> map = get(key1);

        if (map == null) {
            map = new HashMap<K2,V>();
            put(key1, map);
        }

        ret = map.get(key2);
        map.put(key2, value);

        return ret;
    }

    /**
     * Removes the mapping for these keys from this map if present.
     *
     * @param  key1 first key whose mapping is to be removed from the map.
     * @param  key2 second key whose mapping is to be removed from the map.
     * @return previous value associated with specified keys, or <tt>null</tt>
     *	       if there was no mapping for key1 and key2.  A <tt>null</tt>
     *         return can also indicate that the map previously associated
     *	       <tt>null</tt> with the specified keys.
     */
    public V remove(Object key1, Object key2) {
        V ret = null;
        Map<K2,V> map = get(key1);

        if (map != null) {
            ret = map.remove(key2);

            if (map.size() == 0) {
                remove(key1);
            }
        }

        return ret;
    }

    /**
     * Returns <tt>true</tt> if this map contains a mapping for the
     * specified keys.
     *
     * @param   key1  The first key whose presence in this map is to be tested
     * @param   key2  The second key whose presence in this map is to be tested
     * @return <tt>true</tt> if this map contains a mapping for the specified
     * keys.
     */
    public boolean containsKeys(Object key1, Object key2) {
        boolean ret = false;
        Map<K2,V> map = get(key1);

        if (map != null) {
            ret = map.containsKey(key2);
        }

        return ret;
    }

    /**
     * Returns <tt>true</tt> if this map maps one or more pairs of keys to the
     * specified value.
     * @param value value whose presence in this map is to be tested.
     * @return <tt>true</tt> if this map maps one or more pairs of keys to the
     *         specified value.
     */
    @Override
    public boolean containsValue(Object value) {
        boolean ret = false;

        for (K1 key : keySet()) {
            Map<K2,V> map = get(key);

            if (map != null) {
                ret = map.containsValue(value);
            }
        }

        return ret;
    }

    /**
     * This method replaces the Map.values() method for retrieving all value
     * object in the map.  It returns a collection of all values stored in the
     * map.  An object may appear more than once in the collection.
     */
    public Collection<V> keyedValues() {
        Collection<V> retValue = new LinkedList<V>();

        for (Map<K2,V> map : values()) {
            retValue.addAll(map.values());
        }

        return retValue;
    }
}
