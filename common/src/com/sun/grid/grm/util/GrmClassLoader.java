/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import com.sun.grid.grm.bootstrap.PathUtil;
import java.io.File;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 *  Ths GrmClassLoader is a <code>URLClassLoader</code>. 
 * 
 *  <p>The placeholder <code>${distlib}/code> inside a URL will be replaced by the 
 *  real distribution lib directory of the hedeby system.</p>
 *
 *   <H3>Example:</H3>
 *
 *   <pre>
 *       GrmClassLoader cl = GrmClassLoader.newInstance("${distlib}/lib/hedeby-security-impl.jar");
 *   </pre>
 *  
 */
public class GrmClassLoader extends URLClassLoader {
    
    private final String name;
    
    private GrmClassLoader(String name, URL [] urls, ClassLoader parent) {
        super(urls, parent);
        this.name = name;
    }
    
    /**
     * Create a new instance of the <code>GrmClassLoader</code>.
     *
     * @param name       name of the class loader
     * @param classpath  classpath as string 
     * @param delimiter  delimiter for the elements in <code>classpath</code>
     * @param parent     the parent class loader
     * @return the new <code>GrmClassLoader</code>
     */
    public static GrmClassLoader newInstance(String name, String classpath, String delimiter, ClassLoader parent) {
        String [] elements = classpath.split(delimiter);
        return newInstance(name, elements, parent);
    }

    /**
     * Create a new instance of the <code>GrmClassLoader</code>.
     *
     * @param name       name of the class loader
     * @param classpath  classpath as string 
     * @param delimiter  delimiter for the elements in <code>classpath</code>
     * @return the new <code>GrmClassLoader</code>
     */
    public static GrmClassLoader newInstance(String name, String classpath, String delimiter) {
        return newInstance(name, classpath, (ClassLoader)null);
    }
    
    /**
     * Create a new instance of the <code>GrmClassLoader</code>.
     *
     * @param name       name of the class loader
     * @param classpath  classpath as string. delimiter is the system path delimiter
     *                   (<code>File.pathSeparator</code>)
     * @param parent     the parent class loader
     * @return the new <code>GrmClassLoader</code>
     */
    public static GrmClassLoader newInstance(String name, String classpath, ClassLoader parent) {
        return newInstance(name, classpath, File.pathSeparator, parent);
    }

    /**
     * Create a new instance of the <code>GrmClassLoader</code>.
     *
     * @param name       name of the class loader
     * @param classpath  classpath as string. delimiter is the system path delimiter
     *                   (<code>File.pathSeparator</code>)
     * @return the new <code>GrmClassLoader</code>
     */
    public static GrmClassLoader newInstance(String name, String classpath) {
        return newInstance(name, classpath, (ClassLoader)null);
    }
    
    /**
     * Create a new instance of the <code>GrmClassLoader</code>.
     *
     * @param name       name of the class loader
     * @param pathElements elements of the classpath
     * @return the new <code>GrmClassLoader</code>
     */
    public static GrmClassLoader newInstance(String name, String [] pathElements) {
        return newInstance(name, pathElements, null);
    }
    
    /**
     * Create a new instance of the <code>GrmClassLoader</code>.
     *
     * @param name       name of the class loader
     * @param pathElements elements of the classpath
     * @param parent     the parent class loader
     * @return the new <code>GrmClassLoader</code>
     */
    public static GrmClassLoader newInstance(String name, String [] pathElements, ClassLoader parent) {
        
        URL [] urls = new URL[pathElements.length];
        
        String distLibURL = PathUtil.getDistLibURL();
        
        for(int i = 0; i < pathElements.length; i++) {
            String file = pathElements[i].replace("${distlib}", distLibURL);
            try {
                urls[i] = new URL(file);
            } catch (MalformedURLException ex) {
                throw new IllegalStateException("path elements " + pathElements[i] + " defines invalid url"
                                       , ex);
            }

        }        
        return newInstance(name, urls, parent);
    }
    
    /**
     * Create a new instance of <code>GrmClassLoader</code>
     * @param name    name of the class loader
     * @param url     classpath elements
     * @param parent  the parent class loader
     * @return the new <code>GrmClassLoader</code>
     */
    public static GrmClassLoader newInstance(String name, URL[] url, ClassLoader parent) {
        return new GrmClassLoader(name, url, parent);
    }
            
    
    
    
    /**
     * Get a human readable string representation of the classloader.
     * 
     * Prints all classpath elements for the classloader and its parents. 
     * @return the string representation
     */
    @Override
    public String toString() {
        return toString(this);
    }
    
   
    /**
     * Get a human readable string representation of a classloader.
     * 
     * Prints all classpath elements for the classloader and its parents. 
     * @param cl the classloader
     * @return the string representation
     */
    public static String toString(ClassLoader cl) {
        StringWriter sw = new StringWriter();
        Printer p = new Printer(sw);
        try {
            printClassLoader(cl, p);
        } finally {
            p.close();
        }
        return sw.getBuffer().toString();
    }
    
    private static void printClassLoader(ClassLoader cl, Printer p) {
        
        p.print(cl.getClass().getSimpleName());
        if(cl instanceof GrmClassLoader) {
            p.print('(');
            p.print(((GrmClassLoader)cl).getName());
        }
        if(cl instanceof URLClassLoader) {
            URL [] urls = ((URLClassLoader)cl).getURLs();
            p.println('{');
            p.indent();
            for(int i = 0; i < urls.length; i++) {
                p.println(urls[i]);
            }
            p.deindent();
            p.println('}');
        }
        if(cl.getParent() != null) {
            printClassLoader(cl.getParent(), p);
        }
        p.print(']');
    }

    /**
     * Get the name of the class loader.
     *
     * @return name of the class loader
     */
    public String getName() {
        return name;
    }
    
}
