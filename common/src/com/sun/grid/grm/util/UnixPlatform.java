/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.xml.bind.v2.util.ByteArrayOutputStreamEx;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The <code>UnixPlatform</code> class implements the abstract methods of a
 * <code>Platform</code> for a unix system.
 *
 * <ul>
 *    <li>The <code>chown</code> is performed by executing the <code>chown</code> unix
 *        command.</li>
 *    <li>The <code>makeExecutable</code> is performed by execution the <code>chmod u+x</code>
 *        unix command.</li>
 *    <li>The <code>execAs</code> is performed by execution a <code>su user -c command</code>
 *    </li>
 * </ul>
 *
 * <H2>Customizing the executed commands</h2>
 *
 * The commands which will be executed by this class can be defined in an property
 * file. The property file can be specified the <code>com.sun.grid.grm.executor.impl.Platform.commandFile</code>
 * system property.
 *
 * <h3>Property definition</h3>
 *
 * <table>
 *   <tr><th>Property name</th><th>Description</th></tr>
 *   <tr>
 *       <td>chown</td>
 *       <td>Defines the command which will be executed in the by the <code>chown</code>
 *           method.<br>
 *           The parameter <code>{0}</code> will be replaced by the target file<br>
 *       </td>
 *   </tr>
 *   <tr>
 *       <td>chown.recursive</td>
 *       <td>Defines the command which will be executed in the by the <code>chown</code>
 *           method if the <code>recursive</code> parameter is <code>true</code><br>
 *           The parameter <code>{0}</code> will be replaced by the target file<br>
 *       </td>
 *   </tr>
 *   <tr>
 *       <td>rmdir</td>
 *       <td>Defines the command which will be executed in the by the <code>removeDir</code>
 *           method if the <code>recursive</code> parameter is <code>true</code><br>
 *           The parameter <code>{0}</code> will be replaced by the target file<br>
 *       </td>
 *   </tr>
 *   <tr>
 *       <td>rmdir.forced</td>
 *       <td>Defines the command which will be executed in the by the <code>removeDir</code>
 *           method if the <code>force</code> parameter is <code>true</code><br>
 *           The parameter <code>{0}</code> will be replaced by the target file<br>
 *       </td>
 *   </tr>
 *   <tr>
 *       <td>execAs</td>
 *       <td>Defines the command which will be executed in the by the <code>execAs</code>
 *           method<br>
 *           The parameter <code>{0}</code> will be replaced by the username<br>
 *           The parameter <code>{1}</code> will be replaced by command<br>
 *       </td>
 *   </tr>
 *   <tr>
 *       <td>chmod</td>
 *       <td>change files attributes<br>
 *           The parameter <code>{0}</code> will be replaced by the new mode (e.g u+x).
 *           {1} will be replaced by the file
 *     <br>
 *       </td>
 *   </tr>
 *   <tr>
 *       <td>sh</td>
 *       <td>Defines the command which will be used for executing borne shell scripts<br>
 *       </td>
 *     <br>
 *       </td>
 *   </tr>
 * </table>
 *
 * <b>Example property file</b>
 *
 * <pre>
 * chown=chown {0} {1}
 * chown.recursive=chown -R {0} {1}
 * chmod=chmod {0} {1}
 * chmod.recursive=chmod -r {0} {1} *
 * rmdir=rm -r {0}
 * rmdir.forced=rm -rf {0}
 * execAs=su {0} -c {1}
 * sh=/bin/sh
 * </pre>
 *
 */
public class UnixPlatform extends Platform {

    private static final String BUNDLE = "com.sun.grid.grm.util.util";
    private static final Logger log = Logger.getLogger(UnixPlatform.class.getName(), BUNDLE);
    
    /** system property for the command file. */
    public static final String COMMAND_FILE_SYSTEM_PROPERTY = UnixPlatform.class.getName() + ".commandFile";
    
    private String chownCommand;
    private String chownRecursiveCommand;
    private String rmdirCommand;
    private String rmdirForceCommand;
    private String execAsCommand;
    private String chmodCommand;
    private String chmodRecursiveCommand;
    private String copyCommand;
    private String copyRecursiveCommand;
    private String copyPreserveCommand;
    private String copyRecursivePreserveCommand;
    private String moveCommand;
    private String linkCommand;
    private String sh;
    private String wrapper;

    
    private final ShutdownHook shutdownHook = new ShutdownHook();
    /**
     *  Create a new UnixPlatform.
     */
    public UnixPlatform() {
        
        System.loadLibrary("platform");
        
        initTerminal();
        
        try {
            String commandFile = System.getProperty(COMMAND_FILE_SYSTEM_PROPERTY);
            InputStream in = null;
            
            if (commandFile != null) {
                in = new FileInputStream(commandFile);
            } else {
                in = getClass().getResourceAsStream('/' + getClass().getName().replace('.', '/') + ".properties");
            }
            Properties props = new Properties();
            if(in != null) {
                try {
                    props.load(in);
                } finally {
                    in.close();
                }
            } else {
                // Test installer can not handle property files
                // for this case we set the default values by hand
                props.setProperty("chown", "chown {0} {1}");
                props.setProperty("chown.recursive", "chown -R {0} {1}");
                props.setProperty("chmod","chmod {0} {1}");
                props.setProperty("chmod.recursive","chmod -R {0} {1}");
                props.setProperty("cp","cp {0} {1}");
                props.setProperty("cp.recursive","cp -r {0} {1}");
                props.setProperty("cp.preserve","cp -p {0} {1}");
                props.setProperty("cp.recursive.preserve","cp -pr {0} {1}");
                props.setProperty("rmdir","rm -r {0}");
                props.setProperty("rmdir.forced","rm -rf {0}");
                props.setProperty("execAs","su {0} -c \"{1}\"");
                props.setProperty("link","ln -s {0} {1}");
                props.setProperty("sh", "sh");
                props.setProperty("mv", "mv {0} {1}");
            }
            
            chownCommand = getCmd(props, "chown");
            chownRecursiveCommand = getCmd(props, "chown.recursive");
            rmdirCommand = getCmd(props, "rmdir");
            rmdirForceCommand = getCmd(props, "rmdir.forced");
            execAsCommand = getCmd(props, "execAs");
            chmodCommand = getCmd(props, "chmod");
            chmodRecursiveCommand = getCmd(props, "chmod.recursive");
            copyCommand = getCmd(props, "cp");
            copyRecursiveCommand = getCmd(props, "cp.recursive");
            copyPreserveCommand = getCmd(props, "cp.preserve");
            copyRecursivePreserveCommand = getCmd(props, "cp.recursive.preserve");
            moveCommand = getCmd(props, "mv");
            linkCommand = getCmd(props, "link");
            sh = getCmd(props, "sh");
            //read the wrapper script
            wrapper = FileUtil.readResourceAsString(this.getClass(), "wrapper.sh");
            /* The shutdown hook resets the terminal and deletes all tmp file */
            Runtime.getRuntime().addShutdownHook(shutdownHook);
            
        } catch (IOException ioe) {
            throw new IllegalStateException(I18NManager.formatMessage("unixplatform.load.failed", BUNDLE, ioe.getMessage()), ioe);
        }
        
    }
    
    private String getCmd(final Properties props, final String cmd) {
        String ret = props.getProperty(cmd);
        if (ret == null) {
            throw new IllegalStateException(I18NManager.formatMessage("unixplatform.cmd.notdefined", BUNDLE, cmd));
        }
        return ret;
    }
    
    private IOException createCmdError(String msg, Object ... params) {
        return new IOException(I18NManager.formatMessage(msg, BUNDLE, params));
    }
    
    private int execCommand(final String command, final Object[] params,
            final String[] env, final File dir,
            final List<String> stdout, final List<String> stderr, InputStream stdin, int timeout)
            throws IOException, InterruptedException {
        return exec(MessageFormat.format(command, params), env, dir, stdout, stderr, stdin, timeout);
    }
    
    private int execCommand(final String command, final Object[] params,
            final String[] env, final File dir,
            final List<String> stdout, final List<String> stderr, int timeout)
            throws IOException, InterruptedException {
        return execCommand(command, params, env, dir, stdout, stderr, null, timeout);
    }

    
    /**
     * Remove a directory.
     * @param dir     the directory
     * @param force   force the deletetion
     * @throws java.io.IOException on any IO error
     * @throws java.lang.InterruptedException if the deletion has be
     *                                        interrupted
     */
    @Override public final void removeDir(final File dir, final boolean force)
    throws IOException, InterruptedException {
        
        Object [] params = new Object[] {
            dir.getAbsolutePath()
        };
        int ret = 0;
        if (force) {
            ret = execCommand(rmdirForceCommand, params, null, null, null, null, 0);
        } else {
            ret = execCommand(rmdirCommand, params, null, null, null, null, 0);
        }
        if (ret != 0) {
            throw createCmdError("unixplatform.rmdir.failed", dir, ret);
        }
    }
    
    /**
     * Execute a command as an user.
     *
     * @param user      the user name
     * @param command   the command
     * @param env       environment for the command
     * @param dir       workding directory for the command
     * @param stdout    list for stdout message
     * @param stderr    list for sterr message
     * @throws java.io.IOException on any IO error
     * @throws java.lang.InterruptedException if the command has been
     *                                        interrupted
     * @return the exit code of the command
     */
    @Override public final int execAs(String user, String command, String[] env,  
                                      File dir, List<String> stdout,
                                      List<String> stderr, int timeout)
            throws IOException, InterruptedException {

        /* if the jvm is not running as root user switching is not
           possible. Run all commands as normal user */
        user = getExecAsUser(user);
        
        int ret = 0;
        
        ByteArrayOutputStream bout = new ByteArrayOutputStreamEx();
        writeShellScript(command, env, bout);

        bout.flush();
        bout.close();

        InputStream stdin = new ByteArrayInputStream(bout.toByteArray());
        if (user == null || user.equals(System.getProperty("user.name"))) {
            ret = exec(sh, null, dir, stdout, stderr, stdin, timeout);
        } else {
            ret = execCommand(execAsCommand,
                    new Object[]{user, sh},
                    null, dir, stdout, stderr, stdin, timeout);
        }
        return ret;
    }
    
    private void writeShellScript(String command, String [] env, OutputStream out) throws IOException {
        
        Printer p = new Printer(out);
        try {
            if(env != null) {
                for(String envVar: env) {
                    int index = envVar.indexOf('=');
                    String name = envVar.substring(0, index);
                    String value = envVar.substring(index+1, envVar.length());
                    p.print(name);
                    p.print("=\"");
                    //fix for issue 622
                    p.print(value.replace("\"", "\\\""));
                    p.println("\"");
                    p.print("export ");
                    p.println(name);
                }
            }
            p.println(command);

            p.println("exit $?");
        } finally {
            p.close();
        }
    }
    
    private boolean execAsWarningWritten = false;
    
    /*
     *  This method checks if user switching is necessary and possible
     *  for the execAs.
     *
     *  @return  <code>null</code> if user switching is not possible or necessary
     *           else the content of the <code>user</code> parameter
     *
     */
    private String getExecAsUser(String user) {
        
        String jvmUser = System.getProperty("user.name");
        
        /* if the jvm is not runnig as root user switching is not
           possible. Run all commands as normal user */
        if(!isSuperUser() && !jvmUser.equals(user)) {
            if(!execAsWarningWritten) {
                log.log(Level.WARNING, "unixplatform.execAs.notRoot", System.getProperty("user.name"));
                execAsWarningWritten = true;
            }
            return null;
        } else {
            return user;
        }
    }
    
    private boolean chownWarningWritten = false;
    
    /**
     * Change the owner of a file.
     *
     * <b>Attention:</b> chown is on the unix platform only possible if the jvm
     * is running as user root. If not this method simply ignore the chown call
     * @param file    the file
     * @param owner   the new owner
     * @param recursive  change ownership for all subdirectories and files
     * @throws java.io.IOException on any IO error
     * @throws java.lang.InterruptedException if the chown has been interrupted
     */
    @Override public final void chown(final File file, final String owner,
            final boolean recursive)
            throws IOException, InterruptedException {
        
       // chown is only possible as user root.
       // As normal sticke user, do nothing. A warning
       // has been written by the constructor of UnixPlatform
         
        if(isSuperUser()) {
            Object [] params = new Object[] {
                owner, file.getAbsolutePath()
            };
            int ret = 0;
            LinkedList<String> stderr = new LinkedList<String>();
            if (recursive) {
                ret = execCommand(chownRecursiveCommand, params, null, null, null, stderr, 0);
            } else {
                ret = execCommand(chownCommand, params, null, null, null, stderr, 0);
            }
            if (ret != 0) {
                throw createCmdError("unixplatform.chown.failed", file, getErrorMessage(stderr, ret));
            }
        } else {
            if(!chownWarningWritten) {
               log.warning("unixplatform.chown.notRoot");
               chownWarningWritten = true;
            }
        }
    }
    
    
    /**
     *  Get the owner of a file
     *  @param file the file
     *  @return the owner of the file
     *  @throws IOException on error
     */
    public String getFileOwner(File file) throws IOException {
        
        if (!file.exists()) {
            throw new IOException(I18NManager.formatMessage("unixplatform.gfo.ne", BUNDLE, file));
        }
        
        int uid = getUID(file.getAbsolutePath());
        if (uid < 0) {
            throw new IOException(I18NManager.formatMessage("unixplatform.gfo.uid", BUNDLE, file));
        }
        
        String ret = uid2User(uid);
        if (ret == null) {
            throw new IOException(I18NManager.formatMessage("unixplatform.gfi.uid2user", BUNDLE, file));
        }
        return ret;
    }
    
    /**
     * Get the id of the user that owns file
     * @param file the file
     * @return the user id or -1 on error
     */
    private native int getUID(String file);
    
    /**
     * Get the name of a user by its id
     * @param uid the user id
     * @return the user name of <tt>null</tt> if the username could not be determined
     */
    private native String uid2User(int uid);
    
    /**
     *     Create a symbolic link
     *
     *    @param file the file
     *    @param link the symbolic link
     *    @throws java.io.IOException on any io error
     *    @throws java.lang.InterruptedException if the action has been interrupted
     */
    @Override public void link(File file, File link) throws IOException, InterruptedException {
        Object params[] = new Object[] {file.getAbsolutePath(), link.getAbsolutePath()};
        int ret = 0;
        
        LinkedList<String> stderr = new LinkedList<String>();
        ret = execCommand(linkCommand, params, null, null, null, stderr, 0);
        if (ret != 0) {
            throw createCmdError("unixplatform.link.failed", file, link, getErrorMessage(stderr, ret));
        }
    }
    
    /**
     * Change the file system mode of a file.
     *
     * @param file      the file
     * @param mode      the new mode
     * @param recursive include all files and subdirectories
     *
     * @throws java.io.IOException  on any io error
     * @throws java.lang.InterruptedException if the action has been interrupted
     */
    @Override public void chmod(File file, String mode, boolean recursive)
    throws IOException, InterruptedException {
        int ret = 0;
        
        LinkedList<String> stderr = new LinkedList<String>();
        if (recursive) {
            ret = execCommand(chmodRecursiveCommand,
                    new Object [] {
                mode, file.getAbsolutePath()
            }, null, null, null, stderr, 0);
        } else {
            ret = execCommand(chmodCommand,
                    new Object [] {
                mode, file.getAbsolutePath()
            }, null, null, null, stderr, 0);
        }
        
        if (ret != 0) {
            throw createCmdError("unixplatform.chmod.failed", file, getErrorMessage(stderr, ret));
        }
    }
    
    private static String getErrorMessage(LinkedList<String> stderr, int exitCode) {
        String msg = null;
        if(stderr.isEmpty()) {
            msg = "exit code " + exitCode;
        } else if(stderr.size() == 1) {
            msg = stderr.getFirst();
        } else {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            for(String line: stderr) {
                pw.println(line);
            }
            pw.close();
            msg = sw.getBuffer().toString();
        }
        return msg;
    }
    
    @Override public void copy(File file, File targetFile, boolean recursive, boolean preserve) throws IOException,
            InterruptedException {
        int ret = 0;
        
        if (recursive && !preserve) {
            ret = execCommand(copyRecursiveCommand,
                    new Object [] {
                file.getAbsolutePath(), targetFile.getAbsolutePath()
            }, null, null, null, null, 0);
        } else if (!recursive && !preserve) {
            ret = execCommand(copyCommand,
                    new Object [] {
                file.getAbsolutePath(), targetFile.getAbsolutePath()
            }, null, null, null, null, 0);
        } else if (!recursive && preserve) {
            ret = execCommand(copyPreserveCommand,
                    new Object [] {
                file.getAbsolutePath(), targetFile.getAbsolutePath()
            }, null, null, null, null, 0);
        } else {
            ret = execCommand(copyRecursivePreserveCommand,
                    new Object [] {
                file.getAbsolutePath(), targetFile.getAbsolutePath()
            }, null, null, null, null, 0);
        }
        if (ret != 0) {
            throw createCmdError("unixplatform.copy.failed", file, targetFile, ret);
        }
    }

    public void move(File file, File targetFile) throws IOException,
            InterruptedException {
        int ret = 0;

        ret = execCommand(moveCommand,
                new Object[]{
                    file.getAbsolutePath(), targetFile.getAbsolutePath()
                }, null, null, null, null, 0);

        if (ret != 0) {
            throw createCmdError("unixplatform.copy.failed", file, targetFile, ret);
        }
    }
    
    /**
     * Get the gridengine architecture.
     *
     * The arch string is determined by executing the arch script at
     * <code>hedebyDist</code>/hedeby/util/arch.
     *
     * @param hedebyDist  path to the hedeby distribution
     * @return the arch string
     * @throws java.io.IOException execution of the arch script is failed
     */
    @Override public String getArch(File hedebyDist) throws IOException {
        
        File arch = new File(hedebyDist, "util/arch".replace('/', File.separatorChar));
        
        LinkedList<String> stdout = new LinkedList<String>();
        List<String> stderr = new LinkedList<String>();
        try {
            int res = exec(arch.getAbsolutePath(), null, hedebyDist, stdout, stderr, 0);
            if(res != 0) {
                throw new IOException("arch script exited with status (" + res + ")");
            }
            if(stdout.isEmpty()) {
                throw new IOException("arch script did not produce any output");
            }
            String ret = stdout.getFirst();
            if(ret == null || ret.length() == 0) {
                throw new IOException("arch script returned empty string");
            }
            return ret;
        } catch (InterruptedException ex) {
            throw new IOException("arch script has been interrupted");
        }
    }
    
    
    
    /**
     * Get the pid of the jvm.
     *
     * <b>!!!Attention!!!:</b> On some unix platforms each thread
     *  has its own pid. In this case a call of this method will
     *  return the pid of the current thread.
     *
     * @return the pid of the jvm
     */
    @Override public int getPid() {
        return getNativePid();
    }
    
    private native int getNativePid();
    
    /**
     *Get the uid of the user under which the current process is run
     *
     * @return the uid of the user
     */
    
    private int getUid() {
        return getNativeUid();
    }
    
    private native int getNativeUid();
    
    /**
     * Determine if the jvm is running as super user
     * @return <code>true</code> if the jvm is running as super user
     */
    @Override public boolean isSuperUser() { 
        if (getUid() == 0) {
            return true;
        }
        return false;       
  }


    /**
     * Send a process the kill signal
     *
     * @param  pid  id of the proccess
     * @return <code>true</code> if the send signal has been delivered
     */

    public boolean killProcess(int pid) {
        return (killNativeProcess(pid) == 0);
    }

    private native int killNativeProcess(int pid);

    /**
     * Send a process the terminate signal
     *
     * @param  pid  id of the proccess
     * @return <code>true</code> if the send signal has been delivered
     */
    
    public boolean terminateProcess(int pid) {
        return (terminateNativeProcess(pid) == 0);
    }

    private native int terminateNativeProcess(int pid);


    /**
     * Execute the command in native call
     * @param command as a String to be called.
     * @return exit code of executed command
     */
    public int executeScript(String command) {
        return executeNativeScript(command);
    }

    private native int executeNativeScript(String command);

    /**
     *   Execute a command and wait for exit.
     *
     *   @param command  the command
     *   @param env      environment of the command (&lt;name&gt;=&lt;value&gt;)
     *                   , can be <code>null</code>
     *   @param  dir     working directory of the process, make sure that user has
     *                   permissions in this directory to create/delete files
     *   @param stdout   list where the output of stdout of the command will be stored,
     *                    can be <code>null</code>
     *   @param stderr   list where the output of stderr of the command will be stored,
     *                   can be <code>null</code>
     *   @param stdin    input stream for the stdin of the process
     *   @param timeout  timeout in seconds, after this amount of time executed command will be terminated,
     *                   if 0, 3600 seconds is set
     *   @return the exit code of the command, -1 by default.
     *   @throws java.io.IOException if the command fails
     *   @throws java.lang.InterruptedException if the execution of the command has been interrupted
     */
    public int execute(final String command, final String[] env,
            final File dir, final List<String> stdout,
            final List<String> stderr, InputStream stdin, int timeout)
            throws IOException, InterruptedException {
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "UnixPlatform.exec", command);
        }
        List<String> localStdout = stdout;
        List<String> localStderr = stderr;
        File tmpDir = PathUtil.getTmpDir();
        int jvmPid = this.getPid();
        boolean globalerror = false;
        //default return value -1
        int ret = -1;

        //files used for saving streams, result and pid of subprocess, if 1000 tries are not enough to get
        //unique name then exception will be thrown, to protect system from blocking
        File out = PathUtil.getUniqueFile(tmpDir, "out."+jvmPid, 1000);
        File err = PathUtil.getUniqueFile(tmpDir, "err."+jvmPid, 1000);
        File pid = PathUtil.getUniqueFile(tmpDir, "pid."+jvmPid, 1000);
        File res = PathUtil.getUniqueFile(tmpDir, "res."+jvmPid, 1000);
        File script = PathUtil.getUniqueFile(tmpDir, "execute."+jvmPid, 1000);
        File scriptTmp = PathUtil.getUniqueFile(tmpDir, "executeTmp."+jvmPid, 1000);
        File in = PathUtil.getUniqueFile(tmpDir, "in."+jvmPid, 1000);

        int count = timeout;
        
        if (count==0) {
            //default timeout for scripts 1h
            count=3600;
        }
        try {

            FileOutputStream output = new FileOutputStream(scriptTmp);
            try {
                writeShellScript(wrapper, env, output);
            } finally {
                output.close();
            }
            Map<String, String> patterns = new HashMap<String, String>(1);

            patterns.put("@@@RES_FILE@@@", res.getAbsolutePath());
            patterns.put("@@@PID_FILE@@@", pid.getAbsolutePath());
            patterns.put("@@@IN_FILE@@@", in.getAbsolutePath());
            patterns.put("@@@OUT_FILE@@@", out.getAbsolutePath());
            patterns.put("@@@ERR_FILE@@@", err.getAbsolutePath());
            patterns.put("@@@CMD@@@", command);
            if (dir == null) {
                patterns.put("@@@DIR@@@", tmpDir.getAbsolutePath());
            } else {
                patterns.put("@@@DIR@@@", dir.getAbsolutePath());
            }
            patterns.put("@@@TIMEOUT@@@", String.valueOf(count));
            FileUtil.replace(scriptTmp, script, patterns);

            if (!scriptTmp.delete()) {
                //log cleanup error
            }

            //write InputStream for command into file
            FileUtil.writeInputToFile(stdin, in);
            
            StringBuilder c = new StringBuilder();
            c.append("/bin/sh ");
            c.append(script.getAbsolutePath());
            //Execute the command
            executeNativeScript(c.toString());
            //wait for script execution - check whether
            if (stdout == null && log.isLoggable(Level.FINER)) {
                localStdout = new ArrayList<String>();
            }

            if (stderr == null && log.isLoggable(Level.FINER)) {
                localStderr = new ArrayList<String>();
            }
            //read output files
            if (localStdout!=null && out.exists()) {
                localStdout.addAll(FileUtil.readToList(out));
            }
            if (localStderr!=null && err.exists()) {
                localStderr.addAll(FileUtil.readToList(err));
            }

        } catch (IOException ex) {
            //we got interrupt, log message 
            log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.io.execute", BUNDLE, command, ex.getLocalizedMessage()));
 
            globalerror=true;
            throw ex;
        } finally {
            // cleanup process
            if (pid.exists()) {
                //pid file found, some error occured, get pid, remove file, kill process,
                log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.execute.error.pid_file_exists", BUNDLE, command, pid.getAbsolutePath()));
                int pidnr = -1;
                String detail = null;
                //introduce temp variable for error signaling
                boolean error = false;
                try {
                    String r = FileUtil.read(pid);
                    int t = Integer.parseInt(r);
                    pidnr = t;
                } catch (NumberFormatException ex) {
                    //log error reading res, throw GrmException
                    error=true;
                    detail=ex.getLocalizedMessage();
                } catch (IOException ex) {
                    //log error reading res, throw GrmException
                    error=true;
                    detail=ex.getLocalizedMessage();
                } finally {
                    if (!error) {
                        //we have pid, kill the process
                        
                        log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.execute.kill", BUNDLE, command, pidnr));
                        if (!terminateProcess(pidnr)) {
                            log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.execute.kill.failed", BUNDLE, command, pidnr));
                        }

                    } else {
                        //pid wasnt read properly
                        if (!globalerror) {
                            throw new IOException(I18NManager.formatMessage("UnixPlatform.exec.cant_readpidfromfile", BUNDLE, command, pid.getAbsolutePath()));
                        } else {
                            //log failure of killing the process
                            log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.exec.cant_readpidfromfile", BUNDLE, command, pid.getAbsolutePath()));
                        }
                    }

                }
                if(!pid.delete()) {
                    //log error - couldnt make cleanup
                    log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.exec.filecleanup_failed", BUNDLE, pid.getAbsolutePath()));
                }

            }
            
            //cleanup files
            if (in.exists()) {
                if(!in.delete()) {
                    //log error - couldnt make cleanup
                    log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.exec.filecleanup_failed", BUNDLE, in.getAbsolutePath()));
                }
            }
            if (out.exists()) {
                if(!out.delete()) {
                    //log error - couldnt make cleanup
                    log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.exec.filecleanup_failed", BUNDLE, out.getAbsolutePath()));
                }
            }
            if (err.exists()) {
                if(!err.delete()) {
                    //log error - couldnt make cleanup
                    log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.exec.filecleanup_failed", BUNDLE, err.getAbsolutePath()));
                }
            }
            if (script.exists()) {
                if(!script.delete()) {
                    //log error - couldnt make cleanup
                    log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.exec.filecleanup_failed", BUNDLE, script.getAbsolutePath()));
                }
            }

            //result code should be written into file
            if (res.exists()) {
                //if result file exists but globalerror occured ignore reading result,
                String detail = null;
                if (!globalerror) {
                    boolean error = false;
                    try {
                        String r = FileUtil.read(res);
                        r=r.trim();
                        int t = Integer.parseInt(r);
                        ret=t;
                    } catch (NumberFormatException ex) {
                        //log error reading res, throw GrmException
                        detail = ex.getLocalizedMessage();
                        error=true;
                    } catch (IOException ex) {
                        //log error reading res, throw GrmException
                        detail=ex.getLocalizedMessage();
                        error=true;
                    } finally {
                        if (error) {
                           throw new IOException(I18NManager.formatMessage("UnixPlatform.exec.cant_read_resultfromfile", BUNDLE, command, res.getAbsolutePath(), detail));
                        }
                    }
                }
                if(!res.delete()) {
                    //log error - couldnt make cleanup
                    log.log(Level.WARNING, I18NManager.formatMessage("UnixPlatform.exec.filecleanup_failed", BUNDLE, res.getAbsolutePath()));
                }
            } else {
                //no result file error
                //result file is missing only if execution of wrapper was interrupted (killed) because of timeout
                //throw exception
                throw new InterruptedException(I18NManager.formatMessage("UnixPlatform.exec.missing_result_file", BUNDLE, res.getAbsolutePath(), command));

            }
        }
        if (log.isLoggable(Level.FINER)) {
            log.log(Level.FINER, "platform.exec.exit", new Object [] {command, ret});
            log.log(Level.FINER, "stderr: " + localStderr.toString());
            log.log(Level.FINER, "stdout: " + localStdout.toString());
        }
        return ret;
    }
    /**
     *  Edit a file.
     *
     *  <p>Javas <code>Runtime.exec</code> does not provide a valid terminal
     *  for editors like vi. This method call the native method <code>nativeEdit</code>
     *  to start an external editor.</p>
     *  <p>The native method forks a new process and executes the editor. The default
     *  editor is vi. The user can define a different editor by setting the enivroment
     *  variable <code>EDITOR</code>.</p>
     *
     *  @param file  the file which should be edited
     *  @return <code>true</code> if the file has been modified by the editor
     *          or <code>false</code> if the editor did not modify the file
     *  @throws com.sun.grid.grm.GrmException if the editor could not be started
     */
    public boolean edit(File file) throws GrmException {
        
        if(file == null) {
            throw new NullPointerException("file must not be null");
        }
        if(!file.canWrite()) {
            throw new GrmException("unixplatform.fileNotWritable", BUNDLE, file);
        }
        
        switch(nativeEdit(file.getAbsolutePath())) {
            case 0: return true;
            case 1: return false;
            default:
                throw new GrmException("unixplatform.error.editor", BUNDLE);
        }
    }    
    
    
    private native int nativeEdit(String filename);

    private native void initTerminal();
    private native void resetTerminal();
    
    private class ShutdownHook extends Thread {

        @Override
        public void run() {
            resetTerminal();
        }
    }
    
    /**
     * Check if process exists for a given pid number
     * @param pid the id of the process to check
     * @return <code>true</code> if process exists, there was problem with 
     * reading of processes or exception was caught, otherwise <code>false</code>
     */
    public boolean existsProcess(int pid) {
        String processPID = String.valueOf(pid);
        File procDir = new File("/proc", processPID);
        if (procDir.exists()) {
            return true;
        }
        return false;
    }
    
}
