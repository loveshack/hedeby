/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;


/**
 * Printer with indent.
 *
 * Can be used as a <code>PrintWriter</code>, has the possibility to indent
 * the output:
 *
 * <h3>Example</h3>
 *
 * <h4>Source code:</h4>
 * <pre>
 *   Printer p = new Printer(System.out, 3);
 *
 *   p.setIndent(3);
 *
 *   p.println("---------");
 *   p.indent();
 *   p.println("------");
 *   p.deindent();
 *   p.println("---------");
 * </pre>
 *
 * <h4>Output:</h4>
 *
 * <pre>
 *  ---------
 *     ------
 *  ---------
 * </pre>
 *
 */
public class Printer {

    /** Default indent is four. */
    public static final int DEFAULT_DEPTH = 4;

    /** Default indent char is a blank. */
    public static final int DEFAULT_INDENT_CHAR = ' ';
    private PrintWriter pw;
    private final StringBuffer indent = new StringBuffer();
    private boolean needIndent;
    private final StringBuffer space = new StringBuffer();

    private char indentChar = DEFAULT_INDENT_CHAR;

    /**
     * Creates a print which prints into a file.
     * @param file  the file
     * @throws java.io.IOException if the file can not be opened for writting
     */
    public Printer(final File file) throws IOException {
        this(file, false);
    }

    /**
     * Creates a print which prints into a file.
     *
     * @param file    the file
     * @param append  if <code>true</code> open the file in append mode
     * @throws java.io.IOException if the file can not be opened for writting
     */
    public Printer(final File file, final boolean append) throws IOException {

        File parent = file.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }
        FileWriter fw = new FileWriter(file, append);
        pw = new PrintWriter(fw);
        setDepth(DEFAULT_DEPTH);
    }

    /**
     *  Create a printer which prints to <code>System.out</code>.
     */
    public Printer() {
        this(System.out);
    }

    /**
     *  Creates a printer which prints into a <code>OutputStream</code>.
     *
     *  @param out the <code>OutputStream</code>
     */
    public Printer(final OutputStream out) {
        this(new OutputStreamWriter(out));
    }

    /**
     *  Creates a printer which prints into a <code>Writer</code>.
     *
     *  @param writer the <code>Writer</code>
     */
    public Printer(final Writer writer) {
        this(new PrintWriter(writer));
    }

    /**
     *  Creates a printer which prints into a <code>PrintWriter</code>.
     *
     *  @param pw the <code>PrintWriter</code>
     */
    public Printer(final PrintWriter pw) {
        this.pw = pw;
        setDepth(DEFAULT_DEPTH);
    }


    /**
     * Print the content of a file.
     *
     * @param file  the file
     * @throws java.io.IOException if the file can not be opened for reading
     */
    public final void printFile(final File file) throws IOException {

        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);

        String line = null;
        while ((line = br.readLine()) != null) {
            println(line);
        }
        flush();
    }
    
    /**
     * print a formatted string
     * @param format the format string
     * @param args arguments for the format string
     */
    public final void printf(String format, Object ... args) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.printf(format, args);
    }

    /**
     *  Print the string representation of an object.
     *
     *  @param obj the object
     */
    public final void print(final Object obj) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.print(obj);
    }

    /**
     *  Print a character.
     *
     *  @param c  the character
     */
    public final void print(final char c) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.print(c);
    }

    /**
     *  Print an integer.
     *
     *  @param i  the integer
     */
    public final void print(final int i) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.print(i);
    }

    /**
     *  Print a boolean.
     *
     *  @param b  the boolean
     */
    public final void print(final boolean b) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.print(b);
    }

    /**
     *  Print a long.
     *
     *  @param l  the long
     */
    public final void print(final long l) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.print(l);
    }

    /**
     *  Print the string representation of a object
     *  and then terminate the line.
     *
     *  @param obj the object
     */
    public final void println(final Object obj) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.println(obj);
        needIndent = true;
    }

    /**
     *  Print a character
     *  and then terminate the line.
     *
     *  @param c the character
     */
    public final void println(final char c) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.println(c);
        needIndent = true;
    }

    /**
     *  Print an integer
     *  and then terminate the line.
     *
     *  @param i the integer
     */
    public final void println(final int i) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.println(i);
    }

    /**
     *  Terminate the current line by writing the line separator string.
     */
    public final void println() {
        pw.println();
        needIndent = true;
    }


    /**
     *  Print a boolean
     *  and then terminate the line.
     *
     * @param b the boolean
     */
    public final void println(final boolean b) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.println(b);
    }

    /**
     *  Print a long
     *  and then terminate the line.
     *
     * @param l the long
     */
    public final void println(final long l) {
        if (needIndent) {
            pw.print(indent);
            needIndent = false;
        }
        pw.println(l);
    }

    /**
     *  add an indent.
     */
    public final void indent() {
        indent.append(space);
    }

    /**
     *  remove an indent.
     */
    public final void deindent() {
        indent.setLength(indent.length() - space.length());
    }

    /**
     *  Flush the buffers of the printer.
     */
    public final void flush() {
        pw.flush();
    }

    /**
     *  Close the printer.
     */
    public final void close() {
        pw.close();
    }


    /**
     * get the indent depth.
     *
     * @return the indent depth
     */
    public final int getDepth() {
        return space.length();
    }

    /**
     * set the indent depth.
     *
     * @param depth the indent depth
     */
    public final void setDepth(final int depth) {
        this.space.setLength(0);
        for (int i = 0; i < depth; i++) {
            space.append(getIndentChar());
        }
    }

    /**
     * get the indent character.
     *
     * @return the indent character
     */
    public final char getIndentChar() {
        return indentChar;
    }

    /**
     * set the indent character.
     *
     * @param indentChar  the indent character
     */
    public final void setIndentChar(final char indentChar) {
        this.indentChar = indentChar;
    }

}
