/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Utility class for file handling.
 *
 */
public class FileUtil {
    
    
    /**
     * Read the content of a file into a string
     * @param file the file
     * @throws java.io.IOException if the file can not be read
     * @return return the content of the file
     */
    public static String read(File file) throws IOException {
        if (file.length() >= Integer.MAX_VALUE) {
            throw new IOException("file " + file + " to large");
        }
        char[] table =new char[(int)file.length()];
        FileReader read = new FileReader(file);
        try {
            read.read(table);
        } finally {
            read.close();
        }
        return new String(table);
    }
    
    /**
     * Reads the content of <tt>src</tt>, replaces all patterns defined in <tt>patterns</tt> and
     * and returns the result.
     * @param src  the source file
     * @param patterns man with the patterns
     * @return the content of <tt>src</tt> with all patterns replaced
     * @throws java.io.IOException on any I/O error
     */
    public static String readAndReplace(File src, Map<String,String> patterns) throws IOException {
        StringWriter sw = new StringWriter();
        readAndReplace(src, sw, patterns);
        // Closing the StringWriter not neccessary
        return sw.toString();
    }

    /**
     * Reads the content of <tt>src</tt>, replaces all patterns defined in <tt>patterns</tt> and
     * writes the result to <tt>wr</tt>
     * @param src  the source file
     * @param wr   the target writer, this method will not close the writer
     * @param patterns man with the patterns
     * @throws java.io.IOException on any I/O error
     */
    public static void readAndReplace(File src, Writer wr, Map<String,String> patterns) throws IOException {
        
        BufferedReader br = new BufferedReader(new FileReader(src));
        try {
            String line = null;
            StringBuilder buf = new StringBuilder();
            PrintWriter pw = new PrintWriter(wr);
            while((line = br.readLine()) != null) {
                buf.setLength(0);
                buf.append(line);
                for(String key: patterns.keySet()) {
                    int start = buf.indexOf(key);
                    if(start >= 0) {
                        buf.replace(start, start + key.length(), patterns.get(key));
                    }
                }
                pw.println(buf.toString());
            }
            pw.flush();
        } finally {
            br.close();
        }
    }
    /**
     * Write a string into a file
     * @param str   the string
     * @param file  the file
     * @throws java.io.IOException on any IO error
     */
    public static void write(String str, File file) throws IOException {
        
        FileWriter fw  = new FileWriter(file);
        try {
            fw.write(str);
        } finally {
            fw.close();
        }
    }
    
    /**
     * Replace strings in a file
     * @param src      the source file
     * @param target   the target file
     * @param patterns map with the string which should be replaced as key
     *                 and the replacements as values.
     * @throws java.io.IOException on any io error
     */
    public static void replace(File src, File target, Map<String,String> patterns) throws IOException {
        
        FileWriter wr = new FileWriter(target);
        try {
            readAndReplace(src, wr, patterns);
        } finally {
            wr.close();
        }
    }
    
    /**
     * Convert a byte array into a hex string
     * @param bytes  the byte array
     * @return the hex string
     */
    public static String byteArrayToHexString(byte [] bytes) {
        
        byte ch = 0x00;
        
        int i = 0;
        
        if (bytes == null || bytes.length <= 0) {
            return null;
        }
        
        char chars[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        while (i < bytes.length) {
            
            ch = (byte) (bytes[i] & 0xF0);
            ch = (byte) (ch >>> 4);
            ch = (byte) (ch & 0x0F);
            pw.print(chars[ (int) ch]);
            ch = (byte) (bytes[i] & 0x0F);
            pw.print(chars[ (int) ch]);
            i++;
        }
        pw.close();
        return sw.getBuffer().toString();
    }
    
    private static byte charToNibble(char c) {
        switch(c) {
            case '0': return 0;
            case '1': return 1;
            case '2': return 2;
            case '3': return 3;
            case '4': return 4;
            case '5': return 5;
            case '6': return 6;
            case '7': return 7;
            case '8': return 8;
            case '9': return 9;
            case 'A': return 10;
            case 'B': return 11;
            case 'C': return 12;
            case 'D': return 13;
            case 'E': return 14;
            case 'F': return 15;
            default:
                throw new IllegalArgumentException("Invalid character '" + c + "'");
        }        
    }
    
    /**
     * Convert a string containing only hex decimal digits into
     * a byte array
     * @param s  the string
     * @return  the byte array
     */
    public static byte[] hexStringToByteArray(String s) {
        
        StringBuilder buf = new StringBuilder(s.length());
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch(c) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                    buf.append(c);
                    break;
                default:
                    // Skip whitespaces
                    continue;
            }        
        }
        
        byte [] ret = new byte[buf.length()/2];
        
        for(int i = 0; i < ret.length; i++) {
            int index = i*2;
            char c = buf.charAt(index);            
            char c1 = buf.charAt(index+1);
            ret[i] = (byte)((charToNibble(c) << 4) | charToNibble(c1));
        }
        return ret;
    }
    
    /**
     * Check if dir2 is dir1 or one of its subdirectories
     * @param dir1 - first directory for checking
     * @param dir2 - second directory for checking
     * @return true if first directory is equals to the second one or 
     * the second directory is the subdirectory of the first directory, 
     * otherwise false
     */
    public static boolean isSubDirectory(File dir1, File dir2) {
       
        while (dir2 != null) {
            if (dir1.equals(dir2)) {
                return true;
            }
            dir2 = dir2.getParentFile();
        }
        return false;
    }
    /**
     * Read the content of a file into a list of Strings, line break is the delimeter
     * @param file the file to read
     * @throws java.io.IOException if the file can not be read
     * @throws java.io.FileNotFoundException if file not found
     * @return return the content of the file in a list of strings,
     */
    public static List<String> readToList(File file) throws FileNotFoundException, IOException {
        List<String> ret = new ArrayList<String>();
        BufferedReader in =  new BufferedReader(new FileReader(file));
        try {
            String tmp = null; //not declared within while loop
            while (( tmp = in.readLine()) != null){
                ret.add(tmp);
            }
        } finally {
            in.close();
        }
        return ret;
    }
     /**
     * Write a content of input stream to the file
     * @param stdin InputStream to be read from
     * @param file file to which the content should be written
     * @throws java.io.IOException if the file can not be written
     */
    public static void  writeInputToFile(InputStream stdin, File file) throws IOException {
        if (stdin != null) {
            Writer writer = new BufferedWriter(new FileWriter(file));
            try {
                byte[] buf = new byte[1024];
                int len = 0;
                while ((len = stdin.read(buf)) > 0) {
                    char[] tmp = new char[len];
                    for (int i = 0; i < len; i++) {
                        tmp[i] = (char) buf[i];
                    }
                    writer.write(tmp, 0, len);
                    tmp = null;
                }
            } finally {
                writer.close();
            }
        }
    }
    /**
     * Returns a content of a file that is stored in a jar file as a String.
     * @param clazz class from a jar from where you want to read a file
     * @param name of the file in the jar
     * @return Content of a file as a String
     * @throws java.io.IOException when problem with reading a file occurs
     */
    public static String readResourceAsString(Class clazz, String name) throws IOException {
         InputStream in = clazz.getResourceAsStream(name);
         if (in == null) {
             throw new IOException("resource '" + name + "' not found");
         }
         try {
             Reader rd = new InputStreamReader(in);
             char buf [] = new char[1024];
             int len = 0;
             StringBuilder result = new StringBuilder();

             while ( (len=rd.read(buf)) > 0 ) {
                 result.append(buf, 0, len);
             }
             return result.toString();
         } finally {
             in.close();
         }
     } 
}
