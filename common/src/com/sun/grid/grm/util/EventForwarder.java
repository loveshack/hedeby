/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * An instance of this class can be used event listener. The instance itself
 * has also event listeners.
 * 
 * <p>Any call of an event listener method of the event listener interface is forwarder
 * this any registered event listeners.</p>
 * 
 * <p>This class supports synchron and asynchron event delivery. In the synchron
 *    case the event are directly delivered with the calling thread. In the
 *    asynchron case the event are delivered in a separete thread</p>
 * 
 * @param <T> The event listener interface
 */
public class EventForwarder<T> {

    private final T proxy;
    private final EventListenerSupport<T> eventSupport;
    private final ForwardInvocationHandler invocationHandler = new ForwardInvocationHandler();

    @SuppressWarnings("unchecked")
    private EventForwarder(Class<T> listenerInterface, boolean synchron) {
        proxy = (T) Proxy.newProxyInstance(listenerInterface.getClassLoader(),
                new Class<?>[]{listenerInterface}, invocationHandler);

        if (synchron) {
            eventSupport = EventListenerSupport.newSynchronInstance(listenerInterface);
        } else {
            eventSupport = EventListenerSupport.newAsynchronInstance(listenerInterface);
        }
    }

    /**
     * Create a new synchron instance of the EventForwarder.
     * 
     * @param listenerInterface the listener interface
     * @return <T> the synchron instanceof of <code>EventForwarder</code>
     */
    public static <T> EventForwarder<T> newSynchronInstance(Class<T> listenerInterface) {
        return new EventForwarder<T>(listenerInterface, true);
    }

    /**
     * Create a new asynchron instance of the <code>EventForwarder</code>.
     * 
     * @param listenerInterface the listener interface
     * @return <T> the asynchron instanceof of <code>EventForwarder</code>
     */
    public static <T> EventForwarder<T> newAsynchronInstance(Class<T> listenerInterface) {
        return new EventForwarder<T>(listenerInterface, false);
    }

    /**
     * Get the dynamic proxy. 
     * 
     * Any method call on this proxy call the corresponding event listener method
     * of the registered event listener
     * @return the 
     */
    public T getProxy() {
        return proxy;
    }

    /**
     * Add a new event listener
     * @param lis the event listener
     */
    public void addListener(T lis) {
        eventSupport.addListener(lis);
    }

    /**
     * Remove an event listener.
     * @param lis the event listener
     */
    public void removeListener(T lis) {
        eventSupport.removeListener(lis);
    }

    private class ForwardInvocationHandler implements InvocationHandler {

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.getName().equals("hashCode")) {
                return this.hashCode();
            } else if (method.getName().equals("equals")) {
                return this.equals(args[0]);
            } else {
                return method.invoke(eventSupport.getProxy(), args);
            }
        }
    }
}
