/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Generic class for event listener support.
 * 
 * <p>The event listener support can work in two modes:</p>
 * 
 * <p>In synchron mode the events are delivered in the calles thread</p>
 * <p>In the asynchron mode the events are stored in a event queue. A static
 *    thread pool delivers the event. The event queue guarantees that the events
 *    are delivered in the correct order</p>
 * 
 * @param <T> the event listener interface
 */
public class EventListenerSupport<T> implements InvocationHandler {

    public static final String BUNDLE = "com.sun.grid.grm.util.util";
    private final static Logger log = Logger.getLogger(EventListenerSupport.class.getName(), BUNDLE);
    private final T proxy;
    private Set<T> listeners = Collections.emptySet();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final static int CORE_THREAD_COUNT = 1;
    private final static int MAX_THREAD_COUNT = 3;
    private final static ThreadFactory THREAD_FACTORY = new ThreadFactory() {

        private final AtomicInteger instanceCount = new AtomicInteger(0);

        public Thread newThread(Runnable r) {
            Thread ret = new Thread(r, String.format("event_support[%d]", instanceCount.getAndIncrement()));
            return ret;
        }
    };
    private final static ExecutorService executor = new ThreadPoolExecutor(CORE_THREAD_COUNT, MAX_THREAD_COUNT, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), THREAD_FACTORY);
    private final EventProcessor eventProcessor;

    /**
     * Create a new <code>EventListenerSupport</code> instance
     * @param listenerInterface the event listener interface
     * @param synchron if set to true events will be delivered synchron
     */
    @SuppressWarnings("unchecked")
    private EventListenerSupport(Class<T> listenerInterface, EventProcessor processor) {
        proxy = (T) Proxy.newProxyInstance(listenerInterface.getClassLoader(),
                new Class<?>[]{listenerInterface}, this);
        this.eventProcessor = processor;
    }
    
    /**
     * Create a new <code>EventListenerSupport</code> instance
     * @param listenerInterface the event listener interface
     * @param synchron if set to true events will be delivered synchron
     */
    @SuppressWarnings("unchecked")
    private EventListenerSupport(Class<T> listenerInterface, boolean synchron) {
        this(listenerInterface, new Worker(synchron));
    }
    
    
    /**
     * Get the event processor
     * @return the event processor
     */
    public EventProcessor getEventProcessor() {
        return eventProcessor;
    }
    
    /**
     * Get the number of pending events in the event queue
     * @return the number of pending events
     */
    public int getNumberOfPendingEvents() {
        return eventProcessor.getNumberOfPendingEvents();
    }

    /**
     * Create an instanceof EventListenerSupport with synchron event delivery support
     * @param listenerInterface the listener interface
     * @param <T> The type of the EventListener
     * @return the new instance
     */
    public static <T> EventListenerSupport<T> newSynchronInstance(Class<T> listenerInterface) {
        return new EventListenerSupport<T>(listenerInterface, true);
    }

    /**
     * 
     * @param <T> the type of the EventListener
     * @param listenerInterface the listener interface
     * @param processor the event processs
     * @return the event support
     */
    public static <T> EventListenerSupport<T> newInstance(Class<T> listenerInterface, EventProcessor processor) {
        return new EventListenerSupport<T>(listenerInterface, processor);
    }

    /**
     * Create an instanceof EventListenerSupport with asynchron event delivery support
     * @param <T> The type of the EventListener
     * @param listenerInterface the listener interface
     * @return the new instance
     */
    public static <T> EventListenerSupport<T> newAsynchronInstance(Class<T> listenerInterface) {
        return new EventListenerSupport<T>(listenerInterface, false);
    }
    
    /**
     * Get the synchron flag.
     * 
     * @return returns <code>true</code> if the events are delivered synchron
     */
    public boolean isSynchron() {
        return eventProcessor.isSynchron();
    }

    /**
     * add a new listener
     * @param listener the listener
     * @return <code>true</code> if the listener has been added
     */
    public boolean addListener(T listener) {
        boolean ret;
        lock.writeLock().lock();
        try {
            ret = !listeners.contains(listener);
            if (ret) {
                Set<T> newListeners = new HashSet<T>(listeners.size() + 1);
                newListeners.addAll(listeners);
                newListeners.add(listener);
                listeners = newListeners;                
            }
        } finally {
            lock.writeLock().unlock();
        }
        return ret;
    }

    /**
     * remove a listener
     * @param listener the listener
     * @return <code>true</code> if the listener has been removed
     */
    public boolean removeListener(T listener) {
        boolean ret = false;
        lock.writeLock().lock();
        try {
            ret = listeners.contains(listener);
            if (ret) {
                Set<T> newListeners = new HashSet<T>(listeners);
                newListeners.remove(listener);
                listeners = newListeners;
            }
        } finally {
            lock.writeLock().unlock();
        }
        return ret;
    }

    /**
     * Determine if listeners are registered
     * @return <code>true</code> if no listener is registered
     */
    public boolean hasListener() {
        lock.readLock().lock();
        try {
            return !listeners.isEmpty();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get the proxy for the event lister.
     *
     * This proxy can be used to propagate an
     * event to all registered listeners
     *
     * @return the proxy
     */
    public T getProxy() {
        return proxy;
    }

    /**
     * Iterates over all listeners and invokes an method of the listeners.
     * @param proxy   the proxy
     * @param method  the method
     * @param args    argument for the method
     * @throws java.lang.Throwable
     * @return always null
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Set<T> tmp;

        if (method.getDeclaringClass().equals(Object.class)) {
            if(method.getName().equals("toString")) {
                return "";
            } else if (method.getName().equals("equals")) {
                return false;
            } else if (method.getName().equals("hashcode")) {
                return 0;
            } else {
                return null;
            }
        } else {
            lock.readLock().lock();
            try {
                tmp = listeners;
            } finally {
                lock.readLock().unlock();
            }
            eventProcessor.addEvent(tmp, method, args);
            return null;
        }
    }

    public interface EventProcessor extends Runnable {
        
        public void addEvent(Set listeners, Method method, Object[] args);
        public boolean isSynchron();
        
        /**
         * Get the number of pending events in the event queue
         * @return the number of pending events
         */
        public int getNumberOfPendingEvents();
        
    }
    
    /**
     * The Worker guarantees that the event delivered in the correct order (FIFO)
     */
    public static class Worker implements EventProcessor {

        private final Queue<Event> pendingEvents = new LinkedList<Event>();
        private Future future;
        private boolean synchron;
        
        public Worker(boolean synchron) {
            this.synchron = synchron;
        }
        
        public boolean isSynchron() {
            return synchron;
        }
        
        private void deliver(Set listeners, Method method, Object[] args) {
            boolean logIt = log.isLoggable(Level.FINE);
            for (Object lis : listeners) {
                try {                    
                    if (lis == null) {
                         log.log(Level.WARNING, "els.null.listener");   
                    } else {
                        if (logIt) {                    
                            log.log(Level.FINE, "els.delivering", new Object[]{method.getName(), lis, Arrays.toString(args)});
                        }
                        method.invoke(lis, args);
                        if (logIt) {                    
                            log.log(Level.FINE, "els.deliver", new Object[]{method.getName(), lis, Arrays.toString(args)});
                        }
                    }
                } catch (Exception ex) {
                    Throwable t = ex;
                    if (ex instanceof InvocationTargetException) {
                        t = ((InvocationTargetException) ex).getTargetException();
                    }
                    LogRecord lr = new LogRecord(Level.SEVERE, "els.ex.deliver");
                    if (t.getLocalizedMessage() != null) {
                        lr.setParameters(new Object[]{t.getLocalizedMessage()});
                    } else if (ex.getMessage() != null) {
                        lr.setParameters(new Object[]{t.getMessage()});
                    } else {
                        lr.setParameters(new Object[]{t});
                    }
                    lr.setThrown(t);
                    lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                    log.log(lr);
                }
            }
        }
        
        /**
         * Get the number of pending events in the event queue
         * @return the number of pending events
         */
        public int getNumberOfPendingEvents() {
            synchronized(pendingEvents) {
                return pendingEvents.size();
            }
        }
        

        /**
         * Add an event to the worker. 
         * 
         * <p>In synchron mode the event is delivered directly. In asynchron mode
         *    the worker is submited to the thread pool (only if it is not active).
         * 
         * @param listeners  the listeners of the event
         * @param method     the event notification method
         * @param args       the arguments for the event notification method
         */
        public void addEvent(Set listeners, Method method, Object[] args) {
            if (synchron) {
                deliver(listeners, method, args);
            } else {
                synchronized (pendingEvents) {
                    pendingEvents.add(new Event(listeners, method, args));
                    if (future == null) {
                        // The worker is currently not active in the thread pool
                        // => resubmit the worker
                        try {
                            future = executor.submit(this);
                        } catch (RejectedExecutionException ex) {
                            log.log(Level.WARNING, "els.ex.async", ex);
                        }
                    }
                }
            }
        }

        /**
         * Process the next pending event. If further events are pending
         * resubmit the worker into the thread pool
         * 
         * This method is only used in asynchron mode
         */
        public void run() {
            Event event = null;
            synchronized (pendingEvents) {
                event = pendingEvents.peek();
            }
            try {
                if (event != null) {
                    deliver(event.listeners, event.method, event.args);
                }
            } finally {
                synchronized (pendingEvents) {
                    // Remove the events from the queue
                    if (event != null) {
                        pendingEvents.poll();
                    }
                    if (pendingEvents.isEmpty()) {
                        // No pending events are available
                        // Worker can stop working
                        future = null;
                    } else {
                        // We have pending events. Resubmit the worker
                        // The next call of the run method will deliver
                        // the next event
                        try {
                            future = executor.submit(this);
                        } catch (RejectedExecutionException ex) {
                            future = null;
                            log.log(Level.WARNING, "els.ex.async", ex);
                        }
                    }
                }
            }
        }
    }

    private static class Event {

        private final Set listeners;
        private final Method method;
        private final Object[] args;

        public Event(Set listeners, Method method, Object[] args) {
            this.listeners = listeners;
            this.method = method;
            this.args = args;
        }
    }

    /**
     * Shutdown the async event delivery thread
     */
    public static void shutdown() {
        executor.shutdownNow();
    }
}