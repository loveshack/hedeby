/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.net.*;

/**
 * Wraps a set of InetAddress functions. 
 * 
 * <p>It is needed to test the system behavior in network error cases that are 
 * hard to setup otherwise. (eg. hostname renamed and not resolvable any more etc.) </p>
 */
public class InetAddressWrapper {

    /**
     * @return an InetAddress object that represents the localhost 
     * @throws java.net.UnknownHostException if the localhost address could not be obtained
     * @see  java.net.InetAddress
     */
    public InetAddress getLocalHost() throws UnknownHostException {
        return InetAddress.getLocalHost();
    }

    /**
     * @param host is a String identifier of the host (eg. hostname or ip address)
     * @return an InetAddress object that represents the host 
     * @throws java.net.UnknownHostException if the address of the host could not be resolved
     * @see  java.net.InetAddress
     */
    public InetAddress getByName(String host) throws UnknownHostException {
        return InetAddress.getByName(host);
    }

    /**
     * @param host is a String identifier of the host (eg. hostname or ip address)
     * @return an array of InetAddress objects that represents the host 
     * @throws java.net.UnknownHostException if the address of the host could not be resolved
     * @see  java.net.InetAddress
     */
    public InetAddress[] getAllByName(String host) throws UnknownHostException {
        return InetAddress.getAllByName(host);
    }
}
