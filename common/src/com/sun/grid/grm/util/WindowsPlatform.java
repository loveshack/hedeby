/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import com.sun.grid.grm.GrmException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Logger;

/**
 * Implementation of the platform specific actions on windows.
 *
 */
public class WindowsPlatform extends Platform {

    private static final String BUNDLE = "com.sun.grid.grm.util.util";
    private static final Logger LOGGER = Logger.getLogger(WindowsPlatform.class.getName(), BUNDLE);
    private static final ResourceBundle RB = LOGGER.getResourceBundle();
    
    private String buildWindowsCommand(String command) {
        String strSystemRoot;
        String strSystem32_Posix = "\\system32\\CMD.EXE";
        String strCommand;
        
        strSystemRoot = System.getenv("SYSTEMROOT");
        strCommand = strSystemRoot + strSystem32_Posix + " /c " + command;
        
        return strCommand;
    }

    private String buildPosixCommand(String command) {
        String strSystemRoot;
        String strSystem32_Posix = "\\system32\\POSIX.EXE";
        String strCommand;
        
        strSystemRoot = System.getenv("SYSTEMROOT");
        strCommand = strSystemRoot + strSystem32_Posix + " /u /c " + command;
        
        return strCommand;
    }
    
    /** Creates a new instance of WindowsPlatform */
    public WindowsPlatform() {
    }
    
    public void removeDir(File dir, boolean force) throws IOException {
        System.loadLibrary("platform");
        NativeRemoveDir(dir.getAbsolutePath(), force);
    }
    private native boolean NativeRemoveDir(String strDirPath, boolean force);
    
    private void writeShellScript(String command, String [] env, File file) throws IOException {
        Printer p = new Printer(file);
        try {
            p.println("@echo off");
            p.println("REM Windows batch file");

            if(env != null) {
                for(String envVar: env) {
                    int index = envVar.indexOf('=');
                    String name = envVar.substring(0, index);
                    String value = envVar.substring(index+1, envVar.length());
                    p.print("set ");
                    p.print(name);
                    p.print("=");
                    p.println(value);
                }
            }
            p.println(command);

            p.println("exit /b %ERRORLEVEL%");
        } finally {
            p.close();
        }
    }

    @Override
    public void copy(File file, File targetFile, boolean recursive, boolean preserve) throws IOException, InterruptedException {
        throw new UnsupportedOperationException("Windows copy not supported yet.");
    }

    @Override
    public void move(File file, File targetFile) throws IOException, InterruptedException {
        throw new UnsupportedOperationException("Windows move not supported yet.");
    }

    enum enType {
        typeInterixScript,
        typeBatchScript,
        typeWindowsCommand
    };
    
    private enType DetectType(String strCommand) {
        // Get file ending
        return enType.typeInterixScript;
    }
   

    /*
     * This function is only for test purposes and will be replaced
     * by a real password 'handler' later.
     */
    private String ReadUserPassword(String strUser) {
        StringTokenizer strTokens;
        String          strLine;
        String          strPasswd = "";
        int             idx;
        BufferedReader  PasswdFile;
        
        try {
            PasswdFile = new BufferedReader(new FileReader("passwd"));
        
            while((strLine = PasswdFile.readLine())!=null) {
                strTokens = new StringTokenizer(strLine);
                if(strUser.compareTo(strTokens.nextToken("="))==0) {
                    strPasswd = strTokens.nextToken("=");
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return strPasswd;
    }

    private int privateExecAs(boolean bBlocking, String user, 
                              String command, String[] env,
                              File dir, List<String> stdout, List<String> stderr, int timeout)
                              throws IOException, InterruptedException {
        int    ret         = 0;
        enType eType       = DetectType(command);
        String strPassword = ReadUserPassword(user);
        
        System.loadLibrary("platform");        
        
        switch(eType) {
            case typeInterixScript:
                break;
            case typeBatchScript:
                break;
            case typeWindowsCommand:
                File shellscript = File.createTempFile("command", ".bat");        
                try {            
                    writeShellScript(command, env, shellscript);

                    if (user == null || user.equals(System.getProperty("user.name"))) {
                        // ExecAs current user
                        ret = exec(shellscript.getAbsolutePath(), null, dir, stdout, stderr, timeout);
                    } else {
                        // ExecAs different user
//                        chown(shellscript, user);
                        ret = NativeExecAs(bBlocking, shellscript.getAbsolutePath(), null, dir,
                                           stdout, stderr,
                                           user, strPassword);
                    }
                } finally {
                    shellscript.delete();
                }
                break;
            default:
                // Executable
        }
        return ret;
    }
    private native int NativeExecAs(boolean bBlocking, 
                                    String strCommand, String[] env, File dir,
                                    List<String> stdout, List<String> stderr,
                                    String strUser, String strPassword);
    
    // exec/execAs accepts a Interix shell skript (ending .sh or .csh),
    // a Windows batch file (ending .bat or .cmd) or a Windows command (echo, dir, ...)
    public int execAs(String user, String command, String[] env,
                      File dir, List<String> stdout, List<String> stderr, int timeout)
                      throws IOException, InterruptedException {
        return privateExecAs(true, user, command, env, dir, stdout, stderr, timeout);
    }
    

    
    @Override
    public int exec(final String command, final String[] env,
            final File dir, final List<String> stdout,
            final List<String> stderr, int timeout)
            throws IOException, InterruptedException {
        return super.exec(buildWindowsCommand(command), env, dir, stdout, stderr, timeout);
    }
    
    
    public void chown(File file, String owner, boolean recursive) throws IOException, InterruptedException {
/*       
        System.loadLibrary("platform");
        String strError = NativeChown(file.getAbsolutePath(), owner, recursive);
        if(strError.length()>0) {
            System.out.println(strError);
            assert(strError.length()==0);
        }
 */
    }
/*    
    private native String NativeChown(String strPath, String owner, boolean recursive);
*/    
    public void chmod(File file, String mode, boolean recursive) throws IOException, InterruptedException {
        // @todo: add logging "windowsplatform.chmod.notsupported"
    }

    @Override
    public String getFileOwner(File file) throws IOException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private native boolean NativeCopy(String strSourcePath, String strTargetPath, boolean recursive);
    
    
    public void makeExecutable(File file) throws IOException, InterruptedException {
        // Not necessary for Windows, .exes are always executable
        // throw new UnsupportedOperationException(RB.getString("windowsplatform.makeExecutable.notsupported"));
    }
    
    public void link(File file, File link) throws IOException, InterruptedException {
        // Symbolic links are not possible in Windows
        throw new UnsupportedOperationException(RB.getString("windowsplatform.link.notsupported"));
    }
    
    /**
     *  Get the gridengine architecture of the local host.
     *
     * @param hedebyDist path to the gridengine distribution
     * @return always <code>win-x86</code>
     */
    public String getArch(File hedebyDist) {
        return "win32-x86";
    }
    
    /**
     * Get the pid of the jvm.
     *
     * @return the pid of the jvm
     */
    public int getPid() {
        System.loadLibrary("platform");
        return getNativePid();
    }
    private native int getNativePid();
    
    /**
     * Determine if the jvm is running as super user
     * @return <code>true</code> if the jvm is running as super user
     */
    public boolean isSuperUser() {
        System.loadLibrary("platform");
        return isNativeSuperUser();
    }
    private native boolean isNativeSuperUser();

    /**
     * Send a process the kill signal.
     *
     * @param  pid  id of the proccess
     * @return <code>true</code> if the send signal has been delivered
     */
    @Override
    public boolean killProcess(int pid) {
        throw new UnsupportedOperationException("killProcess yet not implemented");
    }

    @Override
    public boolean terminateProcess(int pid) {
        throw new UnsupportedOperationException("killProcess yet not implemented");
    }
    
    @Override
    public boolean edit(File file) throws GrmException {
        throw new UnsupportedOperationException("edit yet not implemented");
    }
    
    public int execute(final String command, final String[] env,
            final File dir, final List<String> stdout,
            final List<String> stderr, InputStream stdin, int timeout)
            throws IOException, InterruptedException {
            throw new UnsupportedOperationException("execute yet not implemented");
    }

    /**
     * Check if process exists for a given pid number
     * @param pid the id of the process to check
     * @return <code>true</code> if process exists, there was problem with 
     * reading of processes or exception was caught, otherwise <code>false</code>
     */
    public boolean existsProcess(int pid) {
        System.loadLibrary("platform");
        return existsNativeProcess(pid);
    }
    private native boolean existsNativeProcess(int i);

}
