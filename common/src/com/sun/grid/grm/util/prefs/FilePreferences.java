/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.prefs;

import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileLock;
import java.nio.charset.Charset;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 *
 */
public class FilePreferences extends AbstractPreferences {

    private final static String BUNDLE = "com.sun.grid.grm.util.prefs.messages";
    private final static Logger log = Logger.getLogger(FilePreferences.class.getName(), BUNDLE);
    private static final String[] EMPTY_STRING_ARRAY = new String[0];
    /**
     * Sync interval in seconds.
     */
    private static final int SYNC_INTERVAL = AccessController.doPrivileged(new PrivilegedAction<Integer>() {

        public Integer run() {
            String propName = "com.sun.grid.grm.util.prefs.syncInterval";

            String syncIntProp = System.getProperty(propName);
            int ret = 30;

            if (syncIntProp != null) {
                try {
                    int tmp = Integer.parseInt(syncIntProp);
                    if (tmp < 1) {
                        log.log(Level.WARNING, "syncInt.invalid", new Object[]{propName, syncIntProp});
                    } else {
                        ret = tmp;
                    }
                } catch (NumberFormatException ex) {
                    log.log(Level.WARNING, "syncInt.invalid", new Object[]{propName, syncIntProp});
                }
            }
            return ret;
        }
    });
    //default access for testing (Issue 601)
    static Timer syncTimer = new Timer(true);
    private static File systemRootDir = null;
    private static RootFilePreferences systemRoot = null;
    private static File userRootDir = null;
    private static RootFilePreferences userRoot = null;

    public static class RootFilePreferences extends FilePreferences {
        // here we use a SaveFileLock to ensure that both the file lock and the open file will be closed correctly (issue 601)
        private SafeFileLock lockFile;
        private File modFile;
        private boolean modified;
        private long modTime;
        private boolean canWrite;

        public RootFilePreferences(File dir, boolean isUser) {
            super(dir, isUser);
            if (!dir.exists()) {
                log.log(Level.FINE, "mkdir", dir);
                if (!dir.mkdirs()) {
                    log.log(Level.FINE, "mkdir.error", dir);
                }
            }
            lockFile = new SafeFileLock(new File(dir, ".lock"));
            modFile = new File(dir, ".modFile");
            canWrite = dir.canWrite();
            if (canWrite && !modFile.exists()) {
                try {
                    // create if does not exist.
                    modFile.createNewFile();
                } catch (IOException e) {
                    log.warning(e.toString());
                }
            }
            modTime = modFile.lastModified();
        }

        /**
         * @param shared
         * @return  a SafeFileLock that allows a relase with an implicit close of the file
         * @throws java.lang.SecurityException
         * @throws java.util.prefs.BackingStoreException
         */
        private SafeFileLock lockFile(boolean shared) throws SecurityException, BackingStoreException {
            try {
                if (lockFile.lock(shared)) {
                    return lockFile;
                }
            } catch (IOException e) {
            //Report it as generic locking error with the line below
            }
            // if we could not successfully executed lockFile.lock() we will throw this error!
            throw new BackingStoreException(I18NManager.formatMessage("lock.error", BUNDLE, lockFile.getPath()));
        }

        @Override
        public void flush() throws BackingStoreException {
            if (canWrite) {
                super.flush();
            }
        }
    }

    static {
        // Setup sync timer task
        syncTimer.schedule(new TimerTask() {

            public void run() {
                syncWorld();
            }
        }, SYNC_INTERVAL * 1000, SYNC_INTERVAL * 1000);

        // Register the shutdown hook
        AccessController.doPrivileged(new PrivilegedAction<Void>() {

            public Void run() {
                Runtime.getRuntime().addShutdownHook(new Thread() {

                    @Override
                    public void run() {
                        syncTimer.cancel();
                        syncWorld();
                    }
                });
                return null;
            }
        });
    }
    private final File dir;
    private final File prefsFile;
    private final File tmpFile;
    private Map<String, String> prefsCache = null;
    private long lastSyncTime = 0;
    private final List<Change> changeLog = new ArrayList<Change>();
    private NodeCreate nodeCreate = null;
    private final boolean isUserNode;

    /**
     * Get the root node of the user preferences
     * @return the root node of the user preferences
     */
    public static synchronized Preferences getUserRoot() {
        if (userRoot == null) {
            userRoot = AccessController.doPrivileged(new PrivilegedAction<RootFilePreferences>() {

                public RootFilePreferences run() {
                    if (userRootDir == null) {
                        userRootDir = new File(System.getProperty("user.home"), ".sdm" + File.separatorChar + "bootstrap");
                    }
                    return new RootFilePreferences(userRootDir, true);
                }
            });
        }
        return userRoot;
    }

    public static synchronized Preferences getSystemRoot() {
        if (systemRoot == null) {
            systemRoot = AccessController.doPrivileged(new PrivilegedAction<RootFilePreferences>() {

                public RootFilePreferences run() {
                    if (systemRootDir == null) {
                        systemRootDir = new File("/etc/sdm/bootstrap".replace('/', File.separatorChar));
                    }
                    return new RootFilePreferences(systemRootDir, false);
                }
            });
        }
        return systemRoot;
    }

    /**
     * Represents a change to a preference.
     */
    private interface Change {

        /**
         * Reapplies the change to prefsCache.
         */
        void replay();
    }

    /**
     * Represents a preference put.
     */
    private class Put implements Change {

        final String key;
        final String value;

        Put(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public void replay() {
            prefsCache.put(key, value);
        }
    }

    /**
     * Represents a preference remove.
     */
    private class Remove implements Change {

        String key;

        Remove(String key) {
            this.key = key;
        }

        public void replay() {
            prefsCache.remove(key);
        }
    }

    private class NodeCreate implements Change {

        public void replay() {
        }
    }

    /**
     * Replay changeLog against prefsCache.
     */
    private void replayChanges() {
        for (int i = 0,  n = changeLog.size(); i < n; i++) {
            changeLog.get(i).replay();
        }
    }

    // default access for testing purpose (issue 601)
    static void syncWorld() {
        /*
         * Synchronization necessary because userRoot and systemRoot are
         * lazily initialized.
         */
        Preferences userRt;
        Preferences systemRt;
        synchronized (FilePreferences.class) {
            userRt = userRoot;
            systemRt = systemRoot;
        }

        try {
            if (userRt != null) {
                userRt.flush();
            }
        } catch (BackingStoreException e) {
            LogRecord lr = new LogRecord(Level.WARNING, "sync.error.user");
            lr.setParameters(new Object[]{e.getLocalizedMessage()});
            lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
            lr.setThrown(e);
            log.log(lr);
        }

        try {
            if (systemRt != null) {
                systemRt.flush();
            }
        } catch (BackingStoreException e) {
            LogRecord lr = new LogRecord(Level.WARNING, "sync.error.system");
            lr.setParameters(new Object[]{e.getLocalizedMessage()});
            lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
            lr.setThrown(e);
            log.log(lr);
        }
    }

    protected FilePreferences(File dir, boolean user) {
        super(null, "");
        isUserNode = user;
        this.dir = dir;
        prefsFile = new File(dir, "prefs.properties");
        tmpFile = new File(dir, "prefs.tmp");
    }

    private FilePreferences(FilePreferences parent, String name) {
        super(parent, name);
        isUserNode = parent.isUserNode;
        dir = new File(parent.dir, dirName(name));
        prefsFile = new File(dir, "prefs.properties");
        tmpFile = new File(dir, "prefs.tmp");
        AccessController.doPrivileged(new PrivilegedAction<Void>() {

            public Void run() {
                newNode = !dir.exists();
                return null;
            }
        });
        if (newNode) {
            // Add an entry to the change log to ensure that
            // the next sync will create the node
            prefsCache = new TreeMap<String, String>();
            nodeCreate = new NodeCreate();
            changeLog.add(nodeCreate);
        }
    }

    @Override
    public boolean isUserNode() {
        return isUserNode;
    }

    protected void putSpi(String key, String value) {
        initCache();
        changeLog.add(new Put(key, value));
        prefsCache.put(key, value);
    }

    protected String getSpi(String key) {
        initCache();
        return prefsCache.get(key);
    }

    protected void removeSpi(String key) {
        initCache();
        changeLog.add(new Remove(key));
        prefsCache.remove(key);
    }

    private void initCache() {
        if (prefsCache == null) {
            try {
                loadCache();
            } catch (Exception e) {
                prefsCache = new TreeMap<String, String>();
            }
        }
    }

    private void loadCache() throws BackingStoreException {
        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {

                public Void run() throws BackingStoreException {
                    Properties props = new Properties();
                    long newLastSyncTime = 0;
                    try {
                        newLastSyncTime = prefsFile.lastModified();
                        FileInputStream fis = new FileInputStream(prefsFile);
                        try {
                            props.load(fis);
                        } finally {
                            fis.close();
                        }
                    } catch (FileNotFoundException e) {
                        log.log(Level.FINE, "prefs.removed", prefsFile);
                    } catch (Exception e) {
                        throw new BackingStoreException(e);
                    }
                    // Attempt succeeded; update state
                    prefsCache = new TreeMap<String, String>();
                    for (Object key : props.keySet()) {
                        String s = key.toString();
                        prefsCache.put(s, props.getProperty(s));
                    }

                    lastSyncTime = newLastSyncTime;
                    return null;
                }
            });
        } catch (PrivilegedActionException e) {
            throw (BackingStoreException) e.getException();
        }
    }

    private void writeBackCache() throws BackingStoreException {
        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {

                public Void run() throws BackingStoreException {
                    try {
                        if (!dir.exists() && !dir.mkdirs()) {
                            throw new BackingStoreException(I18NManager.formatMessage("mkdir.error", BUNDLE, dir));
                        }
                        FileOutputStream fos = new FileOutputStream(tmpFile);
                        try {
                            Properties props = new Properties();
                            props.putAll(prefsCache);
                            props.store(fos, null);
                        } finally {
                            fos.close();
                        }
                        if (!tmpFile.renameTo(prefsFile)) {
                            throw new BackingStoreException(I18NManager.formatMessage("mv.error", BUNDLE, tmpFile, prefsFile));
                        }
                    } catch (Exception e) {
                        if (e instanceof BackingStoreException) {
                            throw (BackingStoreException) e;
                        }
                        throw new BackingStoreException(e);
                    }
                    return null;
                }
            });
        } catch (PrivilegedActionException e) {
            throw (BackingStoreException) e.getException();
        }
    }

    protected String[] keysSpi() {
        initCache();
        return prefsCache.keySet().toArray(new String[prefsCache.size()]);
    }

    protected String[] childrenNamesSpi() {
        return AccessController.doPrivileged(new PrivilegedAction<String[]>() {

            public String[] run() {
                List<String> result = new ArrayList<String>();
                File[] dirContents = dir.listFiles();
                if (dirContents != null) {
                    for (int i = 0; i < dirContents.length; i++) {
                        if (dirContents[i].isDirectory()) {
                            String name = nodeName(dirContents[i].getName());
                            if (log.isLoggable(Level.FINER)) {
                                log.log(Level.FINER, "subdir", new Object[]{dirContents[i], name});
                            }
                            result.add(name);
                        }
                    }
                }
                return result.toArray(EMPTY_STRING_ARRAY);
            }
            });
    }

    protected AbstractPreferences childSpi(String name) {
        return new FilePreferences(this, name);
    }

    @Override
    public void removeNode() throws BackingStoreException {
        RootFilePreferences root = isUserNode() ? userRoot : systemRoot;
        synchronized (root) {
            // to remove a node we need an exclusive lock
            // here we use a SaveFileLock to ensure that both the file lock and the open file will be closed correctly (issue 601)
            SafeFileLock fileLock = root.lockFile(false);

            try {
                super.removeNode();
            } finally {
                try {
                    fileLock.release();
                } catch (IOException ex) {
                    LogRecord lr = new LogRecord(Level.WARNING, "unlock.error");
                    lr.setParameters(new Object[]{ex.getLocalizedMessage()});
                    lr.setThrown(ex);
                    lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                    log.log(lr);
                }
            }
        }
    }

    /**
     * Called with file lock held (in addition to node locks).
     */
    protected void removeNodeSpi() throws BackingStoreException {
        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {

                public Void run() throws BackingStoreException {
                    if (!dir.exists()) {
                        return null;
                    }
                    prefsFile.delete();
                    tmpFile.delete();
                    // dir should be empty now.  If it's not, empty it
                    File[] junk = dir.listFiles();
                    if (junk.length != 0) {
                        log.log(Level.WARNING, "remove.unknown", Arrays.asList(junk));
                        for (int i = 0; i < junk.length; i++) {
                            junk[i].delete();
                        }
                    }
                    if (!dir.delete()) {
                        throw new BackingStoreException(I18NManager.formatMessage("del.error", BUNDLE, dir));
                    }
                    return null;
                }
            });
        } catch (PrivilegedActionException e) {
            throw (BackingStoreException) e.getException();
        }
    }

    @Override
    public synchronized void sync() throws BackingStoreException {
        boolean userNode = isUserNode();
        boolean shared;
        final RootFilePreferences root = userNode ? userRoot : systemRoot;

        if (userNode) {
            shared = false; /* use exclusive lock for user prefs */
        } else {
            shared = !root.canWrite;
        }

        synchronized (root) {
            // here we use a SaveFileLock to ensure that both the file lock and the open file will be closed correctly (issue 601)
            SafeFileLock fileLock = root.lockFile(shared);
            final Long newModTime = AccessController.doPrivileged(new PrivilegedAction<Long>() {

                public Long run() {
                    long nmt = root.modFile.lastModified();
                    root.modified = root.modTime == nmt;
                    return new Long(nmt);
                }
            });
            try {
                super.sync();
                AccessController.doPrivileged(new PrivilegedAction<Void>() {

                    public Void run() {
                        root.modTime = newModTime.longValue() + 1000;
                        root.modFile.setLastModified(root.modTime);
                        return null;
                    }
                });
            } finally {
                try {
                    fileLock.release();
                } catch (IOException ex) {
                    LogRecord lr = new LogRecord(Level.WARNING, "unlock.error");
                    lr.setParameters(new Object[]{ex.getLocalizedMessage()});
                    lr.setThrown(ex);
                    lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                    log.log(lr);
                }
            }
        }
    }

    protected void syncSpi() throws BackingStoreException {
        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {

                public Void run() throws BackingStoreException {
                    syncSpiPrivileged();
                    return null;
                }
            });
        } catch (PrivilegedActionException e) {
            throw (BackingStoreException) e.getException();
        }
    }

    private void syncSpiPrivileged() throws BackingStoreException {
        if (isRemoved()) {
            throw new IllegalStateException("Node has been removed");
        }
        if (prefsCache == null) {
            return;
        }
        long lastModifiedTime;

        RootFilePreferences root = isUserNode() ? userRoot : systemRoot;
        if (root.modified) {
            lastModifiedTime = prefsFile.lastModified();
            if (lastModifiedTime != lastSyncTime) {
                // External modification of this node
                // Reload the node and apply the changes
                loadCache();
                replayChanges();
                lastSyncTime = lastModifiedTime;
            }
        } else if (lastSyncTime != 0 && !dir.exists()) {
            // node has been deleted
            // apply the changes to an empty map
            prefsCache = new TreeMap<String, String>();
            replayChanges();
        }
        if (!changeLog.isEmpty()) {
            writeBackCache();
            lastModifiedTime = prefsFile.lastModified();
            if (lastSyncTime <= lastModifiedTime) {
                lastSyncTime = lastModifiedTime + 1000;
                prefsFile.setLastModified(lastSyncTime);
            }
            changeLog.clear();
        }
    }

    @Override
    public void flush() throws BackingStoreException {
        if (isRemoved()) {
            return;
        }
        sync();
    }

    protected void flushSpi() throws BackingStoreException {
    }

    private static boolean isDirChar(char ch) {
        return ch > 0x1f && ch < 0x7f && ch != '/' && ch != '.' && ch != '_' && ch != '\\' && ch != '#';
    }

    private static int encode(char ch) {
        Charset charSet = Charset.forName("UTF8");
        CharBuffer cb = CharBuffer.allocate(1);
        cb.put(ch);
        cb.position(0);
        ByteBuffer bb = charSet.encode(cb);
        switch (bb.capacity()) {
            case 1:
                return bb.get();
            case 2:
                return bb.get() | (bb.get() << 8);
            default:
                throw new IllegalStateException("encoded char has invalid length");
        }
    }

    private static char decode(int c) {
        ByteBuffer bb = ByteBuffer.allocate(2);
        byte b = (byte) ((0xff00 & c) >> 8);
        if (b != 0) {
            bb.put(b);
        }
        b = (byte) (0xff & c);
        bb.put(b);
        bb.position(0);
        Charset charSet = Charset.forName("UTF8");
        CharBuffer cb = charSet.decode(bb);
        return cb.get();
    }

    private static String dirName(String nodeName) {
        StringBuilder ret = new StringBuilder(nodeName.length());
        for (int i = 0,  n = nodeName.length(); i < n; i++) {
            char c = nodeName.charAt(i);
            if (isDirChar(c)) {
                ret.append(c);
            } else {
                ret.append('#');
                ret.append(String.format("%04x", encode(c)));
            }
        }
        return ret.toString();
    }

    private static String nodeName(String dirName) {
        StringBuffer result = new StringBuffer(dirName.length());
        for (int i = 0,  n = dirName.length(); i < n; i++) {
            char c = dirName.charAt(i);
            if (c == '#' && i + 4 < n) {
                String tmp = dirName.substring(i + 1, i + 5);
                try {
                    c = decode(Integer.parseInt(tmp, 16));
                    i += 4;
                } catch (NumberFormatException ex) {
                // ignore
                }
            }
            result.append(c);
        }
        return result.toString();
    }
}

/**
 * This class encapsulates the file lock file Release mechanism.
 * It is neccesary because a file lock consists of two object that have to be 
 * considered during lock usage:
 * 1) The random access file object that remains open while a lock is aquired
 * 2) The filelock object that represents the aquired lock.
 * 
 * A general File lock is used in 5 steps:
 * 
 * 1) open random access file
 * 2) get file lock 
 * 3) execute code that is secured by the lock
 * 4) release lock
 * 5) close random access file
 * 
 * The method SafeFileLock.lock() encapsulate steps 1-2 and the method 
 * SafeFileLock.release encapsulate steps 4-5. If SafeFileLock is used like this
 * it is ensured that no file is accidentially left open and may lead to a file
 * descriptor leakage.
 * 
 */
class SafeFileLock {

    private File fileToLock;
    private FileLock fileLock;
    private RandomAccessFile raf = null;
    private final static String BUNDLE = "com.sun.grid.grm.util.prefs.messages";
    private final static Logger log = Logger.getLogger(FilePreferences.class.getName(), BUNDLE);
    /**
     * Maximum number of attempts to get the file lock
     */
    private static int MAX_ATTEMPTS = 5;

    /**
     * Constructor
     * @param fileToLock
     */
    SafeFileLock(File fileToLock) {
        this.fileToLock = fileToLock;
    }

    /**
     * Method to test if the lock is allready aquired by this SafeFileLock
     * @return
     */
    boolean isLocked() {
        return (fileLock != null);
    }

    /**
     * This method locks a file and reports the lock  success.
     * If the lock can not be aquired, it will retry to get it 5 times.
     * The time delay between the tries is randomized and lasts at most 1 second 
     * per try.
     * @param shared If the lock is allready aquired by other processes in the shared
     * mode it can be used by this jvm, too if shared is used. Otherwise the lock is 
     * exclusive
     * @return lock success
     * @throws java.io.IOException
     * @throws java.util.prefs.BackingStoreException
     */
    boolean lock(boolean shared) throws IOException, BackingStoreException {
        if (fileLock != null) {
            return true; // this is ok as a filelock holds for the complete jvm!!
        }
        try {
            for (int i = 0; i < MAX_ATTEMPTS; i++) {
                try {
                    if (raf != null) {
                        //this should never happen, however close raf to avoid file descriptor leaks!
                        raf.close();
                        //now it can not be closed twice!
                        raf = null;
                    }
                    raf = new RandomAccessFile(fileToLock, "rw");
                } catch (IOException ex) {
                    BackingStoreException ex1 = new BackingStoreException(I18NManager.formatMessage("lock.error.access", BUNDLE, fileToLock.getPath()));
                    ex1.initCause(ex);
                    throw ex1;
                }

                fileLock = raf.getChannel().tryLock(0, raf.length(), shared);

                if (fileLock != null) {
                    //success lock could be aquired
                    return true;
                }

                try {
                    //lets wait a random time period of average 1 sec andt at most 2 sec to spread the lockrequests
                    Thread.sleep((int) (Math.random() * 1000.0));
                } catch (InterruptedException e) {
                    return false;
                }
            }
        } finally {
            // in this case no file lock could be established. However close the raf in this case!
            if (!isLocked()) {
                if (raf != null) {
                    raf.close();
                    raf = null;
                }
            }
        }
        return false;
    }

    /**
     * If the SafeFileLock is released 
     * close the lock and then the file and set both to null
     * @throws java.io.IOException
     */
    void release() throws IOException {
        if (fileLock != null) {
            fileLock.release();
            fileLock = null;
        }
        if (raf != null) {
            raf.close();
            raf = null;
        }
    }

    /**
     * @return the path of the file to be file locked!
     */
    String getPath() {
        return this.fileToLock.getAbsolutePath();
    }
}
