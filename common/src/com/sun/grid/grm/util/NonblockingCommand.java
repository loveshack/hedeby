/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Instances of this class are returned by the nonblocking
 * <code>Platform.exec</code>.
 *
 * The caller of <code>Platform.exec<code> is responseable for calling
 * the <code>cleanup</code> method of this class.
 *
 * @see com.sun.grid.grm.util.Platform#exec
 */
public class NonblockingCommand {

    private static final String BUNDLE = "com.sun.grid.grm.util.util";
    private final static Logger log = Logger.getLogger(NonblockingCommand.class.getName(), BUNDLE);
    
    private List<Runnable> cleanupHooks = new LinkedList<Runnable>();
    private Process process;
    private Integer exitStatus;
    private List<String>   stdoutMessages = Collections.synchronizedList(new LinkedList<String>());
    private List<String>   stderrMessages = Collections.synchronizedList(new LinkedList<String>());
    
    protected NonblockingCommand() {
    }
    
    /**
     * execute command and return. Do not wait for the end of the command
     * @param command   the command array
     * @param env       environment for the command
     * @param dir       working directory of the command
     * @throws java.io.IOException  if the command can not be executend
     * @return the nonblocking command
     */
    static NonblockingCommand exec(String [] command, String[] env, File dir) throws IOException {
    
        NonblockingCommand ret = new NonblockingCommand();
        
        ret.process = Runtime.getRuntime().exec(command, env, dir);
        
        
        final StreamPump stderrProcessor = new StreamPump(ret.process.getErrorStream(), ret.getStderrMessages(), "stderr", log);
        stderrProcessor.start();
        final StreamPump stdoutProcessor = new StreamPump(ret.process.getInputStream(), ret.getStdoutMessages(), "stdout", log);
        stdoutProcessor.start();
        
        ret.addCleanupHook( new Runnable() {
           
            public void run() {
               stderrProcessor.interrupt(); 
               stdoutProcessor.interrupt();
            }
        });
        
        // We do not provide a stdin to the process
        // close it
//        ret.process.getInputStream().close();
        return ret;
    }
    
    /**
     * Get stdin of the non blocking command
     * @return
     */
    public OutputStream getStdin() {
        return process.getOutputStream();
    }
    
    /**
     * Add a cleanup hook
     * @param cleanupHook the cleanup hook 
     */
    public synchronized void addCleanupHook(Runnable cleanupHook) {
        cleanupHooks.add(cleanupHook);
    }
    
    /**
     *  Run all cleanup hooks
     */
    public synchronized void cleanup() {
        for(Runnable r: cleanupHooks) {
            r.run();
        }
        cleanupHooks.clear();
    }
    
    /**
     * Wait for the end of the process
     * @throws java.lang.InterruptedException 
     */
    public void waitFor() throws InterruptedException {
        process.waitFor();
    }
    
    
    /**
     * Get the exit status of the process
     * @return <code>null</code> if the process is not yet finished
     *         else the exit status
     */
    public synchronized Integer getExitStatus() {
        if(exitStatus == null) {
            try {
                exitStatus = new Integer(process.exitValue());
            } catch(IllegalThreadStateException ex) {
                // ignore
            }
        }
        return exitStatus;
    }

    /**
     * Get the list of so far written stdout messages
     * @return list of stdout messages
     */
    public List<String> getStdoutMessages() {
        return stdoutMessages;
    }

    /**
     * Get the list of so far written stderr messages
     * @return list of stdout messages
     */
    public List<String> getStderrMessages() {
        return stderrMessages;
    }

    protected void finalize() throws Throwable {
        cleanup();
    }
    
    
}
