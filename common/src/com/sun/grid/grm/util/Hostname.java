/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Represents a hostname.
 * 
 * <P>It encapsulates and extends functionality provided by java.net.InetAddres.</P>
 * <P><b>Threadsafty:</b>The Hostname class is thread safe. This includes the static methods and  the internal 
 * Hostname cache.
 */
public class Hostname implements Serializable, Cloneable, Comparable<Hostname> {

    private static final Lock cacheLock = new ReentrantLock();
    private static final Map<String, Hostname> cache = new HashMap<String, Hostname>();
    /** If this timeout out expires the hostname is resolved again (in millis) */
    static final long RESOLVE_TIMEOUT = 10 * 60 * 1000;
    /** If a host is not resolvable not before this time out hostname is
     *  resolved again */
    static final long RESOLVE_RETRY_TIMEOUT = 60 * 1000;
    /** This lock guards the resolving
     *  @see #resolve
     */
    private final Lock lock = new ReentrantLock();
    private static Hostname localHost;
    /**
     * i18n Bundle name
     */
    static final String BUNDLE_NAME = "com.sun.grid.grm.util.util";
    private static final Logger log = Logger.getLogger(Hostname.class.getName(), BUNDLE_NAME);
    private static final long serialVersionUID = -2008090201L;
    private transient long lastResolveTryTimestamp = -1;
    private transient long lastResolveSuccessTimestamp = -1;
    /** Result of the last resolving */
    private transient InetAddress address;
    private transient boolean wroteResolveWarning;
    private transient AtomicReference<List<String>> aliases = new AtomicReference<List<String>>(Collections.<String>emptyList());
    private transient String canonicalName;
    /** hostname without resolving */
    private final String hostname;
    /**
     * This static member serves as entry point for JUnit tests. A mockup can be 
     * placed here to produce IntAdress related errors that are otherwise difficult to produce.
     */
    private static InetAddressWrapper inetAddressWrp = new InetAddressWrapper();

    /**
     * Creates a new instance of Hostname from a specified hostname
     * @param hostname  the hostname
     */
    private Hostname(String hostname) {
        this.hostname = hostname;
    //canonical Host name and Address can not be determined without explicit resolving 

    }

    private Hostname(InetAddress aAddress) {
        address = aAddress;
        hostname = address.getHostName();

        /* as here the address is given, no explicit hostname resolving is needed! 
         * thus set the object state into just resolved! This implies a explicit 
         * creation of the aliasList,too */
        lastResolveTryTimestamp = System.currentTimeMillis();
        lastResolveSuccessTimestamp = lastResolveTryTimestamp;
        updateAliasList();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        lastResolveTryTimestamp = -1;
        lastResolveSuccessTimestamp = -1;
        aliases = new AtomicReference<List<String>>(Collections.<String>emptyList());
    }

    /**
     * The method is using a cache feature.
     * If it is called the first time it tries to resolve the hostname menber
     * If it worked the host name will not be resolved for a specified period of time 
     * (eg. 10 min). The InetAddress object of the last success is returned instead.
     * If it fails to resolve the hostname it returns null. After this the host name will not 
     * be resolved for a specified period of time (eg. 1 min), too.  If resolve is called 
     * again it returns null until the time window expires. After this a new resolve 
     * cycle is posible.
     *
     * @return the InetAddress object for the host
     */
    private InetAddress resolve() {
        InetAddress ret = null;
        lock.lock();
        try {
            long now = System.currentTimeMillis();
            boolean resolve = false;

            //resolve allready failed but call is within RESOLVE_RETRY_TIMEOUT time window
            if (lastResolveTryTimestamp == -1) {
                resolve = true;
            } else {
                if (address == null) {
                    if (now > lastResolveTryTimestamp + RESOLVE_RETRY_TIMEOUT) {
                        resolve = true;
                    }
                //resolve allready worked but call after RESOLVE_TIMEOUT time window
                } else if (now > lastResolveSuccessTimestamp + RESOLVE_TIMEOUT) {
                    resolve = true;
                }
            }
            if (resolve) {
                lastResolveTryTimestamp = now;
                address = null; // this ensures that address is null if resolve fails
                try {
                    log.log(Level.FINER, "Hostname.resolve", hostname);
                    address = inetAddressWrp.getByName(hostname);
                    lastResolveSuccessTimestamp = System.currentTimeMillis();
                    wroteResolveWarning = false;
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "Hostname.resolved", new Object[]{hostname, address.getHostAddress()});
                    }
                } catch (UnknownHostException ex) {
                    if (!wroteResolveWarning) {
                        log.log(Level.WARNING, "Hostname.ex.nr", hostname);
                        wroteResolveWarning = true;
                    }
                } catch (Exception ex) {
                    //here exception can occur - runtime, when
                    //address = staticInetAddressMethods.getByName(hostname); is executed and causes
                    //method unusable when wrong hostname like ".*" for example
                    if (!wroteResolveWarning) {
                        log.log(Level.WARNING, "Hostname.ex.nr", hostname);
                        wroteResolveWarning = true;
                    }
                } finally {
                    // this call will be performed if resolve is called the first time and
                    // every time a new InetAddress is obtained
                    updateAliasList();
                }
            }
            ret = address;
        } finally {
            lock.unlock();
        }
        return ret;
    }

    private void updateAliasList() {
        List<String> tmpAliases = new ArrayList<String>(3);
        String cur = null;
        if (address != null) {
            tmpAliases.add(address.getCanonicalHostName());
            tmpAliases.add(address.getHostName());
            tmpAliases.add(address.getHostAddress());
        } else {
            tmpAliases.add(hostname);
            if (canonicalName != null) {
                tmpAliases.add(canonicalName);
            }
        }
        aliases.set(tmpAliases);
    }

    /**
     * If this Hostname obj is resolvable it will return true if
     * the host comes from loopback network.
     * @return true if this object refers to a loopback network and is
     * resolveable, false otherwise
     */
    private boolean isLoopbackHost() {
        InetAddress adr = resolve();
        if (adr != null) {
            return adr.isLoopbackAddress();
        }
        return false;
    }

    /**
     * Get an instance of Hostname
     * 
     * @param hostname the specified host
     * @return the hostname
     */
    public static Hostname getInstance(String hostname) {
        Hostname localHostnameObj = getLocalHost();
        if (hostname == null || hostname.length() == 0) {
            return localHostnameObj;
        }
        if (localHost.getHostname().toLowerCase().equals(hostname.toLowerCase())) {
            return localHostnameObj;
        }
        cacheLock.lock();
        try {
            Hostname ret = cache.get(hostname);
            if (ret == null) { // here no cache expirery is needed as the hostname is not considered to change for a host object               
                ret = new Hostname(hostname);
                // If host comes from loopback interface and is
                // resolved we will use the localHostnameObj.
                if (ret.isResolved() && ret.isLoopbackHost()) {
                    ret = localHostnameObj;
                }
                cache.put(hostname, ret);
            }
            return ret;
        } finally {
            cacheLock.unlock();
        }
    }

    /**
     * Get the hostname of the local host
     * @return hostname of the local host
     */
    public static Hostname getLocalHost() {
        cacheLock.lock();
        try {
            if (localHost == null) {
                try {
                    log.log(Level.FINER, "Hostname.resolve", "localhost");
                    localHost = new Hostname(inetAddressWrp.getLocalHost());
                    localHost.isResolved();
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "Hostname.resolved", new Object[]{localHost.getHostname(), localHost.getIpAddress()});
                    }
                } catch (UnknownHostException ex) {
                    String localizedMessage = I18NManager.formatMessage("Hostname.error.UnknownHost", BUNDLE_NAME, ex.getLocalizedMessage());
                    log.log(Level.WARNING, "Hostname.ex.nr", "localhost");
                    log.log(Level.WARNING, localizedMessage);
                    //throw new IllegalStateException(ex.getLocalizedMessage(), ex);
                    throw new IllegalStateException(localizedMessage, ex);
                }
            }
            return localHost;
        } finally {
            cacheLock.unlock();
        }
    }

    /**
     * Get the name of the host.
     *
     * @return the name of the host
     */
    public String getHostname() {
        InetAddress adr = resolve();
        if (adr != null) {
            canonicalName = adr.getCanonicalHostName();
        }
        if (canonicalName == null) {
            return hostname;
        } else {
            return canonicalName;
        }
    }

    /**
     * Is the hostname resolved
     * @return true, if hostname is resolved
     */
    public boolean isResolved() {
        return resolve() != null;
    }

    /**
     * Returns the IP address string in textual presentation.
     *
     * @return the raw IP address in a string format, null if
     * hostname is not resolved
     */
    public String getIpAddress() {
        InetAddress adr = resolve();
        if (adr != null) {
            return address.getHostAddress();
        } else {
            return null;
        }
    }

    /**
     * Returns the object as string.
     *
     * @return Object is string
     */
    @Override
    public String toString() {
        return getHostname();
    }

    /**
     * An idividual hashcode can not be determined as the object is mutable
     * It will therefore return allways a constant // dont use Hostname in a map 
     * as it will perform poorly   
     * @return Hashcode
     */
    @Override
    public int hashCode() {
        return 524287;
    }

    /**
     * Compares two Hostnames. If both are equivalent then true will be returned. 
     * If both hostnames are resolvable the address will be compared! 
     * If not the alias list will be searched for any match.
     * A comparison with a String is not supported and will return false.
     * @param obj Reference to a Hostname
     * @return true, if the hostnames reference the same host
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Hostname)) {//here instanceof is ok as no subtyping is possible!
            return false;
        }
        
        
        Hostname other = (Hostname) obj;
        InetAddress otherAddr = other.resolve();
        InetAddress thisAddr = resolve();
        if (thisAddr != null && otherAddr != null) {//the usual case that is unambigous
            return thisAddr.equals(otherAddr);
        } else { // here a set of combinations of comparisons of names have to be considered
            List<String> thisAliases = aliases.get();
            List<String> otherAliases = other.aliases.get();
            for (String thisAlias : thisAliases) {
                if (otherAliases.contains(thisAlias)) {
                    return true;
                }

            }
            return false;
        }
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * @return a negative integer, zero, or a positive integer as this object
     * 		is less than, equal to, or greater than the specified object.
     * @param o the Hostname to be compared.
     */
    public int compareTo(Hostname o) {
        String thisHostname;
        String cmphostname;

        thisHostname = getHostname();
        cmphostname = o.getHostname();

        return thisHostname.compareTo(cmphostname);
    }

    /**
     * Creates and returns a copy of this object.
     *
     *
     * @return a clone of this instance.
     * @see java.lang.Cloneable
     */
    @Override
    public Hostname clone() {
        Hostname retValue = null;

        /* It's OK that this is a shallow copy because JMXServiceURL and
         * ObjectName are immutable. */
        try {
            retValue = (Hostname) super.clone();
        } catch (CloneNotSupportedException e) {
            assert false : "Unable to clone ComponentInfo";
        }

        return retValue;
    }

    /**
     * Matches this hostname a hostname
     * @param aHostname regular expression or hostname which is compared
     *                  to the canonical hostname, the short hostname and to
     *                  the ip address of the host
     * @return true if <code>aHostname</code> matches this hostname
     */
    public boolean matches(String aHostname) {
        if (aHostname == null) {
            return false;
        }
        try {
            Pattern regexp = Pattern.compile(aHostname);
            InetAddress adr = resolve();
            for (String alias : aliases.get()) {
                if (regexp.matcher(alias).matches()) {
                    return true;
                }
            }
        } catch (PatternSyntaxException ex) {
            // Ignore exception and consider that the 
        }
        Hostname hostObj = getInstance(aHostname);
        return hostObj.equals(this);
    }
}
