/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import com.sun.grid.grm.config.common.TimeInterval;
import java.util.concurrent.TimeUnit;

/**
 * Helper class for converting intervals from the configuration.
 */
public class TimeIntervalHelper {
    
    private static final String BUNDLE = "com.sun.grid.grm.util.util";
    private final long value;
    private final TimeUnit unit;
    private final long millis;
    
    /** unit minutes */
    public final static String MINUTES = "minutes";
    /** unit seconds */
    public final static String SECONDS = "seconds";
    /** unit hours */
    public final static String HOURS   = "hours";
    
    /**
     * Creates a new instance of TimeIntervalHelper
     * @param interval the interval configuration
     */
    public TimeIntervalHelper(TimeInterval interval) {
        this(interval.getValue(), interval.getUnit());
    }
    
    public TimeIntervalHelper(int aValue, String aUnit) {
        if(HOURS.equalsIgnoreCase(aUnit)) {
            value = aValue * 3600;
            unit = TimeUnit.SECONDS;
            millis = value * 1000;
        } else if (MINUTES.equalsIgnoreCase(aUnit)) {
            value = aValue * 60;
            unit = TimeUnit.SECONDS;
            millis = value * 1000;
        } else if (SECONDS.equalsIgnoreCase(aUnit)) {
            value = aValue;
            unit = TimeUnit.SECONDS;
            millis = value * 1000;
        } else {
            throw new IllegalArgumentException(
                I18NManager.formatMessage("TimeIntervalHelper.ex.unit", BUNDLE, aUnit));
        }
    }
    
    /**
     * Get the value of the interval
     * @return the value of the interval
     */
    public long getValue() {
        return value;
    }
    
    /**
     *  Get the unit of the interval
     *
     *  @return the unit of the interval
     */
    public TimeUnit getUnit() {
        return unit;
    }

    /**
     *  Get the value of the time interval in millis
     *  @return value of the time interval in millis
     */
    public long getValueInMillis() {
        return millis;
    }
    
    /**
     * Get the value of the time interval in seconds
     * @return value of the time interval in seconds
     */
    public long getValueInSeconds() {
        return millis / 1000;
    }
    
    @Override
    public String toString() {
        return toString(millis);
    }
    
    public static String toString(long duration) {
        String ret = null;
        if (duration >= 1000) {
            long seconds = duration / 1000;
            long millis = duration % 1000;
            if (seconds >= 60) {
                long minutes = seconds / 60;
                seconds = seconds % 60;
                if (minutes > 60) {
                    long hours = minutes / 60;
                    minutes = minutes % 60;
                    ret = String.format("%dh %dm %ds %dms", hours, minutes, seconds, millis);
                } else {
                    ret = String.format("%dm %ds %dms", minutes, seconds, millis);
                }
            } else {
                ret = String.format("%ds %dms", seconds, millis);
            }
        } else {
            ret = String.format("%dms", duration);
        }
        return ret;
    }
    
}
