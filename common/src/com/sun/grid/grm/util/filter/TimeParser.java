/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util.filter;

import com.sun.grid.grm.util.I18NManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * This class parse the reporter date string
 */
public class TimeParser {

    private static final String BUNDLE = "com.sun.grid.grm.util.filter.messages";
    private static final Logger log = Logger.getLogger(TimeParser.class.getName(), BUNDLE);


   private final static int [] FIELDS = {
      Calendar.YEAR,              // 0
      Calendar.MONTH,             // 1
      Calendar.DAY_OF_MONTH,      // 2
      Calendar.HOUR_OF_DAY,       // 3
      Calendar.MINUTE,            // 4
      Calendar.SECOND,            // 5
      Calendar.MILLISECOND        // 6
   };
   
   private final static TSFormat [] FORMATS = {
      new TSFormat( "mm"         , "[0-5][0-9]", Calendar.MINUTE, Calendar.MINUTE )
    , new TSFormat( "HH:mm"      , "[0-2][0-9]:[0-5][0-9]", Calendar.HOUR_OF_DAY, Calendar.MINUTE )
    , new TSFormat( "HH:mm:ss"   , "[0-2][0-9]:[0-5][0-9]:[0-5][0-9]", Calendar.HOUR_OF_DAY, Calendar.SECOND )
    , new TSFormat( "dd HH:mm"   , "[0-3][0-9] [0-2][0-9]:[0-5][0-9]", Calendar.DAY_OF_MONTH, Calendar.MINUTE )
    , new TSFormat( "dd HH:mm:ss"  , "[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]", Calendar.DAY_OF_MONTH, Calendar.SECOND )
    , new TSFormat( "MM/dd HH:mm", "[0-1][0-9]/[0-3][0-9] [0-2][0-9]:[0-5][0-9]", Calendar.MONDAY, Calendar.MINUTE)
    , new TSFormat( "MM/dd HH:mm:ss", "[0-1][0-9]/[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]", Calendar.MONDAY, Calendar.SECOND)
    , new TSFormat( "yyyy/MM/dd HH:mm", "[0-9]{4}/[0-1][0-9]/[0-3][0-9] [0-2][0-9]:[0-5][0-9]", Calendar.YEAR, Calendar.MINUTE)
    , new TSFormat( "yyyy/MM/dd HH:mm:ss", "[0-9]{4}/[0-1][0-9]/[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9]", Calendar.YEAR, Calendar.SECOND)
   };
   
   /**
    * Gets the available formats
    * @return formats
    */
   public static String [] getFormats() {
      String [] ret = new String[FORMATS.length];
      for(int i = 0; i < FORMATS.length; i++ ) {
         ret[i] = FORMATS[i].formatStr;
      }
      return ret;
   }
   
   /** Creates a new instance of Timestamp
    * @param str a string to parse
    * @return Date instance representing the given string
    * @throws java.text.ParseException when a time can not be parsed from given input
    */
   public static Date parse(String str) throws ParseException {
   
      for(int i = 0; i <FORMATS.length; i++)  {
         if(FORMATS[i].matches(str)) {            
            return FORMATS[i].parse(str).getTime();        
         }
      }
      throw new ParseException(I18NManager.formatMessage("ex.time.parser", BUNDLE, str), 0);
   }
   
   /**
    * Creates a new Format
    */
   static class TSFormat {
      
      private String formatStr;
      private SimpleDateFormat df;
      
      private int firstSetField;
      private int lastSetField;
      private Pattern matchExpr;
      
      public TSFormat(String formatStr, String matchExpr,  int firstSetField, int lastSetField) {
         
         this.formatStr = formatStr;
         this.firstSetField = firstSetField;
         this.lastSetField = lastSetField;
         this.matchExpr = Pattern.compile(matchExpr);
      }
      
      public boolean matches(String str) {
          return matchExpr.matcher(str).matches();
      }

       /** Creates a new instance of timestamp
        *
        * @param str a string to parse
        * @return Calendar instance representing the given string
        * @throws java.text.ParseException when a time can not be parsed from given input
        */
      public Calendar parse(String str) throws ParseException {
         
         if(df == null ) {
            df = new SimpleDateFormat(formatStr);
         }
         Date d = df.parse(str);
         Calendar cal = Calendar.getInstance();
         cal.setTime(d);
         
         Calendar ret = Calendar.getInstance();
         
         // 0 = leave, 1 = set, 2 = reset
         int mode = 0;
         for(int i = 0; i < FIELDS.length; i++ ) {
            
            if( FIELDS[i] == firstSetField ) {
               mode = 1;
            }
            
            switch(mode) {
               case 0: // do nothing
                  break;
               case 1: // set field
                  ret.set(FIELDS[i], cal.get(FIELDS[i]));   
                  break;
               case 2: // reset field
                  ret.set(FIELDS[i], cal.getMinimum(FIELDS[i]));
                  break;
            }
            
            if( FIELDS[i] == lastSetField ) {
               mode  = 2;
            }
         }         
         return ret;
      }      
   }
}

