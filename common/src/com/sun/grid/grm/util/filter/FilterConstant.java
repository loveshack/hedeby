/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.filter;

/**
 * FilterValue which never changes
 * @param <T> type of the filtered value
 */
public class FilterConstant<T> implements FilterValue<T> {

    
    private final static long serialVersionUID = -2007281101L;
    
    private final Object value;

    /**
     * Create a new FilterConstant
     * @param value the constant value
     */
    public FilterConstant(Object value) {
        this.value = value;
    }

    /**
     * Get the value of this FilterValue
     * @param resolver the variable resolver
     * @return the filter value
     */
    public Object getValue(VariableResolver<T> resolver) {
        return value;
    }

    @Override
    public String toString() {
        if (value instanceof String) {
            return String.format("\"%s\"", value);
        } else if (value instanceof Character) {
            return String.format("'%s'", value);
        } else if (value == null) {
            return "null";
        } else {
            return value.toString();
        }
    }

    /**
     * This object is immutable. The clone method returns
     * a reference this this
     * @return reference to this
     */
    @Override
    public FilterValue<T> clone() {
        return this;
    }

    /**
     * Indicates whether any other object is "equal to" this object.
     *
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object is equal as the obj
     *          argument; <code>false</code> otherwise.
     * @see     #hashCode()
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final FilterConstant<T> other = (FilterConstant<T>) obj;
        if (this.value != other.value && (this.value == null || !this.value.equals(other.value))) {
            return false;
        }
        return true;
    }

    /**
     * Get the hashcode of this object.
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + (this.value != null ? this.value.hashCode() : 0);
        return hash;
    }
}
