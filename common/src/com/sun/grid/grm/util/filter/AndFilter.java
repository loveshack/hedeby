/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.filter;

import java.util.List;


/**
 *  This filter matches of all terms are matches
 * @param <T> type if the filtered objects
 */
public class AndFilter<T> extends CompoundFilter<T> {
    
    private final static long serialVersionUID = -2007112801L;
    
    /**
     * Create a new AndFilter
     */
    public AndFilter() {
    }
    
    /**
     * Create a new AndFilter with a default capacity
     * @param size the default capacity
     */
    public AndFilter(int size) {
        super(size);
    }
    
    public AndFilter(List<Filter<T>> terms) {
        super(terms);
    }
    
    /**
     * Matches the filter.
     *
     * @param resolver  the variable resolver
     * @return <code>true</code> if all terms of the and filter matches
     */
    public boolean matches(VariableResolver<T> resolver) {
        for(Filter<T> filter: getTerms()) {
            if(!filter.matches(resolver)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder();
        boolean first = true;
        
        for(Filter<T> filter: getTerms()) {
            if(first) { first = false; } else {
                ret.append (" & ");
            }
            if(filter instanceof CompoundFilter) {
                ret.append("(");
            }
            ret.append(filter);
            if(filter instanceof CompoundFilter) {
                ret.append(")");
            }
        }
        return ret.toString();
    }

    @Override
    public Filter<T> clone() {
        return new AndFilter<T>(cloneTerms());
    }
    
}
