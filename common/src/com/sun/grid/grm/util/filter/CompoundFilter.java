/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.filter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *  Abstract base class for compound resource filters.
 * @param <T> type if the filtered objects
 */
public abstract class CompoundFilter<T> implements Filter<T>, Serializable {
    
    private final static long serialVersionUID = -2007112801L;
    
    private final List<Filter<T>> terms;
    
    /**
     * Create a new CompoundFilter
     */
    protected CompoundFilter() {
        terms = new LinkedList<Filter<T>>();
    }

    /**
     * Create a new CompoundFilter with a default capacity (number of terms)
     * 
     * @param size  the default capacity
     */
    protected CompoundFilter(int size) {
        terms = new ArrayList<Filter<T>>(size);
    }
    
    /**
     * Create a new CompoundFilter
     * @param terms the terms
     */
    protected CompoundFilter(List<Filter<T>> terms) {
        this.terms = new ArrayList<Filter<T>>(terms);
    }
    
    /**
     * Add a new filter term
     * @param filter the filter term
     */
    public void add(Filter<T> filter) {
        terms.add(filter);
    }
    
    /**
     * Get the terms of the compound filter
     * @return list of terms
     */
    protected List<Filter<T>> getTerms() {
        return terms;
    }
    
    /**
     * Get the number of terms of the filter
     * @return the number of terms
     */
    public int size() {
       return terms.size(); 
    }
    
    /**
     * Get the first term of the filter
     * @return the first term of <code>null</code> if the filter has not terms
     */
    public Filter<T> getFirstTerm() {
        if(terms.size() > 0) {
            return terms.get(0);
        } else {
            return null;
        }
    }
    
    protected List<Filter<T>> cloneTerms() {
        List<Filter<T>> tmpTerms = getTerms();
        ArrayList<Filter<T>> ret = new ArrayList<Filter<T>>(tmpTerms.size());
        for(Filter<T> term: tmpTerms) {
            ret.add(term.clone());
        }
        return ret;
    }

    /**
     * Indicates whether any other object is "equal to" this object.
     *
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object is equal as the obj
     *          argument; <code>false</code> otherwise.
     * @see     #hashCode()
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final CompoundFilter<T> other = (CompoundFilter<T>) obj;
        if (this.terms != other.terms && (this.terms == null || !this.terms.equals(other.terms))) {
            return false;
        }
        return true;
    }

    /**
     * Get the hashcode of this object.
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (this.terms != null ? this.terms.hashCode() : 0);
        return hash;
    }
    
    @Override
    public abstract Filter<T> clone();
}
