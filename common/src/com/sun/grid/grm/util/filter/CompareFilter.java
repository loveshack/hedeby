/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.filter;

import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.NumberParser;

/**
 * This filter compares to FilterValues
 * @param <T> type if the filtered objects
 */
public class CompareFilter<T> implements Filter<T> {

    private final static long serialVersionUID = -2008101001L;

    public enum Operator {

        LT, LE, EQ, NE, GE, GT, ID
    };
    private final FilterValue<T> operant1;
    private final FilterValue<T> operant2;
    private final Operator op;

    /**
     * Create a new CompareFilter
     * @param v1   the first operant
     * @param v2   the second operant
     * @param operator  the operator
     */
    public CompareFilter(FilterValue<T> v1, FilterValue<T> v2, String operator) {
        this(v1, v2, parseOperator(operator));
    }

    /**
     * Create a new CompareFilter
     * @param v1   the first operant
     * @param v2   the second operant
     * @param operator  the operator
     */
    public CompareFilter(FilterValue<T> v1, FilterValue<T> v2, Operator operator) {
        this.operant1 = v1;
        this.operant2 = v2;
        this.op = operator;
    }

    /**
     * Get the compare operator
     * @return the operator
     */
    public Operator getOperator() {
        return op;
    }

    /**
     * Get the first operant
     * @return the first operant
     */
    public FilterValue<T> getFirstOperant() {
        return operant1;
    }

    /**
     * Get the second operant
     * @return the second operant
     */
    public FilterValue<T> getSecondOperant() {
        return operant2;
    }

    private static Operator parseOperator(String str) {
        if (str.equals("<=")) {
            return Operator.LE;
        } else if (str.equals("<")) {
            return Operator.LT;
        } else if (str.equals(">")) {
            return Operator.GT;
        } else if (str.equals(">=")) {
            return Operator.GE;
        } else if (str.equals("=")) {
            return Operator.EQ;
        } else if (str.equals("==")) {
            return Operator.ID;
        } else if (str.equals("!=")) {
            return Operator.NE;
        } else {
            throw new IllegalArgumentException("Unknown operator " + str);
        }
    }

    /**
     * Convert a operator into a string
     * @param op the operator
     * @return the string representation
     */
    public static String toString(Operator op) {
        switch (op) {
            case LT:
                return "<";
            case LE:
                return "<=";
            case GE:
                return ">=";
            case GT:
                return ">";
            case EQ:
                return "=";
                case ID:
                return "==";
            case NE:
                return "!=";
            default:
                throw new IllegalStateException("Unknown operator " + op);
        }
    }

    /**
     * Comapare the operants
     * @param resolver
     * @return compare expression matches
     */
    public boolean matches(VariableResolver<T> resolver) {
        Object v1 = operant1.getValue(resolver);
        Object v2 = operant2.getValue(resolver);
        return matches(v1, v2);
    }

    @SuppressWarnings("unchecked")
    private boolean matches(Object v1, Object v2) {
        int compResult = 0;
        if (v1 == null) {
            if (v2 == null) {
                if (op == Operator.EQ) {
                    return true;
                } else if (op == Operator.NE) {
                    return false;
                } else {
                    return false;
                }
            } else {
                if (op == Operator.EQ) {
                    return false;
                } else if (op == Operator.NE) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            // v1 has a value
            if (v2 == null) {
                if (op == Operator.EQ) {
                    return false;
                } else if (op == Operator.NE) {
                    return true;
                } else {
                    return false;
                }
            } else if (op == Operator.ID) {
                // identity = perform Object.equals() on two objects, which means
                // that references to instances will be comapred explicitly
                return v1.equals(v2);
            } else if (v1 instanceof Number && v2 instanceof Number) {
                compResult = Double.compare(((Number) v1).doubleValue(), ((Number) v2).doubleValue());
            } else if (v1 instanceof Number && v2 instanceof String) {
                try {
                    Number v2n = NumberParser.parseDouble((String) v2);
                    compResult = Double.compare(((Number) v1).doubleValue(), v2n.doubleValue());
                } catch (NumberFormatException ex) {
                    if (op == Operator.EQ) {
                        return false;
                    } else if (op == Operator.NE) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else if (v1 instanceof String && v2 instanceof Number) {
                try {
                    Number v1n = NumberParser.parseDouble((String) v1);
                    compResult = Double.compare(v1n.doubleValue(), ((Number) v2).doubleValue());
                // evaluation is done below
                } catch (NumberFormatException ex) {
                    if (op == Operator.EQ) {
                        return false;
                    } else if (op == Operator.NE) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else if (v1 instanceof String && v2 instanceof Boolean) {
                compResult = Boolean.valueOf((String) v1).compareTo((Boolean) v2);
            } else if (v1 instanceof Boolean && v2 instanceof String) {
                compResult = ((Boolean) v1).compareTo(Boolean.valueOf((String) v2));
            } else if (v1.getClass().isAssignableFrom(v2.getClass()) && v1 instanceof Comparable) {
                compResult = ((Comparable) v1).compareTo(v2);
            } else if (v1 instanceof String && v2 instanceof Character) {
                String s1 = (String) v1;
                String s2 = v2.toString();
                compResult = s1.compareTo(s2);
            } else if (v1 instanceof Character && v2 instanceof String) {
                String s1 = v1.toString();
                String s2 = (String) v2;
                compResult = s1.compareTo(s2);
            } else if (v1 instanceof Class && v2 instanceof Class) {
                Class c1 = (Class) v1;
                Class c2 = (Class) v2;
                if (op == Operator.EQ) {
                    return c1.isAssignableFrom(c2) || c2.isAssignableFrom(c1);
                } else if (op == Operator.NE) {
                    return !(c1.isAssignableFrom(c2) || c2.isAssignableFrom(c1));
                } else if (op == Operator.GE) {
                    return c2.isAssignableFrom(c1);
                } else {
                    compResult = c1.getName().compareTo(c2.getName());
                }
            } else if (v1 instanceof Class && v2 instanceof String) {
                compResult = ((Class) v1).getName().compareTo((String) v2);
            } else if (v1 instanceof String && v2 instanceof Class) {
                compResult = ((String) v1).compareTo(((Class) v2).getName());
            } else if (v1 instanceof Hostname && v2 instanceof String) {
                Hostname h1 = (Hostname) v1;
                Hostname h2 = Hostname.getInstance((String) v2);
                compResult = h1.compareTo(h2);
            } else if (v1 instanceof String && v2 instanceof Hostname) {
                Hostname h1 = Hostname.getInstance((String) v1);
                Hostname h2 = (Hostname) v2;
                compResult = h1.compareTo(h2);
            } else if (op == Operator.EQ) {
                return v1.equals(v2);
            } else if (op == Operator.NE) {
                return !v1.equals(v2);
            } else {
                return false;
            }
        }
        switch (op) {
            case LT:
                return compResult < 0;
            case LE:
                return compResult <= 0;
            case GE:
                return compResult >= 0;
            case GT:
                return compResult > 0;
            case EQ:
                return compResult == 0;
            case NE:
                return compResult != 0;
            default:
                throw new IllegalStateException("Unknown operator " + op);
        }
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", operant1, toString(op), operant2);
    }

    @Override
    public Filter<T> clone() {
        return new CompareFilter<T>(operant1.clone(), operant2.clone(), op);
    }

    /**
     * Indicates whether any other object is "equal to" this object.
     *
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object is equal as the obj
     *          argument; <code>false</code> otherwise.
     * @see     #hashCode()
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final CompareFilter<T> other = (CompareFilter<T>) obj;
        if (this.operant1 != other.operant1 && (this.operant1 == null || !this.operant1.equals(other.operant1))) {
            return false;
        }
        if (this.operant2 != other.operant2 && (this.operant2 == null || !this.operant2.equals(other.operant2))) {
            return false;
        }
        if (this.op != other.op) {
            return false;
        }
        return true;
    }

    /**
     * Get the hashcode of this object.
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.operant1 != null ? this.operant1.hashCode() : 0);
        hash = 37 * hash + (this.operant2 != null ? this.operant2.hashCode() : 0);
        hash = 37 * hash + (this.op != null ? this.op.hashCode() : 0);
        return hash;
    }
}
