/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.filter;

/**
 * @param <T> type of the filtered objects
 */
public class ConstantFilter<T> implements Filter<T> {

    private final static long serialVersionUID = -2008031001L;
    
    private final boolean value;

    private ConstantFilter(boolean value) {
        this.value = value;
    }

    /**
     * This method returns the constant value of this filter
     * @param resolver the resolver (not used)
     * @return the value of the constant filter
     */
    public boolean matches(VariableResolver resolver) {
        return value;
    }

    /**
     * The ConstantFilter is immuntable. The clone method returns
     * a reference to this.
     * 
     * @return reference to this
     */
    @Override
    public Filter<T> clone() {
        return this;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @SuppressWarnings("unchecked")
    public static <T> ConstantFilter<T> alwaysMatching() {
        return ALWAYS_MATCHING_FILTER;
    }

    @SuppressWarnings("unchecked")
    public static <T> ConstantFilter<T> neverMatching() {
        return NEVER_MATCHING_FILTER;
    }
    /**
     * This filter matches always
     */
    private static final ConstantFilter ALWAYS_MATCHING_FILTER = new ConstantFilter(true);
    /**
     * This filter is never matching
     */
    private static final ConstantFilter NEVER_MATCHING_FILTER = new ConstantFilter(false);

    /**
     * Indicates whether any other object is "equal to" this object.
     *
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object is equal as the obj
     *          argument; <code>false</code> otherwise.
     * @see     #hashCode()
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final ConstantFilter<T> other = (ConstantFilter<T>) obj;
        if (this.value != other.value) {
            return false;
        }
        return true;
    }

    /**
     * Get the hashcode of this object.
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.value ? 1 : 0);
        return hash;
    }
}
