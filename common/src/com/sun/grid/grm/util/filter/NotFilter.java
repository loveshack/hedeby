/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.filter;

/**
 *  Not filter 
 * @param <T> type of the filter
 */
public class NotFilter<T> implements Filter<T> {

    private final static long serialVersionUID = -2007112801L;
    private final Filter<T> filter;

    /**
     * Create a new not filter
     * @param filter the negotiated filter
     */
    public NotFilter(Filter<T> filter) {
        if (filter == null) {
            throw new NullPointerException();
        }
        this.filter = filter;
    }

    /**
     * Matches the filter
     * @param resolver the variable resolver
     * @return return <code>true</code> if the inner filter does not match
     */
    public boolean matches(VariableResolver<T> resolver) {
        return !filter.matches(resolver);
    }

    @Override
    public Filter<T> clone() {
        return new NotFilter<T>(filter.clone());
    }

    /**
     * Indicates whether any other object is "equal to" this object.
     *
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object is equal as the obj
     *          argument; <code>false</code> otherwise.
     * @see     #hashCode()
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final NotFilter<T> other = (NotFilter<T>) obj;
        if (this.filter != other.filter && (this.filter == null || !this.filter.equals(other.filter))) {
            return false;
        }
        return true;
    }

    /**
     * Get the hashcode of this object.
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.filter != null ? this.filter.hashCode() : 0);
        return hash;
    }
}
