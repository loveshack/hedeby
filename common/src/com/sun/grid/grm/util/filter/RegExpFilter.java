/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.filter;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 *  This resource filter allows filtering of one property against
 *  a regular expression.
 *
 *  The resource matches of the regular expression matches the value of 
 *  the resource property.
 * @param <T> type of the filter
 */
public class RegExpFilter<T> implements Filter<T> {

    private final static long serialVersionUID = -2007112801L;
    private final static String BUNDLE = "com.sun.grid.grm.util.filter.messages";
    private final FilterValue<T> value;
    private final Pattern pattern;

    /**
     * Creates a new instance of EqualsFilter.
     * @param value    the filter value
     * @param pattern  the pattern for the regular expression
     * @throws com.sun.grid.grm.util.filter.FilterException if the pattern is not a valid regular expression
     */
    public RegExpFilter(FilterValue<T> value, String pattern) throws FilterException {
        this(value, compile(pattern));
    }

    private static Pattern compile(String pattern) throws FilterException {
        try {
            return Pattern.compile(pattern);
        } catch (PatternSyntaxException ex) {
            throw new FilterException("ex.invalidRegExp", ex, BUNDLE, pattern, ex.getLocalizedMessage());
        }
    }

    /**
     * Creates a new instance of EqualsFilter.
     * @param value   the filter value, must not be null
     * @param pattern  the pattern, must not be null
     */
    public RegExpFilter(FilterValue<T> value, Pattern pattern) {
        if (pattern == null) {
            throw new IllegalArgumentException("pattern must not be null");
        }
        if (value == null) {
            throw new IllegalArgumentException("value must not be null");
        }
        this.value = value;
        this.pattern = pattern;
    }

    /**
     * Matches this filter against <code>source</code>.
     *
     * @param resolver the variable resolver
     * @return <code>true</code> if the regular expression matches the resource property
     */
    public boolean matches(VariableResolver<T> resolver) {

        Object v = value.getValue(resolver);
        if (v == null) {
            return false;
        } else {
            return pattern.matcher(v.toString()).matches();
        }
    }

    @Override
    public String toString() {
        return String.format("%s matches \"%s\"", value, pattern);
    }

    @Override
    public Filter<T> clone() {
        return new RegExpFilter<T>(value.clone(), pattern);
    }

    /**
     * Indicates whether any other object is "equal to" this object. Compares
     * the string representation of the regular expression.
     *
     * To create two equal RegExpFilter, use #RegExpFilter.clone().
     *
     *
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object is equal as the obj
     *          argument; <code>false</code> otherwise.
     * @see     #hashCode()
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final RegExpFilter<T> other = (RegExpFilter<T>) obj;
        if (this.value != other.value && (this.value == null || !this.value.equals(other.value))) {
            return false;
        }
        // pattern can not be null, it is checked in the constructor
        final String patternString = this.pattern.pattern();
        final String otherPatternString = other.pattern.pattern();
        if (!patternString.equals(otherPatternString)) {
            return false;
        }
        return true;
    }

    /**
     * Get the hashcode of this object.
     *
     * Uses the string representation of the regular expression.
     * 
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        // pattern can not be null, it is checked in the constructor
        final String patternString = this.pattern.pattern();
        int hash = 3;
        hash = 59 * hash + (this.value != null ? this.value.hashCode() : 0);
        hash = 59 * hash + (patternString != null ? patternString.hashCode() : 0);
        return hash;
    }
}
