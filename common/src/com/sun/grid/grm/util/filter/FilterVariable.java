/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util.filter;

/**
 *  This <code>FilterValue</code> get the value of a variable
 * @param <T> type of the filter value
 */
public class FilterVariable<T> implements FilterValue<T> {

    private final static long serialVersionUID = -2007112801L;
    
    private final String name;
    
    /**
     * Create a new instance of FilterVariable
     * @param name name ofthe variable
     */
    public FilterVariable(String name) {
        this.name = name;
    }
    
    /**
     * Get the value of the variable
     * @param resolver  the variable resolver
     * @return the value of the variable
     */
    public Object getValue(VariableResolver<T> resolver) {
        return resolver.getValue(name);
    }
    
    @Override
    public String toString() {
        return name;
    }

    /**
     * This object is immutable. This method returns a reference
     * to this.
     * @return reference to this
     */
    @Override
    public FilterValue<T> clone() {
        return this;
    }
    
    /**
     * Get the name of the filter variable
     * @return the name of the filter variable
     */
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final FilterVariable<T> other = (FilterVariable<T>) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    

}
