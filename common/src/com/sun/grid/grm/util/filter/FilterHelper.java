/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util.filter;

import java.io.ByteArrayInputStream;


/**
 * Utilitiy class for filtering
 */
public class FilterHelper {
    
    /**
     * Parse a filter expression
     * @param <T> type of the filter
     * @param filter the filter expression
     * @return the filter
     * @throws com.sun.grid.grm.util.filter.FilterException if the filter expression is not valid
     */
    public static <T> Filter<T> parse(String filter) throws FilterException {
        ByteArrayInputStream in = new ByteArrayInputStream(filter.getBytes());
        try {
            return FilterParser.<T>parse(in);
        } catch(Exception ex) {
            throw new FilterException(ex.getLocalizedMessage(), ex);
        }
    }
    
    /**
     * Parse a filter expression with argument in the printf style
     * @param <T> type of the filter
     * @param filter  the filter expression (printf format string)
     * @param args    the arguments for the filter expression
     * @return the filter
     * @throws com.sun.grid.grm.util.filter.FilterException if the filter expression is not valid
     */
    public static <T> Filter<T> parse(String filter, Object ... args) throws FilterException {
        for(int i = 0; i < args.length; i++) {
            if(args[i] instanceof String) {
                args[i] = quote((String)args[i]);
            } else if (args[i] instanceof Character) {
                args[i] = quote((Character)args[i]);
            }
        }
        return parse(String.format(filter, args));
    }
    
    /**
     * Quote a string (after quoting in can be used in a filter expression)
     * @param arg the string
     * @return the quoted string
     */
    public static String quote(String arg) {
        return arg.replaceAll("\"", "\\\"");
    }
    
    /**
     * Quote a character (after quoting in can be used in a filter expression)
     * @param c the character
     * @return the quoted string
     */
    public static String quote(char c) {
        if(c == '\'') {
            return "\\'";
        } else {
            return Character.toString(c);
        }
    }
    
    private final static String quoteSrc[] = {
        "\\t","\\b","\\r","\\f","\\\\","\\'","\\\"","\\n","\\r", "\\r\\n"
    };
    
    private final static String quoteTarget [] = {
        "\t", "\b", "\r", "\f", "\\", "\'", "\"", "\n", "\r", "\r\n"
    };
    
    /**
     * Remove quote from a String
     * @param s  the string
     * @return the string without quotes
     */
    public static String removeQuotes(String s) {
        int index = s.indexOf('\\');
        if(index >= 0) {
            StringBuilder sb = new StringBuilder(s);
            for(int i = 0; i < quoteSrc.length; i++) {
                index = sb.indexOf(quoteSrc[i]);
                while(index >= 0) {
                    sb.replace(index, index + quoteSrc[i].length(), quoteTarget[i]);
                    index = sb.indexOf(quoteSrc[i]);
                }
            }
            return sb.toString();
        } else {
            return s;
        }
    }
}
