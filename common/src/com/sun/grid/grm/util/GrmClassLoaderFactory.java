/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import com.sun.grid.grm.bootstrap.PathUtil;
import java.io.File;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *  Ths GrmClassLoaderFactory create ClassLoaders.
 * 
 *  <p>The placeholder <code>${distlib}/code> inside a URL will be replaced by the 
 *  real distribution lib directory of the hedeby system.</p>
 * 
 *   <H3>Example:</H3>
 * 
 *   <pre>
 *       ClassLoader cl = GrmClassLoaderFactory.getInstance("${distlib}/lib/hedeby-security-impl.jar");
 *   </pre>
 *
 *  <p>The GrmClassLoaderFactory has a cache inside. The getInstance methods ensure that only one ClassLoader
 *     with a particular classpath and parent class loader is created/p>
 */
public class GrmClassLoaderFactory {
    
    
    private static final Lock lock = new ReentrantLock();
    private static final Map<ClassLoaderElem,ClassLoader> clCache = new HashMap<ClassLoaderElem,ClassLoader>(5);
    
    /**
     * Get an instance of a <code>GrmClassLoader</code>.
     * 
     * @param classpath  classpath as string
     * @param delimiter  delimiter for the elements in <code>classpath</code>
     * @param parent     the parent class loader
     * @return the new <code>ClassLoader</code>
     */
    public static ClassLoader getInstance(String classpath, String delimiter, ClassLoader parent) {
        String [] elements = classpath.split(delimiter);
        return getInstance(elements, parent);
    }

    /**
     * Get an instance of a <code>ClassLoader</code>.
     * 
     * @param classpath  classpath as string
     * @param delimiter  delimiter for the elements in <code>classpath</code>
     * @return the new <code>ClassLoader</code>
     */
    public static ClassLoader getInstance(String classpath, String delimiter) {
        return getInstance(classpath, delimiter, (ClassLoader)null);
    }
    
    /**
     * Get an instance of a <code>ClassLoader</code>.
     * 
     * @param classpath  classpath as string. delimiter is the system path delimiter
     *                   (<code>File.pathSeparator</code>)
     * @param parent     the parent class loader
     * @return the new <code>ClassLoader</code>
     */
    public static ClassLoader getInstance(String classpath, ClassLoader parent) {
        return getInstance(classpath, File.pathSeparator, parent);
    }

    /**
     * Get an instance of a <code>ClassLoader</code>.
     * 
     * @param classpath  classpath as string. delimiter is the system path delimiter
     *                   (<code>File.pathSeparator</code>)
     * @return the new <code>ClassLoader</code>
     */
    public static ClassLoader getInstance(String classpath) {
        return getInstance(classpath, (ClassLoader)null);
    }
    
    /**
     * Get an instance of a <code>ClassLoader</code>.
     * 
     * @param pathElements elements of the classpath
     * @return the new <code>GrmClassLoaderFactory</code>
     */
    public static ClassLoader getInstance(String [] pathElements) {
        return getInstance(pathElements, null);
    }
    
    /**
     * Get an instance of a <code>ClassLoader</code>.
     * 
     * @param pathElements elements of the classpath
     * @param parent     the parent class loader
     * @return the new <code>GrmClassLoaderFactory</code>
     */
    public static ClassLoader getInstance(String [] pathElements, ClassLoader parent) {
        
        URL [] urls = new URL[pathElements.length];
        
        String distLibURL = PathUtil.getDistLibURL();
        
        for(int i = 0; i < pathElements.length; i++) {
            String file = pathElements[i].replace("${distlib}", distLibURL);
            try {
                urls[i] = new URL(file);
            } catch (MalformedURLException ex) {
                throw new IllegalStateException("path elements " + pathElements[i] + " defines invalid url"
                                       , ex);
            }

        }        
        return getInstance(urls, parent);
    }
    
    /**
     * Get an instance of <code>GrmClassLoaderFactory</code>
     * 
     * @param url     classpath elements
     * @param parent  the parent class loader
     * @return the new <code>GrmClassLoaderFactory</code>
     */
    public static ClassLoader getInstance(URL[] url, ClassLoader parent) {
        ClassLoaderElem elem = new ClassLoaderElem(url, parent);
        ClassLoader ret = null;
        lock.lock();
        try {
            ret = clCache.get(elem);
            if(ret == null) {
                ret = new URLClassLoader(url, parent);
                clCache.put(elem, ret);
            }
        } finally {
            lock.unlock();
        }
        return ret;
    }
            
    
    /**
     * Get a human readable string representation of a classloader.
     * 
     * Prints all classpath elements for the classloader and its parents. 
     * @param cl the classloader
     * @return the string representation
     */
    public static String toString(ClassLoader cl) {
        StringWriter sw = new StringWriter();
        Printer p = new Printer(sw);
        try {
            printClassLoader(cl, p);
        } finally {
            p.close();
        }
        
        return sw.getBuffer().toString();
    }
    
    private static void printClassLoader(ClassLoader cl, Printer p) {
        
        p.print(cl.getClass().getSimpleName());
        if(cl instanceof URLClassLoader) {
            URL [] urls = ((URLClassLoader)cl).getURLs();
            p.println('{');
            p.indent();
            for(int i = 0; i < urls.length; i++) {
                p.println(urls[i]);
            }
            p.deindent();
            p.println('}');
        }
        if(cl.getParent() != null) {
            printClassLoader(cl.getParent(), p);
        }
        p.print(']');
    }

    private static class ClassLoaderElem {
        private final URL [] elements;
        private final ClassLoader parent;
        private final int hash;
        
        /**
         * Create a new ClassLoaderElem.
         * @param aElements elements of the classpath
         * @param aParent   the parent classloader
         */
        public ClassLoaderElem(URL [] aElements, ClassLoader aParent) {
            if (aElements == null ) {
                throw new NullPointerException();
            }
            int h = 0;
            for (URL elem: aElements) {
                if(elem == null) {
                    throw new NullPointerException();
                }
                h = h * 31 + elem.hashCode();
            }
            if(aParent != null) {
                h = h * 31 + aParent.hashCode();
            }
            elements = aElements;
            parent = aParent;
            hash = h;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof ClassLoaderElem) {
               ClassLoaderElem cle = (ClassLoaderElem)obj;
               if (parent == null) {
                   if (cle.parent != null ) {
                       return false;
                   }
               } else if (!parent.equals(cle.parent)) {
                   return false;
               }
               return Arrays.equals(elements, cle.elements);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return hash;
        }
    }
    
}
