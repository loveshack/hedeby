/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * The I18Manager is responsible for proper formatting and I18N/L10N of the text
 * used in Hedeby.
 *
 */
public class I18NManager {
    /* Resource bundle with I18Ned messages. */
    private static final String BUNDLE = "com.sun.grid.grm.util.util";
    /* This is the message that will be used when the i18n resource properties
     * cannot be read. */
    private static final String MESSAGE_DEFAULT = "Resource bundle file(s) is(are) missing or unreadable";

    /**
     * This method attempts to resolve the message to an internationalized
     * string. String is being formatted using java.text.MessageFormat.
     * If the message cannot be internationalized, an exception is
     * thrown.
     *
     * @param msg the message key
     * @param bundleName resource bundle containing the message text
     * @param params parameters for the message
     * @return internationalized message
     */
    public static String formatMessage(String msg, String bundleName, Object... params) {
        String pattern = null;
        String ret = null;
        /* First try to load the pattern. */
        try {
            pattern = getBundle(bundleName).getString(msg);
        } catch (MissingResourceException e) {
            /* If the pattern isn't found, throw an exception. */
            String message = null;
            try {
                message = getBundle(BUNDLE).getString("i18n.exception.no_resource");
            } catch (MissingResourceException e2) {
                /* Hard-code the message since we have no i18n resources. */
                message = MESSAGE_DEFAULT;
            }
            throw new IllegalArgumentException(message, e);
        }

        if (pattern != null) {
            /* If the pattern was found, insert parameters if needed. */
            ret = MessageFormat.format(pattern, params);
        } else {
            /* Otherwise, use the message as-is. */
            ret = msg;
        }
        return ret;
    }
    
  
    private static ResourceBundle getBundle(String bundleName) {
        try {
            return ResourceBundle.getBundle(bundleName);
        } catch(MissingResourceException ex) {
            try {
                return ResourceBundle.getBundle(bundleName, Locale.getDefault(), Thread.currentThread().getContextClassLoader());
            } catch(MissingResourceException ex1) {
                throw new IllegalArgumentException("resource bundle " + bundleName + " not found");
            }
        }
    }
    /**
     *  Format a message with printf
     *
     *  @param  msg   the message
     *  @param  bundleName the name of the resource bundle
     *  @param  params parameters for the message
     *  @return internationalized message
     */
    public static String printfMessage(String msg, String bundleName, Object ... params) {
        String pattern = null;
        String ret = null;

        /* First try to load the pattern. */
        try {
            pattern = getBundle(bundleName).getString(msg);
        } catch (MissingResourceException e) {
            /* If the pattern isn't found, throw an exception. */
            String message = null;

            try {
                message = getBundle(BUNDLE).getString("i18n.exception.no_resource");
            } catch (MissingResourceException e2) {
                /* Hard-code the message since we have no i18n resources. */
                message = MESSAGE_DEFAULT;
            }
            throw new IllegalArgumentException(message, e);
        }

        if (pattern != null) {
            /* If the pattern was found, insert parameters if needed. */
            ret = String.format(pattern, params);
        } else {
            /* Otherwise, use the message as-is. */
            ret = msg;
        }
        return ret;
    }
}
