/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.LogManager;

/**
 * This LogManager can be used to write log message
 * in the a ShutdownHook.
 *
 * It block the reset of the default LogManagers shutdown hook.
 */
public class GrmLogManager extends LogManager {

    private AtomicBoolean allowReset = new AtomicBoolean(false);
    
    /** Creates a new instance of GrmLogManager */
    public GrmLogManager() {
    }

    /**
     * All the reset of the loggers
     *
     * @param allowResetFlag the allowReset flag
     */
    public void setAllowReset(boolean allowResetFlag) {
        allowReset.set(allowResetFlag);
    }
    
    /**
     * Reset the loggers
     */
    @Override
    public void reset() {
        if(allowReset.get()) {
            super.reset();
        }
    }
    
}
