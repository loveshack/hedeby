/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * Platform.java
 *
 * Created on May 19, 2006, 9:43 AM
 *
 */

package com.sun.grid.grm.util;

import com.sun.grid.grm.GrmException;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class should encapsulate all methods depending on the platform, such as
 *
 * Use the static <code>getPlatform</code> method to retrieve the implementation of the
 * current platform.
 *
 * <b>Example:</b>
 *
 * <pre>
 *   Platform platform = Platform.getPlatform();
 *
 *   // delete a directory
 *   platform.removeDir(new File("/tmp/blubber"), true);
 *
 *   // change the owner of a file
 *   platform.chown(new File("/tmp/blubber"), "foo_user", true);
 *
 * </pre>
 *
 */
public abstract class Platform {

    private static final String BUNDLE = "com.sun.grid.grm.util.util";
    
    private static final Logger log =
            Logger.getLogger(Platform.class.getName(), BUNDLE);
    
    /** Name of the operating system. */
    public static final String OS_NAME =
            System.getProperty("os.name").toLowerCase(Locale.US);
    
    /** Architecture of the operation system. */
    public static final String OS_ARCH =
            System.getProperty("os.arch").toLowerCase(Locale.US);
    
    /** Version of the operating system. */
    public static final String OS_VERSION =
            System.getProperty("os.version").toLowerCase(Locale.US);
    
    private static final String TEMP_DIR_PROPERTY = "java.io.tmpdir";
    
    private static Platform platform;
    
    private String arch;
    
    private ShutdownHook shutdownHook = null;

    /**
     *   Get the current platform.
     *
     *   @return the current platform
     *   @throws IllegalStateException if the platform is not supported or implemented
     */
    public static synchronized  Platform getPlatform()
    throws IllegalStateException {
        
        if (platform == null) {
            platform = createPlatform();
        }
        return platform;
    }
    
    private static Platform createPlatform() {
        
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "platform.create", new Object [] {OS_NAME, OS_ARCH, OS_VERSION });
        }
        
        Platform ret = null;
        if (OS_NAME.indexOf("windows") >= 0) {
            ret = new WindowsPlatform();
        } else if (OS_NAME.indexOf("mac") >= 0) {
            ret = new UnixPlatform();
        } else if (File.pathSeparatorChar == ':') {
            ret = new UnixPlatform();
        } else {
            throw new IllegalStateException(I18NManager.formatMessage("platform.exception.notsupported", BUNDLE,OS_NAME));
        }
        return ret;
    }

    /**
     *   Remove a directory.
     *
     *   @param  dir  the directory
     *   @param  force  fore the deletion of the directory
     *   @throws IOException on any IO Error
     *   @throws InterruptedException if the deletion of command has been interrupted
     */
    public abstract void removeDir(File dir, boolean force) throws IOException, InterruptedException;
    
    /**
     *  Execute a process and wait for termination.
     *
     *  @param  user     name of the process owner
     *  @param  command  the command
     *  @param  env      environment for the process
     *  @param  dir      working directory of the process
     *  @param  stdout   list where messages from stdout will bestored, can be <code>null</code>
     *  @param  stderr   list where messages from stderr will be stored, can be <code>null</code>
     *  @param  timeout  the amout of time in seconds after which the execution will be interrupted and the exception thrown, if
     *                   <code>0</code> is specified the default 3600 seconds is taken
     *  @return the exit code of the command
     *  @throws java.io.IOException if the command fails
     *  @throws java.lang.InterruptedException if the execution of the command has been interrupted
     */
    public abstract int execAs(String user, String command, String[] env, File dir, List<String> stdout, List<String>stderr, int timeout)
    throws IOException, InterruptedException;
    
    
    /**
     *   Execute a command and wait for exit.
     *
     *   @param command  the command
     *   @param env      environment of the command (&lt;name&gt;=&lt;value&gt;)
     *                   , can be <code>null</code>
     *   @param  dir      working directory of the process
     *   @param stdout   list where the output of stdout of the command will be stored,
     *                    can be <code>null</code>
     *   @param stderr   list where the output of stderr of the command will be stored,
     *                   can be <code>null</code>
     *   @param  timeout  the amout of time in seconds after which the execution will be interrupted and the exception thrown, if
     *                   <code>0</code> is specified the default 3600 seconds is taken
     *   @return the exit code of the command
     *   @throws java.io.IOException if the command fails
     *   @throws java.lang.InterruptedException if the execution of the command has been interrupted
     */
    public int exec(final String command, final String[] env,
            final File dir, final List<String> stdout,
            final List<String> stderr, int timeout)
            throws IOException, InterruptedException {

        return exec(command, env, dir, stdout, stderr, null, timeout);
        
    }

    /**
     *   Execute a command and wait for exit.
     *
     *   @param command  the command
     *   @param env      environment of the command (&lt;name&gt;=&lt;value&gt;)
     *                   , can be <code>null</code>
     *   @param  dir      working directory of the process
     *   @param stdout   list where the output of stdout of the command will be stored,
     *                    can be <code>null</code>
     *   @param stderr   list where the output of stderr of the command will be stored,
     *                   can be <code>null</code>
     *   @param stdin    input stream for the stdin of the process
     *   @param  timeout  the amout of time in seconds after which the execution will be interrupted and the exception thrown, if
     *                   <code>0</code> is specified the default 3600 seconds is taken
     *   @return the exit code of the command
     *   @throws java.io.IOException if the command fails
     *   @throws java.lang.InterruptedException if the execution of the command has been interrupted
     */
    public abstract int execute(final String command, final String[] env,
            final File dir, final List<String> stdout,
            final List<String> stderr, InputStream stdin, int timeout)
            throws IOException, InterruptedException;
    
    /**
     *   Execute a command and wait for exit.
     *
     *   @param command  the command
     *   @param env      environment of the command (&lt;name&gt;=&lt;value&gt;)
     *                   , can be <code>null</code>
     *   @param  dir      working directory of the process
     *   @param stdout   list where the output of stdout of the command will be stored,
     *                    can be <code>null</code>
     *   @param stderr   list where the output of stderr of the command will be stored,
     *                   can be <code>null</code>
     *   @param stdin    input stream for the stdin of the process
     *   @param  timeout  the amout of time in seconds after which the execution will be interrupted and the exception thrown, if
     *                   <code>0</code> is specified the default 3600 seconds is taken
     *   @return the exit code of the command
     *   @throws java.io.IOException if the command fails
     *   @throws java.lang.InterruptedException if the execution of the command has been interrupted
     */
    public int exec(final String command, final String[] env,
            final File dir, final List<String> stdout,
            final List<String> stderr, InputStream stdin, int timeout)
            throws IOException, InterruptedException {
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "platform.exec", command);
        }
        return execute(command, env,
            dir, stdout,
            stderr, stdin, timeout);
    }
    
    /**
     *   Change the owner of a file or directory.
     *
     *   @param file the file or directory
     *   @param owner name of the new owner
     *
     *   @throws java.io.IOException on any IO error
     *   @throws java.lang.InterruptedException if the chown has been interrupted
     */
    public void chown(File file, String owner) throws IOException, InterruptedException {
        chown(file, owner, false);
    }
    
    /**
     *   Change the owner of a file or directory.
     *
     *   @param file the file or directory
     *   @param owner name of the new owner
     *   @param recursive should child files and directiers be affected
     *   @throws java.io.IOException on any IO error
     *   @throws java.lang.InterruptedException if the chown has been interrupted
     */
    public abstract void chown(File file, String owner, boolean recursive)
    throws IOException, InterruptedException;
    
    /**
     *  Get the owner of a file
     *  @param file the file
     *  @return the owner of the file
     *  @throws IOException on error
     */
    public abstract String getFileOwner(File file) throws IOException;
    
    /**
     *   Change the owner of a file or directory.
     *
     *   @param file - the file or directory
     *   @param targetFile - 
     *
     *   @throws java.io.IOException on any IO error
     *   @throws java.lang.InterruptedException if the chown has been interrupted
     */
    public void copy(File file, File targetFile) throws IOException, InterruptedException {
        copy(file, targetFile, false);
    }
    
    /**
     *   Copy a file or directory
     *
     *   @param file - the file or directory
     *   @param targetFile - the target file or directory
     *   @param recursive - should child files and directiers be copied
     *   @throws java.io.IOException on any IO error
     *   @throws java.lang.InterruptedException if the chown has been interrupted
     */
    public void copy(File file, File targetFile, boolean recursive)
    throws IOException, InterruptedException {
        copy(file, targetFile, recursive, false);
    }

    /**
     *   Copy a file or directory
     *
     *   @param file - the file or directory
     *   @param targetFile - the target file or directory
     *   @param recursive - should child files and directiers be copied
     *   @param  preserve - should the owner and permission be preserved if possible
     *   @throws java.io.IOException on any IO error
     *   @throws java.lang.InterruptedException if the chown has been interrupted
     */
    public abstract void copy(File file, File targetFile, boolean recursive, boolean preserve)
    throws IOException, InterruptedException;

    /**
     *   Move a file or directory
     *
     *   @param file - the file or directory
     *   @param targetFile - the target file or directory
     *   @throws java.io.IOException on any IO error
     *   @throws java.lang.InterruptedException if the move operation has been interrupted
     */
    public abstract void move(File file, File targetFile)
    throws IOException, InterruptedException;
    
    /**
     * Copy a file 
     *
     * @param file      the file
     * @param mode      the new mode
     *
     * @throws java.io.IOException  on any io error
     * @throws java.lang.InterruptedException if the action has been interrupted
     */
    public void chmod(File file, String mode)  throws IOException, InterruptedException {
        chmod(file, mode, false);
    }
    
    /**
     * Change the file system mode of a file.
     *
     * @param file      the file
     * @param mode      the new mode
     * @param recursive include all files and subdirectories
     *
     * @throws java.io.IOException  on any io error
     * @throws java.lang.InterruptedException if the action has been interrupted
     */
    public abstract void chmod(File file, String mode, boolean recursive) throws IOException, InterruptedException;
    
    
    /**
     *     Create a symbolic link
     *
     *    @param file the file
     *    @param link the symbolic link
     *    @throws java.io.IOException on any io error
     *    @throws java.lang.InterruptedException if the action has been interrupted
     */
    public abstract void link(File file, File link) throws IOException, InterruptedException;
    
    /**
     *    Find files in a directory by its name.
     *
     *    @param dir the root directory
     *    @param filename name of the searched files
     *    @return all files in the subdirectory of <code>dir</code> with name <code>filename</code>
     */
    public static List<File> findFile(final File dir, final String filename) {
        List<File> ret = new ArrayList<File>();
        
        findFile(dir, new FilenameAndDirectoryFilter(filename), ret);
        return ret;
    }
    
    /**
     *   FileFilter which accepts a specific filename and all directories.
     */
    private static class FilenameAndDirectoryFilter implements FileFilter {
        private String filename;
        public FilenameAndDirectoryFilter(final String filename) {
            this.filename = filename;
        }
        public boolean accept(final File pathname) {
            return pathname.isDirectory()
            || pathname.getName().equals(filename);
        }
        public String getFilename() {
            return filename;
        }
    }
    
    private static void findFile(final File directory,
            final FilenameAndDirectoryFilter filter,
            final List<File> fileList) {
        File [] files = directory.listFiles(filter);
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    if (files[i].getName().equals(filter.getFilename())) {
                        fileList.add(files[i]);
                    }
                    findFile(files[i], filter, fileList);
                } else {
                    fileList.add(files[i]);
                }
            }
        }
    }
    
    
    /**
     * Seperates the command from its arguments and generates a String array. First
     * element is the command, the others are the arguments for the command.
     *
     * The seperation is done based on ' ' and '\"'. '\"' inside an argument does
     * not work. Even a quoted '\"' will not be identified. '-' will become one
     * argument, while "-* *" will be two arguments. To have it as one argument,
     * one need to specify "\"-* *\""
     *
     * @param command the command
     * @return the seperated command
     */
    public String[] splitCommand(String command) {
        
        final String SPACE = " ";
        final String QUOTE = "\"";
        
        StringTokenizer tok = new StringTokenizer(command, " \"", true);
        List<String> params = new LinkedList<String>();
        String elem = "";
        boolean isCollect = false; /* collects seperated strings as one options */
        
        /* create string tokens */
        while (tok.hasMoreTokens()) {
            String token = tok.nextToken();
            
            if (token.compareTo(SPACE) == 0) {
                if (isCollect) {
                    elem = elem + token;
                } else if (elem.length() >= 1) {
                    params.add(elem);
                    elem = "";
                }
            } else if (token.compareTo(QUOTE) == 0) {
                isCollect = !isCollect;
            } else {
                elem = elem + token;
            }
        }
        /* add the last element, which was missed in the while loop */
        if (elem.length() >= 1) {
            params.add(elem);
            elem = "";
        }
        
        log.fine("Command:" + params.toString());
        
        /* return the string array */
        String param[] = new String[params.size()];
        param = params.toArray(param);
        return param;
    }
    
    
    /**
     * Get architecture of gridengine architecture installation on the local host.
     * 
     * The path to the gridengine distribution  is taken from the system 
     * property <code>com.sun.grid.grm.sgedist</code>.
     *
     * @return the current grid engine arch string
     * @throws java.io.IOException on any IO error
     * @see #getArch(File)
     */
    public String getArch()  throws IOException {
        
        if(arch == null) {
            String prop = System.getProperty("com.sun.grid.grm.sgedist");
            if(prop == null) {
                throw new IllegalStateException(log.getResourceBundle().getString("plattform.sgedist.notset"));
            }
            File sgeDist = new File(prop);
            arch = getArch(sgeDist);
        }
        return arch;
    }
    
    /**
     * Returns true if the undelying OS is a Microsoft Windows OS.
     *
     * @return true if this method is executed on a Windows host.
     */
    public static boolean isWindowsOs() {
        String osarray[] = {
            "Windows XP"
            // <== @todo add other windows os names
        };
        boolean ret = false;
        
        String osname = System.getProperty("os.name");
        for(String name : osarray) {
            if (osname.equals(name)) {
                ret = true;
                break;
            }
        }
        return ret;
    }
    
    /**
     * Get the architecture on the local host
     * @param hedebyDist  path to the hedeby distribution
     * @throws java.io.IOException on any io error
     * @return the arch string
     */
    public abstract String getArch(File hedebyDist)  throws IOException;
    
    /**
     *  Get the pid of the jvm.
     *
     *  @return the pid of the jvm
     */
    public abstract int getPid();
    
    /**
     * Determine if the jvm is running as super user
     * @return <code>true</code> if the jvm is running as super user
     */
    public abstract boolean isSuperUser();
    
    /**
     * Send a process the kill signal
     *
     * @param  pid  id of the proccess
     * @return <code>true</code> if the send signal has been delivered
     */
    public abstract boolean killProcess(int pid);
    
    /**
     * Send a process the terminate signal
     *
     * @param  pid  id of the proccess
     * @return <code>true</code> if the send signal has been delivered
     */
    public abstract boolean terminateProcess(int pid);
    
    /**
     *   Start the default editor in this platform and edit a file.
     *   
     *   This call blocks until the user finishes editing the file
     *
     *   @param  file   the file
     *   @return <code>true</code> if the file has been edited or <code>false</code>
     *           if the editor did not modify the file
     *   @throws com.sun.grid.grm.GrmException if the editor could not be started
     */
    public abstract boolean edit(File file) throws GrmException;
    
    /**
     * Check if process exists for a given pid number
     * @param pid the id of the process to check
     * @return <code>true</code> if process exists, there was problem with 
     * reading of processes or exception was caught, otherwise <code>false</code>
     */
    public abstract boolean existsProcess(int pid);
    
    /**
     * Create temporary directory with the name based on system property "java.io.tmpdir"
     * and current time in miliseconds, the created temp directory will be deleted by the 
     * shutdown handler of this class
     * @param prefix - prefix for the dir name
     * @param suffix - suffix for the dir name
     * @return the <code>File</code> representing the created dir
     * @throws com.sun.grid.grm.GrmException if the dir could not be created
     */
    public File createTmpDir(String prefix, String suffix) throws GrmException {
        if (shutdownHook == null) {
            initShutdownHook();
        }
        if (prefix == null) {
            prefix="";
        }
        if (suffix == null) {
            suffix="";
        }
        File tmpDir = new File(System.getProperty(TEMP_DIR_PROPERTY));
        //create tmp unique filename for storing - undo purpose
        File ret = new File(tmpDir, prefix + System.currentTimeMillis() + suffix);
        int i = 0;
        while(!ret.mkdir()) {
            if (i > 100) {
                throw new GrmException("Platform.error.cant_create_tmp", BUNDLE, ret.getAbsolutePath());
            }
            ret = new File(tmpDir, ret.getName() + i);
            i++;
        }
        deleteOnExit(ret);
        return ret;
    }
    
    /**
     * Delete the directory on the virtual machine during vm shutdown
     * @param file - file that should be removed
     */
    public void deleteOnExit(File file) {
        if (shutdownHook == null) {
            initShutdownHook();
        }
        shutdownHook.deleteOnExit(file);
    }
    
    private void initShutdownHook() {
        shutdownHook = new ShutdownHook();
        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }
    
    
    
    /**
     * Shutdown handler for removing files
     */
    private class ShutdownHook extends Thread {
        private final List<File> files = new LinkedList<File>();

        @Override
        public void run() {
            for (File file : files) {
                if(log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "Platform.delTmpFile", file);
                    }
                
                try {
                    removeDir(file, true);
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, "Platform.delTmpFileFailed", file);
                } catch (IOException ex) {
                    log.log(Level.WARNING, "Platform.delTmpFileFailed", file);
                }
            }
        }
        /**
         * Register a file which will be deleted when the <code>Shutdown</code>
         * handler is running.
         * 
         * @param file  the file which will be deleted
         */
        public void deleteOnExit(File file) {
            synchronized (files) {
                files.add(file);
            }
        }
    }
    
}
