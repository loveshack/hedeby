/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.MessageFormat;

/**
 * PrintWriter which allows printing of localized messages.
 * 
 */
public class I18NPrintWriter {

    private final PrintWriter pw;

    /**
     * Create a new I18NPrintWriter.
     * 
     * @param wr the target writer
     */
    public I18NPrintWriter(Writer wr) {
        pw = new PrintWriter(wr, true);
    }

    /**
     * Create a new I18NPrintWriter.
     * 
     * @param out the target output stream
     */
    public I18NPrintWriter(OutputStream out) {
        this(new OutputStreamWriter(out));
    }

    /**
     * Get the used print writer.
     * 
     * @return the used print writer
     */
    public PrintWriter getPrintWriter() {
        return pw;
    }

    /**
     * Print a localized message.
     * 
     * @param msg         key of the message
     * @param bundleName  name of the resource bundle
     * @param params      parameters for the message
     */
    public void print(String msg, String bundleName, Object... params) {
        pw.print(I18NManager.formatMessage(msg, bundleName, params));
    }

    /**
     * Print a localized message. Appends a line feed
     * 
     * @param msg         key of the message
     * @param bundleName  name of the resource bundle
     * @param params      parameters for the message
     */
    public void println(String msg, String bundleName, Object... params) {
        pw.println(I18NManager.formatMessage(msg, bundleName, params));
    }

    /**
     * Print a message directly (without getting the message from the 
     * ResourceBundle).
     * 
     * @param msg    the message
     * @param params parameters for the message
     */
    public void printDirectly(String msg, Object... params) {
        if (params == null || params.length == 0) {
            pw.print(msg);
        } else if (msg == null) {
            pw.print("null");
        } else {
            pw.print(MessageFormat.format(msg, params));
        }
    }

    /**
     * Print a message directly (without getting the message from the 
     * ResourceBundle).
     * 
     * @param msg
     * @param params
     */
    public void printlnDirectly(String msg, Object... params) {
        if (params == null || params.length == 0) {
            pw.println(msg);
        } else if (msg == null) {
            pw.println("null");
        } else {
            pw.println(MessageFormat.format(msg, params));
        }
    }

    /**
     * Print a message directly (without getting the message from the 
     * ResourceBundle).
     * 
     * @param msg    the message
     * @param params parameters for the message
     */
    public void printfDirectly(String msg, Object... params) {
        pw.printf(msg, params);
    }
    
    /**
     * Print a character directly.
     * @param c the character
     */
    public void printDirectly(char c) {
        pw.print(c);
    }

    /**
     * Print a line feed
     */
    public void println() {
        pw.println();
    }

    /**
     * Print a localized message which is defined in printf format.
     * 
     * @param msg          key of the message
     * @param bundleName   name of the resource bundle
     * @param params       parameters for the message
     */
    public void printf(String msg, String bundleName, Object... params) { 
        pw.print(I18NManager.printfMessage(msg, bundleName, params));
    }
    
    /**
     * Flush the output.
     */
    public void flush() {
        pw.flush();
    }
    
    /**
     * Close the I18NPrintWriter
     */
    public void close() {
        pw.close();
    }
    
}
