/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import com.sun.grid.grm.GrmException;
import java.io.Serializable;
import javax.management.remote.JMXServiceURL;

/**
 *   This class is a helper class for parsing urls and strings
 *   which contains host and ports.
 *
 *   <p>This class helps to parse host and port if of a string which has the
 *   form <code>&lt;hostname&gt;[:&lt;port&gt;]".</p>
 *   <p>if the hostname inside the string is not found, the host 
 *   object is <code>null</code></p>
 *
 *   <p>For JMXServiceURL we had the problem that the <code>getHost</code> and
 *   <code>getPort</code> does not returns the correct values for JMXServiceURLs
 *   starting with <code>service:jmx:rmi:///jndi/rmi://</code>. The static methods
 *   of this helper class fixes this problem.</p>
 *   
 */
public class HostAndPort implements Serializable {
    
    private static final long serialVersionUID = 20070725092300L;
    
    private final Hostname host;
    private final int port;
    
    /** Creates a new instance of HostAndPort */
    private HostAndPort(Hostname host, int port) {
        this.host = host;
        this.port = port;
    }
    
    
    /**
     * Create a new instance
     * 
     * @return the new instance
     * @param hostname the hostname
     * @param port the port
     */
    public static HostAndPort newInstance(Hostname hostname, int port) {
        return new HostAndPort(hostname, port);
    }
    
    /**
     * Create a new instance where the hostname is resolved.
     * 
     * @return the new instance
     * @param hostname the hostname
     * @param port the port
     */
    public static HostAndPort newInstance(String hostname, int port) {
        return new HostAndPort(Hostname.getInstance(hostname), port);
    }
    
    /**
     * Parse hostname of port of a string and return a new instance where
     * hostname is resolved.
     * 
     * 
     * 
     * @return the new instance
     * @param hostAndPort string in the form <hostname>[:<port>]
     * @throws java.lang.NumberFormatException if port is not a number
     */
    public static HostAndPort newInstance(String hostAndPort) throws NumberFormatException {
        int index = hostAndPort.indexOf(':');
        if(index > 0) {
            String host = hostAndPort.substring(0, index);
            int port = Integer.parseInt(hostAndPort.substring(index+1));
            return newInstance(host, port);
        } else if (index == 0) {
            int port = Integer.parseInt(hostAndPort.substring(index+1));
            return new HostAndPort(null, port);
        } else { // index < 0
            return newInstance(hostAndPort, 0);
        }
    }
    
    private static String getHostnameAndPortPartFromURL(JMXServiceURL url) {

        String str = url.toString();
        if(str.startsWith("service:jmx:rmi:///jndi/rmi://")) {
            int startIndex = "service:jmx:rmi:///jndi/rmi://".length();
            int index = str.indexOf('/', startIndex);
            
            if(index < 0) {
                str = str.substring(startIndex);
            } else {
                str = str.substring(startIndex, index);
            }
            return str;
        } else {
            return null;
        }
    }
    
    
    /**
     * Parse host and port out of a <code>JMXServiceURL</code> and return
     * a new instance where the hostname is resolved.
     * 
     * @return host and port object
     * @param url the JMXServiceURL the url
     * @throws com.sun.grid.grm.GrmException if the hostname can not be resolved
     * @throws java.lang.NumberFormatException if the port is not a number
     */
    public static HostAndPort newInstance(JMXServiceURL url) throws NumberFormatException {
        
        String hostAndPort = getHostnameAndPortPartFromURL(url);
        
        if(hostAndPort == null) {
            return newInstance(url.getHost(), url.getPort());
        } else {
            return newInstance(hostAndPort);
        }
        
    }
    
    /**
     * Get the host object.
     *
     * @return the host object
     */
    public Hostname getHost() {
        return host;
    }

    /**
     * get the port.
     *
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     *  compare <code>obj</code> with this.
     *  @param obj the object
     *  @return <code>true</code> if obj is an instanceof <code>HostAndPort</code>
     *         and port and host are the same
     *
     *  @see Hostname#equals
     */
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof HostAndPort) {
            HostAndPort hp = (HostAndPort)obj;
            if(port == hp.port) {
                if(host == null) {
                    return hp.host == null;
                } else {
                    return host.equals(hp.host);
                }
            }
        }
        return false;
    }


    /**
     * Get the hash code of this object
     * @return the hashcode of this object
     */
    @Override
    public int hashCode() {
        if(host == null) {
            return port;
        } else {
            return port + 31 * host.hashCode();
        }
    }

    @Override
    public String toString() {
        return host + ":" + port;
    }
    
    
    
    
}
