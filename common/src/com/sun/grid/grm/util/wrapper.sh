#!/bin/sh

#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__
cd @@@DIR@@@
timeout=@@@TIMEOUT@@@
CMD="@@@CMD@@@"

IN_FILE=@@@IN_FILE@@@
PID_FILE=@@@PID_FILE@@@
OUT_FILE=@@@OUT_FILE@@@
ERR_FILE=@@@ERR_FILE@@@
RES_FILE=@@@RES_FILE@@@

echo $$ > $PID_FILE

print_pids() {
    for child in `ps -e -o 'pid ppid' | awk '$2 == '$1' { print $1 }'` ; do
        print_pids $child
    done
    echo $1
}

cancel_script() {
    if [ -n "$PID" ]; then
       ppid=`ps -p $PID -o ppid | tail -1 | sed 's#[^0-9]##g'`
       if [ $ppid -eq $$ ]; then
          pid_list=`print_pids $PID`
          if [ -n "$pid_list" ]; then
             kill -TERM $pid_list
          fi
       fi
    fi
    rm -f $PID_FILE
    exit 2
}

trap "cancel_script" ALRM

cat << EO_WATCHDOG | sh &
   trap "exit 0" ALRM
   count=$timeout
   while : ; do
       if [ ! -f $PID_FILE ]; then
          exit 0
       fi
       if [ \$count -le 0 ]; then
           break
       fi
       sleep 1
       count=\`expr \$count - 1\`
   done
   ppid=\`ps -p \$\$ -o ppid | tail -1 | sed 's#[^0-9]##g'\`
   if [ -f $PID_FILE -a \$ppid -eq $$ ]; then
       kill -ALRM $$
   fi
EO_WATCHDOG
WATCHDOG_PID=$!

cat << EO_CMD | sh > /dev/null 2>&1  &
    if [ -f $IN_FILE ]; then
       ( $CMD ) <$IN_FILE 1>$OUT_FILE 2>$ERR_FILE
    else
       ( $CMD ) 1>$OUT_FILE 2>$ERR_FILE
    fi
EO_CMD
PID=$!
wait $PID
res=$?
PID=
rm -f $PID_FILE
kill -ALRM $WATCHDOG_PID > /dev/null 2>&1
echo $res > $RES_FILE