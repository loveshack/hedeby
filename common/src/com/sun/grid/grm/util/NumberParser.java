/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import java.text.NumberFormat;

/**
 *  Helper class parsing and formatting numbers
 */
public class NumberParser {
    
    /** The binary giga multiplier (2^30) */
    public static final int BINARY_GIGA  = 1024 * 1024 * 1024;
    
    /** The binary mega multiplier (2^20) */
    public static final int BINARY_MEGA  = 1024 * 1024;
    
    /** The binary kilo multiplier (2^10) */
    public static final int BINARY_KILO  = 1024;
    
    /** The decimal kilo multiplier (10^9) */
    public static final int DECIMAL_GIGA = 1000000000;
    
    /** The decimal mega multiplier (10^6) */
    public static final int DECIMAL_MEGA = 1000000;
    
    /** The kilo mega multiplier (10^3) */
    public static final int DECIMAL_KILO = 1000;

    private static final Integer [] MULTIPLIERS_FOR_FORMAT = {
        BINARY_GIGA, BINARY_MEGA, BINARY_KILO
    };
    
    /**
     * Maximum number of fraction digits
     */
    public final static int MAX_FRACTION_DIGITS = 340;
    
    /**
     * Parse a double value
     * @param value the value
     * @return the double
     * @throws java.lang.NumberFormatException if value does not contain a double
     */
    public static Double parseDouble(String value) throws NumberFormatException {
        if (value == null) {
            return 0.0;
        }
        value = value.trim();
        if (value.length() == 0) {
            return 0.0;
        } else if (value.equalsIgnoreCase("infinity") || value.equalsIgnoreCase("inf")) {
            return Double.POSITIVE_INFINITY;
        } else if (value.equalsIgnoreCase("NaN")) {
            return Double.NaN;
        } else {
            Integer multiplier = null;

            int len = value.length();

            if (len > 1) {
                char suffix = value.charAt(len - 1);

                multiplier = getMultiplier(suffix);
            }

            if (multiplier != null) {
                value = value.substring(0, len - 1);
                return multiplier.doubleValue() * Double.parseDouble(value);
            } else {
                return Double.parseDouble(value);
            }
        }
    }

    /**
     * Parse a float value
     * @param value the value
     * @return the float
     * @throws java.lang.NumberFormatException if value does not contain a float
     */
    public static Float parseFloat(String value) throws NumberFormatException {
        if (value == null) {
            return 0.0f;
        }
        value = value.trim();
        if (value.length() == 0) {
            return 0.0f;
        } else if (value.equalsIgnoreCase("infinity") || value.equalsIgnoreCase("inf")) {
            return Float.POSITIVE_INFINITY;
        } else if (value.equalsIgnoreCase("NaN")) {
            return Float.NaN;
        } else {
            Integer multiplier = null;

            int len = value.length();

            if (len > 1) {
                char suffix = value.charAt(len - 1);

                multiplier = getMultiplier(suffix);
            }

            if (multiplier != null) {
                value = value.substring(0, len - 1);
                return multiplier.floatValue() * Float.parseFloat(value);
            } else {
                return Float.parseFloat(value);
            }
        }
    }
    
    /**
     * Parse a long value
     * @param value the value
     * @return the long
     * @throws java.lang.NumberFormatException if value does not contain a long
     */
    public static Long parseLong(String value) throws NumberFormatException {
        if (value == null) {
            return 0L;
        }
        value = value.trim();
        if (value.length() == 0) {
            return 0L;
        } else {
            Integer multiplier = null;

            int len = value.length();

            if (len > 1) {
                char suffix = value.charAt(len - 1);

                multiplier = getMultiplier(suffix);
            }

            if (multiplier != null) {
                value = value.substring(0, len - 1);
                return multiplier.longValue() * Long.parseLong(value);
            } else {
                return Long.parseLong(value);
            }
        }
    }
    
    /**
     * Parse a int value
     * @param value the value
     * @return the int
     * @throws java.lang.NumberFormatException if value does not contain a int
     */
    public static Integer parseInteger(String value) throws NumberFormatException {
        if (value == null) {
            return 0;
        }
        value = value.trim();
        if (value.length() == 0) {
            return 0;
        } else {
            Integer multiplier = null;

            int len = value.length();

            if (len > 1) {
                char suffix = value.charAt(len - 1);

                multiplier = getMultiplier(suffix);
            }

            if (multiplier != null) {
                value = value.substring(0, len - 1);
                long ret = multiplier.longValue() * Long.parseLong(value);
                if (ret > Integer.MAX_VALUE) {
                    // TODO i18n
                    throw new NumberFormatException(value + " is too large for an integer");
                }
                return (int)ret;
            } else {
                return Integer.parseInt(value);
            }
        }
    }

    /**
     * Parse a short value
     * @param value the value
     * @return the short
     * @throws java.lang.NumberFormatException if value does not contain a short
     */
    public static Short parseShort(String value) throws NumberFormatException {
        if (value == null) {
            return 0;
        }
        value = value.trim();
        if (value.length() == 0) {
            return 0;
        } else {
            Integer multiplier = null;

            int len = value.length();

            if (len > 1) {
                char suffix = value.charAt(len - 1);

                multiplier = getMultiplier(suffix);
            }

            if (multiplier != null) {
                value = value.substring(0, len - 1);
                long ret = multiplier.longValue() * Long.parseLong(value);
                if (ret > Short.MAX_VALUE) {
                    // TODO i18n
                    throw new NumberFormatException(value + " is too large for a short");
                }
                return (short)ret;
            } else {
                return Short.parseShort(value);
            }
        }
    }
    
    /**
     * Format a short to a string
     * @param value the short value
     * @return the string
     */
    public static String format(short value) {
        if (value == 0) {
            return "0";
        }
        Character suffix = null;
        for(int multiplier: MULTIPLIERS_FOR_FORMAT) {
            if (value % multiplier == 0) {
                suffix = getSuffixForMultiplier(multiplier);
                value = (short)(value / multiplier);
                break;
            }
        }      
        String ret = Short.toString(value);
        if (suffix != null) {
            StringBuilder sb = new StringBuilder(ret.length()+1);
            sb.append(ret);
            sb.append(suffix);
            ret = sb.toString();
        }
        return ret;
    }
    
    /**
     * Format a int to a string
     * @param value the int value
     * @return the string
     */
    public static String format(int value) {
        if (value == 0) {
            return "0";
        }
        Character suffix = null;
        for(int multiplier: MULTIPLIERS_FOR_FORMAT) {
            if (value % multiplier == 0) {
                suffix = getSuffixForMultiplier(multiplier);
                value = value / multiplier;
                break;
            }
        }      
        String ret = Integer.toString(value);
        if (suffix != null) {
            StringBuilder sb = new StringBuilder(ret.length()+1);
            sb.append(ret);
            sb.append(suffix);
            ret = sb.toString();
        }
        return ret;
    }
    
    /**
     * Format a long to a string
     * @param value the long value
     * @return the string
     */
    public static String format(long value) {
        if (value == 0) {
            return "0";
        }
        Character suffix = null;
        for(int multiplier: MULTIPLIERS_FOR_FORMAT) {
            if (value % multiplier == 0) {
                suffix = getSuffixForMultiplier(multiplier);
                value = value / multiplier;
                break;
            }
        }      
        String ret = Long.toString(value);
        if (suffix != null) {
            StringBuilder sb = new StringBuilder(ret.length()+1);
            sb.append(ret);
            sb.append(suffix);
            ret = sb.toString();
        }
        return ret;
    }
    
    /**
     * Format a double to a string
     * @param value the double value
     * @return the string
     */
    public static String format(double value) {
        if (Double.isNaN(value) || Double.isInfinite(value)) {
            return Double.toString(value);
        } 
        if (Math.abs(value) < Double.MIN_VALUE) {
            return "0";
        }
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(MAX_FRACTION_DIGITS);
        format.setMinimumFractionDigits(0);
        format.setGroupingUsed(false);
        Character suffix = null;
        for(int multiplier: MULTIPLIERS_FOR_FORMAT) {
            if (Math.abs(value % multiplier) < Double.MIN_VALUE) {
                suffix = getSuffixForMultiplier(multiplier);
                value = value / multiplier;
                break;
            }
        }      
        String ret = format.format(value);
        if (suffix != null) {
            StringBuilder sb = new StringBuilder(ret.length()+1);
            sb.append(ret);
            sb.append(suffix);
            ret = sb.toString();
        }
        return ret;
    }
    
    /**
     * Format a value to a string
     * @param value the value value
     * @return the string
     */
    public static String format(float value) {
        
        if (Float.isNaN(value) || Float.isInfinite(value)) {
            return Float.toString(value);
        }
        if (Math.abs(value) < Float.MIN_VALUE) {
            return "0";
        }
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setMaximumFractionDigits(MAX_FRACTION_DIGITS);
        format.setMinimumFractionDigits(0);
        format.setGroupingUsed(false);
        Character suffix = null;
        for(int multiplier: MULTIPLIERS_FOR_FORMAT) {
            if (Math.abs(value % multiplier) < Float.MIN_VALUE) {
                suffix = getSuffixForMultiplier(multiplier);
                value = value / multiplier;
                break;
            }
        }      
        String ret = format.format(value);
        if (suffix != null) {
            StringBuilder sb = new StringBuilder(ret.length()+1);
            sb.append(ret);
            sb.append(suffix);
            ret = sb.toString();
        }
        return ret;
    }
    
    
    private static char getSuffixForMultiplier(int multiplier) {
        switch(multiplier) {
            case DECIMAL_MEGA: return 'm';
            case BINARY_MEGA:  return 'M';
            case DECIMAL_KILO: return 'k';
            case BINARY_KILO:  return 'K';
            case DECIMAL_GIGA: return 'g';
            case BINARY_GIGA:  return 'G';
            default:
                throw new IllegalArgumentException("unknown multipier " + multiplier);
        }
    } 

    private static Integer getMultiplier(char suffix) {

        switch (suffix) {
            case 'm':
                return DECIMAL_MEGA;
            case 'M':
                return BINARY_MEGA;
            case 'k':
                return DECIMAL_KILO;
            case 'K':
                return BINARY_KILO;
            case 'g':
                return DECIMAL_GIGA;
            case 'G':
                return BINARY_GIGA;
            default:
                return null;
        }
    }
}
