/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Set;

/**
 * Class that models a dependency tree that is needed in XMLUtil for
 * schema creation.
 *
 * Each node in the tree has a name (and the dependencies are to this name),
 * and some data of type <pre><T></pre> is stored in each node.
 *
 * The tree allows no cycles and no dangling references. This is checked
 * whenever the tree is iterated. The order of iteration is done in such a
 * way that NO forward references are needed to resolve the dependencies.
 * In other words, if element A is output before element B in the iteration,
 * element A can NOT depend on element B.
 *
 * @param <T> the type of object data to store in the tree
 */
public class DependencyTree<T> implements Iterable<T> {

    private final List<Node> nodes = new LinkedList<Node>();

    private class Node {

        private T data;
        private final String name;
        private final Set<Node> dependsOn = new HashSet<Node>();

        public Node(String name) {
            super();
            if (name == null) {
                throw new IllegalArgumentException("name must not be null");
            }
            this.name = name;
            this.data = null;
        }

        public void setData(T data) {
            this.data = data;
        }

        public T getData() {
            return data;
        }

        public boolean hasData() {
            return data != null;
        }

        public String getName() {
            return name;
        }

        public void addDependency(Node dep) {
            dependsOn.add(dep);
        }

        public void eliminateUnnecessaryDependencies() {
            Collection<Node> toRemove = new HashSet<Node>();
            for (Node n : dependsOn) {
                for (Node n2 : dependsOn) {
                    if (n.doesDependOn(n2)) {
                        toRemove.add(n2);
                    }
                }
            }
            for (Node rem : toRemove) {
                dependsOn.remove(rem);
            }
        }

        public boolean doesDirectlyDependOn(Node dep) {
            return dependsOn.contains(dep);
        }

        public boolean doesDependOn(Node dep) {
            return doesDependOn(dep, new HashSet<Node>());
        }

        private boolean doesDependOn(Node dep, Collection<Node> seen) {
            if (dependsOn.contains(dep)) {
                return true;
            }
            for (Node n : dependsOn) {
                if (seen.contains(n)) {
                    throw new IllegalStateException("Detected cycle between Node " + this + " and Node " + n);
                } else {
                    seen.add(n);
                    if (n.doesDependOn(dep, seen)) {
                        return true;
                    }
                }
            }
            return false;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(name);
            sb.append("=>");
            sb.append((data == null) ? "null" : data.toString());
            sb.append("]");
            return sb.toString();
        }

        public boolean hasDependencies() {
            return dependsOn.size() > 0;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Node other = (Node) obj;
            return this.name.equals(other.name);
        }

        @Override
        public int hashCode() {
            return this.name.hashCode();
        }
    }

    private class DependecyTreeBreadthFirstIterator implements Iterator<T> {

        private final Queue<Node> queue;

        private DependecyTreeBreadthFirstIterator() {
            super();
            queue = new LinkedList<Node>();
            for (Node n : nodes) {
                if (!n.hasDependencies()) {
                    queue.add(n);
                }
            }
            if (queue.isEmpty() && !nodes.isEmpty()) {
                throw new IllegalStateException("There is no node without dependencies.");
            }
        }

        public boolean hasNext() {
            return !queue.isEmpty();
        }

        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            Node node = queue.poll();
            queue.addAll(getNodesDirectlyDependingOn(node));
            return node.getData();
        }

        public void remove() {
            throw new UnsupportedOperationException("DependecyTreeBreadthFirstIterator does not support removal");
        }
    }

    /**
     * The size of the dependency tree.
     * @return the size
     */
    public int size() {
        return nodes.size();
    }

    public void add(String name, T data, List<String> dependencies) {
        Node node = getNode(name);
        node.setData(data);
        for (String dep : dependencies) {
            node.addDependency(getNode(dep));
        }
    }

    public Iterator<T> iterator() {
        checkConsistency();
        eliminateUnnecessaryDependencies();
        return new DependecyTreeBreadthFirstIterator();
    }

    private Collection<Node> getNodesDirectlyDependingOn(Node node) {
        Collection<Node> ret = new LinkedList<Node>();
        for (Node n : nodes) {
            if (n.doesDirectlyDependOn(node)) {
                ret.add(n);
            }
        }
        return ret;
    }

    private void eliminateUnnecessaryDependencies() {
        for (Node n : nodes) {
            n.eliminateUnnecessaryDependencies();
        }
    }

    private void checkConsistency() {
        for (Node n : nodes) {
            if (!n.hasData()) {
                throw new IllegalStateException("Node " + n + " has no data.");
            }
        }
    }

    private Node getNode(String name) {
        Node ret = new Node(name);
        int index = nodes.indexOf(ret);
        if (index < 0) {
            nodes.add(ret);
        } else {
            ret = nodes.get(index);
        }
        return ret;
    }
}
