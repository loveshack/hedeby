/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Pump the content of a stream line by line into
 * a list.
 *
 */
public class StreamPump extends Thread {
    
    private Logger logger;
    
    private static final int BUF_SIZE = 1024;
    private static final String NL = System.getProperty("line.separator");
    private Reader in = null;
    private List<String> messages = null;
    private String tag;
    
    /**
     * Create a new StreamPump
     * @param aIn     the input stream
     * @param aList   the target list
     * @param tag     tag for logging
     * @param logger  the used logger
     */
    public StreamPump(InputStream aIn, List<String> aList, String tag, Logger logger) {
        in = new InputStreamReader(aIn);
        messages = aList;
        this.tag = tag;
        this.logger = logger;
    }
    
    /**
     * the main method for the thread
     */
    @Override
    public void run() {
        String line;
        try {
            char[] buf = new char[BUF_SIZE];
            int len = 0;
            
            StringBuilder sbuf = new StringBuilder();
            
            while ((len = in.read(buf)) > 0) {
                sbuf.append(buf, 0, len);
                int index = 0;
                while((index = sbuf.indexOf(NL)) > 0) {
                    line = sbuf.substring(0, index);
                    sbuf.delete(0, index+1);
                    if (messages != null) {
                        messages.add(line);
                    }
                    if(logger.isLoggable(Level.FINER)) {
                        logger.finer(tag + ": " + line);
                    }
                }
            }
            
            if (sbuf.length() > 0) {
                line = sbuf.toString();
                if (messages != null) {
                    messages.add(line);
                }
                if(logger.isLoggable(Level.FINER)) {
                    logger.finer(tag + ": " + line);
                }
            }
        } catch (IOException e) {
            logger.throwing("StreamPump", "run", e);
        } finally {
            try {
                in.close();
            } catch(Exception ex) {
                // Ignore
            }
        }
        logger.exiting("StreamPump", "run");
    }
    
}
