/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import com.sun.grid.grm.GrmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  The <code>StateMachine<code> allows a generic way to define
 *  state transistions.
 * 
 *  <h3>Usage:</h3>
 * 
 *  <pre>
 *    enum State {
 *        UNO, DUE
 *    }
 * 
 *    StateMachine<State> stateMachine = new StateMachine<State>();
 * 
 *    StateMachine<State>.Instance state stateMachine.newInstance(State.UNO);
 * 
 *    State oldState = state.setState(DUE);
 * 
 *  <pre>
 */
public class StateMachine<S extends Enum>  {
    
    private final static String BUNDLE = "com.sun.grid.grm.util.util";
    
    private final Map<S,List<S>> stateMap = new HashMap<S,List<S>>();
    
    /**
     * Creates a new instance of StateMachine
     */
    public StateMachine() {
    }
    
    
    /**
     * Define a new state transisition for this state maschine.
     *
     * @param state   the original state
     * @param followStates the allowed followups
     */
    public void addState(S state, S... followStates) {
        addState(state, Arrays.asList(followStates));
    }
    
    /**
     * Define a new state transisition for 
     *
     * @param state   the original state
     * @param followStates list of allowed followups
     */
    public void addState(S state, List<S> followStates) {
       stateMap.put(state, followStates);    
    }
    
    /**
     * Create a new instance of a state holder for this
     * <code>StateMachine</code>.
     * 
     * @param initialState the initial state for the state holder
     * @return the new state holder
     */
    public Instance newInstance(S initialState) {
        return new Instance(initialState);
    }
    
    /**
     *  This class holds the current set for the state maschine
     *  Transistion of state are done in the observation of the
     *  state maschine
     */
    public class Instance {
        
        private S state; 
        
        private Instance(S initialState) {
            state = initialState;
        }

        
        /**
         * Perform a state tranisition from the current state.
         * to <code>newState</code>
         * @param newState  the new state
         * @throws InvalidStateTransistionException if the state transistion is not allowed
         * @return the old state
         */
        public synchronized S setState(S newState) throws InvalidStateTransistionException {
            
            if(newState.equals(state)) {
                return state;
            }
            
            if(!stateMap.containsKey(newState)) {
                throw new IllegalArgumentException("Unknown state " + newState);
            }
            
            List<S>followStates = stateMap.get(state);
            if(followStates == null) {
                throw new IllegalArgumentException("Unknown state " + state);
            }
            
            if(!followStates.contains(newState)) {
                throw new InvalidStateTransistionException(state, newState);
            }
            S oldState = state;
            state = newState;
            return oldState;
        }
        
        /**
         * Get the current state
         * @return the current state
         */
        public synchronized S getState() {
            return state;
        }
    } 
    
    
}
