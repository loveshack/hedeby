/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

/**
 * Wrapper class for all main classes.
 *
 *
 * <p>This class is main class of the jar file sdm-starter.jar. It can be started
 *    with the following command line:</p>
 * 
 * <pre>
 * % java -jar &lt;dist&gt;/lib/sdm-starter.jar &lt;main_class&gt; &lt;args&gt;
 * </pre>
 *
 * <p>The <code>hedeby_starter.jar</code> contains only this class. All other classes
 * are loaded by additional <code>ClassLoader</code>s.</p>
 * <p>It searches the sdm-common.jar file in the same directory as the 
 * <code>sdm-starter.jar</code> is stored. If it is found it laad the class 
 * {@link com.sun.grid.grm.cli.SystemFinder} using a new <code>ClassLoader</code>.
 * The <code>SystemFinder</code> tries to find the dist directory of the system by
 * evaluating the <code>-system</code> and <code>-prefs</code> options. 
 * If it is found the <code>sdm-common.jar</code> of the concrete system is 
 * created and the command is executes within this new <code>ClassLoader</code>. 
 * 
 * This functionality makes it possible to start different versions of cli 
 * commands within the same JVM.</p>
 *
 *
 *
 */
public class MainWrapper {

    public final static String BUNDLE = "com.sun.grid.grm.util.MainWrapper";
    private final static String BUNDLE_PATH = BUNDLE.replace('.', '/') + ".properties";
    private final static String SYSTEM_FINDER_CLASSNAME = "com.sun.grid.grm.cli.SystemFinder";
    
    private static void checkJavaVersion() {
        
        String version = System.getProperty("java.version");
        
        StringTokenizer st = new StringTokenizer(version, ".");
        
        int major = 0;
        if(st.hasMoreTokens()) {
            major = Integer.parseInt(st.nextToken());
        } else {
            throw new IllegalStateException("java reports an invalid version string '" + version + "'");
        }
        int minor = 0;
        if(st.hasMoreTokens()) {
            minor =  Integer.parseInt(st.nextToken());
        }
        if(major != 1 || minor < 6) {
            throw new IllegalStateException(format("InvalidJavaVersion", version));
        }
    }
    
    
    private static ResourceBundle rb() {
        return ResourceBundle.getBundle(BUNDLE);
    }
    
    private static String format(String msg, Object obj) {
        return format(msg, new Object[] { obj });
    }
    
    private static String format(String msg, Object [] obj) {
        try {
            msg = rb().getString(msg);
        } catch(MissingResourceException ex) {
            // Ignore
        }
        return MessageFormat.format(msg, obj);
    }
    
    
    private final static String HEDEBY_COMMON_JAR = "sdm-common.jar";
    
    private static URL getJarUrl() {
        URL resource = MainWrapper.class.getClassLoader().getResource(BUNDLE_PATH);
        if(resource == null) {
            throw new IllegalStateException(format("propertyFileNotFound", BUNDLE));
        }
        String path = resource.getPath();
        int index = path.lastIndexOf('!');
        if(index >= 0) {
            path = path.substring(0, index);
        }
        index = path.lastIndexOf('/');
        if(index < 0) {
            throw new IllegalStateException(format("invalidPathToDist", path));
        }
        path = path.substring(0,index) + "/" + HEDEBY_COMMON_JAR;
        
        try {
            return new URL(path);
        } catch (MalformedURLException ex) {
            throw new IllegalStateException(format("invalidURLtoDist", path));
        }
    }
    
    public static void main(String [] args) {
        try {
            run(args);
        } catch(Throwable th) {
            th.printStackTrace();
            System.exit(1);
        }
    }
    
    private static void handleException(Throwable t) {
        
        if(t.getClass().getName().equals("com.sun.grid.grm.GrmException")) {
            System.err.println("Error: "+t.getLocalizedMessage());
            
            try {
                Method getExitCodeMethod = t.getClass().getMethod("getExitCode", null);
                Integer exitCode = (Integer)getExitCodeMethod.invoke(t, null);
                System.exit(exitCode.intValue());
            } catch(Exception ex1) {
                System.exit(1);
            }
        } else {
            t.printStackTrace();
            System.exit(1);
        }
    }
            
            
    private static void run(String [] args) {
        
        try {
            checkJavaVersion();
        } catch(IllegalStateException ex) {
            System.err.println(ex.getMessage());
            System.exit(1);
        }


        
        URL jar = null;
        try {
            jar = getJarUrl();
        } catch(IllegalStateException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        
        String mainClassname = args[0];
        
        String [] newArgs = new String[args.length - 1];
        for(int i = 0; i < newArgs.length; i++) {
            newArgs[i] = args[i+1];
        }
        
        URLClassLoader cl = new URLClassLoader(new URL[] { jar });
        
        SystemFinderThread sysFinder = new SystemFinderThread(cl, newArgs);
        
        sysFinder.start();
        try {
            sysFinder.join();
        } catch (InterruptedException ex) {
            handleException(ex);
        }
        
        List systemClasspath = sysFinder.getSystemClasspath();
        URL [] urls = new URL[systemClasspath.size()];
        systemClasspath.toArray(urls);
        URLClassLoader realClassLoader = new URLClassLoader(urls);
            
        Class mainClass = null;
        try {
            mainClass = realClassLoader.loadClass(mainClassname);
        } catch (ClassNotFoundException ex) {
            System.out.println(format("classNotFoundInJar", new Object [] { mainClassname, getClasspath(realClassLoader) }));
            System.out.println(format("useJar", jar));
            realClassLoader = cl;
            
            try {
                mainClass = realClassLoader.loadClass(mainClassname);
            } catch (ClassNotFoundException ex1) {
                System.out.println(format("classNotFoundInJar", new Object [] { mainClassname, jar }));
                System.exit(1);
            } 
        }
        SystemRunThread sysRunThread = new SystemRunThread(realClassLoader,mainClassname,newArgs);
        sysRunThread.start();
    } 
        
    private static String getClasspath(URLClassLoader cl) {
        StringBuffer buf = new StringBuffer();
        boolean first;
        URL [] urls = cl.getURLs();
        for(int i = 0; i< urls.length; i++) {
            if(i>0) {
                buf.append(File.pathSeparatorChar);
            }
            buf.append(urls[i]);
        }
        return buf.toString();
    }
    static class JarFilenameFilter implements FilenameFilter {
        
        private final static String [] EXCLUDES = { "sdm-starter.jar" };
        
        public boolean accept(File dir, String name) {
            boolean ret = false;
            if(name.endsWith(".jar")) {
                ret = true;
                for(int i = 0; i < EXCLUDES.length; i++) {
                    if(EXCLUDES[i].equalsIgnoreCase(name)) {
                        ret = false;
                        break;
                    }
                }
            }
            return ret;
        }
    }
    
    static class SystemFinderThread extends Thread {
        
        private File distPath;
        private List systemClasspath;
        private boolean securityDisabled;
        private String realSystemName;
        private String [] args;
        private URLClassLoader cl;
        public SystemFinderThread(URLClassLoader cl, String [] args) {
            super("system-finder");
            this.args = args;
            setContextClassLoader(cl);
            this.cl = cl;
        }
        
        public void run() {
            
            Class systemFinderClass = null;
            try {
                systemFinderClass = cl.loadClass(SYSTEM_FINDER_CLASSNAME);
            } catch (ClassNotFoundException ex) {
                System.out.println(format("classNotFoundInJar", new Object [] { SYSTEM_FINDER_CLASSNAME, getClasspath(cl)}));
                System.exit(1);
            }
            
            Constructor cons = null;
            try {
                cons = systemFinderClass.getConstructor(new Class [] {String[].class});
            } catch (NoSuchMethodException ex) {
                handleException(ex);
            }
            
            Object systemFinder = null;
            try {
                systemFinder = cons.newInstance(new Object [] {args});
            } catch (IllegalArgumentException ex) {
                handleException(ex);
            } catch (InstantiationException ex) {
                handleException(ex);
            } catch (IllegalAccessException ex) {
                handleException(ex);
            } catch (InvocationTargetException ex) {
                handleException(ex.getTargetException());
            }
            
            Method getDistPathMethod = null;
            try {
                getDistPathMethod = systemFinderClass.getMethod("getDistPath", null);
            } catch (NoSuchMethodException ex) {
                handleException(ex);
            }
            
            Method getSystemClasspathMethod = null;
            try {
                getSystemClasspathMethod = systemFinderClass.getMethod("getSystemClasspath", null);
            } catch (NoSuchMethodException ex) {
                handleException(ex);
            }
            
            Method isSecurityDisabledMethod = null;
            try {
                isSecurityDisabledMethod = systemFinderClass.getMethod("isSecurityDisabled", null);
            } catch (NoSuchMethodException ex) {
                handleException(ex);
            }
            
            Method getRealSystemNameMethod = null;
            try {
                getRealSystemNameMethod = systemFinderClass.getMethod("getRealSystemName", null);
            } catch (NoSuchMethodException ex) {
                handleException(ex);
            }
            
            distPath = null;
            try {
                distPath = (File) getDistPathMethod.invoke(systemFinder, null);
            } catch (IllegalArgumentException ex) {
                handleException(ex);
            } catch (InvocationTargetException ex) {
                handleException(ex.getTargetException());
            } catch (IllegalAccessException ex) {
                handleException(ex);
            }
            
            systemClasspath = Collections.EMPTY_LIST;
            try {
                systemClasspath = (List) getSystemClasspathMethod.invoke(systemFinder, null);
            } catch (IllegalArgumentException ex) {
                handleException(ex);
            } catch (InvocationTargetException ex) {
                handleException(ex.getTargetException());
            } catch (IllegalAccessException ex) {
                handleException(ex);
            }
            
            
            try {
                securityDisabled = ((Boolean) isSecurityDisabledMethod.invoke(systemFinder, null)).booleanValue();
            } catch (IllegalArgumentException ex) {
                handleException(ex);
            } catch (InvocationTargetException ex) {
                handleException(ex.getTargetException());
            } catch (IllegalAccessException ex) {
                handleException(ex);
            }
            
            
            realSystemName = null;

            try {
                realSystemName = (String) getRealSystemNameMethod.invoke(systemFinder, null);
            } catch (IllegalArgumentException ex) {
                handleException(ex);
            } catch (InvocationTargetException ex) {
                handleException(ex.getTargetException());
            } catch (IllegalAccessException ex) {
                handleException(ex);
            }
        }

        public File getDistPath() {
            return distPath;
        }

        public boolean isSecurityDisabled() {
            return securityDisabled;
        }

        public String getRealSystemName() {
            return realSystemName;
        }

        public List getSystemClasspath() {
            return systemClasspath;
        }
    }
    
    static class SystemRunThread extends Thread {
        private String mainClassname;
        private String [] args;
        private URLClassLoader classLoader;
        
        public SystemRunThread(URLClassLoader cl, String mainClassname, String [] args) {
            super("runner");
            this.mainClassname = mainClassname;
            this.args = args;
            setContextClassLoader(cl);
            this.classLoader = cl;
        }
        
        public void run() {
            Class mainClass = null;
            try {
                mainClass = getContextClassLoader().loadClass(mainClassname);
            } catch (ClassNotFoundException ex) {
                System.out.println(format("classNotFoundInJar", new Object [] { mainClassname, getClasspath(classLoader) }));
                System.exit(1);
            }
            
            Method mainMethod = null;
            try {
                mainMethod = mainClass.getMethod("main", new Class [] { String[].class });
            } catch (NoSuchMethodException ex) {
                handleException(ex);
            }
            try {
                mainMethod.invoke(mainClass, new Object [] { args });
            } catch (IllegalArgumentException ex) {
                handleException(ex);
            } catch (InvocationTargetException ex) {
                handleException(ex.getTargetException());
            } catch (IllegalAccessException ex) {
                handleException(ex);
            }
        }
    }
}
