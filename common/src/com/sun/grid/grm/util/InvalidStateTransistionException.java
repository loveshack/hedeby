/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;

import com.sun.grid.grm.GrmException;

/**
 * This exeception is throws if an invalid state transistion has been
 * made on an object.
 *
 */
public class InvalidStateTransistionException extends GrmException {
    
    private final static long serialVersionUID = -2008110601L;
    
    private final static String BUNDLE = "com.sun.grid.grm.util.util";
    private final Enum orgState;
    private final Enum newState;
    
    /** Creates a new instance of InvalidStateTransistion
     * @param orgState
     * @param newState 
     */
    public InvalidStateTransistionException(Enum orgState, Enum newState) {
        super("stateMaschine.illegalStateTransition", BUNDLE, orgState, newState);
        this.orgState = orgState;
        this.newState = newState;
    }

    /**
     * Create a new instance of the InvalidStateTransistionException
     * @param orgState  the original state
     * @param newState  the new state
     * @param message   the message
     * @param BUNDLE    the resource bundle
     * @param params    the parameters
     */
    public InvalidStateTransistionException(Enum orgState, Enum newState, String message, String BUNDLE, Object... params) {
        super(message,BUNDLE,params);
        this.orgState = orgState;
        this.newState = newState;
    }
    
    public InvalidStateTransistionException(String message, String BUNDLE, Object... params) {
        super(message,BUNDLE,params);
        orgState = null;
        newState = null;
    }
    
    public InvalidStateTransistionException(String message) {
        super(message);
        orgState = null;
        newState = null;
    }

    /**
     * The original state of the object.
     * @return the original state of the object
     */
    public Enum getOrgState() {
        return orgState;
    }

    /**
     * The requsted state of the object.
     * @return return the requsted of the object
     */
    public Enum getNewState() {
        return newState;
    }
    
}
