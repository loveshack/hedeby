/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.util;


import java.io.*;
import java.net.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.MissingResourceException;
import java.util.StringTokenizer;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

/**
 * This Formater formats LogRecords into one line. What columns and
 * delimiters are formated can be configured.
 * The default format is<br>
 *   <code>
 *   &lt;date&gt;|&lt;host&gt;|&lt;source method&gt;|&lt;level&gt;|
 *   &lt;message&gt;
 *   </code>
 *   <br>
 *
 *  <p>If the withStacktrace flag is set, the stacktrace of a logged excpetion
 *  is also included.</p>
 *
 *  <H3>Example</H3>
 *
 *  <pre>
 *     Logger logger = Logger.getLogger("test logger");
 *
 *     ConsoleHandler consoleHandler = new ConsoleHandler();
 *     GrmFormatter  formatter = new GrmFormatter("test formatter");
 *
 *     int columns [] = {
 *       GrmFormatter.COL_LEVEL_LONG, GrmFormatter.COL_MESSAGE
 *     };
 *     formatter.setColumns(columns);
 *     formatter.setDelimiter(":");
 *
 *     consoleHandler.setFormatter(formatter);
 *     logger.addHandler(consoleHandler);
 *     logger.setUseParentHandlers(false);
 *
 *  </pre>
 *
 *   This example will create log entries with the following format:
 *
 *  <pre>
 *   &lt;log level&gt;:&lt;message&gt;
 *  </pre>
 *
 *  <H3>Example for a definition in a logging properties file: </H3>
 *  <pre>
 *  ...
 *  java.util.logging.ConsoleHandler.formatter=com.sun.grid.grm.util.GrmFormatter
 *
 *
 * #
 * # Possible columns:
 * #
 * #   time    timestamp of the log message
 * #   host    hostname of the log message
 * #   name    name of the logger
 * #   thread  id of the thread
 * #   level   log level (short form)
 * #   source  class and method name
 * #   level_long log_level long form
 * #
 * com.sun.grid.grm.util.GrmFormatter.columns = time level message
 * com.sun.grid.grm.util.GrmFormatter.withStacktrace =true
 * com.sun.grid.grm.util.GrmFormatter.delimiter =|
 * </pre>
 *
 */
public class GrmFormatter extends Formatter {

    /** the NL String. */
    private static final String NL = System.getProperty("line.separator");

    /** max. length of the stacktrace. */
    private static final int MAX_STACKTRACE_LEN = 10;
    /** default width of the source code column. */
    private static final int DEFAULT_SOURCE_COL_WIDTH = 45;
    /** the message format which formats the message it self. */
    private MessageFormat mf;
    
    /** prefix string for stack traces. */
    private String thrownPrefix;
    /** this flag signalizes wether the stacktraces should be included. */
    private boolean withStackTraceFlag;

    /** visible columns. */
    private Column[] columns;

    /** the date object. */
    private Date  date = new Date();

    /** the argument array for MessageFormat. */
    private Object [] args = new Object [] {date, null, null, null };

    /**
     * Defines the possible columns for the log entry.
     */
    public static enum Column {
        /** timestamp of the log record. */
        TIME,
        /** host of the log record. */
        HOST,
        /** name of the logger. */
        NAME,
        /** id of the thread. */
        THREAD,
        /** level of the log record (short form). */
        LEVEL,
        /** message of the log record. */
        MESSAGE,
        /** class and method name which procudes the log record. */
        SOURCE,
        /** level of the log record (long form). */
        LEVEL_LONG
    };

    /** The default columns are
     *  <code>TIME</code>, <code>HOST</code>, <code>CSOURCE</code>,
     *  <code>LEVEL</code>, <code>MESSAGE</code>.
     */
    public static final Column [] DEFAULT_COLUMNS = {
       Column.TIME, Column.HOST, Column.SOURCE, Column.LEVEL, Column.MESSAGE
    };

    /** name of the formatter. */
    private String name;

    /** the delimiter between two columns (default is "|"). */
    private String delimiter = "|";

    /**
     *  Create a new <code>GrmFormatter</code>.
     *
     * @param aName   name of the formatter
     */
    public GrmFormatter(final String aName) {
        this(aName, true);
    }

    /**
     *  Create a new <code>GrmFormatter</code>.
     *
     * @param aName            name of the formatter
     * @param withStackTrace  include the stacktrace
     */
    public GrmFormatter(final String aName, final boolean withStackTrace) {
       this(aName, withStackTrace, DEFAULT_COLUMNS);
    }



    /**
     *  Create a new <code>GrmFormatter</code>.
     *
     * @param aName           name of the formatter
     * @param withStackTrace  include the stacktrace
     * @param aColumns         visible columns
     */
    public GrmFormatter(final String aName, final boolean withStackTrace,
                        final Column [] aColumns) {
       this.name = aName;
       this.setWithStackTrace(withStackTrace);
       this.columns = aColumns;
    }

    /**
     *  <p>Create a new <code>GrmFormatter</code></p>
     *
     *  <p>Initializes the following member variable from properties of the
     *  <code>LogManager</code></p>
     *
     *  <p>All properties begins with <code>com.sun.grid.grm.util.GrmFormatter.</code>.</p>
     *
     *  <table>
     *    <tr><th>Property</th><th>Description</th></tr>
     *    <tr>
     *       <td>withStacktrace</td>
     *       <td>Should the stack of a exception be printed. Possible values
     *           are <code>true</code> or <code>false</code>, default is
     *           <code>false</code>
     *           </td>
     *    </tr>
     *    <tr>
     *       <td>columns</td>
     *       <td>Space seperated list of columns which should be included in a
     *           log message.<br>
     *    </tr>
     *    <tr>
     *       <td>delimiter</td>
     *       <td>delimiter between columns, default is <code>|</code><br>
     *    </tr>
     *  </table>
     *
     *
     *  @see java.util.logging.LogManager#getProperty
     */
    public GrmFormatter() {
        LogManager manager = LogManager.getLogManager();

        String cname = getClass().getName();

        String str = manager.getProperty(cname + ".withStacktrace");
        if (str != null) {
           setWithStackTrace(Boolean.valueOf(str).booleanValue());
        }

        str = manager.getProperty(cname + ".columns");
        
        if (str != null) {
           StringTokenizer st = new StringTokenizer(str, " ");

           List<Column> cols = new ArrayList<Column>();

           while (st.hasMoreTokens()) {
              Column col = Column.valueOf(st.nextToken().toUpperCase());// toLowerCase());
              if (col != null) {
                 cols.add(col);
              }
           }

           this.columns = new Column[cols.size()];
           cols.toArray(this.columns);
        } else {
           this.columns = DEFAULT_COLUMNS;
        }

        str = manager.getProperty(cname + ".name");
        if (str == null) {
           str = "Unknown";
        }
        
        this.name = str;

        str = manager.getProperty(cname + ".delimiter");
        if (str == null) {
            str = "|";
        }
        setDelimiter(str);
    }

    /**
     *  clear the formatter, with the next call of format the
     *  formatter will be reinitialized.
     */
    private void clear() {
       mf = null;
       args = null;
    }

    /**
     *   initialize the formatter.
     */
    private void init() {
        String host;

        StringBuffer message = new StringBuffer();

        int argsCount = 0;
        int dateCol = -1;
        for (int i = 0; i < columns.length; i++) {
           if (i > 0) {
              message.append(delimiter);
           }
           switch(columns[i]) {
              case TIME:
                 message.append("{");
                 message.append(argsCount);
                 message.append(",date,MM/dd/yyyy HH:mm:ss}");
                 dateCol = argsCount;
                 argsCount++;
                 break;
              case HOST:
                    try {
                        host = InetAddress.getLocalHost().getHostName();
                    } catch (UnknownHostException unhe) {
                        host = "unknown";
                    }
                    message.append(host);
                    break;
              case NAME:
                 message.append(name);
                 break;
              default:
                     message.append("{" + argsCount  + "}");
                     argsCount++;
           }

           args = new Object [argsCount];
           if (dateCol >= 0) {
              args[dateCol] = date;
           }
        }

        mf = new MessageFormat(message.toString());
        
        LogRecord lr = new LogRecord(Level.FINE,"");
        String emptyMessage = this.format(lr);
        
        StringBuilder pre = new StringBuilder();
        int prefixLen = emptyMessage.length() - delimiter.length() - 1;

        for (int i = 0; i < prefixLen; i++) {
            pre.append(' ');
        }
        pre.append(delimiter);
        thrownPrefix = pre.toString();
    }

    /**
     *  set the delimiter.
     *
     *  @param aDelimiter   the new delimiter
     */
    public final void setDelimiter(final String aDelimiter) {
       clear();
       this.delimiter = aDelimiter;
    }

    /**
     *  set the columns for the formatter.
     *
     * @param aColumns the columns
     */
    public final void setColumns(final Column[] aColumns) {
       clear();
       this.columns = aColumns;
    }


    /**
     * set the <code>withStackTrace</code> flag.
     *
     * @param  withStackTrace  the new value of the flag
     */
    public final void setWithStackTrace(final boolean withStackTrace) {
        withStackTraceFlag = withStackTrace;
    }

    /**
     * get the <code>withStackTrace</code> flag.
     *
     * @return  the <code>withStackTrace</code> flag
     */
    public final boolean getWithStackTrace() {
        return withStackTraceFlag;
    }

    /**
     *  get a string with a fixed length.
     *
     *  @param  str         the content of the string
     *  @param  length      the fixed length of the string
     *  @param  rightAlign  should the fixed str be right aligned
     *  @return the string with the fixed length
     */
    private String getFixStr(final String str, final int length,
                             final boolean rightAlign) {
       if (str.length() > length) {
          return str.substring(str.length() - length);
       } else {
          StringBuilder ret = new StringBuilder(length);

          int len = length - str.length();
          if (rightAlign) {
             while (len > 0) {
                ret.append(' ');
                len--;
             }
             ret.append(str);
          } else {
            ret.append(str);
             while (len > 0) {
                ret.append(' ');
                len--;
             }
          }
          return ret.toString();
       }

    }

    /**
     *   get the formatted message (may be localized) from a log record.
     *   @param record  the log record
     *   @return the formatted message
     */
    private String getMessage(final LogRecord record) {
        String message = null;

        if (record.getResourceBundle() != null) {
            try {
               message = record.getMessage();
               message = record.getResourceBundle().getString(message);
            } catch (MissingResourceException mre) {
               //System.err.println("missing bundle key: " + message);
               message = record.getMessage();
            }
        }

        if (message == null) {
            message = record.getMessage();
        }

        if (message != null && record.getParameters() != null) {
            try {
                message = MessageFormat.format(message, record.getParameters());
            } catch(Exception ex) {
                // Invalid message format
                // Ignore it and retrun the original message
            }
        }

        return message;
    }

    /**
     *   format a log record.
     *   <p><b>Attention:  This method is not thread safe</b></p>
     *   @see java.util.logging.Formatter#format
     *   @param record the log record
     *   @return the formatted log record
     */
    public final String format(final LogRecord record) {

        if (this.mf == null) {
            init();
        }
        ThreadLocalStringBuffer sb = ThreadLocalStringBuffer.aquire();
        try {
            PrintWriter pw = sb.getPrintWriter();

            int argIndex = 0;
            for (int i = 0; i < columns.length; i++) {

               switch(columns[i]) {
                  case TIME:
                     date.setTime(record.getMillis());
                     argIndex++;
                     break;
                  case NAME:
                  case HOST:
                     continue;
                  case SOURCE:
                     String source = record.getSourceClassName() + '.' + record.getSourceMethodName();
                     args[argIndex] = getFixStr(source,
                                                DEFAULT_SOURCE_COL_WIDTH, true);
                     argIndex++;
                     break;
                  case MESSAGE:
                     args[argIndex] = getMessage(record);
                     argIndex++;
                     break;
                  case LEVEL:
                     args[argIndex] = levelToStr(record.getLevel());
                     argIndex++;
                     break;
                  case THREAD:
                     args[argIndex] = Integer.toString(record.getThreadID());
                     argIndex++;
                     break;
                  case LEVEL_LONG:
                     args[argIndex] = record.getLevel();
                     argIndex++;
                     break;
                  default:
                     throw new IllegalStateException("unknown column "
                                                     + columns[i]);
               }

            }

            String message = mf.format(args);
            printMultilineMessage(message, pw);

            if (withStackTraceFlag && record.getThrown() != null) {
                Throwable th = record.getThrown();

                StackTraceElement [] stack = null;

                boolean first = true;
                int len = 0;
                while (th != null) {
                    pw.print(thrownPrefix);
                    if (first) {
                        first = false;
                    } else {
                        pw.print("Caused by ");
                    }

                    String thMessage = th.getLocalizedMessage();

                    if (thMessage == null) {
                       thMessage = th.getMessage();
                    }

                    pw.print(th.getClass().getName());
                    pw.print(": ");
                    printMultilineMessage(thMessage, pw);
                    stack = th.getStackTrace();
                    len = Math.min(MAX_STACKTRACE_LEN, stack.length);
                    for (int i = 0; i < len; i++) {
                        pw.print(thrownPrefix);
                        pw.print("  ");
                        pw.println(stack[i].toString());
                    }
                    th = th.getCause();
                }
            }
            pw.flush();
            return sb.toString();
        } finally {
            sb.release();
        }
    }

    private void printMultilineMessage(String message, PrintWriter pw) {
        // Support for multi line messages
        if(message == null) {
            pw.println();
        } else {
            int index = message.indexOf('\n');
            if(index >= 0) {
                int lastIndex = 0;
                pw.println(message.substring(0,index));
                lastIndex = index + 1;
                while(true) {
                    index = message.indexOf('\n', lastIndex);
                    if(index < lastIndex) {
                        pw.print(thrownPrefix);
                        pw.println(message.substring(lastIndex));
                        break;
                    } else {
                        pw.print(thrownPrefix);
                        pw.println(message.substring(lastIndex, index));
                        lastIndex = index + 1;
                    }
                }

            } else {
                pw.println(message);
            }
        }
    }
    
    /** maps Level object to its string representation. */
    private static HashMap<Level, String> levelMap = new HashMap<Level, String>();

    /** initializes the level map. */
    static {
        levelMap.put(Level.CONFIG, "C");
        levelMap.put(Level.INFO  , "I");
        levelMap.put(Level.SEVERE, "E");
        levelMap.put(Level.WARNING, "W");
        levelMap.put(Level.FINE   , "D");
        levelMap.put(Level.FINER  , "D");
        levelMap.put(Level.FINEST , "D");
    }

    /**
     *  get the string representation of a Level.
     *
     *  @param level  the level
     *  @return the string representation
     */
    private static String levelToStr(final Level level) {
        String ret = levelMap.get(level);
        if (ret == null) {
            return "U";
        } else {
            return ret;
        }
    }
}
