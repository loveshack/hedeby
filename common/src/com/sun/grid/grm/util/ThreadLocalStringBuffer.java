/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

/**
 *  This class implements an StringBuilder that stores the string builder in
 *  a thread local variable.
 */
public class ThreadLocalStringBuffer {

    private final static ThreadLocal<ThreadLocalStringBuffer> INSTANCE = new ThreadLocal<ThreadLocalStringBuffer>() {
        @Override
        protected ThreadLocalStringBuffer initialValue() {
            return new ThreadLocalStringBuffer();
        }
    };

    private final NonSyncCharArrayWriter writer = new NonSyncCharArrayWriter();
    private final PrintWriter pw = new PrintWriter(writer);
    private boolean aquired;

    private ThreadLocalStringBuffer() {
    }

    /**
     * Get the thread local instance of the ThreadLocalStringBuffer.
     *
     * @return the thread local instance
     * @throws IllegalStateException if the thread local string buffer is already in use
     */
    public static ThreadLocalStringBuffer aquire() throws IllegalStateException {
        ThreadLocalStringBuffer ret = INSTANCE.get();
        if(ret.aquired) {
            throw new IllegalStateException("Recursive call of aquire");
        }
        ret.aquired = true;
        return ret;
    }

    /**
     * Get the print writer that writes into the string builder
     * @return the print writer
     */
    public PrintWriter getPrintWriter() {
        return pw;
    }

    /**
     * Release the string buffer (content is cleared)
     */
    public void release() {
        writer.reset();
        aquired = false;
    }

    /**
     * get the content of the buffer as string
     * @return the content of the buffer
     */
    @Override
    public String toString() {
        return writer.getContent();
    }

    /**
     * We did not use StringWriter because it uses internally an StringBuffer which
     * is synchronized. Synchronization is not necessary.
     *
     * We did not use StringBuilder because it fills the excess with zero chars
     * at truncate.
     */
    private static class NonSyncCharArrayWriter extends Writer {

        private char[] buf = new char[1024];
        private int count;

        private void ensureCapacity(int minimumCapacity) {
            if (minimumCapacity > buf.length) {
                expandCapacity(minimumCapacity);
            }
        }

        private void expandCapacity(int minimumCapacity) {
            int newCapacity = (buf.length + 1) * 2;
            if (newCapacity < 0) {
                newCapacity = Integer.MAX_VALUE;
            } else if (minimumCapacity > newCapacity) {
                newCapacity = minimumCapacity;
            }
            char newBuf[] = new char[newCapacity];
            System.arraycopy(buf, 0, newBuf, 0, count);
            buf = newBuf;
        }

        @Override
        public void write(int c) {
            int newcount = count + 1;
            ensureCapacity(newcount);
            buf[count] = (char) c;
            count = newcount;
        }

        /**
         * Writes characters to the buffer.
         * @param c	the data to be written
         * @param off	the start offset in the data
         * @param len	the number of chars that are written
         */
        public void write(char c[], int off, int len) {
            if ((off < 0) || (off > c.length) || (len < 0) ||
                    ((off + len) > c.length) || ((off + len) < 0)) {
                throw new IndexOutOfBoundsException();
            } else if (len == 0) {
                return;
            }
            int newcount = count + len;
            ensureCapacity(newcount);
            System.arraycopy(c, off, buf, count, len);
            count = newcount;
        }

        public void reset() {
            count = 0;
        }

        public String getContent() {
            String ret = new String(buf, 0, count);
            return ret;
        }

        @Override
        public void close() throws IOException {
        }

        @Override
        public void flush() throws IOException {
        }
    };
}
