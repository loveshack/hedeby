/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import java.net.URL;
import java.util.List;

/**
 * This interface has to be implement for each JAXB enabled package
 * that is used by the module. 
 */
public interface ConfigPackage {
     
     /**
      * Get the namespace url which is set for this module in xml schema
      * @return namespace url
      */
     public String getNamespace();

     /**
      * Get the namespace prefix for this module
      * @return prefix for this module
      */
     public String getNamespacePrefix();

     /**
      * Get JAXB enabled package for this Module
      * @return JAXB enabled package for this Module
      */
     public String getPackage();
     
     /**
      * Get the URL to xml schema on the local system
      * @return the URL to the xml schema
      */
     public URL getLocalURL();
     
     /**
      * Get a list of schema dependencies
      * @return list of dependencies
      */
     public List<String> getDependencies();
}
