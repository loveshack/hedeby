/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.cli.CliCommand;
import com.sun.grid.grm.validate.Validator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 *  Module definition for common
 */
public class CommonModule extends AbstractModule {

    private final List<ConfigPackage> configPackages;
    
    public CommonModule() {
        super("common");
        
        List<ConfigPackage> tmp = new ArrayList<ConfigPackage>(5);
        
        ClassLoader cl = getClass().getClassLoader();
        
        List<String> commonDep = Collections.<String>singletonList("common");
        
        tmp.add(new ConfigPackageImpl("com.sun.grid.grm.config.common", 
                                      "http://hedeby.sunsource.net/hedeby-common", 
                                      "common", 
                                      cl.getResource("hedeby-common.xsd"), 
                                      Collections.<String>emptyList()));
        
        tmp.add(new ConfigPackageImpl("com.sun.grid.grm.config.executor", 
                                      "http://hedeby.sunsource.net/hedeby-executor", 
                                      "executor", 
                                      cl.getResource("hedeby-executor.xsd"), 
                                      commonDep));
        
        tmp.add(new ConfigPackageImpl("com.sun.grid.grm.config.resourceprovider", 
                                      "http://hedeby.sunsource.net/hedeby-resource-provider", 
                                      "resource_provider", 
                                      cl.getResource("hedeby-resource-provider.xsd"), 
                                      commonDep));

        tmp.add(new ConfigPackageImpl("com.sun.grid.grm.config.reporter", 
                                      "http://hedeby.sunsource.net/hedeby-reporter", 
                                      "reporter", 
                                      cl.getResource("hedeby-reporter.xsd"),
                                      commonDep));
        
        tmp.add(new ConfigPackageImpl("com.sun.grid.grm.config.sparepool", 
                                      "http://hedeby.sunsource.net/hedeby-sparepool", 
                                      "spare_pool", 
                                      cl.getResource("hedeby-sparepool.xsd"),
                                      commonDep));

        tmp.add(new ConfigPackageImpl("com.sun.grid.grm.config.gef",
                                      "http://hedeby.sunsource.net/hedeby-gef",
                                      "gef",
                                      cl.getResource("hedeby-gef.xsd"),
                                      commonDep));
        
        configPackages = Collections.unmodifiableList(tmp);
    }
    
    private final static CommonModule instance = new CommonModule();
    
    /**
     * Get the singleton instanceof if the common module
     * @return the singleton instance
     */
    public static CommonModule getInstance() {
        return instance;
    }
    
    @Override
    protected void loadCliExtension(Set<Class<? extends CliCommand>> extensions) {
    }

    @Override
    protected void loadValidatorExtension(Set<Class<? extends Validator>> extensions) {
    }

    public String getVendor() {
        return "Sun Microsystems";
    }

    public String getVersion() {
        return "1.0u5";
    }

    public List<ConfigPackage> getConfigPackages() {
        return configPackages;
    }

}
