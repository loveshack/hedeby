/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.prefs.FilePreferences;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;

/**
 * Helper class which hides all the details for preferences handling
 */
public class PreferencesUtil {
    
    private final static Logger logger = Logger.getLogger(PreferencesUtil.class.getName());
    private final static String BUNDLE = "com.sun.grid.grm.bootstrap.messages";
    private final static String MASTER = "master";
    /**
     * Delete a system from the preferences.
     * <p>
     * Does nothing and returns false if the system does not exist.
     * 
     * @param systemName name of the system
     * @param prefs  the preferences type
     * @throws java.util.prefs.BackingStoreException if the user is not allowed to delete the preferences
     * @return <code>true</code> if the system has been deleted from the preferences
     */
    public static boolean deleteSystem(String systemName, PreferencesType prefs) throws BackingStoreException {
        boolean ret = false;
        Preferences node = getBaseNode(prefs);
        
        if (node.nodeExists(systemName)) {
            if(logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "delete system {0} from {1} preferences",
                           new Object [] { systemName, node.isUserNode() ? "user" : "system" } );
            }
            node.node(systemName).removeNode();
            node.flush();
            ret = true;
        }
        return ret;
    }    
    
    /**
     * Add a hedeby system to the preferences
     * @param env    the execution enviroment
     * @param prefs  the type of preferences
     * @throws java.util.prefs.BackingStoreException if the user is not allowed to write the preferences
     */
    public static void addSystem(ExecutionEnv env, PreferencesType prefs) throws BackingStoreException {
        setDistDir(env.getSystemName(), prefs, env.getDistDir());
        setLocalSpoolDir(env.getSystemName(), prefs, env.getLocalSpoolDir());
        setCSInfo(env.getSystemName(), prefs, env.getCSHost().getHostname(), env.getCSPort());
        setPreferencesId(env.getSystemName(), prefs);
        
        Map<String,?> properties = env.getProperties();
        
        Object autoStart = properties.get(ExecutionEnv.AUTO_START);
        if (autoStart instanceof Boolean && ((Boolean)autoStart).booleanValue()) {
            setAutoStart(env.getSystemName(), prefs, true);
        } else {
            setAutoStart(env.getSystemName(), prefs, false);
        }
        
        Object sslDisabled = properties.get(ExecutionEnv.NO_SSL);
        if (sslDisabled instanceof Boolean && ((Boolean)sslDisabled).booleanValue()) {
            setSSLDisabled(env.getSystemName(), prefs, true);
        } else {
            setSSLDisabled(env.getSystemName(), prefs, false);
        }
        boolean sim = false;
        Object simple = properties.get(ExecutionEnv.SIMPLE);
        if (simple instanceof Boolean && ((Boolean)simple).booleanValue()) {
            sim = true;
        } 
        
        Hostname localhost = Hostname.getLocalHost();
        setHost(env.getSystemName(), prefs, localhost.getHostname(), null, null, env.getCSHost().equals(localhost), sim);      
    }
    
    /**
     * Install localhost host in preferences, this means adding specified host to host
     * preferences, if specified system for host does not exist the system is added
     *
     * @param env execution Env of the system
     * @param prefs <code>PreferencesType</code> type of preferences of the system
     *
     * @throws java.util.prefs.BackingStoreException If the user has no access to the preferences
     * @throws com.sun.grid.grm.bootstrap.PreferencesAlreadyExistException if the entry for localhost has been
     *         already found in the preferences
     */
    public static void installHost(ExecutionEnv env, PreferencesType prefs) throws PreferencesAlreadyExistException, BackingStoreException {
        Hostname localhost = Hostname.getLocalHost();
        Preferences node = getBaseNode(prefs);
        
        if (!node.nodeExists(env.getSystemName())) {
            addSystem(env, prefs);
        } else {
            if (isHost(env.getSystemName(), prefs, localhost.getHostname())) {
                throw new PreferencesAlreadyExistException("PreferencesUtil.error.host_already_exists", 
                        BUNDLE, localhost.getHostname(), env.getSystemName());
            }
             boolean sim = false;
            Object simple = env.getProperties().get(ExecutionEnv.SIMPLE);
            if (simple instanceof Boolean && ((Boolean)simple).booleanValue()) {
                sim = true;
            } 
            setHost(env.getSystemName(), prefs, localhost.getHostname(), env.getLocalSpoolDir(), env.getDistDir(), env.getCSHost().equals(localhost), sim);
        }
            
    }
    
    /**
     * Returns all system names which are stored in the preferences of the local host.
     *
     * @param prefs use user of system preferences
     * @return List of hedeby system names.
     */
    public static List<String> getSystemNames(PreferencesType prefs) {
        Preferences node = getBaseNode(prefs);
        List<String> ret = Collections.emptyList();
        String[] nodeNames;
        try {
            nodeNames = node.childrenNames();
            ret = new ArrayList<String>(nodeNames.length);
            for (String nodeName : nodeNames) {
                if(logger.isLoggable(Level.FINE)) {
                    logger.log(Level.FINE, "Found system {0} in {1} preferences",
                        new Object [] { nodeName, node.isUserNode() ? "user" : "system" } );
                }
                ret.add(nodeName);
            }
        } catch (BackingStoreException ex) {
            ret = Collections.emptyList();
        }
        return ret;
    }
    
    /**
     * Checks if the given name is a valid and existing system name.
     *
     * @param systemName System Name.
     * @return True if the name is valid and the system exists.
     */
    public static boolean existsSystem(final String systemName) {
        boolean ret = existsSystem(systemName, PreferencesType.SYSTEM); 
        if(ret == false) {
            ret = existsSystem(systemName, PreferencesType.USER);
        }
        return ret;
    }
    
    /**
     * Checks if the given name is a valid and existing system name.
     *
     * @param systemName System Name.
     * @param prefs   look in user or system preferences
     * @return True if the name is valid and the system exists.
     */
    public static boolean existsSystem(final String systemName, PreferencesType prefs) {
        Preferences baseNode = getBaseNode(prefs);
        try {
            if(baseNode.nodeExists(systemName)) {
                return true;
            }
        } catch (BackingStoreException ex) {
            return false;
        }
        return false;
    }
    
    
    
    /**
     * Returns all system names which are stored in the preferences of the local host 
     * with the autostart flag
     *
     * @param prefs use user or system preferences
     * @param systems if null filter all systems otherwise
     * use just given system names
     * @return List of system names with the autostart flag.
     */
    public static List<String> getAutoStartSystemNames(PreferencesType prefs, List<String> systems) {
        logger.entering("PreferencesUtil", "getAutoStartSystemNames");
        List<String> ret = new LinkedList<String>();
        List<String> allSystems = null;
        
        if (systems == null) {
            allSystems = getSystemNames(prefs);
        } else {
            allSystems = systems;
        }
        
        for (String systemName : allSystems) {
            if(isAutoStart(systemName, prefs)) {
                ret.add(systemName);
            }
        }
        logger.exiting("PreferencesUtil", "getAutoStartSystemNames");
        return ret;
    }
    
    /**
     * Returns all system names which are stored in the preferences of the local host 
     * with the disabled security flag
     *
     * @param prefs use user or system preferences
     * @param systems if null filter all systems otherwise
     * use just given system names
     * @return List of system names with the security disabled flag.
     * which is not a valid GRM system name.
     */
    public static List<String> getSecurityDisabledSystemNames(PreferencesType prefs, List<String> systems) {
        logger.entering("PreferencesUtil", "getSecurityDisabledSystemNames");
        List<String> ret = new LinkedList<String>();
        List<String> allSystems = null; 
        if (systems == null) {
            allSystems = getSystemNames(prefs);
        } else {
            allSystems = systems;
        }
        
        for (String systemName : allSystems) {
            if(isSSLDisabled(systemName, prefs)) {
                ret.add(systemName);
            }
        }
        logger.exiting("PreferencesUtil", "getSecurityDisabledSystemNames");
        return ret;
    }
    
    
    /**
     * Get the distribution directory of a Hedeby system out of
     * the preferences
     * @param systemName  name of the system
     * @param prefs       the preferences type (user or system)
     * @throws com.sun.grid.grm.bootstrap.PreferencesNotAvailableException if the entry  dist directory has not been
     *         found in the preferences
     * @throws java.util.prefs.BackingStoreException if fail to read preferences of system
     * @return the path to the hedeby distribution
     */
    public static File getDistDir(String systemName, PreferencesType prefs) throws PreferencesNotAvailableException, BackingStoreException {
        String hostname = Hostname.getLocalHost().getHostname();
        Preferences baseNode = getBaseNode(systemName, prefs);
        File ret = getDir(baseNode, BootstrapConstants.DIST_KEY);

        boolean tmp = isHost(systemName, prefs, hostname);
        if(!tmp) {
            return ret;
        }
              
        Preferences host = getHostNode(systemName, prefs, hostname);
        String hostDir = host.get(BootstrapConstants.DIST_KEY, null);
        if (hostDir == null) {
            return ret;
        }
        return new File(hostDir);
    }
    
     /**
     * Store the path to the hedeby local spool directory in the preferences.
     *
     * @param systemName name of the system
     * @param prefs      the preferences type
     * @param distDir    path to the hedeby distribution
     * @throws java.util.prefs.BackingStoreException if the user is not allowed to write the preferences
     */
    public static void setDistDir(String systemName, PreferencesType prefs, File distDir) throws BackingStoreException {
        Preferences baseNode = getBaseNode(systemName, prefs);
        baseNode.put(BootstrapConstants.DIST_KEY, distDir.getAbsolutePath());
        baseNode.flush();
    }
    /**
     * Get the CS url of a Hedeby system out of
     * the preferences
     * @param systemName  name of the system
     * @param prefs       the preferences type (user or system)
     * @throws com.sun.grid.grm.bootstrap.PreferencesNotAvailableException if the entry  dist directory has not been
     *         found in the preferences
     * @return the string that should have following data "host:port"
     */
    public static String getCSInfo(String systemName, PreferencesType prefs) throws PreferencesNotAvailableException {
        Preferences baseNode = getBaseNode(systemName, prefs);
        String url = baseNode.get(BootstrapConstants.CS_INFO_KEY, null);
          if(url == null) {
            throw new PreferencesNotAvailableException("key " + BootstrapConstants.CS_INFO_KEY + " not defined in preferences " + baseNode.absolutePath());
        }
        
        return url;
    }
    
     /**
     * Store the url for connecting to CS in the preferences.
     *
     * @param systemName name of the system
     * @param prefs      the preferences type
      * @param host      hostname where cs is running
      * @param port      port of the cs jvm
      * @throws java.util.prefs.BackingStoreException if the user is not allowed to write the preferences
     */
    public static void setCSInfo(String systemName, PreferencesType prefs, String host, int port) throws BackingStoreException {
        String tmpPort = Integer.toString(port);
        int size = host.length() + tmpPort.length()+1;
        StringBuilder str = new StringBuilder(size);
        str.append(host);
        str.append(":");
        str.append(tmpPort);
        Preferences baseNode = getBaseNode(systemName, prefs);
        baseNode.put(BootstrapConstants.CS_INFO_KEY, str.toString());
        baseNode.flush();
    }
    
    /**
     * Store the path to the hedeby local spool directory in the preferences.
     *
     * @param systemName name of the system
     * @param prefs      the preferences type
     * @param localSpoolDir    path to the hedeby distribution
     * @throws java.util.prefs.BackingStoreException if the user is not allowed to write the preferences
     */
    public static void setLocalSpoolDir(String systemName, PreferencesType prefs, File localSpoolDir) throws BackingStoreException {
        Preferences baseNode = getBaseNode(systemName, prefs);
        baseNode.put(BootstrapConstants.LOCAL_SPOOL_KEY, localSpoolDir.getAbsolutePath());
        baseNode.flush();
    }

    /**
     * Get the local spool directory of a Hedeby system out of
     * the preferences
     * @param systemName  name of the system
     * @param prefs       the preferences type (user or system)
     * @throws com.sun.grid.grm.bootstrap.PreferencesNotAvailableException if the entry for the local
     *         spool directory has not been found in the preferences
     * @throws java.util.prefs.BackingStoreException if fail to read preferences of system
     * @return the path to the hedeby distribution
     */
    public static File getLocalSpoolDir(String systemName, PreferencesType prefs) throws PreferencesNotAvailableException, BackingStoreException {
        String hostname = Hostname.getLocalHost().getHostname();
        Preferences baseNode = getBaseNode(systemName, prefs);
        File ret = getDir(baseNode, BootstrapConstants.LOCAL_SPOOL_KEY);
        
        boolean tmp = isHost(systemName, prefs, hostname);
        if(!tmp) {
            return ret;
        }

        Preferences host = getHostNode(systemName, prefs, hostname);
        String hostDir = host.get(BootstrapConstants.LOCAL_SPOOL_KEY, null);
        if (hostDir == null) {
            return ret;
        }
        return new File(hostDir);
    }
     /**
     * Store version of system in the preferences.
     *
     * @param systemName name of the system
     * @param prefs      the preferences type
     * @throws java.util.prefs.BackingStoreException if the user is not allowed to write the preferences
     */
    @Deprecated
    public static void setVersion(String systemName, PreferencesType prefs) throws BackingStoreException {
        setVersion(systemName, prefs, BootstrapConstants.VERSION);
    }
    /**
     * Store version of system in the preferences.
     *
     * @param systemName name of the system
     * @param prefs      the preferences type
     * @throws java.util.prefs.BackingStoreException if the user is not allowed to write the preferences
     */
    @Deprecated
    public static void setVersion(String systemName, PreferencesType prefs, String version) throws BackingStoreException {
        Preferences baseNode = getBaseNode(systemName, prefs);
        baseNode.put(BootstrapConstants.VERSION_KEY, version);
        baseNode.flush();
    }
    /**
     * Get the version of a Hedeby system out of
     * the preferences, if version is not available, version is set to 0.0, and tries
     * automatically to upgrade
     * @param systemName  name of the system
     * @param prefs       the preferences type (user or system)
     * @throws com.sun.grid.grm.bootstrap.PreferencesUpgradeException if there was an error of upgrading the preferences
     * @return the path to the hedeby distribution
     */
    @Deprecated
    public static String getVersion(String systemName, PreferencesType prefs) throws PreferencesNotAvailableException, PreferencesUpgradeException {
        Preferences baseNode = getBaseNode(systemName, prefs);
        String value = baseNode.get(BootstrapConstants.VERSION_KEY, null);
 
        return value;
    }
    /**
     * Store version of system in the preferences.
     *
     * @param systemName name of the system
     * @param prefs      the preferences type
     * @throws java.util.prefs.BackingStoreException if the user is not allowed to write the preferences
     */
    public static void setPreferencesId(String systemName, PreferencesType prefs) throws BackingStoreException {
        setPreferencesId(systemName, prefs, BootstrapConstants.PREFERENCES_ID);
    }
    /**
     * Store version of system in the preferences.
     *
     * @param systemName name of the system
     * @param prefs      the preferences type
     * @param id         id of preferences
     * @throws java.util.prefs.BackingStoreException if the user is not allowed to write the preferences
     */
    public static void setPreferencesId(String systemName, PreferencesType prefs, String id) throws BackingStoreException {
        Preferences baseNode = getHostNode(systemName, prefs, Hostname.getLocalHost().getHostname());
        baseNode.put(BootstrapConstants.PREFERENCES_ID_KEY, id);
        baseNode.flush();
    }
    /**
     * Get the preferences format version of the hedeby system
     * @param systemName  name of the system
     * @param prefs       the preferences type (user or system)
     * @throws com.sun.grid.grm.bootstrap.PreferencesUpgradeException if there was an error of upgrading the preferences
     * @return the path to the hedeby distribution
     */
    public static String getPreferencesId(String systemName, PreferencesType prefs) throws PreferencesNotAvailableException, PreferencesUpgradeException {
        Preferences baseNode = getHostNode(systemName, prefs, Hostname.getLocalHost().getHostname());
        String value = baseNode.get(BootstrapConstants.PREFERENCES_ID_KEY, null);
        
        return value;
    }
    /**
     * Get the SSL disabled flag for a hedeby system.
     *
     * @param systemName the name of the hedeby system
     * @param prefs  the preferences type (user or system)
     * @return the SSL disabled flag
     */
    public static boolean isSSLDisabled(String systemName, PreferencesType prefs) {
        Preferences baseNode = getBaseNode(systemName, prefs);
        return baseNode.getBoolean(ExecutionEnv.NO_SSL, false);
    }
    
    /**
     * Set the SSL disabled flag for a hedeby system.
     *
     * @param systemName  the name of the hedeby system
     * @param prefs       the preferences type (user or system)
     * @param sslDisabled the ssl disabled flag
     * @throws java.util.prefs.BackingStoreException if the use has not access to the
     *                                          Preferences
     */
    public static void setSSLDisabled(String systemName, PreferencesType prefs, boolean sslDisabled) throws BackingStoreException {
        Preferences baseNode = getBaseNode(systemName, prefs);
        if(logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "set {0} for system {1} ({2}}: {3}",
                       new Object [] { ExecutionEnv.NO_SSL, systemName, prefs, sslDisabled  } );
        }
        baseNode.putBoolean(ExecutionEnv.NO_SSL, sslDisabled);
        baseNode.flush();
    }
    
    /**
     * Get get auto start flag for a hedeby system
     * @param systemName the name of the hedeby system
     * @param prefs      the preferences type (user or system)
     * @return the auto start flag
     */
    public static boolean isAutoStart(String systemName, PreferencesType prefs) {
        Preferences baseNode = getBaseNode(systemName, prefs);
        return baseNode.getBoolean(ExecutionEnv.AUTO_START, false);
    }
    
    /**
     * Set the auto start flag for a hedeby system
     * @param systemName   name of the system
     * @param prefs        the preferences type (user or system)
     * @param autoStart    the auto start flag
     */
    public static void setAutoStart(String systemName, PreferencesType prefs, boolean autoStart) throws BackingStoreException {
        Preferences baseNode = getBaseNode(systemName, prefs);
        baseNode.putBoolean(ExecutionEnv.AUTO_START, autoStart);
        baseNode.flush();
    }
    
    /**
     * Get the name of the default system from the user preferences
     * @return the name of the default system or <code>null</code> of
     *         no default system is set
     */
    public static String getDefaultSystemFromPrefs() {
        Preferences baseNode = getBaseNode(PreferencesType.USER);
        String name = baseNode.get("defaultSystem", null);
        if(name == null) {
            return null;
        } else {
            return name;
        }
    }
    
    /**
     * Set the default system in the user preferences.
     *
     * @param systemName  name of the default system
     * @throws java.util.prefs.BackingStoreException If the user has no access to the preferences
     */
    public static void setDefaultSystem(String systemName) throws BackingStoreException {
        Preferences baseNode = getBaseNode(PreferencesType.USER);
        baseNode.put("defaultSystem", systemName);
        baseNode.flush();
    }
    
    
    /**
     * Rest the default system in the user preferences.
     * @throws java.util.prefs.BackingStoreException If the user has no access to the preferences
     */
    public static void resetDefaultSystem() throws BackingStoreException {
        Preferences baseNode = getBaseNode(PreferencesType.USER);
        baseNode.remove("defaultSystem");
        baseNode.flush();
    }
    
    
    /**
     * Uninstall localhost host from preferences, this means removing specified host from host
     * preferences, if no more hosts left, the whole preferences for system are removed
     *
     * @param systemName - name of hedeby system
     * @param prefs - <code>PreferencesType</code> type of preferences of the system
     *
     * @throws java.util.prefs.BackingStoreException If the user has no access to the preferences
     * @throws com.sun.grid.grm.bootstrap.PreferencesNotAvailableException if the entry for localhost has not been
     *         found in the preferences
     */
    public static void uninstallHost(String systemName, PreferencesType prefs) throws PreferencesNotAvailableException, BackingStoreException {
        String hostname = Hostname.getLocalHost().getHostname();
        if (!isHost(systemName, prefs, hostname)) {
            throw new PreferencesNotAvailableException("PreferencesUtil.error.host_not_found", BUNDLE, hostname, systemName);
        }
        removeHost(systemName, prefs,  hostname);
        if (!hasMoreHosts(systemName, prefs)) {
            deleteSystem(systemName, prefs);
        }
    }
    
    /**
     * Export the preferences of Hedeby system to array of bytes
     * @param prefs - <code>PreferencesType</code> type of preferences of the system
     * @param systemName - name of hedeby system
     * @return byte[] - of exported preferences
     *
     * @throws java.util.prefs.BackingStoreException If the user has no access to the preferences
     * @throws java.io.IOException if operation of storing preferences in byte array fails
     */
    public static byte[] exportSystemPreferences(PreferencesType prefs, String systemName) throws IOException, BackingStoreException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Preferences p = getBaseNode(systemName, prefs);
        p.exportSubtree(os);
        byte[] ret = os.toByteArray();
        os.close();
        return ret;
        
    }
    /**
     * Import the preferences of Hedeby system from array of bytes
     * @param prefs - <code>PreferencesType</code> type of preferences of the system
     * @param systemName - name of hedeby system
     * @param p - the array of bytes with preferences 
     *
     * @throws java.util.prefs.BackingStoreException If the user has no access to the preferences
     * @throws java.util.prefs.InvalidPreferencesFormatException if the format of preferences provided
     *         in array of bytes is incorrect
     * @throws java.io.IOException if operation of reading preferences from byte array fails
     */
    public static void importSystemPreferences(PreferencesType prefs, String systemName, byte[] p) throws IOException, InvalidPreferencesFormatException, BackingStoreException {
        ByteArrayInputStream is = new ByteArrayInputStream(p);
        Preferences n = getBaseNode(prefs);
        Preferences.importPreferences(is);
        n.flush();
        is.close();
        
    }
    private static File getDir(Preferences node, String key) throws PreferencesNotAvailableException {
        String path = node.get(key, null);
        if(path == null) {
            throw new PreferencesNotAvailableException("key " + key + " not defined in preferences " + node.absolutePath());
        }
        return new File(path);
    }
    
    private static Preferences getBaseNode(String systemName, PreferencesType type) {
        Preferences prefs = getBaseNode(type);
        return prefs.node(systemName);
    }

    private static Preferences getBaseNode(PreferencesType type) {
        Preferences prefs = null;
        switch(type) {
            case USER:
                prefs = FilePreferences.getUserRoot();
                break;
            default:
                prefs = FilePreferences.getSystemRoot();
        }
        return prefs;
    }
    /**
     * Set of private helper methods for working with host node in preferences
     */
    private static Preferences getHostsNode(String systemName, PreferencesType prefs) {
        Preferences baseNode = getBaseNode(systemName, prefs);
        Preferences hostNode = baseNode.node(BootstrapConstants.HOSTS_KEY);
        return hostNode;
    }
    
    private static void setHost(String systemName, PreferencesType prefs, String hostname, File localspool, File dist, boolean isMaster, boolean simple) throws BackingStoreException {
        Preferences host = getHostNode(systemName, prefs, hostname);
        if (localspool != null) {
            host.put(BootstrapConstants.LOCAL_SPOOL_KEY, localspool.getAbsolutePath());
        }
        if (dist != null) {
            host.put(BootstrapConstants.DIST_KEY, dist.getAbsolutePath());
        }
        
        host.put(MASTER, String.valueOf(isMaster));
        host.put(ExecutionEnv.SIMPLE, String.valueOf(simple));
        host.flush();
    }
    
    
   private static void removeHost(String systemName, PreferencesType prefs, String hostname) throws BackingStoreException {
        Preferences hosts = getHostsNode(systemName, prefs);
        hosts.node(hostname).removeNode();
        hosts.flush();
    }
    
    /**
     * Is hostname a host of the system?
     * 
     * @param systemName - name of the SDM system
     * @param prefs - preferences type of the SDM system
     * @param hostname - name of host that is checked
     * @return true if hostname exists in the preferences of the specified system
     */
    public static boolean isHost(String systemName, PreferencesType prefs, String hostname) {
        if (!existsSystem(systemName, prefs)) {
            return false;
        }

        Preferences hostsNode = getHostsNode(systemName, prefs);
        try {
            return hostsNode.nodeExists(hostname);
        } catch (BackingStoreException ex) {
            return false;
        }
    }
    
    private static boolean hasMoreHosts(String systemName, PreferencesType prefs) throws BackingStoreException {
        Preferences hosts = getHostsNode(systemName, prefs);
        if (hosts.childrenNames().length == 0) {
            return false;
        }
        return true;
    }
    /** 
     * Check whether given host is a master host for system
     * @param systemName of the SDM system
     * @param prefs - preferences type of SDM system
     * @param hostname - name of host which should be checked
     * @return true if hostname is the master host of the system with specified preferences
     * type
     */
    public static boolean isMasterHost(String systemName, PreferencesType prefs, String hostname) {
        String csInfo;
        String [] data;
        String masterHost;
        try {
            csInfo = getCSInfo(systemName, prefs);
            data = SystemUtil.getCSPropertiesFromString(csInfo);
            masterHost = data[0];
        } catch (PreferencesNotAvailableException ex) {
            return false;
        } catch (GrmException e) {
            return false;
        }
        if (masterHost == null || masterHost.length() == 0) {
            return false;
        }
        return Hostname.getInstance(data[0]).equals(Hostname.getInstance(hostname));
    }
    
    private static Preferences getHostNode(String systemName, PreferencesType prefs, String hostname) {
        Preferences hosts = getHostsNode(systemName, prefs);
        Preferences ret = hosts.node(hostname);
        return ret;
    }
    
    
    /**
     * Helper method for checking if in given preferences there are
     * still installed any systems (if boostrap configuration exists)
     * @param prefs - type of preferences
     * @return true if there exist any configuration, otherwise false
     * @throws java.util.prefs.BackingStoreException If the reading preferences failed
     */
    public static boolean hasAnySystem(PreferencesType prefs) throws BackingStoreException {
        if (getBaseNode(prefs).childrenNames().length == 0) {
            return false;
        }
        return true;
    }
    /**
     * Helper method for checking if in given preferences there are
     * still installed any autostart systems 
     * @param prefs - type of preferences
     * @return true if there exist any configuration, otherwise false
     * @throws java.util.prefs.BackingStoreException If the reading preferences failed
     */
    public static boolean hasAnyAutoStartSystem(PreferencesType prefs) throws BackingStoreException {
        String[] systems = getBaseNode(prefs).childrenNames();

        for (int i=0; i<systems.length;i++) {
            if(isAutoStart(systems[i], prefs)) {
                return true;
            }
        }
        return false;
    }
    /**
     * This metod checks if service tags support was disabled during installation process.
     * @param systemName system name    
     * @param prefs system preferences type
     * @return true if Service Tags were disabled false if they werent
     */
    public static boolean isNoST(String systemName, PreferencesType prefs) {
        Preferences node = getHostNode(systemName, prefs, Hostname.getLocalHost().getHostname());
        return node.getBoolean(BootstrapConstants.NOST_KEY, false);
    }
    
    /**
     * This metod checks if host was installed in simple mode
     * @param systemName system name    
     * @param prefs system preferences type
     * @return true if simple mode property is set to true
     */
    public static boolean isSimple(String systemName, PreferencesType prefs) {
        Preferences node = getHostNode(systemName, prefs, Hostname.getLocalHost().getHostname());
        return node.getBoolean(ExecutionEnv.SIMPLE, false);
    }
    
    /**
     * This method adds entry to host preferences if Service Tags were disabled during installation process
     * @param systemName system name
     * @param prefs system preferences type
     * @param nost if true ST were disabled during installation, false enabled
     * @throws java.util.prefs.BackingStoreException
     */
    public static void setNoST(String systemName, PreferencesType prefs, boolean nost) throws BackingStoreException {
        Preferences node = getHostNode(systemName, prefs, Hostname.getLocalHost().getHostname());
        node.putBoolean(BootstrapConstants.NOST_KEY, nost);
        node.flush();
    }
    
    /**
     * This method stores the uniqe service tag instance urn for the system installation.
     * @param systemName system name
     * @param prefs system preferences type
     * @param instance_urn unique instance urn key
     */
    public static void setSTinstanceUrn(String systemName, PreferencesType prefs, String instance_urn) throws BackingStoreException {
        Preferences node = getHostNode(systemName, prefs, Hostname.getLocalHost().getHostname());
        String temp = node.get(BootstrapConstants.ST_INSTANCE_URN, "");
        if (!(temp.equals(""))) {
            node.remove(BootstrapConstants.ST_INSTANCE_URN);
        }
        node.put(BootstrapConstants.ST_INSTANCE_URN, instance_urn);
        node.flush();
    }
    
    public static String getSTinstanceUrn(String systemName, PreferencesType prefs) {
        Preferences node = getHostNode(systemName, prefs, Hostname.getLocalHost().getHostname());
        return node.get(BootstrapConstants.ST_INSTANCE_URN, "");
    }
    
    /**
     * Return the node where information about SMF for host is stored
     */
    private static Preferences getSMFNode(String systemName, PreferencesType prefs, String hostname) {
        Preferences host = getHostNode(systemName, prefs, hostname);
        Preferences ret = host.node(BootstrapConstants.SMF_KEY);
        return ret;
    }
    
    /**
     * Install SMF support info for jvm into preferences, installing such info
     * automatically means disabling autostart support for rcScripts for current system.
     *
     * @param systemName - name of hedeby system
     * @param prefs - <code>PreferencesType</code> type of preferences of the system
     * @param jvmName - name of jvm for which SMF is installed
     * @param serviceName - SMF FMRI name of system 
     *
     * @throws java.util.prefs.BackingStoreException If operation on the preferences fails
     * @throws com.sun.grid.grm.bootstrap.PreferencesAlreadyExistException if the entry for jvm
     *         on this host is already present.
     */
    public static void installSMFKey(String systemName, PreferencesType prefs, String jvmName, String serviceName) throws BackingStoreException, PreferencesAlreadyExistException {
        String hostname = Hostname.getLocalHost().getHostname();
        Preferences smf = getSMFNode(systemName, prefs, hostname);
        String[] keys = smf.keys();
        for (int i=0; i<keys.length; i++) {
            if (jvmName.equals(keys[i])) {
                throw new PreferencesAlreadyExistException("PreferencesUtil.error.jvm_smf_already_exist", BUNDLE, hostname, jvmName);
            }
        }
        smf.put(jvmName, serviceName);
        smf.flush();
        //for sure disable the autostart
        setAutoStart(systemName, prefs, false);
        //mark info about SMF support
        setSMF(systemName, prefs, true);
    }
    
    /**
     * Get SMF service name that is installed for a given jvm.
     *
     * @param systemName - name of hedeby system
     * @param prefs - <code>PreferencesType</code> type of preferences of the system
     * @param jvmName - name of jvm for which SMF is installed
     *
     * @return SMF service name for jvm
     * @throws java.util.prefs.BackingStoreException If operation on the preferences fails
     * @throws com.sun.grid.grm.bootstrap.PreferencesNotAvailableException if the entry for jvm
     *         on this host is not present.
     */
    public static String getSMFService(String systemName, PreferencesType prefs, String jvmName) throws PreferencesNotAvailableException, BackingStoreException {
        String hostname = Hostname.getLocalHost().getHostname();
        Preferences smf = getSMFNode(systemName, prefs, hostname);
        String[] keys = smf.keys();
        for (int i=0; i<keys.length; i++) {
            if (jvmName.equals(keys[i])) {
                String ret = smf.get(keys[i], "null");
                return ret;
            }
        }
        throw new PreferencesNotAvailableException("PreferencesUtil.error.jvm_smf_not_found", BUNDLE, hostname, jvmName);
    }
    
    /**
     * Get all SMF services that are installed for jvms of current system.
     *
     * @param systemName - name of hedeby system
     * @param prefs - <code>PreferencesType</code> type of preferences of the system
     *
     * @return map with pairs jvm name as key and SMF service name as value
     * @throws java.util.prefs.BackingStoreException If operation on the preferences fails
     */
    public static Map<String, String> getAllSMFServices(String systemName, PreferencesType prefs) throws BackingStoreException {
        Map<String, String> ret = new HashMap<String, String>();
        String hostname = Hostname.getLocalHost().getHostname();
        Preferences smf = getSMFNode(systemName, prefs, hostname);
        String[] keys = smf.keys();
        for (int i=0; i<keys.length; i++) {
           ret.put(keys[i], smf.get(keys[i], "NULL"));
        }
        return ret;
    }
    
    /**
     * Uninstall SMF support info for jvm from preferences, if this is the last
     * SMF supported jvm, the flag SMF for this system is set to false
     *
     * @param systemName - name of hedeby system
     * @param prefs - <code>PreferencesType</code> type of preferences of the system
     * @param jvmName - name of jvm for which SMF is installed
     *
     * @throws java.util.prefs.BackingStoreException If operation on the preferences fails
     * @throws com.sun.grid.grm.bootstrap.PreferencesNotAvailableException if the entry for jvm
     *         on this host is not present.
     */
    public static void uninstallSMFKey(String systemName, PreferencesType prefs, String jvmName) throws BackingStoreException, PreferencesNotAvailableException {
        String hostname = Hostname.getLocalHost().getHostname();
        Preferences smf = getSMFNode(systemName, prefs, hostname);
        String[] keys = smf.keys();
        for (int i=0; i<keys.length; i++) {
            if (jvmName.equals(keys[i])) {
                smf.remove(keys[i]);
                smf.flush();
                if (keys.length == 1) {
                    setSMF(systemName, prefs, false);
                }
                return;
            }
        }
        throw new PreferencesNotAvailableException("PreferencesUtil.error.jvm_smf_not_found", BUNDLE, hostname, jvmName);
    }
    
    /**
     * Check whether there is SMF support installed on this host for jvm in preferences
     *
     * @param systemName - name of hedeby system
     * @param prefs - <code>PreferencesType</code> type of preferences of the system
     * @param jvmName - name of jvm for which SMF is installed
     *
     * @throws java.util.prefs.BackingStoreException If operation on the preferences fails
     */
    public static boolean hasSMFSupport(String systemName, PreferencesType prefs, String jvmName) throws BackingStoreException {
        String hostname = Hostname.getLocalHost().getHostname();
        Preferences smf = getSMFNode(systemName, prefs, hostname);
        String[] keys = smf.keys();
        for (int i=0; i<keys.length; i++) {
            if (jvmName.equals(keys[i])) {
                return true;
            }
        }
        return false;
    }
    
    /** 
     * Setting SMF flag in preferences
     */
    private static void setSMF(String systemName, PreferencesType prefs, boolean smf) throws BackingStoreException {
        Preferences baseNode = getBaseNode(systemName, prefs);
        if(logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "set {0} for system {1} ({2}}: {3}",
                       new Object [] { BootstrapConstants.SMF_KEY, systemName, prefs, smf } );
        }
        baseNode.putBoolean(BootstrapConstants.SMF_KEY, smf);
        baseNode.flush();
    }
    /**
     * Get the smf flag for a hedeby system.
     *
     * @param systemName the name of the hedeby system
     * @param prefs  the preferences type (user or system)
     * @return the smf flag
     */
    public static boolean isSMF(String systemName, PreferencesType prefs) throws BackingStoreException {
        Preferences baseNode = getBaseNode(systemName, prefs);
        return baseNode.getBoolean(BootstrapConstants.SMF_KEY, false);
    }
    /**
     * Remove version key from preferences
     * @param system - system name
     * @param type - type of preferences USER or SYSTEM
     * @throws java.util.prefs.BackingStoreException - if operation fails
     */
    public static void removeVersion(String system, PreferencesType type) throws BackingStoreException {
        Preferences node = getBaseNode(system, type);
        node.remove(BootstrapConstants.VERSION_KEY);
        node.flush();
    }
}
