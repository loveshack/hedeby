/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;


/**
 * This exception is thrown if the system property is not 
 * initialized properly.
 *
 */
public class SystemPropertyNotAvailableException extends GrmException {

    private static final String BUNDLE_NAME = "com.sun.grid.grm.bootstrap.messages";

    private final static long serialVersionUID = -2007070301L;
    /**
     * Creates a new instance of <code>SystemPropertyNotAvailableException</code> 
     * without detail message.
     * @param systemPropertyName  name of the system property
     */
    public SystemPropertyNotAvailableException(String systemPropertyName) {
       super("bootstrap.exception.systempropertynotavail", BUNDLE_NAME, systemPropertyName);
    }
}
