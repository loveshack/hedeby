/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.naming.ConfigurationServiceInitialContextFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdException;
import com.sun.grid.grm.resource.impl.DefaultResourceIdFactory;
import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.security.NullSecurityContextFactory;
import com.sun.grid.grm.security.SecurityContext;
import com.sun.grid.grm.ui.CommandService;
import com.sun.grid.grm.ui.impl.CommandServiceImpl;
import com.sun.grid.grm.util.HostAndPort;
import java.io.File;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import com.sun.grid.grm.util.Hostname;

/**
 *  Default implementation of the execution environment.
 */
class ExecutionEnvImpl implements ExecutionEnv {

    private static final String BUNDLE_NAME = "com.sun.grid.grm.bootstrap.messages";
    private final File localSpoolDir;
    private final File distDir;
    private final Map<String, ?> properties;
    private final String name;
    private final SecurityContext securityContext;
    private final JMXServiceURL csURL;
    private final HostAndPort csInfo;
    private final CommandService cs;
    private final FactoryWithException<ResourceId,ResourceIdException> resourceIdFactory;
    private final FactoryWithParamAndException<Context,ExecutionEnv,GrmException> contextFactory;

    private int hash = 0;

    
    private ExecutionEnvImpl(String name, File localSpoolDir, File distDir, Map<String, ?> properties, HostAndPort csInfo,
                             FactoryWithParam<CommandService,ExecutionEnv> csFactory,
                             FactoryWithParamAndException<SecurityContext,ExecutionEnv,GrmSecurityException> securityContextFactory,
                             FactoryWithParamAndException<Context,ExecutionEnv,GrmException> contextFactory,
                             FactoryWithParam<FactoryWithException<ResourceId,ResourceIdException>,ExecutionEnv> resourceIdFactoryFactory
            ) throws GrmSecurityException {
        this.name = name;
        this.localSpoolDir = localSpoolDir;
        this.distDir = distDir;
        this.properties = Collections.unmodifiableMap(properties);
        this.csInfo = csInfo;
        try {
            csURL = new JMXServiceURL("service:jmx:rmi://" + csInfo.getHost().getHostname() + "/jndi/rmi://" + csInfo.getHost().getHostname() + ":" + csInfo.getPort() + "/" + name);
        } catch (MalformedURLException ex) {
            // This should not happen
            throw new IllegalStateException("Can not create JMX service URL", ex);
        }
        this.cs = csFactory.newInstance(this);
        this.securityContext = securityContextFactory.newInstance(this);
        this.contextFactory = contextFactory;
        this.resourceIdFactory = resourceIdFactoryFactory.newInstance(this);
    }


    /**
     * Get the Host and port of CS
     * @return Hand and port of CS
     */
    public HostAndPort getCSInfo() {
        return csInfo;
    }

    /**
     * get the cs host
     * @return the cs host
     */
    public Hostname getCSHost() {
        return csInfo.getHost();
    }

    /**
     * get the cs port
     * @return the cs port
     */
    public int getCSPort() {
        return csInfo.getPort();
    }

    /**
     * Creates the JNDI context for the system
     * @return the JNDI context
     * @throws com.sun.grid.grm.GrmException if the context cound not be created
     */
    public Context getContext() throws GrmException {
        return contextFactory.newInstance(this);
    }

    /**
     * Get the resource id factory
     * @return the resource id factory
     */
    public FactoryWithException<ResourceId,ResourceIdException> getResourceIdFactory() {
        return resourceIdFactory;
    }

    /**
     * Get the JMX service url to the configuration service
     * @return the JMX service url
     */
    public JMXServiceURL getCSURL() {
        return csURL;
    }

    /**
     * get the dist directory
     * @return the dist directory
     */
    public File getDistDir() {
        return distDir;
    }

    /**
     * get the local spool directory
     * @return the local spool directory
     */
    public File getLocalSpoolDir() {
        return localSpoolDir;
    }

    /**
     * Get the properties of the execution env
     * @return the properties of the execution env
     */
    public Map<String, ?> getProperties() {
        return properties;
    }

    /**
     * Get the system name
     * @return the system name
     */
    public String getSystemName() {
        return name;
    }

    /**
     * get the security context
     * @return the security context, can be null
     */
    public SecurityContext getSecurityContext() {
        return securityContext;
    }

    /** 
     * Two ExecutionEnv instances are equal if thay have the same systemName
     * and the connection strings to CS are also equal
     * @object the instance of ExecutionEnv that will be compared with this
     * @return true if both instances are equal
     */
    @Override
    public boolean equals(Object obj) {
        boolean ret = false;
        if (obj instanceof ExecutionEnv) {
            ExecutionEnv env = (ExecutionEnv) obj;
            ret = csInfo.equals(env.getCSInfo());
            if (ret) {
                if (securityContext == null) {
                    ret = env.getSecurityContext() == null;
                } else {
                    ret = securityContext.equals(env.getSecurityContext());
                }
            }
        }
        return ret;
    }

    /**
     * Implementation of hashCode
     * return hashCode representing this object
     */
    @Override
    public int hashCode() {
        int h = hash;
        if (h == 0) {
            h = csInfo.hashCode();
            if (securityContext != null) {
                h = h * 31 + securityContext.hashCode();
            }
            hash = h;
        }
        return h;
    }

    /**
     * get the command service
     * @return the command service
     */
    public CommandService getCommandService() {
        return cs;
    }

    private final static FactoryWithParam<CommandService,ExecutionEnv> DEFAULT_COMMAND_SERVICE_FACTORY = new FactoryWithParam<CommandService,ExecutionEnv>() {
        public CommandService newInstance(ExecutionEnv env) {
            return new CommandServiceImpl(env);
        }
    };

    private final static FactoryWithParamAndException<Context,ExecutionEnv,GrmException> DEFAULT_CONTEXT_FACTORY = new FactoryWithParamAndException<Context,ExecutionEnv,GrmException>() {
        public Context newInstance(ExecutionEnv env) throws GrmException {
            try {
                Hashtable<String, Object> contextEnv = new Hashtable<String, Object>();
                File csDir = PathUtil.getSpoolDirForComponent(env, BootstrapConstants.CS_ID);

                contextEnv.put(Context.INITIAL_CONTEXT_FACTORY, ConfigurationServiceInitialContextFactory.class.getCanonicalName());
                contextEnv.put(Context.PROVIDER_URL, csDir.toURI().toURL());
                return new InitialContext(contextEnv);
            } catch (MalformedURLException ex) {
                GrmException grm = new GrmException("ExecutionEnv.cs.malformedurl", ex, BUNDLE_NAME, ex.getLocalizedMessage());
                throw grm;
            } catch (NamingException ex) {
                GrmException grm = new GrmException("ExecutionEnv.context.creationfailed", ex, BUNDLE_NAME, ex.getLocalizedMessage());
                throw grm;
            }
        }
    };

    private final static FactoryWithParam<FactoryWithException<ResourceId,ResourceIdException>,ExecutionEnv> DEFAULT_RESOURCE_ID_FACTORY_FACTORY = new FactoryWithParam<FactoryWithException<ResourceId,ResourceIdException>,ExecutionEnv>() {

        public FactoryWithException<ResourceId, ResourceIdException> newInstance(ExecutionEnv env) {
            return new DefaultResourceIdFactory(env);
        }

    };

    static class Builder {

        private File localSpoolDir;
        private File distDir;
        private Map<String, ?> properties = Collections.<String,Object>emptyMap();
        private String name;
        private FactoryWithParamAndException<SecurityContext,ExecutionEnv,GrmSecurityException> securityContextFactory = NullSecurityContextFactory.getInstance();
        private FactoryWithParam<CommandService,ExecutionEnv>  csFactory = DEFAULT_COMMAND_SERVICE_FACTORY;
        private FactoryWithParamAndException<Context,ExecutionEnv,GrmException> contextFactory = DEFAULT_CONTEXT_FACTORY;
        private FactoryWithParam<FactoryWithException<ResourceId,ResourceIdException>,ExecutionEnv> resourceIdFactoryFactory = DEFAULT_RESOURCE_ID_FACTORY_FACTORY;
        private HostAndPort csInfo;

        Builder(String name) {
            this.name = name;
        }

        Builder localSpoolDir(File localSpoolDir) {
            this.localSpoolDir = localSpoolDir;
            return this;
        }

        Builder distDir(File distDir) {
            this.distDir = distDir;
            return this;
        }

        Builder properties(Map<String, ?> properties) {
            this.properties = new HashMap<String, Object>(properties);
            return this;
        }

        Builder securityContextFactory(FactoryWithParamAndException<SecurityContext,ExecutionEnv,GrmSecurityException> securityContextFactory) {
            this.securityContextFactory = securityContextFactory;
            return this;
        }

        Builder csInfo(Hostname csHost, int csPort) {
            csInfo = HostAndPort.newInstance(csHost, csPort);
            return this;
        }

        Builder csFactory(FactoryWithParam<CommandService,ExecutionEnv> csFactory) {
            this.csFactory = csFactory;
            return this;
        }

        Builder contextFactory(FactoryWithParamAndException<Context,ExecutionEnv,GrmException> contextFactory) {
            this.contextFactory = contextFactory;
            return this;
        }

        Builder resourceIdFactoryFactory(FactoryWithParam<FactoryWithException<ResourceId,ResourceIdException>,ExecutionEnv> resourceIdFactoryFactory) {
            this.resourceIdFactoryFactory = resourceIdFactoryFactory;
            return this;
        }

        ExecutionEnv newInstance() throws GrmSecurityException {
            return new ExecutionEnvImpl(name, localSpoolDir, distDir, properties, csInfo, csFactory, securityContextFactory, contextFactory, resourceIdFactoryFactory);
        }
    }
}
