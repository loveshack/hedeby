/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.management.Manageable;
import java.util.Map;
import java.util.logging.Filter;
import java.util.logging.Level;

/**
 * Managed interface of the JVM in a Hedeby system
 */
@Manageable
public interface JVM extends GrmComponent {

    /**
     *  Trigger a shutdown the jvm
     *  @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void shutdown() throws GrmRemoteException;
    
    /**
     * 
     * 
     * @return true is JVM contains CS, false otherwise
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public boolean isCS() throws GrmRemoteException;
    
    /**
     *   Add a new component in the JVM.
     * 
     *   <p>On success the component is reachable over it's management interface.
     *      If the <code>autostart</code> flag in <code>comp</code> is set
     *      the component will be started.</p>
     *
     * @param comp definition of the component
     * @throws com.sun.grid.grm.GrmException if the component could not be added
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void addComponent(Component comp) throws GrmRemoteException, GrmException;
    
    
    /**
     *  Remove a component from the JVM.
     * 
     *  <p>If the component is running the shutdown for the component
     *     is triggered.</p>
     * 
     * @return <code>true</code> if the component has been removed
     *          or <code>false</code> if the component has not been found
     * @param name the name of the component
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public boolean removeComponent(String name)  throws GrmRemoteException;
    
    /**
     * Get the used memory of the jvm
     * @return the used memory
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public long getUsedMem() throws GrmRemoteException;

    /**
     * Get the max. memory of the jvm
     * @return the max memory
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public long getMaxMem() throws GrmRemoteException;
    
    /**
     * Gets loggers for this jvm
     * @return map of loggers for this jvm
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public Map<String, Level> getLoggers() throws GrmRemoteException;
    
    /**
     * Sets the log level for this jvm
     * @param loggers log levels for this jvm
     * @param changeAll log level for whole jvm
     * @return the map of all changed loggers (key is the name of the logger,
     *         value is the new log level)
     * @throws GrmRemoteException on any remote error
     */
    public Map<String, Level> setLoggers(Map<String, Level> loggers, Level changeAll) throws GrmRemoteException;


    /**
     * Register a remote logger in the jvm.
     *
     * <p>The jvm will send for each loggable log record which is written in the logging
     *    system a serialized instance of class LogEvent to the host and port as
     *    an UDP datagram.</p>
     * <p>A log record is treated as loggable if its level is &gt;= the level parameter and
     *    if the filter defined the filter parameter matches against the record.</p>
     * <p>The JVM defines a time to live (TTL) for each remote logger. If this time is
     *    elapsed no more log events will be sent to the remote logger. The TTL is returned
     *    by this method. The remote logger can renew the TTL by calling again this method
     *    in time.</p>
     * 
     * @param hostname  the hostname where the log event will be sent to
     * @param port      the port
     * @param level     the log level
     * @param filter    the log filter (can be null)
     * @return time to live of the remote logger in milliseconds
     * @throws com.sun.grid.grm.GrmRemoteException on any communication error
     */
    public long addRemoteLogger(String hostname, int port, Level level, Filter filter) throws GrmRemoteException;

}
