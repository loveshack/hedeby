/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.util.Hostname;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/**
 *  Contains the information about a jvm
 */
public class JvmInfo implements Serializable {
    
    private static final long serialVersionUID = -2007110601L;
    
    private final String name;
    private final String hostname;
    private transient Hostname hostnameObj;
    private transient int hashcode = -1;
    
    /**
     * Create a new instance of JvmInfo.
     * @param aName   the name of the jvm
     * @param aHost   the host of the jvm
     */
    public JvmInfo(String aName, Hostname aHost) {
        if(aName == null) {
            throw new NullPointerException("jvm name must not be null" );
        }
        if(aHost == null) {
            throw new NullPointerException("hostname must not be null");
        }
        name = aName;
        hostname = aHost.getHostname();
        hostnameObj = aHost;
    }
    
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        hostnameObj = Hostname.getInstance(hostname);
    }
    
    @Override
    public boolean equals(Object obj) {
        return obj instanceof JvmInfo
            && name.equals(((JvmInfo)obj).name)
            && hostnameObj.equals(((JvmInfo)obj).hostnameObj);
    }
    
    
    @Override
    public int hashCode() {
        int h = hashcode;
        if(h < 0) {
            h = 31*name.hashCode() + hostname.hashCode(); 
            hashcode = h;
        }
        return h;
    }
    
    @Override
    public String toString() {
        return getIdentifier();
    }

    /**
     * Get the name of the jvm.
     * 
     * @return name of the jvm
     */
    public String getName() {
        return name;
    }

    /**
     * Get the name of the host where the jvm is running.
     * 
     * @return the name of the host 
     */
    public Hostname getHostname() {
        return hostnameObj;
    }
    
    /**Get the identifier of this component
     * @return identifier &lt;name&gt;:&lt;hostname&gt;
     */
    public String getIdentifier() {
        return ComponentInfo.createIdentifier(getName(), hostnameObj.getHostname());
    }
    
    
}
