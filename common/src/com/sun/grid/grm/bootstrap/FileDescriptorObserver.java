/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.util.I18NManager;
import java.lang.management.ManagementFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.ObjectName;

/**
 * Helper class that allows the observation of the number of file descriptors
 * Works only of the platform mbean service has the MBean java.lang:type=OperatingSystem
 * and this MBean has the attributes MaxFileDescriptorCount and OpenFileDescriptorCount.
 * If these attributes are not supported the OpenFileDescriptorCount is treated a 0 and
 * the MaxFileDescriptorCount is treated as Integer.MAX_VALUE.
 */
public class FileDescriptorObserver {

    private final static String BUNDLE = "com.sun.grid.grm.bootstrap.messages";
    private final static Logger log = Logger.getLogger(FileDescriptorObserver.class.getName(), BUNDLE);
    private final ObjectName objectName;
    private final MBeanServer mbs;
    private final long maxFDCount;
    private long openFDCount;

    /**
     *  Create a new FileDescriptorObserver
     */
    public FileDescriptorObserver() {
        MBeanServer tmbs = ManagementFactory.getPlatformMBeanServer();
        long tmpMaxFD = Integer.MAX_VALUE;
        ObjectName on = null;
        try {
            on = new ObjectName("java.lang:type=OperatingSystem");
            MBeanInfo info = tmbs.getMBeanInfo(on);

            boolean foundOpenFDAttr = false;
            boolean foundMaxFDAttr = false;
            for(MBeanAttributeInfo ai: info.getAttributes()) {
                if (ai.getName().equals("MaxFileDescriptorCount")) {
                    log.log(Level.FINE, "FileDescriptorObserver.fdMaxSupported");
                    foundMaxFDAttr = true;
                    continue;
                }
                if (ai.getName().equals("OpenFileDescriptorCount")) {
                    log.log(Level.FINE, "FileDescriptorObserver.fdOpenSupported");
                    foundOpenFDAttr = true;
                    continue;
                }
            }
            if (foundMaxFDAttr) {
                tmpMaxFD = (Long)tmbs.getAttribute(on, "MaxFileDescriptorCount");
                log.log(Level.FINE, "FileDescriptorObserver.maxFD", tmpMaxFD);
            }
            if (!foundOpenFDAttr || !foundMaxFDAttr) {
                tmbs = null;
            }
        } catch(Exception ex) {
            if(log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, I18NManager.formatMessage("FileDescriptorObserver.ex.init", BUNDLE, ex.getLocalizedMessage()), ex);
            }
            tmbs = null;
        }
        this.mbs = tmbs;
        this.maxFDCount = tmpMaxFD;
        this.objectName = on;
        update();
    }

    /**
     * get the number of open file descriptors
     * @return the number of open file descripts
     */
    public long getOpenFileDescriptorCount() {
        return openFDCount;
    }

    /**
     * Read the number of open file descriptors of the java process and
     * update the member variables of this instance
     */
    public void update() {
        if (mbs != null) {
            try {
                openFDCount = (Long) mbs.getAttribute(objectName, "OpenFileDescriptorCount");
                log.log(Level.FINE, "FileDescriptorObserver.update", openFDCount);
            } catch (Exception ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("FileDescriptorObserver.ex.update", BUNDLE, ex.getLocalizedMessage()), ex);
                openFDCount = 0;
            }
        }
    }

    /**
     * get the max number of file descriptors
     * @return
     */
    public long getMaxFileDescriptorCount() {
        return maxFDCount;
    }

    /**
     * get the number of free file descriptors
     * @return the number of free file descriptors
     */
    public long getFreeFileDescriptorCount() {
        return getMaxFileDescriptorCount() - getOpenFileDescriptorCount();
    }

    /**
     * get the use file descritpros ratio ( open FD / max FD )
     * @return
     */
    public double getOpenFileDescriptorRatio() {
        return (double)getOpenFileDescriptorCount() / getMaxFileDescriptorCount();
    }
}
