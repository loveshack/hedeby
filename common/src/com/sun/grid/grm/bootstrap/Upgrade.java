/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;

/**
 * Inteface for Upgrade of the system. 
 */
public interface Upgrade {

    /**
     * Get binary version
     * @return string representing version of binary
     */
    public String getBinaryVersion();

    /**
     * Upgrade current host
     * @param env - ExecutionEnv
     * @param hostVersion - current version of the host
     * @param systemVersion - current version of the system
     * @return UpgradeResult object containg detailed results of upgrade execution
     * @throws com.sun.grid.grm.GrmException if could not trigger the upgrade
     */
    public UpgradeResult upgradeHost(ExecutionEnv env, String hostVersion, String systemVersion) throws GrmException;

    /**
     * Check version of the host is supported by this binary. And if host should be upgraded.
     * @param hostVersion - version of the current host
     * @param systemVersion -version of system
     * @return true if version of current host should be upgraded.
     * @throws com.sun.grid.grm.GrmException - if binaryVersion is unsupported
     */
    public boolean isUpgradeNecessary(ExecutionEnv env, String hostVersion, String systemVersion)  throws GrmException;

}
