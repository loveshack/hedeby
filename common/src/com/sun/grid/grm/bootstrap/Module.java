/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.cli.CliCommand;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.ui.Command;
import java.util.List;
import java.util.Set;
import com.sun.grid.grm.validate.Validator;

/**
 * Plugable module for a Hedeby system
 */
public interface Module {
          
    /**
     * Get the name of the Module.
     * @return name of the module
     */
    public String getName();
    
    /**
     * Get the vendor of the module
     * @return the vendor of the module
     */
    public String getVendor();
    
    /**
     * Get the version of the module
     * @return the version of the module
     */
    public String getVersion();
    
    /**
     * Get the cli extensions of the Module.
     * @return The cli extensions of the module
     */
    public Set<Class<? extends CliCommand>> getCliExtensions();
    
    /**
     * Get the Validator extensions of the Module.
     * @return The Validator extensions of the module
     */
    public Set<Class<? extends Validator>> getValidatorExtensions();
           
    /**
     * Get a set of new resource types introduced by this module
     * @return map of new resource types
     */
    public Set<ResourceType> getResourceTypes();
    
    /**
     *  Get a list of JAXB enabled configuration packages for this module.
     *
     *  @return lsit of JAXB enabled configuration packages
     */
    public List<ConfigPackage> getConfigPackages();
    
    /**
     *  Get the a set of commands which are extecuted with the
     *  InstallMasterCommand.
     * 
     *  @return set of ui commands
     */
     public Set<? extends Command> getMasterInstallExtensions();
}