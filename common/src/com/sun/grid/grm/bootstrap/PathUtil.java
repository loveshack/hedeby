/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;


import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

  
    /** Creates a new instance of PathUtil */
public class PathUtil {
    
    private static final String BUNDLE_NAME = "com.sun.grid.grm.bootstrap.messages";
            
    private static final String PATH_RUN = "run";    
    private static final String PATH_LIB = "lib";
    private static final String PATH_TMP = "tmp";
    private static final String PATH_SPOOL = "spool";
    public static final String PATH_RP_SPOOL = "resource_provider";
    private static final String PATH_ORDERS = "orders";
    private static final String PATH_LOG = "log";
    private static final String PATH_UTIL = "util";
    private static final String PATH_TEMPLATES = PATH_UTIL + File.separator + "templates";
    private static final String SECURITY_DIR = "security";
    private static final String LICENSE_PATH = "doc" + File.separator + "LICENSE";
    private static final String VERSION_FILENAME = "version";
    
    private static final AtomicLong seq = new AtomicLong();
    private static final String SDM_ENV_PATH = "/etc/sdm/sdm.env";
       
    /**
     * Returns the config path configured for the current system.
     * @param env ExecutionEnv of a current hedeby system
     * @return Path to the directory. 
     * @deprecated will be removed with CS    
     */
    public static File getConfigPath(ExecutionEnv env) {
        return new File(env.getLocalSpoolDir(), PATH_SPOOL);
    }
    
    /**
     * Get the path to the templates
     * @param env  the execution env
     * @return path to the templates
     */
    public static File getTemplatePath(ExecutionEnv env) {
        return new File(env.getDistDir() , PATH_TEMPLATES);
    }
    
    /**
     * Get the default hedeby spool directory
     * @throws IllegalStateException Default Spool directory not defined on Windows Platform
     * @param sysName name of the hedeby system
     * @return String with the default hedeby spool directory
     */
    public static String getDefaultSpoolDir(String sysName) {
        String localSpoolPath = null;
        if (!Platform.isWindowsOs()) {
            localSpoolPath = File.separatorChar + "var" +
                    File.separatorChar + "spool" +
                    File.separatorChar + "sdm" +
                    File.separatorChar + sysName;
        } else {
           throw new IllegalStateException("Method not implemented for Windows Platform");
        }
        return localSpoolPath;
    }
    
    private static String distLibURL;
    /**
     * Get the URL to the dist lib directory of the hedeby system.
     *
     * @return URL to the dist lib directory
     */
    public synchronized static String getDistLibURL() {
        if(distLibURL == null) {
            String bundlePath = BUNDLE_NAME.replace('.', '/') + ".properties";
            URL resource = PathUtil.class.getClassLoader().getResource(bundlePath);
            if(resource == null) {
                throw new IllegalStateException("property file " + bundlePath + " not found");
            }
            String path = resource.getPath();
            int index = path.lastIndexOf('!');
            if(index >= 0) {
                path = path.substring(0, index);
            }
            index = path.lastIndexOf('/');
            if(index < 0) {
                throw new IllegalStateException("Invalid path to dist:" + path);
            }
            distLibURL = path.substring(0,index);
        }
        return distLibURL;
    }
    
    private static File distPath;
    
    /**
     * Get the default hedeby distribution directory.
     *
     * @return String containing the default hedeby distribution directory
     */
    public  synchronized static File getDefaultDistDir() {
        if (distPath == null) {
            String distURL = getDistLibURL();
            try {
                File path = new File(new URI(distURL));
                path = path.getAbsoluteFile();
                distPath = path.getParentFile();
            } catch (URISyntaxException ex) {
                throw new IllegalStateException("Invalid dist URL:" + distURL);
            }
        }
        return distPath;
    }    
    
    
    /**
     * Returns the run path configured for the current system.
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return Path to the directory.
     * @deprecated will be removed with CS     
     */
    public static File getRunPath(ExecutionEnv env) {
        return new File(env.getLocalSpoolDir(), PATH_RUN);
    }
    
    /**
     * Returns the dist lib path configured for the current system.
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return Path to the directory
     */ 
    public static File getDistLibPath(ExecutionEnv env) {
        return getDistLibPath(env.getDistDir());
    }
    
    
    /**
     * Get the dist lib path.
     * @param distPath  the dist path
     * @return the dist lib path
     */
    public static File getDistLibPath(File distPath) {
        return new File(distPath, PATH_LIB);
    }
    
    
    

    /**
     * Returns the the list of URL classpaths for the current system.
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return Path to the directorylist of classpaths
     */
    public static List<URL> getSystemClasspath(ExecutionEnv env) {
        
        try {
            FilenameFilter filter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    name = name.toLowerCase();
                    return name.endsWith(".jar") || name.endsWith(".zip");
                }
            };
            
            List<URL> ret  = new LinkedList<URL>();
            
            File libDir = getDistLibPath(env);
            File [] files = libDir.listFiles(filter);
            
            if(files != null) {
                for(File file: files) {
                    ret.add(file.toURI().toURL());
                }
            }
            
            File libLibDir = new File(libDir, "lib");
            files = libLibDir.listFiles(filter);
            
            if(files != null) {
                for(File file: files) {
                    ret.add(file.toURI().toURL());
                }
            }
            
            return ret;
            
        } catch (MalformedURLException ex) {
            throw new IllegalStateException("File.toURL reported error", ex);
        }
    }
    
    
    /**
     *  Get the path of the log directory
     *
     *  @param env ExecutionEnv of a current hedeby system
     *  @return path of the log directory
     */
    public static File getLogPath(ExecutionEnv env) {
        return getLogPath(env.getLocalSpoolDir());
    }
    
    /**
     * Get the path to the log directory of a component
     * @param env the execution env
     * @param componentName the name of the component
     * @return the path to the log directory of a component
     */
    public static File getLogPathForComponent(ExecutionEnv env, String componentName) {
        return new File(getLogPath(env), componentName);
    }

    
    /**
     *  Get the path of the log directory.
     *
     *  @param localSpoolDir the local spool dir
     *  @return path of the log directory
     */
    public static File getLogPath(File localSpoolDir) {
        return new File(localSpoolDir, PATH_LOG);
    }
 
    /**
     * get the spool directory (&lt;local_spool&gt;/spool).
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return  the spool directory
     */
    public static File getSpoolPath(ExecutionEnv env) {
        return new File(env.getLocalSpoolDir(), PATH_SPOOL);
    }
    
    /**
     * get the local spool directory for a component
     *
     * @param env ExecutionEnv of a current hedeby system
     * @param   componentName name of the component
     * @return  the local spool directory
     */
    public static File getSpoolDirForComponent(ExecutionEnv env, String componentName) {
        return new File(getSpoolPath(env), componentName);
    }

    /**
     * get the RP spool directory (&lt;local_spool&gt;/spool/resource_provider).
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return  the local RP spool directory
     */
    public static File getRPSpoolPath(ExecutionEnv env) {
        return new File(getSpoolPath(env), PATH_RP_SPOOL);
    }
    
    /**
     * get the local RP spool directory for a service caching proxy or resource
     * provider
     *
     * @param env ExecutionEnv of a current hedeby system
     * @param   name name of the service or resource provider component
     * @return  the local RP spool directory for a SCP or RP
     */
    public static File getRPSpoolDirForComponent(ExecutionEnv env, String name) {
        return new File(getRPSpoolPath(env),  name);
    }
    
    
    /**
     * Get the orders spool sir in resource provider spool dir for resource provider component
     * @param env ExecutionEnv of a current hedeby
     * @param name of resource provider component
     * @return orders spool dir in resource provider spool dir for resource provider component 
     */
    public static File getRPOrdersDir(ExecutionEnv env, String name) {
        return new File(getRPSpoolDirForComponent(env, name), PATH_ORDERS);
    }
    
    /**
     * Get the local temp directory (&lt;local_spool&gt;/tmp)
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return the local temp directory 
     */
    public static File getLocalTmpPath(ExecutionEnv env) {
        return new File(env.getLocalSpoolDir(), PATH_TMP);
    }
    
    /**
     * Get the run directory in local spool (&lt;local_spool&gt;/run)
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return the local &lt;local_spool&gt;/run directory 
     */
    public static File getLocalSpoolRunPath(ExecutionEnv env) {
        return new File(env.getLocalSpoolDir(), PATH_RUN);
    }

    /**
     * Get the path to host version file
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return the local &lt;local_spool&gt;/upgrade/version file
     */
    public static File getHostVersionFilePath(ExecutionEnv env) {
        return new File(env.getLocalSpoolDir(), VERSION_FILENAME);
    }

    /**
     * Get the host lock file
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return the lock file for a given host
     */
    public static File getLocalSpoolLockFilePath(ExecutionEnv env) {
        return new File(env.getLocalSpoolDir(), BootstrapConstants.LOCAL_SPOOL_UPGRADE_LOCKFILE);
    }
       
    /**
     * Get the local temp directory for a component
     *
     * @param env ExecutionEnv of a current hedeby system
     * @param componentName name of the component
     * @return the local temp directory of the component
     */
    public static File getTmpDirForComponent(ExecutionEnv env, String componentName) {
        return new File(getLocalTmpPath(env), componentName);
    }
    
    /**
     * get the pid file for a jvm
     *
     * @param env ExecutionEnv of a current hedeby system
     * @param jvmName name of the jvm
     * @return the pid file for the jvm
     */
    public static File getPidFile(ExecutionEnv env, String jvmName) {
        return new File(getLocalSpoolRunPath(env), jvmName + '@' + Hostname.getLocalHost().getHostname());
    }
    
    /**
     * get the pid files of all jvms running on a localhost
     *
     * @param env ExecutionEnv of a current hedeby system
     * @return list pf the pid files for the jvms
     */
    public static List<File> getPidFiles(ExecutionEnv env) {
        File runDir = getLocalSpoolRunPath(env);
        File[] filez = runDir.listFiles(new PidFilenameFilter());
        if (filez != null) {
            return java.util.Arrays.asList(runDir.listFiles(new PidFilenameFilter()));
        } else {
            return Collections.<File>emptyList();
        }
    }
    
    /**
     *  Get the local security directory.
     *  (&lt;local_spool&gt;/security)
     *  @param env ExecutionEnv of a current hedeby system
     *  @return the local security directory
     */
    public static File getLocalSecurityPath(ExecutionEnv env) {
        return new File(env.getLocalSpoolDir(), SECURITY_DIR);
    }
    
    /**
     *  Get the java.policy file location
     *  @param env ExecutionEnv of a current hedeby system
     *  @return the path to the java.policy file
     */
    public static File getSecurityJavaPolicy(ExecutionEnv env) {
        return new File(getLocalSecurityPath(env), "java.policy");
    }
     
    /**
     *  Get the jaas.config file location
     *  @param env ExecutionEnv of a current hedeby system
     *  @return the path to the jaas.config file
     */
    public static File getSecurityJaasConfig(ExecutionEnv env) {
        return new File(getLocalSecurityPath(env), "jaas.config");
    }
    
    /**
     *  Get the LICENSE file location
     *  @param distDir path to the distribution directory
     *  @return the path to the LICENSE file
     */
    public static File getLicensePath(File distDir) {
        return new File(distDir, LICENSE_PATH);
    }
    /**
     *  Get the file that contains sdm environment variables for rc support scripts
     *  @return the path to sdm.env file
     */
    public static File getSdmEnvFile() {
        return new File(SDM_ENV_PATH);
    }
    
    /**
     * Hepler method that reads the pid from the pidFile
     * @param file the pid fle
     * @return the pid from the pid file
     * @throws com.sun.grid.grm.GrmException  if the pid could not be read from file
     */
    public static int readPIDfromFile(File file) throws GrmException {
        int pid = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            pid = Integer.valueOf(br.readLine());
            br.close();          
        } catch (IOException ex) {
            throw new GrmException("ParentStartupService.exception.pid_process_exists", BUNDLE_NAME, pid);
        }
        return pid;
    }

    /**
     * Returns the location of java tmp directory
     * @return the File referring to the temp directory. Retrieved from java property "java.io.tmpdir"
     */
    public static File getTmpDir() {
        String tmp = System.getProperty("java.io.tmpdir");
        return new File(tmp);
    }
    
    /**
     * Returns the unique filename in specific directory. Unique means, file with such name does not exist
     * @param dir directory in which the file should be created
     * @param prefix for filename
     * @param retry number of retries before exception is thrown because unique filename cannot be created, with
     *        each retry new file is created
     * @return generated file
     * @throws java.io.IOException  if file cannot be generated
     */
    public static File getUniqueFile(File dir, String prefix, int retry) throws IOException {
        File tmp = new File(dir, prefix+"."+seq.incrementAndGet());
        for (int i=0; i<retry; i++) {
            if (tmp.exists()) {
                tmp = new File(dir, prefix+"."+seq.incrementAndGet());
            } else {
                return tmp;
            }
        }
        if (tmp.exists()) {
            throw new IOException(I18NManager.formatMessage("PathUtil.cannot_create_unique_file.pid_process_exists", BUNDLE_NAME, dir.getAbsolutePath(), prefix));
        }
        return tmp;
    }

    private static class PidFilenameFilter implements FilenameFilter {

        public boolean accept(File dir, String name) {
            if (name.split("@").length != 2) {
                return false;
            } else {
                String host = name.split("@")[1];
                return Hostname.getLocalHost().getHostname().equals(host);
            }            
        }
        
    } 
}
