/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.util.GrmFormatter;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.ErrorManager;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;

/**
 * The java.util.logging Handler that forwards log records JVMImpl instance
 * (jvm component of the SDM JVM).
 *
 * <p>The JVMLogHandler stores the log entries an internal queue. A publishing thraed
 *    thread forwards the log entries to <tt>JVMImpl.getInstance().publish()</tt>.
 *    The queue is processed in a FIFO order. The queue has a limited size (default  1000).
 *    If this limit is reached the oldest log record is discared before putting the new
 *    log record into the queue.</p>
 *
 * <b>Configuration:</b>
 * By default each <tt>JVMLogHandler</tt> is initialized using the following
 * <tt>LogManager</tt> configuration properties.  If properties are not defined
 * (or have invalid values) then the specified default values are used.
 * <ul>
 * <li>   com.sun.grid.grm.bootstrap.JVMLogHandler.level
 *	  specifies the default level for the <tt>Handler</tt>
 *	  (defaults to <tt>Level.ALL</tt>).</li>
 * <li>   com.sun.grid.grm.bootstrap.JVMLogHandler.queueLength
 *   specifies the limit of the log record queue (defaults to <tt>1000</tt>).</li>
 * </ul>
 *
 */
public class JVMLogHandler extends Handler {

    private final GrmFormatter formatter = new GrmFormatter();
    private final LinkedBlockingQueue<LogRecord> queue = new LinkedBlockingQueue<LogRecord>();
    private final int maxQueueLength;
    private final PublishThread publishThread = new PublishThread();

    /**
     * Create a new instance of the JVMLogHandler
     */
    public JVMLogHandler() {
        LogManager manager = LogManager.getLogManager();
        String cname = getClass().getName();

        String levelStr = manager.getProperty(cname + ".level");
        Level level = null;
        try {
            level = Level.parse(levelStr);
        } catch (Exception ex) {
            level = Level.ALL;
        }
        setLevel(level);

        String queueLengthStr = manager.getProperty(cname + ".queueLength");
        int queueLength;
        try {
            queueLength = Integer.parseInt(queueLengthStr);
        } catch(Exception ex) {
            queueLength = 1000;
        }
        maxQueueLength = queueLength;

        publishThread.start();
    }

    /**
     * publish a log record
     * @param record the log record
     */
    @Override
    public void publish(LogRecord record) {
        if (isLoggable(record)) {
            // Avoid high memory consumption
            while (queue.size() >= maxQueueLength) {
                queue.remove();
            }
            if (publishThread.isAlive()) {
                queue.add(record);
            } else {
                getErrorManager().error("Can not publish log record, publish thread died", null, ErrorManager.WRITE_FAILURE);
            }
        }
    }

    private class PublishThread extends Thread {

        PublishThread() {
            // The publishing thread should not block the shutdown process of the jvm
            // => let it run as daemon thread
            setDaemon(true);
        }
        
        @Override
        public void run() {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    LogRecord lr = queue.take();
                    JVMImpl jvm = JVMImpl.getInstance();
                    if (jvm != null) {
                        try {
                            // Translate the message
                            lr.setMessage(formatter.formatMessage(lr));
                            // Delete all elements that are only needed for the messsage
                            // formatting
                            lr.setResourceBundle(null);
                            lr.setResourceBundleName(null);
                            lr.setParameters(null);
                            jvm.publish(lr);
                        } catch (IOException ex) {
                            getErrorManager().error("Can not publish log record", ex, ErrorManager.WRITE_FAILURE);
                        }
                    }
                }
            } catch (InterruptedException ex) {
                // Ignore
            }
        }
    }

    /**
     * The flush method is an empty implementation
     */
    @Override
    public void flush() {
    }

    /**
     * Close the log handler
     *
     * This method interrupts the publishing thread and clear the log record
     * queue
     */
    @Override
    public void close() {
        publishThread.interrupt();
        queue.clear();
    }
}
