/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.security.SecurityModule;
import com.sun.grid.grm.util.EventListenerSupport;
import com.sun.grid.grm.util.GrmClassLoaderFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * This class is responsible for loading the hedeby modules from the system
 * class loader.
 *
 * <p>Hedeby modules includes the entry META-INF/services/com.sun.grid.grm.bootstrap.Module
 *    in the jar file.</p>.
 */
public class Modules {
    
    private static final String BUNDLE = "com.sun.grid.grm.bootstrap.messages";
    private static final String MODULE_SERVICE_ENTRY = "META-INF/services/" + Module.class.getName();
    
    private static Set<Module> modules;

    private static SecurityModule securityModule;
    private final static EventListenerSupport<ModuleListener> listeners = EventListenerSupport.<ModuleListener>newSynchronInstance(ModuleListener.class);
    
    private final static Logger log() {
        return Logger.getLogger(Modules.class.getName(), BUNDLE);
    }
    

    private synchronized static void initModules() {
        if(modules == null) {
            loadModules();
        }
    }
    
    /**
     *  Get all avaliable modules.
     * 
     *  @return set of available modules
     */
    public synchronized static Set<Module> getModules() {
        initModules();
        return modules;
    }
    
    /**
     * Get the security module.
     *
     * @throws com.sun.grid.grm.security.GrmSecurityException if no the security module has been found
     * @return the security modules
     */
    public synchronized static SecurityModule getSecurityModule() throws GrmSecurityException {
        initModules();
        if(securityModule == null) {
            throw new GrmSecurityException("Modules.nosec", BUNDLE, GrmClassLoaderFactory.toString(Modules.class.getClassLoader()));
        }
        return securityModule;
    }
    
    
    @SuppressWarnings(value="unchecked")
    private static void loadModules() {
        
        modules = new HashSet<Module>();
        
        modules.add(CommonModule.getInstance());
        
        ClassLoader loader = Modules.class.getClassLoader();
        
        if(log().isLoggable(Level.FINER)) {
            log().log(Level.FINER,"Modules.search", GrmClassLoaderFactory.toString(loader));
        }
            
        Enumeration<URL> services = null;
        try {
            services = loader.getResources(MODULE_SERVICE_ENTRY);
        } catch (IOException ex) {
            LogRecord lr = new LogRecord(Level.WARNING, "Modules.ioError");
            lr.setParameters(new Object[] { ex.getLocalizedMessage() });
            lr.setThrown(ex);
            lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
            log().log(lr);
            return;
        }
        
        while(services.hasMoreElements()) {
            URL url = services.nextElement();
            String classname = null;
            
            if(log().isLoggable(Level.FINER)) {
                log().log(Level.FINER,"Modules.read", url);
            }
            try {
                // The classname is the first line of the definition file
                BufferedReader rd = new BufferedReader(new InputStreamReader(url.openStream()));
                try {
                    classname = rd.readLine();
                    if(log().isLoggable(Level.FINER)) {
                        log().log(Level.FINER,"Modules.classname", classname);
                    }
                } finally {
                    rd.close();
                }
            } catch(IOException ex) {
                LogRecord lr = new LogRecord(Level.WARNING, "Modules.ioErrorURL");
                lr.setParameters(new Object[] { url, ex.getLocalizedMessage() });
                lr.setThrown(ex);
                lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                log().log(lr);
                continue;
            }
            
            try {
                // Load the class
                Class<? extends Module> moduleClass = (Class<? extends Module>)loader.loadClass(classname);
                try {
                    // Instantiate the module
                    Module module = moduleClass.newInstance();
                    modules.add(module);
                    if(module instanceof SecurityModule) {
                        if(securityModule == null) {
                            securityModule = (SecurityModule)module;
                        } else {
                            log().log(Level.WARNING,"Modules.additionalSec", new Object [] { classname, url });
                        }
                    }
                } catch (Exception ex) {
                    LogRecord lr = new LogRecord(Level.WARNING, "Modules.createError");
                    lr.setParameters(new Object[] { classname, ex.getLocalizedMessage()});
                    lr.setThrown(ex);
                    lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                    log().log(lr);
                }
            } catch (NoClassDefFoundError ex) {
                LogRecord lr = new LogRecord(Level.WARNING, "Modules.dependError");
                lr.setParameters(new Object[] { classname });
                lr.setThrown(ex);
                lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                log().log(lr);
            } catch (ClassCastException ex) {
                log().log(Level.WARNING, "Modules.noModul", classname);
            } catch (ClassNotFoundException ex) {
                log().log(Level.WARNING, "Modules.notFound", classname);
            }
            
        }
    }
    
    /**
     * Add a module.
     *
     * @param module  the module
     */
    public static void addModule(Module module) {
        initModules();
        boolean added;
        synchronized(modules) {
            added = modules.add(module);
        }
        if(added) {
            listeners.getProxy().moduleAdded(module);
        }
    }
    
    /**
     * Remove a module.
     *
     * @param module the module
     */
    public static void removeModule(Module module) {
        initModules();
        boolean removed;
        synchronized(modules) {
            removed = modules.remove(module);
        }
        
        if(removed) {
            listeners.getProxy().moduleRemoved(module);
        }
    }
    
    /**
     *  Add a module listener.
     *
     *  <p>This Listener will be informed of modules has been added/removed.
     *
     *  @param lis the listener
     */
    public static void addModuleListener(ModuleListener lis) {
        listeners.addListener(lis);
    }
    
    /**
     *  Remote a module listener.
     *
     *  @param lis the listener
     */
    public static void removeModuleListener(ModuleListener lis) {
        listeners.removeListener(lis);
    }
    
    /**
     *   ModuleListeners will be informed if a Module has been
     *   added removed
     */
    public interface ModuleListener {
        /**
         * A module has been added.
         *
         * @param module the added module
         */
        public void moduleAdded(Module module);
        
        /**
         *  A Module has been removed.
         *
         *  @param module the removed module
         */ 
        public void moduleRemoved(Module module);
    }
        
}
