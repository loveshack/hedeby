/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

/**
 * Contains constants used in more classes.
 *
 */
public interface BootstrapConstants {
    
    /** Key for the distribution directory. Is reused for
     *  system properties and preferences
     */
    public static final String DIST_KEY = "dist";
    
    /** Key for the local spool directory. Is reused for
     *  system properties and preferences
     */
    public static final String LOCAL_SPOOL_KEY = "localspool";
    
    /** Key for the url containing connection string to CS. 
     */
    public static final String CS_ID = "cs";        
    
    /** Name of the CS jvm. 
     */
    public static final String CS_JVM = "cs_vm";        
    
    /**
     * Name for hosts preferences
     */
    public static final String HOSTS_KEY = "hosts";
    
    /** Key for the url containing connection string to CS. 
     */
    public static final String CS_INFO_KEY = "csInfo";
    
    /** Key for the csHost
     */
    public static final String CS_HOST= "csHost";
    /** Key for the csPort
     */
    public static final String CS_PORT="csPort";
    
    /** Key for the active_component subcontext
     */
    public static final String CS_ACTIVE_COMPONENT="active_component";
    
    /** Key for the active_jvm subcontext
     */
    public static final String CS_ACTIVE_JVM="active_jvm";
    
    
    /** Key for the global object
     */
    public static final String CS_GLOBAL="global";
    
    /** Key for the security object
     */
    public static final String CS_SECURITY="security";
    
    
    /** Key for the component configuration subcontext
     */
    public static final String CS_COMPONENT="component";
       
    /**
     * Env variable name for customizing the memory heap for jvm in config
     */
    public static final String JVM_MEMORY_HEAP_VARIABLE_NAME="SDM_MEMORY_HEAP_SIZE_@@@NAME@@@";
    
    /**
     * Env variable for customizing the memory heap for jvm in config
     */
    public static final String JVM_MEMORY_HEAP_VARIABLE="${"+JVM_MEMORY_HEAP_VARIABLE_NAME+"}";
    
    /**
     * Boostrap i18n Bundle name
     */
    public static final String BUNDLE_NAME = "com.sun.grid.grm.bootstrap.messages";
    
    /**
     * Property name where the system name can be found
     */
    public static final String SYSTEM_NAME_PROPERTY = "com.sun.grid.grm.bootstrap.systemname";
    
    /**
     * Property which indicates wheter start of JVM is forced or not. 
     */
    public static final String JVM_START_FORCED = "com.sun.grid.grm.bootstrap.forceStartup";
    
    /**
     * Property name where the current JVM name can be found
     */
    public static final String JVM_NAME_PROPERTY = "com.sun.grid.grm.bootstrap.jvmname";

    public static final String CS_INFO_PROPERTY = "csInfo";
    
    /**
     * Property name which contains system preferences
     */
    public static final String SYSTEM_PREFERENCES_PROPERTY = "com.sun.grid.grm.bootstrap.preferencesType";
    
    public static final String IS_CS = "com.sun.grid.grm.bootstrap.isCS";
    /**
     * Interface name for components implementing the GrmService interface
     */
    public static final String GRM_SERVICE_INTERFACE = "GrmService";
    
    /**
     * Interface name for components implementing the Executor interface
     */
    public static final String GRM_EXECUTOR_INTERFACE = "Executor";
    
    /**
     * Interface name for components implementing the OsDistributor interface
     */
    public static final String GRM_DISTRIBUTOR_INTERFACE = "OsDistributor";
    
    /**
     * Interface name for components implementing the OsDistributor interface
     */
    public static final String GRM_SERVICE_CONTAINER_INTERFACE = "ServiceContainer";
    
    /**
     * Interface name for components implementing the ServiceManagement interface
     */
    public static final String GRM_SERVICE_MANAGEMENT = "ServiceManagement";
    
    /** 
     * Name of the system property which defines the path to the dist directory 
     */
    public static final String DIST_PATH_PROPERTY = "com.sun.grid.grm.bootstrap.dist";
        
    /** 
     * Name of the system property which defines wether security is disabled
     */
    public static final String SECURITY_DISABLED_PROPERTY = "com.sun.grid.grm.bootstrap.security_disabled";
    
    /** 
     * Name of the system property which defines the path to the local spool directory 
     */
    public static final String LOCAL_SPOOL_PATH_PROPERTY = "com.sun.grid.grm.bootstrap.localSpool";
      
    /**
     * Name of the system property which defines the path to the $SGE_ROOT directory
     */
    public static final String SGEROOT_PATH_PROPERTY = "com.sun.grid.grm.bootstrap.sgeroot";
    
    
    public static final String NAME_KEY_PROPERTY = "name";
    
    /**
     * This is the default value when JVM is not running in debug mode
     */
    public static final int NO_DEBUG_PORT = -1;  
    /**
     * This is the value for version of system
     */
    @Deprecated
    public static final String VERSION = "1.0u3";

    /**
     * This is the key for version pproperty of system
     */
    @Deprecated
    public static final String VERSION_KEY = "version";
    /**
     * This is the value for id of preferences
     */
    public static final String PREFERENCES_ID = "1.0";
    /**
     * This is the key for id of preferences
     */
    public static final String PREFERENCES_ID_KEY = "pref_id";
    
    /**
     * This value is used when sdm-upgrade.jar is not available.
     * To allow system to be installed. 
     */
    public static final String UNKNOWN_SYSTEM_VERSION = "unknown";
    /**
     * File name for the lock.
     */
    public static final String LOCAL_SPOOL_UPGRADE_LOCKFILE = ".upgradeLock";
  
    /**
     * This is key for SMF
     */
    public static final String SMF_KEY = "smf";
    /**
     * This key is used for determine Service Tag support
     */
    public static final String NOST_KEY = "nost";
    /**
     * This key keep the Service Tag unique instance urn for system installation
     */
    public static final String ST_INSTANCE_URN = "instance_urn";
    /**
     * This is name for RP_VM
     */
    public static final String RP_VM = "rp_vm";
    /**
     * This is name for EXECUTOR_VM
     */
    public static final String EXECUTOR_VM = "executor_vm";
}
