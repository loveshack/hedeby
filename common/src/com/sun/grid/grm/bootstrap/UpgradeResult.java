/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Object used to report result of upgrade, so caller can present, log it
 */

public class UpgradeResult implements Serializable {
    private static final long serialVersionUID = -2009102901L;
    private final LinkedList<String> successfulSteps = new LinkedList<String>();
    private GrmException error = null;
    private String errorStep = null;
    private final boolean skipped;

    /**
    * Construct UpgradeResult object
    */
    public UpgradeResult() {
        this.skipped = false;
    }

    /**
     * Construct UpgradeResult object
     * @param skipped - true if execution was skipped, upgrade not required
     */
    public UpgradeResult (boolean skipped) {
        this.skipped = skipped;
    }
    /**
     * @return the successfulSteps
     */
    public List<String> getSuccessfulSteps() {
        return successfulSteps;
    }

    /**
     * @return the error
     */
    public GrmException getError() {
        return error;
    }

    /**
     * Sets error information about failed step
     *
     * @param error execption that was thrown
     * @param errorStep string describing step in which error ocurred
     */
    public void setError(GrmException error, String errorStep) {
        this.error = error;
        this.errorStep = errorStep;
    }

    /**
     * @return the errorStep description string
     */
    public String getErrorStep() {
        return errorStep;
    }

    

    /**
     * Add successfulStep
     * @param stepDescription string describing step successfully executed
     */
    public void addSuccessfulStep(String stepDescription) {
        this.successfulSteps.add(stepDescription);
    }

    /**
     * Check if upgrade finished successfully
     * @return true if no error reported.
     */
    public boolean isSuccess() {
        if (this.error == null) {
            return true;
        }
        return false;
    }

    /**
     * @return true if execution was not triggered (no result)
     */
    public boolean isSkipped() {
        return skipped;
    }

}
