/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.cli.CliCommand;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.validate.Validator;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Abstract base class for the Hedeby modules.
 */
public abstract class AbstractModule implements Module {
    
    private Set<Class<? extends CliCommand>> cliExtensions;
    private Set<Class<? extends Validator>> validatorExtensions;
    private Set<Command> installMasterExtensions;
    private final String name;
    
    /**
     * Creates a new instance of AbstractModule
     * @param name name of the module
     */
    protected AbstractModule(String name) {
        this.name = name;
    }
    
    /**
     * Get the cli extensions of this module
     * @return the cli extensions
     */
    public Set<Class<? extends CliCommand>> getCliExtensions() {
        if(cliExtensions == null) {
            cliExtensions = new HashSet<Class<? extends CliCommand>>();
            loadCliExtension(cliExtensions);
        }
        return Collections.unmodifiableSet(cliExtensions);
    }
    
    /**
     * Get the validator extensions of this module
     * @return the validator extensions
     */
    public Set<Class<? extends Validator>> getValidatorExtensions() {
        if(validatorExtensions == null) {
            validatorExtensions = new HashSet<Class<? extends Validator>>();
            loadValidatorExtension(validatorExtensions);
        }
        return Collections.unmodifiableSet(validatorExtensions);
    }        
    
    /**
     * Load the cli extensions for this modules.
     *
     * @param extensions list where the classes for the cli extensions will be stored
     */
    protected abstract void loadCliExtension(Set<Class<? extends CliCommand>> extensions);
    
    /**
     * Load the validator extensions for this modules.
     *
     * @param extensions list where the classes for the validator extensions will be stored
     */
    protected abstract void loadValidatorExtension(Set<Class<? extends Validator>> extensions);
        
    /**
     *   get the name of the module
     */
    public String getName() {
        return name;
    }

    /**
     *  Get the a set of commands which are extecuted with the
     *  InstallMasterCommand.
     * 
     *  @return set of ui commands
     */
     public Set<? extends Command> getMasterInstallExtensions() {
         if (installMasterExtensions == null) {
             installMasterExtensions = new HashSet<Command>();
             loadMasterInstallExtensions(installMasterExtensions);
         }
         return installMasterExtensions;
     }
     
     /**
      * load the master install extensions
      * @param extensions set for storing the master install extensions
      */
     protected void loadMasterInstallExtensions(Set<Command> extensions) {
         // default implementation
         
     }
     
    /**
     * Get a set of new resource types introduced by this module
     * @return map of new resource types
     */
    public Set<ResourceType> getResourceTypes() {
        return Collections.<ResourceType>emptySet();
    }
     
    
}
