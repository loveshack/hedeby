/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.GrmXMLException;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.MultiComponent;
import com.sun.grid.grm.config.common.HostSet;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.Path;
import com.sun.grid.grm.ui.ConfigurationService;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NPrintWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.config.common.VersionString;
import com.sun.grid.grm.config.naming.FileCache;
import java.util.regex.Pattern;

/**
 * Helper class which provides set of methods for operations on the system like
 * reading/storing configurations
 */
public class SystemUtil {

    private static final String BUNDLE_NAME = "com.sun.grid.grm.bootstrap.messages";
    /* 
     * Allowed characters in Base64: a-z0-9A-Z+/
     * We reduce Base64 character set by "+" and "/" to be sure not to produce
     * "magic" directory creation when someone is using a name like "abc/AD"
     * The "+" would be the only one from preventing only alphanumeric values so
     * also the "+" character is not in the allowed character pattern.
     */
    private static final Pattern SYSTEM_NAME_MATCH_PATTERN = Pattern.compile("[a-zA-Z0-9]*");

    private static final Pattern COMPONENT_NAME_MATCH_PATTERN = Pattern.compile("[a-zA-Z0-9_]*");
    /**
     * Utility method to convert Path to String
     * @param path which will be converted to string
     * @return String that is representing path elements separated using
     *         File.pathSeparator
     */
    public static String getStringFromPath(Path path) {
        if (path != null && path.getPathelement() != null) {
            StringBuilder ret = new StringBuilder();
            boolean first = true;
            for (String elem : path.getPathelement()) {
                if (first) {
                    first = false;
                } else {
                    ret.append(File.pathSeparator);
                }
                ret.append(elem);
            }

            return ret.toString();
        } else {
            return "";
        }
    }

    /**
     * Get the CodeBase format string from Path
     * @param path object with paths
     * @return String which contains Urls separeted with " " character
     * @throws GrmException when convertion of paths to Url fails
     */
    public static String getJvmCodebaseStringFromPath(Path path) throws GrmException {
        StringBuilder ret = new StringBuilder();
        boolean first = true;
        for (String url : path.getPathelement()) {
            if (first) {
                first = false;
            } else {
                ret.append(" ");
            }
            try {
                ret.append(new URL(url));
            } catch (MalformedURLException ex) {
                throw new GrmException("SystemUtil.exception.can_not_create_jvm_classpath", ex, BUNDLE_NAME);
            }
        }
        return ret.toString();
    }

    /**
     * Based on current ExecutionEnv determines if SSL is disabled
     * @param env the system Execution
     * @return true if system is not secured
     */
    public static boolean isSSLDisabled(ExecutionEnv env) {
        Boolean ret = (Boolean) env.getProperties().get(env.NO_SSL);
        if (ret == null) {
            return false;
        } else {
            return ret.booleanValue();
        }
    }

    /**
     * Determin if the auto state flag for a system is set
     * @param env the execution env
     * @return <code>true</code> if the autostart flag is set
     */
    public static boolean isAutoStart(ExecutionEnv env) {
        Boolean ret = (Boolean) env.getProperties().get(env.AUTO_START);
        if (ret == null) {
            return false;
        } else {
            return ret.booleanValue();
        }
    }
    /**
     * Determine the system version
     * @param env the execution env
     * @return <code>String</code> representing the system version,
     * if version is not set "unknown" is returned.
     */
    @Deprecated
    public static String getVersion(ExecutionEnv env) {
        String ret = (String) env.getProperties().get(BootstrapConstants.VERSION_KEY);
        if (ret == null) {
            return "unknown";
        } else {
            return ret;
        }
    }
    /**
     * Should this component be started on the this host.
     * This method returns true if the given host is not contained in the
     * list of excluded host and and also contained in the list of
     * host.
     *
     * @param compConfig component configuration to evaluate
     * @return Hostname matches
     */
    public static boolean isThisHostMatching(Component compConfig) {
        return isHostMatching(compConfig, Hostname.getLocalHost());
    }

    /**
     * Should this component be started on the specified host.
     * This method returns true if the given host is not contained in the
     * list of excluded host and also contained in the list of
     * host.
     * @param compConfig component configuration to evaluate
     * @param hostname Hostname object of the host which should match
     * @return Hostname matches
     */
    public static boolean isHostMatching(Component compConfig, Hostname hostname) {
        if(compConfig instanceof Singleton) {
            return hostname.matches(((Singleton)compConfig).getHost());
        } else if (compConfig instanceof MultiComponent) {
            HostSet compHosts = ((MultiComponent)compConfig).getHosts();
            for (String excludeHost : compHosts.getExclude()) {
                if (hostname.matches(excludeHost)) {
                    return false;
                }
            }
            for (String host : compHosts.getInclude()) {
                if (hostname.matches(host)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determine of this jvm has a component which should run on the
     * localhost
     * @param jvmConfig jvm configuration to evaluate
     * @return <code>true</code> of this jvm has components on the localhost
     */
    public static boolean hasComponentOnLocalhost(JvmConfig jvmConfig) {
        for (Component comp : jvmConfig.getComponent()) {
            if (isThisHostMatching(comp)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Helper method that converts list of jvm arguments (of strings) to one
     * string, the elements are separeted with space character
     * @param jvm configuration of jvm to be evaluated
     * @return String
     */
    public static String getJvmArgs(JvmConfig jvm) {
        StringBuilder ret = new StringBuilder();
        for (String str : jvm.getJvmArg()) {
            ret.append(str);
            ret.append(" ");
        }
        return ret.toString();
    }

    /**
     * Helper method for parsing csInformation String, the validation of parameter is done here
     * @param csInfo - string with information about CS component "&lt;host&gt;:&lt;port&gt;"
     * @return - array of String parameters retrieved from csInfo
     * @throws GrmException if csInfo is contains corrupted data
     */
    public static String[] getCSPropertiesFromString(String csInfo) throws GrmException {
        String[] info = csInfo.split(":");
        if (info.length != 2) {
            throw new GrmException("SystemUtil.exception.csinfo_wrong_format", BUNDLE_NAME, csInfo);
        }
        //Check if port is an integer
        String port = info[1];
        try {
            int i = Integer.parseInt(port);
        } catch (NumberFormatException ex) {
            throw new GrmException("SystemUtil.exception.csPort_not_int", ex, BUNDLE_NAME, port);
        }
        return info;
    }

    /** Method retriving property for CS from array of CS properities
     * @param csProperties - array of CS properties
     * [0] - hostname information
     * [1] - port information
     * @param key - the property name
     * @return - parameter as string
     * @throws GrmException when parameter is not found
     */
    public static String getCSProperty(String[] csProperties, String key) throws GrmException {
        String host = csProperties[0];
        String port = csProperties[1];

        if (key.equals(BootstrapConstants.CS_HOST)) {
            return host;
        }
        if (key.equals(BootstrapConstants.CS_PORT)) {
            return port;
        }
        throw new GrmException("SystemUtil.exception.cs_property_not_available", BUNDLE_NAME, key);
    }   

    /**
     *  Determine if we are running in the JVM hosting the CS.
     *
     *  @return <code>true</code> if the host jvm is the one with CS
     */
    public static boolean isThisCSJvm() {
        return Boolean.valueOf(System.getProperty(BootstrapConstants.IS_CS));
    }
                
    /**
     *  Determine if the host and port are the ones of JVM hosting the CS.
     *
     * @param env execution enviroment to take CS communication information
     * @param host jvm hostname to be evaluated
     * @param port jvm port number to be evaluated
     * @return <code>true</code> if the host and port of a JVM belongs to CS JVM
     */
    public static boolean isCSJvmHostAndPort(ExecutionEnv env, Hostname host, int port) {
        return env.getCSHost().equals(host)
            && port==env.getCSPort();
    }
    
    /**
     *  Determine if the host and port are the ones of JVM hosting the CS.
     *
     * @param env execution enviroment to take CS communication information
     * @param host jvm hostname to be evaluated
     * @param port jvm port number to be evaluated
     * @return <code>true</code> if the host and port of a JVM belongs to CS JVM
     */
    public static boolean isCSJvmHostAndPort(ExecutionEnv env, String host, int port) {        
        Hostname urlHost = Hostname.getInstance(host);
        return env.getCSHost().equals(urlHost)
            && env.getCSPort() == port;
    }

    /**
     * Determine if a string is a valid name for a Hedeby component
     *
     * @param name the name
     * @throws InvalidComponentNameException if <code>name</code> of not a valid component name
     */
    public static void validateComponentName(String name) throws InvalidComponentNameException {
        if ((name == null)  || (name.length() == 0) || !COMPONENT_NAME_MATCH_PATTERN.matcher(name).matches()) {
            throw new InvalidComponentNameException("bootstrap.exception.invalid_comp_name", BUNDLE_NAME, new Object[]{name});
        }
    }

    /**
     * Determine if a string is a valid name for a Hedeby system
     *
     * @param name the name
     * @throws InvalidComponentNameException if <code>name</code> of not a valid hedeby system name
     */
    public static void validateSystemName(String name) throws InvalidComponentNameException {
        if ((name == null)  || (name.length() == 0) || !SYSTEM_NAME_MATCH_PATTERN.matcher(name).matches()) {
            throw new InvalidComponentNameException("bootstrap.exception.invalid_grm_name", BUNDLE_NAME, new Object[]{name});
        }
    }
    /**
     * Determine whether the CS is started.
     * 
     * @param env execution enviroment
     * @return true is CS is started, false otherwise
     */
    public static boolean isCSStarted(ExecutionEnv env) {
        try {
            ConfigurationService cs = ComponentService.getCS(env);
            if (cs.getState().equals(ComponentState.STARTED)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception exception) {
            return false;
        }
    }
    
    /**
     * SMF FMRI - full name of service with instance that is generated by svccfg when importing
     * manifest. This helper method generates the same string for SDM. There are two templates for
     * the name, one for CS jvm and the other for the rest of jvms.
     * cs_jvm - svc:/application/management/SDM/systemName/cs_vm:default
     * other jvms - svc:/application/management/SDM/systemName/jvm:jvmName
     *
     * @param systemName name of the system
     * @param jvmName name of jvm
     * @return full name of service with instance
     */
    public static String getSMFFMRI(String systemName, String jvmName) {
        String prefix = "svc:/";
        StringBuilder sb = new StringBuilder();
        sb.append(prefix);
        sb.append(getSMFName(systemName, jvmName));
        sb.append(":");
        sb.append(getSMFInstanceName(jvmName));
        return sb.toString();
    }
    
    /**
     * Returns name of service. This helper method generates the same string for SDM. 
     * There are two templates for the name, one for CS jvm and the other for the rest of jvms.
     * cs_jvm - application/management/SDM/systemName/cs_vm
     * other jvms - application/management/SDM/systemName/jvm
     *
     * @param systemName name of the system
     * @param jvmName name of jvm
     * @return name of SMF service for jvm
     */
    public static String getSMFName(String systemName, String jvmName) {
        String tmp = "application/management/sdm";
        StringBuilder sb = new StringBuilder();
        sb.append(tmp);
        sb.append("/");
        sb.append(systemName);
        sb.append("/");
        tmp = "jvm";
        if (jvmName.equals(BootstrapConstants.CS_JVM)) {
            tmp = jvmName;
        }
        sb.append(tmp);
        return sb.toString();
    }
    
    /**
     * Returns instance name for jvm. For 
     * cs_jvm - it is default
     * other jvms - it is jvmName
     *
     * @param jvmName name of jvm
     * @return SMF instance name for jvm
     */
    public static String getSMFInstanceName(String jvmName) {
        if (jvmName.equals(BootstrapConstants.CS_JVM)) {
            jvmName="default";
        }       
        return jvmName;
    }
    
    /**
     * Determine if the smf flag for a system is set
     * @param env the execution env
     * @return <code>true</code> if the smf flag is set
     */
    public static boolean isSMF(ExecutionEnv env) {
        Boolean ret = (Boolean) env.getProperties().get(BootstrapConstants.SMF_KEY);
        if (ret == null) {
            return false;
        } else {
            return ret.booleanValue();
        }
    }
    
    /** 
     * Retrieve the License String and print it out
     * @param distDir distribution directory
     * @param pw <code>I18NPrintWriter</code> used for printing out the license
     * @throws GrmException when the error during reading the license file occurs
     */
    public static void printLicense(File distDir, I18NPrintWriter pw) throws GrmException{
        File tmp = PathUtil.getLicensePath(distDir);
        
        try {
            BufferedReader in = new BufferedReader(new FileReader(tmp.getPath()));
            String line;
            while ((line = in.readLine()) != null) {
                pw.printlnDirectly(line);
            }
            in.close();
        } catch (FileNotFoundException ex) {
            throw new GrmException("SystemUtil.exception.license_file_not_found", ex, BUNDLE_NAME, tmp.getAbsolutePath());
        } catch (IOException ex) {
            throw new GrmException("SystemUtil.exception.license_file_read_failed", ex, BUNDLE_NAME, tmp.getAbsolutePath());
        }

    }
    
    /**
     * Return string representing memory variable
     * @param jvm name for which memory variabole should be generated
     * return memory variable for jvm
     * 
     * Example: for jvm=cs_vm
     * Return value "${SDM_MEMORY_HEAP_SIZE_cs_vm}" 
     */
    public static String getMemoryVariable(String jvm) {
        String ret = BootstrapConstants.JVM_MEMORY_HEAP_VARIABLE;
        ret = ret.replace("@@@NAME@@@", jvm);
        return ret;
    }
    
    /**
     * Return string representing memory variavle name
     * @param jvm name for which memory variabole should be generated
     * return memory variable name for jvm
     * 
     * Example: for jvm=cs_vm
     * Return value "SDM_MEMORY_HEAP_SIZE_cs_vm" 
     */
    public static String getMemoryVariableName(String jvm) {
        String ret = BootstrapConstants.JVM_MEMORY_HEAP_VARIABLE_NAME;
        ret = ret.replace("@@@NAME@@@", jvm);
        return ret;
    }

    /**
     * Retrieve spooled version information for localhost
     * @param env ExecutionEnv
     * @return String representing the version of host
     * @throws GrmException when cannot read from version file
     */
    public static String getHostVersion(ExecutionEnv env) throws GrmException {
        String version;
        File versionfile = PathUtil.getHostVersionFilePath(env);
        if (!versionfile.exists()) {
            return BootstrapConstants.VERSION;
        }
        try {
            version = ((VersionString) FileCache.getInstance(versionfile).read()).getVersion();
        } catch (GrmXMLException ex) {

            throw new GrmException("SystemUtil.error.cannotreadhostversion", ex, BUNDLE_NAME, versionfile.getAbsolutePath(), ex.getLocalizedMessage());
        } catch (IOException ex) {

            throw new GrmException("SystemUtil.cannotreadhostversion", ex, BUNDLE_NAME, versionfile.getAbsolutePath(), ex.getLocalizedMessage());
        }
        return version;
    }

    /**
     * Retrive system version from a running ConfigurationService (CS)
     * @param env ExecutionEnv
     * @return version of running CS
     * @throws GrmException when cannot ask ConfigurationService for version 
     */
    public static String getSystemVersion(ExecutionEnv env) throws GrmException {
        ConfigurationService cs;
        try {
            cs = ComponentService.getCS(env);
            return cs.getVersion();

        } catch (Exception ex) {
            throw new GrmException("SystemUtil.error.cannotgetversion", ex, BUNDLE_NAME);
        }
    }
    /**
     * Retrieve version of binaries
     * @param env ExecutionEnv
     * @return String representing release version of binaries, if it is not official release
     *         unknown is returned
     * @throws GrmException when cannot read from version file
     */
    public static String getBinaryVersion(ExecutionEnv env) throws GrmException {
        String version;
        try {
            version = UpgradeUtil.getBinaryVersion(env);
        } catch (UpgradeNotAvailableException ex) {
            version = BootstrapConstants.UNKNOWN_SYSTEM_VERSION;
        }
        return version;
    }
}
