/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * ParentStartupService.java
 *
 * Created on May 24, 2006, 10:49 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.Path;
import com.sun.grid.grm.executor.impl.ExecutorImpl;
import com.sun.grid.grm.reporting.impl.ReporterImpl;
import com.sun.grid.grm.resource.impl.ResourceProviderImpl;
import com.sun.grid.grm.sparepool.SparePoolServiceImpl;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.component.GetJVMConfigurationCommand;
import com.sun.grid.grm.ui.component.GetJVMConfigurationsCommand;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.NonblockingCommand;
import com.sun.grid.grm.util.Platform;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.io.File;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * The parent startup service will spawn multiple JVMs.
 *
 * <p> One instance of this class will be created during bootime of a machine.
 * This instance will read all system names of GRM systems which are
 * running on that machine in parallel.
 *
 * <p> It will read the system configuration of each system to identify
 * how many JVMs have to be started for a GRM system.
 *
 * <p> Then it will create these new JVMs and start one instance of the
 * <code>ClientStartupService</code> in each JVM. The commandline to start those
 * JVMs will be read from the Property <code>com.sun.grid.grm.bootstrap.jvmcommand</code>
 * which has to be contained in the file <code>ParentStartupService.properties</code>
 *
 * <p> Example:
 * <p><code>com.sun.grid.grm.bootstrap.jvmcommand= \<br>
 * ${java.home}/bin/java \<br>
 * -Dcom.sun.grid.grm.bootstrap.systemname=${systemname} \<br>
 * -Dcom.sun.grid.grm.bootstrap.jvmname=${jvmname} \<br>
 * -Dcom.sun.grid.grm.bootstrap.preferencesToUse=User \<br>
 * -cp ${java.class.path} \
 * com.sun.grid.grm.bootstrap.ChildStartupService</code>
 *
 * <p> Before the command is executed several parameters will be replaced
 * by corresponding values. Following parameter/values are allowed:
 *
 * <p><table>
 *      <tr><td><b>Parameter</b></td>                <td><b>Value</b></td></tr>
 *      <tr><td><code>${java.home}</code></td>       <td>Property value of "java.home"</td></tr>
 *      <tr><td><code>${java.class.path}</code></td> <td>Property value of "java.class.path"</td></tr>
 *      <tr><td><code>${systemname}</code></td>      <td>Current GRM systemname</td></tr>
 *      <tr><td><code>${jvmname}</code></td>         <td>Current JVM name</td></tr>
 *      <tr><td><code>${local_spool}</code></td>     <td>local spool directory of the GRM system</td></tr>
 *      <tr><td><code>${dist}</code></td>            <td>dist directory of the GRM system</td></tr>
 *      <tr><td><code>${shared}</code></td>          <td>shared directory of the GRM system</td></tr>
 *      <tr><td><code>${libPath}</code></td>         <td>shared library path</td></tr>
 *      <tr><td><code>${java.class.path}</code></td> <td>the classpath </td></tr>
 * </table>
 *
 */
public final class ParentStartupService {

    private static final String BUNDLE_NAME = "com.sun.grid.grm.bootstrap.messages";

    /**
     * Logger which will be used by this component.
     */
    private static Logger log = Logger.getLogger(ParentStartupService.class.getName(), BUNDLE_NAME);
    private static final String clz = ParentStartupService.class.getName();
    private static Map<File, String> datamodelMap = new HashMap<File, String>();
    private static final String MEMORY_BUNDLE = "com.sun.grid.grm.bootstrap.memory";
    private static final ResourceBundle rb = ResourceBundle.getBundle(MEMORY_BUNDLE);
    
    /**
     * Property name where this class can find the jvm command.
     */
    private static final String propertyNameJvmCommand = "com.sun.grid.grm.bootstrap.jvmcommand";
    
    /**
     * Start a single JVM
     * 
     * @param forced forced startup
     * @param env the system Execution env
     * @param jvm the name of the jvm
     * @param debugPort debug port for running JVM in debug mode
     * @param prefs system preferences 
     * @throws java.lang.InterruptedException if the startup has been interrupted
     * @throws GrmException if general error occured that did not allow to begin the process of startup of JVM
     * @return map with string containing name of Jvm as key, and value <code>Throwable</code>
     * containing the expection that occurred during startup, if startup was successful the value equals null
     */
    public static Map<String, Throwable> start(ExecutionEnv env, String jvm, int debugPort, boolean forced, PreferencesType prefs) throws InterruptedException, GrmException {
        log.entering(clz, "start");
        Map<String, Throwable> ret = new HashMap<String, Throwable>(1);
        ret.put(jvm, null);
        
        JvmConfig jvmConfig = getJvmConf(env, jvm);
        if (jvmConfig != null) {
            File pidFile = PathUtil.getPidFile(env, jvm);
            if (pidFile.exists() && (!forced || forced && Platform.getPlatform().existsProcess(PathUtil.readPIDfromFile(pidFile)))) {
                //error for jvm
                ret.put(jvm, new GrmException("ParentStartupService.skipRunningJvm", BUNDLE_NAME, jvmConfig.getName(), pidFile));
                return ret;
            } else {
                //check if has running components and is not CS vm
                if (!SystemUtil.isCSJvmHostAndPort(env, Hostname.getLocalHost(), jvmConfig.getPort()) && !SystemUtil.hasComponentOnLocalhost(jvmConfig)) {
                    //dont start jvm - throw exception
                    ret.put(jvmConfig.getName(), new SkippedJVMException("ParentStartupService.skipped.nocomponents", BUNDLE_NAME));
                    return ret;
                }
                try {
                    int r = fork(env, jvmConfig, debugPort, forced, pidFile.getAbsolutePath(), prefs);
                    if (r == 1) {
                        ret.put(jvm, new GrmException("ParentStartupService.failed_jvm_fork_jvm_died", BUNDLE_NAME, jvmConfig.getName()));
                        return ret;
                    } else if (r == 2) {
                        ret.put(jvm, new GrmException("ParentStartupService.failed_jvm_fork_pid_file_timeout", BUNDLE_NAME, jvmConfig.getName(),pidFile));
                        return ret;
                        //Issue 621 fix
                    } if (r != 0) {
                        ret.put(jvm, new GrmException("ParentStartupService.failed_jvm_unexpected", BUNDLE_NAME, jvmConfig.getName(),r));
                    }
                    //fork can return 0,1,2 - 0 means no error detected
                } catch (InterruptedException ex) {
                    ret.put(jvm, new GrmException("ParentStartupService.failed_jvm_fork", ex, BUNDLE_NAME, jvmConfig.getName(), ex.getLocalizedMessage()));
                    return ret;
                } catch (GrmException ex) {
                    ret.put(jvm, ex);
                    return ret;
                } catch (IOException ex) {
                    ret.put(jvm, new GrmException("ParentStartupService.failed_jvm_fork", ex, BUNDLE_NAME, jvmConfig.getName(), ex.getLocalizedMessage()));
                    return ret;
                }
                
            }
        } else {
            ret.put(jvm, new GrmException("ParentStartupService.jvmnotfound", BUNDLE_NAME, jvm));
            return ret;
        }
        
        log.exiting(clz, "start");
        return ret;
    }
        
    /**
     * If CS is started, use regular way of getting the JVM configurations otherwise
     * retrieve configurations locally.
     * 
     * @param env execution enviroment
     * @return list of retrieved JVM configurations
     * @throws GrmException if there is any problem
     */
    private static List<JvmConfig> getJvmConfList(ExecutionEnv env) throws GrmException {
        Command<List<JvmConfig>> cmd = new GetJVMConfigurationsCommand();
        if (SystemUtil.isCSStarted(env)) {
            return env.getCommandService().<List<JvmConfig>>execute(cmd).getReturnValue();
        } else if (Hostname.getLocalHost().equals(env.getCSHost())){
            return env.getCommandService().<List<JvmConfig>>executeLocally(cmd).getReturnValue();
        } else {
            throw new GrmException("ParentStartupService.cs_vm_not_reachable_for_managed", BUNDLE_NAME, env.getCSHost(), Hostname.getLocalHost());
        }
    }

    /**
     * If CS is started, use regular way of getting the JVM configuration otherwise
     * retrieve configurations locally.
     * 
     * @param env execution environment
     * @param jvm name of the JVM
     * @return retrieved JVM configuration
     * @throws GrmException if there is any problem
     */
    private static JvmConfig getJvmConf(ExecutionEnv env, String jvm) throws GrmException {
        GetJVMConfigurationCommand cmd = new GetJVMConfigurationCommand(jvm);
        if (SystemUtil.isCSStarted(env)) {                        
            return env.getCommandService().<JvmConfig>execute(cmd).getReturnValue();
        } else {
            return env.getCommandService().<JvmConfig>executeLocally(cmd).getReturnValue();
        }
    }

    /**
     * Start a system.
     * 
     * @param  env  the system Execution env
     * @param forced if set to true JVM will run even if the pid file exists 
     * in local Run directory.
     * @param prefs system preferences
     * @throws GrmException in case of a problem when retrieving JVM configurations
     * 
     * @throws InterruptedException if forking a new JVM process is interrupted
     * @return Map with the name of Jvm as a key, the value contains result of forking the jvm,
     * this method can start serveral jvms, start of some of them can fail and the
     * information about failure details can be retrieved from this map, if jvm started successfully the value
     * will be null
     */
    public static Map<String, Throwable> start(ExecutionEnv env, boolean forced, PreferencesType prefs) throws InterruptedException, GrmException {
        log.entering(clz, "start");
        Map<String, Throwable> ret = new HashMap<String, Throwable>();
        
        List<String> skippedJvms = new LinkedList<String>();
        Map<String, NonblockingCommand> commands = new HashMap<String, NonblockingCommand>();



            List<JvmConfig> list = getJvmConfList(env);
            if (list != null) {
                // find if a csJVM is in the list, if so put it a side
                // to give it enough time to start first
                List<JvmConfig> jvmList = new LinkedList<JvmConfig>();
                JvmConfig csConf = null;
                for (JvmConfig jvmConf : list) {

                    File pidFile = PathUtil.getPidFile(env, jvmConf.getName());
                    if (pidFile.exists()) {
                        if (!forced || (forced && Platform.getPlatform().existsProcess(PathUtil.readPIDfromFile(pidFile)))) {
                            ret.put(jvmConf.getName(), new GrmException("ParentStartupService.skipRunningJvm", BUNDLE_NAME, jvmConf.getName(), pidFile));
                            continue;
                        }
                    }

                    // if the jvm is the cs jvm we start it despite it has no
                    // components
                    if (SystemUtil.isCSJvmHostAndPort(env, Hostname.getLocalHost(), jvmConf.getPort())) {
                        //add cs to the begining
                        jvmList.add(0, jvmConf);
                    } else if (SystemUtil.hasComponentOnLocalhost(jvmConf)) {
                        jvmList.add(jvmConf);
                    } else {
                        // If the jvm has not running components in the localhost
                        // we do not start it.
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "ParentStartupService.jvmNoComponent", new Object[]{jvmConf.getName(), Hostname.getLocalHost().getHostname()});
                        }
                        continue;
                    }
                }

                for (JvmConfig jvmConf : jvmList) {
                    try {              
                        File pidFile = PathUtil.getPidFile(env, jvmConf.getName());
                        int r = fork(env, jvmConf, BootstrapConstants.NO_DEBUG_PORT, forced, pidFile.getAbsolutePath(), prefs);
                        if (r == 1) {
                            ret.put(jvmConf.getName(), new GrmException("ParentStartupService.failed_jvm_fork_jvm_died", BUNDLE_NAME, jvmConf.getName()));
                            continue;
                        } else if (r == 2) {
                            ret.put(jvmConf.getName(), new GrmException("ParentStartupService.failed_jvm_fork_pid_file_timeout", BUNDLE_NAME, jvmConf.getName(),pidFile));
                            continue;
                            //Issue 621
                        } if (r != 0) {
                            ret.put(jvmConf.getName(), new GrmException("ParentStartupService.failed_jvm_unexpected", BUNDLE_NAME, jvmConf.getName(),r));
                            continue;
                        }
                        ret.put(jvmConf.getName(), null);
                        //fork can return 0,1,2 - 0 means no error detected
                    } catch (InterruptedException ex) {
                        ret.put(jvmConf.getName(), new GrmException("ParentStartupService.failed_jvm_fork", ex, BUNDLE_NAME, jvmConf.getName(), ex.getLocalizedMessage()));
                        continue;
                    } catch (GrmException ex) {
                        ret.put(jvmConf.getName(), ex);
                        continue;
                    } catch (IOException ex) {
                        ret.put(jvmConf.getName(), new GrmException("ParentStartupService.failed_jvm_fork", ex, BUNDLE_NAME, jvmConf.getName(), ex.getLocalizedMessage()));
                        continue;
                    }
                }
            }
        log.exiting(clz, "start");
        return ret; 
        
    }

    private static int fork(ExecutionEnv env, JvmConfig jvmConf, int debugPort, boolean forced, String pidFile, PreferencesType prefs) throws IOException, InterruptedException, GrmException {

        String jvmCommand = null;

        final String ldLibPath = SystemUtil.getStringFromPath(jvmConf.getLdLibraryPath());
        final String distLibPath = PathUtil.getDistLibPath(env).getAbsolutePath() + File.separatorChar + getMappedArch(Platform.getPlatform().getArch(env.getDistDir()));
        final String libPath = (ldLibPath != null) ? (File.pathSeparator + distLibPath + File.pathSeparator + ldLibPath) : (File.pathSeparator + distLibPath);
        jvmCommand = getJvmCommand(env, jvmConf, libPath, debugPort, forced, prefs);
        //Contains environment
        Env parentEnv = new Env(System.getenv());
        //Setup env variables
        parentEnv.addAndReplace("LD_LIBRARY_PATH", libPath);
        String mem = "-Xmx"+getDefaultMemoryHeapSize(env, jvmConf)+"M";
        parentEnv.addNew(SystemUtil.getMemoryVariableName(jvmConf.getName()), mem);
        
        
        String username = jvmConf.getUser();

        if (log.isLoggable(Level.FINE)) {
            log.fine("Parameter for \"" + jvmConf.getName() + "\"");
            log.fine("Commandline: \"" + jvmCommand + "\"");
            log.fine("User: \"" + username + "\"");
        }
        if (log.isLoggable(Level.FINER)) {
            log.fine("Environment:");
            for (String e : parentEnv.getEnvAsArray()) {
                log.finer("> " + e.toString());
            }
        }
        File startTemplate = new File(PathUtil.getTemplatePath(env), "start_sh.template");
        
        File tmp = PathUtil.getLocalTmpPath(env);
        tmp = new File(tmp, "start_" + jvmConf.getName() + ".sh");
        Platform.getPlatform().deleteOnExit(tmp);
        Map<String, String> patterns = new HashMap<String, String>(2);
        patterns.put("@@@COMMAND@@@", jvmCommand);
        patterns.put("@@@PID_FILE@@@", pidFile);
        FileUtil.replace(startTemplate, tmp, patterns);
        Platform.getPlatform().chmod(tmp, "755");
        return Platform.getPlatform().execAs(username, tmp.getAbsolutePath(), parentEnv.getEnvAsArray(), env.getLocalSpoolDir(), null, null, 0);
    }


   

    /**
     * Get the datamodel for the child jvm.
     * @param hedebyDist
     * @return the datamodel
     * @see com.sun.grid.grm.util.Platform.getArch(java.io.File)
     */
    private static String getDataModel(File hedebyDist) {
        String ret = datamodelMap.get(hedebyDist);
        if (ret == null) {
            ret = "";
            try {
                String arch = Platform.getPlatform().getArch(hedebyDist);

                if (arch.equals("solaris64") || arch.equals("sol-sparc64") || arch.equals("sol-amd64") || arch.equals("darwin-x86_64")) {
                    ret = "-d64";
                }
            } catch (IOException ioe) {
                log.log(Level.WARNING, "bootstrap.warning.arch_error", ioe.getLocalizedMessage());
            }
            datamodelMap.put(hedebyDist, ret);
        }
        return ret;
    }

    /**
     * This method returns a shell command which can be used to start another JVM.
     * To get the correct command the function reads the property file
     * which has the same basename like this class. From that file it will
     * read the string property <code>com.sun.grid.grm.bootstrap.jvmcommand</code>.
     * Before the String is returned a parameter/value mapping will be applied.
     * Following parameters will be replaced by corresponding values:
     * <table>
     *      <tr><td><b>Parameter</b></td><td><b>Value</b></td></tr>
     *      <tr><td>${java.home}</td><td>Property value of "java.home"</td></tr>
     *      <tr><td>${java.class.path}</td><td>Property value of "java.class.path"</td></tr>
     *      <tr><td>${systemname}</td><td>Current GRM systemname</td></tr>
     *      <tr><td>${jvmname}</td><td>Current JVM name</td></tr>
     *      <tr><td>${libPath}</td><td>Current JVM native lib path</td></tr>
     * </table>
     * @param env
     * @param jvmConfig
     * @param libPath
     * @param prefs system preferences type
     * @return string representing generated command
     * @throws java.io.IOException
     * @throws com.sun.grid.grm.GrmException
     */
    private static final String getJvmCommand(final ExecutionEnv env, final JvmConfig jvmConfig, final String libPath, int debugPort, boolean forced, PreferencesType prefs) throws IOException, GrmException {


        log.entering(clz, "getJvmCommand");
        String systemName = env.getSystemName();
        String name = new String('/' + ParentStartupService.class.getName().replace('.', '/') + ".properties");
        InputStream in = ParentStartupService.class.getResourceAsStream(name);
        Properties props = new Properties();


        props.load(in);
        // validations
        File loggingProp = new File(env.getLocalSpoolDir().getAbsolutePath() + File.separator + "logging.properties");
        if (!loggingProp.exists()) {
            String pattern = ResourceBundle.getBundle(BUNDLE_NAME).getString("bootstrap.exception.file_does_not_exist");
            throw new IOException(MessageFormat.format(pattern, loggingProp.toString()));
        } else if (!loggingProp.canRead()) {
            String pattern = ResourceBundle.getBundle(BUNDLE_NAME).getString("bootstrap.exception.file_is_not_readable");
            throw new IOException(MessageFormat.format(pattern, loggingProp.toString()));
        }

        StringBuilder jvmCommandBuffer = new StringBuilder(props.getProperty(propertyNameJvmCommand));

        replaceParameter(jvmCommandBuffer, "${jvm.data.model}", getDataModel(env.getDistDir()));
        replaceParameter(jvmCommandBuffer, "${java.home}", System.getProperty("java.home"));
        replaceParameter(jvmCommandBuffer, "${file.separatorChar}", "" + File.separatorChar);
        if (Platform.isWindowsOs()) {
            replaceParameter(jvmCommandBuffer, "${java.binary}", "java.exe");
        } else {
            replaceParameter(jvmCommandBuffer, "${java.binary}", "java");
        }
        
        // Setup classpath;
        File distLibDir = PathUtil.getDistLibPath(env);

        Path systemClasspath = new Path();
        List<URL> codebaseList = new LinkedList<URL>();

        File[] files = distLibDir.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                return !pathname.isDirectory() && !pathname.getName().endsWith("impl.jar") && pathname.getName().endsWith(".jar");
            }
        });
        if (files != null) {
            for (File file : files) {
                log.log(Level.FINE, "bootstrap.add_to_system_classpath", file);
                systemClasspath.getPathelement().add(file.getAbsolutePath());
                try {
                    codebaseList.add(file.toURI().toURL());
                } catch (MalformedURLException ex) {
                    throw new IllegalStateException("file toURL returned error", ex);
                }
            }
        }


        File extDir = new File(distLibDir, "ext");
        files = extDir.listFiles(new FileFilter() {

            public boolean accept(File pathname) {
                return !pathname.isDirectory() && pathname.getName().endsWith(".jar");
            }
        });
        if (files != null) {
            for (File file : files) {
                log.log(Level.FINE, "bootstrap.add_to_system_classpath", file);
                systemClasspath.getPathelement().add(file.getAbsolutePath());
                try {
                    codebaseList.add(file.toURI().toURL());
                } catch (MalformedURLException ex) {
                    throw new IllegalStateException("file toURL returned error", ex);
                }
            }
        }


        String classpath = SystemUtil.getStringFromPath(systemClasspath);


        StringBuilder codebaseBuf = new StringBuilder();
        for (URL url : codebaseList) {
            codebaseBuf.append(url.toString());
            codebaseBuf.append(' ');
        }

        replaceParameter(jvmCommandBuffer, "${java.class.path}", classpath);
        //TODO: probably we don't need this, check this when testing running system
        replaceParameter(jvmCommandBuffer, "${codebase}", codebaseBuf.toString());
        replaceParameter(jvmCommandBuffer, "${systemname}", systemName);
        replaceParameter(jvmCommandBuffer, "${jvmname}", jvmConfig.getName());
        replaceParameter(jvmCommandBuffer, "${libPath}", System.getProperty("java.library.path") + File.pathSeparator + libPath);
        replaceParameter(jvmCommandBuffer, "${dist}", env.getDistDir().getAbsolutePath());
        replaceParameter(jvmCommandBuffer, "${localSpool}", env.getLocalSpoolDir().getAbsolutePath());
        replaceParameter(jvmCommandBuffer, "${addargs}", SystemUtil.getJvmArgs(jvmConfig));
        replaceParameter(jvmCommandBuffer, "${jvm.debug.args}", getDebugArgs(debugPort, props));
        replaceParameter(jvmCommandBuffer, "${csInfo}", env.getCSHost() + ":" + env.getCSPort());
        replaceParameter(jvmCommandBuffer, "${prefs}", prefs.toString());
        replaceParameter(jvmCommandBuffer, "${hostname}", Hostname.getLocalHost().getHostname());
        if(forced) {
            replaceParameter(jvmCommandBuffer, "${forced}", "-forced");
        } else {
            replaceParameter(jvmCommandBuffer, "${forced}", "");
        }
        
        replaceParameter(jvmCommandBuffer, "${isCS}", String.valueOf(SystemUtil.isCSJvmHostAndPort(env, Hostname.getLocalHost(), jvmConfig.getPort())));


        String jvmSecArgs = "";
        StringBuilder buf = new StringBuilder();
        if (SystemUtil.isSSLDisabled(env)) {
            String prop = ExecutionEnvFactory.getSystemPropertyName(ExecutionEnv.NO_SSL);
            buf.append("-D");
            buf.append(prop);
            buf.append("=true");
        }
        buf.append(" -Djava.security.manager=java.rmi.RMISecurityManager");
        // Issue 692: Use the == version for the system property java.security.policy
        //           It prevents that $java.home/lib/security/java.policy and
        //           $user.name/.java.policy is read
        buf.append(" -Djava.security.policy==");
        buf.append(PathUtil.getSecurityJavaPolicy(env).getAbsoluteFile());
        buf.append(" -Djava.security.auth.login.config=");
        buf.append(PathUtil.getSecurityJaasConfig(env).getAbsoluteFile());
        jvmSecArgs = buf.toString();
        replaceParameter(jvmCommandBuffer, "${jvm.security.args}", jvmSecArgs);

        log.exiting(clz, "getJvmCommand");
        return jvmCommandBuffer.toString();
    }

    /**
     * Replaces parameters contained in the string by values. The first character
     * sequence in <code>buffer</code> which is equivalent with <code>parameter</code>
     * will be replaced by <code>value</code>.
     *
     * @param buffer StringBuffer which will be modified.
     * @param parameter Sequence which should be replaced
     * @param value New character sequence
     */
    private static final void replaceParameter(StringBuilder buffer, String parameter, String value) {
        log.entering(clz, "replaceParameter");
        int startIndex = buffer.indexOf(parameter);

        while (startIndex != -1) {
            if (startIndex != -1) {
                buffer.replace(startIndex, startIndex + parameter.length(), value);
            }
            startIndex = buffer.indexOf(parameter);
        }
        log.exiting(clz, "replaceParameter");
    }
    
    private static String getDebugArgs(int debugPort, Properties props) throws GrmException {
        if (debugPort > 0) {
            StringBuilder javaDebugCommand = new StringBuilder();
            String javaVendor = System.getProperty("java.vendor");
            if (javaVendor.startsWith("Sun")) {
                javaDebugCommand.append(props.getProperty("jvm.debug.args.sun"));
            } else {
                GrmException grm = new GrmException("ParentStartupService.UnknownJVMVendor", BUNDLE_NAME);
                throw grm;
            }
            replaceParameter(javaDebugCommand, "${host}", Hostname.getLocalHost().getHostname());
            replaceParameter(javaDebugCommand, "${port_number}", String.valueOf(debugPort));
            return javaDebugCommand.toString();
        } else {
            return "";
        }
    }
    /**
     * Mapping of linux kernels 2.4, 2.6 to lx - general mapping
     * @param arch - architecture to be mapped
     * @return string representing mapped architecture
     */
    private static String getMappedArch(String arch) {
        if (arch.equals("lx26-amd64")) {
            return new String("lx-amd64");
        } else if (arch.equals("lx26-x86")) {
            return new String("lx-x86");
        } else if (arch.equals("lx24-amd64")) {
            return new String("lx-amd64");
        } else if (arch.equals("lx24-x86")) {
            return new String("lx-x86");
        } else {
            return arch;
        }
    }
    /**
     * We need to calculate required memory. The default values are read from properties file.
     */
    private static int getDefaultMemoryHeapSize(ExecutionEnv env, JvmConfig jvmConf) {
        int mem = 0;
        
        if (SystemUtil.isCSJvmHostAndPort(env, Hostname.getLocalHost(), jvmConf.getPort())) {
            mem+=convertMemory(rb.getString("cs"));
        }
        for (Component c : jvmConf.getComponent()) {
            
           if (SystemUtil.isThisHostMatching(c)) {
               if (ExecutorImpl.class.getName().equals(c.getClassname())) {
                   mem+=convertMemory(rb.getString("executor"));
               } else if (SparePoolServiceImpl.class.getName().equals(c.getClassname())) {
                   mem+=convertMemory(rb.getString("spare_pool"));
               } else if (ReporterImpl.class.getName().equals(c.getClassname())) {
                   mem+=convertMemory(rb.getString("reporter"));
               } else if (ResourceProviderImpl.class.getName().equals(c.getClassname())) {
                   mem+=convertMemory(rb.getString("resource_provider"));
               } else if (c.getClassname().equals("com.sun.grid.grm.security.ca.impl.GrmCAComponentContainer")) {
                   //our ca
                   mem+=convertMemory(rb.getString("ca"));
               } else {
                   //other services - ge_adapter.....
                   mem+=convertMemory(rb.getString("other"));
               }
           }
        }
        return mem;
    }
    
    /**
     * Converts string (which should represent memory in MB for components read from Resource Bundle
     */
    private static int convertMemory (String value) {
        //Default value if conversion is not possible
        int ret = 128;
        try {
            ret = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            //log
            log.log(Level.WARNING, "ParentStartupService.convert_memory_not_int", new Object[] {ret});
        }
        return ret;
    }
    
    /**
     * Helper object for mofifying Map with env variables
     */
    private static class Env {
        private Map<String,String> env;
        Env(Map<String,String> env) {           
            this.env = new HashMap<String, String>(env);
        }
        void addNew(String variable, String value) {
            if (!hasVariable(variable)) {
                env.put(variable, value);
            }
        }
        void addAndReplace(String variable, String value) {
            env.put(variable, value);
        }
        void remove(String variable) {
            env.remove(variable);
        }
        boolean hasVariable(String variable) {
            return env.containsKey(variable);
        }
        String[] getEnvAsArray() {
            String[] ret = new String[env.size()];
            int i = 0;
            for (String key : env.keySet()) {
                ret[i++] = key+"="+env.get(key);
            }
            return ret;
        }
    }
}
