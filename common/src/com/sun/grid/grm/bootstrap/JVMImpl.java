/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.impl.AbstractComponent;
import com.sun.grid.grm.impl.ComponentExecutors;
import com.sun.grid.grm.management.ComponentWrapper;
import com.sun.grid.grm.management.ConnectionPool;
import com.sun.grid.grm.management.Manageable;
import com.sun.grid.grm.management.SecureMBeanServer;
import com.sun.grid.grm.security.ServerSecurityContext;
import com.sun.grid.grm.security.role.RolePrincipal;
import com.sun.grid.grm.ui.ConfigurationService;
import com.sun.grid.grm.ui.component.GetJVMConfigurationCommand;
import com.sun.grid.grm.ui.component.service.RemoveActiveJVMCommand;
import com.sun.grid.grm.ui.component.service.StoreActiveJVMCommand;
import com.sun.grid.grm.ui.impl.ConfigurationServiceImpl;
import com.sun.grid.grm.ui.install.AddSTEntryCommand;
import com.sun.grid.grm.ui.install.CheckSTRegistrationCommand;
import com.sun.grid.grm.ui.install.CheckSTSupportCommand;
import com.sun.grid.grm.util.EventListenerSupport;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.GrmLogManager;
import com.sun.grid.grm.util.HostAndPort;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.InvalidStateTransistionException;
import com.sun.grid.grm.util.Platform;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.AccessController;
import java.security.Principal;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Filter;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.management.remote.JMXServiceURL;
import javax.security.auth.Subject;

/**
 * Instances of this class are the daemons of a Hedeby system.
 * 
 * The daemons are started with the main method of the this class. It is assumed
 * that system properties for a <code>ExecutionEnv</code> are set.
 *
 */
public class JVMImpl extends AbstractComponent<ExecutorService,JvmConfig> implements JVM {
    
    
    private final static String BUNDLE = "com.sun.grid.grm.bootstrap.messages";

    /** defines together with <code>CS_CONTACT_ATTEMPS</code> the timeout for waiting 
     *  for CS startup (only of CS is running in a remote JVM) 
     */
    private static final int CS_CONTACTING_INTERVAL = 1000;
    
    /** defines together with <code>CS_CONTACTING_INTERVAL</code> the timeout for waiting 
     *  for CS startup (only of CS is running in a remote JVM) 
     */
    private static final int CS_CONTACT_ATTEMPS = 60;
    
    private static Logger log = Logger.getLogger(JVMImpl.class.getName(), BUNDLE);

    /** the singleton instance of the JVM */
    private static JVMImpl theInstance;

    /** set to <code>true</code> if the jvm is hosting CS */
    private final boolean isCs;
    
    /** pid of the jvm */
    private final int pid;
    
    /** the mbean server (is created at startup */
    private AtomicReference<SecureMBeanServer> secureMBeanServer = new AtomicReference<SecureMBeanServer>();
    
    /**
     * Handler handles the shutdown of Jvm
     */
    private final ShutdownHandler shutdownHandler = new ShutdownHandler();
    
    
    private final Lock componentLock = new ReentrantLock();
    
    /** this condition is signaled of JVM is shuting down, guarded by executorLock */
    private final Condition shutdownCondition = componentLock.newCondition();

    /** contains the lifecycle objects of all components */
    private final Map<String,ComponentLifecycle> componentMap = new HashMap<String,ComponentLifecycle>();
    
    
    /** the instance of CS (is null if CS is not running inside of this JVM */
    private ConfigurationService cs;

    /**
     * Creates a new instance of JVMImpl
     * 
     * @param aPid the pid of the jvm
     * @param aEnv the execution env of the jvm
     * @param aName name of the jvm
     * @param csJvm is this jvm the CS jvm
     * @throws com.sun.grid.grm.GrmException if the command queue can not be initialized
     */
    private JVMImpl(ExecutionEnv aEnv, int aPid, String aName, boolean csJvm) throws GrmException {
        super(aEnv, aName, ComponentExecutors.<JVMImpl,JvmConfig>newCachedThreadPoolExecutorServiceFactory());
        isCs = csJvm;
        pid = aPid;
        if(isCs) {
            cs = new ConfigurationServiceImpl(env, BootstrapConstants.CS_ID, BootstrapConstants.CS_ID);
        }
    }
    
    /**
     *  Get the instanceof of the jvm.
     *
     *  @return the instance of the jvm (can be null of the instance is not initialized)
     */
    public static JVMImpl getInstance() {
        return theInstance;
    }
    
    
    
    /**
     *  Load the configuration of the JVM.
     *
     *  The first call of this method will load the configuration for CS.
     *  Futher call returns the cached configuration.
     *
     * @throws com.sun.grid.grm.GrmException if the configuration can not read
     * @return the configuration of this jvm
     */
    @Override
    protected final JvmConfig loadConfig() throws GrmException {
        JvmConfig ret = null;
        int counter = 0;
        Throwable lastError = null;
        while (counter < CS_CONTACT_ATTEMPS) {
            try {
                GetJVMConfigurationCommand cmd = new GetJVMConfigurationCommand(this.getName());
                ret = getExecutionEnv().getCommandService().<JvmConfig>execute(cmd).getReturnValue();    
                break;
            } catch (GrmRemoteException ex) {
                if(ex.isCausedBySSLProblem()) {
                    // We have a problem with the SSL setup
                    // waiting for CS makes no sense
                    // => throw a new error with a descriptive error message
                    throw new GrmRemoteException("JVMImpl.csSSLProblem", ex, BUNDLE, 
                                                 ex.getLocalizedMessage(), getName(), Hostname.getLocalHost().getHostname());
                } else {
                    lastError = ex;
                }
            } catch (GrmException ex) {
                lastError = ex;
            }
            if (counter == 0) {
                log.log(Level.WARNING, "JVMImpl.waitForCS", lastError);
            }
            try {
                Thread.sleep(CS_CONTACTING_INTERVAL);
            } catch (InterruptedException ex1) {
              //this is handled in the code below ignore this error
            }
            counter++;
        }

        if (counter == CS_CONTACT_ATTEMPS) {
            throw new GrmException("JVMImpl.exception.cs.error", lastError, BUNDLE);
        }

        if (ret == null) {
            throw new GrmException("JVMImpl.exception.jvmconfig.not.found", BUNDLE, getName());
        }
        return ret;
    }
    

    
    /**
     *  Start the jvm.
     *
     * @throws com.sun.grid.grm.GrmException if the startup failed
     */
    @Override
    protected void startComponent(JvmConfig config) throws GrmException {
        startComponents(config);
        registerServiceTaqs();
    }

    /**
     * Stops all components running inside of the JVM.
     * 
     * @param forced if <code>true</code> the all components will be stopped in forced
     *               mode. 
     * @see GrmComponent#stop(boolean) 
     * @throws com.sun.grid.grm.GrmException if the shutdown was not possible. It can be that
     *                    some components has been stopped
     */
    @Override
    protected void stopComponent(boolean forced) throws GrmException {
        shutdown();
        // Setting the state to STOPPED is not necessary since
        // after the shutdown all threads are dead
    }

    /**
     *  Reload the jvm.
     *
     * @param forced forced flag
     * @throws com.sun.grid.grm.GrmException if the jvm can not be restarted
     */
    @Override
    protected void reloadComponent(JvmConfig config, boolean forced) throws GrmException {
        shutdownComponents();
        startComponents(config);
    }
    
    /**
     *  Shutdown the JVM.
     */
    public void shutdown() {
        shutdownComponents();
        componentLock.lock();
        try {
            shutdownCondition.signalAll();
        } finally {
            componentLock.unlock();
        }
    }
    
    /**
     * Check whether Service Tags support is available and if the product was already 
     * added to the local registry. 
     */
    private void registerServiceTaqs() {
        if (isCS()) {
            String prefs = System.getProperty(BootstrapConstants.SYSTEM_PREFERENCES_PROPERTY);
            if (!PreferencesUtil.isNoST(env.getSystemName(), PreferencesType.valueOf(prefs))) {
                CheckSTSupportCommand checkST = new CheckSTSupportCommand();
                try {
                    boolean st = env.getCommandService().<Boolean>execute(checkST).getReturnValue();
                    if (st) {
                        CheckSTRegistrationCommand checkReg = new CheckSTRegistrationCommand(prefs);
                        boolean exist = env.getCommandService().<Boolean>execute(checkReg).getReturnValue();
                        if (!exist) {
                            AddSTEntryCommand addST = new AddSTEntryCommand(prefs);
                            env.getCommandService().<Void>execute(addST);
                        }
                    }
                } catch (GrmException ex) {
                    log.log(Level.FINE, "JVMImpl.ex.regST", ex);
                }
            }
        }
    }
    
    private void waitForShutdown() throws InterruptedException {
        componentLock.lock();
        try {
            shutdownCondition.await();  
        } finally {
            componentLock.unlock();
        }
    }
    
    /**
     *   Add a new component in the JVM.
     *
     *   If the autostart flag in the component definition is set
     *   the component is also started.
     *
     *   @param  comp  definition of the component
     */
    public void addComponent(Component comp) throws GrmException {
        ComponentLifecycle cl = null;
        componentLock.lock();
        try {
            if(componentMap.containsKey(comp.getName())) {
                throw new GrmException("JVMImpl.ex.compExists", BUNDLE, comp.getName());
            }
            cl = new DefaultComponentLifecycle(comp);
            componentMap.put(cl.getComponentName(), cl);
        } finally {
            componentLock.unlock();
        }
        cl.startup();
        try {
            try {
                if(cl.waitForComponentStartup() != ComponentState.STARTED) {
                    if(cl.getLastError() != null) {
                        throw cl.getLastError();
                    } else {
                        // This only happen if a java.lang.Error is thrown in 
                        // the execute method of ComponentLifeCycle
                        throw new GrmException("JVMImpl.ex.startupUnknown", BUNDLE, cl.getComponentName());
                    }
                }
            } catch(InterruptedException ex) {
                throw new GrmException("JVMImpl.ex.startupInt", BUNDLE, cl.getComponentName());
            }
        } catch(GrmException ex) {
            // If the startup did not work we remove the component
            // from the component map
            componentLock.lock();
            try {
                componentMap.remove(comp.getName());
            } finally {
                componentLock.unlock();
            }
            // Be sure that component is shutdown
            cl.shutdown();
            throw ex;
        }
    }

    /**
     *  Remove a component from the JVM.
     *
     *  @param name the name of the component
     */
    public boolean removeComponent(String name) {
        componentLock.lock();
        try {
            ComponentLifecycle cl = componentMap.remove(name);
            if(cl == null) {
                return false;
            }
            cl.remove();
            log.log(Level.INFO,"JVMImpl.ex.compRm", name);
            return true;
        } finally {
            componentLock.unlock();
        }
    }
    
    
    /**
     * Start all components from a JvmConfiguration
     *
     * @param config  the jvm configuration
     */
    private void startComponents(JvmConfig config) {
        componentLock.lock();        
        try {
            componentMap.clear();
            JvmLifecycle jvmComponentHandler = new JvmLifecycle();
            
            componentMap.put(jvmComponentHandler.getComponentName(), jvmComponentHandler);
            
            List<ComponentLifecycle> dependencies = Collections.<ComponentLifecycle>singletonList(jvmComponentHandler);
            
            ComponentLifecycle shutdownDependency = jvmComponentHandler;
            
            if(isCS()) {
                CSLifecycle cl = new CSLifecycle(dependencies);
                componentMap.put(cl.getComponentName(), cl);
                dependencies = Collections.<ComponentLifecycle>singletonList(cl);
                jvmComponentHandler.addShutdownDependency(cl);
                // CS should wait for the shutdown for all other components
                shutdownDependency = cl;
            }
            
            for(Component comp: config.getComponent()) {
                if(SystemUtil.isThisHostMatching(comp)) {
                    if(componentMap.containsKey(comp.getName())) {
                        log.log(Level.WARNING, "JVMImpl.dupCmp", comp.getName());
                    } else {
                        DefaultComponentLifecycle cl = new DefaultComponentLifecycle(comp, dependencies);
                        if(log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "JVMImpl.unkCmpL", comp.getName());
                        }
                        componentMap.put(cl.getComponentName(), cl);
                        shutdownDependency.addShutdownDependency(cl);
                    }
                } else {
                    if(log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "JVMImpl.unkCmpNL", comp.getName());
                    }
                }
            }
            
            for(Map.Entry<String,ComponentLifecycle> entry: componentMap.entrySet()) {
                entry.getValue().startup();
            }

        } finally {
            componentLock.unlock();
        }
    }
    
    /**
     *  Shutdown all components.
     */
    private void shutdownComponents() {
        componentLock.lock();
        try {
            for(Map.Entry<String,ComponentLifecycle> entry: componentMap.entrySet()) {
                entry.getValue().shutdown();
            }
        } finally {
            componentLock.unlock();
        }
    }
    
    private void waitUntilAllComponentsShutdown() throws InterruptedException {
        componentLock.lock();
        try {
            for(Map.Entry<String,ComponentLifecycle> entry: componentMap.entrySet()) {
                entry.getValue().waitForComponentShutdown();
            }
        } finally {
            componentLock.unlock();
        }
    }
    
    
    /**
     * Checks whether the JVM is hosting the command service.
     * @return true if the JVM is hosting the command service
     */
    public boolean isCS() {
        return isCs;
    }
    
    /**
     *   Get CS.
     *
     *   @return cs of <code>null</code> if this jvm is not the CS jvm
     */
    public ConfigurationService getCs() {
        return cs;
    }
    
    /**
     * Gets the JVM MBean server URL.
     * 
     * @return JMXServiceUrl of the MBean Server or null if the MBean Server 
     *         has not been started
     */
    public JMXServiceURL getJMXServiceUrl() {
        SecureMBeanServer srv = secureMBeanServer.get();
        if(srv == null) {
            return null;
        } else {
            return srv.getJMXServiceUrl();
        }
    }

    private static void usage() {
        System.err.println("JVMImpl [-forced]");
        System.exit(1);
    }
    
    /**
     * The main method.
     *
     * @param args Commandline arguments
     */
    public static void main(String[] args) {

        Queue<String> queue = new LinkedList<String>(Arrays.asList(args));
        
        PrivilegedStartAction startupAction = new PrivilegedStartAction();
        
        while(!queue.isEmpty()) {
            String arg = queue.poll();
            if(arg.equals("-forced")) {
                startupAction.setForcedStartup(true);
            } else {
                System.err.println(I18NManager.formatMessage("JVMImpl.unknownOption", BUNDLE, arg));
                usage();
                return;
            }
        }

        RolePrincipal rolePrincipal = new RolePrincipal("daemon");
        Set<Principal> principals = Collections.<Principal>singleton(rolePrincipal);
        Subject subject = new Subject(true, principals, Collections.emptySet(), Collections.emptySet());

        try {
            try {
                Subject.doAsPrivileged(subject, startupAction, null);
                System.exit(0);
            } catch(PrivilegedActionException ex) {
                throw ex.getException();
            }
            
        } catch (Throwable ex) { 
            if(log.isLoggable(Level.SEVERE)) {
                LogRecord lr = new LogRecord(Level.SEVERE, I18NManager.formatMessage("JVMImpl.startupError",BUNDLE, ex.getLocalizedMessage()));
                lr.setThrown(ex);
                log.log(lr);
            }
            System.exit(1);
        }
    }
   
    /**
     *  This class manages the lifecylce of a component.
     *
     *  <p>The lifecycle of a component run in it's own thread in the executor
     *  if this thread is interrupted the component is shutdown. This thread
     *  is executed under the subject of the JVM daemon.</p>
     *
     *  <p>It is possible to define startup/shutdown dependencies for a component.
     *     The component will not start the lifecylce of all startup dependencies
     *     has started the components and vice versa</p>
     */
    private abstract class  ComponentLifecycle implements Runnable {
        
        /** state of the component from the view of the lifecycle 
            (threadsafe, guarded by statelock)*/
        private ComponentState state = ComponentState.STARTING;
        
        /** guards state */
        private final Lock stateLock = new ReentrantLock();
        
        /** this condition is signaled if the state changes */
        private final Condition stateChangedCondition = stateLock.newCondition();
        
        /** name of the component */
        private final String componentName;
        
        /** the instanceof of the component */
        protected GrmComponent component;
        
        /** subject of the daemon */
        private final Subject subject;
        
        /** list of startup dependencies (set in the constructors)*/
        private final List<ComponentLifecycle> startupDependencies;
        
        /** list of shutdown dependencies */
        private final List<ComponentLifecycle> shutdownDependencies = new LinkedList<ComponentLifecycle>();
        
        private GrmException lastError;
        
        /** mandatory flag. If this flag is set to <code>true</code> and the component can not be instaniated
         *  and registered
         *  the JVM will shutdown */
        private boolean mandatory;
        
        
        private Future future;

        private boolean removeDirectoriesOnShutdown;
        
        
        /**
         *  Create a ComponentLifecycle which has no startup dependencies.
         *
         *  @param aComponentName name of the component
         */
        protected ComponentLifecycle(String aComponentName) {
            this(aComponentName, Collections.<ComponentLifecycle>emptyList());
        }
        
        /**
         *  Create a ComponentLifecycle with startup dependencies.
         *
         *  @param aComponentName name of the component
         *  @param aDependencies  the startup dependencies
         */
        protected ComponentLifecycle(final String aComponentName, List<ComponentLifecycle> aDependencies) {
            if(aComponentName == null) {
                throw new NullPointerException();
            }
            componentName = aComponentName;
            subject = Subject.getSubject(AccessController.getContext());
            startupDependencies = aDependencies;
        }
        
        /**
         * Get the name of the component.
         *
         * @return  the name of the component
         */
        public String getComponentName() {
            return componentName;
        }
        
        /**
         *  Add a shutdown dependency.
         *
         *  @param lifecycle the shutdown dependency
         */
        public void addShutdownDependency(ComponentLifecycle lifecycle) {
            shutdownDependencies.add(lifecycle);
        }
        
        /**
         *  This method creates the instance of the component
         * 
         * @return the instance of the component
         * @throws GrmException if the instance of the component can not be created
         */
        protected abstract GrmComponent createInstance() throws GrmException;

        
        /**
         * Startup the component.
         *
         * This method is called after the component has been created.
         *
         * By default nothing is done at component startup
         *
         * @throws GrmException if the startup failed
         */
        protected void componentStartup() throws GrmException {
            // The default implementation does not need any component start
        }

        protected void waitForShutdown() throws InterruptedException, GrmException {
            Thread.sleep(Integer.MAX_VALUE);
        }
        
        /**
         * Shutdown the component.
         * @throws GrmException if the shtudown failed
         */
        protected void componentShutdown() throws GrmException {
            // The default implementation does not need any component shutdown
        }
        
        /**
         * Publishes the component info of the component at CS
         * @param componentInfo   the component info
         * @throws GrmException   publishing failed
         */
        protected void publishComponentInfo(ComponentInfo componentInfo) throws GrmException {
            log.log(Level.FINE, "JVMImpl.pubCi", getComponentName());
            ComponentService.storeComponentInfo(getExecutionEnv(), componentInfo);
        }
        
        /**
         * Remote the component info from CS
         * @param componentInfo the component info
         * @throws GrmException the component info can not be removed
         */
        protected void removeComponentInfo(ComponentInfo componentInfo) throws GrmException {
            if(log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "JVMImpl.removeCi", getComponentName());
            }
            ComponentService.removeComponentInfo(getExecutionEnv(), componentInfo.getIdentifier());
        }
        
        /**
         *  Executes the component lifecylce in a <code>PrivilegedAction</code>.
         */
        public void run() {
            PrivilegedExceptionAction<Void> action = new PrivilegedExceptionAction<Void>() {
                public Void run() throws Exception {
                    execute();
                    return null;
                }
            };
            
            try {
                Subject.doAsPrivileged(subject, action, null);
            } catch(PrivilegedActionException ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("JVMImpl.ex.lcp", BUNDLE, getComponentName(), ex.getException().getLocalizedMessage()),
                        ex);
            } catch(Exception ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("JVMImpl.ex.lc", BUNDLE, getComponentName(), ex.getLocalizedMessage()),
                        ex);
            }
        }
        
        /**
         * Set the state of the component lifecylce.
         *
         * @param newState the new state
         */
        private void setState(ComponentState newState) {
            stateLock.lock();
            try {
                if(state != newState) {
                    state = newState;
                    stateChangedCondition.signalAll();
                }
            } finally {
                stateLock.unlock();
            }
        }
        
        /**
         *   Startup this component
         */
        public void startup() {
            stateLock.lock();
            try {
                if (future != null) {
                    log.log(Level.WARNING, "JVMImpl.comp.isRunning", getComponentName());
                } else {
                    future = getExecutorService().submit(this);
                }
            } finally {
                stateLock.unlock();
            }
        }

        /**
         * Shutdown the component and cleanup the spool directory
         */
        public void remove() {
            stateLock.lock();
            try {
                if (future == null) {
                    // Component is not running
                    // => cleanup the directories directly
                    cleanupDirectories();
                } else {
                    removeDirectoriesOnShutdown = true;
                    shutdown();
                }
            } finally {
                stateLock.unlock();
            }
        }
        
        /**
         *  Shutdown the component
         */
        public void shutdown() {
            stateLock.lock();
            try {
                if(future != null) {
                    future.cancel(true);
                    future = null;
                } else {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "JVMImpl.comp.isNotRunning", getComponentName());
                    }
                }
            } finally {
                stateLock.unlock();
            }
        }
        
        
        /**
         * Wait until the component is started or the startup of the component
         * is failed
         * @throws java.lang.InterruptedException if the thread has been interrupted
         * @return the last this of the component
         */
        public ComponentState waitForComponentStartup() throws InterruptedException {
            stateLock.lock();
            try {
                while(state == ComponentState.STARTING) {
                    stateChangedCondition.await();
                }
                return state;
            } finally {
                stateLock.unlock();
            }
        }
        
        /**
         * Wait until all startup dependencies are fullfilled.
         *
         * @throws com.sun.grid.grm.GrmException if the startup dependencies are not fullfilled
         */
        private void waitForStartupDependencies() throws GrmException {
            
            for(ComponentLifecycle ch: startupDependencies) {
                try {
                    ComponentState chState = ch.waitForComponentStartup();
                    if(chState != ComponentState.STARTED) {
                        throw new GrmException("JVMImpl.ex.startupDep", BUNDLE,
                                               getComponentName(), ch.getComponentName());
                    }
                } catch(InterruptedException ex) {
                    throw new GrmException("JVMImpl.ex.waitStartupInt", BUNDLE, getComponentName());
                }
            }
        }
        

        /**
         * Wait until the component is shutdown.
         *
         * @throws java.lang.InterruptedException if the thread has been interupted while
         *                  waiting for shutdown
         */
        public void waitForComponentShutdown() throws InterruptedException {
            stateLock.lock();
            try {
                while(state != ComponentState.STOPPED) {
                    stateChangedCondition.await();
                }
            } finally {
                stateLock.unlock();
            }
        }
        
        /**
         * Wait for the shutdown dependencies
         */
        private void waitForShutdownDependencies() {
            for(ComponentLifecycle ch: shutdownDependencies) {
                try {
                    if(log.isLoggable(Level.FINER)) {
                        log.log(Level.FINER, "JVMImpl.waitShutdown", new Object [] { getComponentName(), ch.getComponentName() });
                    }
                    ch.waitForComponentShutdown();
                } catch(InterruptedException ex) {
                    if(log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "JVMImpl.waitShutdownInt", new Object [] { getComponentName(), ch.getComponentName() });
                    }
                    return;
                }
            }
        }
        
        /**
         * Create the the component and search for it's management interface
         *
         * @throws com.sun.grid.grm.GrmException if the component can not be created or if the component
         *             implements no management interface
         * @return the management interface of the component
         */
        protected Class<?> createComponent() throws GrmException {
            component = createInstance();
            
            Class<?> ret = getInterface(component.getClass());
            
            if(ret == null) {
                throw new GrmException("JVMImpl.noManInf", BUNDLE,
                    getComponentName(), component.getClass().getName());
            }
            return ret;
        }
        
        
        /**
         * Register the mbean for the component.
         * 
         * @param managedInterface the management interface of the component
         * @throws com.sun.grid.grm.GrmException if the mbean can not be registered
         */
        protected MBeanInfo registerMBean(Class<?> managedInterface) throws GrmException {
            log.log(Level.FINER,"JVMImpl.regMBean", getComponentName());
            
            ComponentInfo ci = ComponentInfo.newInstance(getExecutionEnv(), Hostname.getLocalHost(), JVMImpl.this.getName(),
                                                         getComponentName(), managedInterface);
            
            ComponentWrapper<GrmComponent> managedGrmComponent = new ComponentWrapper<GrmComponent>(component, managedInterface, ci.getObjectName());
            
            try {
                secureMBeanServer.get().getMBeanServer().registerMBean(managedGrmComponent, managedGrmComponent.getObjectName());
                return new MBeanInfo(ci, managedGrmComponent);
            } catch (Exception ex) {
                throw new GrmException("JVMImpl.ex.regMBean", ex, BUNDLE,
                                       getComponentName(), ex.getLocalizedMessage());
            }
        }
        
        /**
         * Unregister the mbean from the MBean server.
         *
         * @param componentInfo the component info of the component
         */
        protected void unregisterMBean(MBeanInfo mbeanInfo) {
            if(log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "JVMImpl.unregMBean", getComponentName());
            }
            if(mbeanInfo != null) {
                try {
                    secureMBeanServer.get().getMBeanServer().unregisterMBean(mbeanInfo.getComponentWrapper().getObjectName());
                } catch(Exception ex) {
                    if(log.isLoggable(Level.WARNING)) {
                        LogRecord lr = new LogRecord(Level.WARNING, I18NManager.formatMessage("JVMImpl.ex.unregMBean",BUNDLE, getComponentName(), ex.getLocalizedMessage()));
                        lr.setThrown(ex);
                        log.log(lr);
                    }
                }
                // Reset all event listener to give GC a chances to free memory
                mbeanInfo.getComponentWrapper().resetEventListeners();
            }
        }
        
        /**
         *  Handles the lifecycle of a component
         */
        private void execute() throws GrmException {
            
            try {
                lastError = null;
                waitForStartupDependencies();
                
                log.log(Level.FINER, "JVMImpl.createComp", getComponentName());
                Class<?> managedInterface = createComponent();
                
                log.log(Level.FINER, "JVMImpl.startComp", getComponentName());
                componentStartup();
                
                try {
                    MBeanInfo mBeanInfo = registerMBean(managedInterface);
                    try {
                        publishComponentInfo(mBeanInfo.getCi());
                        try {
                            setState(ComponentState.STARTED);
                            log.log(Level.FINE, "JVMImpl.compStarted", getComponentName());

                            while(!Thread.currentThread().isInterrupted()) {
                                waitForShutdown();
                            }
                            
                        } catch(InterruptedException ex) {
                            log.log(Level.FINER, "JVMImpl.compInt", getComponentName());
                        } finally {
                            try {
                                setState(ComponentState.STOPPING);
                                
                                waitForShutdownDependencies();
                                
                                removeComponentInfo(mBeanInfo.getCi());
                            } catch(Exception ex) {
                                LogRecord lr = new LogRecord(Level.WARNING, I18NManager.formatMessage("JVMImpl.ex.rmCi",BUNDLE, getComponentName(), ex.getLocalizedMessage()));
                                lr.setThrown(ex);
                                log.log(lr);
                            }
                        }
                    } finally {
                        setState(ComponentState.STOPPING);
                        unregisterMBean(mBeanInfo);
                    }
                } finally {
                    setState(ComponentState.STOPPING);
                    try {
                        log.log(Level.FINER, "JVMImpl.shutdownComp", getComponentName());
                        componentShutdown();
                    } catch(Exception ex) {
                        LogRecord lr = new LogRecord(Level.WARNING, I18NManager.formatMessage("JVMImpl.shutdownComp",BUNDLE, getComponentName(), ex.getLocalizedMessage()));
                        lr.setThrown(ex);
                        log.log(lr);
                    }
                }
            } catch(Exception ex) {
                if(isMandatory()) {
                    // The startup of a mandatory component failed
                    // => shutdown the jvm
                    JVMImpl.this.shutdown();
                }
                GrmException tmpEx = new GrmException("JVMImpl.ex.startComp", ex, BUNDLE, getComponentName(), ex.getLocalizedMessage());
                lastError = tmpEx;
                throw tmpEx;
            } finally {
                boolean cleanup = false;
                stateLock.lock();
                try {
                    future = null;
                    cleanup = removeDirectoriesOnShutdown;
                    removeDirectoriesOnShutdown = false;
                } finally {
                    stateLock.unlock();
                }
                log.log(Level.FINE, "JVMImpl.compStopped", getComponentName());
                setState(ComponentState.STOPPED);
                if (cleanup) {
                    cleanupDirectories();
                }
            }
        }

        private void cleanupDirectories() {
            try {
                log.log(Level.INFO, "JVMImpl.comp.cleanup.spool", getComponentName());
                Platform.getPlatform().removeDir(PathUtil.getSpoolDirForComponent(getExecutionEnv(), getComponentName()), true);
            } catch (Exception ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("JVMImpl.comp.cleanup.spool.failed", BUNDLE, getComponentName(), ex.getLocalizedMessage()), ex);
            }
            try {
                log.log(Level.INFO, "JVMImpl.comp.cleanup.tmp", getComponentName());
                Platform.getPlatform().removeDir(PathUtil.getTmpDirForComponent(getExecutionEnv(), getComponentName()), true);
            } catch (Exception ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("JVMImpl.comp.cleanup.tmp.failed", BUNDLE, getComponentName(), ex.getLocalizedMessage()), ex);
            }
        }

        /**
         *  get the mandatory flag of the component.
         *
         *  @return <code>true</code> if this component is mandatory
         */
        public boolean isMandatory() {
            return mandatory;
        }

        /**
         *  Set the mandatory flag of the component
         *
         *  @param isMandatory the mandatory flag
         */
        public void setMandatory(boolean isMandatory) {
            this.mandatory = isMandatory;
        }

        /**
         *  Get the last error if this component;
         *
         *  @return the last error
         */
        public GrmException getLastError() {
            return lastError;
        }
        
        protected class MBeanInfo {
            
            private final ComponentInfo ci;
            private final ComponentWrapper<GrmComponent> componentWrapper;
            
            public MBeanInfo(ComponentInfo ci, ComponentWrapper<GrmComponent> componentWrapper) {
                this.ci = ci;
                this.componentWrapper = componentWrapper;
            }

            public ComponentInfo getCi() {
                return ci;
            }

            public ComponentWrapper<GrmComponent> getComponentWrapper() {
                return componentWrapper;
            }
                    
        }
        
    }
    
    /**
     *  This class handles the lifecycle of a JVM.
     *
     *  At componentStartup the mbean service is setup and the pid file is written.
     *
     *  A JVM is a mandatory component.
     */
    private class JvmLifecycle extends ComponentLifecycle {

        private final FileDescriptorObserver fdo = new FileDescriptorObserver();
        private long maxOpenFD;


        /**
         *  Create a new instance of <code>JvmLifecycle</code>.
         */ 
        public JvmLifecycle() {
            super(JVMImpl.this.getName());
            setMandatory(true);
        }
        
        /**
         *  Create the instance of the jvm
         *
         *  @return the instanc of the jvm
         */
        protected GrmComponent createInstance() {
            return JVMImpl.this;
        }

        /**
         * Creates the mbean server and writes the pid file
         * @throws com.sun.grid.grm.GrmException  if the mbean service can not be created
         *            of if the pid file can not be written
         */
        @Override
        protected void componentStartup() throws GrmException {
            
            // Shutdown handler
            Runtime.getRuntime().addShutdownHook(shutdownHandler);
            
            JvmConfig config = getConfig();
            
            ServerSecurityContext ctx = (ServerSecurityContext) getExecutionEnv().getSecurityContext();
            
            try {
                secureMBeanServer.set(new SecureMBeanServer(getExecutionEnv().getSystemName(), getName(), config.getPort(), ctx));
            } catch(IOException ex) {
                throw new GrmException("JVMImpl.ex.mbeanServer", ex, BUNDLE, config.getPort(), ex.getLocalizedMessage());
            }
            
            File pidFile = PathUtil.getPidFile(getExecutionEnv(), getName());
            File runDir = pidFile.getParentFile();
            if (!runDir.exists()) {
                throw new GrmException("JVMImpl.noRunDir", BUNDLE, runDir);
            }
            
            shutdownHandler.deleteOnExit(pidFile);
            
            int port = secureMBeanServer.get().getRegistryPort();
            
            try {
                PrintWriter pw = new PrintWriter(new FileWriter(pidFile));
                try {
                    pw.println(pid);
                    pw.println(port);
                } finally {
                    pw.close();
                }
                if (log.isLoggable(Level.FINE)) {
                   log.log(Level.FINE, "JVMImpl.pidFile", new Object[]{pid, port, pidFile});
                }
            } catch(IOException ex) {
                SecureMBeanServer sr = secureMBeanServer.getAndSet(null);
                sr.close();
                throw new GrmException("JVMImpl.ex.pidIO", ex, BUNDLE, ex.getLocalizedMessage());
            }
            
            try {
                ActiveJvm aj = new ActiveJvm();
                aj.setName(getName());
                aj.setHost(Hostname.getLocalHost().getHostname());
                aj.setPort(secureMBeanServer.get().getRegistryPort());
                StoreActiveJVMCommand saj = new StoreActiveJVMCommand(aj);
                getExecutionEnv().getCommandService().execute(saj);
            } catch(GrmException ex) {
                SecureMBeanServer sr = secureMBeanServer.getAndSet(null);
                sr.close();
                throw ex;
            }

            log.log(Level.INFO, "max file descriptor limit is set to " + fdo.getMaxFileDescriptorCount());
        }

        @Override
        protected void waitForShutdown() throws InterruptedException, GrmException {
            Thread.sleep(5000);
            fdo.update();
            double r = fdo.getOpenFileDescriptorRatio();
            int percent = (int) (r * 10);

            if (r > 0.9) {
                throw new GrmException("JVMImpl.openFD.ex", BUNDLE, percent, fdo.getOpenFileDescriptorCount(), fdo.getMaxFileDescriptorCount());
            } else if (r > 0.8) {
                log.log(Level.WARNING, "JVMImpl.openFD.warn",
                        new Object[]{percent, fdo.getOpenFileDescriptorCount(), fdo.getMaxFileDescriptorCount()});
            } else if (maxOpenFD < fdo.getOpenFileDescriptorCount()) {
                maxOpenFD = fdo.getOpenFileDescriptorCount();
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "JVMImpl.openFD", new Object[]{fdo.getOpenFileDescriptorCount(), fdo.getMaxFileDescriptorCount(), percent});
                }
            }
        }

        /**
         * unregister the jvm from the mbean server
         *
         * This method stops the mbean server. unregistering the mbean is not necessary
         */
        @Override
        protected void unregisterMBean(MBeanInfo mBeanInfo) {
            log.log(Level.FINE, "JVMImpl.stopMbeanServer");
            super.unregisterMBean(mBeanInfo);
            try {
                RemoveActiveJVMCommand rjc = new RemoveActiveJVMCommand(new JvmInfo(getName(), Hostname.getLocalHost()));
                getExecutionEnv().getCommandService().execute(rjc);
            } catch(GrmException ex) {
                log.log(Level.WARNING, ex.getLocalizedMessage());
            }
            secureMBeanServer.get().close();
        }
    }
    
    /**
     *  This <code>ComponentLifecycle</code> manages the lifecycle of 
     *  the CS service.
     *
     *  CS is a mandatory component
     */
    private class CSLifecycle extends ComponentLifecycle {
        
        /**
         * Creata a new <code>CSLifecycle</code>
         * @param dependencies startup dependencies
         */
        public CSLifecycle(List<ComponentLifecycle> dependencies) {
            super(BootstrapConstants.CS_ID, dependencies);
            setMandatory(true);
        }
        
        /**
         *  get the CS component.
         *
         *  @return the cs component
         */
        protected GrmComponent createInstance() throws GrmException {
            return JVMImpl.this.getCs();
        }

        /**
         * We have to override this method since CS does not store a active component
         * info, the contact information is part of the bootrap information.
         *
         * @param componentInfo the component info
         */
        @Override
        protected void publishComponentInfo(ComponentInfo componentInfo) {
            // CS does not store a active component info, the contact information is part
            // of the bootstrap information
        }
        
        /**
         * We have to override this method since CS does not store a active component
         * info, the contact information is part of the bootrap information.
         *
         * @param componentInfo the component info
         */
        @Override
        protected void removeComponentInfo(ComponentInfo componentInfo) {
            // CS does not store a active component info, the contact information is part
            // of the bootstrap information
        }
    }
    
    /**
     *  This <code>ComponentLifecycle</code> manages the lifecycle of normal component.
     */
    private class DefaultComponentLifecycle extends ComponentLifecycle {
        
        private final Component componentConfig;
        
        /**
         * Create a new instanceof of <code>DefaultComponentLifecycle</code>
         * @param aComponentConfig  definition of the component from the global configration
         * @param dependencies startup dependencies
         */
        public DefaultComponentLifecycle(Component aComponentConfig, List<ComponentLifecycle> aDependencies) {
            super(aComponentConfig.getName(), aDependencies);
            componentConfig = aComponentConfig;
        }
        
        /**
         *  Create a new instanceof of <code>DefaultComponentLifecycle</code>
         *  This instance has not startup dependencies
         *
         * @param aComponentConfig  definition of the component from the global configration
         */
        public DefaultComponentLifecycle(Component aComponentConfig) {
            this(aComponentConfig, Collections.<ComponentLifecycle>emptyList());
        }
        
        /**
         *  This method instantiates the component out of the component definition
         * 
         * @return the component
         * @throws com.sun.grid.grm.GrmException component could not be instantiated

         */
        protected GrmComponent createInstance() throws GrmException {
            
            Class<?> clazz = null;
            Constructor ct = null;
            
            String classname = componentConfig.getClassname();
            
            try {
                clazz = Class.forName(classname);
            } catch (ClassNotFoundException ex) {
                throw new GrmException("JVMImpl.no_class_for_component", ex, BUNDLE, classname, getComponentName());
            } catch (NullPointerException ex) {
                throw new GrmException("JVMImpl.no_class_for_component", ex, BUNDLE, classname, getComponentName());
            }
            try {
                ct = clazz.getConstructor(new Class[]{ExecutionEnv.class, String.class});
            } catch (NoSuchMethodException ex) {
                throw new GrmException("JVMImpl.ex.compConstr", BUNDLE, classname);
            }
            
            //create object using constructor that was found
            try {
                return (GrmComponent)ct.newInstance(getExecutionEnv(), componentConfig.getName());
            } catch (InvocationTargetException ex) {
                throw new GrmException("JVMImpl.ex.unable_to_create", ex.getTargetException(), BUNDLE, classname);
            } catch (Exception ex) {
                throw new GrmException("JVMImpl.ex.unable_to_create", ex, BUNDLE, classname);
            }
        }

        /**
         * Startup the component.
         *
         * Calls the <code>start</code> of the component if the autostart flag
         * is set.
         *
         * The default componentStartup method does not throw an exception if the
         * startup fails. The component stays in stopped state and a log
         * message is writen.
         */
        @Override
        protected void componentStartup() {
            if(componentConfig.isAutostart()) {
                log.log(Level.FINE, "JVMImpl.compStarting", getComponentName());
                try {
                    component.start();
                } catch(Exception ex) {
                    LogRecord lr = new LogRecord(Level.WARNING,"JVMImpl.ex.autostart");
                    lr.setThrown(ex);
                    lr.setParameters(new Object [] {getComponentName(), ex.getLocalizedMessage()});
                    lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                    log.log(lr);
                }
            } else {
                log.log(Level.FINE, "JVMImpl.compSkip", getComponentName());
            }
        }

        
        /**
         * Calls the <code>stop</code> method of the component with the forced
         * flag. The component should shutdown as fast a possible
         *
         * @throws com.sun.grid.grm.GrmException throws by the stop method
         * @see com.sun.grid.grm.GrmComponent#stop
         */
        @Override
        protected void componentShutdown()  throws GrmException {
            try {
                component.stop(true);
            } catch(InvalidStateTransistionException ex) {
                // If the component is already stopped we must not propagate
                // the error
                if (!ex.getOrgState().equals(ComponentState.STOPPED)) {
                    throw ex;                }
            }
        }
        
    }
    
    
    /**
     * Returns the class of the first interface encountered which has the
     * Manageable annotation and is not the GrmComponent interface.  Because each
     * component should only implement two Manageable interfaces, GrmServer, and
     * the component's management interface, this method will effectively return
     * the component's management interface.
     * @param clazz desired interface class
     * @return the first Manageable interface encountered
     */
    private static Class getInterface(Class<?> clazz) {
        for(Class<?> aClass = clazz; aClass != null; aClass = aClass.getSuperclass()) {
            Class<?>[] interfaces = aClass.getInterfaces();

            if (interfaces != null) {
                for (Class<?> intf : interfaces) {
                    if ((intf.getAnnotation(Manageable.class) != null) && !intf.equals(GrmComponent.class)) {
                        return intf;
                    }
                }
            }
        }
        return null;
    }
    
    
    
    /**
     * Initialize the logging on this jvm
     */
    private static void initLogging(String jvmName) {
        try {
            File spoolDir = ExecutionEnvFactory.getLocalSpoolDirFromSystemProperties();
            LogManager logManager = LogManager.getLogManager();
            
            // Read the configuration
            File logConf = new File(spoolDir, "logging.properties");
            File logDir = PathUtil.getLogPath(spoolDir);
            Map<String,String> patterns = new HashMap<String,String>(2);
            patterns.put("${log_dir}", logDir.getAbsolutePath());
            patterns.put("${jvm_name}", jvmName);
            String logConfig  = FileUtil.readAndReplace(logConf, patterns);
            
            logManager.readConfiguration(new ByteArrayInputStream(logConfig.getBytes()));

            // Logging is setup correctly
            // Now we can redirect stdout and stderr
            File stdout = new File(logDir, jvmName + ".stdout");
            File stderr = new File(logDir, jvmName + ".stderr");

            FileOutputStream fo = new FileOutputStream(stdout);
            System.setOut(new PrintStream(fo, true));

            fo = new FileOutputStream(stderr);
            System.setErr(new PrintStream(fo, true));
            
            if(logManager instanceof GrmLogManager) {
                
                ((GrmLogManager)logManager).setAllowReset(false);
            } else {
                throw new IllegalStateException(I18NManager.formatMessage("JVMImpl.ex.logManager", BUNDLE));
            }
        } catch (Exception e) {
            System.err.println(I18NManager.formatMessage("JVMImpl.ex.logging", BUNDLE, e.getLocalizedMessage()));
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }


    
    
    /**
     *  This action encapsulates the code with has to run under the subject
     *  of a daemon.
     */
    private static class PrivilegedStartAction implements PrivilegedExceptionAction<Void> {
        
        private boolean forcedStartup;
        
        public Void run() throws Exception {

            String jvmName = System.getProperty(BootstrapConstants.JVM_NAME_PROPERTY);
            
            if (jvmName == null) {
                throw new SystemPropertyNotAvailableException(BootstrapConstants.JVM_NAME_PROPERTY);
            }
            
            initLogging(jvmName);
            
            int pid = Platform.getPlatform().getPid();
            
            log.log(Level.INFO, "JVMImpl.startup", pid);
            
            
            ExecutionEnv env = ExecutionEnvFactory.getInstanceFromSystemProperties();
            
            theInstance = new JVMImpl(env, pid, jvmName, SystemUtil.isThisCSJvm());
            theInstance.start();
            theInstance.waitForShutdown();
            
            return null;
        }

        /**
         *  get the forced flag
         *
         *  @return <code>true</code> if the jvm should be started in forced mode
         *          (existing pid file will be ignored)
         */
        public boolean isForcedStartup() {
            return forcedStartup;
        }

        /**
         *  set the forced flag.
         *
         *  @param forcedStartup the forced flag
         */
        public void setForcedStartup(boolean forced) {
            this.forcedStartup = forced;
        }
    }
    
    /**
     *  Calls the <code>shutdown</code> method of the JVM.
     *
     *  We do not trust the <code>File.deleteOnExit</code> method
     *  So we need a shutdownhandler which deletes run files
     *  and active components entries on exit.
     */
    private class ShutdownHandler extends Thread {

        private final List<File> files = new LinkedList<File>();

        @Override
        public void run() {
            try {
                if(log.isLoggable(Level.INFO)) {
                    log.log(Level.INFO, "JVMImpl.shutdown");
                }
                
                JVMImpl.this.shutdownComponents();
                
                JVMImpl.this.waitUntilAllComponentsShutdown();
                 
                // Stop all connections to remote jvms
                ConnectionPool.getInstance(env).closeAllConnections();

                // Stop the asynchron event delivery
                EventListenerSupport.shutdown();
                
                for (File file : files) {
                    if(log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "JVMImpl.delTmpFile", file);
                    }
                    if(!file.delete()) {
                        log.log(Level.WARNING, "JVMImpl.delTmpFileFailed", file);
                    }
                }
            } catch(Exception ex) {
                if(log.isLoggable(Level.SEVERE)) {
                    log.log(Level.SEVERE, "JVMImpl.shutdownError", ex);
                }
            } finally {
                if(log.isLoggable(Level.INFO)) {
                    log.log(Level.INFO, "JVMImpl.shutdownComplete");
                }
                // Beyond this point no logging is possible
                LogManager logManager = LogManager.getLogManager();
                if(logManager instanceof GrmLogManager) {
                    ((GrmLogManager)logManager).setAllowReset(true);
                    logManager.reset();
                }
            }
        }

        /**
         * Register a file which will be deleted when the <code>Shutdown</code>
         * handler is running.
         * 
         * @param file  the file which will be deleted
         */
        public void deleteOnExit(File file) {
            synchronized (files) {
                files.add(file);
            }
        }
    }

    
    public long getUsedMem() {
         MemoryMXBean mbean = ManagementFactory.getMemoryMXBean();
         return mbean.getHeapMemoryUsage().getUsed();
    }
    
    public long getMaxMem() {
         MemoryMXBean mbean = ManagementFactory.getMemoryMXBean();
         return mbean.getHeapMemoryUsage().getMax();
    }


    /**
     * Gets loggers for this jvm
     * @return map of loggers for this jvm
     */
    public Map<String, Level> getLoggers() {
        Map<String, Level> loggers = new HashMap<String, Level>();
        LogManager lm = LogManager.getLogManager();
        Enumeration<String> logNameEmu = lm.getLoggerNames();
        while(logNameEmu.hasMoreElements()) {
            String logName = logNameEmu.nextElement();
            // We skip anonymous loggers
            if (logName.length() == 0) {
                continue;
            }
            Logger logger = lm.getLogger(logName);
            Level level = null;
            do {
                level = logger.getLevel();
                if (level == null) {
                    logger = logger.getParent();
                    level = logger.getLevel();
                }
            } while (level == null);
            loggers.put(logName, level);
        }
        return Collections.unmodifiableMap(loggers);
    }
    
    /**
     * Sets the log level for this jvm
     * @param loggerMap log levels for this jvm
     * @param changeAll logger level
     * @return map with the changed loggers
     */
    public Map<String, Level> setLoggers(Map<String, Level> loggerMap, Level changeAll) {
        Map<String, Level> changed = new HashMap<String, Level>();
        LogManager lm = LogManager.getLogManager();        
        Enumeration<String> logNames = lm.getLoggerNames();
        while (logNames.hasMoreElements()) {
            String logName = logNames.nextElement();
            if (changeAll == null) {              
                for (Map.Entry<String, Level> entry : loggerMap.entrySet()) {
                    if (logName.startsWith(entry.getKey())) {
                        lm.getLogger(logName).setLevel(entry.getValue());
                    }
                }       
            } else {
                if (!logName.equals("REPORTER")) {
                    lm.getLogger(logName).setLevel(changeAll);
                    changed.put(logName, changeAll);
                }
            }
        }
        if (changeAll == null) {
            changed = loggerMap;
        }
        return changed;
    }

    private final Map<HostAndPort, RemoteLogger> remoteLoggerMap = new ConcurrentHashMap<HostAndPort, RemoteLogger>();

    /**
     * Publish a log record to all registered remote loggers
     * @param logRecord the log record
     * @throws java.io.IOException on any io error
     */
    void publish(LogRecord logRecord) throws IOException {

        DatagramSocket socket = null;
        try {
            for(RemoteLogger rl: new ArrayList<RemoteLogger>(remoteLoggerMap.values()) ) {
                if (rl.isExpired()) {
                    HostAndPort hp = rl.getHostAndPort();
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "JVMImpl.log.leaseEx", new Object [] { hp.getHost(), hp.getPort() });
                    }
                    remoteLoggerMap.remove(hp);
                } else {
                    if (socket == null) {
                        socket = new DatagramSocket();
                    }
                    rl.publish(logRecord, socket);
                }
            }
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    /**
     * Add a remote logger.
     * @param hostname  the name of the host where the remote logger is running
     * @param port      the port where the remote logger is listening for UDP datagrams
     * @param level     the log level
     * @param filter    the log fitler (can be null)
     * @return  the time to live of the remote logger
     */
    public long addRemoteLogger(String hostname, int port, Level level, Filter filter) {
        HostAndPort hp = HostAndPort.newInstance(hostname, port);
        RemoteLogger rl = new RemoteLogger(hp, level, filter);
        remoteLoggerMap.put(hp, rl);
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "JVMImpl.log.add", new Object [] { hp.getHost(), hp.getPort(), new Date(rl.getExpireTS())});
        }
        return rl.getTTL();
    }


    
    private class RemoteLogger {

        private final HostAndPort hostAndPort;
        private final Level level;
        private final Filter filter;
        private final long expireTS;
        private final static long LEASE_TIME = 5 * 60 * 1000;

        public RemoteLogger(HostAndPort hostAndPort, Level level, Filter filter) {
            this.hostAndPort = hostAndPort;
            this.level = level == null ? Level.ALL : level;
            this.filter = filter;
            expireTS = System.currentTimeMillis() + LEASE_TIME;
        }

        public long getExpireTS() {
            return expireTS;
        }

        public long getTTL() {
            return expireTS - System.currentTimeMillis();
        }

        public boolean isExpired() {
            return System.currentTimeMillis() > expireTS;
        }
        
        public void publish(LogRecord logRecord, DatagramSocket socket) throws IOException {

            if ( logRecord.getLevel().intValue() < level.intValue() || level.intValue() == Level.OFF.intValue()) {
                return;
            }
            if (filter != null && !filter.isLoggable(logRecord)) {
                return;
            }

            LogEvent evt = new LogEvent(logRecord, getName(), Hostname.getLocalHost());
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            ObjectOutputStream oout = new ObjectOutputStream(bout);

            oout.writeObject(evt);
            oout.close();

            byte[] buf = bout.toByteArray();
            
            // send request
            InetAddress address = InetAddress.getByName(hostAndPort.getHost().getHostname());
            DatagramPacket packet = new DatagramPacket(buf, buf.length, address, hostAndPort.getPort());
            socket.send(packet);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final RemoteLogger other = (RemoteLogger) obj;
            return hostAndPort.equals(other.hostAndPort);
        }

        @Override
        public int hashCode() {
            return hostAndPort.hashCode();
        }

        /**
         * @return the hostAndPort
         */
        public HostAndPort getHostAndPort() {
            return hostAndPort;
        }
    }

}
