/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.management.ComponentProxy;
import com.sun.grid.grm.ui.component.service.GetComponentInfosCommand;
import com.sun.grid.grm.ui.component.service.RemoveActiveComponentCommand;
import com.sun.grid.grm.ui.component.service.StoreActiveComponentCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import static com.sun.grid.grm.bootstrap.BootstrapConstants.*;
import com.sun.grid.grm.ui.ConfigurationService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.management.remote.JMXServiceURL;

/**
 *  The <code>ComponentService</code> provides the functionality for accessing
 *  remote components.
 *
 *
 */
public class ComponentService {

    private static final Logger log = Logger.getLogger(ComponentService.class.getName());

    private static Map<JMXServiceURL, ConfigurationService> csMap = new HashMap<JMXServiceURL, ConfigurationService>(3);
    private static Map<JMXServiceURL, ComponentInfo> csInfoMap = new HashMap<JMXServiceURL, ComponentInfo>(3);
    
    /**
     * Get the CS component
     * @param env the execution env
     * @throws com.sun.grid.grm.GrmException if the CS component is not reachable
     * @return the CS component
     */
    public static ConfigurationService getCS(ExecutionEnv env) throws GrmException {
        synchronized(csMap) {
            ConfigurationService ret = csMap.get(env.getCSURL());
            if(ret == null) {
                ComponentInfo compInfo = getCSInfo(env);
                ret = ComponentProxy.<ConfigurationService>newProxy(env, compInfo);
                csMap.put(env.getCSURL(), ret);
            }
            return ret;
        }
    }
    
    /**
     * Get the component info of the CS component
     * @param env the execution env
     * @return the component info of the CS component
     */
    public static ComponentInfo getCSInfo(ExecutionEnv env) {
        synchronized(csInfoMap) {
            ComponentInfo ret = csInfoMap.get(env.getCSURL());
            if(ret == null) {
                ret = ComponentInfo.newInstance(env, env.getCSHost(), BootstrapConstants.CS_JVM , BootstrapConstants.CS_ID, ConfigurationService.class);
                csInfoMap.put(env.getCSURL(), ret);
            }
            return ret;
        }
    }
    
    /**
     * Gets a proxy to a first found component of type <code>type</code>.
     *
     * @param env   the execution environment
     * @param type  the type of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the component or <code>null</code> if no component has not been found
     */
    @SuppressWarnings("unchecked")
    public static <T> T getComponentByType(ExecutionEnv env, Class<T> type) throws GrmException {
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return (T)getCS(env);
        }
        ComponentInfo ci = getComponentInfoByType(env,type);
        if(ci != null) {
            return ComponentProxy.<T>newProxy(env, ci);
        } else {
            return null;
        }
    }


    /**
     * Gets a proxy to a first found component of type <code>type</code>
     * and name <code>name</code>.
     *
     * @param env   the execution environment
     * @param name  name of the component
     * @param type  the type of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the component or <code>null</code> if no component has not been found
     */
    @SuppressWarnings("unchecked")
    public static <T> T getComponentByNameAndType(ExecutionEnv env, String name, Class<T> type) throws GrmException {
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return (T)getCS(env);
        }
        ComponentInfo ci = getComponentInfoByNameAndType(env, name, type);
        if(ci != null) {
            return ComponentProxy.<T>newProxy(env, ci);
        } else {
            return null;
        }
    }


    /**
     * Gets a proxy to a first found component of type <code>type</code>
     * which is assigned to run on a host <code>host</code>.
     *
     * @param env   the execution environment
     * @param type  the type of the component
     * @param host  name of the host where the component is running
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the component or <code>null</code> if no component has not been found
     */
    @SuppressWarnings("unchecked")
    public static <T> T getComponentByTypeAndHost(ExecutionEnv env, Class<T> type, Hostname host) throws GrmException {
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return (T)getCS(env);
        }
        ComponentInfo ci = getComponentInfoByTypeAndHost(env, type, host);
        if(ci != null) {
            return ComponentProxy.<T>newProxy(env, ci);
        } else {
            return null;
        }
    }

    /**
     * Gets a proxy to a first found component with name <code>name</code>
     * which is assigned to run on a host <code>host</code>.
     *
     * @param env   the execution environment
     * @param type  the type of the component
     * @param name  name of the component
     * @param host  name of the host where the component is running
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the component or <code>null</code> if the component has not been found
     */
    @SuppressWarnings("unchecked")
    public static <T> T getComponentByNameTypeAndHost(ExecutionEnv env, String name, Class<T> type, Hostname host) throws GrmException {
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return (T)getCS(env);
        }        
        ComponentInfo ci = getComponentInfoByNameTypeAndHost(env, name, type, host);
        if(ci != null) {
            return ComponentProxy.<T>newProxy(env, ci);
        } else {
            return null;
        }
    }


    /**
     * Gets a proxy to a first found component which matches ComponentInfo
     * <code>compInfo</code>.
     *
     * @param env   the execution environment
     * @param compInfo the component info
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the component or <code>null</code> if the component has not been found
     */
    @SuppressWarnings("unchecked")
    public static <T> T getComponent(ExecutionEnv env, ComponentInfo compInfo) throws GrmException {
        if(compInfo.isAssignableTo(ConfigurationService.class)) {
            return (T)getCS(env);
        }
        return ComponentProxy.<T>newProxy(env, compInfo);
    }
    
    /**
     *  Get a list of component which matches a filter
     * 
     * @return the list of components which matches a filter
     * @param env      the execution env
     * @param filter   the filter
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getComponents(ExecutionEnv env, Filter filter)  throws GrmException {
        List<ComponentInfo> cis = getComponentInfos(env, filter);
        ArrayList<T> ret = new ArrayList(cis.size());
        for(ComponentInfo ci: cis) {
            ret.add(ComponentProxy.<T>newProxy(env, ci));
        }
        return ret;
    }

    /**
     * Gets a list of proxies of all components of type <code>type</code>.
     *
     * @param env   the execution environment
     * @param type  the type of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the list of components or a empty list
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getComponentsByType(ExecutionEnv env, Class<T> type) throws GrmException {
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return Collections.<T>singletonList((T)getCS(env));
        }
        List<ComponentInfo> cis = getComponentInfosByType(env, type);
        ArrayList<T> ret = new ArrayList(cis.size());
        for(ComponentInfo ci: cis) {
            ret.add(ComponentProxy.<T>newProxy(env, ci));
        }
        return ret;
    }
    
    /**
     * Gets a list of proxies of all components of type <code>type</code>, with name
     * <code>name</code> which are assigned to run on a host <code>host</code>.
     *
     * @param env   the execution environment
     * @param type  the type of the component
     * @param host  the host where the component is running
     * @param name  the name of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the list of components or a empty list
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> getComponentsByNameTypeAndHost(ExecutionEnv env, String name, Class<T> type, Hostname host) throws GrmException {
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return Collections.<T>singletonList((T)getCS(env));
        }
        List<ComponentInfo> cis = getComponentInfosByNameTypeAndHost(env, name, type, host);
        ArrayList<T> ret = new ArrayList(cis.size());
        for(ComponentInfo ci: cis) {
            ret.add(ComponentProxy.<T>newProxy(env, ci));
        }
        return ret;
    }

    /**
     * Gets the active component info object of a first found component with
     * name <code>name</code> which is assigned to run on a host <code>host</code>.
     *
     * @param env   the execution environment
     * @param name  the name of the component
     * @param host  the host where the component is running
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the active component information
     */
    public static ComponentInfo getComponentInfoByNameAndHost(ExecutionEnv env, String name, Hostname host) throws GrmException {
        if(BootstrapConstants.CS_ID.equals(name)) {
            return getCSInfo(env);
        }
        List<ComponentInfo> canidates = getComponentInfosByNameAndHost(env, name, host);
        if(canidates.isEmpty()) {
            return null;
        } else {
            return canidates.iterator().next();
        }
    }

    /**
     * Gets the active component info object of a first found component with
     * name <code>name</code>.
     *
     * @param env   the execution environment
     * @param name  the type of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the active component information
     */
    public static ComponentInfo getComponentInfoByName(ExecutionEnv env, String name) throws GrmException {
        
        if(BootstrapConstants.CS_ID.equals(name)) {
            return getCSInfo(env);
        }
        
        List<ComponentInfo> canidates = getComponentInfosByName(env, name);
        if(canidates.isEmpty()) {
            return null;
        } else {
            return canidates.iterator().next();
        }
    }

    /**
     * Gets the active component info object of a first found component with
     * name <code>name</code> od of a type <code>type</code>.
     *
     * @param env   the execution environment
     * @param name  the name of the component
     * @param type  the type of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the active component information
     */
    public static ComponentInfo getComponentInfoByNameAndType(ExecutionEnv env, String name, Class<?> type) throws GrmException {
        
        if(BootstrapConstants.CS_ID.equals(name)) {
            return getCSInfo(env);
        }
        List<ComponentInfo> canidates = getComponentInfosByNameAndType(env, name, type);
        if(canidates.isEmpty()) {
            return null;
        } else {
            return canidates.iterator().next();
        }
    }
    
    /**
     * Gets the active component info object of a first found component with
     * name <code>name</code> od of a type <code>type</code>.
     *
     * @param env   the execution environment
     * @param name  the name of the component
     * @param type  the type of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the active component information
     */
    public static ComponentInfo getComponentInfoByNameTypeAndHost(ExecutionEnv env, String name, Class<?> type, Hostname host) throws GrmException {
        
        if(BootstrapConstants.CS_ID.equals(name)) {
            return getCSInfo(env);
        }        
        List<ComponentInfo> canidates = getComponentInfosByNameTypeAndHost(env, name, type, host);
        if(canidates.isEmpty()) {
            return null;
        } else {
            return canidates.iterator().next();
        }
    }
    

    /**
     * Gets the active component info object of a first found component of
     * a type <code>type</code>.
     *
     * @param env   the execution environment
     * @param type  the type of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs     
     * @return the active component information
     */
    public static ComponentInfo getComponentInfoByType(ExecutionEnv env, Class<?> type) throws GrmException {
        
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return getCSInfo(env);
        }
        List<ComponentInfo> canidates = getComponentInfosByType(env, type);
        if(canidates.isEmpty()) {
            return null;
        } else {
            return canidates.iterator().next();
        }
    }


    /**
     * Gets the active component info object of a first found component of
     * a type <code>type</code> which is assigned to run on a host <code>host</code>.
     *
     * @param env   the execution environment
     * @param host  the hostname of the component
     * @param type  the type of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs    
     * @return the active component information
     */
    public static ComponentInfo getComponentInfoByTypeAndHost(ExecutionEnv env, Class<?> type, Hostname host) throws GrmException {
        
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return getCSInfo(env);
        }
        
        List<ComponentInfo> canidates = getComponentInfosByTypeAndHost(env, type, host);
        if(canidates.isEmpty()) {
            return null;
        } else {
            return canidates.iterator().next();
        }
    }
    
    /**
     * Gets the list of active component info objects of all components of
     * a type <code>type</code>.
     *
     * @param env   the execution environment
     * @param type  the type of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the list of component info objects
     */
    public static List<ComponentInfo> getComponentInfosByType(ExecutionEnv env, Class<?> type) throws GrmException {
        List<ComponentInfo> cis = new LinkedList<ComponentInfo>();
        
        if(ConfigurationService.class.isAssignableFrom(type)) {
            cis.add(getCSInfo(env));
            return cis;
        }
        GetComponentInfosCommand cmd = GetComponentInfosCommand.newInstanceWithTypeFilter(type);
        return env.getCommandService().execute(cmd).getReturnValue();
    }

    /**
     * Gets the list of active component info objects of all components of
     * a type <code>type</code> and name <code>name</code>.
     *
     * @param env   the execution environment
     * @param type  the type of the component
     * @param name  name of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the list of component info objects
     */
    public static List<ComponentInfo> getComponentInfosByNameAndType(ExecutionEnv env, String name, Class<?> type) throws GrmException {
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return Collections.<ComponentInfo>singletonList(getCSInfo(env));
        }
        GetComponentInfosCommand cmd = GetComponentInfosCommand.newInstanceWithNameAndTypeFilter(name, type);
        return env.getCommandService().<List<ComponentInfo>>execute(cmd).getReturnValue();
    }
    
    /**
     * Gets the list of active component info objects of all components of
     * which has a name with matches <code>name</code>.
     *
     * @param env   the execution environment
     * @param name  name of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the list of component info objects
     */
    public static List<ComponentInfo> getComponentInfosByName(ExecutionEnv env, String name) throws GrmException {
        if(BootstrapConstants.CS_ID.equals(name)) {
            return Collections.<ComponentInfo>singletonList(getCSInfo(env));
        }
        GetComponentInfosCommand cmd = GetComponentInfosCommand.newInstanceWithNameFilter(name);
        return env.getCommandService().<List<ComponentInfo>>execute(cmd).getReturnValue();
    }
    
    /**
     * Gets the list of active component info objects of all components of
     * which has a name with matches <code>name</code> and runs host host <code>host</code>.
     *
     * @param env   the execution environment
     * @param name  name of the component
     * @param host  the host
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the list of component info objects
     */
    public static List<ComponentInfo> getComponentInfosByNameAndHost(ExecutionEnv env, String name, Hostname host) throws GrmException {
        if(BootstrapConstants.CS_ID.equals(name)) {
            return Collections.<ComponentInfo>singletonList(getCSInfo(env));
        }
        GetComponentInfosCommand cmd = GetComponentInfosCommand.newInstanceWithNameAndHostFilter(name, host);
        return env.getCommandService().<List<ComponentInfo>>execute(cmd).getReturnValue();
    }

    /**
     * Gets the list of active component info objects of all components of
     * which has a name with matches <code>name</code> and has a type which
     * is assignable to <code>type</code> and runs on host <code>host</code>.
     *
     * @param env   the execution environment
     * @param name  name of the component
     * @param type  the type of the component
     * @param host  the host the host
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the list of component info objects
     */
    public static List<ComponentInfo> getComponentInfosByNameTypeAndHost(ExecutionEnv env, String name, Class<?> type, Hostname host) throws GrmException {
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return Collections.<ComponentInfo>singletonList(getCSInfo(env));
        }
        GetComponentInfosCommand cmd = GetComponentInfosCommand.newInstanceWithNameTypeAndHostFilter(name, type, host);
        return env.getCommandService().<List<ComponentInfo>>execute(cmd).getReturnValue();
    }

    /**
     * Gets the list of active component info objects of all components of
     * a type <code>type</code> which are assigned to run on a host <code>host</code>.
     *
     * @param env   the execution environment
     * @param type  the type of the component
     * @param host  the host of the component
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return the list of component info objects
     */
    public static List<ComponentInfo> getComponentInfosByTypeAndHost(ExecutionEnv env, Class<?> type, Hostname host) throws GrmException {
        if(ConfigurationService.class.isAssignableFrom(type)) {
            return Collections.<ComponentInfo>singletonList(getCSInfo(env));
        }
        GetComponentInfosCommand cmd = GetComponentInfosCommand.newInstanceWithHostnameAndTypeFilter(host, type);
        return env.getCommandService().<List<ComponentInfo>>execute(cmd).getReturnValue();
    }

    /**
     * Get the list of active component info objects which matches a filter
     * @param env     the execution env
     * @param filter  the filter
     * @throws com.sun.grid.grm.GrmException if any problem during command execution occurs
     * @return list of active component info objects which matches a filter
     */
    public static List<ComponentInfo> getComponentInfos(ExecutionEnv env, Filter<ComponentInfo> filter) throws GrmException {
        GetComponentInfosCommand cmd = new GetComponentInfosCommand(filter);
        return env.getCommandService().<List<ComponentInfo>>execute(cmd).getReturnValue();
    }
    
    /**
     * Stores/persists componentInfo information of active component in the CS
     *
     * @param env   the execution environment
     * @param compInfo ComponentInfo instance to be stored
     * @throws GrmException when the operation of storing active component info fails
     */
    public static void storeComponentInfo(ExecutionEnv env, ComponentInfo compInfo) throws GrmException {
        StoreActiveComponentCommand sac = new StoreActiveComponentCommand(compInfo.getIdentifier(), compInfo.toActiveComponent());
        env.getCommandService().execute(sac);
    }
    
    /**
     * Remove componentInfo information of active component from the CS
     *
     * @param env   the execution environment
     * @param identifier of ComponentInfo instance to be removed
     * @throws GrmException when the operation of removing active component info fails
     */
    public static void removeComponentInfo(ExecutionEnv env, String identifier) throws GrmException {
        RemoveActiveComponentCommand rac = new RemoveActiveComponentCommand(identifier);
        env.getCommandService().execute(rac);
    }
}
