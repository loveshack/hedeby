/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.util.Hostname;
import java.io.Serializable;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.util.I18NManager;

/**
 * Runtime component information for active MBean.
 *
 * An instance of this class represents an available MBean interface
 * provided by a MBean component available and active on a specific host.
 *
 * This class makes also information about running and registered MBeans
 * accessable which is stored in a single directory in form of single files.
 *
 */
public class ComponentInfo implements Serializable {
    
    private static final long serialVersionUID = -4267161754426407532L;
    private static final String BUNDLE_NAME = "com.sun.grid.grm.bootstrap.messages";
    
    private final ObjectName objName;
    private final JvmInfo jvmInfo;
    
    /**
     * Create a new instance of <code>ComponentInfo</code>
     * out of a <code>ActiveComponent</code> object
     * 
     * @return the <code>ComponentInfo</code>
     * @param ac the <code>ActiveComponent</code> object
     */
    public static ComponentInfo newInstance(ActiveComponent ac) {
        if (ac == null) {
           throw new NullPointerException("ac must not be null");
        }
        try {
            return new ComponentInfo(Hostname.getInstance(ac.getHost()), ac.getJvm(), new ObjectName(ac.getMBeanName()));
        } catch (MalformedObjectNameException ex) {
            throw new IllegalArgumentException(I18NManager.formatMessage("ComponentInfo.invalidObj", BUNDLE_NAME), ex);
        }
    }
    
    /**
     * Create a new instance of ComponentInfo
     * @param env        the execution env
     * @param hostname   the hostname
     * @param jvm        the jvm name
     * @param name       the component name
     * @param type       the component type
     * @return the component info
     */
    public static ComponentInfo newInstance(ExecutionEnv env, Hostname hostname, String jvm, String name, Class<?> type) {
        return new ComponentInfo(hostname, jvm, createObjectName(env, name, type));
    }
    
    /**
     * Create a new instance of ComponentInfo
     * @param hostname    the hostname
     * @param jvm         the jvmname
     * @param objName     the mbean name
     */
    public ComponentInfo(Hostname hostname, String jvm, ObjectName objName) {
        if (objName == null) {
            throw new NullPointerException("objName must not be null");
        }
        jvmInfo = new JvmInfo(jvm, hostname);
        
        if(objName.getKeyProperty(BootstrapConstants.NAME_KEY_PROPERTY) == null) {
            throw new IllegalArgumentException("object name " + objName + " does not contain a name");
        }
        
        String type = objName.getKeyProperty("type");
        
        if(type == null) {
            throw new IllegalArgumentException("object name " + objName + " does not contain a type");
        }
        this.objName = objName;
    }
    
    
    /**
     * Create a JMX object name for a component.
     *
     * @param systemName  name of the Hedeby system
     * @param name  the name of the component
     * @param type  type of the component
     * @return the object name
     */
    public static ObjectName createObjectName(String systemName, String name, Class type) {
        String typeName = type.getName();
        StringBuilder buf = new StringBuilder("com.sun.grid.grm.".length()
                                            + systemName.length() 
                                            + ":name)".length()
                                            + name.length()
                                            + ",type=".length()
                                            + typeName.length());
        
        buf.append("com.sun.grid.grm.");
        buf.append(systemName);
        buf.append(":name=");
        buf.append(name);
        buf.append(",type=");
        buf.append(typeName);
        try {
            return new ObjectName(buf.toString());
        } catch (MalformedObjectNameException ex) {
            // This can only occur of the system name contains invalid parameters
            throw new IllegalArgumentException(I18NManager.formatMessage("ComponentInfo.invalidObj", BUNDLE_NAME), ex);
        }
    }
    
    /**
     * Create a JMX object name for a component.
     *
     * @param env   the execution environment
     * @param name  the name of the component
     * @param type  type of the component
     * @return the object name
     */
    public static ObjectName createObjectName(ExecutionEnv env, String name, Class type) {
        return createObjectName(env.getSystemName(), name, type);
    }
    
    /**
     * get the name of the component
     *
     * @return the name of the component
     */
    public String getName() {
        return this.objName.getKeyProperty(BootstrapConstants.NAME_KEY_PROPERTY);
    }
    
    /**
     * Returns the ContanctString
     *
     * @return MBean name
     */
    public ObjectName getObjectName() {
        return objName;
    }
    
    /**
     * Get the hostname of this component
     * @return the hostname;
     */
    public Hostname getHostname() {
        return jvmInfo.getHostname();
    }
    
    /**
     *  Get the jvm of the component.
     *
     *  @return the jvm
     */
    public JvmInfo getJvm() {
        return jvmInfo;
    }
       
    /**
     *  Get full qualified classname of the interface
     *  which is implemented by the component
     *  @return the full qualified classname of the interface
     */
    public String getInterface() {
        return objName.getKeyProperty("type");
    }
    
    
    /**
     * Is the interface of this component assignable to a type.
     *
     * @param type the interface
     * @return <code>true</code> if the interface of the component is assignable to <code>type</code>
     */
    public boolean isAssignableTo(Class<?> type) {
        try {
            Class<?> interfaceClass = Class.forName(getInterface(), true, type.getClassLoader());
            return type.isAssignableFrom(interfaceClass);
        } catch(ClassNotFoundException ex) {
            return false;
        }
    }
    
    /**
     * Returns the object as string.
     * 
     * @return Object is string
     */
    @Override
    public String toString() {
        return String.format("%s/%s", jvmInfo, objName);
    }

    /**
     * Returns the hashcode
     *
     * @return Hashcode
     */
    @Override
    public int hashCode() {
        int h = jvmInfo.hashCode();
        h = h * 31 + objName.hashCode();
        return h;
    }
    
    /**
     * Compares two ComponentInfos or a ComponentInfo with a String.
     * If both are equivalent then true will be returned
     *
     * @param obj Reference to a String or ComponentInfo.
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof ComponentInfo
            && jvmInfo.equals(((ComponentInfo)obj).jvmInfo)
            && objName.equals(((ComponentInfo)obj).objName);
    }


    /**
     * Convert a <code>ComponentInfo</code> into an <code>ActiveComponent</code> object.
     *
     * @param componentInfo  the <code>ComponentInfo</code>
     * @return the <code>ActiveComponent</code> object
     */
    public static ActiveComponent toActiveComponent(ComponentInfo componentInfo) {
        ActiveComponent ret = new ActiveComponent();
        ret.setMBeanName(componentInfo.getObjectName().toString());
        ret.setJvm(componentInfo.jvmInfo.getName());
        ret.setHost(componentInfo.jvmInfo.getHostname().getHostname());
        return ret;
    }
    
    
    /**
     * Convert this <code>ComponentInfo</code> into an 
     * <code>ActiveComponent</code> object.
     *
     * @return the <code>ActiveComponent</code> object
     */
    public ActiveComponent toActiveComponent() {
        return toActiveComponent(this);
    }
    
    /**Get the identifier of this component
     * @return identifier &lt;name&gt;:&lt;hostname&gt;
     */
    public String getIdentifier() {
        return createIdentifier(getName(), jvmInfo.getHostname().getHostname());
    }
    
    /**
     * Helper static method to create identifier of Active Component
     * without the need of creating object
     * @param name of the component
     * @param hostname on which component is running
     * @return String representation of identifier
     */
    public static String createIdentifier(String name, String hostname) {
        int size = name.length() + hostname.length() +1;
        StringBuilder ret = new StringBuilder(size);
        ret.append(name);
        ret.append('@');
        ret.append(hostname.replace('.', '_'));
        return ret.toString();
    }

}
