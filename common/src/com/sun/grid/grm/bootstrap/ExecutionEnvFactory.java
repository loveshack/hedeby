/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *  
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;


import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.security.ClientSecurityContextFactory;
import com.sun.grid.grm.security.DefaultClientSecurityContextFactory;
import com.sun.grid.grm.security.DefaultServerSecurityContextFactory;
import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.security.NullSecurityContextFactory;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.BackingStoreException;
import javax.security.auth.callback.CallbackHandler;

/**
 *  Factory class for the <code>ExecutionEnv</code> instances.
 *
 *  The exists three ways to create an <code>ExecutionEnv</code>:
 *
 *  <ul>
 *      <li>Construct it out of the information which are stored
 *          in user or system preferences.</br>
 *          The UI uses the information in the preferences to construct
 *          the execution environment.</li>
 *      <li>Construct it out of the system property.</br>
 *          The hedeby process defines a set of system properties
 *          which hold the necessary information about the execution
 *          environment.
 *      </li>
 *      <li>Specify all parameters manually.<br>
 *          Junit tests uses this possibility to construct a dummy execution
 *          env.
 *      </li>
 *  </ul>
 *
 *  <p>
 *  Hedeby system defines the configuration information in the preferences node
 *  <code>com/sun/grid/grm/bootstrap/&lt;system name&gt;</code>. The following key
 *  are used:
 *  </p>
 *
 *  <table>
 *    <tr><th>Key</th><th>Description</th></tr>
 *    <tr><td><code>shared</code></td>
 *        <td>Path to the shared directory</td>
 *    </tr>
 *    <tr><td><code>dist</code></td>
 *        <td>Path to the hedeby distribution</td>
 *    </tr>
 *    <tr><td><code>localSpool</code></td>
 *        <td>Path to local spool directory</td>
 *    </tr>
 *    <tr><td><code>auto_start</code></td>
 *        <td>Auto start flag for the hedeby system (default value is <code>false</code>)</td>
 *    </tr>
 *    <tr><td><code>no_ssl</code></td>
 *        <td>NO_SSL flag (default value is <code>false</code>)</td>
 *    </tr>
 *  </table>
 *
 *  <p>The <code>ExecutionEnvFactory</code> uses the following system properties to 
 *     construct an execution environment</p>
 *
 *  <table>
 *    <tr><th>Key</th><th>Description</th></tr>
 *    <tr><td><code>com.sun.grid.grm.bootstrap.shared</code></td>
 *        <td>Path to the shared directory</td>
 *    </tr>
 *    <tr><td><code>com.sun.grid.grm.bootstrap.dist</code></td>
 *        <td>Path to the hedeby distribution</td>
 *    </tr>
 *    <tr><td><code>com.sun.grid.grm.bootstrap.localSpool</code></td>
 *        <td>Path to local spool directory</td>
 *    </tr>
 *    <tr><td><code>com.sun.grid.grm.bootstrap.auto_start</code></td>
 *        <td>Auto start flag for the hedeby system (default value is <code>false</code>)</td>
 *    </tr>
 *    <tr><td><code>com.sun.grid.grm.bootstrap.no_ssl</code></td>
 *        <td>NO_SSL flag (default value is <code>false</code>)</td>
 *    </tr>
 *  </table>
 *
 *  <p><b>Example:</b> Get the execution env from the preferences</p>
 *
 *  <pre>
 *     String systemName = "test_system";
 *     PreferencesType prefs = PreferencesType.USER;
 *     ExecutionEnv env = ExecutionEnvFact.newInstanceFromPrefs(systemName, prefs);
 *  </pre>
 *
 *  <p><b>Example:</b> Get the execution env from the system properties</p>
 *
 *  <pre>
 *     ExecutionEnv env = ExecutionEnvFact.newInstanceFromSystemProperties();
 *  </pre>
 *
 *
 *  <p><b>Example:</b> Get the execution env manually</p>
 *  <pre>
 *     File sharedDir = new File("/net/&lt;host&gt;/grm/shared");
 *     File localSpoolDir = new File("/var/spool/grm");
 *     File distDir = new File("/opt/grm");
 *     Map<String,?> properties = Collections.emptyMap();
 * 
 *     PreferencesType prefs = PreferencesType.USER;
 *     ExecutionEnv env = ExecutionEnvFact.newInstanceFromPrefs(sharedDir, localSpoolDir, distDir, properties);
 *  </pre>
 *
 * <p><b>Important:</b> Because of updates/upgrades with each minor release ExecutionEnv creation from preferences has to be compatible
 * This means that ExecutionEnv should be able to create from preferences for u3,u4,u$current_release. The reason for that is that
 * upgrade/update of preferences is triggered after ExecutionEnv is created.</p>
 */
public class ExecutionEnvFactory {
    
    
    /**
     * property name for system name
     */
    public static final String SYSTEM_NAME = "systemname";
    /**
     * used resource bundle for this class
     */
    public static final String BUNDLE = "com.sun.grid.grm.bootstrap.messages";
    
    /**
     * Create a new instance of the <code>ExecutionEnv</code>.
     * All necessary are specified manually.
     * 
     * <p><b>Attention!!!</b> The returned <code>ExecutionEnv</code> contains
     * no security context. It can not be used to communicate with a remote
     * system.
     * @return the <code>ExecutionEnv</code>
     * @param csHost host where CS is running
     * @param csPort port where CS is listening
     * @param name system name
     * @param localSpoolDir path to the local spool directory
     * @param distDir path to the hedeby distribution
     * @param properties additional properties
     */
    public static ExecutionEnv newInstance(String name, File localSpoolDir, File distDir, Hostname csHost, int csPort, Map<String,?> properties) {
        try {
            return new ExecutionEnvImpl.Builder(name)
                    .localSpoolDir(localSpoolDir)
                    .distDir(distDir)
                    .csInfo(csHost, csPort)
                    .properties(properties)
                    .securityContextFactory(NullSecurityContextFactory.getInstance())
                    .newInstance();
        } catch (GrmSecurityException ex) {
            throw new IllegalStateException("Must not happen, NULL_SECURITY_CONTEXT_FACTORY does not throw an exception", ex);
        }
    }
    
    /**
     * Create a new instance of the <code>ExecutionEnv</code>.
     * All parameters are null. Such instance is needed for retrieving
     * CommandService when the system is not existing, so Local commands
     *  can be executed.
     * 
     * <p><b>Attention!!!</b> The returned <code>ExecutionEnv</code> contains
     * no security context. It can not be used to communicate with a remote
     * system. 
     * @return the <code>ExecutionEnv</code>
     */
    public static ExecutionEnv newNullInstance() {
        try {
            return new ExecutionEnvImpl.Builder("null")
                    .csInfo(Hostname.getLocalHost(), 0)
                    .securityContextFactory(NullSecurityContextFactory.getInstance())
                    .newInstance();
        } catch(GrmSecurityException ex) {
            throw new IllegalStateException("Must not happen, NULL_SECURITY_CONTEXT_FACTORY does not throw an exception", ex);
        }
    }
    
    /**
     * Create a new <code>ExecutionEnv</code> for a client
     * @return the execution env
     * @param csHost host where CS is running
     * @param csPort port where CS is listening
     * @param name system name
     * @param localSpoolDir path to the local spool dir
     * @param distDir path to the hedeby distribution
     * @param properties additonal properties
     * @param username username
     * @param callback callback handler for getting credentials
     * @throws com.sun.grid.grm.security.GrmSecurityException if the setup of the security context failed
     */
    public static ExecutionEnv newClientInstance(String name, File localSpoolDir, File distDir, Hostname csHost, int csPort, Map<String,?> properties, 
                                                 String username, CallbackHandler callback) throws GrmSecurityException {

        return new ExecutionEnvImpl.Builder(name)
                .localSpoolDir(localSpoolDir)
                .distDir(distDir)
                .csInfo(csHost, csPort)
                .properties(properties)
                .securityContextFactory(new DefaultClientSecurityContextFactory(username, callback))
                .newInstance();
    }
    
    /**
     * Create a new instance of the <code>ExecutionEnv</code> for a client.
     * @return the <code>ExecutionEnv</code>
     * @param systemName name of the system
     * @param prefs preferences type (user or system preferences)
     * @param username name of the user
     * @param callback CallBackHandler for providing login information
     * @throws com.sun.grid.grm.security.GrmSecurityException if the setup of the security context failed
     * @throws com.sun.grid.grm.bootstrap.PreferencesNotAvailableException if required values are not defined
     *         in the preferences
     * @throws java.util.prefs.BackingStoreException if fail to read preferences of system
     * @throws com.sun.grid.grm.GrmException if creation of CS context fails
     */
    public static ExecutionEnv newClientInstance(String systemName, PreferencesType prefs, String username, CallbackHandler callback) 
            throws BackingStoreException, PreferencesNotAvailableException, GrmSecurityException, GrmException {
        return newClientInstance(systemName, prefs, new DefaultClientSecurityContextFactory(username, callback));
    }

    /**
     * Create a new instance of the <code>ExecutionEnv</code> for a client.
     * @return the <code>ExecutionEnv</code>
     * @param systemName name of the system
     * @param prefs preferences type (user or system preferences)
     * @param csf factory for the client security context
     * @throws com.sun.grid.grm.security.GrmSecurityException if the setup of the security context failed
     * @throws com.sun.grid.grm.bootstrap.PreferencesNotAvailableException if required values are not defined
     *         in the preferences
     * @throws java.util.prefs.BackingStoreException if fail to read preferences of system
     * @throws com.sun.grid.grm.GrmException if creation of CS context fails
     */
    public static ExecutionEnv newClientInstance(String systemName, PreferencesType prefs, final ClientSecurityContextFactory csf)
            throws BackingStoreException, PreferencesNotAvailableException, GrmSecurityException, GrmException {

        return createBuilderFromPrefs(systemName, prefs)
                         .securityContextFactory(csf)
                         .newInstance();
    }
    
    private static ExecutionEnvImpl.Builder createBuilderFromPrefs(String systemName, PreferencesType prefs) throws BackingStoreException, PreferencesNotAvailableException, GrmException {
        if (PreferencesUtil.existsSystem(systemName, prefs) == false) {
            throw new PreferencesNotAvailableException("ExecutionEnvFactory.preferences_not_available", BUNDLE, systemName);
        }
        Map<String, Object> properties = new HashMap<String, Object>();
        
        properties.put(ExecutionEnv.NO_SSL, PreferencesUtil.isSSLDisabled(systemName, prefs));
        properties.put(ExecutionEnv.AUTO_START, PreferencesUtil.isAutoStart(systemName, prefs));
        properties.put(BootstrapConstants.SMF_KEY, PreferencesUtil.isSMF(systemName, prefs));
        
        String csInfo = PreferencesUtil.getCSInfo(systemName, prefs);
        String [] csProperties = SystemUtil.getCSPropertiesFromString(csInfo);
        String csHost = SystemUtil.getCSProperty(csProperties, BootstrapConstants.CS_HOST);
        Hostname csHostname = Hostname.getInstance(csHost);
        
        String csPortStr = SystemUtil.getCSProperty(csProperties, BootstrapConstants.CS_PORT);
        
        int csPort = Integer.parseInt(csPortStr);

        return new ExecutionEnvImpl.Builder(systemName)
                    .distDir(PreferencesUtil.getDistDir(systemName, prefs))
                    .localSpoolDir(PreferencesUtil.getLocalSpoolDir(systemName, prefs))
                    .csInfo(csHostname, csPort)
                    .properties(properties);
    }
    
    
    /**
     * Create a new instance of the <code>ExecutionEnv</code>. The information are
     * read for the preferences.
     *
     * <p><b>Attention !!!</b> This execution env can not be used for communicating
     * with another jvm, because it contains no security context.
     * 
     * @return the <code>ExecutionEnv</code>
     * @param systemName name of the system
     * @param prefs preferences type (user or system preferences)
     * @throws com.sun.grid.grm.bootstrap.PreferencesNotAvailableException if required values are not defined
     *         in the preferences 
     * @throws com.sun.grid.grm.GrmException if creation of CS context fails
     * @throws java.util.prefs.BackingStoreException if fail to read preferences of system
     */
    public static ExecutionEnv newInstanceFromPrefs(String systemName, PreferencesType prefs) throws BackingStoreException, PreferencesNotAvailableException, GrmException {
        return createBuilderFromPrefs(systemName, prefs).newInstance();
    }
    
    
    private static ExecutionEnv instanceFromSystemProperties;
    
    /**
     * Create a get instance of the  <code>ExecutionEnv</code> from the system properties.
     *
     * @return the <code>ExecutionEnv</code>
     * @throws com.sun.grid.grm.bootstrap.SystemPropertyNotAvailableException if a required
     *            system property is not defined
     * @throws com.sun.grid.grm.GrmException if creation of CS context fails
     */
    public synchronized static ExecutionEnv getInstanceFromSystemProperties() throws SystemPropertyNotAvailableException, GrmException {
        
        if(instanceFromSystemProperties == null) {
            String systemName = getRequiredSystemProperty(SYSTEM_NAME);
            File distDir = getPathFromSystemProperty(BootstrapConstants.DIST_KEY);
            File localSpoolDir = getPathFromSystemProperty(BootstrapConstants.LOCAL_SPOOL_KEY);
            String jvmName = System.getProperty(BootstrapConstants.JVM_NAME_PROPERTY);
            if(jvmName == null) {
                throw new SystemPropertyNotAvailableException(BootstrapConstants.JVM_NAME_PROPERTY);
            }

            String csInfo = getRequiredSystemProperty(BootstrapConstants.CS_INFO_PROPERTY);
            String [] csProperties = SystemUtil.getCSPropertiesFromString(csInfo);
            String csHost = SystemUtil.getCSProperty(csProperties, BootstrapConstants.CS_HOST);
            String csPortStr = SystemUtil.getCSProperty(csProperties, BootstrapConstants.CS_PORT);
            
            Hostname csHostname = Hostname.getInstance(csHost);
            int csPort = Integer.parseInt(csPortStr);

            Map<String, Object> properties = new HashMap<String, Object>();

            boolean sslDisabled = Boolean.valueOf(getOptionalSystemProperty(ExecutionEnv.NO_SSL));
            properties.put(ExecutionEnv.NO_SSL, sslDisabled);
            properties.put(ExecutionEnv.AUTO_START, Boolean.valueOf(getOptionalSystemProperty(ExecutionEnv.AUTO_START)));
            properties.put(BootstrapConstants.SMF_KEY, Boolean.valueOf(getOptionalSystemProperty(BootstrapConstants.SMF_KEY)));

            ExecutionEnv ret = new ExecutionEnvImpl.Builder(systemName)
                                    .distDir(distDir)
                                    .localSpoolDir(localSpoolDir)
                                    .csInfo(csHostname, csPort)
                                    .properties(properties)
                                    .securityContextFactory(new DefaultServerSecurityContextFactory(jvmName))
                                    .newInstance();
            
            instanceFromSystemProperties = ret;
        }
        return instanceFromSystemProperties;
    }
    
    /**
     * Get the path to the local spool directory from the system properties
     * @throws com.sun.grid.grm.bootstrap.SystemPropertyNotAvailableException if system properties are not defined
     * @return the path to the local spool directory
     */
    public static File getLocalSpoolDirFromSystemProperties() throws SystemPropertyNotAvailableException {
        return getPathFromSystemProperty(BootstrapConstants.LOCAL_SPOOL_KEY);
    }
    
    /**
     *  Get the name of a system property
     *
     *  @param key  name of the system property without package definititon
     *  @return the system property name
     */
    public static String getSystemPropertyName(String key) {
            return ExecutionEnv.class.getPackage().getName() + "." + key;
    }
    
    private static String getOptionalSystemProperty(String key) {
        String name = getSystemPropertyName(key);
        return System.getProperty(name);
    }
    
    private static String getRequiredSystemProperty(String key) throws SystemPropertyNotAvailableException {
        String name = getSystemPropertyName(key);
        String value = System.getProperty(name);
        if(value == null) {
            throw new SystemPropertyNotAvailableException(name);
        }
        return value;
    }
    
    private static File getPathFromSystemProperty(String key) throws SystemPropertyNotAvailableException {
        String str = getRequiredSystemProperty(key);
        return new File(str);
    }
    
}
