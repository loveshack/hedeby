/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.util.GrmClassLoaderFactory;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Helper class which provides set of methods for retrieving upgrade specific information
 */
public class UpgradeUtil {

    private static final String BUNDLE_NAME = "com.sun.grid.grm.bootstrap.messages";
    private static Upgrade proxy;
    private static boolean initialize = false;
 
    private static Upgrade getProxy(ExecutionEnv env) {
        if (!initialize) {
            proxy = UpgradeProxy.getProxy(env);
            initialize = true;
        }
        return proxy;
    }

    /*
     * Check if sdm-upgrade-impl.jar is available
     * @param env ExecutionEnv
     * @return true if UpgradeImpl from sdm-upgrade-impl.jar is available, otherwise false
     */
    public static boolean isUpgradeAvailable(ExecutionEnv env) {
        if (getProxy(env) == null) {
            return false;
        }
        return true;
    }

    /**
     * Check version of the host is supported by this binary. And if host should be upgraded.
     * @param hostVersion - version of the current host
     * @param systemVersion - version of system
     * @param env ExecutionEnv
     * @return true if version of current host should be upgraded.
     * @throws com.sun.grid.grm.GrmException - if binaryVersion is unsupported
     */
     public static boolean isUpgradeNecessary(ExecutionEnv env, String hostVersion, String systemVersion)  throws GrmException {
        if (getProxy(env) == null) {
            throw new UpgradeNotAvailableException("UpgradeUtil.error.upgradenotavailable", BUNDLE_NAME);
        }
        return getProxy(env).isUpgradeNecessary(env, hostVersion, systemVersion);
    }

    /**
     * Upgrade the host to latest version (binary version)
     * @param env - ExecutionEnv
     * @param version - version of the host
     * @throws UpgradeNotAvailableException - missing version binary
     * @throws GrmException - when upgrade procedure cannot be executed
     */
    public static UpgradeResult executeHostUpgrade(ExecutionEnv env, String hostVersion, String systemVersion)
            throws UpgradeNotAvailableException, GrmException {
        if (getProxy(env) == null) {
            throw new UpgradeNotAvailableException("UpgradeUtil.error.upgradenotavailable", BUNDLE_NAME);
        }
        return getProxy(env).upgradeHost(env, hostVersion, systemVersion);
    }

    /**
     * Get the version of binaries
     * @param env ExecutionEnv
     * @return String representing version of binary
     * @throws com.sun.grid.grm.bootstrap.UpgradeNotAvailableException - missing version binary
     * @throws com.sun.grid.grm.GrmException - failed to call version method
     */
    public static String getBinaryVersion(ExecutionEnv env) throws UpgradeNotAvailableException {
        if (getProxy(env) == null) {
            throw new UpgradeNotAvailableException("UpgradeUtil.error.upgradenotavailable", BUNDLE_NAME);
        }
        return getProxy(env).getBinaryVersion();
    }

    

    }
/**
 * Helper class to hide reflections to UpgradeImpl from sdm-upgrade-impl.jar
 */
class UpgradeProxy implements InvocationHandler {

    private static final String UPGRADEIMPL_CLASSNAME = "com.sun.grid.grm.upgrade.UpgradeImpl";
    private String BUNDLE_NAME = "com.sun.grid.grm.bootstrap.messages";
    private final Upgrade proxy;
    private Upgrade upgradeImpl;
    private ClassLoader delegateClassLoader;

    @SuppressWarnings(value = "unchecked")
    private UpgradeProxy(ExecutionEnv env) {
        boolean exists = true;

        delegateClassLoader = GrmClassLoaderFactory.getInstance(getClasspath(env), UpgradeUtil.class.getClassLoader());
       
        try {
            Class cls = Class.forName(UPGRADEIMPL_CLASSNAME, true, delegateClassLoader);
            
            Constructor cons = cls.getConstructor(new Class[]{});
            upgradeImpl = (Upgrade) cons.newInstance(new Object[]{});
        } catch (InstantiationException ex) {
            exists = false;
        } catch (IllegalAccessException ex) {
            exists = false;
        } catch (IllegalArgumentException ex) {
            exists = false;
        } catch (InvocationTargetException ex) {
            exists = false;
        } catch (NoSuchMethodException ex) {
            exists = false;
        } catch (SecurityException ex) {
            exists = false;
        } catch (ClassNotFoundException ex) {
            exists = false;
        } finally {
            if (exists) {

                proxy = (Upgrade) Proxy.newProxyInstance(delegateClassLoader,
                        new Class<?>[]{Upgrade.class}, this);
            } else {
                proxy = null;
                upgradeImpl = null;
            }
        }

    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        
        if (getUpgradeInstance() == null) {
            throw new UpgradeNotAvailableException("UpgradeUtil.error.upgradenotavailable", BUNDLE_NAME);
        }
        if (method.getDeclaringClass().equals(Object.class)) {
            if(method.getName().equals("toString")) {
                return "";
            } else if (method.getName().equals("equals")) {
                return false;
            } else if (method.getName().equals("hashCode")) {
                return 0;
            } else {
                return null;
            }
        } else {
            try {
                return method.invoke(getUpgradeInstance(), args);
            } catch (InvocationTargetException ex) {
                throw ex.getTargetException();
            }           
        }
    }

    /**
     * @param env ExecutionEnv
     * @return the proxy to Upgrade, null if upgrade is not available
     */
    public static Upgrade getProxy(ExecutionEnv env) {
        return new UpgradeProxy(env).proxy;
    }
    private Upgrade getUpgradeInstance() {
        return upgradeImpl;
    }
    /**
     * The classpath has to be generated so we can add private jar into classloader
     * Ignores all error when contructing jgdi.jar url
     * @param env ExecutionEnv
     * @return URL[] classpath
     */
    @SuppressWarnings(value = "unchecked")
    private URL[] getClasspath(ExecutionEnv env) {
         //public api contains this configuration object
        //$sge_root/lib is required for jgdi.jar the only reliable location we can take is from CA component,
        //the reflection has to be used to get it properly
        File jgdiJar = null;
        if (env.getCSHost().equals(Hostname.getLocalHost())) {
            try {
                //get sgeroot from CA component
                //public api contains this configuration object
                String caConfig = "com.sun.grid.grm.config.security.CAComponentConfig";
                String configName = "ca";
                //get CA config from CS, locally, If accessed pre-installed system ignore the failure
                Object config = null;
                Command cmd = new GetConfigurationCommand(configName);
                config = cmd.execute(env).getReturnValue();
                
                Class caClass = Class.forName(caConfig);
                Method method = caClass.getMethod("getSgeCaScript", new Class<?>[0]);
                String caScriptPath = (String) method.invoke(config, new Object[0]);
                File sge = new File(caScriptPath);
                for (int i=0; i<3; i++) {
                    if (sge != null) {
                        sge = sge.getParentFile();
                    } else {
                        throw new GrmException();
                    }
                }
                //the 3 last elements from path were cut off, sge points to sgeroot
                jgdiJar = new File(sge, "lib"+File.separator+"jgdi.jar");

            } catch (IllegalAccessException ex) {
                 //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (IllegalArgumentException ex) {
                 //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (InvocationTargetException ex) {
                 //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (NoSuchMethodException ex) {
                 //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (SecurityException ex) {
                 //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (ClassNotFoundException ex) {
                 //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (GrmException ex) {
                //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar

            }

        } else {
            try {
                //managed host check if ge_adapter is running there Global
                String geConfig = "com.sun.grid.grm.config.gridengine.GEServiceConfig";
                String connectionConfig = "com.sun.grid.grm.config.gridengine.ConnectionConfig";
 
                GlobalConfig global = env.getCommandService().execute(new GetGlobalConfigurationCommand()).getReturnValue();
                //find ge adapter on this host
                String name = getGEAdapterName(global);
                if (name != null) {
                    Object config = env.getCommandService().execute(new GetConfigurationCommand(name)).getReturnValue();
                    //check if classes are there
                    Class geConfigClass = Class.forName(geConfig);
                    Class connectionConfigClass = Class.forName(connectionConfig);
                    //methods
                    Method methodGeConfig = geConfigClass.getMethod("getConnection", new Class<?>[0]);
                    Method methodConnection = connectionConfigClass.getMethod("getRoot", new Class<?>[0]);
                    Object connection = methodGeConfig.invoke(config, new Object[0]);
                    String root = (String) methodConnection.invoke(connection, new Object[0]);
                    jgdiJar = new File(root, "lib" + File.separator + "jgdi.jar");
                }
            } catch (IllegalAccessException ex) {
                //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (IllegalArgumentException ex) {
                //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (InvocationTargetException ex) {
                //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (NoSuchMethodException ex) {
                //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (SecurityException ex) {
               //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (ClassNotFoundException ex) {
                //jgdi path cannot be constructed, sgeroot not available, ignore errors upgrade will be accessed without jgdi.jar
            } catch (GrmException ex) {
                //ignore jdgi cannot be constructed, it will not be part of classpath
            }
        }
        //Jgdi.jar is not added to classpath if we could not create it, if upgrade is missing this jar the exception will be thrown anyway
 
 
        File serviceImplJar = new File(PathUtil.getDistLibPath(env), "sdm-ge-adapter-impl.jar");
        File upgradeJar = new File(PathUtil.getDistLibPath(env), "sdm-upgrade-impl.jar");

        URL urls[];
        try {
            if (jgdiJar != null) {
                urls = new URL[]{
                            upgradeJar.toURI().toURL(),
                            serviceImplJar.toURI().toURL(),
                            jgdiJar.toURI().toURL()
                        };
            } else {
                urls = new URL[]{
                            upgradeJar.toURI().toURL(),
                            serviceImplJar.toURI().toURL()
                        };
            }
        } catch (MalformedURLException ex) {
            throw new IllegalStateException("File.toURL throwed exception", ex);
        }
        return urls;
    }
    /**
     * Search the GlobalConfig for defined GE Adapter on localhost, if geadapter exists, sgeroot should be accessible
     * @param gc GlobalConfig
     * @return first matched geAdapter name, null if not found
     */
    private String getGEAdapterName(GlobalConfig gc) {
        String geAdapter = "com.sun.grid.grm.service.impl.ge.GEServiceContainer";
        for (JvmConfig jvm : gc.getJvm()) {
            for (Component c : jvm.getComponent()) {
                if (c.getClassname().equals(geAdapter)) {
                    if (c instanceof Singleton) {
                        if (Hostname.getLocalHost().equals(Hostname.getInstance(((Singleton) c).getHost()))) {
                            //got the matching component
                            return c.getName();
                        }
                    }
                }
            }
        }
        return null;
    }
}
