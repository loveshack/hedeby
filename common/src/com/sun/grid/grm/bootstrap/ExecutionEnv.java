/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.bootstrap;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdException;
import com.sun.grid.grm.security.SecurityContext;
import com.sun.grid.grm.ui.CommandService;
import com.sun.grid.grm.util.HostAndPort;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.util.Map;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;

/**
 *  The execution environment contains all mandatory information for a 
 *  Hedeby system. The component only uses an instance of this class to
 *  get enviroment properties. They must not use system properties or 
 *  preferences.
 *
 *  The <code>ExecutionEnv</code> contains the following information:
 *
 *  <ul>
 *    <li>References to the initial JNDI context to the Hedeby configuration
 *        service.</li>
 *    <li>A file object which points to the local spool directory</li>
 *    <li>A file object which point to the directory with the hedeby
 *        distribution.</li>
 *    <li>A map of extended properties.</li>
 *  </ul>
 *
 *  An instance of the <code>ExecutionEnv</code> can be created with the 
 *  <code>ExecutionEnvFactory</code>.
 *
 *  The following extended properties are known:
 *
 *  <table border="1">
 *    <tr><th>property</th><th>description</th></tr>
 *    <tr><td>no_ssl</td>
 *        <td>Disables the ssl encrypted communication between the Hedeby
 *            components. Only usefull for test systems.</td></tr>
 *    <tr><td>auto_start</td>
 *        <td>Flag which indicates that the process of the system will be started 
 *            host boot time (Additional steps are necessary to boot a hedeby system
 *            host boot time).</td>
 *    </tr>
 *  </table>
 */
public interface ExecutionEnv {

    /** name of the property which hold the NO_SSL flag */
    public static final String NO_SSL = "no_ssl";
    
    /** name of the property which holds the AUTO_START flag */
    public static final String AUTO_START = "auto_start";
    
    /** name of property that signals system installed in simple mode */
    public static final String SIMPLE = "simple";
    
    
    /**
     *  Get the name of the Hedeby system
     * @return the name of the system
     */
    public String getSystemName();
    
    /**
     * Get the JNDI context - this method should only be used in SystemCommands and
     * Junit tests
     * @return the JNDI context
     * @throws com.sun.grid.grm.GrmException if the JNDI context could not be
     *            created
     */
    public Context getContext() throws GrmException;
    /**
     * Get CommandService for executing commands in system. CommandService is different
     * for real system and for dummy one.
     * @return CommandService instance for executing UI commands
     */
    public CommandService getCommandService();

    /**
     * Get the resource id factory
     * @return the resource id factory
     */
    public FactoryWithException<ResourceId,ResourceIdException> getResourceIdFactory();
    
    /**
     * Get connection url to CS component
     * @return JMXServiceURL to CS
     */ 
    public JMXServiceURL getCSURL();
    
    /**
     * Get the security context 
     * @return the security context
     */
    public SecurityContext getSecurityContext();
    
    /** 
     *  Get the CS host name.
     *
     *  @return the CS hostname
     */
    public Hostname getCSHost();
    
    /**
     * Get the CS port
     * @return the CS port
     */
    public int getCSPort();
    
    
    /**
     * Get the Host and port of CS
     * @return Hand and port of CS
     */
    public HostAndPort getCSInfo();
    
    /**
     * Get the directory with the Hedeby distribution
     * @return directory with the hedeby distribution
     */
    public File getDistDir();
    
    /**
     * Getthe local spool directory if the Hedeby system
     * @return the local spool directory
     */
    public File getLocalSpoolDir();
    
    /**
     *  This method returns an unmodifiable map of additional
     *  properties of the Hedeby system
     *  @return unmodifiable map of additional properties
     */
    public Map<String,?> getProperties();
    
}
