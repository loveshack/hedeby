/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.table;

import javax.swing.table.AbstractTableModel;

/**
 * All local cli table models should extend this abstract class. It adds 2 methods
 * that need to be implemented for machine output of the table.
 */
public abstract class AbstractDefaultTableModel extends AbstractTableModel {
    
    private boolean usedAllColumn = false;
    private boolean usedDebugColumn = false;
    private boolean allColumnExist = false;
    private boolean debugColumnExist = false;
       
    /**
     * Default constructor
     */
    public AbstractDefaultTableModel() {
        
    }
    
    /**
     * Constructor with table model as argument from with information about
     * additional columns are obtained.
     * @param model table model of the wrapped table model
     */
    public AbstractDefaultTableModel(AbstractDefaultTableModel model) {
        this(model.isAllColumnExist(),model.isDebugColumnExist(),model.isUsedAllColumn(),model.isUsedDebugColumn());
    }
    
    /**
     * Contructor with arguments that determine if addtional columns are used
     * @param usedAllColumn flag if all column is used
     * @param usedDebugColumn flag if debug column is used
     * @param allColumnExist flag that determines if all column was used in cli command
     * @param debugColumnExist flag that determines if debug column was used in cli command
     */
    public AbstractDefaultTableModel(boolean allColumnExist, boolean debugColumnExist, boolean usedAllColumn, boolean usedDebugColumn) {
        this.allColumnExist = allColumnExist;
        this.debugColumnExist = debugColumnExist;
        setUsedAllColumn(usedAllColumn);
        setUsedDebugColumn(usedDebugColumn);
    }
    
    /**
     * Determines if all column was used in cli command
     * @return true if all column was used in cli command
     */
    public boolean isAllColumnExist() {
        return allColumnExist;
    }
    
    /**
     * Determines if debug column was used in cli command
     * @return true if debug column was used in cli command
     */
    public boolean isDebugColumnExist() {
        return debugColumnExist;
    }
   
    /**
     * Sets the flag that determine if all column was used in cli
     * only when all column exists in the table
     * @param usedAllColumn is used in cli 
     */
    public void setUsedAllColumn(boolean usedAllColumn) {
        if (isAllColumnExist()) {
            this.usedAllColumn = usedAllColumn;
        }
    }

    /**
     * Sets the flag that determine if debug column was used in cli
     * only when debug column exists in the table
     * @param usedDebugColumn is used in cli
     */
    public void setUsedDebugColumn(boolean usedDebugColumn) {
        if(isDebugColumnExist()) {
            this.usedDebugColumn = usedDebugColumn;
        }
    }
    
    /**
     * Gets information if additional all column is used
     * only when all column exists in the table
     * @return true if additional all column is used
     */
    public boolean isUsedAllColumn() {
        if (isAllColumnExist()) {
            return usedAllColumn;
        } else {
            return false;
        }
    }
    
    /**
     * Gets information if additional debug column is used
     * only when debug column exists in the table
     * @return true if additional debug column is used
     */
    public boolean isUsedDebugColumn() {
        if (isDebugColumnExist()) {
            return usedDebugColumn;
        } else {
            return false;
        }
    }
    
    /**
     * get the value for column that contains addiotnal content
     * @param row index of the row
     * @return String representation of additional content
     */
    public String getValueForAllColumn(int row) {
        return "";
    }
    
    /**
     * get the value for column that contains debug content
     * @param row index of the row
     * @return String representation of debug content
     */
    public String getValueForDebugColumn(int row) {
        return "";
    }    
}
