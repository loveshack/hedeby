/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.table;

import com.sun.grid.grm.GrmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This Table model should be used when sorting of table rows is required
 */
public class SortedTableModel extends AbstractDefaultTableModel implements Comparator<Integer> {

    private AbstractDefaultTableModel tm;
    private List<Integer> sortedLines;
    private List<String> columnSortOrder;
    private final static long serialVersionUID = -2007113001L;
    
    private final static String BUNDLE = "com.sun.grid.grm.cli.table.messages";
    
    /**
     * Constructor of this sorted table model
     * @param model original table model
     * @param columnSortOrder list of sort order of column names 
     * @throws GrmException when unknown column was found
     */
    public SortedTableModel(AbstractDefaultTableModel model, List<String> columnSortOrder) throws GrmException {
        super(model);
        tm = model;
        this.columnSortOrder = columnSortOrder;
        if (columnSortOrder != null) {
            for (String colName : columnSortOrder) {
                int colIndex = findColumn(colName);
                if (colIndex < 0) {
                    throw new GrmException("SortedTableModel.uc", BUNDLE, colName);
                }
            }
        }
        if (sortedLines == null) {
            int rowCount = tm.getRowCount();
            sortedLines = new ArrayList<Integer>(tm.getRowCount());
            for (int i = 0; i < rowCount; i++) {
                sortedLines.add(i);
            }
            Collections.sort(sortedLines, this);
        }      
    }
    
    @Override
    public int findColumn(String columnName) {
        return tm.findColumn(columnName);
    }

    public int getRowCount() {
        return tm.getRowCount();
    }

    public int getColumnCount() {
        return tm.getColumnCount();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return tm.getValueAt(sortedLines.get(rowIndex), columnIndex);
    }

    public int compare(Integer line1, Integer line2) {
        int ret = 0;
        int colCount = getColumnCount();
        if (columnSortOrder == null) {
            for (int col = 0; col < colCount && ret == 0; col++) {
                if (Comparable.class.isAssignableFrom(getColumnClass(col))) {
                    Comparable c1 = (Comparable) tm.getValueAt(line1, col);
                    Comparable c2 = (Comparable) tm.getValueAt(line2, col);
                    ret = compare(c1, c2);
                }
            }
        } else {
            for (String col : columnSortOrder) {
                int index = findColumn(col);
                if (Comparable.class.isAssignableFrom(getColumnClass(index))) {
                    Comparable c1 = (Comparable) tm.getValueAt(line1, index);
                    Comparable c2 = (Comparable) tm.getValueAt(line2, index);
                    ret = compare(c1, c2);
                    if (ret != 0) {
                        break;
                    }
                }
            }
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    private int compare(Comparable c1, Comparable c2) {
        if (c1 == null) {
            if (c2 != null) {
                return -1;
            } else {
                return 0;
            }
        } else {
            return c1.compareTo(c2);
        }
    }

    @Override
    public String getColumnName(int column) {
        return tm.getColumnName(column);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return tm.getColumnClass(columnIndex);
    }

    @Override
    public String getValueForAllColumn(int row) {
        return tm.getValueForAllColumn(sortedLines.get(row));
    }

    @Override
    public String getValueForDebugColumn(int row) {
        return tm.getValueForDebugColumn(sortedLines.get(row));
    }
    
    
}
