/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.table;

import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.util.EventListenerSupport;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import javax.swing.table.TableModel;

/**
 *  Component for formatting cli output as table. 
 *  This class contains implemented common functionality for tables.
 *  HumanReadableTable and MachineReadableTable extend this class.
 *
 *  <p>Each table uses a TableModel to get the printed data.<p>
 *
 *  <h3>Conversion</h3>
 *
 *  <p>This class uses the <code>String.format</code> method to convert the value
 *     of a cell into a String. Per default the column class defines the format
 *     column:</p>
 *
 *  <table>
 *    <tr><th>Column class</th>              <th>Format conversion</th></tr>
 *    <tr><td>null or String<td>             <td>%s</td></tr>
 *    <tr><td>Integer, Byte, Long, Short</td><td>%d</td></tr>
 *    <tr><td>Double, Float</td>             <td>%f</td></tr>
 *    <tr><td>Date</td>                      <td>%t</td></tr>
 *    <tr><td>Otherwise</td>                 <td>%s</td></tr>
 *  </table>
 *
 *  <p>It is possible to override the formatting. E.g. for hexadecimal formating
 *     of a column you can use the setConversion method:</p>
 *
 *  <pre>
 *       TableModel tm = ...
 *       Table table = new Table(tm);
 *       table.setConversion(1,'x');
 *  </pre>
 *
 *  <h3>Column width</h3>
 *
 *  <p>Per default the width of a column is the maximum length of the string of
 *     any cell value. This automatic calculation of the column width is an expensive
 *     operation which can be turned off with the <code>autoCalcColumnWidth</code> property.</p>
 *  <p>If <code>autoCalcColumnWidth</code> is switched off and a column has no defined
 *     width the default column width is used (per default 50, can be modified with
 *     the <code>setDefaultColumnWidth</code> method).</p>
 *  <p>With the method setColumnWidth it is possible to define a fixed width for a column.</p>
 *  <p><b>Example:</b></p>
 *  <pre>
 *       TableModel tm = ...
 *       Table table = new Table(tm);
 *       table.setAutoCalcColumnWidth(false);
 *       table.setColumnWidth(0,10);
 * </pre>
 *
 * <h3>Conversion flags and precision</h3>
 *
 *  <p>The formatting of the cell values is done with String.format. It is possible to
 *     define conversion flags and the precision for each column. For details about
 *     allowed conversion flags and percision please have a look at the javadoc
 *     page of the class <code>java.util.Formatter</code>.</p>
 *
 *  <p><b>Example:</b></p>
 *
 *  <pre>
 *       TableModel tm = ...
 *       Table table = new Table(tm);
 *
 *       int colIndex = 1
 *       table.setConversion(colIndex,'d');
 *       table.setColumnWidth(colIndex, 5);
 *       table.setFlags(colIndex, "-");
 *       table.setPrecision(colIndex, 2);
 *  </pre>
 *  
 */
public abstract class AbstractDefaultTable implements Table {
    
    /** default column delimiter ( ) */
    public final static char DEFAULT_COLUMN_DELIMITER = ' ';
    /** default header delimiter (-). This delimiter separate header from the rest of the table vertically */
    public final static char DEFAULT_HEADER_DELIMITER = '-';
    
    /** the table model of the table */
    protected AbstractDefaultTableModel model;
    
    private char headerDelimiter = DEFAULT_HEADER_DELIMITER;
    private char columnDelimiter = DEFAULT_COLUMN_DELIMITER;
    
    private Map<Integer,Column> columns;
    private int    defaultColumnWidth = 50;
    private boolean duplicateValuesHidden = false;
    private boolean printHeader = true;
    private boolean autoCalcColumnWidth = true;
    /*This flag determine if table format need to be calculated*/
    private boolean isDirty = true;
    protected final EventListenerSupport<TableListener> evt = EventListenerSupport.<TableListener>newSynchronInstance(TableListener.class);
   
    /**
     * Creates a new instance of Table
     * @param aModel the model of the table
     */
    public AbstractDefaultTable(AbstractDefaultTableModel aModel) {
        model = aModel;
    }
    
    /**
     * Add a table listener.
     * @param lis  the table listener
     */
    public void addTableListener(TableListener lis) {
        evt.addListener(lis);
    }
    
    /**
     * Remove a table listener.
     * @param lis  the table listener
     */
    public void removeTableListener(TableListener lis) {
        evt.removeListener(lis);
    }
    
    protected void calculateFormat() {
        if(isDirty) {
            if(isAutoCalcColumnWidth()) {
                calculateColumnWidth();
            }
            StringBuilder buf = new StringBuilder();
            int colCount = model.getColumnCount();
            for(int col = 0; col < colCount; col++) {
                Column column = getColumn(col);
                column.calculateFormat(buf);
            }
            isDirty = false;
        }
    }
    
    private void calculateColumnWidth() {
        int rowCount = model.getRowCount();
        int colCount = model.getColumnCount();
        HashMap<Integer,Column> calcColumn = new HashMap<Integer,Column>(colCount);
        HashMap<Integer,String> columnFormat = new HashMap<Integer,String>(colCount);
        StringBuilder buf = new StringBuilder();
        for(int col = 0; col < colCount; col++) {
            Column column = getColumn(col);
            if (column.width <= 0)  {
                calcColumn.put(col, column);
                column.width = Math.max(column.width, model.getColumnName(col).length());
                buf.append('%');
                buf.append(column.getConversion());
                columnFormat.put(col, buf.toString());
                buf.setLength(0);
            }
        }
        
        for(int row = 0; row < rowCount; row++) {
            for(Map.Entry<Integer,Column> entry: calcColumn.entrySet()) {
                int col = entry.getKey();
                Column column = entry.getValue();
                Object value = model.getValueAt(row, col);
                String format = columnFormat.get(col);
                String str = String.format(format, value);
                int len = 0;
                if(column.isWordWrap) {
                    StringTokenizer tokens = new StringTokenizer(str, "\n\r \t");
                    while (tokens.countTokens() > 0) {
                        String word = tokens.nextToken();
                        len = Math.max(len, word.length());
                    }
                } else {
                    len = str.length();
                }
                if(column.unit != null) {
                    len += column.unit.length();
                }
                column.width = Math.max(len, column.width);
            }
        }
    }
    
    
    /**
     * the the minimun width of a column
     * @param col    index of the column
     * @param width  the width of the column
     */
    public void setColumnWidth(int col, int width) {
        isDirty=true;
        getColumn(col).width = width;
    }
    
    /**
     * Enable or disable the auto word wrap option for the specified column
     * @param col affected column number
     * @param value boolean value for auto word wrap mode
     */
    public void setAutoWordWrap(int col, boolean value) {
        isDirty=true;
        getColumn(col).isWordWrap = value;
    }
    
    /**
     * return the state of the auto word wrap flag for the specified column
     * @param col affected column number
     * @return boolean value for auto word wrap mode
     */
    public boolean isAutoWordWrap(int col) {
        return getColumn(col).isWordWrap;
    }
    
    protected Column getColumn(int col) {
        if(columns == null) {
            columns = new HashMap<Integer,Column>(model.getColumnCount());
        }
        Column ret = columns.get(col);
        if(ret == null) {
            ret = new Column(col);
            columns.put(col,ret);
        }
        return ret;
    }
    
    /**
     * Get the width of a column.
     * @param col index of the column
     * @return  the width of the column
     */
    public int getColumnWidth(int col) {
        return getColumn(col).getWidth();
    }
    
    /**
     * Get the width of the whole table. The method will
     * first calculate the table format.
     * @return width of the table in characters
     */
    public int getTableWidth() {
        calculateFormat();
        int tableWidth = 0;
        for (int i = 0; i < model.getColumnCount(); i++) {
           tableWidth = tableWidth + getColumnWidth(i);
        }
        return tableWidth;
    }
    
    /**
     * get the conversion of a column
     * @param columnName name of the column
     * @return the conversion of a column
     */
    public char getConversion(String columnName) {
        return getColumn(model.findColumn(columnName)).getConversion();
    }
    
    /**
     * Set the conversion of a column
     * @param columnName  the column name
     * @param conversion the conversion
     */
    public void setConversion(String columnName, char conversion) {
        isDirty=true;
        getColumn(model.findColumn(columnName)).conversion = conversion;
    }
    
    /**
     * Get the unit of the column
     * @param columnName the column name
     * @return the unit of the column
     */
    public String getUnit(String columnName) {
        return getColumn(model.findColumn(columnName)).unit;
    }

    /**
     * Set the unit of the column
     * 
     * @param columnName the column name
     * @param unit  the unit of the column
     */
    public void setUnit(String columnName, String unit) {
        isDirty = true;
        getColumn(model.findColumn(columnName)).unit = unit;
    }
    
    /**
     * Get the conversion flags of a column
     * @param columnName the column index
     * @return the conversion flags of a column
     */
    public String getFlags(String col) {
        return getColumn(model.findColumn(col)).getFlags();
    }
    
    /**
     * Set the conversion flags of a column
     * 
     * @param flags flag value
     * @param columnName the column name
     */
    public void setFlags(String columnName, String flags) {
        isDirty=true;
        getColumn(model.findColumn(columnName)).flags = flags;
    }
    
    /**
     *  Get the precision of a column
     *  @param  columnName  the column name
     *  @return the precision
     */
    public int getPrecision(String columnName) {
        return getColumn(model.findColumn(columnName)).precision;
    }
    
    /**
     *   Set the precision of a column
     *   @param columnName the column index
     *   @param precision the precision;
     */
    public void setPrecision(String columnName, int precision) {
        isDirty=true;
        getColumn(model.findColumn(columnName)).precision = precision;
    }
            
    /**
     *  Print the table.
     *  @param cli the cli object
     */
    public void print(AbstractCli cli) {
        print(cli.out().getPrintWriter());
    }
     
    /**
     * Get current header delimiter character
     * @return char
     */
    public char getHeaderDelimiter() {
        return headerDelimiter;
    }
    
    /**
     * Set header delimiter character
     * @param headerDelimiter new header delimiter
     */
    public void setHeaderDelimiter(char headerDelimiter) {
        isDirty=true;
        this.headerDelimiter = headerDelimiter;
    }
    
    /**
     * Get current column delimiter character
     * @return delimiter
     */
    public char getColumnDelimiter() {
        return columnDelimiter;
    }
    
    /**
     * Set column delimiter character
     * @param columnDelimiter new column delimiter
     */
    public void setColumnDelimiter(char columnDelimiter) {
        isDirty=true;
        this.columnDelimiter = columnDelimiter;
    }
    
    /**
     * Get value of duplicate value hide option
     * @return boolean value
     */
    public boolean isDuplicateValuesHidden() {
        return duplicateValuesHidden;
    }
    
    /**
     * Set value of duplicate value hide option
     * @param duplicateValuesHidden value
     */
    public void setDuplicateValuesHidden(boolean duplicateValuesHidden) {
        isDirty=true;
        this.duplicateValuesHidden = duplicateValuesHidden;
    }
    
    /**
     * Get value of print header option 
     * @return true or false
     */
    public boolean isPrintHeader() {
        return printHeader;
    }
    
    /**
     * Set value of print header option 
     * @param printHeader true enables headers, false disables printing the table headers.
     */
    public void setPrintHeader(boolean printHeader) {
        isDirty=true;
        this.printHeader = printHeader;
    }
    
    /**
     * Return the value of auto column width calculation mode
     * @return if true the table column width is calculated automatically
     */
    public boolean isAutoCalcColumnWidth() {
        return autoCalcColumnWidth;
    }
    
    /**
     * Set the value of auto column width calculation mode
     * @param autoCalcColumnWidth true enables this mode
     */
    public void setAutoCalcColumnWidth(boolean autoCalcColumnWidth) {
        isDirty=true;
        this.autoCalcColumnWidth = autoCalcColumnWidth;
    }
    
    protected class Column {
        private boolean isWordWrap = false;
        private int width = -1;
        private Character conversion;
        private String flags = "-";
        private String pattern;
        private int precision = -1;
        private String unit;
        
        private final int index;
        
        public Column(int aIndex) {
            index = aIndex;
        }
        
        public int getWidth() {
            if(width <= 0) {
                return defaultColumnWidth;
            } else {
                return width;
            }
        }
        
        public String getFlags() {
            if(flags == null) {
                return "";
            }
            return flags;
        }
        
        public String getUnit() {
            return unit;
        }
        
        public String getPattern() {
            return pattern;
        }
        
        private char getConversion() {
            if(conversion != null) {
                return conversion;
            }
            Class cls = model.getColumnClass(index);
            if (cls == null || String.class.equals(cls)) {
                return 's';
            }
            if (Integer.class.equals(cls) || Byte.class.equals(cls) || Long.class.equals(cls)
            || Short.class.isAssignableFrom(cls)) {
                return 'd';
            }
            if (Double.class.equals(cls) || Float.class.equals(cls)) {
                return 'f';
            }
            if(Date.class.equals(cls)) {
                return 't';
            }
            return 's';
        }
        
        private void calculateFormat(StringBuilder buf) {
            buf.append('%');
            buf.append(getFlags());
            int currentWidth = getWidth();
            if(unit != null) {
                width = Math.max(3, currentWidth - unit.length());
            }
            buf.append(width);
            if(precision >= 0) {
                buf.append('.');
                buf.append(precision);
            }
            buf.append(getConversion());
            pattern = buf.toString();         
            buf.setLength(0);
        }
        
    }

    /**
     * Get the underlying table model on which table operates.
     * @return the table model
     */
    public TableModel getTableModel() {
        return model;
    }
}
