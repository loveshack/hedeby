/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.table;

import com.sun.grid.grm.cli.AbstractCli;
import java.io.PrintWriter;
import javax.swing.table.TableModel;

/**
 * Table defines a contract which has to be implemented by a class that provides
 * table-like output for ui.
 * 
 */
public interface Table {

    /**
     * Add a table listener.
     * @param lis  the table listener
     */
    void addTableListener(TableListener lis);

    /**
     * Get current column delimiter character
     * @return char
     */
    char getColumnDelimiter();

    /**
     * Get the width of a column.
     * @param col index of the column
     * @return  the width of the column
     */
    int getColumnWidth(int col);

    /**
     * Get the unit of the column
     * @param  columnName  the column index
     * @return the unit of the column or null if no unit is set for this column
     */
    String getUnit(String columnName);
    
    /**
     * get the conversion of a column
     * @param columnName index of the column
     * @return the conversion of a column
     */
    char getConversion(String columnName);

    /**
     * Get the conversion flags of a column
     * @param columnName the column index
     * @return the conversion flags of a column
     */
    String getFlags(String columnName);

    /**
     * Get current header delimiter character
     * @return char
     */
    char getHeaderDelimiter();

    /**
     * Get the precision of a column
     * @param  columnName  the column index
     * @return the precision
     */
    int getPrecision(String columnName);

    /**
     * Get the width of the whole table. The method will
     * first calculate the table format.
     * @return width of the table in characters
     */
    int getTableWidth();

    /**
     * Return the value of auto column width calculation mode
     * @return if true the table column width is calculated automatically
     */
    boolean isAutoCalcColumnWidth();

    /**
     * return the state of the auto word wrap flag for the specified column
     * @param col affected column number
     * @return boolean value for auto word wrap mode
     */
    boolean isAutoWordWrap(int col);

    /**
     * Get value of duplicate value hide option
     * @return boolean value
     */
    boolean isDuplicateValuesHidden();

    /**
     * Get value of print header option
     * @return true or false
     */
    boolean isPrintHeader();

    /**
     * Print the table.
     * @param cli the cli object
     */
    void print(AbstractCli cli);

    /**
     * Print the table directly into a PrintWriter
     * @param pw the PrintWriter
     */
    public void print(PrintWriter pw);

    /**
     * Remove a table listener.
     * @param lis  the table listener
     */
    void removeTableListener(TableListener lis);

    /**
     * Set the value of auto column width calculation mode
     * @param autoCalcColumnWidth true enables this mode
     */
    void setAutoCalcColumnWidth(boolean autoCalcColumnWidth);

    /**
     * Enable or disable the auto word wrap option for the specified column
     * @param col affected column number
     * @param value boolean value for auto word wrap mode
     */
    void setAutoWordWrap(int col, boolean value);

    /**
     * Set column delimiter character
     * @param columnDelimiter new column delimiter
     */
    void setColumnDelimiter(char columnDelimiter);

    /**
     * the the minimun width of a column
     * @param col    index of the column
     * @param width  the width of the column
     */
    void setColumnWidth(int col, int width);

    /**
     * Set the conversion of a column
     * @param columnName  the column index
     * @param conversion the conversion
     */
    void setConversion(String columnName, char conversion);
    
    /**
     * Set the unit of a column
     * @param columnName  the column index
     * @param unit the unit of the column
     */
    void setUnit(String columnName, String unit);

    /**
     * Set value of duplicate value hide option
     * @param duplicateValuesHidden value
     */
    void setDuplicateValuesHidden(boolean duplicateValuesHidden);

    /**
     * Set the conversion flags of a column
     *
     * @param flags flag value
     * @param columnName the column index
     */
    void setFlags(String columnName, String flags);

    /**
     * Set header delimiter character
     * @param headerDelimiter new header delimiter
     */
    void setHeaderDelimiter(char headerDelimiter);

    /**
     * Set the precision of a column
     * @param columnName the column index
     * @param precision the precision;
     */
    void setPrecision(String columnName, int precision);

    /**
     * Set value of print header option
     * @param printHeader true enables headers, false disables printing the table headers.
     */
    void setPrintHeader(boolean printHeader);

    /**
     * Get the underlying table model on which table operates.
     * @return the table model
     */
    TableModel getTableModel();
}
