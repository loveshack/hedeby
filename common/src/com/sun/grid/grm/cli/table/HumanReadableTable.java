/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.table;

import com.sun.grid.grm.cli.AbstractCli;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/*
 * This class prints table in a human readable format.
 *
 */
public class HumanReadableTable extends AbstractDefaultTable {
    
    /**
     * Constructor of the human readable table model
     * @param tm original table model
     * @param cli cli object
     */
    public HumanReadableTable(AbstractDefaultTableModel tm, AbstractCli cli) {
        super(tm);
        if (model.isUsedAllColumn()) {
            this.addFullOutputListner(cli);
        }
        if (model.isUsedDebugColumn()) {
            this.addDebugListner(cli);
        }
    }
    
    /**
     *   Print the table
     *   @param pw the print writer
     */
    public void print(PrintWriter pw) {
        calculateFormat();
        if(isPrintHeader()) {
            printHeader(pw);
        }
        int rowCount = model.getRowCount();
        for(int i = 0; i < rowCount; i++) {
            evt.getProxy().printingRow(i);
            printRow(i, pw);
            evt.getProxy().rowPrinted(i);
        }
    }
    
    private void printHeader(PrintWriter pw) {
        int colCount = model.getColumnCount();
        StringBuilder buf = new StringBuilder();
        int lineWidth = 0;
        for (int col = 0; col < colCount; col++) {
            if(col > 0) {
                pw.print(getColumnDelimiter());
                lineWidth++;
            }
            int width = getColumnWidth(col);
            lineWidth += width;
            buf.append("%-");
            buf.append(width);
            buf.append('s');
            pw.printf(buf.toString(), model.getColumnName(col));
            buf.setLength(0);
        }
        pw.println();
        for(int i = 0; i < lineWidth; i++) {
            pw.print(getHeaderDelimiter());
        }
        pw.println();
    }
    
    private Object [] lastValues = null;
    
    private void printRow(int row, PrintWriter pw) {
        if (isAutoWordWrap(row)) {
            printRowWithAutoWrap(row, pw);
        } else {
            printRowNoAutoWrap(row, pw);
        }
    }
    
    private void printRowNoAutoWrap(int row, PrintWriter pw) {
        int colCount = model.getColumnCount();
        
        if(isDuplicateValuesHidden()) {
            boolean lastValueChanged=false;
            if(lastValues == null) {
                lastValues = new Object[colCount];
                lastValueChanged = true;
            }

            for(int col = 0; col < colCount; col++) {
                if (col > 0) {
                    pw.print(getColumnDelimiter());
                }
                Object value = model.getValueAt(row, col);
                if(!lastValueChanged && value.equals(lastValues[col])) {
                    int colWidth = getColumnWidth(col);
                    for(int i = 0; i < colWidth; i++) {
                        pw.print(' ');
                    }
                } else {
                    Column column = getColumn(col);
                    pw.printf(column.getPattern(), value);
                    lastValues[col] = value;
                    if(column.getUnit() != null) {
                        pw.print(column.getUnit());
                    }
                    lastValueChanged = true;
                }
            }
        } else {
            for(int col = 0; col < colCount; col++) {
                if (col > 0) {
                    pw.print(getColumnDelimiter());
                }
                Object value = model.getValueAt(row, col);
                Column column = getColumn(col);
                pw.printf(column.getPattern(), value);
                if (column.getUnit() != null) {
                    pw.print(column.getUnit());
                }
            }
        }
        pw.println();
    }
    
    /**
     * Print a row of the table
     * @param row row number
     * @param pw PrintWriter instance used for printing
     */
    private void printRowWithAutoWrap(int row, PrintWriter pw) {
        int colCount = model.getColumnCount();
        boolean lastValueChanged=false;
        
        // create map for column list lines
        Map<Integer,List<String>> linesMap = new HashMap<Integer,List<String>>(colCount);
        for (int col = 0; col < colCount; col++) {
            linesMap.put(col, new LinkedList<String>());
        }
        
        // init last line cache
        if(lastValues == null) {
            lastValues = new Object[colCount];
            lastValueChanged = true;
        }
        StringBuilder sb = new StringBuilder();
        // first we write each column line into lists
        for (int col = 0; col < colCount; col++) {
            Object value = model.getValueAt(row, col);
            int colWidth = getColumnWidth(col);

            if(!lastValueChanged && value.equals(lastValues[col]) && isDuplicateValuesHidden()) {
                //linesMap.get(col).add(null);
            } else {
                Column column = getColumn(col);
                String str = String.format(column.getPattern(), value);
                if(column.getUnit() != null) {
                    str += column.getUnit();
                }
                
                if (isAutoWordWrap(col) == true && colWidth > 0) {
                    
                    StringTokenizer tokens = new StringTokenizer(str, "\n\r \t");
                    sb.setLength(0);
                    while (tokens.countTokens() > 0) {
                        String word = tokens.nextToken();
                        
                        if (word.length() > colWidth) {
                            // the word is to long for the column width
                            
                            // first flash current line
                            if (sb.length() > 0) {
                                linesMap.get(col).add(sb.toString());
                                sb.setLength(0);
                            }
                            // now break the word
                            int start = 0;
                            int end = colWidth;
                            while (end <= word.length()) {
                                String line = word.substring(start, end);
                                linesMap.get(col).add(line);
                                start = start + colWidth;
                                end = end + colWidth;
                            }
                            String rest = word.substring(start, word.length());
                            sb.append(rest);
                        } else {
                            if (sb.length() + word.length() < colWidth) {
                                // word fits into culumn line
                                if (sb.length() > 0) {
                                    sb.append(' ');
                                }
                                sb.append(word);
                            } else{
                                // word is to long for this line, wraparound ...
                                linesMap.get(col).add(sb.toString());
                                sb.setLength(0);
                                sb.append(word);
                            }
                        }
                    }
                    // add the last StringBuilder
                    if (sb.length() > 0) {
                       linesMap.get(col).add(sb.toString());
                    }
                } else {
                    linesMap.get(col).add(str);
                }
                lastValues[col] = value;
                lastValueChanged = true;
            }
        }
        
        
        // print out row
        int currentLine = 0;
        boolean printNextLine = false;
        do {
            printNextLine = false;
            for (int col = 0; col < colCount; col++) {
                if (col > 0) {
                    pw.print(getColumnDelimiter());
                }
                int colWidth = getColumnWidth(col);
                int colLines = linesMap.get(col).size();
                if (colLines > currentLine) {
                    // we have to print currentLine
                    String lineText = linesMap.get(col).get(currentLine);
                    printColumn(pw, lineText, colWidth);
                    try {
                        linesMap.get(col).get(currentLine + 1);
                        printNextLine = true;
                    } catch (IndexOutOfBoundsException e) {
                    }
                } else {
                    // nothing to print
                    printColumn(pw, "",colWidth);
                }
            }
            pw.println();
            currentLine++;
        } while (printNextLine == true);
    }
    
    
    private void printColumn(PrintWriter pw, String colText, int colWidth) {
        if (colText == null) {
            colText = "";
        }
        pw.print(colText);
        if (colWidth > 0) {
            int missingChars = colWidth - colText.length();
            for (int i = 0; i < missingChars; i++) {
                pw.print(' ');
            }
        } 
    }
    
    private void addDebugListner(AbstractCli cli) {
        this.addTableListener(new DebugListener(cli));
    }
    
    private void addFullOutputListner(AbstractCli cli) {
        this.addTableListener(new FullDescListener(cli));
    }
    
    private class DebugListener implements TableListener {

        private final AbstractCli cli;

        private DebugListener(AbstractCli cli) {            
            this.cli = cli;
        }

        public void printingRow(int row) {
        // do nothing
        }

        public void rowPrinted(int row) {
            cli.err().printDirectly(model.getValueForDebugColumn(row));
        }
    }
    
    private class FullDescListener implements TableListener {

        private final AbstractCli cli;

        private FullDescListener(AbstractCli cli) {
            this.cli = cli;
        }

        public void printingRow(int row) {
        // do nothing
        }

        public void rowPrinted(int row) {
            cli.out().printDirectly(model.getValueForAllColumn(row));
        }
    }
    
    
}
