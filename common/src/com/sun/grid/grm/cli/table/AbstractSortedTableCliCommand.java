/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.table;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.*;
import java.util.List;

/**
 * Abstract base class for all cli command which displays a sorted table.
 */
@CliCommandDescriptor(name = "AbstractSortedTableCliCommand", category = CliCategory.ADMINISTRATION, bundle = "com.sun.grid.grm.cli.client")
public abstract class AbstractSortedTableCliCommand extends AbstractTableCliCommand {

    @CliOptionDescriptor(name = "AbstractSortedTableCliCommand.columnSortOrder", numberOfOperands = 1, required = false, list = true)
    private List<String> columnSortOrder;    
    private List<String> defaultColumnSortOrder;
    
    /**
     * This methods creates a table which uses a table model which
     * sorts the rows of the table model.
     *
     * @param model the table model
     * @throws com.sun.grid.grm.GrmException if the sort order contains an unknown column
     * @return the table object
     */
    @Override
    protected Table createTable(AbstractDefaultTableModel model, AbstractCli cli) throws GrmException {       
        List<String> sortOrder = columnSortOrder != null ? columnSortOrder : defaultColumnSortOrder;   
        SortedTableModel tm = new SortedTableModel(model, sortOrder);
        return super.createTable(tm, cli);
    }
    
    /**
     * Sets the default sort order of the table columns
     * @param columnSortOrder list of column names strings
     */
    public void setDeafultColumnSortOrder(List<String> columnSortOrder) {
        this.defaultColumnSortOrder = columnSortOrder;
    }
}
