/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.table;

import java.io.PrintWriter;

/**
 * Special table used for printing selected columns from a table. The difference
 * between this table and HumanReadable class table is that all debug output and all 
 * additional description output is printed as addiotional columns all and debug.
 * To use additional columns "all" or "debug" has  to be specify as selected 
 * column.
 */
public class MachineReadableTable extends AbstractDefaultTable {
    
    private final static char DEFAULT_MR_COLUMN_DELIMITER = '|';
    
    /**
     * Constructor of the machine readable table
     * @param tm table model for this table
     */
    public MachineReadableTable(AbstractDefaultTableModel tm) {
        super(tm);
        super.setColumnDelimiter(DEFAULT_MR_COLUMN_DELIMITER);        
    }

    @Override
    public boolean isPrintHeader() {
        return false;
    }

    @Override
    public boolean isAutoWordWrap(int col) {
        return false;
    }

    @Override
    public boolean isDuplicateValuesHidden() {
        return false;
    }

    @Override
    public boolean isAutoCalcColumnWidth() {
        return true;
    }

    @Override
    public void setHeaderDelimiter(char headerDelimiter) {
        throw new UnsupportedOperationException("SetHeaderDelimiter method not allowed in machine readable table");
    }

    @Override
    public void setPrintHeader(boolean printHeader) {
        throw new UnsupportedOperationException("SetPrintHeader not allowed in machine readable table");
    }

    @Override
    public void setDuplicateValuesHidden(boolean duplicateValuesHidden) {
        throw new UnsupportedOperationException("SetDuplicateValuesHidden not allowed in machine readable table");
    }

    @Override
    public void setAutoCalcColumnWidth(boolean autoCalcColumnWidth) {
        throw new UnsupportedOperationException("SetAutoCalcColumnWidth not allowed in machine readable table");
    }
    
    
        
    /**
     *   Print the table to a given PrintWriter
     *   @param pw the print writer
     */
    @Override
    public void print(PrintWriter pw) {
        calculateFormat();
        int rowCount = model.getRowCount();
        for(int i = 0; i < rowCount; i++) {
            evt.getProxy().printingRow(i);
            printRow(i, pw);
            evt.getProxy().rowPrinted(i);
        }
    }

    private void printRow(int row, PrintWriter pw) {
        int colCount = model.getColumnCount();
        for(int col = 0; col < colCount; col++) {
            if (col > 0) {
                pw.print(getColumnDelimiter());
            }
            Object value = model.getValueAt(row, col);
            Column column = getColumn(col); 
            pw.print(escapeSpecialCharacters(String.format(column.getPattern(),value).trim()));
            if (column.getUnit() != null) {
                pw.print(escapeSpecialCharacters(column.getUnit()));
            }
        }
        pw.println();
    }
    
    /*
     * This method does escaping of the special characters for machine 
     * readable output. The following characters are escaped : \n, \r, \ and
     * column delimiter. 
     */
    private String escapeSpecialCharacters(Object content) {
        char[] tab = content.toString().toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char i : tab) {
            if (i == getColumnDelimiter() || i == '\\') {
                sb.append("\\");
            }
            if (i == '\n') {
                sb.append("\\n");
            } else if (i == '\r') {
                sb.append("\\r");
            } else {
                sb.append(i);
            }
        }
        return sb.toString();
    }
}
