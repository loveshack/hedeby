/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.table;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.*;
import java.util.List;

/**
 *  Abstract base class for all cli commands which prints out a table.
 */
@CliCommandDescriptor(
    name = "AbstractTableCommand", 
    category = CliCategory.ADMINISTRATION,
    bundle = "com.sun.grid.grm.cli.client")
public abstract class AbstractTableCliCommand extends AbstractCliCommand {
    
    /* We use here \r because is highly unlikly that user will choose \r caret return char
     * as column delimiter. */
    @CliOptionDescriptor(name = "AbstractTableCommand.colDel", numberOfOperands = 1, required = false)
    private char colDel = '\r';
    
    @CliOptionDescriptor(name = "AbstractTableCommand.headerDelimiter", numberOfOperands = 1, required = false)
    private char headerDelimiter = '-';
    
    @CliOptionDescriptor(name = "AbstractTableCommand.duplicateValuesDisplayed", numberOfOperands = 0, required = false)
    private boolean duplicateValuesDisplayed;
    
    @CliOptionDescriptor(name = "AbstractTableCommand.noHeader", numberOfOperands = 0, required = false)
    private boolean noHeader;
    
    @CliOptionDescriptor(name = "AbstractTableCommand.outputColumn", numberOfOperands = 1, required = false, list = true)
    private List<String> outputColumns;
    
    
    
    /**
     * Return information if column delimiter was set by the user in cli
     * @return true if delimiter was set by user in cli, false otherwise and it 
     * means that default delimiter is going to be used.
     */
    public boolean isColumnDelimiterSet() {
        return colDel != '\r';
    }
    
    /**
     * Gets list of selected columns from cli
     * @return list of strings of selected columns names
     */
    public List<String> getSelectedColumns() {
        return outputColumns;
    }
    /**
     *  Get the column delimiter
     *  @return the column delimiter
     */
    public char getColDelimiter() {
        return colDel;
    }
    
    /**
     * Determine if output column switch was used
     * @return true if output column swithc was used
     */
    public boolean isSetMachineReadableOutput() {
         return outputColumns != null;
    }
    /**
     *  Should duplicate values be displayed
     *  @return <code>true</code> if duplicate values should be displayed
     */
    public boolean isDuplicateValuesDisplayed() {
        return duplicateValuesDisplayed;
    }
    
    /**
     *  Create the table which is initialized with the formating
     *  options from the cli.
     *
     *  @param model the model for the table
     *  @return the table
     */
    protected Table createTable(AbstractDefaultTableModel model, AbstractCli cli) throws GrmException {
        Table ret = null;
        if (isSetMachineReadableOutput()) {          
           ret = new MachineReadableTable(new ColumnSelectTableModel(model, getSelectedColumns()));          
        } else {
            ret = createHumanReadableTable(model, cli);
        } 
        if (isColumnDelimiterSet()) {
            ret.setColumnDelimiter(getColDelimiter());
        }
        return ret;
    }   
    
    private Table createHumanReadableTable(AbstractDefaultTableModel model,AbstractCli cli) {
        Table ret = new HumanReadableTable(model, cli);
        ret.setDuplicateValuesHidden(!duplicateValuesDisplayed);
        ret.setHeaderDelimiter(headerDelimiter);
        ret.setPrintHeader(!noHeader);
        return ret;
    }
}
