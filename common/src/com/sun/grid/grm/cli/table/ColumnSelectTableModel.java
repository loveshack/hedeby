/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.table;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.I18NManager;
import java.util.List;

/**
 * Create table model using original table module from cli and make 
 * the format machine readable. New table model is created.
 */
public class ColumnSelectTableModel extends AbstractDefaultTableModel {

    private final static String BUNDLE = "com.sun.grid.grm.cli.table.messages";
    
    private AbstractDefaultTableModel model;
    private List<String> columns;
    private int[] columnIndexes;     
    private final String ALL_COLUMN_NAME = I18NManager.formatMessage("ColumnSelectTableModel.all", BUNDLE);
    private final String DEBUG_COLUMN_NAME = I18NManager.formatMessage("ColumnSelectTableModel.debug", BUNDLE);
    
    /**
     * Constructor of this table model
     * @param model original table model
     * @param columns list of selected columns
     * @throws com.sun.grid.grm.GrmException when unknown column was found
     */
    public ColumnSelectTableModel(AbstractDefaultTableModel model, List<String> columns) throws GrmException {
        super(model);
        this.model = model;
        this.columns = columns;
        columnIndexes = new int[columns.size()];
        checkColumnNames();                        
    }

    public int getRowCount() {
        return model.getRowCount();
    }
    
    public int getColumnCount() {
        return columns.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columns.get(columnIndex).equals(ALL_COLUMN_NAME)) {
            return model.getValueForAllColumn(rowIndex);
        }
        if (columns.get(columnIndex).equals(DEBUG_COLUMN_NAME)) {
            return model.getValueForDebugColumn(rowIndex);
        }
        return model.getValueAt(rowIndex, columnIndexes[columnIndex]);
    }

    private void checkColumnNames() throws GrmException {
        for (int i = 0; i < columns.size(); i++) {           
            if (columns.get(i).equals(ALL_COLUMN_NAME) && isAllColumnExist()) {
                columnIndexes[i] = -1;
                setUsedAllColumn(true);
                continue;
            }
            if (columns.get(i).equals(DEBUG_COLUMN_NAME) && isDebugColumnExist()) {
                columnIndexes[i] = -1;
                setUsedDebugColumn(true);
                continue;
            }
            boolean isColumn = false;           
            int ret = model.findColumn(columns.get(i));
            if (ret != -1) {
                columnIndexes[i] = ret;
                isColumn = true;
            }
            if (isColumn == false) {                    
                throw new GrmException(I18NManager.formatMessage("ColumnSelectTableModel.uc",BUNDLE,columns.get(i)));
            }
        }
    }

    @Override
    public String getColumnName(int column) {
        return columns.get(column);
    } 
    
    @Override
    public String getValueForAllColumn(int row) {
        return model.getValueForAllColumn(row);
    }

    @Override
    public String getValueForDebugColumn(int row) {
        return model.getValueForDebugColumn(row);
    }

    @Override
    public int findColumn(String columnName) {
        for (int i = 0; i<columns.size(); i++) {
            if (this.getColumnName(i).equals(columnName)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return model.getColumnClass(columnIndex);
    }
}
