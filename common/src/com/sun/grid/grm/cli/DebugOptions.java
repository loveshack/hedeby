/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;

/**
 *  This class parse the argument of the global debug option.
 *  The argument has the form
 *
 *  <pre>
 *      argument := loggerdef ( ':' loggerdef )*
 *      loggerdef := level | loggername '=' level
 *  </pre>
 *
 *  <b>Examples:</b><br>
 *
 *  <pre>
 *  INFO:com.sun.grid=FINE:com.sun.grid.grm=FINEST
 *  </pre>
 */
public class DebugOptions {
    
    private Level lowerestLevel = null;
    private Level globalLevel = null;
    private Map<String, Level> loggerMap = new HashMap<String, Level>();
    
    public DebugOptions(String debug) {
        this(debug, null);
    }
    
    /**
     * Create a new Instance of the <code>DebugOptions</code>
     * 
     * @param debug the debug option string
     * @throws java.lang.IllegalArgumentException
     */
    public DebugOptions(String debug, Level defaultLevel) throws IllegalArgumentException {
        
        this.lowerestLevel = defaultLevel;
        this.globalLevel = defaultLevel;
        if(debug != null) {
            
            StringTokenizer st = new StringTokenizer(debug, ":");
            
            while(st.hasMoreTokens()) {
                String logdef = st.nextToken();
                
                String logger = null;
                String levelStr = null;
                int index = logdef.indexOf('=');
                
                if(index > 0) {
                    logger = logdef.substring(0,index);
                    levelStr = logdef.substring(index+1, logdef.length());
                } else {
                    // the log definition does not contain a =
                    // it is a definition for the global log level
                    logger = null;
                    levelStr = logdef;
                }
                
                Level level;
                try {
                    level = Level.parse(levelStr);
                } catch(IllegalArgumentException ex) {
                    throw new IllegalArgumentException("Invalid log level " + levelStr);
                }
                
                if(logger == null) {
                    globalLevel = level;
                } else {
                    loggerMap.put(logger, level);
                }
                
                if(lowerestLevel != null && level.intValue() < getLowerestLevel().intValue()) {
                    lowerestLevel = level;
                }
            }
        }
        
    }
    
    /**
     * Gets the map containing the all logers including global loger
     * @return the logger map
     */
    public Map<String, Level> getLoggerMapWithGlobal() {
        if (getGlobalLevel() != null) {
            loggerMap.put("global", getGlobalLevel());
        }
        return Collections.unmodifiableMap(loggerMap);
    }
    
    /**
     * Get the lowerest level which is referred by the debug options
     * @return the lowerest level
     */
    public Level getLowerestLevel() {
        return lowerestLevel;
    }
    
    /**
     * Get the global level
     * @return the global level
     */
    public Level getGlobalLevel() {
        return globalLevel;
    }
    
    /**
     * Get a map containing the name of the loggers as key
     * and the log level as value
     * @return the logger map
     */
    public Map<String, Level> getLoggerMap() {
        return Collections.unmodifiableMap(loggerMap);
    }
    
    public void setLogLevel(String loggerName, Level level) {
        
        loggerMap.put(loggerName, level);
        if(lowerestLevel == null || level.intValue() < lowerestLevel.intValue()) {
            lowerestLevel = level;
        }
    }
    
}
