/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.SystemUtil;
import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

/**
 * Find where a system is installed
 */
public class SystemFinder extends AbstractCli {
    
    private static final Logger log = Logger.getLogger(SystemFinder.class.getName());
    
    public static final String DEFAULT_SYSTEM_NAME = "default";
    
    private File distPath;
    private String realSystemName;
    private boolean securityDisabled;
    private List<URL> systemClasspath;
    /**
     * Creates a new instance of SystemFinder.
     * @param args command line arguments for finding a system
     * @throws com.sun.grid.grm.GrmException On errors
     */
    public SystemFinder(String[] args) throws GrmException {
        super("system_finder");
        
        Queue<String> argQueue = new LinkedList<String>(Arrays.asList(args));

        try {
            parseGlobalArgs(argQueue);
        } catch(PrintUsageException ex) {
            // Ignore it, user gave us the -help option
        } catch(PrintVersionException ex) {
            // Ignore it, user gave us the -version option
        } catch(UsageException ex) {
            // Ignore it, user request help for a command
        }
        
        initLogging();
        
        initFromPrefs();
    }
    
    private void initFromPrefs() throws GrmException {
        PreferencesType prefsType [] = new  PreferencesType[1];
        String name = evaluateSystemName(prefsType);
        if(name == null) {
            log.log(Level.FINE, "Have no systemname");
            distPath = null;
            securityDisabled = false;
            systemClasspath = getLocalSystemClasspath();
        } else {
            try {
                ExecutionEnv env = null;
                if(prefsType[0] == null) {
                    try {
                        env = ExecutionEnvFactory.newInstanceFromPrefs(name, PreferencesType.SYSTEM);
                    } catch (PreferencesNotAvailableException ex) {
                        env = ExecutionEnvFactory.newInstanceFromPrefs(name, PreferencesType.USER);
                    } catch (BackingStoreException ex) {
                        env = ExecutionEnvFactory.newInstanceFromPrefs(name, PreferencesType.USER);
                    }
                } else {
                    env = ExecutionEnvFactory.newInstanceFromPrefs(name, prefsType[0]);
                }

                realSystemName = env.getSystemName();
                distPath = env.getDistDir();
                
                systemClasspath = getSystemClasspath(PathUtil.getDistLibPath(env));
                securityDisabled = SystemUtil.isSSLDisabled(env);
                
            } catch(PreferencesNotAvailableException ex) {
                // So far we have not system. May be some what's to add a new system
                log.log(Level.FINE, "system {0} not yet initialized", name);
                distPath = null;
                securityDisabled = true;
                systemClasspath = getLocalSystemClasspath();
            } catch(GrmException ex) {
                // Unexpected error while creating ExecutionEnv
                if(log.isLoggable(Level.FINE)) {
                    LogRecord lr = new LogRecord(Level.FINE, "Can not access system {0}: {1}");
                    lr.setParameters(new Object [] { name, ex.getLocalizedMessage() });
                    lr.setThrown(ex);
                    log.log(lr);
                }
                distPath = null;
                securityDisabled = true;
                systemClasspath = getLocalSystemClasspath();
                
            } catch(BackingStoreException ex) {
                // Unexpected error while creating ExecutionEnv
                if(log.isLoggable(Level.FINE)) {
                    LogRecord lr = new LogRecord(Level.FINE, "Can not access system {0}: {1}");
                    lr.setParameters(new Object [] { name, ex.getLocalizedMessage() });
                    lr.setThrown(ex);
                    log.log(lr);
                }
                distPath = null;
                securityDisabled = true;
                systemClasspath = getLocalSystemClasspath();
                
            }      
        }
    }

    
    private static List<URL> getLocalSystemClasspath() {
        return getSystemClasspath(PathUtil.getDistLibPath(PathUtil.getDefaultDistDir()));
    }
    
    /**
     * 
     * @param distLibDir 
     * @return 
     */
    private static List<URL> getSystemClasspath(File distLibDir) {
        
        List<URL> systemClasspath = new LinkedList<URL>();
        
        File [] files = distLibDir.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return !pathname.isDirectory()
                &&  !pathname.getName().endsWith("impl.jar")
                &&  pathname.getName().endsWith(".jar");
            }
        });
        if(files != null) {
            for(File file: files) {
                log.log(Level.FINE, "Add {0} to system classpath", file);
                try {
                    systemClasspath.add(file.toURI().toURL());
                } catch (MalformedURLException ex) {
                    throw new IllegalStateException("Can not convert a file " + file + " into URL", ex);
                }
            }
        }
        
        
        File extDir = new File(distLibDir, "ext");
        files = extDir.listFiles(new FileFilter() {
            
            public boolean accept(File pathname) {
                return !pathname.isDirectory()
                && pathname.getName().endsWith(".jar");
            }
            
        });
        
        if(files != null) {
            for(File file: files) {
                log.log(Level.FINE, "Add {0} to system classpath", file);
                try {
                    systemClasspath.add(file.toURI().toURL());
                } catch (MalformedURLException ex) {
                    throw new IllegalStateException("Can not convert a file " + file + " into URL", ex);
                }
            }
        }
        
        return systemClasspath;
    }
    
    /**
     * get the dist directory of the system
     * @return the dist directory of the system
     */
    public File getDistPath() {
        return distPath;
    }

    /**
     * Get the real system name
     * @return the real system name
     */
    public String getRealSystemName() {
        return realSystemName;
    }

    /**
     * Determine if security is disabled for this system.
     * @return <code>true</code> if security is disabled for this system
     */
    public boolean isSecurityDisabled() {
        return securityDisabled;
    }

    /**
     * 
     * @return Returns an array with all URLs for the system classpath
     */
    public List<URL> getSystemClasspath() {
        return systemClasspath;
    }
}
