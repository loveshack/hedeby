/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Formatter that formats the output of logging system
 * for cli clients
 *
 */
public class CLIFormatter extends Formatter {
    
    private final static String NL = System.getProperty("line.separator");
    
    private static ResourceBundle rb() {
        return ResourceBundle.getBundle("com.sun.grid.grm.cli.client", Locale.getDefault(), Thread.currentThread().getContextClassLoader());
    }
    
    /**
     * format a log record.
     *
     * @param record the logging record
     * @return the formetted log record
     */
    public String format(LogRecord record) {
        StringBuilder ret = new StringBuilder();
        if(record.getLevel().intValue() >= Level.SEVERE.intValue()) {
            ret.append(rb().getString("client.level.error"));
        } else if (record.getLevel().intValue() >= Level.WARNING.intValue()) {
            ret.append(rb().getString("client.level.warning"));
        } else if (record.getLevel().intValue() < Level.INFO.intValue()) {
            ret.append(rb().getString("client.level.debug"));
        }
        
        String msg = formatMessage(record);
        ret.append(msg);
        ret.append(NL);
        return ret.toString();
    }
    
}
