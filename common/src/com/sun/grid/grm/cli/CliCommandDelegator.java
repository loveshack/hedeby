/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.GrmClassLoaderFactory;
import java.net.URL;

/**
 *  Abstract base class for all CLI commands which depends on classes
 *  that are not available in the System classpath of a Hedeby system.
 *
 *  This class loads a delegate class on a separate class loader.
 */
public abstract class CliCommandDelegator implements CliCommand {
    

    /**
     * name of the resource bundle
     */
    public static final String BUNDLE = "com.sun.grid.grm.cli.client";
    
    /**
     * Get the classpath for the component delegate.
     * @return an array with the URLs of all classpath elements
     * @param cli the cli object
     * @throws com.sun.grid.grm.cli.CliException if the classpath can not be constructed
     */
    protected abstract URL[] getClasspath(AbstractCli cli) throws GrmException;
    
    
    /**
     * Get the classname of the CliCommand delegate
     * @return the classname of the component delegate
     * @param cli the cli object
     * @throws com.sun.grid.grm.cli.CliException if classname could not be constructed
     */
    protected abstract String getDelegateClassname(AbstractCli cli) throws GrmException;
    
    /**
     * This method creates the command delegate.
     * 
     * <p>First out of the classpath a new <code>ClassLoader</code> is created. This
     * <code>ClassLoader</code> is used find the class of the component delegate.</p>
     * @return the command delegate
     * @see #getDelegateClassname
     * @see #getClasspath
     * @param cli the cli object
     * @throws com.sun.grid.grm.cli.CliException if the component delegate can not be constructed
     */
    @SuppressWarnings("unchecked")
    protected CliCommandDelegate createDelegate(AbstractCli cli) throws GrmException {
        
        URL [] classpath = getClasspath(cli);
        String classname = getDelegateClassname(cli);
        
        ClassLoader cl = GrmClassLoaderFactory.getInstance(classpath, getClass().getClassLoader());
        
        Class cls;
        try {
            cls = Class.forName(classname, true, cl);
        } catch (ClassNotFoundException ex) {
            throw new GrmException("commandDelegate.error.classNotFound", ex, BUNDLE, classname);
        }
        try {
            return (CliCommandDelegate)cls.newInstance();
        } catch (IllegalAccessException ex) {
            throw new GrmException("commandDelegate.error.noConstructor", ex, BUNDLE, classname);
        } catch (InstantiationException ex) {
            throw new GrmException("commandDelegate.error", ex, BUNDLE, ex.getLocalizedMessage());
        }
    }
    
    /**
     * Execute the CLI command.
     *
     * Calls inside the execute method of the <code>CLICommandDelegate</code>.
     *
     * @param cli  the cli object
     * @throws com.sun.grid.grm.GrmException  ony any error
     */
    public final void execute(AbstractCli cli) throws GrmException {
        
        CliCommandDelegate delegate = createDelegate(cli);
        
        delegate.execute(cli, this);
    }
}
