/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.InvalidComponentNameException;
import com.sun.grid.grm.bootstrap.Module;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.bootstrap.UpgradeNotAvailableException;
import com.sun.grid.grm.bootstrap.UpgradeUtil;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.AbstractTableCliCommand;
import com.sun.grid.grm.cli.table.HumanReadableTable;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.cli.table.TableListener;
import com.sun.grid.grm.management.ConnectionPool;
import com.sun.grid.grm.security.ConsoleCallbackHandler;
import com.sun.grid.grm.security.FileCallback;
import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.security.SecurityConstants;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.I18NPrintWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * Abstract base class for command line clients.  This abstract base class
 * provides the functionality for parsing command line options and executing the
 * parsed commands.
 */
public abstract class AbstractCli {
    private static final String HELP_OPTION = "client.option.help";
    private static final String COMMAND_HELP_OPTION = "client.option.command.help";
    private static final String VERSION_OPTION = "client.option.version";
    
    /**
     * Used message bundle
     */
    private static final String BUNDLE = "com.sun.grid.grm.cli.client";

    /*  "sdmadm"  This comment is needed for the checkbundles ant task, this class uses
     *            the resource key "sdmadm.description" and  "sdmadm.long_description"
     *            !!! DO NOT DELETE IT!!
     */


    /**
     * Map for available implemented CLI commands.
     * Please note that this map will also store the commands under it's shortcuts
     */
    protected final Map<String, Class<? extends CliCommand>> commands =
        new HashMap<String, Class<? extends CliCommand>>();
    
    /**
     * Set with all available commands
     */
    protected final Set<Class<? extends CliCommand>> commandSet = new HashSet<Class<? extends CliCommand>>();
    
    /**
     * Logger for this class
     */
    protected final Logger log =
        Logger.getLogger(AbstractCli.class.getName(), BUNDLE);
    
    private ExecutionEnv env = null;
    private BufferedReader stdin = null;
    private String name = null;
    /**
     * Actual command
     */
    protected CliCommand command = null;
    
    private I18NPrintWriter out = new I18NPrintWriter(System.out);
    private I18NPrintWriter err = new I18NPrintWriter(System.err);
    
    
    /**
     * global debug option
     */
    @CliOptionDescriptor(
    name = "client.option.debug",
        hasShortcut = true
        )
        protected boolean debug = false;
    
    /**
     * global Debug option
     */
    @CliOptionDescriptor(
    name = "client.option.capitalDebug",
        hasShortcut = true
        )
        protected boolean capitalDebug = false;
    
    /**
     * global promptPW option
     */
    @CliOptionDescriptor(
    name = "client.option.promptPW",
        hasShortcut = true
        )
        protected boolean promptPW = false;
    
    /**
     * global logging option
     */
    @CliOptionDescriptor(
    name = "client.option.loggers",
        numberOfOperands = 1,
            hasShortcut = true
        )
        protected String loggers;
    
    /**
     * global system name option
     */
    @CliOptionDescriptor(
    name = "client.option.system",
        numberOfOperands = 1,
        hasShortcut = true
        )
        protected String systemName = null;
    
    /**
     * global prefs option
     */
    @CliOptionDescriptor(
    name = "client.option.prefs",
        numberOfOperands = 1,
        hasShortcut = true
        )
        protected String prefs = null;
    
    
    /**
     * global caCert option
     */
    @CliOptionDescriptor(
    name = "client.option.caCertFile",
        numberOfOperands = 1,
        hasShortcut = true
        )
        protected File caCertFile;
    
    /**
     * global keystore option
     */
    @CliOptionDescriptor(
    name = "client.option.keyStoreFile",
        numberOfOperands = 1,
        hasShortcut = true
        )
        protected File keyStoreFile;
    
    /**
     * Create a new CLI
     * @param name name of the CLI
     */
    protected AbstractCli(String name) {
        this.name = name;
        
        try {
            initLogging();
        } catch (GrmException ex) {
            // This exception can not happen since we have
            // at this point no logging definition
            throw new AssertionError("initLogging throws a exception, but no logging is defined");
        }
    }
    
    
    
    /*
     *  Load all classes from the extension property file
     */
    private List<Class<? extends CliCommand>> getExtensionClasses() {
        
        List<Class<? extends CliCommand>> ret = new LinkedList<Class<? extends CliCommand>>();
        for(Module module: Modules.getModules()) {
            ret.addAll(module.getCliExtensions());
        }
        return ret;
    }
    
    
    private boolean extensionsNotLoaded = true;
    
    /**
     * Load the extension for a command
     */
    public void loadAllExtensions() {
        if(extensionsNotLoaded) {
            extensionsNotLoaded = false;
            List<Class<? extends CliCommand>> extensionClasses = getExtensionClasses();

            for(Class<? extends CliCommand> commandClass: extensionClasses) {
                addCommand(commandClass);
            }
        }
    }
    
    /**
     * Execute the command
     * @param args The arguments
     */
    public void run(String [] args) {
        int exitState = 0;
        try {
            Queue<String> argQueue = new LinkedList<String>(Arrays.asList(args));
            CliCommand cmd = parseArgs(argQueue);
            this.validateGlobalOptions();
            cmd.execute(this);
        } catch (PrintUsageException e) {
            //this one is just for expected usage
            printUsage(e.getTarget());
        } catch (UsageException e) {
            err.print("client.error", BUNDLE);
            if (debug) {
                Throwable t = e;
                while(t != null) {
                    t.printStackTrace(err.getPrintWriter());
                    t = t.getCause();
                }
            } else {
                err.printlnDirectly(e.getLocalizedMessage());
            }
            exitState = 1;
        } catch (PrintVersionException e) {
            printVersion();
        } catch (GrmRemoteException e) {
            if (e.isCausedBySSLProblem()) {
                err.println("client.error.ssl", BUNDLE, e.getLocalizedMessage(), System.getProperty("user.name"));
            } else {
                err.print("client.error", BUNDLE);
                err.printlnDirectly(e.getLocalizedMessage());
            }
            if (debug) {
                Throwable t = e;
                while(t != null) {
                    t.printStackTrace(err.getPrintWriter());
                    t = t.getCause();
                }
            }

            exitState = 2;
            
        } catch (GrmException e) {
            err.print("client.error", BUNDLE);
            if (debug) {
                Throwable t = e;
                while(t != null) {
                    t.printStackTrace(err.getPrintWriter());
                    t = t.getCause();
                }
            } else {
                err.printlnDirectly(e.getLocalizedMessage());
            }

            exitState = 2;

        } catch (Exception e) {
            err.println("client.error", BUNDLE);
            Throwable t = e;
            if (e instanceof UndeclaredThrowableException) {
                t = ((UndeclaredThrowableException)e).getUndeclaredThrowable();
            }
            
            /* If there are uncaught exceptions, there isn't much we can do
             * other than print the error message, if there is one.  If there
             * isn't one, have no choice but to dump the stack. */
            if (t.getLocalizedMessage() != null) {
                err.printlnDirectly(t.getLocalizedMessage());
                if(debug) {
                    t.printStackTrace();
                    t = t.getCause();
                    while(t != null) {
                        err.println("client.cause", BUNDLE);
                        t.printStackTrace(err.getPrintWriter());
                        t = t.getCause();
                    }
                }
            } else {
                t.printStackTrace(err.getPrintWriter());
            }
            exitState = 3;
        } finally {
            err.flush();
            out.flush();
            if(env != null) {
                ConnectionPool.getInstance(env).closeAllConnections();
            }
        }
        exit(exitState);
    }
    
    /**
     * After parsing the arguments we should be able validate global options
     * for CLI commands, like -keystore, -cacert we should check if the files are 
     * existing and they are readable by the user
     */
    
    private void validateGlobalOptions() throws GrmException {
    
        if ((this.caCertFile != null) && !(this.caCertFile.isFile() && this.caCertFile.canRead())) {
            throw new GrmException("client.error.globaloption_cacert_validation", BUNDLE, this.caCertFile.getAbsolutePath());
        }
        
        if ((this.keyStoreFile != null) && !(this.keyStoreFile.isFile() && this.keyStoreFile.canRead())) {
            throw new GrmException("client.error.globaloption_keystore_validation", BUNDLE, this.keyStoreFile.getAbsolutePath());
        }
        
    }
    
    /**
     * Load CLI extention classes from "com.sun.grid.grm.cli.extensions"
     * properties file
     * @param ext The name of the extention command
     * @return null or the loaded CLI command class
     */
    protected Class<? extends CliCommand> loadExtension(String ext) {
        
        Class<? extends CliCommand> clazz = commands.get(ext);
        if(clazz != null) {
            return clazz;
        }
        
        List<Class<? extends CliCommand>> extensionClasses = getExtensionClasses();
        
        for(Class<? extends CliCommand> commandClass: extensionClasses) {
            CliCommandDescriptor tag = commandClass.getAnnotation(CliCommandDescriptor.class);
            
            String extName = getName(tag, commandClass.getClassLoader());
            if(ext.equals(extName)) {
                addCommand(commandClass);
                return commandClass;
            }
            
            if(tag.hasShortcut()) {
                String shortcut = getShortcut(tag, commandClass.getClassLoader());
                if(ext.equals(shortcut)) {
                    addCommand(commandClass);
                    return commandClass;
                }
            }
        }
        
        log.log(Level.FINE, "client.loadExt.extUnknown", ext);
        return null;
    }
    
    /**
     * Initializes the logger to output debug info to stdout and warnings and
     * errors to stderr.
     * @throws com.sun.grid.grm.GrmException On creating logger errors
     */
    protected void initLogging() throws GrmException {
        final DebugOptions debugOptions;
        try {
            debugOptions = new DebugOptions(loggers, Level.SEVERE);
            
            // If no log level for com.sun.grid.grm is defined we the it to INFO
            if(!debugOptions.getLoggerMap().containsKey("com.sun.grid.grm")) {
                debugOptions.setLogLevel("com.sun.grid.grm", Level.INFO);
            }
            
        } catch(IllegalArgumentException ex) {
            throw new GrmException("client.error.invalid_log_level", AbstractCli.BUNDLE, loggers);
        }
        
        try {
            PipedInputStream pin = new PipedInputStream();
            final PipedOutputStream pout = new PipedOutputStream(pin);
            
            Thread logConfWriter = new Thread("AbstractCli.logConfWriter") {
                @Override
                public void run() {
                    
                    try {
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        
                        String handler = ConsoleHandler.class.getName();
                        String formatter = CLIFormatter.class.getName();
                        
                        pw.print("handlers=");
                        pw.println(handler);
                        
                        pw.print(handler);
                        pw.print(".level=");
                        pw.println(debugOptions.getLowerestLevel().getName());
                        
                        // print formatter of the handler
                        pw.print(handler);
                        pw.print(".formatter=");
                        pw.println(formatter);
                        
                        
                        // print global log level
                        pw.print(".level=");
                        pw.println(debugOptions.getGlobalLevel().getName());
                        
                        // print the log levels for individual loggers
                        Map<String, Level> loggerMap = debugOptions.getLoggerMap();
                        for(String logger: loggerMap.keySet()) {
                            pw.print(logger);
                            pw.print(".level=");
                            pw.println(loggerMap.get(logger).getName());
                        }
                        pw.close();
                        
                        
                        OutputStreamWriter writer = new OutputStreamWriter(pout);
                        writer.write(sw.getBuffer().toString());
                        writer.close();
                    } catch(IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            };
            
            logConfWriter.start();
            
            LogManager lm = LogManager.getLogManager();
            // Reconfigure the log manager
            lm.readConfiguration(pin);
        } catch(IOException ioe) {
            // Should not happen
            throw new AssertionError("IO Error while setting up logging");
        }
    }
    
    
    /**
     * This method must be called once for each command class before calling
     * parseArgs().  This method registers the command class with the
     * AbstractCli so that it and its field are able to participate in
     * parameter parsing and help output generation.
     * @param commandClass the class of the new command
     */
    protected void addCommand(Class<? extends CliCommand> commandClass) {
        CliCommandDescriptor tag = commandClass.getAnnotation(CliCommandDescriptor.class);
        assert tag != null : "Command has no descriptor";
        assert !(tag.name() == null) : "Command has no name";
        assert !(tag.name().length() == 0) : "Command has no name";
        
        String aName = getName(tag, commandClass.getClassLoader());
        
        if (commands.containsKey(aName)) {
            // Command has been loaded before
            return;
        }
        
        commandSet.add(commandClass);
        commands.put(aName, commandClass);
        
        if (tag.hasShortcut()) {
            String shortcut = getShortcut(tag, commandClass.getClassLoader());
            
            if (commands.containsKey(shortcut)) {
                throw new AssertionError("Command option conflict for " +
                    shortcut + " between " +
                    commands.get(shortcut).getName() +
                    " and " + commandClass.getName());
            }
            
            commands.put(shortcut, commandClass);
        }
    }
    
    /**
     * Clears the registered commands.
     */
    public void clear() {
        commands.clear();
        commandSet.clear();
    }
    
    /**
     * Returns an unmodifiable collection of the loaded command classes.
     * @return the loaded command classes
     */
    public Collection<Class<? extends CliCommand>> getCommands() {
        return Collections.<Class<? extends CliCommand>>unmodifiableCollection(commandSet);
    }
    
    /**
     * Constructs the usage message from the given commands and options
     * and prints it to stdout.
     * @param commandClass the target command or null for global help output
     * @see #addCommand
     */
    protected void printUsage(Class<? extends CliCommand> commandClass) {
        if (commandClass == null) {
            printGlobalUsage();
        } else {
            printCommandUsage(commandClass);
        }
    }
    
    private class CommandUsageTableModel extends AbstractDefaultTableModel implements TableListener {
        private final List<String> rows;
        private final Map<String,CommandInfo> values;
        
        public CommandUsageTableModel(Class<? extends CliCommand> command, List<Field> globalOptions) {
            CommandInfo ci;
            rows = new ArrayList<String>(globalOptions.size() + 1);
            values = new HashMap<String,CommandInfo>(rows.size());
            
            CliCommandDescriptor commandTag = command.getAnnotation(CliCommandDescriptor.class);
            if (commandTag == null) {
                throw new IllegalStateException("Command " + command.getName() + " has no descriptor");
            }                    
            
            List<Field> fields = getAnnotatedFields(command);
            
            for(Field field: fields) {
                String commandCategory = getCommandOptionsCategory(field.getDeclaringClass());
                ci = new CommandInfo(commandCategory, field);
                rows.add(ci.arg);
                values.put(ci.arg, ci);
            }
            ci = newCommandHelpCommandInfo(getCommandOptionsCategory(null));
            rows.add(ci.arg);
            values.put(ci.arg, ci);
        
            
            String gc = getGlobalOptionsCategory();
            for(Field field: globalOptions) {
                ci = new CommandInfo(gc, field);
                rows.add(ci.arg);
                values.put(ci.arg, ci);
            }
            ci = newHelpCommandInfo(gc);
            rows.add(ci.arg + "1");
            values.put(ci.arg + "1", ci);
            
            Collections.sort(rows, new CliCommandComparator(values));

        }

        public int getRowCount() {
           return rows.size();
        }

        public int getColumnCount() {
            return 3;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            String name = rows.get(rowIndex);
            CommandInfo info = values.get(name);
            
            switch(columnIndex) {
                case 0: return info.arg;
                case 1: return info.shortcut;
                case 2: return info.desc;
                default:
                    throw new IllegalArgumentException("unknown columun " + columnIndex);
            }
        }
        
        @Override
        public String getColumnName(int columnIndex) {
            switch(columnIndex) {
                case 0: return "command";
                case 1: return "shortcut";
                case 2: return "description";
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        public void printingRow(int row) {
            CommandInfo ci = values.get(rows.get(row));
            boolean printCategory = false;
            if(row == 0) {
                printCategory = true; 
            } else {
                CommandInfo lastCi = values.get(rows.get(row-1));
                
                if(lastCi.category == null) {
                    printCategory = ci.category != null; 
                } else {
                    printCategory = !lastCi.category.equals(ci.category);
                }
            }
            if(printCategory) {
                out().println();
                out().printlnDirectly(ci.category);
            }
        }

        public void rowPrinted(int row) {
            
        }
    }
    
    private class GlobalUsageTableModel extends AbstractDefaultTableModel implements TableListener {
        
        private final List<String> rows;
        private final Map<String,CommandInfo> values;
        
        public GlobalUsageTableModel(Map<String,Class<? extends CliCommand>> commands, List<Field> globalOptions) {
            rows = new ArrayList<String>(commands.size());
            values = new HashMap<String,CommandInfo>(rows.size());
            
            for(Map.Entry<String,Class<? extends CliCommand>> entry: commands.entrySet()) {
               Class<? extends CliCommand> cmd = entry.getValue();
               CommandInfo ci = new CommandInfo(cmd);
               if(!values.containsKey(ci.arg)) {
                   rows.add(ci.arg);
                   values.put(ci.arg, ci);
               }
            }
            
            String gc = getGlobalOptionsCategory();
            for(Field field: globalOptions) {
                CommandInfo ci = new CommandInfo(gc, field);
                rows.add(ci.arg);
                values.put(ci.arg, ci);
            }
            CommandInfo ci = newHelpCommandInfo(gc);
            rows.add(ci.arg);
            values.put(ci.arg, ci);

            CommandInfo cvi = newVersionCommandInfo(gc);
            rows.add(cvi.arg);
            values.put(cvi.arg, cvi);
            
            Collections.sort(rows, new CliCommandComparator(values));
            
        }

        public int getRowCount() {
           return rows.size();
        }

        public int getColumnCount() {
            return 3;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            String name = rows.get(rowIndex);
            CommandInfo info = values.get(name);
            
            switch(columnIndex) {
                case 0: return info.arg;
                case 1: return info.shortcut;
                case 2: return info.desc;
                default:
                    throw new IllegalArgumentException("unknown columun " + columnIndex);
            }
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch(columnIndex) {
                case 0: return "command";
                case 1: return "shortcut";
                case 2: return "description";
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        public void printingRow(int row) {
            CommandInfo ci = values.get(rows.get(row));
            boolean printCategory = false;
            if(row == 0) {
                printCategory = true; 
            } else {
                CommandInfo lastCi = values.get(rows.get(row-1));
                
                if(lastCi.category == null) {
                    printCategory = ci.category != null; 
                } else {
                    printCategory = !lastCi.category.equals(ci.category);
                }
            }
            if(printCategory) {
                out().println();
                out().printlnDirectly(ci.category);
            }
        }

        public void rowPrinted(int row) {
            
        }
    }
    
    
    private class CliCommandComparator implements Comparator<String> {
        
        private final Map<String,CommandInfo> commands;
        
        public CliCommandComparator(Map<String,CommandInfo> commands) {
            this.commands = commands;
        }
        public int compare(String o1, String o2) {
            
            CommandInfo cmd1 = commands.get(o1);
            CommandInfo cmd2 = commands.get(o2);
            
            int ret = 0;
            // Special handling for the global options category
            // ensures the the global options are display at the end
            String globalOptionsCategory = getGlobalOptionsCategory();
            if(globalOptionsCategory.equals(cmd1.category)) {
                if(globalOptionsCategory.equals(cmd2.category)) {
                    ret = 0;
                } else {
                    ret = 1;
                }
            } else if (globalOptionsCategory.equals(cmd2.category)) {
                ret = -1;
            } else {           
                ret = compareStrs(cmd1.category, cmd2.category);
            }
            
            if(ret == 0) {
                ret = compareStrs(cmd1.arg, cmd2.arg);
            }
            return ret;
        }
        
        private int compareStrs(String s1, String s2) {
            if(s1 == null) {
                if(s2 == null) {
                    return 0;
                } else {
                    return 1;
                }
            } else if ( s2 == null) {
                return 0;
            } else {
                return s1.compareTo(s2);
            }
        }
    }

    private class VersionTableModel extends AbstractDefaultTableModel {
        private final static long serialVersionUID = -2009110401L;
        private final String location;
        private final String version;

        public VersionTableModel(String location, String version) {
            this.location = location;
            //Version may contain error message why version could not be retrieved
            this.version = version;

        }

        public int getRowCount() {
           return 1;
        }

        public int getColumnCount() {
            return 2;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            //ignore rowIndex we just have one row here

            switch(columnIndex) {
                case 0: return location;
                case 1: return version;
                default:
                    throw new IllegalArgumentException("unknown columun " + columnIndex);
            }
        }
        @Override
        public String getColumnName(int columnIndex) {
            switch(columnIndex) {
                case 0: return ResourceBundle.getBundle(AbstractCli.BUNDLE).getString("AbstractCli.version.col.location");
                case 1: return ResourceBundle.getBundle(AbstractCli.BUNDLE).getString("AbstractCli.version.col.version");
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }
    }
    /**
     * Constructs and prints the version information for the binary if possible
     * 
     */
    private void printVersion() {
        
        String version;
        try {
            version = UpgradeUtil.getBinaryVersion(getExecutionEnv());
        } catch (UpgradeNotAvailableException ex) {
            version = I18NManager.formatMessage("AbstractCli.version.col.unsupportedVersion", AbstractCli.BUNDLE);
        } catch (GrmException ex) {
            version = ex.getLocalizedMessage();
        }
        VersionTableModel tm = new VersionTableModel(PathUtil.getDefaultDistDir().toString(), version);
        Table table = new HumanReadableTable(tm, this);
        table.print(this);

    }

    /**
     * Constructs and prints the usage information for the global commands and
     * options.
     */
    private void printGlobalUsage() {
        loadAllExtensions();
        List<Field> globalOptions = getAnnotatedFields(this.getClass());
        GlobalUsageTableModel tm = new GlobalUsageTableModel(this.commands, globalOptions);   
        
        Table table = new HumanReadableTable(tm, this);
        table.setAutoWordWrap(2, true);
        table.setColumnWidth(2, 60);
        table.addTableListener(tm);
        table.setPrintHeader(false);
        
        table.print(this);
    }
    
    
    private void printAndWrap(PrintWriter pw, int colWidth, String output) {
        // pw.print(output);
        StringTokenizer tokens = new StringTokenizer(output, "\n\r ");
        StringBuilder sb = new StringBuilder(2*colWidth);
        while (tokens.countTokens() > 0) {
            String word = tokens.nextToken();
            if (word.length() > colWidth) {
                // the word is to long for the column width
                
                // first flash current line
                if (sb.length() > 0) {
                    pw.println(sb.toString());
                    sb.setLength(0);
                }
                // now break the word
                int start = 0;
                int end = colWidth;
                while (end <= word.length()) {
                    String line = word.substring(start, end);
                    pw.println(line);
                    start = start + colWidth;
                    end = end + colWidth;
                }
                sb.append(word.substring(start, word.length()));
            } else {
                if (sb.length() + word.length() < colWidth) {
                    // word fits into culumn line
                    if (sb.length() > 0) {
                        sb.append(' ');
                    }
                    sb.append(word);
                } else{
                    // word is to long for this line, wraparound ...
                    pw.println(sb.toString());
                    sb.setLength(0);
                    sb.append(word);
                }
            }
            
        }
        // add the last StringBuilder
        if (sb.length() > 0) {
            pw.println(sb.toString());
        }
    }
    
    private void printCommandUsage(Class<? extends CliCommand> commandClass) {
       loadAllExtensions();
       List<Field> globalOptions = getAnnotatedFields(this.getClass());
       CommandUsageTableModel tm = new CommandUsageTableModel(commandClass, globalOptions);   
       Table table = new HumanReadableTable(tm, this);
       table.setAutoWordWrap(2, true);
       table.setColumnWidth(2, 60);
       table.addTableListener(tm);
       table.setPrintHeader(false);
       CommandInfo ci = new CommandInfo(commandClass);
       String usageline = I18NManager.formatMessage("client.usage", BUNDLE, getCommandUsageText(commandClass));
       int table_width =  table.getTableWidth();
       printAndWrap(out.getPrintWriter(), table_width, usageline);
       
       out.println();
       out.printDirectly(ci.desc);
       out.println();
       
       table.print(this);
    }
    
    
    private String getCommandUsageText(Class<? extends CliCommand> commandClass) {
        CliCommandDescriptor commandTag = commandClass.getAnnotation(CliCommandDescriptor.class);
        if (commandTag == null) {
           throw new IllegalStateException("Command " + commandClass.getCanonicalName() + " has no descriptor");
        }    
        List<Field> fields = getAnnotatedFields(commandClass);
        String aName = getName(commandTag, commandClass.getClassLoader());
        String combo = null;
        if (commandTag.hasShortcut()) {
            combo = aName + "|" + getShortcut(commandTag, commandClass.getClassLoader());
        } else {
            combo = aName;
        }
        ResourceBundle rb = ResourceBundle.getBundle(BUNDLE);
        StringBuilder sb = new StringBuilder(this.name);
        sb.append(' ');
        sb.append(rb.getString("client.command_line"));
        sb.append(' ');
        sb.append(combo);
        sb.append(' ');
        sb.append(buildOptionLine(fields));
        return sb.toString();
    }
    
    /**
     * Adds -help to the help output.
     * @param usage the CliUsage object currently in use
     */
//    private static void addHelpOption(CliUsage usage) {
//        String name = getName(HELP_OPTION);
//        String description = getDescription(HELP_OPTION);
//        String shortcut = getShortcut(HELP_OPTION);
//        usage.addCommand(name, shortcut, null, description);
//    }
    
    /**
     * Creates a 1-line description of the possible options for a command.
     *
     * @param fields the command's options
     * @return Description of the command
     */
    public static String buildOptionLine(List<Field> fields) {
        List<CliOptionDescriptor> tags = new ArrayList<CliOptionDescriptor>(fields.size());        
        Map<CliOptionDescriptor, Field> fieldMap = new HashMap<CliOptionDescriptor, Field>();
        /* First build a list of command options. */
        for (Field field : fields) {
            CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
            if (tag == null) {
               throw new IllegalStateException("Field has no descriptor");
            }
            tags.add(tag);
            fieldMap.put(tag, field);
        }
        
        /* Sort the command option list. */
        Collections.sort(tags, new Comparator<CliOptionDescriptor>() {
            public int compare(CliOptionDescriptor o1, CliOptionDescriptor o2) {
                return o1.name().compareTo(o2.name());
            }
        });
        
        StringBuilder optionalBool = new StringBuilder();
        StringBuilder nonOptional = new StringBuilder();
        StringBuilder optional = new StringBuilder();
        
        /* Devide the options up into categories. */
        for (CliOptionDescriptor tag : tags) {
            Field field = fieldMap.get(tag);
            
            String name = getName(field);
            
            if (tag.required() && (tag.numberOfOperands() == 0)) {
                throw new IllegalStateException(name + "is a required boolean switch");
            } else if (!tag.required() && (tag.numberOfOperands() == 0)) {
                optionalBool.append(" [");
                optionalBool.append(name);
                optionalBool.append("]");
              } else if (tag.required()) {
                nonOptional.append(' ');
                nonOptional.append(name);
                nonOptional.append(' ');
                nonOptional.append(getArgument(field));
            } else {
                optional.append(" [");
                optional.append(name);
                optional.append(' ');
                optional.append(getArgument(field));
                optional.append(']');
            }
        }
        
        /* append command help command */
        optional.append(" [");
        optional.append(getShortcut(HELP_OPTION));
        optional.append(']');
        
        /* Assemble the categories into a string. */
        StringBuilder ret = new StringBuilder();
        
        if (optionalBool.length() > 0) {
            ret.append(optionalBool);
        }
        
        if (nonOptional.length() > 0) {
            ret.append(nonOptional);
        }
        
        if (optional.length() > 0) {
            ret.append(optional);
        }
        
        return ret.toString();
    }
    
    /**
     * Returns a list of fields in this given class and its parent classes
     * which have the CliDescriptor annotation.
     * @param clazz the target command class
     * @return a list of annotated fields
     */
    public List<Field> getAnnotatedFields(Class clazz) {
        List<Field> ret = new LinkedList<Field>();
        
        getDeclaredAnnotatedFields(clazz, ret);
        
        return ret;
    }
    
    /**
     * Adds a list of fields in this given class and its parent classes
     * which have the CliDescriptor annotation to the field list.
     * @param clazz the command class
     * @param list the list to which to add annotated fields
     */
    private void getDeclaredAnnotatedFields(Class clazz,
        List<Field> list) {
        Field[] declaredFields = clazz.getDeclaredFields();
        
        for (int i = 0; i < declaredFields.length; i++) {
            if (declaredFields[i].isAnnotationPresent(CliOptionDescriptor.class)) {
                list.add(declaredFields[i]);
            }
        }
        
        Class parent = clazz.getSuperclass();
        
        if (parent != null && parent != Object.class) {
            getDeclaredAnnotatedFields(parent, list);
        }
    }
    
    /**
     * Exit the CLI.  If the exit code came from a UsageException, print usage
     * information.
     * @param exitCode exit code
     */
    public void exit(int exitCode) {
        /* If the exit code is a parsing error, print the usage before
         * exiting. */
        if ( exitCode == 1 ) {
            Class<? extends CliCommand> clazz = null;
            if (command != null) {
                clazz = command.getClass();
                String usageline = I18NManager.formatMessage("client.usage",
                        BUNDLE, getCommandUsageText(clazz));
                printAndWrap(out.getPrintWriter(), 80, usageline);
            } else {
                out.println("client.useGlobalHelpOption", BUNDLE);
            }
        }
        System.exit(exitCode);
    }
    
    /**
     * Parse the arguments and return the <code>CliCommand</code>
     * which should be executed.
     * @param args the arguments
     * @return the CliCommand
     * @throws com.sun.grid.grm.GrmException on errors
     */
    protected CliCommand parseArgs(final Queue<String> args) throws GrmException {
        String commandString = null;
        List<Field> requiredSwitches = null;
        if (args.size() != 0) {
        
            Map<String,Field> switchMap = buildSwitchMap(getClass());
            requiredSwitches = getRequiredSwitches(switchMap);

            commandString = parseGlobalArgs(args, switchMap, requiredSwitches);

            initLogging();
        }
        if (commandString == null) {
             //no sdm command was provided or no parameter at all
            throw new PrintUsageException();
        }

        /* Make sure all required field have been set. */
        if (requiredSwitches.size() != 0) {
            /* If not, just complain about the first one. */
            Field field = requiredSwitches.get(0);
            CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
            
            assert tag != null : "Field has no descriptor";
            
            throw new UsageException("client.error.missing_required_option",
                BUNDLE, getName(field));
        }
        
        
        if (commandString.startsWith("-")) {
            throw new UsageException("client.error.unknown_option", BUNDLE,
                commandString);
        } else {
            Class<? extends CliCommand> commandClass = loadCommand(commandString);
            parseCommandArgs(args, commandClass);
        }
        
        return command;
    }
    
    /**
     * Parses the global options and sets the corresponding field values.  This
     * method returns the first argument which does not map to a global option
     * or argument thereof.
     * @param args the command args
     * @return the first argument which does not map to a global option
     * @throws com.sun.grid.grm.GrmException if the global options are invalid
     */
    protected String parseGlobalArgs(Queue<String> args) throws GrmException {
        Map<String,Field> switchMap = buildSwitchMap(getClass());
        List<Field> requiredSwitches = getRequiredSwitches(switchMap);
        
        return parseGlobalArgs(args, switchMap,  requiredSwitches);
    }
    
    private String parseGlobalArgs(Queue<String> args, Map<String,Field> switchMap,  List<Field> requiredSwitches) throws GrmException {
        /* Build a list of global switches. */
        
        String arg = null;
        
        /* Parse the global args. */
        while ((arg = args.poll()) != null) {
            Field option = switchMap.get(arg);
            
            if (option != null) {
                parseOption(option, args, this);
                requiredSwitches.remove(option);
            } else if (arg.equals(getName(HELP_OPTION)) ||
                arg.equals(getShortcut(HELP_OPTION))) {
                throw new PrintUsageException();
            } else if (arg.equals(getName(VERSION_OPTION)) ||
                arg.equals(getShortcut(VERSION_OPTION))) {
                throw new PrintVersionException();
            } else if (arg.startsWith("-") &&
                parseBooleanString(arg, switchMap,
                requiredSwitches, this)) {
                continue;
            } else if (arg.startsWith("-")) {
                throw new UsageException("client.error.unknown_option",
                    BUNDLE, arg);
            } else {
                /* Assume the first unrecognized non-switch is a command. */
                break;
            }
        }
        return arg;
    }
    
    
    /**
     * This method attempts to treat the string as a concatination of boolean
     * switches.  If every character in the string has a corresponding
     * boolean switch, those switches are set to true, and this method returns
     * true.  Otherwise, this method returns false.
     * @param arg the boolean string arg
     * @param switchMap the parsed map of switches
     * @param requiredSwitches the switches required for the current command
     * @param obj the target command object
     * @return true if every character in <i>arg</i> represents a boolean switch
     * @see #buildSwitchMap(Class)
     */
    private boolean parseBooleanString(String arg,
        Map<String,Field> switchMap, List<Field> requiredSwitches,
        Object obj) throws GrmException {
        char[] chars = arg.toCharArray();
        boolean foundAll = true;
        
        assert chars[0] == '-' : "Boolean string doesn't start with '-'";
        
        /* It's ok to process the args as we go, because if we hit
         * a char that isn't a boolean arg, we throw everything
         * away. */
        for (int i = 1; i < chars.length; i++) {
            Field option = switchMap.get("-" + Character.toString(chars[i]));
            
            /* If the option isn't found, or it isn't boolean, this isn't a
             * boolean option string. */
            if ((option == null) || !parseBooleanOption(option, obj)) {
                foundAll = false;
                break;
            } else {
                /* If the option is boolean, mark it as set.  The call to
                 * parseBooleanOption() in the if did the actual setting. */
                requiredSwitches.remove(option);
            }
        }
        
        return foundAll;
    }
    /**
     * Creates an instance of the named command class, stores the instance in
     * the <i>command</i> field, and returns the Class object for the command.
     * @param commandString the name of the command class
     * @return an instance of the named command class
     */
    private Class<? extends CliCommand> loadCommand(String commandString)
    throws UsageException {
        /* Try to load the command class. */
        Class<? extends CliCommand> commandClass = commands.get(commandString);
        
        if(commandClass == null) {
            commandClass = loadExtension(commandString);
        }
        
        if (commandClass != null) {
            try {
                command = commandClass.newInstance();
            } catch (InstantiationException e) {
                throw new AssertionError("Unable to instantiate command class: " + commandClass.getName());
            } catch (IllegalAccessException e) {
                throw new AssertionError("Unable to access command class constructor: " + commandClass.getName());
            }
        } else {
            throw new UsageException("client.error.unknown_command", BUNDLE,
                commandString);
        }
        
        return command.getClass();
    }
    
    /**
     * Parses the options associated with the given command and sets the
     * corresponding field values.
     * @param args the command args
     * @param commandClass the class of the command
     */
    private void parseCommandArgs(Queue<String> args,
        Class<? extends CliCommand> commandClass) throws GrmException {
        /* Build a list of the command options. */
        Map<String,Field> switchMap = buildSwitchMap(commandClass);
        List<Field> requiredSwitches = getRequiredSwitches(switchMap);
        
        /* Parse the command options. */
        while (args.size() > 0) {
            String arg = args.peek();
            Field option = switchMap.get(arg);
            
            if (option != null) {
                args.remove();
                parseOption(option, args, command);
                requiredSwitches.remove(option);
            } else if (arg.equals(getName(HELP_OPTION)) ||
                arg.equals(getShortcut(HELP_OPTION))) {
                throw new PrintUsageException(commandClass);
            }
            /* Try treating the arg as a combination of boolean
             * switches. */
            else if (arg.startsWith("-") &&
                parseBooleanString(arg, switchMap,
                requiredSwitches, command)) {
                args.remove();                
                continue;
            } else {
                throw new UsageException("client.error.unknown_option",
                    BUNDLE, arg);
            }
        }
        
        /* Make sure all the required fields have been set. */
        if (requiredSwitches.size() != 0) {
            Field field = requiredSwitches.get(0);
            CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
            
            assert tag != null : "Field has no descriptor";
            
            throw new UsageException("client.error.missing_required_option",
                    BUNDLE, getName(field));
        }
    }
    
    /**
     * Returns a map relating switches to the fields which represent them.
     * @param clazz the command class
     * @return a parsed map of switches
     */
    private Map<String,Field> buildSwitchMap(Class clazz) {
        List<Field> fields = getAnnotatedFields(clazz);
        Map<String,Field> fieldMap = new HashMap<String,Field>();
        
        for (Field field : fields) {
            CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
            
            assert tag != null : "Field has no descriptor";

            fieldMap.put(getName(field), field);
            
            if (tag.hasShortcut()) {
                String shortcut = getShortcut(field);
                
                fieldMap.put(shortcut, field);
            }
        }
        
        return fieldMap;
    }
    
    /**
     * Returns a list of the fields which represent required switches.  The list
     * contains the fields instead of the switches themselves because a switch
     * may have more than one representation.
     * @param map the parse switch map
     * @return the list of required switches
     * @see #buildSwitchMap(Class)
     */
    private List<Field> getRequiredSwitches(Map<String,Field> map) {
        List<Field> ret = new LinkedList<Field>();
        
        for (Field field : map.values()) {
            CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
            
            assert tag != null : "Field has no descriptor";
            
            if (tag.required()) {
                ret.add(field);
            }
        }
        
        return ret;
    }
    
    /**
     * Reads the arguments for the given switch and sets the corresponding
     * field's value.
     * @param option the switch
     * @param args the command args
     * @param obj the command object
     */
    private void parseOption(Field option, Queue<String> args, Object obj) throws GrmException {
        CliOptionDescriptor tag = option.getAnnotation(CliOptionDescriptor.class);
        
        if (Modifier.isPrivate(option.getModifiers())) {
            option.setAccessible(true);
        }
        
        try {
            if (tag.numberOfOperands() == 0) {
                assert option.getType() == Boolean.TYPE : "Non-boolean no-arg option";
                option.setBoolean(obj, true);
            } else if (tag.numberOfOperands() == 1) {
                String arg = args.poll();
                
                if (arg == null) {
                    throw new UsageException("client.error.not_enough_args",
                        BUNDLE, getName(option),
                        1);
                }
                
                if (tag.list() == true) {
                    assert List.class.isAssignableFrom(option.getType()) : "Non-list option for list";
                
                    List<String> params = new LinkedList<String>(); 
                    String[] a = arg.split(",");
                    params = Arrays.<String>asList(a);
                    if (params.size() == 0) {
                        throw new UsageException("client.error.not_enough_args",
                        BUNDLE, getName(option),
                        1);
                    }                   
                    option.set(obj, params);
                    
                } else if (option.getType() == String.class) {
                    option.set(obj, arg);
                } else if (option.getType() == Integer.TYPE) {
                    try {
                        option.setInt(obj, Integer.parseInt(arg));
                    } catch (NumberFormatException e) {
                        throw new UsageException("client.error.bad_number",
                            BUNDLE, arg,
                            getName(option));
                    }
                } else if (option.getType() == Boolean.TYPE) {
                    option.setBoolean(obj, Boolean.parseBoolean(arg));

                } else if (option.getType() == Character.TYPE) {
                    if (arg.length() != 1) {
                        throw new UsageException("client.error.bad_char", BUNDLE, arg, getName(option));
                        
                    }
                    option.setChar(obj, arg.charAt(0));
                } else if (option.getType() == File.class) {
                    option.set(obj, new File(arg));
                } else {
                    throw new AssertionError("Unknown option type: " + option.getType());
                }
            } else {
                assert List.class.isAssignableFrom(option.getType()) : "Non-list multi-arg option";
                
                List<String> params = null;
                
                if (tag.numberOfOperands() > 1) {
                    params = new ArrayList<String>(tag.numberOfOperands());
                    
                    for (int i = 0; i < tag.numberOfOperands(); i++) {
                        String arg = args.poll();
                        
                        if (arg == null) {
                            throw new UsageException("client.error.not_enough_args",
                                BUNDLE, getName(option),
                                tag.numberOfOperands());
                        }
                        
                        params.add(arg);
                    }
                } else /* tag.numberOfOperands() < 0 */ {
                    params = new ArrayList<String>(args.size());
                    
                    while (args.size() > 0) {
                        params.add(args.poll());
                    }
                }
                
                option.set(obj, params);
            }
        } catch (IllegalAccessException e) {
            throw new AssertionError("Unable to access field: " + option.getName());
        }
    }
    
    /**
     * Parses a boolean option.
     * @param option the boolean switch
     * @param obj the command object
     */
    private boolean parseBooleanOption(Field option, Object obj) {
        CliOptionDescriptor tag = option.getAnnotation(CliOptionDescriptor.class);
        
        if (tag.numberOfOperands() != 0) {
            return false;
        }
        
        assert option.getType() == Boolean.TYPE : "Non-boolean no-arg option";
        
        if (Modifier.isPrivate(option.getModifiers())) {
            option.setAccessible(true);
        }
        
        try {
            option.setBoolean(obj, true);
        } catch (IllegalAccessException e) {
            throw new AssertionError("Unable to access field: " + option.getName());
        }
        
        return true;
    }
    
    
    /**
     * Reads a line from standard in.
     * @return the line read
     * @throws com.sun.grid.grm.GrmException if an I/O error occured
     */
    public String readLine() throws GrmException {
        if (stdin == null) {
            stdin = new BufferedReader(new InputStreamReader(System.in));
        }
        
        try {
            return stdin.readLine();
        } catch(IOException ex) {
            throw new GrmException("client.error.reading_input", BUNDLE, ex.getLocalizedMessage());
        }
    }
    
    /**
     * Name of the environment variable used for overwriting the system name option
     */
    public static final String SDM_SYSTEM_ENV_VARIABLE = "SDM_SYSTEM";
    /**
     * Evaluate the specified global parameters for finding the hedeby system
     * @param retPrefsType The first entry in this array will get the PreferencesType of the specified system
     * @return Name of the hedeby system
     * @throws com.sun.grid.grm.GrmException if the system name could not be evaluated
     */
    protected String evaluateSystemName(PreferencesType [] retPrefsType) throws GrmException {
        String realSystemName = null;
        
        if (systemName == null) {
            String nameFromEnv = System.getenv(SDM_SYSTEM_ENV_VARIABLE);
            if (nameFromEnv != null) {
                
                try {
                    SystemUtil.validateSystemName(nameFromEnv);
                    realSystemName = nameFromEnv;
                } catch (InvalidComponentNameException ex) {
                    throw new GrmException(ex.getLocalizedMessage(), ex);
                }
            }
            if (realSystemName == null) {
                realSystemName = PreferencesUtil.getDefaultSystemFromPrefs();
            }
        } else {
            try {
                SystemUtil.validateSystemName(systemName);
                realSystemName = systemName;
            } catch (InvalidComponentNameException ex) {
                throw new GrmException(ex.getLocalizedMessage(), ex);
            }
        }
        
        if (realSystemName != null) {
            PreferencesType aPrefsType = getPreferencesType();
            
            if (aPrefsType == PreferencesType.SYSTEM_PROPERTIES) {
                if (PreferencesUtil.existsSystem(realSystemName, PreferencesType.SYSTEM)) {
                    aPrefsType = PreferencesType.SYSTEM;
                    log.log(Level.FINE, "client.evaluateSystemName.use_system_prefs", realSystemName);
                } else if (PreferencesUtil.existsSystem(realSystemName, PreferencesType.USER)) {
                    aPrefsType = PreferencesType.USER;
                    log.log(Level.FINE, "client.evaluateSystemName.use_user_prefs", realSystemName);
                } else {
                    log.log(Level.FINE, "client.evaluateSystemName.system_not_found_in_prefs", realSystemName);
                    aPrefsType = null;
                }
            }
            
            retPrefsType[0] = aPrefsType;
            
            return realSystemName;
        } else {
            log.log(Level.FINE, "no usable system found, hopefully the show_configs command is executed");
            return null;
        }
    }
    
    /**
     * Evaluate the Execution environment for this client
     * This method currently checks only the system name
     * @throws com.sun.grid.grm.GrmException if the executor environment can not be constructed
     */
    public synchronized void evaluateExecutionEnv() throws GrmException {
        getExecEnv(true);
    }
    
    /**
     * Get the Execution environment for this client
     * @throws com.sun.grid.grm.cli.CliException if the executor environment can not be constructed
     * @return the execution environment
     */
    public synchronized ExecutionEnv getExecutionEnv() throws GrmException {
        return getExecEnv(false);
    }
    
    private PreferencesType executionPrefs = null;
    /**
     * Get preferences that were used to create the Execution Env
     * @return type of preferences used to create executor environment
     * @throws com.sun.grid.grm.GrmException if the executor environment can not be constructed
     */
    public synchronized PreferencesType getExecutionEnvPreferences() throws GrmException {
        if (executionPrefs == null) {
            // Initialize preferences type, 
            getExecutionEnv();
        }
        return executionPrefs;
    }
    /**
     * Recreate the execution environment for this client.
     *
     * This method is useful if a command has modified the preferences.
     *
     * @throws com.sun.grid.grm.GrmException if the executor environment can not be constructed
     * @return the execution environment
     */
    public synchronized ExecutionEnv recreateExecutionEnv() throws GrmException {
        env = createExecEnv(false);
        return env;
    }
    
    
    /**
     * This method is used to have exactly one 	place for the evaluating and
     * creating the ExecutionEnv. The public method getExecutionEnv() is used
     * for getting it and the method evaluateExecutionEnv() should be called
     * just for evaluating it.
     * More evaluating should be done here.
     */
    private synchronized ExecutionEnv getExecEnv(boolean onlyEvaluate) throws GrmException {
        if(env == null) {
            env = createExecEnv(onlyEvaluate);
        }
        return env;
    }
    
    private ExecutionEnv createExecEnv(boolean onlyEvaluate) throws GrmException {
        // Use system prefsType as default
        PreferencesType aPrefsType [] = new PreferencesType[1];
        String aName = evaluateSystemName(aPrefsType);
        
        if (aName == null) {
            throw new UsageException("client.error.need_system",
                BUNDLE);
        }
        
        if (onlyEvaluate == true) {
            return null;
        }
        
        ExecutionEnv ret = null;
        
        String username = System.getProperty("user.name");
        try {
            CallbackHandler callbackHandler = new CliCallbackHandler();
            
            if(aPrefsType[0] == null) {
                try {
                    ret = ExecutionEnvFactory.newClientInstance(aName, PreferencesType.SYSTEM, username, callbackHandler);
                    executionPrefs = PreferencesType.SYSTEM;
                } catch(PreferencesNotAvailableException ex) {
                    ret = ExecutionEnvFactory.newClientInstance(aName, PreferencesType.USER, username, callbackHandler);
                    executionPrefs = PreferencesType.USER;
                } catch(GrmException ex) {
                    ret = ExecutionEnvFactory.newClientInstance(aName, PreferencesType.USER, username, callbackHandler);
                    executionPrefs = PreferencesType.USER;
                } catch(BackingStoreException ex) {
                    ret = ExecutionEnvFactory.newClientInstance(aName, PreferencesType.USER, username, callbackHandler);
                    executionPrefs = PreferencesType.USER;
                }
            } else {
                ret = ExecutionEnvFactory.newClientInstance(aName, aPrefsType[0], username, callbackHandler);
                executionPrefs = aPrefsType[0];
            }
            
            return ret;
        } catch (PreferencesNotAvailableException e) {
            throw new GrmException("client.error.system_not_found", e, BUNDLE, aName);
        } catch (GrmSecurityException e) {
            throw new GrmException("client.error.ssl_error", e, BUNDLE, e.getLocalizedMessage());
        } catch (GrmException e) {
            throw new GrmException("client.error.system_context_failed", BUNDLE, aName);
        } catch(BackingStoreException e) {
            throw new GrmException("client.error.system_context_failed", e, BUNDLE, aName);
        }
    }
    
    
    private PreferencesType prefsType;
    
    /**
     *  Get the selected preferences from the command line (-prefs option).
     *  @return the selected preferences or <code>null</code> if no preferences
     *          are selected
     *  @throws com.sun.grid.grm.GrmException if the selected preferences type can not be parsed
     */
    public PreferencesType getPreferencesType() throws GrmException {
        if (prefs == null) {
            prefsType = PreferencesType.SYSTEM_PROPERTIES;
        } else if (prefsType == null) {
            try {
                prefsType = PreferencesType.valueOf(prefs.toUpperCase());
            } catch (IllegalArgumentException ex) {
                throw new GrmException("client.error.invalid_prefs_value", BUNDLE, prefs);
            }
        }
        
        return prefsType;
    }
    
    /**
     * Get the name of the CLI
     * @return the name of the CLI
     */
    public String getName() {
        return name;
    }
    
    
    
    /**
     * Get the name of the hedeby system
     * @return specified hedeby system name
     */
    public String getSystemName() {
        return systemName;
    }
    
    
    /**
     * Get the ResourceBundle of CLI command
     * @param tag CliCommandDesriptor tag
     * @param cl class loader
     * @return ResourceBundle
     */
    public static ResourceBundle getBundle(CliCommandDescriptor tag, ClassLoader cl) {
        try {
            return ResourceBundle.getBundle(tag.bundle(), Locale.getDefault(), cl);
        } catch(MissingResourceException ex) {
            throw new IllegalStateException("bundle " + tag.bundle() + " not found in " + cl, ex);
        }
    }
    
    /**
     * Get the ResourceBundle of a CLI command
     * @param field Field of a CLI command class
     * @return ResourceBundle
     */
    public static ResourceBundle getBundle(Field field) {
        Class<?> cls = field.getDeclaringClass();
        CliCommandDescriptor cmdTag  = cls.getAnnotation(CliCommandDescriptor.class);
        if(cmdTag == null) {
            return ResourceBundle.getBundle(BUNDLE);
        } else {
            ClassLoader cl = cls.getClassLoader();
            return getBundle(cmdTag, cl);
        }
    }
    
    /**
     * Returns the name of the command.
     * @param name the base name of the command
     * @return the name of the command
     */
    public static String getName(String name) {
        return getName(ResourceBundle.getBundle(BUNDLE), name);
    }
    
    /**
     * Returns the name of the command
     * @param bundle ResourceBundle of command
     * @param name base name of the command
     * @return name of the command
     */
    public static String getName(ResourceBundle bundle, String name) {
        return bundle.getString(name + ".name");
    }
    
    /**
     * Returns the name of the command
     * @param tag CliCommandDesriptor tag
     * @param cl class loader
     * @return the name of the command
     */
    public static String getName(CliCommandDescriptor tag, ClassLoader cl) {
        return getName(getBundle(tag, cl), tag.name());
    }
    
    /**
     * Returns the name of the command
     * @param field Field of a CLI command class
     * @return the name of the command
     */
    @SuppressWarnings("unchecked")
    public static String getName(Field field) {
        CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
        return getName(getBundle(field), tag.name());
    }
    
    private static String getCommandOptionsCategory(Class<?> cls) {
        if(AbstractSortedTableCliCommand.class.equals(cls) ||
           AbstractTableCliCommand.class.equals(cls)) {
           return ResourceBundle.getBundle(BUNDLE).getString("client.table_options"); 
        }
        return ResourceBundle.getBundle(BUNDLE).getString("client.command_options");
    }
    
    private static String getGlobalOptionsCategory() {
        return ResourceBundle.getBundle(BUNDLE).getString("client.global_options");   
    }
    
    private static String getCategory(CliCommandDescriptor tag) {
        if(tag.category() != null) {
            return getLocalizedCategoryName(tag.category());
        } else {
            throw new IllegalArgumentException("CLI command descriptor " + tag.name() + " has no category");
        }
    }
    
    /**
     * Get the localized name of the CliCategory.
     * 
     * @param category  the category
     * @return the localized name
     * @throws java.lang.IllegalArgumentException if the parameter category is null.
     * @throws java.lang.IllegalStateException  if no localization for category parameter exists
     * 
     */
    public static String getLocalizedCategoryName(CliCategory category) {
        if (category == null) {
            throw new IllegalArgumentException("parameter category must not be null");
        }
        ResourceBundle rb = ResourceBundle.getBundle(BUNDLE);
        
        switch (category) {
            case ADMINISTRATION:
                return rb.getString("client.admcommands");
            case COMPONENTS:
                return rb.getString("client.comcommands");
            case INSTALL:
                return rb.getString("client.instcommands");
            case MONITORING:
                return rb.getString("client.moncommands");
            case RESOURCES:
                return rb.getString("client.rescommands");
            case SECURITY:
                return rb.getString("client.seccommands");
            case SERVICES:
                return rb.getString("client.sercommands");
            case TEST:
                return rb.getString("client.testcommands");
            default:
                throw new IllegalStateException("CLI category " + category.name() + " is not localized");
            }
    }
    
    /**
     * Returns the shortcut of the command.
     * @param name the base name of the command
     * @return the shortcut of the command
     */
    public static String getShortcut(String name) {
        return getShortcut(ResourceBundle.getBundle(BUNDLE), name);
    }
    
    /**
     * Returns the shortcut of the command.
     * @param bundle ResourceBundle of command
     * @param name base name of the command
     * @return the shortcut of the command
     */
    public static String getShortcut(ResourceBundle bundle, String name) {
        return bundle.getString(name + ".shortcut");
    }
    
    
    /**
     * Returns the shortcut of the command.
     * @param tag CliCommandDesriptor tag
     * @param cl class loader
     * @return the shortcut of the command
     */
    public static String getShortcut(CliCommandDescriptor tag, ClassLoader cl) {
        if (tag.hasShortcut()) {
           return getShortcut(getBundle(tag, cl), tag.name());
        } else {
           return null;
        }
    }
    
    /**
     * Returns the shortcut of the command.
     * @param field Field of a CLI command class
     * @return the shortcut of the command
     */
    public static String getShortcut(Field field) {
        CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
        if (tag.hasShortcut()) {
           return getShortcut(getBundle(field), tag.name());
        } else {
           return null;
        }
    }
    
    
    /**
     * Returns the description of the command.
     * @param name the base name of the command
     * @return the description of the command
     */
    public static String getDescription(String name) {
        return getDescription(ResourceBundle.getBundle(BUNDLE), name);
    }
    
    /**
     * Returns the description of the command.
     * @param bundle ResourceBundle of command
     * @param name base name of the command
     * @return the description of the command
     */
    public static String getDescription(ResourceBundle bundle, String name) {
        return bundle.getString(name + ".description");
    }
    
    
    /**
     * Returns the description of the command.
     * @param tag CliCommandDesriptor tag
     * @param cl class loader
     * @return the description of the command
     */
    public static String getDescription(CliCommandDescriptor tag, ClassLoader cl) {
        return getDescription(getBundle(tag, cl), tag.name());
    }
    
    /**
     * Returns the description of the command.
     * @param field Field of a CLI command class
     * @return the description of the command
     */
    public static String getDescription(Field field) {
        CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
        return getDescription(getBundle(field), tag.name());
    }
    
    /**
     * Returns the long description of the command.
     * @param name the base name of the command
     * @return the long description of the command
     */
    public static String getLongDescription(String name) {
        return getLongDescription(ResourceBundle.getBundle(BUNDLE), name);
    }
    
    /**
     * Returns the long description of the command.
     * @param bundle ResourceBundle of command
     * @param name base name of the command
     * @return the long description of the command
     */
    public static String getLongDescription(ResourceBundle bundle, String name) {
        return bundle.getString(name + ".long_description");
    }
    
    /**
     * Get the description of the required privileges of this command
     * @param name
     * @return the description of the privileges
     */
    public static String getPrivilegesDescription(String name) {
        return getPrivilegesDescription(ResourceBundle.getBundle(BUNDLE), name);
    }
    
    /**
     * Get the description of the required privileges of this command
     * @param bundle the resource bundle of the command
     * @param name the base name of the command
     * @return the description of the privileges
     */
    public static String getPrivilegesDescription(ResourceBundle bundle, String name) {
        return bundle.getString(name + ".privileges_description");
    }
    
    /**
     * Get the description of the required privileges of this command
     * @param tag CliCommandDesriptor tag
     * @param cl class loader
     * @return the description of the privileges
     */
    public static String getPrivilegesDescription(CliCommandDescriptor tag, ClassLoader cl) {
        return getPrivilegesDescription(getBundle(tag, cl), tag.name());
    }
    
    /**
     * Returns the long description of the command.
     * @param tag CliCommandDesriptor tag
     * @param cl class loader
     * @return the long description of the command
     */
    public static String getLongDescription(CliCommandDescriptor tag, ClassLoader cl) {
        return getLongDescription(getBundle(tag, cl), tag.name());
    }
    
    /**
     * Returns the long description of the command.
     * @param field Field of a CLI command class
     * @return the long description of the command
     */
    public static String getLongDescription(Field field) {
        CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
        return getLongDescription(getBundle(field), tag.name());
    }
    
    /**
     * Returns the argument of the command.
     * @param name the base name of the command
     * @return the argument of the command
     */
    public static String getArgument(String name) {
        return getArgument(ResourceBundle.getBundle(BUNDLE), name);
    }
    
    /**
     * Returns the argument of the command.
     * @param bundle ResourceBundle of command
     * @param name base name of the command
     * @return the argument of the command
     */
    public static String getArgument(ResourceBundle bundle, String name) {
        return bundle.getString(name + ".argument");
    }
    
    /**
     * Returns the argument of the command.
     * @param tag CliCommandDesriptor tag
     * @param cl class loader
     * @return the argument of the command
     */
    public static String getArgument(CliCommandDescriptor tag, ClassLoader cl) {
        return getArgument(getBundle(tag, cl), tag.name());
    }
    
    /**
     * Returns the argument of the command.
     * @param field Field of a CLI command class
     * @return the argument of the command
     */
    public static String getArgument(Field field) {
        CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
        return getArgument(getBundle(field), tag.name());
    }
    
    
    /**
     *  The CliCallbackHandler is a ConsoleCallbackHandler which support
     *  also a FileCallback for ca certificate file and keystore file.
     */
    private class CliCallbackHandler extends ConsoleCallbackHandler {
        
        @Override
        protected void handle(Callback cb) throws IOException,
            UnsupportedCallbackException {
            
            if(cb instanceof FileCallback) {
                
                FileCallback fcb = (FileCallback)cb;
                
                switch(fcb.getType()) {
                    case SecurityConstants.CACERT_FILE_CALLBACK:
                        fcb.setFile(caCertFile);
                        return;
                    case SecurityConstants.KEYSTORE_FILE_CALLBACK:
                        fcb.setFile(keyStoreFile);
                        return;
                }
            } else if (cb instanceof PasswordCallback) {
                if(getPromptPW()) {
                    super.handle(cb);
                    return;
                } else {
                    ((PasswordCallback)cb).setPassword(new char[0]);
                    return;
                }
            } else if (cb instanceof NameCallback) {
                if(getPromptPW()) {
                    super.handle(cb);
                    return;
                } else {
                    ((NameCallback)cb).setName(System.getProperty("user.name"));
                    return;
                }
            }
            super.handle(cb);
        }
    }
    
    
    /**
     * Used to associate the argument and description of a command so they
     * can be inserted as a value in a Map.
     */
    private class CommandInfo {
        String category;
        String arg;
        String desc;
        String shortcut;
        
        CommandInfo(Class<? extends CliCommand> cmd) {
            CliCommandDescriptor tag = cmd.getAnnotation(CliCommandDescriptor.class);
            arg = getName(tag, cmd.getClassLoader());
            desc = getDescription(tag, cmd.getClassLoader());
            shortcut = getShortcut(tag, cmd.getClassLoader());
            if(shortcut == null) {
                shortcut = "";
            }
            category = getCategory(tag);
            if (category == null) {
                throw new IllegalStateException("category is NULL");
            }
        }
        
        CommandInfo(String category, Field field) {
            this.category = category;
            if (category == null) {
                throw new IllegalStateException("category is NULL");
            }
            CliOptionDescriptor tag = field.getAnnotation(CliOptionDescriptor.class);
            if(tag == null) {
                throw new IllegalStateException("Field " + field + " has no descriptor");
            }
            shortcut = getShortcut(field);
            if(shortcut == null) {
                shortcut = "";
            }
            desc = getDescription(field);
            
            String name = getName(field);

            if (tag.numberOfOperands() != 0) {
                arg = name + " " + getArgument(field);
            } else {
                arg = name;
            }
        }
        
        /**
         * Creates new CommandInfo class.
         * @param arg the command arg
         * @param desc the command description
         */
        CommandInfo(String arg, String shortcut, String desc, String category) {
            this.arg = arg;
            this.desc = desc;
            this.shortcut = shortcut;
            this.category = category;
            if (category == null) {
                throw new IllegalStateException("category is NULL");
            }
        }
    }
    
    /**
     * Creates new CommandInfo class for global help with specified category.
     * @param category command category name
     * @return new CommandInfo Instance
     */
    CommandInfo newHelpCommandInfo(String category) {
        CommandInfo ret = new CommandInfo(getName(HELP_OPTION),
                getShortcut(HELP_OPTION),
                getDescription(HELP_OPTION),
                category);
        return ret;
    }

    /**
     * Creates new CommandInfo class for version with specified category.
     * @param category command category name
     * @return new CommandInfo Instance
     */
    CommandInfo newVersionCommandInfo(String category) {
        CommandInfo ret = new CommandInfo(getName(VERSION_OPTION),
                getShortcut(VERSION_OPTION),
                getDescription(VERSION_OPTION),
                category);
        return ret;
    }
    
    /**
     * Creates new CommandInfo class for command help with specified category.
     * @param category command category name
     * @return new CommandInfo Instance
     */
    CommandInfo newCommandHelpCommandInfo(String category) {
        CommandInfo ret = new CommandInfo(getName(COMMAND_HELP_OPTION),
                getShortcut(COMMAND_HELP_OPTION),
                getDescription(COMMAND_HELP_OPTION),
                category);
        return ret;
    }
    
    /**
     * Returns the debug flag value
     * @return value of the debug option
     */
    public boolean getDebug(){
        return this.debug;
    }
    
    /**
     * Get current value of -ppw option
     * @return value of the prompt for password option
     */
    public boolean getPromptPW() {
        return this.promptPW;
    }

    /**
     * Set current value of -ppw option, overwrite value given by user     *
     */
    public void setPromptPW(boolean promptPW) {
       this.promptPW = promptPW;
    }
    
    /**
     * Get stdout of the CLI.
     * @return the stdout print writer
     */
    public I18NPrintWriter out() {
        return out;
    }
    
    /**
     * Get stderr of the CLI.
     * @return the stderr print writer
     */
    public I18NPrintWriter err() {
        return err;
    }
    
    /**
     * Get the map with available categories of the CLI commands as key and
     * set of CLI commands belonging to the CLI category as value.
     * 
     * <p>The returned map is a SortedMap. The keys are sorted by the localized names
     * of the CLI command category.</p>
     * 
     * <p>The sets of CLI commands stored as values in the returned map are sorted by the
     *    localized name of the CLI command</p>
     * 
     * <p><b>Attention:</b> This method is used for the man page generation. The "Find Usage"
     *    tool of Netbeans will detect that this method is never used. However this method
     *    is used for generation of man pages.</p>
     * 
     * @return command category map.
     */
    public Map<CliCategory, Set<Class<? extends CliCommand>>> getSortedCategoryCommandMap() {

        // The categoryComparator compare two instances of CliCategory by it's localized name
        Comparator<CliCategory> categoryComparator = new Comparator<CliCategory>() {
            public int compare(CliCategory c1, CliCategory c2) {
                String n1 = getLocalizedCategoryName(c1);
                String n2 = getLocalizedCategoryName(c2);
                return n1.compareTo(n2);
            }
        };

        // This commandComparator compares two CliCommand classes by it's localized name
        Comparator<Class<? extends CliCommand>> commandComparator = new Comparator<Class<? extends CliCommand>>() {

            public int compare(Class<? extends CliCommand> c1, Class<? extends CliCommand> c2) {
                CliCommandDescriptor d1 = c1.getAnnotation(CliCommandDescriptor.class);
                CliCommandDescriptor d2 = c2.getAnnotation(CliCommandDescriptor.class);

                String n1 = AbstractCli.getName(d1, c1.getClassLoader());
                String n2 = AbstractCli.getName(d2, c2.getClassLoader());
                return n1.compareTo(n2);
            }
        };

        TreeMap<CliCategory, Set<Class<? extends CliCommand>>> categoryMap = new TreeMap<CliCategory, Set<Class<? extends CliCommand>>>(categoryComparator);
        for (Class<? extends CliCommand> cmd : this.getCommands()) {
            CliCommandDescriptor descr = cmd.getAnnotation(CliCommandDescriptor.class);
            Set<Class<? extends CliCommand>> cmdList = categoryMap.get(descr.category());
            if (cmdList == null) {
                cmdList = new TreeSet<Class<? extends CliCommand>>(commandComparator);
                categoryMap.put(descr.category(), cmdList);
            }
            cmdList.add(cmd);
        }

        return categoryMap;
    }
    
}
