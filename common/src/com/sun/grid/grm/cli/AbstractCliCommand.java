/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli;

import com.sun.grid.grm.util.I18NManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ResourceBundle;

/**
 *   Abstract base clase for cli commands
 */
public abstract class AbstractCliCommand implements CliCommand {

    private static final String BUNDLE = "com.sun.grid.grm.cli.client";
    
    /**
     * Get the descriptor of the cli command.
     * 
     * @return the descriptor of the cli command
     */
    public CliCommandDescriptor getDescriptor() {
        return getDescriptor(getClass());
    }

    /**
     * Get the descriptor of the cli command.
     * 
     * @param  clazz  the class of the cli command
     * @return the descriptor of the cli command
     */
    public static CliCommandDescriptor getDescriptor(Class<? extends CliCommand> clazz) {
        CliCommandDescriptor ret = clazz.getAnnotation(CliCommandDescriptor.class);
        if (ret == null) {
            throw new IllegalStateException(I18NManager.formatMessage("AbstractCliCommand.nodescr", BUNDLE, clazz.getName()));
        }
        return ret;
    }

    /**
     * Get the name of the resource bundle from the cli command descriptor.
     * @return the name of the resource bundle
     */
    public String getBundleName() {
        return getBundleName(getClass());
    }

    /**
     * Get the name of the resource bundle from the cli command descriptor
     * of the <code>CliCommand</code> class.
     * 
     * @param clazz  the <code>CliCommand</code> class
     * @return the name of the resource bundle
     */
    public static String getBundleName(Class<? extends CliCommand> clazz) {
        return getDescriptor(clazz).bundle();
    }

    /**
     * Get the name of the resource bundle from the cli command descriptor.
     * @return the resource bundle
     */
    public ResourceBundle getBundle() {
        return getBundle(getClass());
    }

    /**
     * Get the resource bundle from the cli command descriptor
     * of the <code>CliCommand</code> class.
     * 
     * @param clazz  the <code>CliCommand</code> class
     * @return the resource bundle
     */
    public static ResourceBundle getBundle(Class<? extends CliCommand> clazz) {
        return ResourceBundle.getBundle(getBundleName(clazz));
    }
    
    /**
     * This helper method allows to convert from Throwable error stack trace 
     * to the String representation
     * @param error Throwable from which stack trace is taken
     * @return String representation of Throwable stacktrace
     */
    public static String errorStackTraceToString(Throwable error) {
        String ret = "";
        if (error != null) {
            Writer res = new StringWriter();
            PrintWriter pw = new PrintWriter(res);
            error.printStackTrace(pw);
            ret = res.toString();
        }
        return ret;
    }
}
