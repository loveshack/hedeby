/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/

/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.resource.ResourceIdAndName;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.resource.ShowBlackListCommand;
import com.sun.grid.grm.ui.resource.ShowBlackListResult;
import java.util.ArrayList;
import java.util.List;


/**
 * This command is responsible for showing the list of resource id that are 
 * ignored by a service (eg. they are on service's resource blacklist).
 */
@CliCommandDescriptor(name = "ShowBlackListCliCommand", hasShortcut = true, category = CliCategory.MONITORING, bundle = "com.sun.grid.grm.cli.cmd.monitoring.messages",
 requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR})
public class ShowBlackListCliCommand extends AbstractSortedTableCliCommand {

    @CliOptionDescriptor(name = "ShowBlackListCliCommand.s", required = false, numberOfOperands = 1)
    private String service;
    
    public void execute(AbstractCli cli) throws GrmException {
        String bundle = getBundleName();
        ShowBlackListCommand sb = new ShowBlackListCommand(service);
        List<ShowBlackListResult> result = cli.getExecutionEnv().getCommandService().execute(sb).getReturnValue();

        if (result.isEmpty()) {
            cli.out().println("ShowBlackListCliCommand.bl.none", bundle);

            return;
        }

        TableModel tm = new TableModel(cli, result);
        Table table = createTable(tm, cli);
        table.print(cli);
    }

    private class TableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = -2007122101L;
        private List<Line> rows;
        
        public TableModel(AbstractCli aCli,
                List<ShowBlackListResult> results) {
            super(false,true,false,aCli.getDebug());
            rows = new ArrayList<Line>(results.size());

            for (ShowBlackListResult entry : results) {

                for (ResourceIdAndName res : entry.getResources()) {
                    rows.add(new Line(entry.getServiceName(), res.getName(), entry.getError()));
                }
            }
        }

        public Line getRow(int row) {
            return rows.get(row);
        }

        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 2;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Line line = rows.get(rowIndex);

            switch (columnIndex) {
                case 0:
                    return line.service;

                case 1:
                    return line.resourceid;

                default:
                    throw new IllegalArgumentException("unknown column " +
                            columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("ShowBlackListCliCommand.col.service");

                case 1:
                    return getBundle().getString("ShowBlackListCliCommand.col.resource");

                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            Line l = getRow(row);
            return errorStackTraceToString(l.error);
        }
    }
    
    private static class Line {

        private String service;
        private String resourceid;
        private Throwable error;

        public Line(String aService, String aResourceId, Throwable aError) {
            service = aService;
            resourceid = aResourceId;
            error = aError;
        }
    }
}
