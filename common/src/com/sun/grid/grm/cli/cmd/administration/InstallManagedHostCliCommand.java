/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.administration;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.install.AddSystemCommand;
import com.sun.grid.grm.ui.install.CheckSMFSupportCommand;
import com.sun.grid.grm.ui.install.CheckSimpleInstallationCommand;
import com.sun.grid.grm.ui.install.InstallManagedHostCommand;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CLI implmentation class for installing a managed host
 */
@CliCommandDescriptor(
    name = "InstallManagedHostCliCommand",
    hasShortcut = true,
    category = CliCategory.INSTALL,
    bundle="com.sun.grid.grm.cli.cmd.administration.messages",
    requiredPrivileges = { UserPrivilege.WRITE_BOOTSTRAP_CONFIG, UserPrivilege.WRITE_LOCAL_SPOOL_DIR, UserPrivilege.ADMINISTRATOR},
    optionalPrivileges= { UserPrivilege.SUPER_USER })
public class InstallManagedHostCliCommand extends AbstractCliCommand {

    private static final Logger log = Logger.getLogger(InstallManagedHostCliCommand.class.getName());
    
    @CliOptionDescriptor(
    name = "InstallManagedHostCliCommand.l",
            numberOfOperands = 1
            )
    private String localSpoolPath;
    
    
    @CliOptionDescriptor(
    name = "InstallManagedHostCliCommand.d",
            numberOfOperands = 1
            )
    private String distDirPath;
    
    @CliOptionDescriptor(
    name = "InstallManagedHostCliCommand.a",
            numberOfOperands = 1,
            required = true
            )
    private String adminUser;
    
    @CliOptionDescriptor(
    name = "InstallManagedHostCliCommand.cs_url",
            numberOfOperands = 1,
            required = true
            )
    private String csURL;
    
    @CliOptionDescriptor(
    name = "InstallManagedHostCliCommand.sslDisabled",
            required = false
            )
    private boolean sslDisabled;
    
    @CliOptionDescriptor(
    name = "InstallManagedHostCliCommand.autostart",
            required = false
            )
    private boolean autostart;
    
    
    /**
     * Execute the operation of installing a managed host
     * @param cli CLI envirionment
     * @throws com.sun.grid.grm.GrmException if the command failed
     */
    public void execute(AbstractCli cli) throws GrmException {
        boolean spoolExists = true;

        //Issue 699 - check for exclusive options, autostart and user preferences
        if (autostart && (cli.getPreferencesType().equals(PreferencesType.USER))) {
            throw new GrmException("InstallManagedHostCliCommand.error.autostart_userprefs", getBundleName());
        }
        // Evaluate
        cli.evaluateExecutionEnv();
        
        if (distDirPath == null) {
            distDirPath = PathUtil.getDefaultDistDir().getAbsolutePath();
        }
        
        if (localSpoolPath == null) {
            localSpoolPath = PathUtil.getDefaultSpoolDir(cli.getSystemName());
            log.log(Level.INFO, I18NManager.formatMessage("InstallManagedHostCliCommand.defaultSpool", getBundleName(), localSpoolPath));
        }
        
        AddSystemCommand addSystemCmd = new AddSystemCommand(cli.getSystemName(),
                csURL, localSpoolPath, distDirPath, cli.getPreferencesType(), false);
        addSystemCmd.setSslDisabled(sslDisabled);
        addSystemCmd.setAutostart(autostart);
        ExecutionEnv env = ExecutionEnvFactory.newNullInstance();
        env.getCommandService().execute(addSystemCmd);
        

        // Now we have a system. We create an ExecutionEnv ...
        ExecutionEnv newEnv = cli.recreateExecutionEnv();

        boolean smf = false;
        boolean auto = SystemUtil.isAutoStart(newEnv);
        boolean simple = false;
        try {
            if (auto) {
                CheckSMFSupportCommand checkCmd = new CheckSMFSupportCommand();
                smf = newEnv.getCommandService().<Boolean>execute(checkCmd).getReturnValue();
            }
            //check is simple mode
            CheckSimpleInstallationCommand checkCmd = new CheckSimpleInstallationCommand(cli.getPreferencesType());
            simple = newEnv.getCommandService().<Boolean>execute(checkCmd).getReturnValue();
            // Ok we can setup the install macro command ...
            InstallManagedHostCommand mhiCmd = new InstallManagedHostCommand(adminUser, auto, smf, simple, cli.getPreferencesType());
            
            /* We are installing a new system, so the spool dir for this system should not exist
             * so if it exists the installation fails, we do this check because if we know that
             * spool dir does not exists before installation we can delete safely if installation fails
             * on any steps
             */
             spoolExists = newEnv.getLocalSpoolDir().isDirectory();
            // Ok, finally we can start install managed host command ...
            newEnv.getCommandService().execute(mhiCmd);
        } catch (GrmException ex) {
            //If error occured we need to revert AddSystem, so we call undo system
            
            cli.out().println("install_failed.remove_system", getBundleName(), newEnv.getSystemName());
            if(cli.getDebug()) {
                ex.printStackTrace(cli.err().getPrintWriter());
            }
            addSystemCmd.undo(newEnv);
            
            /**
             * If spool dir before installation did not exist it should be removed including
             * its contents
             */
            if (!spoolExists) {
                cli.out().println("install_failed.remove_spoolDir", getBundleName(), newEnv.getSystemName(), newEnv.getLocalSpoolDir());
                //try to remove create spool dir with its contents
                try {
                    Platform.getPlatform().removeDir(newEnv.getLocalSpoolDir(), true);
                } catch(Exception ex1) {
                    cli.out().println("install_cleanup_failed.remove_spoolDir", getBundleName(), newEnv.getLocalSpoolDir());
                    if(cli.getDebug()) {
                        ex1.printStackTrace(cli.err().getPrintWriter());
                    }
                }
                cli.out().println("install_failed.removed_spoolDir", getBundleName(), newEnv.getLocalSpoolDir());
            }
            throw ex;
        }
    }
}