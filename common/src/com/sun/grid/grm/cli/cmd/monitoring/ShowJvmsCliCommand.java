/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.component.service.GetComponentInfosCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.AndFilter;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This command show all running jvms in Hedeby system.
 * 
 */

@CliCommandDescriptor(
    name = "ShowJvmsCliCommand",
    hasShortcut = true,
    category = CliCategory.MONITORING,
    bundle="com.sun.grid.grm.cli.cmd.monitoring.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ShowJvmsCliCommand extends AbstractSortedTableCliCommand {
    
    @CliOptionDescriptor(name = "ShowJvmsCliCommand.j", numberOfOperands = 1, required = false)
    private String name = null;
    @CliOptionDescriptor(name = "ShowJvmsCliCommand.h", numberOfOperands = 1, required = false)
    private String host = null;
    

    public void execute(AbstractCli cli) throws GrmException {
        
        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>(3);
        filter.add(GetComponentInfosCommand.newTypeFilter(JVM.class));
        if(name != null) {
            filter.add(GetComponentInfosCommand.newRegExpNameFilter(name));
        }
        if(host != null) {
            /* resolve hostname in case of 'localhost' */       
            filter.add(GetComponentInfosCommand.newHostFilter(Hostname.getInstance(host)));
        }
        
        List<ComponentInfo> componentList = ComponentService.getComponentInfos(cli.getExecutionEnv(), filter);
        
        TableModel tm = new TableModel(cli, componentList);
        
        List<String> order = new ArrayList<String>(2);
        order.add(tm.getColumnName(0));
        order.add(tm.getColumnName(1));
        setDeafultColumnSortOrder(order);
        
        Table table = createTable(tm, cli);
        if (!isSetMachineReadableOutput()) {
            table.setAutoWordWrap(5, true);
        }
              
        table.setUnit(getBundle().getString("ShowJvmCliCommand.col.usedMem"), "M");
        table.setFlags(getBundle().getString("ShowJvmCliCommand.col.usedMem"), "");
        table.setUnit(getBundle().getString("ShowJvmCliCommand.col.maxMem"), "M");
        table.setFlags(getBundle().getString("ShowJvmCliCommand.col.maxMem"), "");
               
        table.print(cli);
    }
    
    private class TableModel extends AbstractDefaultTableModel {
        
        private final static long serialVersionUID = -20072901L;
        
        private final List<ComponentInfo> componentList;
        private final Map<Integer, StateHolder> stateList;
        private final AbstractCli cli;
        
        public TableModel(AbstractCli cli, List<ComponentInfo> aComponentList) throws GrmException {
            super(false,true,false,cli.getDebug());
            this.cli = cli;
            componentList = aComponentList;
            stateList = new HashMap<Integer,StateHolder>(componentList.size());
        }
        
        public int getRowCount() {
            return componentList.size();
        }

        
        public int getColumnCount() {
            return 6;
        }
        
        private StateHolder getStateHolder(int rowIndex) {
            StateHolder ret = stateList.get(rowIndex);
            if (ret == null) {
                ret = new StateHolder();
                try {
                    ComponentInfo ci = componentList.get(rowIndex);
                    JVM jvm = ComponentService.<JVM>getComponent(cli.getExecutionEnv(), ci);
                    ret.state = jvm.getState().toString();
                    ret.usedMem = jvm.getUsedMem() / (1024*1024);
                    ret.maxMem  = jvm.getMaxMem() / (1024*1024);
                    ret.message = "";
                } catch (Throwable ex) {
                    ret.state = ComponentState.UNKNOWN.toString();
                    if (ex instanceof UndeclaredThrowableException) {
                        ret.error = ((UndeclaredThrowableException) ex).getUndeclaredThrowable();
                    } else {
                        ret.error = ex;
                    }
                    ret.message = I18NManager.formatMessage("ShowJvmCliCommand.err", getBundleName(), ret.error.getLocalizedMessage());
                }
                
                stateList.put(rowIndex, ret);
            }
            return ret;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            switch(columnIndex) {
                case 0:  return componentList.get(rowIndex).getName();
                case 1:  return componentList.get(rowIndex).getHostname().getHostname();
                case 2:  return getStateHolder(rowIndex).state;
                case 3:  return getStateHolder(rowIndex).usedMem;
                case 4:  return getStateHolder(rowIndex).maxMem;
                case 5:  return getStateHolder(rowIndex).message;
                default:
                    throw new IllegalStateException("Unknown column " + columnIndex);
            }
        }

        @Override
        public String getColumnName(int column) {
            switch(column) {
                case 0: return getBundle().getString("ShowJvmCliCommand.col.name");
                case 1: return getBundle().getString("ShowJvmCliCommand.col.host");
                case 2: return getBundle().getString("ShowJvmCliCommand.col.state");
                case 3: return getBundle().getString("ShowJvmCliCommand.col.usedMem");
                case 4: return getBundle().getString("ShowJvmCliCommand.col.maxMem");
                case 5: return getBundle().getString("ShowJvmCliCommand.col.message");
                default:
                    throw new IllegalStateException("Unknown column " + column);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch(columnIndex) {
                case 3: return Long.class;
                case 4: return Long.class;
                default:
                     return String.class;
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            StateHolder elem = stateList.get(row);
            return errorStackTraceToString(elem.error);
        }
    }
    
    private static class StateHolder {
        String state;
        Throwable error;
        long usedMem;
        long maxMem;
        String message;
    }
    
}
    
