/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.UsageException;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.ui.component.service.GetServiceNeedsCommand;
import com.sun.grid.grm.ui.component.service.GetServiceResourceUsageCommand;
import com.sun.grid.grm.ui.component.service.ServiceNeed;
import com.sun.grid.grm.ui.component.service.ServiceResourceUsage;
import java.util.List;

/**
 *
 */
@CliCommandDescriptor(name = "ShowSLOCliCommand", hasShortcut = true, category = CliCategory.MONITORING, bundle = "com.sun.grid.grm.cli.cmd.monitoring.messages",
   requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ShowSLOCliCommand extends AbstractSortedTableCliCommand {

    @CliOptionDescriptor(name = "ShowSLOCliCommand.s", numberOfOperands = 1, required = false)
    private String service = null;
    
    @CliOptionDescriptor(name = "ShowSLOCliCommand.u", required = false)
    private boolean showUsage;
    
    @CliOptionDescriptor(name = "ShowSLOCliCommand.r", numberOfOperands = 1, required = false)
    private String resource = null;
    

    private final String na = getBundle().getString("ShowSLOCliCommand.na");
    
    public void execute(AbstractCli cli) throws GrmException {
        if(showUsage) {
            GetServiceResourceUsageCommand cmd = new GetServiceResourceUsageCommand();
            if (service != null) {
                cmd.setServiceName(service);
            }
            if (resource != null) {
                if(!showUsage) {
                    throw new UsageException("ShowSLOCliCommand.r_needs_u", getBundleName());
                }
                cmd.setResourceFilter(resource);
            }
            List<ServiceResourceUsage> res = cli.getExecutionEnv().getCommandService().execute(cmd).getReturnValue();
            
            UsageTableModel tm = new UsageTableModel(cli, res);
            Table table = createTable(tm, cli);
            if (!isSetMachineReadableOutput()) {
                if(tm.hasError()) {
                    table.setColumnWidth(4, 30);
                    table.setAutoWordWrap(4, true);
                } 
            }
            table.print(cli);
        } else {
            GetServiceNeedsCommand cmd = new GetServiceNeedsCommand();
            if (service != null) {
                cmd.setServiceName(service);
            }
            List<ServiceNeed> slos = cli.getExecutionEnv().getCommandService().execute(cmd).getReturnValue();

            NeedTableModel tm = new NeedTableModel(cli, slos);
            Table table = createTable(tm, cli);
            if (!isSetMachineReadableOutput()) {
                table.setAutoWordWrap(4, true);
                table.setColumnWidth(4, 50);
            }
            table.print(cli);
        }
    }
    
    private class UsageTableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = -2007121701L;
        
        private final List<ServiceResourceUsage> rows;
        private boolean hasError;
        private int colCount;
        private final AbstractCli cli;
        
        public UsageTableModel(AbstractCli cli, List<ServiceResourceUsage> rows) {
            super(false,true,false,cli.getDebug());
            this.cli = cli;
            this.rows = rows;
            for(ServiceResourceUsage row: rows) {
                if(row.getError() != null) {
                    hasError = true;
                    break;
                }
            }
            if(hasError) {
                colCount = 5;
            } else {
                colCount = 4;
            }
        }
        
        public boolean hasError() {
            return hasError;
        }
        
        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return colCount;
        }

        @Override
        public Class<?> getColumnClass(int column) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch(column) {
                case 0: return getBundle().getString("ShowSLOCliCommand.col.service");
                case 1: return getBundle().getString("ShowSLOCliCommand.col.slo");
                case 2: return getBundle().getString("ShowSLOCliCommand.col.resource");
                case 3: return getBundle().getString("ShowSLOCliCommand.col.usage");
                case 4: return getBundle().getString("ShowSLOCliCommand.col.error");
                default: throw new IllegalArgumentException("Unknown column " + column);
            }
        }
        
        public Object getValueAt(int row, int col) {
            ServiceResourceUsage rowObj = rows.get(row);
            switch (col) {
                case 0:
                    return rowObj.getServiceName();
                case 1:
                    return rowObj.getSloName() == null ? na : rowObj.getSloName();
                case 2: 
                    return rowObj.getResourceName() == null ? na: rowObj.getResourceName();
                case 3:
                    return rowObj.getUsage() == null ? 0: rowObj.getUsage().toString();
                case 4:
                    return rowObj.getError() == null ? "": rowObj.getError().getLocalizedMessage();
                default:
                    throw new IllegalArgumentException("Unknown column " + col);
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            ServiceResourceUsage r = rows.get(row);
            return errorStackTraceToString(r.getError());
        }
        
    }
    

    private class NeedTableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = -2007121701L;
        
        private final List<ServiceNeed> rows;
        private final AbstractCli cli;
        public NeedTableModel(AbstractCli cli, List<ServiceNeed> rows) {
            super(false,true,false,cli.getDebug());
            this.cli = cli;
            this.rows = rows;
        }
        
        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 5;
        }

        @Override
        public Class<?> getColumnClass(int column) {
            switch(column) {
                case 2: return Integer.class;
                case 3: return Integer.class;
                default: return String.class;
            }
        }

        @Override
        public String getColumnName(int column) {
            switch(column) {
                case 0: return getBundle().getString("ShowSLOCliCommand.col.service");
                case 1: return getBundle().getString("ShowSLOCliCommand.col.slo");
                case 2: return getBundle().getString("ShowSLOCliCommand.col.quantity");
                case 3: return getBundle().getString("ShowSLOCliCommand.col.urgency");
                case 4: return getBundle().getString("ShowSLOCliCommand.col.request");
                default: throw new IllegalArgumentException("Unknown column " + column);
            }
        }
        
        public Object getValueAt(int row, int col) {
            ServiceNeed rowObj = rows.get(row);
            switch (col) {
                case 0:
                    return rowObj.getServiceName();
                case 1:
                    return rowObj.getSloName() == null ? na : rowObj.getSloName();
                case 2:  return rowObj.getNeed() == null ? 0: rowObj.getNeed().getQuantity();
                case 3:
                    return rowObj.getNeed() == null ? 0: rowObj.getNeed().getUrgency().getLevel();
                case 4:
                {
                    if(rowObj.getError() != null) {
                        return rowObj.getError().getLocalizedMessage();
                    } else {
                        Need need = rowObj.getNeed();
                        if(need == null) {
                            return getBundle().getString("ShowSLOCliCommand.noNeed");
                        } else if (need.getResourceFilter() != null) {
                            return rowObj.getNeed().getResourceFilter().toString();
                        } else {
                            return "";
                        }
                    }
                }
                default:
                    throw new IllegalArgumentException("Unknown column " + col);
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            ServiceNeed r = rows.get(row);
            return errorStackTraceToString(r.getError());
        }
        
    }
    
    
}
