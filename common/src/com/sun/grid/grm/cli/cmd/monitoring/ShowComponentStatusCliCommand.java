/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.ui.component.service.GetComponentInfosCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.NotFilter;
import com.sun.grid.grm.util.filter.OrFilter;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.List;

/**
 * Command that shows the full system status.
 */
@CliCommandDescriptor(name = "ShowComponentStatusCliCommand", hasShortcut = true, category = CliCategory.MONITORING, bundle = "com.sun.grid.grm.cli.cmd.monitoring.messages",
  requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR} )
public class ShowComponentStatusCliCommand extends AbstractSortedTableCliCommand {

    public enum Column {
        HostCol,
        JvmCol,
        NameCol,
        StateCol,
        TypeCol
    }
    private final static String BUNDLE = "com.sun.grid.grm.cli.cmd.monitoring.messages";
    @CliOptionDescriptor(name = "ShowComponentStatusCliCommand.c", numberOfOperands = 1, required = false)
    private String name = null;
    @CliOptionDescriptor(name = "ShowComponentStatusCliCommand.j", numberOfOperands = 1, required = false)
    private String jvm = null;
    @CliOptionDescriptor(name = "ShowComponentStatusCliCommand.h", numberOfOperands = 1, required = false)
    private String host = null;
    @CliOptionDescriptor(name = "ShowComponentStatusCliCommand.t", numberOfOperands = 1, required = false)
    private String type = null;
    
    /**
     * Execute the cli command
     * @param cli instance of cli
     * @throws com.sun.grid.grm.GrmException of the command failed
     */
    public void execute(AbstractCli cli) throws GrmException {

        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>(5);

        if (type != null) {
            if (type.equalsIgnoreCase(ResourceProvider.class.getSimpleName())) {
                filter.add(GetComponentInfosCommand.newTypeFilter(ResourceProvider.class));
            } else if (type.equalsIgnoreCase(Service.class.getSimpleName())) {
                filter.add(GetComponentInfosCommand.newTypeFilter(Service.class));
            } else if (type.equalsIgnoreCase(Executor.class.getSimpleName())) {
                filter.add(GetComponentInfosCommand.newTypeFilter(Executor.class));
            } else if (type.equalsIgnoreCase(getBundle().getString("ShowComponentStatusCliCommand.other"))) {
                OrFilter<ComponentInfo> orFilter = new OrFilter<ComponentInfo>(3);
                orFilter.add(GetComponentInfosCommand.newTypeFilter(ResourceProvider.class));
                orFilter.add(GetComponentInfosCommand.newTypeFilter(Service.class));
                orFilter.add(GetComponentInfosCommand.newTypeFilter(Executor.class));
                filter.add(new NotFilter<ComponentInfo>(orFilter));
            }
        } else {
            filter.add(GetComponentInfosCommand.newTypeFilter(GrmComponent.class));
        }
        // We don't want the JVMs in the output
        filter.add(new NotFilter<ComponentInfo>(GetComponentInfosCommand.newTypeFilter(JVM.class)));

        if (name != null) {
            filter.add(GetComponentInfosCommand.newRegExpNameFilter(name));
        }
        if (jvm != null) {
            filter.add(GetComponentInfosCommand.newRegExpJvmFilter(jvm));
        }
        if (host != null) {
            /* resolve hostname in case of 'localhost' */
            filter.add(GetComponentInfosCommand.newHostFilter(Hostname.getInstance(host)));
        }

        List<ComponentInfo> componentList = ComponentService.getComponentInfos(cli.getExecutionEnv(), filter);

        MyTableModel tm = new MyTableModel(cli, componentList.size());

        ExecutionEnv env = cli.getExecutionEnv();

        for (ComponentInfo info : componentList) {
            Elem elem = new Elem(env, info);
            tm.add(elem);
        }
        
        List<String> order = new ArrayList<String>(4);
        order.add(tm.getColumnName(0));
        order.add(tm.getColumnName(1));
        order.add(tm.getColumnName(2));
        order.add(tm.getColumnName(3));
        setDeafultColumnSortOrder(order);
        
        Table table = createTable(tm, cli);
        if (!isSetMachineReadableOutput()) {
            table.setColumnWidth(4, 12);
        }
        table.print(cli);
    }

    private class MyTableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = -200711290L;
        private final Column cols[] = {Column.HostCol, Column.JvmCol, Column.NameCol, Column.TypeCol, Column.StateCol};
        private final ArrayList<Elem> elems;
        private final AbstractCli cli;

        public MyTableModel(AbstractCli cli, int cap) {
            super(false,true,false,cli.getDebug());
            this.cli = cli;
            elems = new ArrayList<Elem>(cap);
        }

        public int getRowCount() {
            return elems.size();
        }

        public int getColumnCount() {
            return cols.length;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Elem elem = elems.get(rowIndex);
            return elem.getValue(cols[columnIndex]);
        }

        public void add(Elem elem) {
            elems.add(elem);
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            String msg;
            switch(cols[column]) {
                case HostCol: msg = "ShowComponentStatusCliCommand.HostCol"; break;
                case JvmCol:  msg = "ShowComponentStatusCliCommand.JvmCol"; break;
                case NameCol: msg = "ShowComponentStatusCliCommand.NameCol"; break;
                case StateCol: msg = "ShowComponentStatusCliCommand.StateCol"; break;
                case TypeCol: msg = "ShowComponentStatusCliCommand.TypeCol"; break;
                default:
                    throw new IllegalStateException("Unknown column " + cols[column]);
            }
            return I18NManager.formatMessage(msg, BUNDLE);
        }

        @Override
        public String getValueForDebugColumn(int row) {
            Elem elem = elems.get(row);
            return errorStackTraceToString(elem.error);
        }
    }

    private class Elem {

        private final ExecutionEnv env;
        private final ComponentInfo comp;
        private final String type;
        private String state;
        private Throwable error;

        public Elem(ExecutionEnv aEnv, ComponentInfo aComp) {
            env = aEnv;
            comp = aComp;
            if (comp.isAssignableTo(ResourceProvider.class)) {
                type = ResourceProvider.class.getSimpleName();
            } else if (comp.isAssignableTo(Service.class)) {
                type = Service.class.getSimpleName();
            } else if (comp.isAssignableTo(Executor.class)) {
                type = Executor.class.getSimpleName();
            } else {
                type = getBundle().getString("ShowComponentStatusCliCommand.other");
            }
        }

        public String getState() {
            if (state == null) {
                try {
                    GrmComponent c = ComponentService.<GrmComponent>getComponent(env, comp);
                    state = c.getState().toString();
                //if jvm was killed with kill -9 getState will throw UndeclaredThrowableException 
                } catch (Exception ex) {
                    if (ex instanceof UndeclaredThrowableException) {
                        error = ((UndeclaredThrowableException) ex).getUndeclaredThrowable();
                    } else {
                        error = ex;
                    }
                    state = I18NManager.formatMessage("ShowComponentStatusCliCommand.err", getBundleName(), error.getLocalizedMessage());
                }
            }
            return state;
        }

        public String getValue(Column col) {

            switch (col) {
                case HostCol:
                    return comp.getHostname().getHostname();
                case JvmCol:
                    return comp.getJvm().getName();
                case NameCol:
                    return comp.getName();
                case StateCol:
                    return getState();
                case TypeCol:
                    return type;
                default:
                    return "Unknown col " + col;
            }

        }
    }
}

