/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.reporting.Reporter;
import com.sun.grid.grm.cli.table.AbstractTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.reporting.Report;
import com.sun.grid.grm.reporting.RowIterator;
import com.sun.grid.grm.util.filter.TimeParser;
import com.sun.grid.grm.reporting.impl.ReportingVariableResolver;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterHelper;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * Get the data stored by Reporter component.
 */
@CliCommandDescriptor(
    name = "GetReporterDataCliCommand",
    hasShortcut = true,
    category = CliCategory.MONITORING,
    bundle="com.sun.grid.grm.cli.cmd.monitoring.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ShowReporterDataCliCommand extends AbstractTableCliCommand {
    @CliOptionDescriptor(name = "GetReporterDataCliCommand.sd", numberOfOperands = 1, required = false)
    private String startDate = null;
    
    @CliOptionDescriptor(name = "GetReporterDataCliCommand.ed", numberOfOperands = 1, required = false)
    private String endDate = null;

    @CliOptionDescriptor(name = "GetReporterDataCliCommand.r", numberOfOperands = 1, required = false)
    private String resIdOrName = null;
    
    @CliOptionDescriptor(name = "GetReporterDataCliCommand.s", numberOfOperands = 1, required = false)
    private String service = null;
    
    @CliOptionDescriptor(name = "GetReporterDataCliCommand.f", numberOfOperands = 1, required = false)
    private String filters = null;
    
    @CliOptionDescriptor(name = "GetReporterDataCliCommand.t", numberOfOperands = 1, required = false)
    private String eventType = null;
    
    private Filter<Report> advancedFilter = null;
    
    /** Creates a new instance of ShowReporterDataCliCommand */
    public ShowReporterDataCliCommand() {
    }
    
    /**
     * Gets the data from the reporter component and print it out on the screen.
     * @param cli
     * @throws com.sun.grid.grm.GrmException
     */
    public void execute(AbstractCli cli) throws GrmException {
        Reporter reporter = ComponentService.<Reporter>getComponentByType(cli.getExecutionEnv(), Reporter.class);
        if (reporter != null) {                       
            Filter<Report> filter = createFilter();

            final Report rd = reporter.createReport(filter);

            TableModel tm = new TableModel(rd);
            Table table = createTable(tm, cli);
            if (!isSetMachineReadableOutput()) {
                table.setColumnWidth(0, 23);
                table.setColumnWidth(1, 27);
                table.setColumnWidth(2, 14);
                table.setColumnWidth(3, 20);
                table.setColumnWidth(4, 50);
                table.setAutoCalcColumnWidth(false);
                table.print(cli);
                table.setPrintHeader(false);
            }            
            while(tm.nextRow(reporter)) {
                table.print(cli);
            }
        } else {
            throw new GrmException(I18NManager.formatMessage("GetReporterDataCliCommand.ex.norep", getBundleName()));
        }
    }

    private Filter<Report> createFilter() throws GrmException {
        Filter<Report> startFilter = null;
        Filter<Report> endFilter = null;
        if(filters != null) {
            advancedFilter = FilterHelper.<Report>parse(filters);
        }

        if (startDate != null) {
            try {
                startFilter = ReportingVariableResolver.createStartimeFilter(TimeParser.parse(startDate));
            } catch (ParseException ex) {
                throw new GrmException(I18NManager.formatMessage("GetReporterDataCliCommand.ex.startDate", getBundleName(), startDate), ex);
            }
        }

        if (endDate != null) {
            try {
                endFilter = ReportingVariableResolver.createEndtimeFilter(TimeParser.parse(endDate));
            } catch (ParseException ex) {
                throw new GrmException(I18NManager.formatMessage("GetReporterDataCliCommand.ex.endDate", getBundleName(), endDate), ex);
            }
        }
        
        AndFilter<Report> ret = new AndFilter<Report>(6);
        
        if (resIdOrName != null) {
            ret.add(ReportingVariableResolver.createResourceFilter(resIdOrName));
        }
        if (service != null) {
            ret.add(ReportingVariableResolver.createServiceFilter(service));
        }
        if (eventType != null) {
            ret.add(ReportingVariableResolver.createEventTypeFilter(eventType));
        }   
        if (startFilter != null) {
            ret.add(startFilter);
        }
        if (endFilter != null) {
            ret.add(endFilter);
        }
        if (advancedFilter != null) {
            ret.add(advancedFilter);
        }
        if (ret.size() == 0) {
            return ConstantFilter.<Report>alwaysMatching();
        }
        return ret;       
    }

    
    /**
     * Table model for reporter output table
     */
    private class TableModel extends AbstractDefaultTableModel {

        private final Report report;
        private final RowIterator iter;
        private Object [] values;
        private final int [] colIndex;
        private final int resNameIndex;
        private final int resIdIndex;
        
        public TableModel(Report report) {          
            this.report = report;
            colIndex = new int[5];
            colIndex[0] = report.getIndexForColumn(ReportingVariableResolver.TIME.getName());
            colIndex[1] = report.getIndexForColumn(ReportingVariableResolver.TYPE.getName());
            colIndex[2] = report.getIndexForColumn(ReportingVariableResolver.SERVICE.getName());
            colIndex[3] = -1;
            colIndex[4] = report.getIndexForColumn(ReportingVariableResolver.DESCRIPTION.getName());
            resIdIndex = report.getIndexForColumn(ReportingVariableResolver.RESOURCE_ID.getName());
            resNameIndex = report.getIndexForColumn(ReportingVariableResolver.RESOURCE_NAME.getName());
            iter  = report.iterator();
        }
        
        public int getRowCount() {
            return values == null ? 0 : 1;
        }
        
        public int getColumnCount() {
            return 5;
        }

        public boolean nextRow(Reporter reporter) throws GrmException {
            if (iter.hasNext(reporter)) {
                values = iter.next(reporter);
                return true; // we have to call nextRow at least once more
            } else {
                return false;
            }
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Object ret = null;
            if (values != null) {
                 if (columnIndex == 3) {
                    if (values[resNameIndex] != null) {
                        ret = String.format("%s(%s)", values[resNameIndex], values[resIdIndex]);
                    } else {
                        ret = "";
                    }
                } else {
                    ret = values[colIndex[columnIndex]];
                    if (ret instanceof Date) {
                        ret = SF.format((Date)ret);
                    } else if (ret == null) {
                        ret = "";
                    }
                }
            }
            return ret;
        }

        private final DateFormat SF = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S");

        @Override
        public String getColumnName(int column) {
            switch(column) {
                case 0: return getBundle().getString("GetReporterDataCliCommand.col.time");
                case 1: return getBundle().getString("GetReporterDataCliCommand.col.type");
                case 2: return getBundle().getString("GetReporterDataCliCommand.col.service");
                case 3: return getBundle().getString("GetReporterDataCliCommand.col.resid");
                case 4: return getBundle().getString("GetReporterDataCliCommand.col.desc");
                default:
                    throw new IllegalArgumentException(I18NManager.formatMessage("GetReporterDataCliCommand.ex.uc", getBundleName(), column));
            }
        }
    }    
}
