/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.ui.resource.ShowResourceStateCommand;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.resource.AbstractResourceType;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.resource.ShowResourceResult;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.NumberParser;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * This command is responsible for showing the current resource states in 
 * Hedeby system.
 */
@CliCommandDescriptor(name = "ShowResourceStateCliCommand", hasShortcut = true, category = CliCategory.MONITORING, bundle = "com.sun.grid.grm.cli.cmd.monitoring.messages",
   requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ShowResourceStateCliCommand extends AbstractSortedTableCliCommand {

    private final static long serialVersionUID = -2007120501L;
    @CliOptionDescriptor(name = "ShowResourceStateCliCommand.s", numberOfOperands = 1, required = false)
    private String service = null;
    @CliOptionDescriptor(name = "ShowResourceStateCliCommand.r", numberOfOperands = 1, required = false)
    private String resourceId = null;
    @CliOptionDescriptor(name = "ShowResourceStateCliCommand.c", required = false)
    private boolean cached = false;
    @CliOptionDescriptor(name = "ShowResourceStateCliCommand.a", required = false)
    private boolean all = false;
    @CliOptionDescriptor(name = "ShowResourceStateCliCommand.rf", numberOfOperands = 1, required = false)
    private String filter = null;
    @CliOptionDescriptor(name = "ShowResourceStateCliCommand.se", numberOfOperands = 0, required = false)
    private boolean showServicesWithNoResource;
    
    @CliOptionDescriptor(name = "ShowResourceStateCliCommand.o", numberOfOperands = 1, required = false)
    private String output;
    
    private final String error = I18NManager.formatMessage("ShowResourceStateCliCommand.error", getBundleName());
    private final String empty = I18NManager.formatMessage("ShowResourceStateCliCommand.empty", getBundleName());
    
    public void execute(AbstractCli cli) throws GrmException {
        String bundle = getBundleName();
        ShowResourceStateCommand srs = new ShowResourceStateCommand(service, resourceId, cached);
        srs.setIncludeServicesWithNoResource(showServicesWithNoResource);
        if(filter != null) {
            Filter<Resource> advancedFilter = FilterHelper.<Resource>parse(filter);
            srs.setAdvancedFilter(advancedFilter);
        }
        List<ShowResourceResult> results = cli.getExecutionEnv().getCommandService().execute(srs).getReturnValue();
        if (results.isEmpty()) {
            cli.out().println("ShowResourceStateCliCommand.res.notfound", bundle);
            return;
        }

       if (output == null || getBundle().getString("ShowResourceStateCliCommand.o.table").equalsIgnoreCase(output)) {
            TableModel tm = new TableModel(results, cli);                       
            Table table = createTable(tm,cli);
            if (!isSetMachineReadableOutput()) {
                if (all) {
                    table.setDuplicateValuesHidden(false);
                }
            }
            table.print(cli);
        } else if(getBundle().getString("ShowResourceStateCliCommand.o.id_list").equalsIgnoreCase(output)) {
            boolean first = true;
            for(ShowResourceResult r: results) {
                if(r.getResource() != null) {
                    if(first) {
                        first = false;
                    } else {
                        cli.out().printDirectly(',');
                    }
                    cli.out().printDirectly(r.getResource().getId().toString());
                }
            }
            cli.out().println();
        } else {
            throw new GrmException("ShowResourceStateCliCommand.o.invalid", getBundleName(), output);
        }
    }

    private class TableModel extends AbstractDefaultTableModel {
        private final static long serialVersionUID = -2008030101L;
        private List<ShowResourceResult> rows;

        public TableModel(List<ShowResourceResult> results, AbstractCli cli) {
            super(true,true,all,cli.getDebug());
            rows = results;
        }

        public ShowResourceResult getRow(int row) {
            return rows.get(row);
        }

        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 8;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            ShowResourceResult srr = rows.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return srr.getServiceName();
                case 1: {
                    if (srr.getResource() != null) {
                        return srr.getResource().getId();
                    } else {
                        return error;
                    }
                }
                case 2: {
                    if (srr.getResource() != null) {
                        return srr.getResource().getName();
                    } else {
                        return error;
                    }
                }
                case 3: {
                    if (srr.getResource() != null) {
                        return srr.getResource().getState();
                    } else {
                        return error;
                    }
                }
                case 4: {
                    if (srr.getResource() != null) {
                        return srr.getResource().getType().getName();
                    } else {
                        return error;
                    }
                }
                case 5: {
                    if (srr.getResource() != null) {
                        return getFlags(srr.getResource());
                    } else {
                        return error;
                    }
                }
                case 6: {
                    if (srr.getResource() != null) {
                        return srr.getResource().getUsage().toString();
                    } else {
                        return error;
                    }
                }
                case 7: {
                    if (srr.getResource() != null) {
                        if (srr.getResource().getAnnotation() != null) {
                            return srr.getResource().getAnnotation();
                        } else {
                            return empty;
                        }
                    } else {
                        return error;
                    }
                }
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("ShowResourceStateCliCommand.col.service");
                case 1:
                    return getBundle().getString("ShowResourceStateCliCommand.col.id");
                case 2:
                    return getBundle().getString("ShowResourceStateCliCommand.col.name");
                case 3:
                    return getBundle().getString("ShowResourceStateCliCommand.col.state");
                case 4:
                    return getBundle().getString("ShowResourceStateCliCommand.col.type");
                case 5:
                    return getBundle().getString("ShowResourceStateCliCommand.col.flags");
                case 6:
                    return getBundle().getString("ShowResourceStateCliCommand.col.usage");
                case 7:
                    return getBundle().getString("ShowResourceStateCliCommand.col.anno");
                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }

        private String getFlags(Resource resource) {
            Map<String,Object> rp = resource.getProperties();
            StringBuilder sb = new StringBuilder(3);
            try {
                Object staticFlag = AbstractResourceType.STATIC.getValue(rp);
                if (Boolean.TRUE.equals(staticFlag)) {
                    sb.append('S');
                }
            } catch(InvalidResourcePropertiesException ex) {
                throw new IllegalStateException("Resource properties do not contain the mandatory property 'static'");
            }
            Object ambiguousFlag = resource.isAmbiguous();
            if (Boolean.TRUE.equals(ambiguousFlag)) {
                sb.append('A');
            }
            if (!resource.isBound()) {
                sb.append('U');
            }
            return sb.toString();
        }

        @Override
        public String getValueForAllColumn(int row) {
            ShowResourceResult srr = getRow(row);
            StringBuilder sb = new StringBuilder();
            if (srr.getResource() != null) {
                Map<String,Object> rp = srr.getResource().getProperties();
                List<String> keys = new ArrayList<String>(rp.keySet());
                Collections.sort(keys);
                for (String key : keys) {
                    Object value = rp.get(key);
                    String str = null;
                    if (value instanceof Integer) {
                        str = NumberParser.format((Integer) value);
                    } else if (value instanceof Short) {
                        str = NumberParser.format((Short) value);
                    } else if (value instanceof Long) {
                        str = NumberParser.format((Long) value);
                    } else if (value instanceof Double) {
                        str = NumberParser.format((Double) value);
                    } else if (value instanceof Float) {
                        str = NumberParser.format((Float) value);
                    } else if (value == null) {
                        str = null;
                    } else {
                        str = value.toString();
                    }
                    sb.append(I18NManager.printfMessage("ShowResourceStateCliCommand.resprop", getBundleName(), key, str));
                }
            }
            return sb.toString();
        }
        
        @Override
        public String getValueForDebugColumn(int row) {
            ShowResourceResult srr = getRow(row);
            return errorStackTraceToString(srr.getError());
        }
    }
}
