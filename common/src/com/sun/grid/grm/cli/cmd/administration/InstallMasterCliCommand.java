/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.administration;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.install.AddSystemCommand;
import com.sun.grid.grm.ui.install.CheckSMFSupportCommand;
import com.sun.grid.grm.ui.install.CheckSTSupportCommand;
import com.sun.grid.grm.ui.install.InstallMasterCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * CLI command class for master host installation
 */
@CliCommandDescriptor(
    name = "InstallMasterCliCommand",
    hasShortcut = true,
    category = CliCategory.INSTALL,
    bundle="com.sun.grid.grm.cli.cmd.administration.messages",
    requiredPrivileges={UserPrivilege.WRITE_LOCAL_SPOOL_DIR, UserPrivilege.WRITE_BOOTSTRAP_CONFIG},
    optionalPrivileges={UserPrivilege.SUPER_USER})
public class InstallMasterCliCommand extends AbstractCliCommand {
    
    private static final Logger log = Logger.getLogger(InstallMasterCliCommand.class.getName());
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.l",
            numberOfOperands = 1
            )
    private String localSpoolPath;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.d",
            numberOfOperands = 1
            )
    private String distDirPath=null;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.cp",
            numberOfOperands = 1,
            required = true
            )
    private int csPort;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.a",
            numberOfOperands = 1,
            required = true
            )
    private String adminUser;

    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.ca_country",
            numberOfOperands = 1,
            required = true
            )
    private String caCountry;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.ca_state",
            numberOfOperands = 1,
            required = true
            )
    private String caState;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.ca_location",
            numberOfOperands = 1,
            required = true
            )
    private String caLocation;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.ca_org",
            numberOfOperands = 1,
            required = true
            )
    private String caOrg;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.ca_org_unit",
            numberOfOperands = 1,
            required = true
            )
    private String caOrgUnit;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.ca_admin_mail",
            numberOfOperands = 1,
            required = true
            )
    private String caAdminMail;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.sge_root",
            numberOfOperands = 1,
            required = true
            )
    private String sgeRoot;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.sslDisabled",
            required = false
            )
    private boolean sslDisabled;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.autostart",
            required = false
            )
    private boolean autostart;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.acceptLicense",
            required = false
            )
    private boolean accepted = false;
    
    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.noST",
            required = false
            )
    private boolean noST = false;

    @CliOptionDescriptor(
    name = "InstallMasterCliCommand.simpleInstall",
            required = false,
            hasShortcut = true
            )
    private boolean simpleInstall = false;
    
    /**
     * Execute the master host installation
     * @param cli command line environment
     * @throws com.sun.grid.grm.GrmException on errors
     */
    public void execute(AbstractCli cli) throws GrmException {
        boolean spoolExists = true;

        //Issue 699 - check for exclusive options, autostart and user preferences
        if (autostart && (cli.getPreferencesType().equals(PreferencesType.USER))) {
            throw new GrmException("InstallMasterCliCommand.error.autostart_userprefs", getBundleName());
        }
        // Evaluate
        cli.evaluateExecutionEnv();
        
        String hostname = Hostname.getLocalHost().getHostname();
        String csURL = hostname + ":" + csPort;
        if (distDirPath == null) {
            distDirPath = PathUtil.getDefaultDistDir().getAbsolutePath();
        }
        
        //We have location of dist dir, license stuff can be done
        if (accepted) {
            printLicense(cli, distDirPath);
        } else {
            askForLicense(cli, distDirPath);
        }
        
        if (localSpoolPath == null) {
            localSpoolPath = PathUtil.getDefaultSpoolDir(cli.getSystemName());
            log.log(Level.INFO, I18NManager.formatMessage("InstallMasterCliCommand.default", getBundleName(), localSpoolPath));
        }
        // add system to preferences
        AddSystemCommand addSystemCmd = new AddSystemCommand(cli.getSystemName(),
                csURL, localSpoolPath, distDirPath, cli.getPreferencesType(), true);
        addSystemCmd.setSslDisabled(sslDisabled);
        addSystemCmd.setAutostart(autostart);
        addSystemCmd.setSimple(simpleInstall);
        // Here we have to add a new ExecutionEnv so we set the
        // env to null 
        
        ExecutionEnvFactory.newNullInstance().getCommandService().executeLocally(addSystemCmd);
        
        // Now we have a system. We create an ExecutionEnv ...
        ExecutionEnv newEnv = cli.recreateExecutionEnv();

        // Ok we can setup the install macro command ...
        // set ca parameters
        Map<String, Object> caParams = new HashMap<String, Object>();

        caParams.put("ADMIN_USER", adminUser);
        caParams.put("PREFS_TYPE", cli.getPreferencesType());
        caParams.put("SGE_ROOT", sgeRoot);
        caParams.put("CA_ADMIN_EMAIL", caAdminMail);
        caParams.put("CA_COUNTRY", caCountry);
        caParams.put("CA_LOCATION", caLocation);
        caParams.put("CA_ORG", caOrg);
        caParams.put("CA_ORG_UNIT", caOrgUnit);
        caParams.put("CA_STATE", caState);
        
        boolean smf = false;
        boolean st = false;
        boolean auto = SystemUtil.isAutoStart(newEnv);
        try {
            if (auto) {
                CheckSMFSupportCommand checkCmd = new CheckSMFSupportCommand();
                smf = newEnv.getCommandService().<Boolean>execute(checkCmd).getReturnValue();
            }
            
            if (noST != true) {
                CheckSTSupportCommand checkST = new CheckSTSupportCommand();
                st = newEnv.getCommandService().<Boolean>execute(checkST).getReturnValue();
            } 
            
            InstallMasterCommand miCmd = new InstallMasterCommand(adminUser, caParams, csPort, auto, smf, st, cli.getExecutionEnvPreferences(), noST, simpleInstall);

        
            /* We are installing a new system, so the spool dir for this system should not exist
             * so if it exists the installation fails, we do this check because if we know that
             * spool dir does not exists before installation we can delete safely if installation fails
             * on any steps
             */
             spoolExists = newEnv.getLocalSpoolDir().isDirectory();
                 
            // Ok, finally we can start install master command ...
            newEnv.getCommandService().executeLocally(miCmd);
        } catch (Exception ex) {
            //If error occured we need to revert AddSystem, so we call remove system
            cli.err().println("install_failed.remove_system", getBundleName(), newEnv.getSystemName());
            addSystemCmd.undo(newEnv);
            
            /** 
             * If spool dir before installation did not exist it should be removed including
             * its contents
             */
            if (!spoolExists) {
                cli.err().println("install_failed.remove_spoolDir", getBundleName(), newEnv.getSystemName(), newEnv.getLocalSpoolDir());
                try {
                    Platform.getPlatform().removeDir(newEnv.getLocalSpoolDir(), true);
                } catch(Exception exp) {
                    cli.err().println("install_cleanup_failed.remove_spoolDir", getBundleName(), newEnv.getLocalSpoolDir());
                }
                cli.err().println("install_failed.removed_spoolDir", getBundleName(), newEnv.getLocalSpoolDir());
            }
            
            if (ex instanceof RuntimeException) {
                throw (RuntimeException) ex;
            } else {
                throw (GrmException) ex;
            }          
        }
    }
    /**
     * Print out the content of licence file to stream.
     * @param cli command line environment
     * @param distDirPath string containing path to the distribution
     * @throws com.sun.grid.grm.GrmException when license file not found or reading of this file fails
     */
    private void printLicense(AbstractCli cli, String distDirPath) throws GrmException{
        cli.out().println("InstallMasterCliCommand.license.already.accepted", getBundleName());
        cli.out().println();
        SystemUtil.printLicense(new File(distDirPath), cli.out());
        cli.out().println();
    }
    
    /**
     * Print out the content of licence file to stream.
     * @param cli command line environment
     * @param distDirPath string containing path to the distribution
     * @throws com.sun.grid.grm.GrmException when license file not found or reading of this file fails,
     *      or user does not accept the license terms
     */
    private void askForLicense(AbstractCli cli, String distDirPath) throws GrmException {
        cli.out().println("InstallMasterCliCommand.license.tobe.accepted", getBundleName());
        cli.out().println();
        SystemUtil.printLicense(new File(distDirPath), cli.out());
        cli.out().println();
        cli.out().print("InstallMasterCliCommand.license.question", getBundleName());
        cli.out().flush();
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader buf = new BufferedReader(in);
        try {
            String resp = buf.readLine();
            String tmp = I18NManager.formatMessage("InstallMasterCliCommand.accept.license.character", getBundleName());
            if (!resp.equalsIgnoreCase(tmp)) {
                throw new GrmException("InstallMasterCliCommand.error.license.rejected", getBundleName());
            }
        } catch (IOException ex) {
            throw new GrmException("InstallMasterCliCommand.error.read.user.answer", ex, getBundleName());
        }
        cli.out().println("InstallMasterCliCommand.license.accepted", getBundleName());
        cli.out().flush();
    }
}
