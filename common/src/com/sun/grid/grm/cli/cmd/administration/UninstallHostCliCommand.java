/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.administration;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.install.UninstallHostCommand;
import com.sun.grid.grm.util.Hostname;

/**
 * CLI Command for uninstalling system from Hedeby
 */

@CliCommandDescriptor(
    name = "UninstallHostCliCommand",
    hasShortcut = true,
    category = CliCategory.INSTALL,
    bundle="com.sun.grid.grm.cli.cmd.administration.messages",
    requiredPrivileges = { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.WRITE_BOOTSTRAP_CONFIG, UserPrivilege.WRITE_LOCAL_SPOOL_DIR},
    optionalPrivileges = { UserPrivilege.SUPER_USER })
public class UninstallHostCliCommand extends AbstractCliCommand {
    @CliOptionDescriptor(
    name = "UninstallHostCliCommand.force",
            required = false
            )
    private boolean force;
    
    @CliOptionDescriptor(
    name = "UninstallHostCliCommand.m",
            required = false
            )
    private boolean master = false;

    private static String BUNDLE="com.sun.grid.grm.cli.cmd.administration.messages";
    public void execute(AbstractCli cli) throws GrmException {
        Hostname host = Hostname.getLocalHost();
        UninstallHostCommand cmd = new UninstallHostCommand(host, cli.getExecutionEnvPreferences(), master, force);
        cli.getExecutionEnv().getCommandService().execute(cmd);
        if (master) {
            cli.out().println("UninstallHostCliCommand.success.master", BUNDLE, host.getHostname());
        } else {
            cli.out().println("UninstallHostCliCommand.success.managed", BUNDLE, host.getHostname());
        }
    }
    
}
