/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.administration;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.install.AddSystemCommand;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Add a Hedeby system to the preferences
 */
@CliCommandDescriptor(
   name = "AddSystemCliCommand",
   hasShortcut = true,
   category = CliCategory.ADMINISTRATION,
   bundle="com.sun.grid.grm.cli.cmd.administration.messages",
   requiredPrivileges = { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.WRITE_BOOTSTRAP_CONFIG})
public class AddSystemCliCommand extends AbstractCliCommand {

    private static final Logger log = Logger.getLogger(AddSystemCliCommand.class.getName(), getBundleName(AddSystemCliCommand.class));
    
    /* -d <dist_dir> */
    @CliOptionDescriptor(
        name = "AddSystemCliCommand.d",
        numberOfOperands = 1,
        required = false
    )
    private String distPath = null;

    /* -l <local_spool_dir> */
    @CliOptionDescriptor(
        name = "AddSystemCliCommand.l",
        numberOfOperands = 1,
        required = false
    )
    private String localSpoolPath = null;


    /* -cs <host>:<port> */
    @CliOptionDescriptor(
        name = "AddSystemCliCommand.cs",
        numberOfOperands = 1,
        required = true
    )
    private String csURL = null;
    
    /**
     * execute this command
     * @param cli the cli object
     * @throws com.sun.grid.grm.GrmException if the command failed
     */
    public void execute(AbstractCli cli) throws GrmException {
        
        // We take sysName and prefType from cli
        String sysName = cli.getSystemName();
        PreferencesType prefType = cli.getPreferencesType();
        
        // Evaluate
        cli.evaluateExecutionEnv();
        
        if (localSpoolPath == null) {
            localSpoolPath = PathUtil.getDefaultSpoolDir(cli.getSystemName());
            log.log(Level.INFO, "AddSystemCliCommand.using_default_spool_path", localSpoolPath);
        }
      
        // if distribution path is not specified we use our own distribution directory path
        if (distPath == null) {
            distPath = PathUtil.getDefaultDistDir().getAbsolutePath();
            log.log(Level.INFO, "AddSystemCliCommand.using_default_dist_path", distPath);
        }
        
        // Now we create AddSystem UI command
        AddSystemCommand uiCmd = new AddSystemCommand(sysName , csURL, localSpoolPath,
                distPath, prefType, true);
        //we need executionenv to get command service
        ExecutionEnv env = ExecutionEnvFactory.newNullInstance();
        env.getCommandService().execute(uiCmd);

    }
}
