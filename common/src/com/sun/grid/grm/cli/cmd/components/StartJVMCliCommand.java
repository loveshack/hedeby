/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.components;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.UpgradeResult;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.administration.UpgradeHostCommand;
import com.sun.grid.grm.ui.component.JVMResultObject;
import com.sun.grid.grm.ui.component.StartJVMCommand;
import com.sun.grid.grm.util.I18NManager;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * The command is used to start up all or just the specified JVM on a localhost.
 */
@CliCommandDescriptor(name = "StartJVMCliCommand",
hasShortcut = true,
category = CliCategory.COMPONENTS,
bundle = "com.sun.grid.grm.cli.cmd.components.messages",
requiredPrivileges = {UserPrivilege.READ_BOOTSTRAP_CONFIG},
optionalPrivileges = {UserPrivilege.SUPER_USER})
public class StartJVMCliCommand extends AbstractSortedTableCliCommand {

    @CliOptionDescriptor(name = "StartJVMCliCommand.j",
    numberOfOperands = 1)
    private String jvm = null;
    @CliOptionDescriptor(name = "StartJVMCliCommand.d",
    numberOfOperands = 1)
    private int port = BootstrapConstants.NO_DEBUG_PORT;
    @CliOptionDescriptor(name = "StartJVMCliCommand.f")
    private boolean forced = false;
    private final String empty = I18NManager.formatMessage("StartJVMCliCommand.empty", getBundleName());
    private static final String errorBundle = "com.sun.grid.grm.cli.client";

    public void execute(AbstractCli cli) throws GrmException {
        UpgradeResult result;
        /**
         * Check if upgrade is available/necessary and execute it
         * Because of issue688 we need to ask for username and password if keystore is not available
         * so upgrade can succeed on managed host
         * If keystore for user is available there will be no question for user
         */
        cli.setPromptPW(true);

        ExecutionEnv env = cli.recreateExecutionEnv();

        UpgradeHostCommand upd = new UpgradeHostCommand();

        result = env.getCommandService().<UpgradeResult>execute(upd).getReturnValue();
        if (!result.isSkipped()) {
            UpgradeTableModel upgradeTm = new UpgradeTableModel(cli, result);
            Table upgradeTable = createTable(upgradeTm, cli);

            upgradeTable.setColumnWidth(3, 30);
            upgradeTable.setAutoWordWrap(3, true);

            upgradeTable.print(cli.err().getPrintWriter());
            //check whether any error

            if (result.getError() != null) {
                throw new GrmException("client.error.cli_error", errorBundle, result.getError());
            }
        }



        StartJVMCommand uiCmd = new StartJVMCommand(cli.getExecutionEnvPreferences());
        uiCmd.setJVMName(jvm);
        uiCmd.setJVMPort(port);
        uiCmd.setForcedStartup(forced);

        List<JVMResultObject> ret = cli.getExecutionEnv().getCommandService().<List<JVMResultObject>>execute(uiCmd).getReturnValue();

        TableModel tm = new TableModel(ret, cli);
        Table table = createTable(tm, cli);
        if (!isSetMachineReadableOutput()) {
            table.setColumnWidth(3, 30);
            table.setAutoWordWrap(3, true);
        }
        table.print(cli);

        //check whether any error
        for (JVMResultObject r : ret) {
            if (r.getError() != null) {
                throw new GrmException("client.error.cli_error", errorBundle);
            }
        }
    }

    private class TableModel extends AbstractDefaultTableModel {

        private List<JVMResultObject> rows;

        public TableModel(List<JVMResultObject> result, AbstractCli cli) {
            super(false, true, false, cli.getDebug());
            rows = result;
        }

        public JVMResultObject getRow(int row) {
            return this.rows.get(row);
        }

        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 4;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            JVMResultObject elem = getRow(rowIndex);
            switch (columnIndex) {
                case 0:
                    return elem.getJvmName();
                case 1:
                    return elem.getHostName();

                case 2:
                    if (elem.getResult() == null) {
                        return empty;
                    }
                    return elem.getResult();
                case 3:
                    if (elem.getMessage() == null) {
                        return empty;
                    }
                    return elem.getMessage();
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("StartJVMCliCommand.col.jvm");
                case 1:
                    return getBundle().getString("StartJVMCliCommand.col.host");
                case 2:
                    return getBundle().getString("StartJVMCliCommand.col.result");
                case 3:
                    return getBundle().getString("StartJVMCliCommand.col.message");
                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            JVMResultObject r = getRow(row);
            return errorStackTraceToString(r.getError());
        }
    }

    private class UpgradeTableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = -2009110401L;
        private List<UpgradeRow> rows = new LinkedList<UpgradeRow>();
        private final String SUCCESS = getBundle().getString("StartJVMCliCommand.upgrade.success");
        private final String ERROR = getBundle().getString("StartJVMCliCommand.upgrade.error");

        public UpgradeTableModel(AbstractCli cli, UpgradeResult result) {
            super(false, true, false, cli.getDebug());
            int i = 1;
            for (String step : result.getSuccessfulSteps()) {
                rows.add(new UpgradeRow(I18NManager.formatMessage("StartJVMCliCommand.upgrade.step", getBundleName(), i, step), SUCCESS, null));
                i++;
            }
            if (!result.isSuccess()) {
                rows.add(new UpgradeRow(I18NManager.formatMessage("StartJVMCliCommand.upgrade.step", getBundleName(), i, result.getErrorStep()), ERROR, result.getError()));
            }
        }

        public UpgradeRow getRow(int row) {
            return this.rows.get(row);
        }

        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 3;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            UpgradeRow elem = getRow(rowIndex);
            switch (columnIndex) {
                case 0:
                    return elem.getStepDescription();
                case 1:
                    return elem.getStepResult();

                case 2:
                    if (elem.getMessage() == null) {
                        return empty;
                    }
                    return elem.getMessage();
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("StartJVMCliCommand.upgrade.col.step");
                case 1:
                    return getBundle().getString("StartJVMCliCommand.upgrade.col.result");
                case 2:
                    return getBundle().getString("StartJVMCliCommand.upgrade.col.message");
                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            UpgradeRow elem = getRow(row);
            if (elem.getError() != null) {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                elem.getError().printStackTrace(pw);
                pw.flush();
                return sw.toString();
            }
            return "";
        }
    }

    private class UpgradeRow {

        private final String stepDescription;
        private final String stepResult;
        private final String message;
        private final GrmException error;

        private UpgradeRow(String stepDescription, String stepResult, GrmException error) {
            this.stepDescription = stepDescription;
            this.stepResult = stepResult;
            this.error = error;
            if (error != null) {
                this.message = error.getLocalizedMessage();
            } else {
                this.message = null;
            }
        }

        /**
         * @return the stepDescription
         */
        public String getStepDescription() {
            return stepDescription;
        }

        /**
         * @return the stepResult
         */
        public String getStepResult() {
            return stepResult;
        }

        /**
         * @return the error
         */
        public GrmException getError() {
            return error;
        }

        /**
         * @return the message
         */
        public String getMessage() {
            return message;
        }
    }
}
