/*___INFO__MARK_BEGIN__*/ /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.administration;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.UsageException;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.administration.GetLoggingCommand;
import com.sun.grid.grm.ui.component.JVMResultObject;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * This command can set up logging level for jvms
 */
@CliCommandDescriptor(
    name = "GetLoggingCliCommand",
    hasShortcut = true,
    category = CliCategory.ADMINISTRATION,
    bundle="com.sun.grid.grm.cli.cmd.administration.messages",
    requiredPrivileges = { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class GetLoggingCliCommand extends AbstractSortedTableCliCommand {

    
    private static final String BUNDLE = getBundleName(GetLoggingCliCommand.class);
    private static final String EMPTY = I18NManager.formatMessage("GetLoggingCliCommand.empty", BUNDLE);
    
    @CliOptionDescriptor(
        name = "GetLoggingCliCommand.loggers",
        numberOfOperands = 1
    )
    private String loggers;
    
    @CliOptionDescriptor(
        name = "GetLoggingCliCommand.level",
        numberOfOperands = 1
    )
    private String level;
    
    @CliOptionDescriptor(
        name = "GetLoggingCliCommand.hostname",
        numberOfOperands = 1
    )
    private String hostname;
    
    @CliOptionDescriptor(
        name = "GetLoggingCliCommand.jvm",
        numberOfOperands = 1
    )
    private String jvm;
    
    @CliOptionDescriptor(
        name = "GetLoggingCliCommand.all",
        numberOfOperands = 0
    )
    private boolean all;
    
    /**
     * This method gets the logging data from jvms
     * @param cli cli variable
     * @throws com.sun.grid.grm.GrmException
     */
    public void execute(AbstractCli cli) throws GrmException {
        
        if ((jvm == null || hostname == null) && all == false 
                || (jvm != null && hostname != null && all == true)) {
            throw new UsageException(I18NManager.formatMessage("GetLoggingCliCommand.ex.nall", BUNDLE));
        }
        
        Hostname host = null;
        if (hostname != null) {
            /* resolve hostname in case of 'localhost' */
            host = Hostname.getInstance(hostname);
        }
        
        Level logLevel = null;
        if (level != null) {
            try {
                logLevel = Level.parse(level);
            } catch (IllegalArgumentException iae) {
                throw new GrmException("GetLoggingCliCommand.ex.wronglevel", iae, BUNDLE, level);
            }
        }

        GetLoggingCommand uiCmd = new GetLoggingCommand(host, jvm, loggers, logLevel);
        
        List<JVMResultObject> ret = cli.getExecutionEnv().getCommandService().<List<JVMResultObject>>execute(uiCmd).getReturnValue();
        
        TableModel tm = new TableModel(ret);
        Table table = createTable(tm, cli);
        table.print(cli);
        
    }
    
    private class TableModel extends AbstractDefaultTableModel {
        
        private final static long serialVersionUID = -200807190L;
        private List<JVMResultObject> rows;

        public TableModel(List<JVMResultObject> result) {
            rows = result;
        }

        public JVMResultObject getRow(int row) {
            return this.rows.get(row);
        }

        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 4;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            JVMResultObject elem = getRow(rowIndex);
            switch (columnIndex) {
                case 0:
                    return elem.getJvmName();
                case 1: 
                    return elem.getHostName();
                
                case 2: 
                    if (elem.getResult() == null) {
                        return EMPTY;
                    }
                    return elem.getResult(); 
                case 3: 
                    if (elem.getMessage() == null) {
                        return EMPTY;
                    }
                    return elem.getMessage();   
                default:
                    throw new IllegalArgumentException("Unknown column "+columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("GetLoggingCliCommand.col.jvm");
                case 1:
                    return getBundle().getString("GetLoggingCliCommand.col.host");                       
                case 2:
                    return getBundle().getString("GetLoggingCliCommand.col.logger");
                case 3:
                    return getBundle().getString("GetLoggingCliCommand.col.level");                       
                default:
                    throw new IllegalArgumentException("Unknown column "+column);
            }
        }
    }
}
