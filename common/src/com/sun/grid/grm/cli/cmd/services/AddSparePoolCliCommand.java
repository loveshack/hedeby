/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.services;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.config.common.SLOSet;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.sparepool.SparePoolServiceImpl;
import com.sun.grid.grm.ui.component.AddComponentWithConfigurationCommand;
import com.sun.grid.grm.ui.component.InstantiateComponentCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;

/**
 *  This cli command is responsible for adding Spare Pool Service 
 *  to the system
 */
@CliCommandDescriptor(
    name = "AddSparePoolCliCommand",
    hasShortcut = true,
    category = CliCategory.SERVICES,
    bundle = "com.sun.grid.grm.cli.cmd.services.messages",
    requiredPrivileges = { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR }
)
public class AddSparePoolCliCommand extends AbstractSortedTableCliCommand {

    @CliOptionDescriptor(name = "AddSparePoolCliCommand.j",
    numberOfOperands = 1,
    required = true)
    private String jvmName;
    @CliOptionDescriptor(name = "AddSparePoolCliCommand.h",
    numberOfOperands = 1,
    required = true)
    private String host;
    @CliOptionDescriptor(name = "AddSparePoolCliCommand.n",
    numberOfOperands = 1,
    required = true)
    private String name;
    @CliOptionDescriptor(name = "AddSparePoolCliCommand.u",
    numberOfOperands = 1)
    private int urgency = -1;
    @CliOptionDescriptor(name = "AddSparePoolCliCommand.start",
    numberOfOperands = 0)
    private boolean start;
    private static final String errorBundle = "com.sun.grid.grm.cli.client";
    private final String empty = I18NManager.formatMessage("AddSparePoolCliCommand.empty", getBundleName());

    public void execute(AbstractCli cli) throws GrmException {

        Throwable error = null;
        //Prepare Spare Pool Configuration for component
        SparePoolServiceConfig sparePoolConf = new SparePoolServiceConfig();
        // prepare SLO config
        PermanentRequestSLOConfig sloConf = new PermanentRequestSLOConfig();
        if (urgency != -1) {
            sloConf.setUrgency(urgency);
        } else {
            sloConf.setUrgency(1);
        }
        // create a condigtion for filtering the affected resources
        sloConf.setName("default");
        sloConf.setRequest(String.format("%s = \"%s\"", ResourceVariableResolver.TYPE_VAR, HostResourceType.HOST_TYPE));

        // assign slo to spare pool config
        SLOSet sloset = new SLOSet();
        sloset.getSlo().add(sloConf);
        sparePoolConf.setSlos(sloset);
        
        /* Convert any provided hostname string to the canonical name if possible.
           This especially covers the case where the provided hostname is 'localhost' */
        Hostname temp = Hostname.getInstance(host);
        if (temp.isResolved()) {
            host = temp.getHostname();
        } else {
            throw new GrmException("AddSparePoolCliCommand.failed.host", getBundleName(), name, host);
        }
        //Add entry to global and component configuration
        AddComponentWithConfigurationCommand aeCmd =
                AddComponentWithConfigurationCommand.newInstanceForSingleton().componentName(name).classname(SparePoolServiceImpl.class.getName()).config(sparePoolConf).hostname(host).jvmName(jvmName);
        //add service to Global config and create service config

        try {
            cli.getExecutionEnv().getCommandService().execute(aeCmd);
        } catch (Throwable ex) {
            error = ex;
        }

        if (error == null && start) {
            try {
                InstantiateComponentCommand cmd = new InstantiateComponentCommand();
                cmd.setComponentName(name);                
                cmd.setHostName(host);
                cmd.setJvmName(jvmName);
                cli.getExecutionEnv().getCommandService().execute(cmd);
            } catch (Throwable ex) {
                error = new GrmException("AddSparePoolCliCommand.failed.start", ex, getBundleName());
            }
        }


        TableModel tm = new TableModel(name, host, jvmName, error, cli);
        Table table = createTable(tm, cli);
        table.print(cli);

        //check whether any error
        if (error != null) {
            throw new GrmException("client.error.cli_error", errorBundle);
        }
    }

    private class TableModel extends AbstractDefaultTableModel {

        private String name = null;
        private String host = null;
        private String jvmName = null;
        private Throwable error = null;

        public TableModel(String name, String host, String jvmName, Throwable error, AbstractCli cli) {
            super(false,true,false,cli.getDebug());
            this.name = name;
            this.host = host;
            this.jvmName = jvmName;
            this.error = error;
        }

        public int getRowCount() {
            return 1;
        }

        public int getColumnCount() {
            return 4;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return name;
                case 1:
                    return host;
                case 2:
                    return jvmName;
                case 3:
                    if (error == null) {
                        return empty;
                    }
                    return error.getLocalizedMessage();
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("AddSparePoolCliCommand.col.name");
                case 1:
                    return getBundle().getString("AddSparePoolCliCommand.col.host");
                case 2:
                    return getBundle().getString("AddSparePoolCliCommand.col.jvm");
                case 3:
                    return getBundle().getString("AddSparePoolCliCommand.col.message");
                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            String ret = "";
            return errorStackTraceToString(error);
        }
    }
}
