/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.prefs.BackingStoreException;

/**
 * Command that shows all installed system configurations.
 */
@CliCommandDescriptor(name = "ShowConfigsCliCommand", hasShortcut = true, category = CliCategory.MONITORING, bundle = "com.sun.grid.grm.cli.cmd.monitoring.messages",
   requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG })
public class ShowConfigsCliCommand extends AbstractSortedTableCliCommand {

    /**
     * Option for printing just names
     */
    @CliOptionDescriptor(name = "ShowConfigsCliCommand.n")
    private boolean namesOnly;
    /**
     * Option for printing systems marked with flag auto_start
     */
    @CliOptionDescriptor(name = "ShowConfigsCliCommand.a")
    private boolean autoStartOnly;
    /**
     * Option for printing systems marked with flag security disabled
     */
    @CliOptionDescriptor(name = "ShowConfigsCliCommand.d")
    private boolean securityDisabledOnly;
    /**
     * Option for printing the full information for a system
     */
    @CliOptionDescriptor(name = "ShowConfigsCliCommand.f")
    private boolean fullOutput;

    public void execute(AbstractCli cli) throws GrmException {

        PreferencesType prefs = cli.getPreferencesType();
        String systemName = cli.getSystemName();
        List<Elem> elems = new LinkedList<Elem>();

        if (systemName == null) {
            switch (prefs) {
                case USER:
                     {
                        List<String> names = PreferencesUtil.getSystemNames(PreferencesType.USER);
                        for (String name : names) {
                            elems.add(new Elem(name, PreferencesType.USER));
                        }
                    }
                    break;
                case SYSTEM:
                     {
                        List<String> names = PreferencesUtil.getSystemNames(PreferencesType.SYSTEM);
                        for (String name : names) {
                            elems.add(new Elem(name, PreferencesType.SYSTEM));
                        }
                    }
                    break;
                default:
                    List<String> names = PreferencesUtil.getSystemNames(PreferencesType.USER);
                    for (String name : names) {
                        elems.add(new Elem(name, PreferencesType.USER));
                    }
                    names = PreferencesUtil.getSystemNames(PreferencesType.SYSTEM);
                    for (String name : names) {
                        elems.add(new Elem(name, PreferencesType.SYSTEM));
                    }
            }
        } else if (PreferencesType.SYSTEM_PROPERTIES.equals(prefs)) {
            // User did not specify the prefernces type 
            // Look into system and user preferences
            if (PreferencesUtil.existsSystem(systemName, PreferencesType.SYSTEM)) {
                elems.add(new Elem(systemName, PreferencesType.SYSTEM));
            } else if (PreferencesUtil.existsSystem(systemName, PreferencesType.USER)) {
                elems.add(new Elem(systemName, PreferencesType.USER));
            } else {
                throw new GrmException("ShowConfigsCliCommponent.ex.unknownSystem", getBundleName(), systemName);
            }
        } else if (PreferencesUtil.existsSystem(systemName, prefs)) {
            elems.add(new Elem(systemName, prefs));
        } else {
            throw new GrmException("ShowConfigsCliCommponent.ex.unknownSystemInPrefs", getBundleName(),
                    systemName, prefs);
        }

        if (!namesOnly || autoStartOnly || securityDisabledOnly) {
            ListIterator<Elem> iter = elems.listIterator();
            while (iter.hasNext()) {
                Elem elem = iter.next();
                try {
                    elem.load();
                } catch (GrmException ex) {
                    cli.err().println("ShowConfigsCliCommponent.ex.invalidSystem", getBundleName(),
                            elem.name, elem.type, ex.getLocalizedMessage());
                    iter.remove();
                    continue;
                } catch (BackingStoreException ex) {
                    cli.err().println("ShowConfigsCliCommponent.ex.invalidSystem", getBundleName(),
                            elem.name, elem.type, ex.getLocalizedMessage());
                    iter.remove();
                    continue;
                }
                if (autoStartOnly && !elem.isAutoStart()) {
                    iter.remove();
                } else if (securityDisabledOnly && !elem.isSSLDisabled()) {
                    iter.remove();
                }
            }
        }
        if (!elems.isEmpty()) {
            
            String defaultSystem = PreferencesUtil.getDefaultSystemFromPrefs();
            if(defaultSystem != null) {
                for (Elem elem : elems) {
                    if(elem.name.equals(defaultSystem)) {
                        elem.setDefaultSystem(true);
                    }
                }
            }
            if (namesOnly) {
                for (Elem elem : elems) {
                    cli.out().printlnDirectly(elem.name);
                }
            } else {
                TableModel tm = new TableModel(elems);
                Table table = createTable(tm, cli);
                table.print(cli);
            }
        }
    }
    
    /**
     * This class holds the information about a system
     */
    private class Elem {

        private final String name;
        private final PreferencesType type;
        private ExecutionEnv env;
        private String flags;
        private Boolean isAutoStart;
        private Boolean isSSLDisabled;
        private Boolean isSMF;
        private boolean defaultSystem;

        public Elem(String name, PreferencesType type) {
            this.name = name;
            this.type = type;
        }

        public ExecutionEnv getEnv() {
            if (env == null) {
                throw new IllegalStateException("execution env not loaded, please call load(), before calling getEnv()");
            }
            return env;
        }

        public void load() throws BackingStoreException, GrmException {
            env = ExecutionEnvFactory.newInstanceFromPrefs(name, type);
        }

        public Hostname getCSHost() {
            return getEnv().getCSHost();
        }

        public int getCSPort() {
            return getEnv().getCSPort();
        }
        

        public boolean isSSLDisabled() {
            if (isSSLDisabled == null) {
                isSSLDisabled = SystemUtil.isSSLDisabled(getEnv());
            }
            return isSSLDisabled;
        }

        public boolean isAutoStart() {
            if (isAutoStart == null) {
                isAutoStart = SystemUtil.isAutoStart(getEnv());
            }
            return isAutoStart;
        }
        public boolean isSMF() {
            if (isSMF == null) {
                isSMF = SystemUtil.isSMF(getEnv());
            }
            return isSMF;
        }
        
        public String getFlags() {
            if (flags == null) {
                StringBuilder buf = new StringBuilder();
                boolean autoStart = isAutoStart();
                if (autoStart) {
                    buf.append(getBundle().getString("ShowConfigsCliCommand.auto_start"));
                }
                boolean sslDisabled = isSSLDisabled();
                if (sslDisabled) {
                    if (buf.length() > 0) {
                        buf.append(',');
                    }
                    buf.append(getBundle().getString("ShowConfigsCliCommand.no_ssl"));
                }
                if (isSMF()) {
                    if (buf.length() > 0) {
                        buf.append(',');
                    }
                    buf.append(getBundle().getString("ShowConfigsCliCommand.smf"));
                }
                if (isDefaultSystem()) {
                    if (buf.length() > 0) {
                        buf.append(',');
                    }
                    buf.append(getBundle().getString("ShowConfigsCliCommand.default"));
                }
                flags = buf.toString();
            }
            return flags;
        }

        public boolean isDefaultSystem() {
            return defaultSystem;
        }

        public void setDefaultSystem(boolean defaultSystem) {
            this.defaultSystem = defaultSystem;
        }
    }

    private class TableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = 200711200;
        private final List<Elem> elems;

        private TableModel(List<Elem> elems) {
            super(true,false,fullOutput,false);
            this.elems = elems;
        }

        public int getRowCount() {
            return elems.size();
        }

        public int getColumnCount() {
            return 5;
        }

        public Elem getElem(int row) {
            return elems.get(row);
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Elem elem = elems.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    return elem.name;
                case 1:
                    return elem.type.toString();
                case 2:
                    return elem.getCSHost().getHostname();
                case 3:
                    return elem.getCSPort();
                case 4:
                    return elem.getFlags();
                default:
                    throw new IllegalArgumentException("Unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            switch (columnIndex) {
                case 0:
                case 1:
                case 2:
                case 4:
                    return String.class;
                case 3:
                    return Integer.class;
                default:
                    throw new IllegalArgumentException("Unknown column " + columnIndex);
            }
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return getBundle().getString("ShowConfigsCliCommand.col.name");
                case 1:
                    return getBundle().getString("ShowConfigsCliCommand.col.type");
                case 2:
                    return getBundle().getString("ShowConfigsCliCommand.col.host");
                case 3:
                    return getBundle().getString("ShowConfigsCliCommand.col.port");
                case 4:
                    return getBundle().getString("ShowConfigsCliCommand.col.flags");
                default:
                    throw new IllegalArgumentException("Unknown column " + columnIndex);
            }
        }

        @Override
        public String getValueForAllColumn(int row) {
            Elem elem = getElem(row);
            StringBuilder sb = new StringBuilder();
            sb.append(I18NManager.formatMessage("ShowConfigsCliCommand.spool", getBundleName(), elem.getEnv().getLocalSpoolDir()));
            sb.append("\n");
            sb.append(I18NManager.formatMessage("ShowConfigsCliCommand.dist", getBundleName(), elem.getEnv().getDistDir()));
            sb.append("\n");
            return sb.toString();
        }
    }
}
