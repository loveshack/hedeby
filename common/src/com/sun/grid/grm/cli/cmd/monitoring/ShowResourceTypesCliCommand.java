/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.resource.ResourcePropertyType;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.CommandService;
import com.sun.grid.grm.ui.resource.ShowResourceTypesCommand;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 */
@CliCommandDescriptor(name = "ShowResourceTypesCliCommand", hasShortcut = true, category = CliCategory.MONITORING, bundle = "com.sun.grid.grm.cli.cmd.monitoring.messages",
   requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG })
public class ShowResourceTypesCliCommand extends AbstractSortedTableCliCommand {

    public void execute(AbstractCli cli) throws GrmException {
        ShowResourceTypesCommand cmd = new ShowResourceTypesCommand();
        
        CommandService svc = cli.getExecutionEnv().getCommandService();
        Collection<ResourceType> types = svc.execute(cmd).getReturnValue();
        
        TableModel tm = new TableModel(types);
        Table table = createTable(tm, cli);
        table.print(cli);
    }
    
    
    private class TableModel extends AbstractDefaultTableModel {
    
        private final static long serialVersionUID = -2008013001L;
        
        private final List<Row> rows;
        public TableModel(Collection<ResourceType> types) {
            
            rows = new ArrayList<Row>();
            for(ResourceType t: types) {
                for(ResourcePropertyType pt: t.getPropertyTypes().values()) {
                    rows.add(new Row(t.getName(), pt));
                }
            }
        }
        
        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 4;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Row row = rows.get(rowIndex);
            switch(columnIndex) {
                case 0: return row.getTypeName();
                case 1: return row.getPropertyName();
                case 2: return row.getFlags();
                case 3: return row.getPropertyType();
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch(columnIndex) {
                case 0: return getBundle().getString("ShowResourceTypesCliCommand.col.type");
                case 1: return getBundle().getString("ShowResourceTypesCliCommand.col.propName");
                case 2: return getBundle().getString("ShowResourceTypesCliCommand.col.flags");
                case 3: return getBundle().getString("ShowResourceTypesCliCommand.col.propType");
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }
    }

    private class Row {
        private String type;
        private ResourcePropertyType property;
        
        public Row(String type, ResourcePropertyType property) {
            this.type = type;
            this.property = property;
        }
        
        public String getTypeName() {
            return type;
        }
        
        public String getPropertyName() {
            return property.getName();
        }
        
        public String getPropertyType() {
            return property.getType().getSimpleName();
        }
        
        public String getFlags() {
            if(property.isMandatory()) {
                return getBundle().getString("ShowResourceTypesCliCommand.mandatory");
            } else {
                return "";
            }
        }
    }
}
