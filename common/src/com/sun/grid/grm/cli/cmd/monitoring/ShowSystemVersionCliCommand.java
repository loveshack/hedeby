/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2009
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;

/**
 * Cli command that prints out system version of running system.
 */
@CliCommandDescriptor(
    name = "ShowSystemVersionCliCommand",
    hasShortcut = true,
    category = CliCategory.MONITORING,
    bundle="com.sun.grid.grm.cli.cmd.monitoring.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ShowSystemVersionCliCommand extends AbstractSortedTableCliCommand {

    public void execute(AbstractCli cli) throws GrmException {

        ExecutionEnv env = cli.getExecutionEnv();
        TableModel tm = new TableModel(env);
        Table table = createTable(tm, cli);
        table.print(cli);
    }

    private class TableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = -2009102001L;
        private String name;
        private String version;

        public TableModel(ExecutionEnv env) {
            name = env.getSystemName();
            try {
                version = SystemUtil.getSystemVersion(env);
            } catch (GrmException ex) {
                version = ex.getLocalizedMessage();
            }
        }


        public int getRowCount() {
            return 1;
        }

        public int getColumnCount() {
            return 2;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {

            switch (columnIndex) {
                case 0:
                    return name;

                case 1:
                    return version;

                default:
                    throw new IllegalArgumentException("unknown column " +
                            columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("ShowSystemVersionCliCommand.col.name");

                case 1:
                    return getBundle().getString("ShowSystemVersionCliCommand.col.version");

                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }
    }
}
