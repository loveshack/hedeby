/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.resources;

import com.sun.grid.grm.cli.cmd.*;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.ResourcePropertyType;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.resource.MultipleResourceActionCommand;
import com.sun.grid.grm.ui.resource.AbstractResourceActionCommand;
import com.sun.grid.grm.ui.resource.AddResourceCommand;
import com.sun.grid.grm.util.I18NManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * This command is used to add a new resource to a system (temporarily just a 
 * host resource).
 * 
 */
@CliCommandDescriptor(name = "AddResourceCliCommand", hasShortcut = true, category = CliCategory.RESOURCES, 
      bundle = "com.sun.grid.grm.cli.cmd.resources.messages", 
      requiredPrivileges = {UserPrivilege.ADMINISTRATOR, UserPrivilege.READ_BOOTSTRAP_CONFIG})
public class AddResourceCliCommand extends AbstractResourceCliCommand {

    @CliOptionDescriptor(name = "AddResourceCliCommand.i", numberOfOperands = 1, required = false)
    private String resourceName = null;
    @CliOptionDescriptor(name = "AddResourceCliCommand.t", numberOfOperands = 1, required = false)
    private String type = HostResourceType.HOST_TYPE;
    @CliOptionDescriptor(name = "AddResourceCliCommand.pf", numberOfOperands = 1, required = false)
    private String rpFile = null;
    @CliOptionDescriptor(name = "AddResourceCliCommand.s", numberOfOperands = 1, required = false)
    private String service = null;

    @Override
    protected AbstractResourceActionCommand createCommand(AbstractCli cli) throws GrmException {
        
        List<AddResourceCommand> cmdList = null;
        ResourceType rt = DefaultResourceFactory.getResourceType(type);
        
        if (rpFile != null) {
            if (resourceName != null) {
                throw new GrmException("AddResourceCliCommand.mutalExclusive", getBundleName());
            }
            List<Map<String, Object>> propsList = readResourceProperties(rpFile, rt);
            cmdList = new ArrayList<AddResourceCommand>(propsList.size());
            for(Map<String, Object> props: propsList) {
                AddResourceCommand cmd = new AddResourceCommand(service, rt, props);
                cmdList.add(cmd);
            }
        } else {
            // Interactive use case

            boolean haveAllProperties = true;

            Map<String, Object> propsDerivedFromName = null;

            if (resourceName != null) {
                propsDerivedFromName = rt.createResourcePropertiesForName(resourceName);
            } else {
                propsDerivedFromName = Collections.<String, Object>emptyMap();
            }

            for (ResourcePropertyType rpt : rt.getPropertyTypes().values()) {
                Object prop = propsDerivedFromName.get(rpt.getName());
                if (prop != null) {
                    continue;
                }
                if (rpt.isOptional()) {
                    continue;
                }
                if (rpt.getDefaultValue() != null) {
                    continue;
                }
                haveAllProperties = false;
                break;
            }
            
            if (haveAllProperties) {
               AddResourceCommand cmd = new AddResourceCommand(service, rt, propsDerivedFromName);
               cmdList = Collections.<AddResourceCommand>singletonList(cmd);
            } else {
               // Open the editor
               Map<String, Object> props = editProperties(rt, propsDerivedFromName, I18NManager.formatMessage("AddResourceCliCommand.comments", getBundleName(), rt.getName()));
               if (props != null) {
                   AddResourceCommand cmd = new AddResourceCommand(service, rt, props);
                   cmdList = Collections.<AddResourceCommand>singletonList(cmd);
               }
            }
        }
        
        if(cmdList == null || cmdList.isEmpty()) {
            cli.out().println("AddResourceCliCommmand.propsNotModified", getBundleName());
            return null;
        } else {
            return new MultipleResourceActionCommand<AddResourceCommand>(cmdList);
        }
    }
}
