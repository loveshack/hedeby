/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.bootstrap.LogEvent;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.component.service.GetComponentInfosCommand;
import com.sun.grid.grm.util.GrmFormatter;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.Filter;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This cli command prints out logs of SDM jvms.
 *
 * <p>For this purpose it binds a port and receives via this port UDP datagrams. This
 * data is interperted a serialized instances of <tt>LogEvent</tt>. After
 * successfully deserialization the log event is printed to stdout.</p>
 *
 * <p>The <tt>ShowLogsCliCommand</tt> registers a remote logger in the JVM
 *    by calling <tt>JVM#addRemoteLogger</tt>. The JVM will send log events
 *    a UDP datagrams to the port of the <tt>ShowLogsCliCommand</tt>.</p>
 *
 * <h2>Autodiscovery of JVMs</h2>
 *
 * The <tt>ShowLogsCliCommand</tt> command autodiscovery SDM jvm by polling the
 * configuration service. If a new JVM came up the and this jvm matches the the cli option
 * -s and -h the log events of the new JVM will be also reported.
 * The polling interval can be specified with the -poll option.
 * 
 */
@CliCommandDescriptor(name = "ShowLogsCliCommand",
hasShortcut = true,
category = CliCategory.MONITORING,
bundle = "com.sun.grid.grm.cli.cmd.monitoring.messages",
requiredPrivileges = {UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR})
public class ShowLogsCliCommand extends AbstractCliCommand {

    private final static Logger log = Logger.getLogger(ShowLogsCliCommand.class.getName(), "com.sun.grid.grm.cli.cmd.monitoring.messages");
    @CliOptionDescriptor(name = "ShowLogsCliCommand.j", numberOfOperands = 1, required = false)
    private String name = null;
    @CliOptionDescriptor(name = "ShowLogsCliCommand.h", numberOfOperands = 1, required = false)
    private String host = null;
    @CliOptionDescriptor(name = "ShowLogsCliCommand.l", numberOfOperands = 1, required = false)
    private String levelStr;
    private Level level = Level.ALL;
    @CliOptionDescriptor(name = "ShowLogsCliCommand.st", numberOfOperands = 0, required = false)
    private boolean withStacktrace;
    @CliOptionDescriptor(name = "ShowLogsCliCommand.col", numberOfOperands = 1, required = false, list = true)
    private List<String> columns;
    @CliOptionDescriptor(name = "ShowLogsCliCommand.port", numberOfOperands = 1, required = false)
    private int port;
    @CliOptionDescriptor(name = "ShowLogsCliCommand.poll", numberOfOperands = 1, required = false)
    private int pollingInterval = 60;
    
    private ScheduledExecutorService exe;
    private final GrmFormatter formatter = new GrmFormatter();
    private volatile String formatStr = "%s|%s|%s";
    private final Map<ComponentInfo, LeaseRenewer> discoveredJvms = new ConcurrentHashMap<ComponentInfo, LeaseRenewer>();

    /**
     * Execute this command
     * @param cli
     * @throws com.sun.grid.grm.GrmException
     */
    public void execute(AbstractCli cli) throws GrmException {

        if (levelStr != null) {
            try {
                level = Level.parse(levelStr);
            } catch (Exception ex) {
                throw new GrmException("ShowLogsCliCommand.l.invalid", ex, getBundleName(), levelStr);
            }
        }

        if (port < 0) {
            throw new GrmException("ShowLogsCliCommand.ex.negativePort", getBundleName());
        }
        
        if (pollingInterval < 0) {
            throw new GrmException("ShowLogsCliCommand.ex.negativePoll", getBundleName());
        }

        formatter.setWithStackTrace(withStacktrace);

        GrmFormatter.Column cols[];
        if (columns == null) {
            cols = new GrmFormatter.Column[]{
                        GrmFormatter.Column.TIME,
                        GrmFormatter.Column.SOURCE,
                        GrmFormatter.Column.THREAD,
                        GrmFormatter.Column.LEVEL,
                        GrmFormatter.Column.MESSAGE
                    };
        } else {
            cols = new GrmFormatter.Column[columns.size()];
            for (int i = 0; i < cols.length; i++) {
                try {
                    cols[i] = GrmFormatter.Column.valueOf(columns.get(i));
                } catch (Exception ex) {
                    throw new GrmException("ShowLogsCliCommand.col.invalid", ex, getBundleName(), columns.get(i));
                }
            }
        }
        formatter.setColumns(cols);

        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>(3);
        filter.add(GetComponentInfosCommand.newTypeFilter(JVM.class));
        if (name != null) {
            filter.add(GetComponentInfosCommand.newRegExpNameFilter(name));
        }
        if (host != null) {
            /* resolve hostname in case of 'localhost' */
            filter.add(GetComponentInfosCommand.newHostFilter(Hostname.getInstance(host)));
        }


        DatagramSocket socket;
        try {
            socket = new DatagramSocket(port);
        } catch (SocketException ex) {
            throw new GrmException("ShowLogsCliCommand.ex.socket", ex, getBundleName(), ex.getLocalizedMessage());
        }

        cli.out().println("ShowLogsCliCommand.listen", getBundleName(), Integer.toString(socket.getLocalPort()));

        exe = Executors.newScheduledThreadPool(2);

        DiscoverTask dt = new DiscoverTask(cli.getExecutionEnv(), filter, socket.getLocalPort());
        exe.scheduleWithFixedDelay(dt, 0, this.pollingInterval, TimeUnit.SECONDS);
        
        try {
            byte[] buf = new byte[1000 * 1024];
            while (!Thread.currentThread().isInterrupted()) {
                DatagramPacket dp = new DatagramPacket(buf, buf.length);
                socket.receive(dp);

                ByteArrayInputStream bin = new ByteArrayInputStream(dp.getData(), dp.getOffset(), dp.getLength());
                ObjectInputStream oin = new ObjectInputStream(bin);
                LogEvent evt = (LogEvent) oin.readObject();
                cli.out().printfDirectly(formatStr, evt.getHostname(), evt.getJvmName(), formatter.format(evt.getLogRecord()));
            }
        } catch (Exception ex) {
            throw new GrmException(ex.getLocalizedMessage(), ex);
        }
    }

    private class DiscoverTask implements Runnable {

        private final ExecutionEnv env;
        private final Filter<ComponentInfo> filter;
        private final int port;
        private int maxJvmLen;
        private int maxHostLen;

        public DiscoverTask(ExecutionEnv env, Filter<ComponentInfo> filter, int port) {
            this.env = env;
            this.filter = filter;
            this.port = port;
        }

        public void run() {
            log.log(Level.FINE, "ShowLogsCliCommand.discover", filter);
            try {
                for(ComponentInfo ci: new ArrayList<ComponentInfo>(discoveredJvms.keySet())) {
                    try {
                        JVM jvm = ComponentService.<JVM>getComponent(env, ci);
                        jvm.getState();
                        log.log(Level.FINER, "ShowLogsCliCommand.jvmReg", ci.getIdentifier());
                    } catch(GrmException ex) {
                        log.log(Level.WARNING, "ShowLogsCliCommand.jvmDied", ci.getIdentifier());
                        discoveredJvms.remove(ci);
                    }
                }

                for (ComponentInfo ci : ComponentService.getComponentInfos(env, filter)) {
                    if (!discoveredJvms.containsKey(ci)) {
                        LeaseRenewer lrn = new LeaseRenewer(env, ci, port);
                        if (lrn.call()) {
                            discoveredJvms.put(ci, lrn);
                            maxJvmLen = Math.max(ci.getJvm().getName().length(), maxJvmLen);
                            maxHostLen = Math.max(ci.getHostname().getHostname().length(), maxHostLen);
                        }
                    }
                }
                formatStr = "%" + maxHostLen + "s|%" + maxJvmLen + "s|%s";
            } catch(Exception ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("ShowLogsCliCommand.discoveryFailed", getBundleName(), ex.getLocalizedMessage()), ex);
            }
        }
    }

    private class LeaseRenewer implements Callable<Boolean> {

        private final ComponentInfo ci;
        private final ExecutionEnv env;
        private final int port;

        public LeaseRenewer(ExecutionEnv env, ComponentInfo ci, int port) {
            this.ci = ci;
            this.port = port;
            this.env = env;
        }

        public Boolean call() {
            try {
                log.log(Level.FINE, "ShowLogsCliCommand.renew", ci);
                JVM jvm = ComponentService.<JVM>getComponent(env, ci);
                long ttl = jvm.addRemoteLogger(Hostname.getLocalHost().getHostname(), port, level, null);
                ttl = Math.max(10000, ttl - 2000);
                exe.schedule(this, ttl, TimeUnit.MILLISECONDS);
                return true;
            } catch (GrmException ex) {
                log.log(Level.WARNING, I18NManager.formatMessage("ShowLogsCliCommand.renewLeaseFailed", getBundleName(), ex.getLocalizedMessage()), ex);
                discoveredJvms.remove(ci);
                return false;
            }
        }
    }

}
    
