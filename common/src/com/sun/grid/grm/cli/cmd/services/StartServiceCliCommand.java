/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.services;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.component.ServiceResultObject;
import com.sun.grid.grm.ui.component.StartServiceCommand;
import com.sun.grid.grm.util.I18NManager;

/**
 * Cli command for starting service itself with given name,
 * To start service the component representing service has to be in proper state
 * (STARTED), start of the service is (should be) implemented in startService()
 * method of Service interface
 */
@CliCommandDescriptor(
    name = "StartServiceCliCommand",
    hasShortcut = true,
    category = CliCategory.SERVICES,
    bundle="com.sun.grid.grm.cli.cmd.services.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class StartServiceCliCommand extends AbstractSortedTableCliCommand {
    @CliOptionDescriptor(
    name = "StartServiceCliCommand.n",
            numberOfOperands = 1,
            required = true
            )
    private String name = null;
    
    private final String empty = I18NManager.formatMessage("StartServiceCliCommand.empty", getBundleName());
    private static final String errorBundle = "com.sun.grid.grm.cli.client";
    
    public void execute(AbstractCli cli) throws GrmException {
        
        StartServiceCommand startCmd = new StartServiceCommand(name);
        ServiceResultObject res = cli.getExecutionEnv().getCommandService().execute(startCmd).getReturnValue();
        
        TableModel tm = new TableModel(res, cli);
        Table table = createTable(tm, cli);
        table.print(cli);
        
        //check whether any error
        if (res.getError()!=null) {
            throw new GrmException("client.error.cli_error", errorBundle);
        }
        
    }
    private class TableModel extends AbstractDefaultTableModel {

        private ServiceResultObject row;

        public TableModel(ServiceResultObject result, AbstractCli cli) {
            super(false,true,false,cli.getDebug());
            row = result;
        }

        public ServiceResultObject getRow(int row) {
            return this.row;
        }

        public int getRowCount() {
            return 1;
        }

        public int getColumnCount() {
            return 3;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            ServiceResultObject elem = getRow(rowIndex);
            switch (columnIndex) {
                case 0:
                    return elem.getServiceName();
                case 1: 
                    if (elem.getResult() == null) {
                        return empty;
                    }
                    return elem.getResult();
                
                case 2: 
                    if (elem.getMessage() == null) {
                        return empty;
                    }
                    return elem.getMessage();                             
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("StartServiceCliCommand.col.service");
                case 1:
                    return getBundle().getString("StartServiceCliCommand.col.result");
                case 2:
                    return getBundle().getString("StartServiceCliCommand.col.message");                
                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            ServiceResultObject r = getRow(row);
            return errorStackTraceToString(r.getError());
        }

        
    }
}
