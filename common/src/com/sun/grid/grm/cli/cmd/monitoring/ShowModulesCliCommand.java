/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.Module;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import java.util.ArrayList;
import java.util.List;

/**
 *  Cli command which prints out the available modules.
 *
 */
@CliCommandDescriptor(
   name = "ShowModulesCliCommand",
   hasShortcut = true,
   category = CliCategory.MONITORING,
   bundle="com.sun.grid.grm.cli.cmd.monitoring.messages",
   requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG })

public class ShowModulesCliCommand extends AbstractSortedTableCliCommand {

    /**
     *  Execution this command
     * @param cli the cli object
     * @throws com.sun.grid.grm.GrmException on any error
     */
    public void execute(AbstractCli cli) throws GrmException {
        TableModel tm = new TableModel();
        Table table = createTable(tm, cli);
        table.print(cli);
    }
    
    private class TableModel extends AbstractDefaultTableModel {
        private final List<Module> modules;
        
        public TableModel() {
            modules = new ArrayList<Module>(Modules.getModules());
        }
        
        public int getRowCount() {
            return modules.size();
        }

        public int getColumnCount() {
            return 3;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Module module = modules.get(rowIndex);
            switch(columnIndex) {
                case 0: return module.getName();
                case 1: return module.getVersion();
                case 2: return module.getVendor();
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch(column) {
                case 0: return getBundle().getString("ShowModulesCliCommand.col.name");
                case 1: return getBundle().getString("ShowModulesCliCommand.col.version");
                case 2: return getBundle().getString("ShowModulesCliCommand.col.vendor");
                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }
    }
}
