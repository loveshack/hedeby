/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.administration;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.ui.component.ModifyComponentConfigurationCommand;
import com.sun.grid.grm.validate.ValidatorService;
import java.io.File;

/**
 *  This cli command get the component configuration from CS, opens
 *  it in an editor, validate the modified configuration and
 *  sends it back to CS
 */
@CliCommandDescriptor(name = "ModifyComponentConfigCliCommand",
hasShortcut = true,
category = CliCategory.ADMINISTRATION,
bundle = "com.sun.grid.grm.cli.cmd.administration.messages",
requiredPrivileges = {UserPrivilege.ADMINISTRATOR, UserPrivilege.READ_BOOTSTRAP_CONFIG})
public class ModifyComponentConfigCliCommand extends AbstractCliCommand {
    //Name of the component that is going to be modified
    @CliOptionDescriptor(name = "ModifyComponentConfigCliCommand.n",
    numberOfOperands = 1,
    required = true)
    private String name = null;    //Optional parameter for import configuration file
    @CliOptionDescriptor(name = "ModifyComponentConfigCliCommand.f",
    numberOfOperands = 1)
    private String fileName = null;

    public void execute(AbstractCli cli) throws GrmException {
        ExecutionEnv env = cli.getExecutionEnv();
        ComponentConfig newConf = null;
        // try to import configuration file
        if (fileName != null) {
            File tmpFile = new File(fileName);
            if (!tmpFile.exists()) {
                throw new GrmException("ModifyComponentConfigCliCommand.missing_importfile", getBundleName(), fileName);
            }
            try {
                newConf = (ComponentConfig) XMLUtil.loadAndValidate(tmpFile);
                ValidatorService.validateChain(env, newConf);
            }catch (ClassCastException e) {
                throw new GrmException("ModifyComponentConfigCliCommand.wrong_xml_config", getBundleName());
            }
        } else {
            //Retrieve component configuration that is going to be modified
            GetConfigurationCommand<ComponentConfig> cmd = new GetConfigurationCommand<ComponentConfig>();
            cmd.setConfigName(name);
            ComponentConfig conf = env.getCommandService().execute(cmd).getReturnValue();
            newConf = XMLUtil.<ComponentConfig>edit(env, conf, "component_config");
        }
        if (newConf != null) {
            ModifyComponentConfigurationCommand mod = new ModifyComponentConfigurationCommand(name, newConf);
            //Modify the configuration of component
            env.getCommandService().execute(mod);
            cli.out().println("ModifyComponentConfigCliCommand.updated", getBundleName());
        } else {
            cli.out().println("ModifyComponentConfigCliCommand.unchanged", getBundleName());
        }
    }
}
