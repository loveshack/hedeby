/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.resources;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.resource.ResourcePropertyType;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.impl.ResourceIdImpl;
import com.sun.grid.grm.ui.resource.MultipleResourceActionCommand;
import com.sun.grid.grm.ui.resource.AbstractResourceActionCommand;
import com.sun.grid.grm.ui.resource.ResourceActionResult;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.NumberParser;
import com.sun.grid.grm.util.Platform;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

/**
 * Abstract base class which invokes a method on a list of matching resources.
 */
@CliCommandDescriptor(name = "AbstractResourceCliCommand", category = CliCategory.RESOURCES, bundle = "com.sun.grid.grm.cli.cmd.resources.messages")
public abstract class AbstractResourceCliCommand extends AbstractTableCliCommand {

    private static final String errorBundle = "com.sun.grid.grm.cli.client";

    /**
     * Create the resource action command 
     * @param cli  the cli object
     * @return  the resource action command or <code>null</code> if no action should be executed
     * @throws com.sun.grid.grm.GrmException can be used for error reporting
     */
    protected abstract AbstractResourceActionCommand createCommand(AbstractCli cli) throws GrmException;

    private Table createTable(TableModel tm, AbstractCli cli) throws GrmException {
        Table table = super.createTable(tm, cli);
        if (!isSetMachineReadableOutput()) {
            // set word-wrap for message column
            table.setAutoWordWrap(1, true);
            // set width for message column 
            table.setColumnWidth(1, 50);
        }
        return table;
    }

    public final void execute(final AbstractCli cli) throws GrmException {
        AbstractResourceActionCommand uiCmd = createCommand(cli);
        if (uiCmd != null) {
            final TableModel tm;
            if (uiCmd instanceof MultipleResourceActionCommand) {
                MultipleResourceActionCommand muiCmd = (MultipleResourceActionCommand) uiCmd;

                tm = new TableModel(cli, muiCmd.getActionCount());

                muiCmd.addListener(tm);

                final Table table = createTable(tm, cli);

                final Thread t = new Thread() {

                    @Override
                    public void run() {
                        table.print(cli);
                    }
                };
                t.start();

                cli.getExecutionEnv().getCommandService().execute(uiCmd).getReturnValue();
                try {
                    t.join();
                } catch (InterruptedException ex) {
                // Ignore
                }

            } else {
                List<ResourceActionResult> affectedResources = cli.getExecutionEnv().getCommandService().execute(uiCmd).getReturnValue();
                tm = new TableModel(cli, affectedResources);
                final Table table = createTable(tm, cli);
                table.print(cli);
            }
            if (tm.errorOccured()) {
                throw new GrmException("client.error.cli_error", errorBundle);
            }
        }
    }

    /**
     * Read resource properties from a file. The rpFile parameter can be a
     * filename or '-' to indicate reading from stdin.
     * 
     * @param rpFile  the filename
     * @param rt the resource type
     * @return  the resource properties
     * @throws com.sun.grid.grm.GrmException if <code>rpFile</code> could not be read
     */
    protected List<Map<String, Object>> readResourceProperties(String rpFile, ResourceType rt) throws GrmException {
        return readResourceProperties(makeInputStream(rpFile), rt);
    }

    /**
     * Helper method to create an input stream from a filename string. If given
     * the string '-', this method returns System.in (stdin).
     * 
     * @param rpFile - the filename
     * @return InputStream constructed from given filename
     * @throws com.sun.grid.grm.GrmException
     */
    private InputStream makeInputStream(String rpFile) throws GrmException {
        if (rpFile.equals("-")) {
            return System.in;
        } else {
            return makeInputStream(new File(rpFile));
        }
    }

    /**
     * Helper method to create an input stream from a File.
     * 
     * @param rpFile the file
     * @return the file InputStream
     * @throws com.sun.grid.grm.GrmException
     */
    private InputStream makeInputStream(File rpFile) throws GrmException {
        try {
            return new FileInputStream(rpFile);
        } catch (FileNotFoundException ex) {
            throw new GrmException("AbstractResourceCliCommand.ex.read", ex, getBundleName(), ex.getLocalizedMessage());
        }
    }

    /**
     * Read resource properties from an input stream.
     * 
     * @param input  the input stream
     * @param rt the resource type
     * @return  the resource properties
     * @throws com.sun.grid.grm.GrmException if <code>input</code> could not be
     * read, resource id could not be created, or the stream contains no or
     * duplicate entries.
     */
    private List<Map<String, Object>> readResourceProperties(InputStream input, ResourceType rt) throws GrmException {
        List<Map<String, Object>> ret = new LinkedList<Map<String, Object>>();

        try {
            ResourcePropertiesInputStream in = new ResourcePropertiesInputStream(input);
            Properties p = new Properties();
            while (!in.eofReached()) {
                InputStream pin = in.readNextResource();
                if (pin == null) {
                    break;
                }
                p.clear();
                p.load(pin);
                if (p.isEmpty()) {
                    continue;
                }
                HashMap<String, Object> rp = new HashMap<String, Object>(p.size());
                Set<Entry<Object, Object>> entries = p.entrySet();
                for (Entry<Object, Object> entry : entries) {
                    rp.put(entry.getKey().toString(), entry.getValue());
                }
                ret.add(rp);
            }
        } catch (IOException ex) {
            throw new GrmException("AbstractResourceCliCommand.ex.read", getBundleName(), ex.getLocalizedMessage());
        }
        if (ret.size() == 0) {
            throw new GrmException("AbstractResourceCliCommand.ex.noResFound", getBundleName());
        }
        return ret;
    }

    /**
     * Read resource properties for a single resource from file. The rpFile
     * parameter can be a filename or '-' to indicate reading from stdin.
     *  
     * @param rpFile  the filename
     * @param rt the resource type
     * @return  the resource properties
     * @throws com.sun.grid.grm.GrmException if <code>rpFile</code> could not be read
     */
    protected Map<String, Object> readSingleResourceProperties(String rpFile, ResourceType rt) throws GrmException {
        return readSingleResourceProperties(makeInputStream(rpFile), rt);
    }

    /**
     * Read resource properties for a single resource from input stream.
     * 
     * @param input - the InputStream
     * @param rt - the resource type
     * @return
     * @throws com.sun.grid.grm.GrmException
     */
    protected Map<String, Object> readSingleResourceProperties(InputStream input, ResourceType rt) throws GrmException {
        List<Map<String, Object>> props = readResourceProperties(input, rt);
        if (props.size() != 1) {
            throw new GrmException("AbstractResourceCliCommand.ex.fileMultipleRes", getBundleName());
        }
        return props.get(0);
    }

    private class ResourcePropertiesInputStream {

        private BufferedReader in;
        private boolean eofReached;
        private int lineNumber;

        public ResourcePropertiesInputStream(InputStream in) {
            this.in = new BufferedReader(new InputStreamReader(in));
        }

        public InputStream readNextResource() throws IOException {
            ByteArrayOutputStream bout = null;
            PrintWriter pw = null;
            String line = null;
            while (true) {
                line = in.readLine();
                lineNumber++;
                if (line == null) {
                    eofReached = true;
                    break;
                }
                line = line.trim();
                if (pw == null) {
                    bout = new ByteArrayOutputStream();
                    pw = new PrintWriter(bout);
                }
                if (line.startsWith("===")) {
                    break;
                }
                pw.println(line);
            }
            if (pw == null) {
                return null;
            } else {
                pw.flush();
                return new ByteArrayInputStream(bout.toByteArray());
            }
        }

        public boolean eofReached() {
            return eofReached;
        }

        public int getLineNumber() {
            return lineNumber;
        }
    }

    /**
     * Open resource properties in an editor
     * @param rt    the type of the resource
     * @param properties  the properties
     * @param comment leading comment
     * @return the modified resource properties or <code>null</code> if the user did not 
     *         modify the resource properties
     * @throws com.sun.grid.grm.GrmException if the editor could not be started
     */
    protected Map<String, Object> editProperties(ResourceType rt, Map<String, Object> properties, String comment) throws GrmException {

        // Start the editor
        File tmpFile = null;
        try {
            try {
                tmpFile = File.createTempFile("addresource", "tmp");
                tmpFile.deleteOnExit();
                PrintWriter pw = new PrintWriter(new FileOutputStream(tmpFile));
                try {
                    pw.println("# ");
                    pw.print("# ");
                    pw.println(comment);
                    pw.println("# ");

                    List<ResourcePropertyType> propertyTypes = new ArrayList<ResourcePropertyType>(rt.getPropertyTypes().values());
                    Collections.sort(propertyTypes, new ResourcePropertyTypeComparator(properties));

                    Map<String, Object> unknownProps = new HashMap<String, Object>(properties);

                    for (ResourcePropertyType rtp : propertyTypes) {
                        unknownProps.remove(rtp.getName());
                    }

                    boolean unkownPropsWritten = false;
                    for (ResourcePropertyType rtp : propertyTypes) {
                        Object value = properties.get(rtp.getName());
                        if (unkownPropsWritten == false && rtp.isOptional() && value == null) {
                            for (Map.Entry<String, Object> entry : unknownProps.entrySet()) {
                                pw.print(entry.getKey());
                                pw.print(" = ");
                                pw.println(entry.getValue());
                            }
                            unkownPropsWritten = true;
                        }
                        if (rtp.isOptional() && value == null) {
                            pw.print("# ");
                        }
                        pw.print(rtp.getName());
                        pw.print(" = ");
                        if (value == null) {
                            value = getDefaultValue(rtp);
                        }
                        if (value instanceof Short) {
                            value = NumberParser.format((Short) value);
                        } else if (value instanceof Integer) {
                            value = NumberParser.format((Integer) value);
                        } else if (value instanceof Long) {
                            value = NumberParser.format((Long) value);
                        } else if (value instanceof Double) {
                            value = NumberParser.format((Double) value);
                        } else if (value instanceof Float) {
                            value = NumberParser.format((Float) value);
                        }
                        pw.println(value);
                    }
                } finally {
                    pw.close();
                }
            } catch (IOException ex) {
                throw new GrmException("AbstractResourceCliCommand.ex.store", ex, getBundleName(), ex.getLocalizedMessage());
            }

            if (Platform.getPlatform().edit(tmpFile)) {
                return readSingleResourceProperties(makeInputStream(tmpFile), rt);
            } else {
                return null;
            }
        } finally {
            if (tmpFile != null) {
                tmpFile.delete();
            }
        }
    }

    private Object getDefaultValue(ResourcePropertyType rtp) {
        if (rtp.getDefaultValue() != null) {
            return rtp.getDefaultValue();
        } else if (rtp.isOptional()) {
            return I18NManager.formatMessage("AbstractResourceCliCommand.optional", getBundleName(), rtp.getType().getSimpleName());
        } else {
            return I18NManager.formatMessage("AbstractResourceCliCommand.mandatory", getBundleName(), rtp.getType().getSimpleName());
        }
    }

    private static class ResourcePropertyTypeComparator implements Comparator<ResourcePropertyType> {

        private final Map<String, Object> props;

        public ResourcePropertyTypeComparator(Map<String, Object> props) {
            this.props = props;
        }

        public int compare(ResourcePropertyType o1, ResourcePropertyType o2) {
            if (o1.isMandatory()) {
                if (o2.isOptional()) {
                    return -1;
                } else {
                    return o1.getName().compareTo(o2.getName());
                }
            } else if (o2.isMandatory()) {
                // o1 is optinal o2 is mandatory
                return 1;
            } else {
                // o1 and o2 are optional
                if (props.containsKey(o1.getName())) {
                    if (props.containsKey(o2.getName())) {
                        return o1.getName().compareTo(o2.getName());
                    } else {
                        return -1;
                    }
                } else {
                    if (props.containsKey(o2.getName())) {
                        return 1;
                    } else {
                        return o1.getName().compareTo(o2.getName());
                    }
                }
            }
        }
    }

    private class TableModel extends AbstractDefaultTableModel implements MultipleResourceActionCommand.Listener {

        private final static long serialVersionUID = -2007231101L;
        private final int rowCount;
        private final Map<Integer, ResourceActionResult> results;
        private final AbstractCli cli;
        private boolean errorOccured;

        public TableModel(AbstractCli cli, List<ResourceActionResult> rows) {
            super(false,true,false,cli.getDebug());
            this.cli = cli;
            this.rowCount = rows.size();
            this.results = new HashMap<Integer, ResourceActionResult>(rows.size());
            int row = 0;
            for (ResourceActionResult res : rows) {
                results.put(row++, res);
                if (res.getError() != null) {
                    errorOccured = true;
                }
            }
        }

        public TableModel(AbstractCli cli, int rowCount) {
            super(false,true,false,cli.getDebug());
            this.cli = cli;
            this.rowCount = rowCount;
            this.results = new HashMap<Integer, ResourceActionResult>(rowCount);
        }

        public boolean errorOccured() {
            return errorOccured;
        }

        public int getRowCount() {
            return rowCount;
        }

        public int getColumnCount() {
            return 2;
        }

        private ResourceActionResult getRow(int rowIndex) throws InterruptedException {
            ResourceActionResult ret = null;
            synchronized (results) {
                while (true) {
                    ret = results.get(rowIndex);
                    if (ret != null) {
                        break;
                    }
                    results.wait();
                }
            }
            return ret;
        }

        public void addRow(ResourceActionResult row) {
            synchronized (results) {
                results.put(results.size(), row);
                if (row.getError() != null) {
                    errorOccured = true;
                }
                results.notifyAll();
            }
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            try {
                switch (columnIndex) {
                    case 0:
                        return getRow(rowIndex).getResourceName();
                    case 1:
                        return getRow(rowIndex).getMessage();
                    default:
                        throw new IllegalArgumentException(I18NManager.formatMessage("AbstractResourceCliCommand.col.unknown", getBundleName(), columnIndex));
                }
            } catch (InterruptedException ex) {
                return getBundle().getString("AbstractResourceCliCommand.interrupted");
            }
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return getBundle().getString("AbstractResourceCliCommand.col.id");
                case 1:
                    return getBundle().getString("AbstractResourceCliCommand.col.msg");
                default:
                    throw new IllegalArgumentException(I18NManager.formatMessage("AbstractResourceCliCommand.col.unknown", getBundleName(), columnIndex));
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        public void commandExecuted(ResourceActionResult result) {
            addRow(result);
        }

        public void finished() {
            synchronized (results) {
                for(int i = 0; i < getRowCount(); i++) {
                    if (results.get(i) == null) {
                        results.put(i, new ResourceActionResult(new ResourceIdImpl(0), "unknown", getBundle().getString("AbstractResourceCliCommand.resultMissing")));
                    }
                }
                results.notifyAll();
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            String ret = "";
            try {
                ResourceActionResult r = getRow(row);               
                ret = errorStackTraceToString(r.getError());     
            } catch (InterruptedException ex) {
                //ignore
            }
            return ret;
        }
    }
}

