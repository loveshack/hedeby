/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceState;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Cli command that prints out services information - it prints out name, component
 * state in which the service is running, and service state itself
 */
@CliCommandDescriptor(
    name = "ShowServicesCliCommand",
    hasShortcut = true,
    category = CliCategory.MONITORING,
    bundle="com.sun.grid.grm.cli.cmd.monitoring.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ShowServicesCliCommand extends AbstractSortedTableCliCommand {

    
    public void execute(AbstractCli cli) throws GrmException {
        List<ComponentInfo> componentList = ComponentService.getComponentInfosByType(cli.getExecutionEnv(), Service.class);
        
        TableModel tm = new TableModel(cli.getExecutionEnv(), componentList);
        List<String> order = new ArrayList<String>(2);
        order.add(tm.getColumnName(0));
        order.add(tm.getColumnName(1));
        setDeafultColumnSortOrder(order);
        
        Table table = createTable(tm, cli);
        table.print(cli);
    }
    
    private class TableModel extends AbstractDefaultTableModel {
        
        private final ExecutionEnv env;
        private final List<ComponentInfo> componentList;
        private final Map<Integer,Service> serviceMap;
        
        public TableModel(ExecutionEnv aEnv, List<ComponentInfo> aComponentList) {
            env = aEnv;
            componentList = aComponentList;
            serviceMap = new HashMap<Integer,Service>(componentList.size());
        }
        
        public int getRowCount() {
            return componentList.size();
        }

        public int getColumnCount() {
            return 4;
        }

        public Service getService(int row) throws GrmException {
            Service ret = serviceMap.get(row);
            if(ret == null) {
                ComponentInfo ci = componentList.get(row);
                ret = ComponentService.<Service>getComponent(env, ci);
                serviceMap.put(row, ret);
            }
            return ret;
        }
        
        public Object getValueAt(int rowIndex, int columnIndex) {
            ComponentInfo ci = componentList.get(rowIndex);
            switch(columnIndex) {
                case 0: return ci.getHostname().getHostname();
                case 1: return ci.getName();
                case 2:
                {
                       ComponentState ret;
                       try {
                           Service svc = getService(rowIndex);
                           if(svc == null) {
                               ret = ComponentState.UNKNOWN;
                           } else {
                               ret = svc.getState();
                           }
                       } catch(Exception ex) {
                           ret = ComponentState.UNKNOWN;
                       }
                       return ret.toString();
                }
                case 3:
                {
                       ServiceState ret;
                       try {
                           Service svc = getService(rowIndex);
                           if(svc == null) {
                               ret = ServiceState.UNKNOWN;
                           } else {
                               ret = svc.getServiceState();
                           }
                       } catch(Exception ex) {
                           ret = ServiceState.UNKNOWN;
                       }
                       return ret.toString();
                }
                default:
                    throw new IllegalArgumentException("unknown column " + columnIndex);
            }
        }

        @Override
        public String getColumnName(int column) {
            switch(column) {
                case 0: return getBundle().getString("ShowServicesCliCommand.col.host");
                case 1: return getBundle().getString("ShowServicesCliCommand.col.name");
                case 2: return getBundle().getString("ShowServicesCliCommand.col.cstate");
                case 3: return getBundle().getString("ShowServicesCliCommand.col.sstate");
                default:
                    throw new IllegalArgumentException("unknown column " + column);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }
    }
}

