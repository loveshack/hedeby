/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/

/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.monitoring;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.ui.resource.ShowRequestCommand;
import com.sun.grid.grm.util.filter.Filter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;



/**
 * This command is responsible for showing the queued or the pending requests
 * as registered by resource provider.
 */
@CliCommandDescriptor(name = "ShowRequestCliCommand", hasShortcut = true, category = CliCategory.MONITORING, bundle = "com.sun.grid.grm.cli.cmd.monitoring.messages",
   requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ShowRequestCliCommand extends AbstractSortedTableCliCommand {
    @CliOptionDescriptor(name = "ShowRequestCliCommand.q", required = false)
    private boolean queued;
    @CliOptionDescriptor(name = "ShowRequestCliCommand.p", required = false)
    private boolean pending;
    @CliOptionDescriptor(name = "ShowRequestCliCommand.f", required = false)
    private boolean fullDesc;
    @CliOptionDescriptor(name = "ShowRequestCliCommand.s", numberOfOperands = 1, required = false)
    private String service = null;
    @CliOptionDescriptor(name = "ShowRequestCliCommand.slo", numberOfOperands = 1, required = false)
    private String slo = null;

    
    public void execute(AbstractCli cli) throws GrmException {
        String bundle = getBundleName();
        ShowRequestCommand srs = new ShowRequestCommand(queued, pending);
        srs.setSLO(slo);
        srs.setService(service);
        
        Map<String, List<Request>> resMap = cli.getExecutionEnv().getCommandService().execute(srs).getReturnValue();

        if (resMap.isEmpty()) {
            cli.out().println("ShowRequestCliCommand.res.notfound", bundle);

            return;
        }

        TableModel tm = new TableModel(resMap);
        Table table = createTable(tm, cli);
        if (!isSetMachineReadableOutput()) {
            if(fullDesc) {
                table.setColumnWidth(5, 30);
                table.setAutoWordWrap(5, true);
            }
        }
        table.print(cli);
    }

    private class TableModel extends AbstractDefaultTableModel {
        
        private final static long serialVersionUID = -2008012401L;
        
        private List<Line> rows;

        public TableModel(Map<String, List<Request>> resourceMap) {
            int rowCount = 0;

            for (List<Request> list : resourceMap.values()) {
                rowCount += list.size();
            }

            rows = new ArrayList<Line>(rowCount);

            for (Map.Entry<String, List<Request>> entry : resourceMap.entrySet()) {
                String type = entry.getKey();

                for (Request res : entry.getValue()) {
                    for (Need n : res.getCurrentNeeds()) {
                        rows.add(new Line(type, res.getServiceName(), res.getSLOName(), n.getQuantity(), n.getUrgency(), n.getResourceFilter()));
                    }
                }
            }
        }

        public Line getRow(int row) {
            return rows.get(row);
        }

        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            if (fullDesc) {
                return 6;
            } else {
                return 4;
            }
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            Line line = rows.get(rowIndex);

            switch (columnIndex) {
            case 0:
                return line.type;

            case 1:
                return line.service;

            case 2:
                return line.slo;

            case 3:
                return line.urgency;

            case 4:return line.quantity;

            case 5: return line.request;
            default:
                throw new IllegalArgumentException("unknown column " +
                    columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
            case 0:
                return getBundle().getString("ShowRequestCliCommand.col.type");

            case 1:
                return getBundle().getString("ShowRequestCliCommand.col.service");

            case 2:
                return getBundle().getString("ShowRequestCliCommand.col.slo");

            case 3:
                return getBundle().getString("ShowRequestCliCommand.col.urgency");

            case 4:
                return getBundle()
                           .getString("ShowRequestCliCommand.col.quantity");

            case 5:
                return getBundle()
                           .getString("ShowRequestCliCommand.col.request");
                
            default:
                throw new IllegalArgumentException("unknown column " + column);
            }
        }
    }

    private static class Line {
        private String type;
        private String service;
        private String slo;
        private int quantity;
        private Usage urgency;
        private Filter request;

        public Line(String aType, String aService, String aSlo, int aQuantity,
            Usage aUrgency, Filter aRequest) {
            type = aType;
            service = aService;
            slo = aSlo;
            quantity = aQuantity;
            urgency = aUrgency;
            request = aRequest;
        }
    }
}
