/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli.cmd.components;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.UsageException;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.component.JVMResultObject;
import com.sun.grid.grm.ui.component.StopJVMCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.util.List;

/**
 * The command is used to shut down all or just the specified JVM on a specified
 * host or on all hosts (if the host is not specified).
 */

@CliCommandDescriptor(
    name = "StopJVMCliCommand", 
    hasShortcut = true, 
    category = CliCategory.COMPONENTS,
    bundle = "com.sun.grid.grm.cli.cmd.components.messages",
    requiredPrivileges = {UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR})
public class StopJVMCliCommand extends AbstractSortedTableCliCommand {
    
    @CliOptionDescriptor(name = "StopJVMCliCommand.j", numberOfOperands = 1)
        private String jvm = null;       
    
    @CliOptionDescriptor(name = "StopJVMCliCommand.h", numberOfOperands = 1)
        private String host = null;
    
    @CliOptionDescriptor(name = "StopJVMCliCommand.a", numberOfOperands = 0)
        private boolean all = false;
    
    private final String empty = I18NManager.formatMessage("StopJVMCliCommand.empty", getBundleName());
    private static final String errorBundle = "com.sun.grid.grm.cli.client";
    
    public void execute(AbstractCli cli) throws GrmException {
        
        if ((jvm == null || host == null) && all == false 
                || (jvm != null && host != null && all == true)) {
            throw new UsageException(I18NManager.formatMessage("client.stopjvm.nall", errorBundle));
        } 
        
        StopJVMCommand uiCmd = new StopJVMCommand();
        uiCmd.setJVMName(jvm); 
        
        if (host != null) {
            /* resolve hostname in case of 'localhost' */
            uiCmd.setHostname(Hostname.getInstance(host).getHostname());
        }
        
        List<JVMResultObject> ret = cli.getExecutionEnv().getCommandService().<List<JVMResultObject>>execute(uiCmd).getReturnValue();            
        
        TableModel tm = new TableModel(ret, cli);
        Table table = createTable(tm, cli);
        table.print(cli);
        
        //check whether any error
        for (JVMResultObject r : ret) {
            if (r.getError()!=null) {
                throw new GrmException("client.error.cli_error", errorBundle);
            }
        }     
    }
    
    private class TableModel extends AbstractDefaultTableModel {

        private List<JVMResultObject> rows;

        public TableModel(List<JVMResultObject> result, AbstractCli cli) {
            super(false,true,false,cli.getDebug());
            rows = result;
        }

        public JVMResultObject getRow(int row) {
            return this.rows.get(row);
        }

        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 4;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            JVMResultObject elem = getRow(rowIndex);
            switch (columnIndex) {
                case 0:
                    return elem.getJvmName();
                case 1: 
                    return elem.getHostName();
                
                case 2: 
                    if (elem.getResult() == null) {
                        return empty;
                    }
                    return elem.getResult(); 
                case 3: 
                    if (elem.getMessage() == null) {
                        return empty;
                    }
                    return elem.getMessage();   
                default:
                    throw new IllegalArgumentException(I18NManager.formatMessage("client.stopjvm.uc", errorBundle, columnIndex));
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return getBundle().getString("StopJVMCliCommand.col.jvm");
                case 1:
                    return getBundle().getString("StopJVMCliCommand.col.host");                       
                case 2:
                    return getBundle().getString("StopJVMCliCommand.col.result");
                case 3:
                    return getBundle().getString("StopJVMCliCommand.col.message");                       
                default:
                    throw new IllegalArgumentException(I18NManager.formatMessage("client.stopjvm.uc", errorBundle, column));
            }
        }

        @Override
        public String getValueForDebugColumn(int row) {
            JVMResultObject jrr = getRow(row);
            return errorStackTraceToString(jrr.getError());
        }   
    }
}
