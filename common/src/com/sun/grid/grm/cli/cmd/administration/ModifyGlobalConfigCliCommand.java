/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.administration;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.component.ModifyGlobalConfigurationCommand;

/**
 *  This cli command get the global configuration from CS, opens
 *  it in an editor, validate the modified configuration and
 *  sends it back to CS
 */
@CliCommandDescriptor(
    name = "ModifyGlobalConfigCommand",
    hasShortcut = true,
    category = CliCategory.ADMINISTRATION,
    bundle="com.sun.grid.grm.cli.cmd.administration.messages",
    requiredPrivileges= { UserPrivilege.ADMINISTRATOR, UserPrivilege.READ_BOOTSTRAP_CONFIG })
public class ModifyGlobalConfigCliCommand extends AbstractCliCommand {
    
    public void execute(AbstractCli cli) throws GrmException {
        
        GetGlobalConfigurationCommand cmd = new GetGlobalConfigurationCommand();
        GlobalConfig conf = cli.getExecutionEnv().getCommandService().execute(cmd).getReturnValue();
        
        GlobalConfig newConf = XMLUtil.edit(cli.getExecutionEnv(), conf, "global");
        if(newConf != null) {
            ModifyGlobalConfigurationCommand mod = new ModifyGlobalConfigurationCommand(newConf);
            cli.getExecutionEnv().getCommandService().execute(mod);
            cli.out().println("ModifyGlobalConfigCommand.updated", getBundleName());
        } else {
            cli.out().println("ModifyGlobalConfigCommand.unchanged", getBundleName());
        }
    }
    
    
    
}
