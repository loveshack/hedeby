/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli.cmd.components;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.ui.component.AbstractComponentCommand;
import com.sun.grid.grm.ui.component.ComponentChangeResult;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.util.List;

/**
 * Abstract base class which invokes a method on a list of matching components.
 */
@CliCommandDescriptor(name = "AbstractComponentCliCommand", hasShortcut = false, category = CliCategory.COMPONENTS, bundle = "com.sun.grid.grm.cli.cmd.components.messages")
public abstract class AbstractComponentCliCommand extends AbstractSortedTableCliCommand {

    @CliOptionDescriptor(name = "AbstractComponentCliCommand.c", numberOfOperands = 1)
        private String component = null;
    @CliOptionDescriptor(name = "AbstractComponentCliCommand.h", numberOfOperands = 1)
        private String host = null;

    private static final String errorBundle = "com.sun.grid.grm.cli.client";
    /**
     * create new command
     * @throws com.sun.grid.grm.GrmException 
     * @return AbstractComponentCommand instance
     */
    protected abstract AbstractComponentCommand createCommand() throws GrmException;
        
    /**
     * Executes the command. 
     * 
     * @param cli the cli object
     * @throws com.sun.grid.grm.GrmException 
     */
    public final void execute(AbstractCli cli) throws GrmException {
        AbstractComponentCommand uiCmd = createCommand();
        uiCmd.setComponentName(component);
        /* resolve hostname in case of 'localhost' */
        if(host != null) {
            uiCmd.setHostname(Hostname.getInstance(host).getHostname());
        }
        List<ComponentChangeResult> affectedComps = cli.getExecutionEnv().getCommandService().<List<ComponentChangeResult>>execute(uiCmd).getReturnValue();        
        TableModel tm = new TableModel(cli, affectedComps);
        Table table = createTable(tm, cli);
        table.print(cli);
        
        for (ComponentChangeResult res : affectedComps) {
            if (res.getError()!=null) {
                throw new GrmException("client.error.cli_error", res.getError(), errorBundle);
            }
        }
    }
    
    private class TableModel extends AbstractDefaultTableModel {
        private final static long serialVersionUID = -2007231101L;
        
        private final List<ComponentChangeResult> rows;
        private final AbstractCli cli;
        
        public TableModel(AbstractCli cli, List<ComponentChangeResult> rows) {
            super(false,true,false,cli.getDebug());
            this.cli = cli;
            this.rows = rows;
        }
        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 3;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            ComponentChangeResult row = rows.get(rowIndex);
            switch(columnIndex) {
                case 0: return row.getComponentInfo().getName();
                case 1: return row.getComponentInfo().getHostname().toString();
                case 2:
                    return row.getMessage();
                default:
                    throw new IllegalArgumentException(I18NManager.formatMessage("client.stopcomp.uc", errorBundle, columnIndex));
            }
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch(columnIndex) {
                case 0: return getBundle().getString("AbstractComponentCliCommand.col.name");
                case 1: return getBundle().getString("AbstractComponentCliCommand.col.host");
                case 2: return getBundle().getString("AbstractComponentCliCommand.col.msg");
                default:
                    throw new IllegalArgumentException(I18NManager.formatMessage("client.stopcomp.uc", errorBundle, columnIndex));
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getValueForDebugColumn(int row) {
            ComponentChangeResult ccr = rows.get(row);
            return errorStackTraceToString(ccr.getError());
        }
        
    }
}

