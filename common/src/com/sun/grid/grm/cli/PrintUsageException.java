/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli;

import com.sun.grid.grm.GrmException;

/**
 * Used to tell the CLI that it should print usage information and exit.
 *
 */
public class PrintUsageException extends GrmException {
    private Class<? extends CliCommand> target = null;

    /**
     * Creates a new instance of <code>PrintUsageException</code> without detail
     * message.
     */
    public PrintUsageException() {
    }

    /**
     * Constructs an instance of <code>PrintUsageException</code> with the
     * specified target.
     * @param target the usage message target
     */
    public PrintUsageException(Class<? extends CliCommand> target) {
        this();

        this.target = target;
    }
    
    /**
     * Returns the target CLI command class
     * @return CliCommand class which has thrown the exception
     */
    public Class<? extends CliCommand> getTarget() {
        return target;
    }
}
