/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Annotation for all CLI options.  This annotation is used by the
 * AbstractCli class to parse argument and produce help output.
 *
 * @see AbstractCli
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CliCommandDescriptor {
    /**
     * The root name of the resource property key to be used to localize this
     * command.  The command's name is &lt;name&gt;.name.  The description is
     * &lt;name&gt;.description.  The shortcut is &lt;name&gt;.shortcut.  The
     * argument is &lt;name&gt;.argument.  The name is how the command is
     * invoked, e.g. add_resource or --debug.  The description is a short
     * explanation of what the command does.  The shortcut is a shortened
     * version of the name that can also be used to invoke the command, e.g.
     * ar or -d.  The argument is a description of any arguments the command
     * can take.  Because there is only one argument field, a command
     * that can multiple arguments must describe them all in a single string.
     * 
     * @return the resource property key for the cli command
     */
    String name();
    
    /**
     * If true, this command has a shortcut.  The shortcut is found by the
     * resource property key, &lt;name&gt;.shortcut.
     * 
     * @return <code>true</code> of the cli command has a shortcut
     */
    boolean hasShortcut() default false;
    
    /**
     *  Name of the resource bundle of this cli command
     * 
     * @return the resource bundle of the Cli command
     */
    String bundle() default "com.sun.grid.grm.cli.client";
    
    /**
     * Category of the Cli Command
     * Allowed Categories are:
     * 
     * Monitoring
     * Security
     * Installation
     * Resources
     * Services
     * Components
     * @return the category of the Cli command
     */
    CliCategory category();
    
    /**
     * Get the list of required privilieges for executing the command
     * 
     * @return the list of required privileges for this command
     */
    String[] requiredPrivileges() default {};
    
    /**
     * Get the list of optional privileges this command is using
     * @return the list of optional privileges
     */
    String[] optionalPrivileges() default {};
}
