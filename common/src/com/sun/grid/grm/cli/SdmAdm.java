/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.cli;

import com.sun.grid.grm.cli.cmd.monitoring.ShowSMFSupportCliCommand;
import com.sun.grid.grm.cli.cmd.resources.AddResourceCliCommand;
import com.sun.grid.grm.cli.cmd.services.AddSparePoolCliCommand;
import com.sun.grid.grm.cli.cmd.administration.AddSystemCliCommand;
import com.sun.grid.grm.cli.cmd.administration.GetLoggingCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowReporterDataCliCommand;
import com.sun.grid.grm.cli.cmd.administration.InstallManagedHostCliCommand;
import com.sun.grid.grm.cli.cmd.administration.InstallMasterCliCommand;
import com.sun.grid.grm.cli.cmd.administration.ModifyComponentConfigCliCommand;
import com.sun.grid.grm.cli.cmd.administration.ModifyGlobalConfigCliCommand;
import com.sun.grid.grm.cli.cmd.resources.ModifyResourceCliCommand;
import com.sun.grid.grm.cli.cmd.resources.MoveResourceCliCommand;
import com.sun.grid.grm.cli.cmd.components.ReloadComponentCliCommand;
import com.sun.grid.grm.cli.cmd.resources.RemoveResourceCliCommand;
import com.sun.grid.grm.cli.cmd.services.RemoveServiceCliCommand;
import com.sun.grid.grm.cli.cmd.administration.RemoveSystemCliCommand;
import com.sun.grid.grm.cli.cmd.resources.ResetResourceCliCommand;
import com.sun.grid.grm.cli.cmd.administration.SetDefaultSystemCliCommand;
import com.sun.grid.grm.cli.cmd.administration.SetLoggingCliCommand;
import com.sun.grid.grm.cli.cmd.administration.SetSystemPropertyCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowConfigsCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowJvmsCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowModulesCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowRequestCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowResourceStateCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowServicesCliCommand;
import com.sun.grid.grm.cli.cmd.components.StartComponentCliCommand;
import com.sun.grid.grm.cli.cmd.components.StartJVMCliCommand;
import com.sun.grid.grm.cli.cmd.services.StartServiceCliCommand;
import com.sun.grid.grm.cli.cmd.components.StopComponentCliCommand;
import com.sun.grid.grm.cli.cmd.components.StopJVMCliCommand;
import com.sun.grid.grm.cli.cmd.services.StopServiceCliCommand;
import com.sun.grid.grm.cli.cmd.administration.UninstallHostCliCommand;
import com.sun.grid.grm.cli.cmd.administration.UnsetDefaultSystemCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowBlackListCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowComponentStatusCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowLogsCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowResourceTypesCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowSLOCliCommand;
import com.sun.grid.grm.cli.cmd.monitoring.ShowSystemVersionCliCommand;
import com.sun.grid.grm.cli.cmd.resources.AddResourceIdToBlackListCliCommand;
import com.sun.grid.grm.cli.cmd.resources.PurgeResourceCliCommand;
import com.sun.grid.grm.cli.cmd.resources.RemoveResourceIdFromBlackListCliCommand;

/**
 * Command line client for hedeby system
 */
public class SdmAdm extends AbstractCli {

    /** Creates a new instance of SdmAdm */
    public SdmAdm() {
        super("sdmadm");
        /*
         * Commands for simplified installation, uninstallation
         */
        addCommand(InstallMasterCliCommand.class);
        addCommand(InstallManagedHostCliCommand.class);
        addCommand(UninstallHostCliCommand.class);
        /*
         * Commands for managing preferences
         */
        addCommand(AddSystemCliCommand.class);                                  /* add_system */
        addCommand(RemoveSystemCliCommand.class);                               /* remove_system */
        addCommand(SetDefaultSystemCliCommand.class);                           /* set_default_system */
        addCommand(UnsetDefaultSystemCliCommand.class);                         /* unset_default_system */
        addCommand(SetSystemPropertyCliCommand.class);                          /* set_system_property */

        /*
         * Commands for component lifecycle 
         */
        addCommand(StartJVMCliCommand.class);                                   /* start */
        addCommand(StopJVMCliCommand.class);                                    /* shutdown */
        addCommand(StartComponentCliCommand.class);                             /* start_component */
        addCommand(StopComponentCliCommand.class);                              /* shutdown_component */
        addCommand(ReloadComponentCliCommand.class);                            /* reload_component */

        /*
         * Commands for showing information
         */
        addCommand(ShowComponentStatusCliCommand.class);                        /* show_status */
        addCommand(ShowConfigsCliCommand.class);                                /* show_configs */
        addCommand(ShowModulesCliCommand.class);                                /* show_modules */
        addCommand(ShowServicesCliCommand.class);                               /* show_services */
        addCommand(ShowJvmsCliCommand.class);                                   /* show_jvms */
        addCommand(ShowLogsCliCommand.class);                                   /* log_jvms */
        addCommand(ShowRequestCliCommand.class);                                /* show_request */
        addCommand(ShowSLOCliCommand.class);                                    /* show_slo */
        addCommand(ShowReporterDataCliCommand.class);                           /* show_history */
        addCommand(ShowBlackListCliCommand.class);                              /* show_blacklist */
        addCommand(ShowSMFSupportCliCommand.class);                             /* show_smf_support */
        addCommand(ShowResourceTypesCliCommand.class);                          /* show_resource_types */
        addCommand(ShowSystemVersionCliCommand.class);                          /* show_system_version */
        
        /*
         * Commands for setting and getting loggers information
         */
        addCommand(GetLoggingCliCommand.class);                                 /* get_log_level */
        addCommand(SetLoggingCliCommand.class);                                 /* set_log_level */
                
        /*
         * Commands for modifing the configuration
         */
        addCommand(ModifyGlobalConfigCliCommand.class);
        addCommand(ModifyComponentConfigCliCommand.class);
        /*
         * Commands for resource maintenance
         */
        addCommand(AddResourceCliCommand.class);
        addCommand(MoveResourceCliCommand.class);
        addCommand(RemoveResourceCliCommand.class);
        addCommand(PurgeResourceCliCommand.class);
        addCommand(ResetResourceCliCommand.class);
        addCommand(ModifyResourceCliCommand.class);
        addCommand(ShowResourceStateCliCommand.class);
        addCommand(AddResourceIdToBlackListCliCommand.class);                   /* add_resource_to_blacklist */
        addCommand(RemoveResourceIdFromBlackListCliCommand.class);              /* remove_resource_from_blacklist */

        /*
         * Commands for managing services
         */
        addCommand(AddSparePoolCliCommand.class);                               /* add_spare_pool */
        addCommand(StartServiceCliCommand.class);                               /* start_service */
        addCommand(StopServiceCliCommand.class);                                /* stop_service */
        addCommand(RemoveServiceCliCommand.class);
    }

    /**
     * main method of cli client
     * @param args command line arguments
     */
    public static void main(String[] args) {
        SdmAdm sdmadm = new SdmAdm();
        sdmadm.run(args);
        sdmadm.out().close();
        sdmadm.err().close();
    }
}
