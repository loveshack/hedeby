/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Annotation for all CLI options.  This annotation is used by the
 * AbstractCli class to parse argument and produce help output.
 *
 * @see AbstractCli
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CliOptionDescriptor {
    
    /**
     * The root name of the resource property key to be used to localize this
     * command.  The command's name is &lt;name&gt;.name.  The description is
     * &lt;name&gt;.description.  The shortcut is &lt;name&gt;.shortcut.  The
     * argument is &lt;name&gt;.argument.  The name is how the command is
     * invoked, e.g. add_resource or --debug.  The description is a short
     * explanation of what the command does.  The shortcut is a shortened
     * version of the name that can also be used to invoke the command, e.g.
     * ar or -d.  The argument is a description of any arguments the command
     * can take.  Because there is only one argument field, a command
     * that can multiple arguments must describe them all in a single string.
     */
    String name();
    
    /**
     * The number of arguments this command can take.  If 0, this option is
     * a boolean switch, e.g. -f.  If 1,
     * then this command takes exactly 1 argument, described by the argument
     * string.  If more than 1, this command takes exactly that number of
     * arguments, and all the arguments are described by the same argument
     * string.  If less than 0, this command takes 0 or more arguments as
     * described by the argument string.  
     */
    int numberOfOperands() default 0;
    
    /**
     * If true, then this command must be included in a command-line invocation
     * for the execution to succeed.
     */
    boolean required() default false;
    
    /**
     * If true, this command has a shortcut.  The shortcut is found by the
     * resource property key, &lt;name&gt;.shortcut.
     */
    boolean hasShortcut() default false;
    
    /**
     * If true, then this command assumes that the argument is a list of arguments
     * separated by "," sign
     * example: -t first,second,third
     */
    boolean list() default false;
    
}
