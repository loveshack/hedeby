/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.cli;

import com.sun.grid.grm.GrmException;

/**
 * This interface describes a command class.  In addition to implementing this
 * interface, a command class must also have a CliDescriptor annotation applied
 * to it and any fields which represent command options.
 *
 * @see CliCommandDescriptor
 */
public interface CliCommand {
    /**
     * Causes the command object to be executed.  The reference to the
     * AbstractCli is useful for retrieving global option values.  All annotated
     * fields will have their values assigned before the execute() method is
     * called.  The execute() method can simply assume the values are there.
     * Dependecies which cannot be expressed through the CliDescriptor
     * annotation must be checked explicitly by this method.
     * @param cli the cli object
     * @throws com.sun.grid.grm.GrmException if the command failed
     */
    public void execute(AbstractCli cli) throws GrmException;
    
    /**
     * Get the descriptor of the CliCommand
     * 
     * @return the descriptor
     */
    public CliCommandDescriptor getDescriptor();

}
