/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import com.sun.grid.grm.util.I18NManager;
import javax.net.ssl.SSLHandshakeException;

/**
 *  Generic exception which transports unknown communication errors to the 
 *  caller. 
 *  Each remote manageable method must declare the GrmRemoteException in the
 *  clause.
 */
public class GrmRemoteException extends GrmException {

    private static final String BUNDLE = "com.sun.grid.grm.messages";
    private static final long serialVersionUID = -2007120301L;

    /**
     * Creates a new instance of <code>GrmRemoteException</code> without detail message.
     */
    public GrmRemoteException() {
        super();
    }

    /**
     * Constructs an instance of <code>GrmRemoteException</code> with the specified
     * detail message.
     * @param msg the detail message.
     */
    public GrmRemoteException(String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>GrmRemoteException</code> with the specified
     * internationalized detail message.
     * @param msg the detail message.
     * @param bundleName the name of the resource bundle
     * @param params the parameters to the message
     */
    public GrmRemoteException(String msg, String bundleName, Object... params) {
        super(msg, bundleName, params);
    }

    /**
     * Constructs an instance of <code>GrmRemoteException</code> with the specified
     * detail message and source.
     * @param msg the detail message.
     * @param cause the source Throwable
     */
    public GrmRemoteException(String msg, Throwable cause) {
        super(msg, cause);
    }

    /**
     * Constructs an instance of <code>GrmRemoteException</code> with the specified
     * internationalized detail message.
     * @param msg the detail message.
     * @param cause the source Throwable
     * @param bundleName the name of the resource bundle
     * @param params the parameters to the message
     */
    public GrmRemoteException(String msg, Throwable cause, String bundleName, Object... params) {
        super(msg, cause, bundleName, params);
    }

    /**
     * It the cause of this exception a ssl problem
     * @return <code>true</code> if the cause of this exception is an ssl problem
     */
    public boolean isCausedBySSLProblem() {
        for (Throwable t = getCause(); t != null; t = t.getCause()) {
            if (t instanceof SSLHandshakeException) {
                return true;
            }
        }
        return false;
    }

    /**
     * Find the real error in the causes of an exception
     * @param ex the exception
     * @return the real error message
     */
    /*
    public static String findRealMessage(Throwable ex) {
    for (Throwable t = ex; t != null; t = t.getCause()) {
    if (t instanceof SSLHandshakeException) {
    return t.getLocalizedMessage();
    } else if (t instanceof java.net.ConnectException) {
    return t.getLocalizedMessage();
    } else if (t instanceof java.net.SocketException) {
    return t.getLocalizedMessage();
    } else if (t instanceof java.io.EOFException) {
    return t.getLocalizedMessage();
    }
    }
    return ex.getLocalizedMessage();
    }
     */
    /**
     * Find the real error in the causes of an exception 
     * These exception can be any exception involved in communication problems
     * @param ex the exception
     * @return the real error message (communication related problem)
     */
    public static String findRealMessage(Throwable ex) {
        for (Throwable t = ex; t != null; t = t.getCause()) {
            String curMsg = t.getLocalizedMessage();
            boolean isCurMsgOk = (curMsg != null);

            //this list of exceptions can be extended, but be aware of inheritance
            //example: before checking for IOException check EOFException, as EOFException is subclass 
            if (t instanceof SSLHandshakeException) {
                return isCurMsgOk ? curMsg : I18NManager.formatMessage("remoteException.realErrorSSLHandshake", BUNDLE);
            } else if (t instanceof java.net.ConnectException) {
                return isCurMsgOk ? curMsg : I18NManager.formatMessage("remoteException.realErrorConnect", BUNDLE);
            } else if (t instanceof java.net.SocketException) {
                return isCurMsgOk ? curMsg : I18NManager.formatMessage("remoteException.realErrorSocket", BUNDLE);
            } else if (t instanceof java.io.EOFException) {
                return isCurMsgOk ? curMsg : I18NManager.formatMessage("remoteException.realErrorEOF", BUNDLE);
            }
        }
        return ex.getLocalizedMessage();
    }
}
