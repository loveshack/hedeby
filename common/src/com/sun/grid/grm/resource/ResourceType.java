/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource;

import java.util.Map;

/**
 *  Interface for the type of a resource
 */
public interface ResourceType {

    
    /**
     * Get the name of the resource type
     * @return the name of the resource type
     */
    public String getName();
    
    /**
     * Get the name of the resource
     * @param properties the properties if the resource
     * @return the name of the resource
     * @throws InvalidResourcePropertiesException if the properties does not contain the information for the resource name
     */
    public String getResourceName(Map<String,Object> properties) throws InvalidResourcePropertiesException;

    /**
     * Get the bound state of the resource by evaluating the properties of the
     * resource
     * @param properties the resource properties
     * @return <code>true</code> if the resource is bound
     */
    public boolean isResourceBound(Map<String,Object> properties);

    /**
     * Get the property types of the resource type
     * @return map with name of the property type as key and the property type a value
     *         (immutable)
     */
    public Map<String,ResourcePropertyType> getPropertyTypes();

    /**
     * Create resource properties which are derived from the name of the resource
     * @param name the name of the resource
     * @return the resource properties
     */
    public Map<String, Object> createResourcePropertiesForName(String name);

    /**
     * Fill the property map with the default values
     * @param props modifiable map
     */
    public void fillResourcePropertiesWithDefaults(Map<String,Object> props);
    
    /**
     * Validate a map of properties
     * 
     * This method should only validate these properties which are absolutetly 
     * necessary for the resource (e.g. check mandatory properties)
     * 
     * @param properties  the properties
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the properties are not valid
     *      for this resource type
     */
    public void validate(Map<String,Object> properties) throws InvalidResourcePropertiesException;
    
}
