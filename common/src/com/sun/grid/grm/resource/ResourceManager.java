/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;

import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.service.impl.DefaultService;
import com.sun.grid.grm.ui.resource.ResourceActionResult;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * The ResourceManager interface defines the operations which apply to the
 * management of Resource objects.  This interface is intended for use in the
 * Resource provider component.
 *
 */
public interface ResourceManager {

    /**
     * Add a resource to the resource manager.
     *
     * <p>This method adds a resource to default service of the resource manager (holds all unassigned resources).
     *    With  the next resource processing the resource will be moved to a real service.</p>
     * 
     * @param resource  the resource.
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the default service is not active
     * @throws InvalidResourceException if the resource is not accepted by the default service
     */
    public void addResource(Resource resource) throws ServiceNotActiveException, InvalidResourceException;

    /**
     * Get the default service of the resource manager
     *
     * <p>The default service is not a real service, it is the place where unassigned
     *    resource are stored.</p>
     * 
     * @return the default service
     */
    public DefaultService getDefaultService();
    
    /**
     * Remove resources from the system
     * @param resIdOrNameList list of resource names or resource ids
     * @param descr the removal descriptor
     * @return list of actions that describe the result for each resource
     */
    public List<ResourceActionResult> removeResources(List<String> resIdOrNameList, ResourceRemovalDescriptor descr);

    /**
     * Reset a list of resources.
     * @param resIdOrNameList resource names or resource ids
     * @return result of the action for each resource
     */
    public List<ResourceActionResult> resetResources(List<String> resIdOrNameList);
    
    /**
     * Modify a resource.
     * @param resId the resource id
     * @param properties the new properties
     * @return the result of the resource action
     */
    public ResourceActionResult modifyResource(ResourceId resId, Map<String,Object> properties);

    /**
     * Modifies resources.
     *
     * @param resIdOrNameMapWithProperties Map containing the resource id or name as key and the new resource
     *             properties as values
     * @return the list of resource action results (for each key in <tt>resIdOrNameMapWithProperties</tt> one result)
     */
    public List<ResourceActionResult> modifyResources(Map<String, Map<String, Object>> resIdOrNameMapWithProperties);
    
    /**
     * Retrieves a resource with specified ID.
     * 
     * @param id a resource to be retrieved
     * @return resource if found
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource
     * was not found in the system
     */
    Resource getResource(ResourceId id) throws UnknownResourceException;

    /**
     * Get the list of unassigned resources that matches a filter
     * @param filter  the filter
     * @return the list of unassigned resources
     */
    List<Resource> getUnassignedResources(Filter<Resource> filter);
    
    
    /**
     * Gets a list of all resources that belong to the given service and matching
     * a filter.
     * (cached information).
     *
     * @param service the target service's id
     * @param filter  the resource filter
     * @return a list of resources
     * @throws com.sun.grid.grm.service.UnknownServiceException if the service
     * was not found in the system
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service
     * is not active
     * @throws com.sun.grid.grm.service.InvalidServiceException if the target
     * service is not longer valid
     */
    List<Resource> getResources(String service, Filter<Resource> filter)
            throws UnknownServiceException, ServiceNotActiveException,
            InvalidServiceException;

    /**
     * Get a resource by it's or name.
     * 
     * @param idOrName
     * @return the resources
     * @throws ServiceNotActiveException if the owning service is not active
     * @throws com.sun.grid.grm.resource.InvalidResourceException if idOrName does not
     *    address exactly one resource
     * @throws UnknownResourceException If no resource with this id or name has been found
     */
    public Resource getResourceByIdOrName(String idOrName) throws ServiceNotActiveException, InvalidResourceException, UnknownResourceException;

    /**
     * Move resource to a service
     * @param idOrNameList   list of resource names or resource ids
     * @param service       the target service
     * @param forced       if <tt>true</tt> move the resources forced
     * @param isStatic      if <tt>true</tt> the resource will become static at the target service
     * @return results of the individual resource movements
     */
    public List<ResourceActionResult> moveResources(List<String> idOrNameList, String service, boolean forced, boolean isStatic);

    /**
     *  Add an ResourceProviderEventListener to the resource manager
     *  @param lis the listener
     */
    public void addResourceProviderEventListener(
            ResourceProviderEventListener lis);

    /**
     *  Remove an ResourceProviderEventListener to the resource manager
     *  @param lis the listener
     */
    public void removeResourceProviderEventListener(
            ResourceProviderEventListener lis);

    /**
     * Handles notification that a a service requested a resource.
     * @param event a resource request event with details
     */
    public void processResourceRequestEvent(ResourceRequestEvent event);

    /**
     * Handles notification that a resource was added to service.
     * @param service the id of the target service
     * @param added the target resource
     */
    public void processResourceAddedEvent(String service, Resource added);

    /**
     * Handles notification that a resource was modified by the service.
     * @param service the id of the target service
     * @param modified the target resource
     * @param changes a set of properties and attributes that were changed
     */
    public void processResourceChangedEvent(String service, Resource modified, Collection<ResourceChanged> changes);

    /**
     * Handles notification that a resource is in error.
     * 
     * @param serviceName the id of the source service
     * @param bad the target resource
     * @param message a message describing the reason for the error
     */
    public void processResourceErrorEvent(String serviceName, Resource bad, String message);

    /**
     * Handles notification that a resource was released from a service.
     * @param serviceName the id of the source service
     * @param released the target resource
     */
    public void processResourceRemovedEvent(String serviceName, Resource released);

    /**
     * Handles notification that a service has started to add a resource.
     * @param serviceName id of the target service
     * @param resource the target resource
     */
    public void processAddResourceEvent(String serviceName, Resource resource);

    /**
     * Handles notification that a service has started to remove a resource.
     * @param serviceName id of the target service
     * @param resource the resource
     */
    public void processRemoveResourceEvent(String serviceName, Resource resource);

    /**
     * Handles notification that a resource was rejected from a service during the assignement.
     * 
     * TODO should we provide more advanced logic or is throwing the resource
     * back to RP enough?
     * 
     * @param serviceName the id of the source service
     * @param rejected the target resource
     */
    public void processResourceRejectedEvent(String serviceName, Resource rejected);

    /**
     * Handles notification that a resource was reset (from error state).
     *
     * @param serviceName the id of the source service
     * @param reset the target resource
     */
    public void processResourceResetEvent(String serviceName, Resource reset);

    /**
     * Registers Id of a resource on a service's resource blacklist. Resource
     * on service's blacklist is not considered as a candidate to be assigned
     * to the service (during the resource request processing or manual
     * move of the resource to the service).
     *
     * @param idOrNameList list of resource ids or resource names
     * @param service name of the service that registers the resource
     *
     * @return list of resource action result that describes what happend for each entry of the
     *          idOrNameList
     */
    public List<ResourceActionResult> putResourcesOnBlackList(List<String> idOrNameList, String service);

    /**
     * Remove resources from the blacklist of a service. The resource are addressed
     * by it's name or id.
     * @param idOrNameList list of resource names or ids
     * @param service the name of the service
     * @return list of action resource for each entry if the idOrNameList
     */
    public List<ResourceActionResult> removeResourcesFromBlackList(List<String> idOrNameList, String service);

    /**
     * Gets a list of all resource ids that are on a blacklist of specified service. 
     * 
     * @param service name of the service
     * @return the list of blacklisted resource ids
     */
    public List<ResourceIdAndName> getBlackList(String service);

    /**
     * Unregisters ID of a resource from a service's resource blacklist.
     *
     * @param resourceId the id to be unregistered
     * @param service name of the service that unregisters the resource
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not
     * on service's blacklist
     * @return the number of remaining services that has the resource id on its blacklist
     */
    public int removeResourceIDFromBlackList(ResourceId resourceId, String service) throws UnknownResourceException;

    /** 
     * Checks whether the resource with specified Id is on the service's resource
     * blacklist. 
     * 
     * @param resourceId the id to be find out
     * @param service name of the service 
     * @return true is resource with given id is on service's resource blacklist
     */
    public boolean isResourceIDOnBlackList(ResourceId resourceId, String service);

    /**
     * Starts the resource manager.
     */
    public void start();

    /**
     * Stops the resource manager.
     */
    public void stop();

    /**
     * Sets the request queue for working with resource requests.
     * 
     * @param rq a request queue to set.
     */
    public void setRequestQueue(RequestQueue rq);

    /**
     * Delegate method for RequestQueue.triggerRequestQueueReprocessing().
     */
    public void triggerRequestQueueReprocessing();        
    
    
     /**
     * Handles notification that a orders and Requests have to be purged.
     * The method should be implemented in a way that all orders and request that
     * have been received *after* processPurgeOrdersAndRequestsEvent should be purged   
     * @param source the id of the source service
     */
    public void processPurgeOrdersAndRequestsEvent(String source);
   
   }
