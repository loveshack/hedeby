/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.impl.ResourceImpl;
import com.sun.grid.grm.bootstrap.Module;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.resource.adapter.impl.DefaultResourceAdapter;
import com.sun.grid.grm.resource.impl.AbstractResourceFactory;
import com.sun.grid.grm.resource.impl.TempResourceType;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Factory class for Resource objects
 * @param <RA> the type of the <tt>ResourceAdapter</tt>
 */
public class DefaultResourceFactory<RA extends ResourceAdapter> extends AbstractResourceFactory<RA> {

    private final static String BUNDLE = "com.sun.grid.grm.resource.resource";
    private final static Map<String, ResourceType> KNOWN_TYPES;

    static {
        Map<String, ResourceType> tmp = new HashMap<String, ResourceType>();
        tmp.put(HostResourceType.HOST_TYPE, HostResourceType.getInstance());
        tmp.put(TempResourceType.TEMP_TYPE, TempResourceType.getInstance());

        for (Module m : Modules.getModules()) {
            for (ResourceType rt : m.getResourceTypes()) {
                tmp.put(rt.getName(), rt);
            }
        }
        KNOWN_TYPES = Collections.unmodifiableMap(tmp);
    }

    /**
     * Get all known resource types as unmodifiable collection.
     * @return all known resource types
     */
    public static Collection<ResourceType> getResourceTypes() {
        return KNOWN_TYPES.values();
    }
    
    /**
     * Get a resource type
     * @param name  the name of the resource type
     * @return the resource type (it never returns null)
     * @throws com.sun.grid.grm.resource.UnknownResourceTypeException if the resource type is unknown
     */
    public static ResourceType getResourceType(String name) throws UnknownResourceTypeException {
        ResourceType ret = KNOWN_TYPES.get(name);
        if (ret == null) {
            throw new UnknownResourceTypeException(name);
        } else {
            return ret;
        }
    }

    private final static DefaultResourceFactory<DefaultResourceAdapter> instance =
            new DefaultResourceFactory<DefaultResourceAdapter>();

    /**
     * Get the singleton instance of the default resource factory
     * @return the singleton instance of the default resource factory
     */
    public static DefaultResourceFactory<DefaultResourceAdapter> getInstance() {
        return instance;
    }

    /**
     * This method throws always a GrmException, becase the <tt>DefaultResourceFactory</tt> does
     * not support an interanl representation of a resource
     * @param env  the execution env of the system
     * @param internalRepresentation the internal representation of a resource
     * @return nothing
     * @throws com.sun.grid.grm.GrmException the resource adapter could not be create
     */
    public RA createResourceAdapter(ExecutionEnv env, Object internalRepresentation) throws GrmException {
        throw new GrmException("rf.ex.ns", BUNDLE);
    }

    /**
     * Create a resource object out of the resource name.
     * @param env    the execution env
     * @param type   the type of the resource
     * @param name   the name of the resource
     * @return  the resource
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the name is not valid
     * @throws ResourceIdException if the id of the resource could not be created
     */
    public static Resource createResourceByName(ExecutionEnv env, ResourceType type, String name) throws InvalidResourcePropertiesException, ResourceIdException {
        return getInstance().createResource(env, type, type.createResourcePropertiesForName(name));
    }

    /**
     * This method reads a resource from a file 
     * @param file the target file
     * @return the resource read from file
     * @throws java.io.IOException if any error while reading from file occured
     */
    public static Resource readFromFile(File file) throws IOException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
        try {
            Object obj = in.readObject();
            if (obj instanceof ResourceImpl) {
                return (Resource) obj;
            }
            // Here we can do upgrade

            // We do not know this resource implementation
            throw new IOException(I18NManager.formatMessage("rf.ex.cnf", BUNDLE, file.getAbsolutePath(), obj.getClass().getName()));
        } catch (ClassNotFoundException e) {
            IOException ioe = new IOException(I18NManager.formatMessage("rf.ex.cnf", BUNDLE, file.getAbsolutePath(), e.getMessage()));
            ioe.initCause(e);
            throw ioe;
        } finally {
            in.close();
        }
    }

}
