/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * Policies.java
 *
 * Created on June 23, 2006, 9:39 AM
 *
 */

package com.sun.grid.grm.resource.policy;

import com.sun.grid.grm.config.resourceprovider.PolicyManagerConfig;
import com.sun.grid.grm.config.resourceprovider.PriorityPolicyManagerConfig;

/**
 * This class provides factory methods for creating PolicyManager instances.
 *
 */
public final class Policies {

    /**
     * Creates a new instance of Policies.
     */
    private Policies() {
    }

    
    /**
     * Create a policy manager
     * @param config configuration of the policy manager
     * @return the policy manager
     */
    public static PolicyManager createManager(PolicyManagerConfig config) {
        
        if(config instanceof PriorityPolicyManagerConfig) {
            return new PriorityPolicyManager((PriorityPolicyManagerConfig)config);
        } else {
            throw new IllegalStateException("unknown policy type " + config.getClass().getName());
        }
    }
}
