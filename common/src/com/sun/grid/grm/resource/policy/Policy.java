/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * Policy.java
 *
 * Created on May 1, 2006, 11:17 AM
 *
 */

package com.sun.grid.grm.resource.policy;

import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.service.Need;
import java.io.Serializable;

/**
 * <p>The Policy class represents a policy mechanism.  Using the ResourceView
 * and the details of a resource request, the Policy generates a multiplicative
 * modifier for the urgency of the request.</p>
 *
 * <p>Subclasses of Policy should override the toString() method to produce a
 * String descriptive of the policy.</p>
 * <p>Subclasses of Policy must override the equals() and hashCode() methods to
 * allows objects to be compared.</p>
 *
 */
public abstract class Policy implements Serializable {
    private final String name;
    protected transient ResourceView view = null;

    /**
     * Creates a new Policy object.
     * @param name the name of the policy
     */
    protected Policy(String name) {
        this.name = name;
    }

    /**
     * Tells this Policy object to prepare for use.  The init() method may be
     * called more than once in the lifetime of a Policy.
     * @param view the ResourceView to provide information about the system
     */
    public void init(ResourceView view) {
        this.view = view;
    }

    /**
     * Returns this policy's name.
     * @return getSubValue
     */
    public String getName() {
        return name;
    }

    /**
     * This method calculates the multiplicative modifier for the urgency of a
     * resource request or resource usage.
     * @param need the unmodified need
     * @param service the service container to which the resource in question
     * is assigned
     * @return the multiplicative modifier
     */
    public abstract float applyPolicy(Need need, String service);
}
