/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.policy;

import com.sun.grid.grm.config.resourceprovider.PriorityPolicyConfig;
import com.sun.grid.grm.service.Need;

import java.util.ResourceBundle;


/**
 * The Priority class represents a relative ranking of services, on a
 * scale from 0 to 99.
 *
 */
public class PriorityPolicy extends Policy {
    private static final long serialVersionUID = -2007111401L;
    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";

    /* The name of the service to which the priority should be
       applied. */
    private final String service;

    /* The priority to be applied to the service. */
    private final int priority;

    /**
     * Creates a new instance of Priority
     * should be applied
     * @param config configuration with policy attributes
     */
    public PriorityPolicy(PriorityPolicyConfig config) {
        this(config.getName(), config.getService(), config.getValue());
    }

    /**
     * Creates a new instance of Priority. 
     * 
     * @param name the policy's name
     * @param service name of the service for the policy
     * @param priority priority of the service
     */
    public PriorityPolicy(String name, String service, int priority) {
        super(name);

        if ((priority < 0) || (priority > 99)) {
            throw new IllegalArgumentException(ResourceBundle.getBundle(BUNDLE)
                                                             .getString("policy.exception.bad_priority"));
        } else if (service == null) {
            throw new IllegalArgumentException(ResourceBundle.getBundle(BUNDLE)
                                                             .getString("policy.exception.bad_service"));
        }
                
        this.priority = priority;
        this.service = service;
    }

    /**
     * This method calculates the multiplicative modifier for the urgency of a
     * resource request or resource usage.
     * @return the multiplicative modifier
     * @param need the expressed need
     */
    public float applyPolicy(Need need, String service) {
        if (this.service.equals(service)) {
            return applyPolicy(need, priority);
        }
        return 1.0f;
    }
    
    /**
     * Calculates the multiplicative modifier for the urgency
     * 
     * @param need the expressed need
     * @param priority the priority
     * @return the multiplicative modifier
     */
    public static float applyPolicy(Need need, int priority) {
        if(need != null && need.getUrgency() != null) {
            return need.getUrgency().getLevel() * (priority) / 100.0f;
        } else {
            return 0.0f;
        }
    }

    /**
     * Tests whether this Policy represents the same policy as another Policy.
     * @param obj   the reference object with which to compare.
     * @return <code>true</code> if this Policy is the same as the obj
     *          argument; <code>false</code> otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof PriorityPolicy &&
        (priority == ((PriorityPolicy) obj).priority) &&
        service.equals(((PriorityPolicy) obj).service);
    }

    /**
     * Returns a hash code value for the Policy. This method is
     * supported for the benefit of hashtables such as those provided by
     * <code>java.util.Hashtable</code>.
     * @return a hash code value for this Policy.
     * @see #equals(java.lang.Object)
     * @see java.util.Hashtable
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = (29 * hash) +
            ((this.service != null) ? this.service.hashCode() : 0);
        hash = (29 * hash) + this.priority;

        return hash;
    }

    /**
     * Returns a string representation of the Policy.
     * @return a string representation of the Policy.
     */
    @Override
    public String toString() {
        return "priority: " + service + "=" + priority;
    }

    /**
     * get the name of the service
     * @return the name of the service
     */
    public String getService() {
        return service;
    }

    /**
     * get the priority of the service
     * @return the priority of the service
     */
    public int getPriority() {
        return priority;
    }
}
