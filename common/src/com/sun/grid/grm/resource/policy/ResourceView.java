/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * ResourceView.java
 *
 * Created on May 1, 2006, 1:13 PM
 *
 */

package com.sun.grid.grm.resource.policy;

import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.resource.Resource;
import java.util.List;
import java.util.Map;

/**
 * The ResourceView interface defines the access methods for information made
 * available to the PolicyManager and configured Policy objects by the resource
 * provider.
 *
 */
public interface ResourceView {
    /**
     * Get the number of resources assigned to the given service container which
     * match the given pattern.
     * @param container the service container ComponentInfo
     * @param pattern the resource properties to match
     * @return the number of resources assigned to the given service container
     * which match the given pattern
     */
    int getResourceCount(ComponentInfo container,
                         Map<String,Object> pattern);
    /**
     * Get the number of resources in the given state which match the given
     * pattern.
     * @param state the state
     * @param pattern the resource properties to match
     * @return the number of resources in the given state which match the given
     * pattern
     */
    int getResourceCount(Resource.State state, Map<String,Object> pattern);
    /**
     * Get a list of the configured service container ids.
     * @return a list of the service container ComponentInfos
     */
    List<String> getServiceIds();
}
