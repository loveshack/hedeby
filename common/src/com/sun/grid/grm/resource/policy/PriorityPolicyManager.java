/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * SimplePolicyManager.java
 *
 * Created on May 1, 2006, 1:21 PM
 *
 */

package com.sun.grid.grm.resource.policy;

import com.sun.grid.grm.config.resourceprovider.PriorityPolicyConfig;
import com.sun.grid.grm.config.resourceprovider.PriorityPolicyManagerConfig;
import com.sun.grid.grm.service.Need;
import java.util.HashMap;
import java.util.Map;

/**
 * The PriorityPolicyManager class is a simple implementation of the
 * PolicyManager interface.  It maintians a list of Policy objects, and when
 * asked for a policy ruling, it forwards the request to each Policy object
 * sequentially.  The product of the results is then returned.
 *
 */
public class PriorityPolicyManager implements PolicyManager {

    /* The list of configured Policy objects. */
    private final Map<String, PriorityPolicy> policies;
    
    private final int defaultPriority;

    /** Creates a new instance of PriorityPolicyManager
     * @param config the configuration of the PriorityPolicyManager
     */
    public PriorityPolicyManager(PriorityPolicyManagerConfig config) {
        defaultPriority = config.getDefaultPriority();
        policies = new HashMap<String, PriorityPolicy>(config.getPriority().size());
        for(PriorityPolicyConfig policyConfig: config.getPriority()) {
            policies.put(policyConfig.getService(), new PriorityPolicy(policyConfig));
        }
    }

    /**
     * Using the contained policies, calculate a multiplicative modifier for an
     * urgency or usage value.
     *
     * @param need the expressed resource need
     * @param service the service to which the resource in question
     * is assigned
     * @return a multiplicative modifier
     */
    public float applyPolicies(Need need, String service) {
        PriorityPolicy policy = policies.get(service);
        if(policy == null) {
            // There is no priority for this specific service defined
            // => use the default priority
            return PriorityPolicy.applyPolicy(need, defaultPriority);
        }
        return policy.applyPolicy(need, service);
    }

}
