/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.event;

import com.sun.grid.grm.management.ManagedEventListener;
import java.util.EventListener;

/**
 * The ResourceEventListener interface defines the notifications which can be
 * sent by the OS provisioner using a ResourceEvent.
 *
 * @see ResourceEvent
 */
@ManagedEventListener
public interface ResourceEventListener extends EventListener {
    /**
     * When the OS Dispatcher receives information about new resources
     * it will send an event containing the new resource.
     *
     * @param event add event containing the resource.
     * @see ResourceEvent#getResource()
     */
    void resourceAdded(ResourceEvent event);

    /**
     * Whenever the OS Provisioner gets information about resource changes for
     * resources which where previously reported via the resourceAdded() method,
     * it will send an event via this method.
     * @param event modify event containig the modified resource.
     * @see ResourceEvent#getResource()
     */
    void resourceModified(ResourceEvent event);

    /**
     * Resources removed from the OS Distributor and underlaying components
     * will be reported by this method.
     *
     * @param event remove event containing the resource id of the removed
     * resource.
     * @see ResourceEvent#getResourceId()
     */
    void resourceRemoved(ResourceEvent event);
}
