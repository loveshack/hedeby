/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * ResourceEvent.java
 *
 * Created on April 12, 2006, 8:07 PM
 *
 */

package com.sun.grid.grm.resource.event;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.rmi.RemoteEvent;

/**
 * The ResourceEvent is used by the OS provisioner to notify the resource
 * provider of resource-related events, such as when a host has been
 * reprovisioned.
 *
 * @see ResourceEventListener
 */
public class ResourceEvent extends RemoteEvent {
    
    private final static long serialVersionUID = -2008031701L;
    private Resource resource = null;
    private ResourceId resourceId = null;

    /**
     * Creates a new instance of ResourceEvent
     *
     * @param osProvisionerName name of the os provisioner
     */
    public ResourceEvent(String osProvisionerName) {
        super(osProvisionerName);
    }

    /**
     * Creates a new instance of ResourceEvent
     *
     * @param osProvisionerName name of the os provisioner
     * @param resource the resource which changed
     */
    public ResourceEvent(String osProvisionerName, Resource resource) {
        this(osProvisionerName);

        this.resource = resource;
    }
    
    /**
     * Get the name of the OsProvisioner which has generated the event.
     * @return the name of the OsProvisioner
     */
    public String getOsProvisionerName() {
        return (String)getSource();
    }

    /**
     * Creates a new instance of ResourceEvent
     *
     * @param osProvisionerName the name of the OsProvisioner which generated the event
     * @param resourceId the id of the resource which changed
     */
    public ResourceEvent(String osProvisionerName, ResourceId resourceId) {
        this(osProvisionerName);
        this.resourceId = resourceId;
    }

    /**
     * Returns the resource which has been added or modified.
     *
     * @return the new or modified resource.
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * Returns the resource id for the removed resource
     *
     * @return the resource id of a resource.
     */
    public ResourceId getResourceId() {
        return resourceId;
    }
}
