/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource;

import java.io.Serializable;

/**
 *
 * Instances of this class describe changes on a resource
 * 
 * @param <T> the type of the value of the change
 */
public interface ResourceChanged<T> extends Serializable {
    
    /**
     * Gets the name of the property or attribute that has been changed
     * @return  the name of the property or attribute that has been changed
     */
    String getName();
    
    /**
     * Gets the new value of the changed property or attribute
     * @return the new value
     */
    T getValue();

    /**
     * Has the operation that produced this instance changed the resource?
     *
     * @return <tt>true</tt> if the operation has changed the resource
     */
    boolean hasChangedResource();

    /**
     * Undo this change.
     * 
     * @param resource the resource
     */
    void undo(Resource resource);


    /**
     * Reconstruct the operation that has produced this change
     * @return the operation
     */
    ResourceChangeOperation createOperation();
    
}