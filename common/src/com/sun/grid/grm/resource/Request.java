/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;


import com.sun.grid.grm.resource.impl.NeedProcessor;
import com.sun.grid.grm.service.Need;
import java.util.List;


/**
 * Request encapsulates the information about the resource request from
 * the service as well as it encapsulates the action that is needed
 * to fulfill the request.
 *
 */
public interface Request {
    /**
     * Executes the request.
     * @return true if the request was fulfilled
     */
    boolean execute();

    /**
     * Gets a modifiable list of request need processors
     * @return list of needs
     */
    List<NeedProcessor> getNeedProcessors();
    
    /**
     * Get the current needs of the request. 
     * 
     * <p>The current need is equal to the original need, but the
     *    quantity is decremented by the number of the already assigned resources</p>
     * @return
     */
    List<Need> getCurrentNeeds();

    /**
     * Gets a requesting service name.
     * @return service name
     */
    String getServiceName();

    /**
     * Gets a name of the slo that triggered the request.
     * @return slo name
     */
    String getSLOName();
    
    /**
     * Get the state of the request
     * @return the state of the request
     */
    public RequestState getState();
}
