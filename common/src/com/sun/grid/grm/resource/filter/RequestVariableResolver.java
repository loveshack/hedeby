/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.filter;

import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.RequestState;
import com.sun.grid.grm.util.filter.FilterVariable;
import com.sun.grid.grm.util.filter.VariableResolver;

/**
 *  This class resolves the variables of a resource request for a filter.
 *  Known filter variables are
 * 
 *  <table>
 *     <tr><th>Variable</th><th>Description</th></tr>
 *     <tr><td>service </td><td>Name of the service which produced the request</td></tr>
 *     <tr><td>slo     </td><td>Name of the  slo which produced the request</td></tr>
 *     <tr><td>state   </td><td>State of the request (queued or pending)</td></tr>
 *  </table>
 *  
 */
public class RequestVariableResolver implements VariableResolver<Request> {

    private Request request;
    private RequestState state;
    
    /**
     * The service filter variable
     */
    public final static FilterVariable<Request> SERVICE_VAR = new FilterVariable<Request>("service");
    
    /**
     * the slo filter variable
     */
    public final static FilterVariable<Request> SLO_VAR = new FilterVariable<Request>("slo");
    
    /**
     * the state filter variable
     */
    public final static FilterVariable<Request> STATE_VAR = new FilterVariable<Request>("state");
    
    /**
     * Get the value of a filter variable
     * @param name name of the filter variable
     * @return the value of the filter variable
     */
    public Object getValue(String name) {
        if(SERVICE_VAR.getName().equalsIgnoreCase(name)) {
            return request.getServiceName();
        } else if (SLO_VAR.getName().equalsIgnoreCase(name)) {
            return request.getSLOName();
        } else if (STATE_VAR.getName().equalsIgnoreCase(name)) {
            return state.toString();
        } else {
            return null;
        }
    }
    
    /**
     * Set the resource request
     * @param request the request
     * @param state the state of the request
     */
    public void setRequest(Request request, RequestState state) {
        if(request == null) {
            throw new NullPointerException();
        }
        if(state == null) {
            throw new NullPointerException();
        }
        this.request = request;
        this.state = state;
    }

}
