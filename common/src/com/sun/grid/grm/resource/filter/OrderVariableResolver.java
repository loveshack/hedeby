/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.filter;

import com.sun.grid.grm.resource.order.OrderType;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.order.Order;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.FilterVariable;
import com.sun.grid.grm.util.filter.VariableResolver;

/**
 *  This class resolves the variables of a resource order for a filter.
 *  Known filter variables are
 * 
 *  <table>
 *     <tr><th>Variable</th><th>Description</th></tr>
 *     <tr><td>resourceid   </td><td>ResourceId of the resource</td></tr>
 *     <tr><td>source       </td><td>Name of the source service - owner of the resource</td></tr>
 *     <tr><td>target       </td><td>Name of the target service - where resource should be moved</td></tr>
 *     <tr><td>type         </td><td>Type of the order</td></tr>
 *     <tr><td>maintenance  </td><td>Flag specifying whether the order is issued by administrator</td></tr>
 *     <tr><td>static       </td><td>Flag specifying whether the resource has to be made static</td></tr>
 *  </table>
 *  
 */
public class OrderVariableResolver implements VariableResolver<Order> {

    private Order order;
    /**
     * The instance filter variable
     */
    public final static FilterVariable<Order> INSTANCE_VAR = new FilterVariable<Order>("instance");
    /**
     * The resource id filter variable
     */
    public final static FilterVariable<Order> RESOURCE_ID_VAR = new FilterVariable<Order>("resourceid");
    /**
     * The source service filter variable
     */
    public final static FilterVariable<Order> SOURCE_VAR = new FilterVariable<Order>("source");
    /**
     * the target service filter variable
     */
    public final static FilterVariable<Order> TARGET_VAR = new FilterVariable<Order>("target");
    /**
     * the order type filter variable
     */
    public final static FilterVariable<Order> TYPE_VAR = new FilterVariable<Order>("ordertype");
    /**
     * the maintenance flag filter variable
     */
    public final static FilterVariable<Order> MAINTENANCE_VAR = new FilterVariable<Order>("maintenance");

    /**
     * Get the value of a filter variable
     * @param name name of the filter variable
     * @return the value of the filter variable
     */
    public Object getValue(String name) {
        if (SOURCE_VAR.getName().equalsIgnoreCase(name)) {
            return order.getSource();
        } else if (TARGET_VAR.getName().equalsIgnoreCase(name)) {
            return order.getTarget();
        } else if (RESOURCE_ID_VAR.getName().equalsIgnoreCase(name)) {
            return order.getResourceId();
        } else if (TYPE_VAR.getName().equalsIgnoreCase(name)) {
            return order.getType();
        } else if (MAINTENANCE_VAR.getName().equalsIgnoreCase(name)) {
            return order.isMaintenance();
        } else if (INSTANCE_VAR.getName().equalsIgnoreCase(name)) {
            return order;
        } else {
            return null;
        }
    }

    /**
     * Set the order 
     * 
     * @param order the order
     */
    public void setOrder(Order order) {
        if (order == null) {
            throw new IllegalArgumentException("Order must not be null");
        }
        this.order = order;
    }

    
    public static Filter<Order> newResourceIdAndSourceFilter(ResourceId id, String source) {
        AndFilter<Order> ret = new AndFilter<Order>(2);
        ret.add(newResourceIdFilter(id));
        ret.add(newSourceFilter(source));
        return ret;
    }
    
    public static Filter<Order> newResourceIdAndTargetFilter(ResourceId id, String source) {
        AndFilter<Order> ret = new AndFilter<Order>(2);
        ret.add(newResourceIdFilter(id));
        ret.add(newTargetFilter(source));
        return ret;
    }
    
    /**
     * Create a new filter for the name of a source service
     *
     * @param source the name of the source service
     * @return the source filter
     */
    public static Filter<Order> newSourceFilter(String source) {
        return new CompareFilter<Order>(
                OrderVariableResolver.SOURCE_VAR,
                new FilterConstant<Order>(source),
                CompareFilter.Operator.EQ);
    }

    /**
     * Create a new filter for the name of a target service
     *
     * @param target the name of the target service
     * @return the target filter
     */
    public static Filter<Order> newTargetFilter(String target) {
        return new CompareFilter<Order>(
                OrderVariableResolver.TARGET_VAR,
                new FilterConstant<Order>(target),
                CompareFilter.Operator.EQ);
    }

    /**
     * Create a new filter for the order - the order instance is considered as a filter,
     * so only the same instance will of order will match.
     *
     * @param order the order whose instance serves as a filter
     * @return the order id filter
     */
    public static Filter<Order> newOrderFilter(Order order) {
        return new CompareFilter<Order>(
                OrderVariableResolver.INSTANCE_VAR,
                new FilterConstant<Order>(order),
                CompareFilter.Operator.ID);
    }

    /**
     * Create a new filter for the resource id contained in order
     *
     * @param id the resource id
     * @return the resource id filter
     */
    public static Filter<Order> newResourceIdFilter(ResourceId id) {
        return new CompareFilter<Order>(
                OrderVariableResolver.RESOURCE_ID_VAR,
                new FilterConstant<Order>(id),
                CompareFilter.Operator.EQ);
    }

    /**
     * Create a new filter for the order type
     *
     * @param type the type of order
     * @return the type filter
     */
    public static Filter<Order> newOrderTypeFilter(OrderType type) {
        return new CompareFilter<Order>(
                OrderVariableResolver.TYPE_VAR,
                new FilterConstant<Order>(type),
                CompareFilter.Operator.EQ);
    }

    /**
     * Create a new filter for the maintenance orders
     *
     * @param isMaintenance flag specifying whether the order is maintenance
     * @return the maintenance filter
     */
    public static Filter<Order> newMaintenanceFilter(boolean isMaintenance) {
        return new CompareFilter<Order>(
                OrderVariableResolver.MAINTENANCE_VAR,
                new FilterConstant<Order>(isMaintenance),
                CompareFilter.Operator.EQ);
    }

}
