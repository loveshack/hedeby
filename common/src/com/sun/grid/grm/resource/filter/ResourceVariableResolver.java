/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.filter;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.FilterVariable;
import com.sun.grid.grm.util.filter.VariableResolver;
import java.util.regex.Pattern;

/**
 * This adapter allows filtering of resource by it's resource properties.
 */
public class ResourceVariableResolver implements VariableResolver<Resource> {

    private Resource resource;

    /**
     * name of the filter variable to get the resource name
     */
    public final static String NAME_VAR = "name";

    /**
     * name of the filter variable to get the resource bound state
     */
    public final static String BOUND_VAR = "bound";

    /**
     * name of the filter variable to get the resource type
     */
    public final static String TYPE_VAR = "type";
    
    /**
     * name of the filter variable to get the resource usage
     */
    public final static String USAGE_VAR = "usage";
    
    /**
     * name of the filter variable to get the resource annotation
     */
    public final static String ANNOTATION_VAR = "annotation";
    
    /**
     * name of the filter variable to get the resource ambiguous state
     */
    public final static String AMBIGUOUS_VAR = "ambiguous";
    
    /**
     * name of the filter variable to get the state of a resource
     */
    public final static String STATE_VAR = "state";
    
    /**
     * name of the filter variable to get the id of a resource
     */
    public final static String ID_VAR = "id";
    
    /**
     * Get the value of a resource property by it's name
     * @param name  name of the resource property
     * @return the value of the resource property or <code>null</code>
     *         if the resource property is not defined for the resource
     */
    public Object getValue(String name) {
        if(resource != null && name != null) {
            if(ID_VAR.equals(name)) {
                return resource.getId().toString();
            } else if (NAME_VAR.equals(name)) {
                return resource.getName();
            } else if (BOUND_VAR.equals(name)) {
                return resource.isBound();
            } else if (STATE_VAR.equals(name)) {
                return resource.getState().toString();
            } else if (TYPE_VAR.equals(name)) {
                return resource.getType().getName();
            } else if (USAGE_VAR.equals(name)) {
                // We need a special handling for usage, because
                // in the filter we want the int value of the usage
                return resource.getUsage().getLevel();
            } else if (ANNOTATION_VAR.equals(name)) {
                return resource.getAnnotation();
            } else if (AMBIGUOUS_VAR.equals(name)) {
                return resource.isAmbiguous();
            } else {
                return resource.getProperties().get(name);
            }
        } else {
            return null;
        }
    }
    
    /**
     * Set the resource which should be adapted.
     * @param aResource  the resource
     */
    public void setResource(Resource aResource) {
        resource = aResource;
    }

    private final static Pattern RES_ID_PATTERN = Pattern.compile("res#[1-9][0-9]*");

    /**
     * Is the string a resource id
     * @param idOrName the string
     * @return <tt>true</tt> if the string is a resource id
     */
    public static boolean isResourceId(String idOrName) {
        return RES_ID_PATTERN.matcher(idOrName).matches();
    }
    
    /**
     * Create a resource filter for an resource id or the resource name
     * @param idOrName the resource id or the resource name
     * @return the resource filter
     */
    public static Filter<Resource> createIdOrNameFilter(String idOrName) {

        String varName;
        if (isResourceId(idOrName)) {
            varName = ID_VAR;
        } else {
            varName = NAME_VAR;
        }
        return new CompareFilter<Resource>(new FilterVariable<Resource>(varName), new FilterConstant<Resource>(idOrName),
                                           CompareFilter.Operator.EQ);
    }
    
}
