/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource;

import com.sun.grid.grm.util.filter.Filter;
import java.util.List;
import java.util.Set;

/**
 * The interface specifying the operations for storage of resources.
 * 
 * Implementing classes can decide whether they use in-memory only storage
 * of resources or a persistent storage.
 * 
 */
public interface ResourceStore {

    /**
     * Load all resource from the persistent storage.
     * 
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the resource could
     *           not be loaded
     */
    public void loadResources() throws ResourceStoreException;
    
    /**
     * Removes all resources
     * @throws com.sun.grid.grm.resource.ResourceStoreException when the call fails
     */
    public void clear() throws ResourceStoreException;

    /**
     * Adds a resource. ResourceId is used as a key.
     * @param resource a resource to be added
     * @return an old resource with the same Id as added object or null if such
     * resource was not stored
     * @throws com.sun.grid.grm.resource.ResourceStoreException when the call fails
     */
    public Resource add(Resource resource) throws ResourceStoreException;

    /**
     * Removes a resource.
     * 
     * @param resourceId Id of a resource to be removed
     * @return the removed object
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource 
     * with such Id was not found
     * @throws com.sun.grid.grm.resource.ResourceStoreException when the call fails
     */
    public Resource remove(ResourceId resourceId) throws UnknownResourceException, ResourceStoreException;

    /**
     * Gets a set or ids of all stored resources.
     * @return the set of ResourceIds
     * @throws com.sun.grid.grm.resource.ResourceStoreException when the call fails
     */
    public Set<ResourceId> getResourceIds() throws ResourceStoreException;

    /**
     * Gets a resource with the specified Id.
     * @param resourceId Id of a resource to be found
     * @return the resource with specified Id
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource
     * with such Id was not found
     * @throws com.sun.grid.grm.resource.ResourceStoreException when the call fails
     */
    public Resource getResource(ResourceId resourceId) throws UnknownResourceException, ResourceStoreException;

    /**
     * Gets all stored resources.
     * @return list of resources.
     * @throws com.sun.grid.grm.resource.ResourceStoreException when the call fails
     */
    public List<Resource> getResources() throws ResourceStoreException;

    /**
     * get all stored resource matching a filter
     * @param filter the filter
     * @return list of resources.
     * @throws com.sun.grid.grm.resource.ResourceStoreException when the call fails
     */
    public List<Resource> getResources(Filter<Resource> filter) throws ResourceStoreException;
}

