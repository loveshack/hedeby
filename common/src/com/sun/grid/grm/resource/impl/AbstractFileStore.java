/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.ObjectStoreException;
import java.io.File;
import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This class provides the functionality to store objects into files
 * 
 * @param T type of the stored objects
 * @param E The exeception type which is thrown if an error occurs
 */
public abstract class AbstractFileStore<T,E extends ObjectStoreException> {

    private final File spoolDir;
    
    /**
     * Extension of a filename of spooled Object.
     */
    private static final String EXT = ".srf";
    /**
     * Extension of an intermediate filename of spooled Object.
     */
    private static final String NEW_FILE_EXT = ".0";
    /**
     * Extension of an intermediate filename of spooled Object.
     */
    private static final String BACKUP_FILE_EXT = ".1";
    /**
     * Filename FILE_FILTER for "xxx.srf" Object fiels.
     */
    private static final FilenameFilter FILE_FILTER;
    /**
     * Filename filter for "xxx.0" Object fiels.
     */
    private static final FilenameFilter NEW_FILE_FILTER;
    /**
     * Filename filter for "xxx.1" Object fiels.
     */
    private static final FilenameFilter BACKUP_FILE_FILTER;

    static {
        FILE_FILTER = new FilenameFilter() {

            public boolean accept(File dir, String name) {
                return name.endsWith(EXT);
            }
        };
        NEW_FILE_FILTER = new FilenameFilter() {

            public boolean accept(File dir, String name) {
                return name.endsWith(NEW_FILE_EXT);
            }
        };
        BACKUP_FILE_FILTER = new FilenameFilter() {

            public boolean accept(File dir, String name) {
                return name.endsWith(BACKUP_FILE_EXT);
            }
        };
    }

    /**
     * This map will contain all the mapping between objects and the filename
     * The string representation of a object is not always enough to map it
     * into a filename.
     */
    private final Map<T,File> fileMap = new HashMap<T,File>();
    
    /**
     * Create a new File Spool
     * @param spoolDir the spool directory
     */
    protected AbstractFileStore(File spoolDir) {
        this.spoolDir = spoolDir;
    }

    /**
     * The load method uses this method to load the object from a obj
     * @param file the file
     * @return the object or <code>null</code> if the object should not be used
     * @throws E the exeception
     */
    protected abstract T readObjectFromFile(File file) throws E;

    /**
     * Write an object into a file
     * @param obj   the object
     * @param file  the file
     * @throws E if the object could not be stored 
     */
    protected abstract void writeObjectToFile(T obj, File file) throws E;

    /**
     * Get the filename prefix for an object
     * @param obj  the object
     * @return the filename prefix
     */
    protected abstract String getFilenamePrefixForObject(T obj);

    /**
     * This method reads the entire set of objects from the spool
     * directory.
     * @return a list of all loaded objects
     * @throws E on any error
     */
    public Set<T> load() throws E {
        Set<T> ret = new HashSet<T>();
        synchronized(fileMap) {
            fileMap.clear();
            File[] files = getSpoolDir().listFiles(FILE_FILTER);
            if (files != null) {
                for (File file: files) {
                    T obj = readObjectFromFile(file);
                    if (obj != null) {
                        fileMap.put(obj, file);
                        ret.add(obj);
                    }
                }
            }
        }
        return ret;
    }
    
    /**
     * This method repairs any damage that may have been done by an unexpected
     * JVM termination.
     */
    public void cleanup() {
        /* First deal with objects for which we have a file. */
        cleanupExistingFiles();

        /* Next deal with objects with .0 but no file. */
        cleanupNewFiles();

        /* Next deal with objects with .1 but no file or .0. */
        cleanupBackupFiles();
    }
    

    /**
     * Get the filename of an object
     * @param obj the object
     * @return the filename
     */
    public File getFileForObject(T obj) {
        synchronized(fileMap) {
            File file = fileMap.get(obj);
            if (file == null) {
                String prefix = getFilenamePrefixForObject(obj);
                /* First store the object to a new file. */
                file = new File( getSpoolDir(),createFileName(prefix));
            }
            return file;
        }
    }
    
    /**
     * Write an object
     * @param obj the object 
     * @throws E on any error
     */
    public void write(T obj) throws E {

        synchronized(fileMap) {
            
            File file = getFileForObject(obj);
            File newFile = createNewFile(file);

            writeObjectToFile(obj, newFile);

            /* Next, make a backup */
            File backupFile = null;

            if (file.exists()) {
                backupFile = createBackupFile(file);
                file.renameTo(backupFile);
            }

            /* Next, rename the new file to the old file's name. */
            newFile.renameTo(file);

            /* Finally, remove the backup file. */
            if (backupFile != null) {
                backupFile.delete();
            }
        }
    }
    
    /**
     * Removes the spooled object file.
     * @param obj the object
     * @return true of the file for the object does not longer exist
     */
    public boolean delete(T obj) {
        synchronized(fileMap) {
            File file = getFileForObject(obj);
            if(file.exists()) {
                return file.delete();
            } else {
                return true;
            }
        }
    }
    

    /**
     * Creates a file name with EXT extension for string respresentation of an object
     */
    private static String createFileName(String id) {
        StringBuilder sb = new StringBuilder(id.length() + EXT.length());
        sb.append(id);
        sb.append(EXT);
        return sb.toString();
    }

    /**
     * Get the name of the new file (with NEW_FILE_EXT extension)
     */
    private static File createNewFile(File file) {
        StringBuilder sb = new StringBuilder(file.getName().length() + NEW_FILE_EXT.length());
        sb.append(file.getName());
        sb.append(NEW_FILE_EXT);
        return new File(file.getParent(), sb.toString());
    }

    /**
     * Get the name of the bckup file (with BACKUP_FILE_EXT extension)
     */
    private static File createBackupFile(File file) {
        StringBuilder sb = new StringBuilder(file.getName().length() + BACKUP_FILE_EXT.length());
        sb.append(file.getName());
        sb.append(BACKUP_FILE_EXT);
        return new File(file.getParent(), sb.toString());
    }


    /**
     * This method removes .0 and .1 files for object files which exist.
     */
    private void cleanupExistingFiles() {

        /* If either a .0 or .1 file exists, delete it, because we have
         * no way of knowing whether it's current or complete. */
        File[] files = getSpoolDir().listFiles(NEW_FILE_FILTER);
        if (files != null) {
            for (File file : files) {
                file.delete();
            }
        }
        files = getSpoolDir().listFiles(BACKUP_FILE_FILTER);
        if (files != null) {
            for (File file : files) {
                file.delete();
            }
        }
    }

    /**
     * This method cleans up .0 files for object files which don't exist.
     */
    private void cleanupNewFiles() {
        File[] files = getSpoolDir().listFiles(NEW_FILE_FILTER);

        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                File file0 = files[i];
                String name = file0.getName();
                File file = new File( getSpoolDir(),name.substring(0, name.length() - NEW_FILE_EXT.length()));
                File file1 = new File( getSpoolDir(),name.substring(0, name.length() - BACKUP_FILE_EXT.length()));
                if (file1.exists()) {
                    /* If a .1 exists, use it and delete the .0. */
                    file1.renameTo(file);
                    file0.delete();
                } else {
                    /* Otherwise, restore the .0. */
                    file0.renameTo(file);
                }
            }
        }
    }

    /**
     * This method cleans up .1 files for object files which don't exist.
     */
    private void cleanupBackupFiles() {
        File[] files = getSpoolDir().listFiles(BACKUP_FILE_FILTER);

        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                /* Delete it because we don't know if it's complete. */
                files[i].delete();
            }
        }
    }

    /**
     * Get the spool directory
     * @return the spool directory
     */
    public File getSpoolDir() {
        return spoolDir;
    }
}
