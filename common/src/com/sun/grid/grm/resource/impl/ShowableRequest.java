/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.policy.PolicyManager;
import com.sun.grid.grm.util.I18NManager;

import java.io.Serializable;


/**
 * Implementation of the request for the monitoring purposes.
 *
 */
public final class ShowableRequest extends AbstractRequest implements Serializable {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.resource.resource";
    private final static long serialVersionUID = -2007112001L;

    /**
     * Creates a new ShowableRequest object.
     * @param request the request implementation used by request queue
     * 
     */
    public ShowableRequest(Request request) {
        super(request);
    }
    
    /**
     * Creates a new ShowableRequest object.
     * @param request the request implementation used by request queue
     * @param policyManager policy manager responsible for normalizing of needs
     * 
     */
    public ShowableRequest(Request request, PolicyManager policyManager) {
        super(request, policyManager);
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret = false;

        if (obj instanceof ShowableRequest) {
            ret = super.equals(obj);
        }

        return ret;
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Operation is not supported. If used, an UnsupportedOperationException is 
     * thrown.
     * @return not applicable
     */
    public boolean execute() {
        throw new UnsupportedOperationException(I18NManager.formatMessage(
                "sr.notexecutable", BUNDLE_NAME));
    }
}
