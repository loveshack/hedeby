/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.ResourceId;

/**
 *  A AnyResourceIdImpl can be used to find resources by it's id
 *  The type of the resource id is not considered in equals and hashcode method
 * @deprecated 
 */
@Deprecated
public class AnyResourceIdImpl extends AbstractResourceId {
    
    private final static long serialVersionUID = -2008013001L;
    
    private final String id;
    
    /**
     * Create a new instance.
     * @param id the id of the resource
     * @throws java.lang.NullPointerException if id is null
     */
    public AnyResourceIdImpl(String id) {
        if(id == null) {
            throw new NullPointerException("id must not be null");
        }
        this.id = id;
    }
    
    /**
     * Get the id of the resource
     * @return the id of the resource
     */
    public String getId() {
        return id;
    }

    /**
     * Compare this resource id  with o
     * @param o  the other resource id
     * @return compare resoult
     */
    public int compareTo(ResourceId o) {
        return id.compareTo(o.getId());
    }

    /**
     * 
     * @param o  the other object
     * @return returns <code>true</code> if the id string is equals
     */
    @Override
    public boolean equals(Object o) {
        if(o instanceof ResourceId) {
            ResourceId that = (ResourceId)o;
            return id.equals(that.getId());
        } else {
            return false;
        }
    }   

    @Override
    public String toString() {
        return id;
    }

}
