/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.bootstrap.FactoryWithException;
import com.sun.grid.grm.resource.*;
import com.sun.grid.grm.service.Usage;

import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Default Implemenation of a Resource
 */
public final class ResourceImpl implements Resource, Serializable {

    private static final long serialVersionUID = -2008013002L;
    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    private final ResourceId id;
    private final Map<String, Object> properties;
    private final String typeName;
    private State state = State.UNASSIGNED;
    private Usage usage = Usage.MIN_VALUE;
    private boolean ambiguous = false;
    private String annotation;
    private transient ReadWriteLock lock = new ReentrantReadWriteLock();
    private transient ResourceType type;

    /**
     * Create a new instance of a Resource
     * @param type   the type of the resource
     * @param resIdFactory the resource id factory
     * @param props  the properties if the resource
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the properties are note valid
     * @throws ResourceIdException if the resource id could not be created
     */
    public ResourceImpl(ResourceType type, FactoryWithException<ResourceId,ResourceIdException> resIdFactory, Map<String, Object> props) throws InvalidResourcePropertiesException, ResourceIdException {
        this(type, resIdFactory, props, State.UNASSIGNED);
    }

    private ResourceImpl(ResourceType type, FactoryWithException<ResourceId,ResourceIdException> resIdFactory, Map<String, Object> props, State state) throws InvalidResourcePropertiesException, ResourceIdException {
        if (type == null) {
            throw new NullPointerException("type must not be null");
        }
        if (props == null) {
            throw new NullPointerException("props must not be null");
        }

        // Initialize the properties
        this.properties = new HashMap<String, Object>(props);

        // Set the default values for the properties
        type.fillResourcePropertiesWithDefaults(properties);
        
        type.validate(properties);
        this.type = type;
        this.typeName = type.getName();
        // now that the properties are validated we can create the resource id
        this.id = resIdFactory.newInstance();
        this.state = state;
    }
    
    //this private constructor should be used only while cloning the resource, validation of properties
    //and setting default values for them were omitted.
    private ResourceImpl(ResourceId id, ResourceType type, Map<String, Object> props, State state) {
        // Initialize the properties
        this.properties = new HashMap<String, Object>(props);
        this.type = type;
        this.typeName = type.getName();
        this.id = id;
        this.state = state;
    }

    /**
     * Clone this resource
     * @return the clone
     */
    @Override
    public Resource clone() {
        lock.readLock().lock();
        try {         
            ResourceImpl ret = new ResourceImpl(id, type, properties, state);
            ret.setAmbiguous(ambiguous);
            ret.setAnnotation(annotation);
            ret.setUsage(usage);
            return ret;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get a clone of the resource with a new resource id
     * @param id  the new resource id
     * @return the clone resource
     * @deprecated
     */
    @Deprecated
    public ResourceImpl cloneWithNewId(ResourceId id, ResourceType type) {
        lock.readLock().lock();
        try {
            ResourceImpl ret = new ResourceImpl(id, type, properties, state);
            ret.setAmbiguous(ambiguous);
            ret.setAnnotation(annotation);
            ret.setUsage(usage);
            return ret;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Apply the changes in <code>changed</code> to <code>this.properties</code>.
     * 
     * Must be called from code that holds the propertiesLock.
     * 
     * @param changed  changes to apply
     */
    private void applyPropertyChanges(Collection<ResourceChanged> changed) {
        for (ResourceChanged item : changed) {
            if (item instanceof ResourcePropertyDeleted) {
                properties.remove(item.getName());
            }
            if (item instanceof ResourcePropertyInserted) {
                properties.put(item.getName(), item.getValue());
            }
            if (item instanceof ResourcePropertyUpdated) {
                properties.put(item.getName(), item.getValue());
            }
        }
    }

    /**
     * This method is call from the java serialization after a host object
     * has been deserialized.
     * 
     * @param in  the input stream
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        try {
            in.defaultReadObject();
            lock = new ReentrantReadWriteLock();
            type = DefaultResourceFactory.getResourceType(typeName);
        } catch (UnknownResourceTypeException ex) {
            // Cannot happen, all resource has a type
            throw new IllegalStateException("resource has invalid type", ex);
        }
    }

    /**
     * Gets the resource's id.
     * @return the resource's id
     */
    public ResourceId getId() {
        return id;
    }
    
    /**
     * Get the name of the resource.
     *
     * @return the name of the resource
     */
    public String getName() {
        lock.readLock().lock();
        try {
            return type.getResourceName(this.properties);
        } catch (InvalidResourcePropertiesException ex) {
            // This should not happen, the properties should be always valid
            throw new IllegalStateException("The resource does not have a valid properties set, can not build name", ex);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get the bound state of the resource
     * @return the bound state of the resource
     */
    public boolean isBound() {
        lock.readLock().lock();
        try {
            return type.isResourceBound(properties);
        } finally {
            lock.readLock().unlock();
        }
    }



    /**
     * Gets the resource's properties.
     * @return the resource's properties (immutable)
     */
    public Map<String, Object> getProperties() {
        lock.readLock().lock();
        try {
            return Collections.<String, Object>unmodifiableMap(properties);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * get the value of a resource property
     * @param name the name of the resource property
     * @return the value
     */
    public Object getProperty(String name) {
        lock.readLock().lock();
        try {
            return properties.get(name);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Gets the resource's state.
     * @return the resource's state
     */
    public State getState() {
        lock.readLock().lock();
        try {
            return state;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Sets the resource's state.
     * @param state the resource's state
     */
    public void setState(State state) {
        lock.writeLock().lock();
        try {
            this.state = state;
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Sets the resource's annotation.  The annotation is used to provide
     * additional information about the resource's state.
     * @param annotation the resource's annotation
     */
    public void setAnnotation(String annotation) {
        lock.writeLock().lock();
        try {
            this.annotation = annotation;
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Gets the resource's annotation.
     * @return the resource's annotation
     */
    public String getAnnotation() {
        lock.readLock().lock();
        try {
            return this.annotation;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Sets a property's value in this resource.
     * @param name the property's name
     * @param value the property's value
     * @return the object describing the change
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if <code>value</code>
     *                   does not meet the containts of the property
     */
    public ResourceChanged setProperty(String name, Object value)
            throws InvalidResourcePropertiesException {

        if (name == null) {
            throw new IllegalArgumentException(I18NManager.formatMessage("resource.exception.property.name_null", BUNDLE));
        }

        ResourceChanged changed;
        ResourcePropertyType rpType = type.getPropertyTypes().get(name);
        if (rpType == null) {
            rpType = ResourcePropertyType.newDefault(name);
        }
        lock.writeLock().lock();
        try {
            changed = rpType.setValue(this.properties, value);
        } finally {
            lock.writeLock().unlock();
        }
        return changed;
    }

    /**
     * Removes a resource property.
     * @param name the name of the property to remove
     * @return the value of the removed property
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException the property is mandatory
     */
    public Object removeProperty(String name) throws InvalidResourcePropertiesException {
        ResourceChanged changed = setProperty(name, null);
        // If the property has been deleted a ResourcePropertyDeleted instance is returned.
        // The getValue method will contain the old value.
        // If the property has not been deleted (was already null) a ResourceNotChanged instance
        // is returned. The getValue method returns null
        return changed.getValue();
    }

    /**
     * This method modifies resource properties and attributes
     * @param operations that will be done on resource properties and attributes
     * @return list of changed property objects
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if <code>props</code>
     *                           contains an invalid values. In this case the resource is not
     *                           modified
     */
    public Collection<ResourceChanged> modify(Collection<ResourceChangeOperation> operations) throws InvalidResourcePropertiesException {
        ArrayList<ResourceChanged> ret = new ArrayList<ResourceChanged>(operations.size());
        lock.writeLock().lock();
        try {
            try {
                for(ResourceChangeOperation op: operations) {
                    ResourceChanged changed = modify(op);
                    if(changed.hasChangedResource()) {
                        ret.add(changed);
                    }
                }
                return ret;
            } catch(InvalidResourcePropertiesException ex) {
                for(int i = ret.size() - 1; i >= 0; i--) {
                    ret.get(i).undo(this);
                }
                throw ex;
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * This method modify resource properties and attributes
     * @param operation that will be done on resource properties and attributes
     * @return list of changed property objects
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if <code>props</code>
     *                           contains an invalid values. In this case the resource is not
     *                           modified
     */
    public ResourceChanged modify(ResourceChangeOperation operation) throws InvalidResourcePropertiesException {
       ResourceChanged ret;
        lock.writeLock().lock();
        try {
            ret = operation.execute(this);
        } finally {
            lock.writeLock().unlock();
        }
        return ret;
    }

    /**
     * Set the properties of the resource.
     * @param props the properties
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the properties are not valid
     */
    public void setProperties(Map<String, Object> props) throws InvalidResourcePropertiesException {
        type.validate(props);
        lock.writeLock().lock();
        try {
            properties.clear();
            properties.putAll(props);
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Compares this Resource to another object.
     * @param object another object
     * @return true if this Resource is the same as the other object
     */
    @Override
    public boolean equals(Object object) {
        if (object instanceof Resource) {
            return this.id.equals(((Resource) object).getId());
        } else if (object instanceof ResourceId) {
            return this.id.equals(object);
        } else {
            return false;
        }
    }

    /**
     * Returns a hash code for this object.
     * @return a hash code for this object
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    /**
     * Returns a String representation of this Resource.
     * @return a String representation of this Resource
     */
    @Override
    public String toString() {
        return String.format("%s(id=%s,type=%s)", getName(), id, getType().getName());
    }

    /**
     * This method writes the resource to the given file in a format that is
     * human readable.
     * @param file the target file
     * @throws java.io.IOException if any error while writing to file occured
     */
    public void writeToFile(File file) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
        try {
            lock.readLock().lock();
            try {
                out.writeObject(this);
            } finally {
                lock.readLock().unlock();
            }
        } finally {
            out.close();
        }
    }

    /**
     * Convenient way to set the static flag property.
     * @param isStatic true if resource has to be set static
     */
    public void setStatic(boolean isStatic) {
        lock.writeLock().lock();
        try {
            setProperty(AbstractResourceType.STATIC.getName(), isStatic);
        } catch (InvalidResourcePropertiesException ex) {
            // Can not happen
            throw new IllegalStateException("set static property failed", ex);
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Convenient way to set the ambiguous flag property.
     * @param isAmbiguous true if resource has to be set ambiguous
     */
    public void setAmbiguous(boolean isAmbiguous) {
        lock.writeLock().lock();
        try {
            this.ambiguous = isAmbiguous;
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Convenient way to set the usage property.
     * @param usage the usage to be set
     */
    public void setUsage(Usage usage) {
        lock.writeLock().lock();
        try {
            this.usage = usage;
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Convenient way to get the usage property.
     * @return the Usage object constructed using the usage level property or Usage.MIN_VALUE
     * if usage property is not set or invalid
     */
    public Usage getUsage() {
        lock.readLock().lock();
        try {
            return this.usage;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Convenient wat to find out if resource is static.
     * @return true if resource is static, false otherwise
     */
    public boolean isStatic() {
        lock.readLock().lock();
        try {
            return (Boolean) AbstractResourceType.STATIC.getValue(properties);
        } catch (InvalidResourcePropertiesException ex) {
            // Can not happen
            throw new IllegalStateException("get static property failed", ex);
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Convenient wat to find out if resource is ambiguous.
     * @return true if resource is ambiguous, false otherwise
     */
    public boolean isAmbiguous() {
        lock.readLock().lock();
        try {
            return this.ambiguous;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get the type of the resource
     * @return the type of the resource
     */
    public ResourceType getType() {
        return type;
    }
}
