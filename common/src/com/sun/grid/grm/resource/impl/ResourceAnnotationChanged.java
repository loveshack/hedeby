/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.util.I18NManager;
import java.io.Serializable;

/**
 *
 * Information that only the annotation of a resource has changed
 */
public class ResourceAnnotationChanged extends AbstractResourceChanged<String> implements Serializable {

    private final static String BUNDLE = "com.sun.grid.grm.resource.resource";

    /**
     * The serial version UID
     */
    private static final long serialVersionUID = 2009030201;

    private final String oldAnnotation;
    
    /**
     * Create instance of this class
     * @param newAnnotation the new value for the annotation
     * @param oldAnnotation the old value of the annotation
     */
    public ResourceAnnotationChanged(String oldAnnotation, String newAnnotation) {
        super(Resource.ANNOTATION, newAnnotation);
        this.oldAnnotation = oldAnnotation;
    }

    /**
     * get the old value of the annotation
     * @return the old value of the annotation
     */
    public String getOldAnnotation() {
        return oldAnnotation;
    }

    /**
     * Reconstruct the operation that has produced this change
     * @return the operation
     */
    public ResourceChangeOperation createOperation() {
        return new ResourceAnnotationUpdateOperation(getValue());
    }

    /**
     * undo this change
     * @param resource the resource
     */
    public void undo(Resource resource) {
        resource.setAnnotation(oldAnnotation);
    }

    
    @Override
    public String toString() {
        return I18NManager.printfMessage("ResourceAnnoChanged.toString", BUNDLE, getName(), getValue());
    }


}
