/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
/**
 * File resource store is a resource store with persistence support that
 * keeps a set of resources in memory. A simple java object serialization
 * to a fileystem is used for a persistence mechanism.
 *
 * Resource are stored to a spooldir which is specified in the constructor.
 *
 * When a instance of a file resource store is created it attempts to load
 * persisted resources and if succesfull, it stores the loaded resources in
 * in-memory map.
 *
 * Resources are persisted immediately when added to a in-memory store and removed
 * from persistance storage immediately upone a removal from in-memory store.
 *
 */
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.Filter;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents the thread-safe store of Resource objects with
 * persistence support using the filesystem.
 *
 */
public class FileResourceStore extends AbstractFileStore<Resource,ResourceStoreException> implements ResourceStore {

    private Map<ResourceId, Resource> storage;
    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    private static final Logger log = Logger.getLogger(FileResourceStore.class.getName(), BUNDLE);
    private final Lock storeLock = new ReentrantLock();
    
    
    /**
     * Creates a new instance of ResourceStore
     * @param spoolDir
     */
    public FileResourceStore(File spoolDir) {
        super(spoolDir);
        this.storage = new ConcurrentHashMap<ResourceId, Resource>();
        if (!spoolDir.exists()) {
            spoolDir.mkdirs();
        }
    }

    /**
     * Gets a list of the resources.
     * @return a list of resources
     */
    public List<Resource> getResources() {
        storeLock.lock();
        try {
            return new ArrayList<Resource>(storage.values());
        } finally {
            storeLock.unlock();
        }
    }

    /**
     * Gets a list of the resources matching a filter
     * @param filter the resource filter
     * @return a list of resources
     */
    public List<Resource> getResources(Filter<Resource> filter) throws ResourceStoreException {
        if (filter == null) {
            return getResources();
        } else {
            storeLock.lock();
            try {
                List<Resource> ret = new LinkedList<Resource>();
                ResourceVariableResolver resolver = new ResourceVariableResolver();
                for (Resource resource : storage.values()) {
                    resolver.setResource(resource);
                    if (filter.matches(resolver)) {
                        ret.add(resource);
                    }
                }
                return ret;
            } finally {
                storeLock.unlock();
            }
        }
    }

    /**
     * Gets a list of the names of the resources.
     * @return a set of resource names
     */
    public Set<ResourceId> getResourceIds() {
        storeLock.lock();
        try {
            return storage.keySet();
        } finally {
            storeLock.unlock();
        }
    }

    /**
     * This method is a convenience method that is equivalent to
     * put(resource.getId(), resource).
     * @param resource the resource whose id will be added
     * @return the resource whose id was added
     */
    public Resource add(Resource resource) throws ResourceStoreException {
        storeLock.lock();
        try {
            write(resource);
            return storage.put(resource.getId(), resource);
        } finally {
            storeLock.unlock();
        }
    }

    /**
     * Removes the key (and its corresponding value) from this
     * table. This method does nothing if the key is not in the table.
     * If the key was successfully removed from the table, the corresponding
     * spool file will also be deleted.
     *
     * @param resourceId id of a resource to be removed
     * @return the value to which the key had been mapped in this table,
     *          or <tt>null</tt> if the key did not have a mapping.
     * @throws NullPointerException  if the key is
     *               <tt>null</tt>.
     */
    public Resource remove(ResourceId resourceId) throws ResourceStoreException, UnknownResourceException {
        storeLock.lock();
        try {
            Resource retValue;
            retValue = storage.remove(resourceId);
            if (retValue == null) {
                UnknownResourceException ure = new UnknownResourceException(I18NManager.formatMessage("frs.error.unknownresource", BUNDLE, resourceId));
                if (log.isLoggable(Level.FINER)) {
                    log.throwing(FileResourceStore.class.getName(), "remove", ure);
                }
                throw ure;
            }
            if(!delete(retValue)) {
                ResourceStoreException rse = new ResourceStoreException("frs.error.deleteresource", BUNDLE, resourceId);
                if (log.isLoggable(Level.FINER)) {
                    log.throwing(getClass().getName(), "deleteResourceFile", rse);
                }
                throw rse;
            }
            return retValue;
        } finally {
            storeLock.unlock();
        }
    }

    /**
     * Returns the resource with the specified Id 
     * @param resourceId the Id of a resource
     * @return the found resource
     * @throws UnknownResourceException if there is no such resource
     */
    public Resource getResource(ResourceId resourceId) throws UnknownResourceException, ResourceStoreException {
        storeLock.lock();
        try {
            Resource res = storage.get(resourceId);
            if (res == null) {
                UnknownResourceException ure = new UnknownResourceException(I18NManager.formatMessage("frs.error.unknownresource", BUNDLE, resourceId));
                if (log.isLoggable(Level.FINER)) {
                    log.throwing(FileResourceStore.class.getName(), "getResource", ure);
                }
                throw ure;
            }
            return res;
        } finally {
            storeLock.unlock();
        }
    }

    /**
     * Clears both the in-memory and the persistent storage.
     *
     */
    public void clear() throws ResourceStoreException {
        storeLock.lock();
        try {
            for (ResourceId r : storage.keySet()) {
                try {
                    remove(r);
                } catch (UnknownResourceException ure) {
                    if (log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING, "frs.clear.unexpected", ure);
                    }
                }
            }
        } finally {
            storeLock.unlock();
        }
    }

    /**
     * Clears the in-memory store, re-reads the contents from the spool directory
     * and fill it back to memory.
     *
     * @throws ResourceStoreException 
     */
    public void loadResources() throws ResourceStoreException {
        // storage does not have to be locked as we do not care about overwriting
        // or removing the in-memory items during the loading
        storeLock.lock();
        try {
            cleanup();
            Collection<Resource> resources = load();
            clear();
            for (Resource resource : resources) {
                    storage.put(resource.getId(), resource);
            }
        } catch (Exception ex) {
            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "frs.load.unexpected", ex);
            }
        } finally {
            storeLock.unlock();
        }
    }
    
    @Override
    protected Resource readObjectFromFile(File file) throws ResourceStoreException {
        try {
            return DefaultResourceFactory.readFromFile(file);
        } catch(Exception ex) {
            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "frs.rrff.loadresource", file);
            }
            return null;
        }
    }

    @Override
    protected void writeObjectToFile(Resource obj, File file) throws ResourceStoreException {
        try {
            obj.writeToFile(file);
        } catch(Exception ex) {
            ResourceStoreException rse = new ResourceStoreException("frs.error.writetofile", BUNDLE, ex.getLocalizedMessage());
            if (log.isLoggable(Level.FINER)) {
                log.throwing(getClass().getName(), "writeResourceToFile", rse);
            }
            throw rse;            
        }
    }

    @Override
    protected String getFilenamePrefixForObject(Resource obj) {
        return obj.getId().getId();
    }

}