/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.resourceprovider.ResourceProviderConfig;
import com.sun.grid.grm.impl.AbstractComponent;
import com.sun.grid.grm.impl.ComponentExecutors;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.RequestQueue;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.resource.ResourceProviderNotActiveException;
import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.management.ManagementEventListener;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.resource.policy.Policies;
import com.sun.grid.grm.resource.policy.PolicyManager;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import com.sun.grid.grm.service.impl.ServiceCachingProxy;
import com.sun.grid.grm.service.impl.ServiceCachingProxyManager;
import com.sun.grid.grm.service.impl.SimpleServiceStore;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.OrderStore;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.ResourceFactory;
import com.sun.grid.grm.resource.ResourceIdAndName;
import com.sun.grid.grm.resource.adapter.impl.DefaultResourceAdapter;
import com.sun.grid.grm.resource.management.ManagementEventSupport;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.ServiceManager;
import com.sun.grid.grm.service.event.ServiceEventAdapter;
import com.sun.grid.grm.service.impl.DefaultService;
import com.sun.grid.grm.ui.resource.ResourceActionResult;
import com.sun.grid.grm.util.EventListenerSupport;

import com.sun.grid.grm.util.filter.Filter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The ResourceProviderImpl class is the implementation of the
 * ResourceProvider interface. It is facade to a service manager and resource
 * manager which exposes the functionality to the outside world te be used
 * as a administrative management interface of hedeby.
 */
public class ResourceProviderImpl extends AbstractComponent<ExecutorService,ResourceProviderConfig>
        implements ResourceProvider {

    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    private static final Logger log = Logger.getLogger(ResourceProviderImpl.class.getName(),
            BUNDLE);
    private final static ResourceFactory<DefaultResourceAdapter> RESOURCE_FACTORY = new DefaultResourceFactory<DefaultResourceAdapter>();
    
    private RequestQueue requestQueue;
    private OrderStore orderStore;
    private ResourceStore tempStore;
    private ServiceStore<ServiceCachingProxy> serviceStore;
    private ServiceManager<ServiceCachingProxy> serviceManager;
    private ResourceManager resourceManager;
    private final EventListenerSupport<ResourceProviderEventListener> rpEventListenerSupport;
    private final ManagementEventSupport mgmtEventListenerSupport;
    private final InnerResourceProviderEventListener rpEventListener;
    private PolicyManager policyProvider;
    /** lock for state changes */
    private final Lock stateLock = new ReentrantLock();

    /**
     * Creates a new instance of ResourceProviderImpl
     * @param env Execution environment
     * @param name name of the resource provider
     */
    public ResourceProviderImpl(ExecutionEnv env, String name) {
        super(env, name, 
              ComponentExecutors.<ResourceProviderImpl,ResourceProviderConfig>newSingleThreadedExecutorServiceFactory());

        rpEventListenerSupport = EventListenerSupport.<ResourceProviderEventListener>newAsynchronInstance(ResourceProviderEventListener.class);
        mgmtEventListenerSupport = new ManagementEventSupport(name);

        rpEventListener = new InnerResourceProviderEventListener();
    }

    /**
     * Checks if the RP is active.
     *
     * @throws ResourceProviderNotActiveException if the RP is not in the
     * STARTED state.
     */
    private void testIfActive() throws ResourceProviderNotActiveException {
        stateLock.lock();
        try {
            if (getState() != ComponentState.STARTED) {
                throw new ResourceProviderNotActiveException();
            }
        } finally {
            stateLock.unlock();
        }
    }

    /**
     * Triggers the stop/start sequence for resource provider.
     * @param forced if true, the stop will not wait until any running operations
     * are finished
     * @throws com.sun.grid.grm.GrmException if any problem occurs
     */
    @Override
    protected void reloadComponent(ResourceProviderConfig config, boolean forced) throws GrmException {
        log.entering(ResourceProviderImpl.class.getName(), "reloadComponent", forced);
        
        stateLock.lock();
        try {
            stopComponent(forced);
            startComponent(config);
        } finally {
            stateLock.unlock();
        }
        
        log.exiting(ResourceProviderImpl.class.getName(), "reloadComponent");
    }

    /**
     * Provides a string representation of the RP
     * @return string representation of RP
     */
    @Override
    public String toString() {
        return "ResourceProviderImpl: " + getName();
    }

    /**
     * Starts the resource provider and its components.
     * @throws com.sun.grid.grm.GrmException if any problem during the startup
     * occurs
     */
    @Override
    protected void startComponent(ResourceProviderConfig config) throws GrmException {
        log.entering(ResourceProviderImpl.class.getName(), "startComponent");
        
        stateLock.lock();
        try {
            if (serviceStore == null) {
                serviceStore = new SimpleServiceStore<ServiceCachingProxy>();
            }

            if (tempStore == null) {
                tempStore = new FileResourceStore(PathUtil.getRPSpoolDirForComponent(env, getName()));
                tempStore.loadResources();
            }

            if (orderStore == null) {
                orderStore = new SimpleOrderStore();
            }

            
            if (resourceManager == null) {
                DefaultService defaultService = new DefaultService(getExecutionEnv(), getName(), tempStore);
                defaultService.startService();
                defaultService.addServiceEventListener(rpEventListener);
                resourceManager = new ResourceManagerImpl<ServiceCachingProxy>(getName(), serviceStore, defaultService, orderStore);
            }

            policyProvider = Policies.createManager(config.getPolicies());

            if (requestQueue == null) {
                requestQueue = new RequestQueueImpl<ServiceCachingProxy>(serviceStore, resourceManager, orderStore, policyProvider, mgmtEventListenerSupport);
            } else {
                requestQueue.setPolicyProvider(policyProvider);
            }

            resourceManager.setRequestQueue(requestQueue);

            if (serviceManager == null) {
                serviceManager = new ServiceCachingProxyManager(env, serviceStore, resourceManager);
            }
            
            requestQueue.start();
            resourceManager.start();
            serviceManager.start();

        } finally {
            stateLock.unlock();
        }

        if (log.isLoggable(Level.INFO)) {
            log.log(Level.INFO, "rp.start.started");
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(ResourceProviderImpl.class.getName(), "startComponent");
        }
    }

    

    /**
     * Stops the RP and all its components.
     * @param forced if fordced, the RP does not wait until any running
     * operations are finished.
     * @throws com.sun.grid.grm.GrmException if any problem during the shutdown
     * occurs
     */
    @Override
    protected void stopComponent(boolean forced) throws GrmException {
        log.entering(ResourceProviderImpl.class.getName(), "stopComponent", forced);

        stateLock.lock();
        try {
            requestQueue.stop();
            resourceManager.stop();
            serviceManager.stop();
            resourceManager.getDefaultService().removeServiceEventListener(rpEventListener);
        } finally {
            stateLock.unlock();
        }

        if (log.isLoggable(Level.INFO)) {
            log.log(Level.INFO, "rp.start.stopped");
        }

        log.exiting(ResourceProviderImpl.class.getName(), "stopComponent");
    }

    /**
     * Create a new resource and assign it
     * @param type       the type of the resource
     * @param properties the properties of the resource
     * @throws ResourceProviderNotActiveException if the resource provides is not active
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the resource properties are not valid
     * @throws com.sun.grid.grm.GrmRemoteException  communication error
     * @throws com.sun.grid.grm.GrmException if the resource could not be created
     * @return the resource
     */
    public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ResourceProviderNotActiveException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
        testIfActive();
        Resource res = RESOURCE_FACTORY.createResource(getExecutionEnv(), type, properties);
        try {
            resourceManager.addResource(res);
        } catch(ServiceNotActiveException ex) {
            throw new ResourceProviderNotActiveException();
        }
        return res.clone();
    }
    
    /**
     * Move resources to a service.
     * 
     * @param idOrNameList  list of resource names or resource ids
     * @param service    the target service
     * @param isForced if true, the resource will be removed from its current service
     * by force
     * @param isStatic if true, the resource will be marked as static in target service
     * @return list of resource action that describe the result for each entry of the idOrNameList
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     */
    public List<ResourceActionResult> moveResources(List<String> idOrNameList, String service, boolean isForced, boolean isStatic) throws ResourceProviderNotActiveException {
        testIfActive();
        return resourceManager.moveResources(idOrNameList, service, isForced, isStatic);
    }

    /**
     * Remove resources from the system
     * @param idOrNameList list of resource names or resource ids
     * @param descr the removal descriptor
     * @return list of actions that describe the result for each resource
     * @throws ResourceProviderNotActiveException if the resource provider is not active
     */
    public List<ResourceActionResult> removeResources(List<String> idOrNameList, ResourceRemovalDescriptor descr) throws ResourceProviderNotActiveException {
        testIfActive();
        return resourceManager.removeResources(idOrNameList, descr);
    }

    /**
     * Reset a list of resources
     * @param idOrNameList resource names or resource ids
     * @return result of the action for each resource
     * @throws ResourceProviderNotActiveException if the reource provider is not active
     */
    public List<ResourceActionResult> resetResources(List<String> idOrNameList) throws ResourceProviderNotActiveException {
        testIfActive();
        return resourceManager.resetResources(idOrNameList);
    }


    /**
     * Modify a resource.
     * @param resId the resource id
     * @param properties the new properties
     * @return the result of the resource action
     * @throws ResourceProviderNotActiveException if resource provider is not active
     */

    public ResourceActionResult modifyResource(ResourceId resId, Map<String,Object> properties) throws ResourceProviderNotActiveException {
        testIfActive();
        return resourceManager.modifyResource(resId, properties);
    }

    /**
     * Modifies resources.
     *
     * @param idOrNameMapWithProperties Map containing the resource id or name as key and the new resource
     *             properties as values
     * @return the list of resource action results (for each key in <tt>resIdOrNameMapWithProperties</tt> one result)
     * @throws ResourceProviderNotActiveException if resource provider is not active
     */
    public List<ResourceActionResult> modifyResources(Map<String, Map<String, Object>> idOrNameMapWithProperties) throws ResourceProviderNotActiveException {
        testIfActive();
        return resourceManager.modifyResources(idOrNameMapWithProperties);
    }



    /**
     * Get a resource by its id or name.
     *
     * @param idOrName
     * @return the resources
     * @throws ResourceProviderNotActiveException if the resource provider is not active
     * @throws com.sun.grid.grm.resource.InvalidResourceException if idOrName does not
     *    address exactly one resource
     * @throws UnknownResourceException If no resource with this id or name has been found
     * @throws ServiceNotActiveException if the owning service is not active
     */
    public Resource getResourceByIdOrName(String idOrName) throws ResourceProviderNotActiveException, InvalidResourceException, UnknownResourceException, ServiceNotActiveException {
        testIfActive();
        return resourceManager.getResourceByIdOrName(idOrName);
    }


    /**
     * Gets a list of all resources that belong to the given service and matches
     * a resource filter
     * (cached information).
     *
     * @param service the target service's id
     * @param filter  the resource filter
     * @return a list of resources
     * @throws com.sun.grid.grm.service.UnknownServiceException if the service
     * was not found in the system
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service
     * is not active
     * @throws com.sun.grid.grm.service.InvalidServiceException if the target
     * service is not longer valid
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public List<Resource> getResources(String service, Filter<Resource> filter) throws UnknownServiceException, ServiceNotActiveException, InvalidServiceException, ResourceProviderNotActiveException, GrmRemoteException {
        testIfActive();
        return resourceManager.getResources(service, filter);
    }

    /**
     * Get the list of unassigned resources that matches a filter
     * @param filter  the filter
     * @return the list of unassigned resources
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if the resource provider is not active
     */
    public List<Resource> getUnassignedResources(Filter<Resource> filter) throws ResourceProviderNotActiveException {
        testIfActive();
        return resourceManager.getUnassignedResources(filter);
    }

    /**
     * Returns a list of request matching a request filter
     * @param requestFilter the requestFilter
     * @return list of requests
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     */
    public List<Request> getRequests(Filter<Request> requestFilter) throws ResourceProviderNotActiveException {
        testIfActive();
        return requestQueue.getRequests(requestFilter);
    }

    /**
     * Registers Id of a resource on a service's resource blacklist. Resource
     * on service's blacklist is not considered as a candidate to be assigned
     * to the service (during the resource request processing or manual
     * move of the resource to the service).
     *
     * @param idOrNameList list of resource ids or resource names
     * @param service name of the service that registers the resource
     *
     * @return list of resource action result that describes what happend for each entry of the
     *          idOrNameList
     * @throws ResourceProviderNotActiveException if the resource provider is not active
     */
    public List<ResourceActionResult> putResourcesOnBlackList(List<String> idOrNameList, String service)
     throws ResourceProviderNotActiveException {
        testIfActive();
        return resourceManager.putResourcesOnBlackList(idOrNameList, service);
    }


    /**
     * Gets a list of all resource ids that are on a blacklist of specified service. 
     * 
     * @param service name of the service
     * @return the list of blacklisted resource ids
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public List<ResourceIdAndName> getBlackList(String service)
            throws ResourceProviderNotActiveException, GrmRemoteException {
        testIfActive();

        return resourceManager.getBlackList(service);
    }

    /**
     * Unregisters ID of a resource from a service's resource blacklist.
     *
     * @param resourceId the id to be unregistered
     * @param service name of the service that unregisters the resource
     * @return the number of remaining services that has the resource id on its blacklist
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not
     * on service's blacklist
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public int removeResourceIDFromBlackList(ResourceId resourceId, String service)
            throws UnknownResourceException, ResourceProviderNotActiveException, GrmRemoteException {
        testIfActive();
        return resourceManager.removeResourceIDFromBlackList(resourceId, service);
    }

    /**
     * Remove resources from the blacklist of a service. The resource are addressed
     * by it's name or id.
     * @param idOrNameList list of resource names or ids
     * @param service the name of the service
     * @return list of action resource for each entry if the idOrNameList
     * @throws ResourceProviderNotActiveException if the resource provider is not active
     */
    public List<ResourceActionResult> removeResourcesFromBlackList(List<String> idOrNameList, String service)
                throws ResourceProviderNotActiveException {
        testIfActive();
        return resourceManager.removeResourcesFromBlackList(idOrNameList, service);
    }



    /**
     * Registers a ManagementEventListener.
     * @param lis listener to be registered
     */
    public void addManagementEventListener(ManagementEventListener lis) {
        mgmtEventListenerSupport.addManagementEventListener(lis);
    }

    /**
     * Unregisters a ManagementEventListener.
     * @param lis listener to be unregistered
     */
    public void removeManagementEventListener(ManagementEventListener lis) {
        mgmtEventListenerSupport.removeManagementEventListener(lis);
    }

    /**
     * Registers a ResourceProviderEventListener.
     * @param lis listener to be registered
     */
    public void addResourceProviderEventListener(
            ResourceProviderEventListener lis) {
        rpEventListenerSupport.addListener(lis);
    }

    /**
     * Unregisters a ResourceProviderEventListener.
     * @param lis listener to be unregistered
     */
    public void removeResourceProviderEventListener(
            ResourceProviderEventListener lis) {
        rpEventListenerSupport.removeListener(lis);
    }

    /* helper delegator or resource provider resource related events */
    private class InnerResourceProviderEventListener
            extends ServiceEventAdapter {


        public void addResource(AddResourceEvent event) {
            rpEventListenerSupport.getProxy().addResource(event);
        }

        public void resourceAdded(ResourceAddedEvent event) {
            rpEventListenerSupport.getProxy().resourceAdded(event);
        }

        public void removeResource(RemoveResourceEvent event) {
            rpEventListenerSupport.getProxy().removeResource(event);
        }

        public void resourceRemoved(ResourceRemovedEvent event) {
            rpEventListenerSupport.getProxy().resourceRemoved(event);
        }

        public void resourceError(ResourceErrorEvent event) {
            rpEventListenerSupport.getProxy().resourceError(event);
        }

        public void resourceReset(ResourceResetEvent event) {
            rpEventListenerSupport.getProxy().resourceReset(event);
        }

        public void resourceChanged(
                ResourceChangedEvent event) {
            rpEventListenerSupport.getProxy().resourceChanged(event);
        }

        public void resourceRejected(ResourceRejectedEvent event) {
            rpEventListenerSupport.getProxy().resourceRejected(event);
        }
    }

}
