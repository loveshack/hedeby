/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.order.Order;
import com.sun.grid.grm.resource.OrderStore;
import com.sun.grid.grm.resource.filter.OrderVariableResolver;
import com.sun.grid.grm.resource.order.ResourceRequestOrder;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.util.Deque;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents a cache of orders to be executed for resources upon the
 * receipt of an asynchronous notification, usually that the resource has been
 * freed.
 */
public class SimpleOrderStore implements OrderStore {

    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    private static final Logger log = Logger.getLogger(SimpleOrderStore.class.getName(), BUNDLE);
    /* for orders issued by RP, it has to be a list as there is slight chance
     * that the resource can be used as a candidate for a resource request processing
     * twice.the fifo processing is used for taking care of such orders, and once
     * the oldest order is done, all later orders are removed.
     * In contrary, the administration orders are processed on the lifo basis.
     * to achieve possibility to 'rollback' added/removed orders (RP needs this),
     * we need to keep all maintenance orders as well.
     */
    private final Map<String, Deque<Order>> orders;
    private final ReadWriteLock orderStoreLock;
    

    /**
     * Creates a new instance of DequeueOrderStore
     */
    public SimpleOrderStore() {
        this.orders = new HashMap<String, Deque<Order>>();
        this.orderStoreLock = new ReentrantReadWriteLock();
    }

    /**
     * Adapts the NeedProcessors nps for a particular service and SLO. This
     * looks through the existing orders, reuses or changes the NeedProcessors
     * if necessary and deletes obsolete orders for this service and SLO
     * combination.
     * @param nps the list of NeedProcessors of a resource request, will be changed!
     * @param serviceName the service that requested the resources
     * @param sloName the SLO that requested the resources
     */
    public void adaptExistingOrders(List<NeedProcessor> nps, String serviceName, String sloName) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(SimpleOrderStore.class.getName(), "adaptExistingOrders", new Object [] { nps, serviceName, sloName });
        }

        final List<NeedProcessor> orgNeedProcessorList = new ArrayList<NeedProcessor>(nps);
        nps.clear();
        
        orderStoreLock.writeLock().lock();
        try {
            final List<ResourceRequestOrder> matchingOrders = getOrdersForSLO(serviceName, sloName);
            for(NeedProcessor npFromRequest: orgNeedProcessorList) {
                Need needFromRequest = npFromRequest.getNeed();
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "os.examine.need", npFromRequest);
                }
                NeedProcessor np = null;
                Iterator<ResourceRequestOrder> iter = matchingOrders.iterator();
                while(iter.hasNext()) {
                    ResourceRequestOrder order = iter.next();
                    Need needFromOrder = order.getNeedProcessor().getNeed();
                    if(needFromOrder.equalsDisregardingQuantity(npFromRequest.getNeed())) {
                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "os.found.matching.need", new Object[]{needFromOrder, order});
                        }
                        iter.remove();
                        np = order.getNeedProcessor();
                        // Increase the quantity of the need from the order
                        // if service requests now more resources, never decrease!
                        if (needFromRequest.getQuantity() > needFromOrder.getQuantity()) {
                            if (log.isLoggable(Level.FINER)) {
                                log.log(Level.FINER, "os.increase.need.quantity", new Object[]{needFromOrder.getQuantity(), needFromRequest.getQuantity()});
                            }
                            try {
                                np.incrementNeedQuantityTo(needFromRequest.getQuantity());
                            } catch (IllegalArgumentException ex) {
                                log.log(Level.WARNING, I18NManager.formatMessage("os.could.not.increment.need", BUNDLE, needFromOrder, needFromRequest.getQuantity()), ex);
                            }
                        }

                    } else {
                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "os.found.not.matching.need", new Object[]{needFromOrder, order});
                        }
                    }
                }
                if (np == null) {
                    np = npFromRequest;
                    if (log.isLoggable(Level.FINER)) {
                        log.log(Level.FINER, "os.old.needprocessor", npFromRequest.getNeed());
                    }
                }
                nps.add(np);
            }

            // The remaining order can not be associated to a need. We consider them
            // as obsolete.
            // We simply delete them. The next request run will recreate the order
            // It can happen that a wrong resource decision is made if first a different
            // request is processed
            for(ResourceRequestOrder obsoleteOrder: matchingOrders) {
                Filter<Order> order = OrderVariableResolver.newOrderFilter(obsoleteOrder);
                removeAll(order);
            }
        } finally {
            orderStoreLock.writeLock().unlock();
        }
        log.exiting(SimpleOrderStore.class.getName(), "adaptExistingOrders", nps);
    }

    private final List<ResourceRequestOrder> getOrdersForSLO(String serviceName, String sloName) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(SimpleOrderStore.class.getName(), "getOrdersForSLO", new Object [] { serviceName, sloName });
        }
        List<ResourceRequestOrder> ret = new LinkedList<ResourceRequestOrder>();
        orderStoreLock.writeLock().lock();
        try {
            Iterator<Map.Entry<String, Deque<Order>>> oi = orders.entrySet().iterator();
            while (oi.hasNext()) {
                Deque<Order> orderDeque = oi.next().getValue();
                Iterator<Order> i = orderDeque.iterator();
                while (i.hasNext()) {
                    Order order = i.next();
                    if (log.isLoggable(Level.FINER)) {
                        log.log(Level.FINER, "os.examine.order", order);
                    }
                    if (order instanceof ResourceRequestOrder) {
                        final String serviceNameFromOrder = order.getTarget();
                        final String sloNameFromOrder = ((ResourceRequestOrder) order).getSloName();
                        if (serviceNameFromOrder != null && serviceNameFromOrder.equals(serviceName) && sloName != null && sloNameFromOrder.equals(sloName)) {
                            ret.add((ResourceRequestOrder) order);
                        }
                    }
                }
            }
        } finally {
            orderStoreLock.writeLock().unlock();
        }
        log.exiting(SimpleOrderStore.class.getName(), "getOrdersForSLO", ret);
        return ret;
    }

    /**
     * Add an order to the deque.
     * 
     * @param order the Order to add
     */
    public final void add(Order order) {
        orderStoreLock.writeLock().lock();
        try {
            Deque<Order> orderDeque = orders.get(order.getHashKey());
            if (orderDeque == null) {
                orderDeque = new Deque<Order>();
                orders.put(order.getHashKey(), orderDeque);
            }
            if (!order.isMaintenance()) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "os.add.normal", order);
                }
                orderDeque.addLast(order);
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "os.add.maintenance", order);
                }
                orderDeque.addFirst(order);
            }
        } finally {
            orderStoreLock.writeLock().unlock();
        }
    }

    /**
     * Returns and removes all orders for the resource or an empty list if there
     * is no order for the resource. If no(null) filter is used, an AlwaysMatching
     * filter is used by default.
     * 
     * @param orderFilter the filter specifying the template for search
     * @return a list of all order matching the filter
     */
    public final List<Order> removeAll(Filter<Order> orderFilter) {
        if (orderFilter == null) {
            orderFilter = ConstantFilter.<Order>alwaysMatching();
        }

        List<Order> ret = new ArrayList<Order>(orders.size());
        OrderVariableResolver resolver = new OrderVariableResolver();
        orderStoreLock.writeLock().lock();
        try {


            Iterator<Map.Entry<String, Deque<Order>>> oi = orders.entrySet().iterator();
            while (oi.hasNext()) {
                Deque<Order> orderDequeu = oi.next().getValue();
                Iterator<Order> i = orderDequeu.iterator();
                while (i.hasNext()) {
                    Order order = i.next();
                    resolver.setOrder(order);
                    if (orderFilter.matches(resolver)) {
                        ret.add(order);
                        i.remove();
                    }
                }
                if (orderDequeu.isEmpty()) {
                    oi.remove();
                }
            }
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "os.takeall.orders", new Object[]{ret, orderFilter});
            }
            return ret;
        } finally {
            orderStoreLock.writeLock().unlock();
        }
    }

    /**
     * Returns and removes the first found order for the resource or returns null
     * if there is no order for the resource.
     * 
     * @param orderFilter the filter specifying the template for search
     * @return the first found order matching the filter
     */
    public final Order remove(Filter<Order> orderFilter) {
        OrderVariableResolver resolver = new OrderVariableResolver();
        orderStoreLock.writeLock().lock();
        try {

            Iterator<Map.Entry<String, Deque<Order>>> oi = orders.entrySet().iterator();
            while (oi.hasNext()) {
                Deque<Order> orderDequeu = oi.next().getValue();
                Iterator<Order> i = orderDequeu.iterator();
                while (i.hasNext()) {
                    Order order = i.next();
                    resolver.setOrder(order);
                    if (orderFilter.matches(resolver)) {
                        i.remove();
                        if (order.isMaintenance()) {
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "os.take.maintenance", new Object[]{order, orderFilter});
                            }
                        } else {
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "os.take.normal", new Object[]{order, orderFilter});
                            }
                        }
                        if (orderDequeu.isEmpty()) {
                            oi.remove();
                        }
                        return order;
                    }
                }
            }

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "os.remove.noorder");
            }
            return null;
        } finally {
            orderStoreLock.writeLock().unlock();
        }
    }

    /**
     * Returns the first found order for the resource or returns null
     * if there is no order for the resource.
     * 
     * @param orderFilter the filter specifying the template for search
     * @return the first found order matching the filter
     */
    public final Order get(Filter<Order> orderFilter) {
        OrderVariableResolver resolver = new OrderVariableResolver();
        orderStoreLock.writeLock().lock();
        try {

            Iterator<Map.Entry<String, Deque<Order>>> oi = orders.entrySet().iterator();
            while (oi.hasNext()) {
                Deque<Order> orderDequeu = oi.next().getValue();
                Iterator<Order> i = orderDequeu.iterator();
                while (i.hasNext()) {
                    Order order = i.next();
                    resolver.setOrder(order);
                    if (orderFilter.matches(resolver)) {
                        if (order.isMaintenance()) {
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "os.take.maintenance", new Object[]{order, orderFilter});
                            }
                        } else {
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "os.take.normal", new Object[]{order, orderFilter});
                            }
                        }
                        return order;
                    }
                }
            }

            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "os.remove.noorder");
            }
            return null;
        } finally {
            orderStoreLock.writeLock().unlock();
        }
    }

    /**
     * Returns the number of stored orders.
     * @return the size of the order queue
     */
    public final int size() {
        orderStoreLock.readLock().lock();
        int size = 0;
        try {
            Iterator<Map.Entry<String, Deque<Order>>> oi = orders.entrySet().iterator();
            while (oi.hasNext()) {
                size += oi.next().getValue().size();
            }
            return size;
        } finally {
            orderStoreLock.readLock().unlock();
        }
    }
    
    /**
     * Clear the order store.
     * @return true if order store was cleared
     */
    public final boolean clear() {
        boolean flag;
        orderStoreLock.writeLock().lock();
        try {
            List<Order> toRemove = new LinkedList<Order>();
            for(Deque<Order> dq: orders.values()) {
                Iterator<Order> iter = dq.iterator();
                while(iter.hasNext()) {
                    toRemove.add(iter.next());
                }
            }
            for(Order order: toRemove) {
                remove(OrderVariableResolver.newOrderFilter(order));
            }
            orders.clear();
            flag = true;
        } finally {
            orderStoreLock.writeLock().unlock();
        }
        return flag;
    }

    /**
     * Cancel all orders that matches a filter
     * @param filter the filter
     */
    public final void cancelOrders(Filter<Order> filter) {
        log.entering(SimpleResourceStore.class.getName(), "cancelOrders", filter);
        List<Order> rem = removeAll(filter);
        for (Order order : rem) {
            log.log(Level.FINE,"os.cancel", order);
            order.cancel();
        }
        log.exiting(SimpleResourceStore.class.getName(), "cancelOrders");
    }
    
    /**
     * Commit all orders that matches a filter.
     * 
     * @param filter the filter
     */
    public final void commitOrders(Filter<Order> filter) {
        log.entering(ResourceManagerImpl.class.getName(), "commitOrders", filter);
        List<Order> rem = removeAll(filter);

        Iterator<Order> iter = rem.iterator();
        if (iter.hasNext()) {
            Order order = iter.next();
            log.log(Level.FINE, "os.commit", order);
            order.commit();
            // Normally we should not have more than one order for a resource-service tuple
            // However if we find more, we cancel the additional orders
            while(iter.hasNext()) {
                order = iter.next();
                log.log(Level.WARNING, "os.commit.fuo", order);
                order.cancel();
            }
        } else {
            log.log(Level.FINER, "os.commit.no", filter);
        }
        log.exiting(ResourceManagerImpl.class.getName(), "commitOrders");
    }
    
}
