/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.ResourceId;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  A TempResourceId can be used to find resources by it's id
 *  The type of the resource id is not considered in equals and hashcode method
 */
public class TempResourceId extends AbstractResourceId {

    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    private static final Logger log = Logger.getLogger(ResourceManagerImpl.class.getName(), BUNDLE);
    
    private final static long serialVersionUID = -20080522801L;
    private final ResourceId delegate;
    private final String service;
    private final String id;
    
    /**
     * Create a new instance.
     * @param delegate the delegate
     * @param service the name of the service
     * @throws java.lang.NullPointerException if delegate or service is null
     */
    public TempResourceId(ResourceId delegate, String service) {
        if (delegate == null) {
            throw new NullPointerException("delegate must not be null");
        }
        if (service == null) {
            throw new NullPointerException("service must not be null");
        }
        this.delegate = delegate;
        this.service = service;
        this.id = String.format("%s@%s", delegate.getId(), service);       
    }

    public static TempResourceId unwrapTemporaryResourceId(ResourceId id) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "unwrapTemporaryResourceId", id);
        }
        TempResourceId ret;
        if (id instanceof TempResourceId) {
            ret = (TempResourceId) id;
        } else {
            /* try to parse the resource id - in case it is AnyResourceID 
             * with id string that contains temporary resource id
             */
            int pos = id.getId().lastIndexOf('@');
            String original_id;
            String original_service;
            if (pos == -1) {
                original_id = id.getId();
                /* pound sign is not allowed for service name so it is safe to use it here */
                original_service = "#unknown#";
            } else {
                original_id = id.getId().substring(0, pos);
                original_service = id.getId().substring(pos + 1);
            }
            ret = new TempResourceId(new AnyResourceIdImpl(original_id), original_service);
        }
        if (log.isLoggable(Level.FINER)) {
            log.exiting(ResourceManagerImpl.class.getName(), "unwrapTemporaryResourceId", ret);
        }
        return ret;
    }

    public String getId() {
        return id;
    }

    public int compareTo(ResourceId o) {
        int ret = -1;
        if (o instanceof TempResourceId) {
            TempResourceId that = (TempResourceId) o;
            ret = getDelegate().compareTo(that.getDelegate());
            if (ret == 0) {
                ret = getService().compareTo(that.getService());
            }
        }
        return ret;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TempResourceId) {
            TempResourceId that = (TempResourceId) o;
            if (getDelegate().getId().equals(that.getDelegate().getId())) {
                return getService().equals(that.getService());
            }
        }
        return false;
    }
    
    public ResourceId getDelegate() {
        return delegate;
    }

    public String getService() {
        return service;
    }

    @Override
    public String toString() {
        return id;
    }

    public boolean matches(ResourceId id) {
        return equals(id);
    }
}
