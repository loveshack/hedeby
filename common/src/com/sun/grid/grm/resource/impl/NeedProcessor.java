/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.util.I18NManager;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 *  <p>The <code>NeedProcess</code> provides the calculation for the following measurements for a
 *     Need which is in progress.</p>
 * 
 *  <table border="1">
 *     <tr><th>Measurement</th<th>Description</th></tr>
 *     <tr><th>Committed Quantity</th>
 *         <td>
 *          Number of resources which has already been processed because of this
 *          Need. The requesting resource has already this resources assigned.
 *         </td>
 *     </tr>
 *     <tr><th>Awaited Quantity</th>
 *         <td>Number of resource for which an order is in progress.</br></td>
 *     </tr>
 *     <tr><th>Canceled Quantity</th>
 *         <td>Number of resources for which an orders has been placed, but
 *             the order could not be executed.</td>
 *     </tr>
 *     <tr><th>Remaining Quantity</th>
 *         <td>Original Quantity - Committed  Quantity</br>
 *             if the Remaining Quantity is zero the Need is fulfilled.</td>
 *     </tr>
 *     <tr><th>Open Quantity</th>
 *         <td>Original Quantity - Committed  Quantity - Awaited Quantity</br>
 *             Additional resource will be released if the Open Quantity is not zero.</td>
 *     </tr>
 * 
 *  </table>
 *    
 *     <p>The NeedProcessor holds further the list of resources for which an order
 *        has been placed, but the order could not be executed. Resource which are
 *        in the list are no longer considered the this Need.</p>
 *  
 */
public class NeedProcessor implements Serializable {
    
    private final static long serialVersionUID = -2009040901L;
    
    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    private final static Logger log = Logger.getLogger(NeedProcessor.class.getName(), BUNDLE);

    // all member variables are protected by the member lock
    private Need need;
    private int commitedQuantity;
    private final Set<ResourceId> awaitedResources = new HashSet<ResourceId>();
    private final Set<ResourceId> canceledAwaitedResources = new HashSet<ResourceId>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    
    /**
     * Create a new <code>NeedProcessor</code>.
     * @param need the original <code>Need</code>
     */
    public NeedProcessor(Need need) {
        this.need = need;
    }

    /**
     * Increase the quantity of the original <tt>Need</tt> to quantity.
     * 
     * @param quantity increase <tt>Need</tt> quantity to this value
     * @throws java.lang.IllegalArgumentException if the quantity was not increased
     */
    public void incrementNeedQuantityTo(int quantity) throws IllegalArgumentException {
        lock.writeLock().lock();
        try {
            if (need.getQuantity() > quantity) {
                throw new IllegalArgumentException("Quantity can only be increased. Old=" + need.getQuantity() + ", new=" + quantity);
            }
            need = new Need(need.getResourceFilter(), need.getUrgency(), quantity);
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    /**
     * Create a human readable string representation of the NeedProcessor
     * @return the string representation
     */
    @Override
    public String toString() {
        int committed;
        int awaited;
        int canceled;
        lock.readLock().lock();
        try {
            committed = getCommitedQuantity();
            awaited = getAwaitedQuantity();
            canceled = getCanceledQuantity();
        } finally {
            lock.readLock().unlock();
        }
        return I18NManager.formatMessage("np.toStr", BUNDLE, need, committed, awaited, canceled);
    }
    
    /**
     * Get the original need.
     * 
     * @return the original need
     */
    public Need getNeed() {
        lock.readLock().lock();
        try {
            return need;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Get the current need.
     * 
     * <p>The current need is copy if the original need, but the quantity is the 
     * reduced by committed quantity.</p>
     * 
     * @return the current need
     */
    public Need getCurrentNeed() {
        lock.readLock().lock();
        try {
            return new Need(need.getResourceFilter(), need.getUrgency(), getRemainingQuantity());
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Get the remaining quantity of the need.
     * 
     * <p>The remaining quantity of the <code>NeedProcessor</code> is the
     *    quantity of the original need minus the number of committed orders for
     *    this need.</p>
     * @return
     */
    public int getRemainingQuantity() {
        lock.readLock().lock();
        try {
            return need.getQuantity() - commitedQuantity;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Get the open quantity of the need.
     * 
     * <p>The open quantity of the need is the quantity of the original need
     *    minus the number of committed order minus the awaited resources.</p>
     * @return
     */
    public int getOpenQuantity() {
        lock.readLock().lock();
        try {
            return getRemainingQuantity() - getAwaitedQuantity();
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Get the number of resources which has been successfully assigned
     * to the requesting service.
     * 
     * @return the number of resources
     */
    public int getCommitedQuantity() {
        lock.readLock().lock();
        try {
            return commitedQuantity;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Get the number of resources where order has been placed, but the
     * order is still ongoing.
     * 
     * @return the number of resource 
     */
    public int getAwaitedQuantity() {
        lock.readLock().lock();
        try {
            return awaitedResources.size();
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Get the number of resource where an order has been placed, but it 
     * has been canceled.
     * 
     * @return the number of canceled resources
     */
    public int getCanceledQuantity() {
        lock.readLock().lock();
        try {
            return canceledAwaitedResources.size();
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * This method checks whether an order for a resource has already been placed
     * within this need process life cycle.
     * 
     * @param id the id of the resource
     * @return <code>true</code> if 
     */
    public boolean hadCanceledOrderForResource(ResourceId id) {
        log.entering(NeedProcessor.class.getName(), "hadCanceledOrderForResource", id);
        boolean ret;
        lock.readLock().lock();
        try {
            ret = canceledAwaitedResources.contains(id);
        } finally {
            lock.readLock().unlock();
        }
        log.exiting(NeedProcessor.class.getName(), "hadCanceledOrderForResource", ret);
        return ret;
    }
    
    /**
     * This method determines whether for the complete quantity of
     * the need orders has been placed (Remaining Quantity == Awaited Quantity).
     * 
     * @return <code>true</code> of all order for the need has been placed
     */
    public boolean isFullQuantityAwaited() {
        lock.readLock().lock();
        try {
            return (getRemainingQuantity() == getAwaitedQuantity());
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * 
     * @param resourceId the id of the resource
     */
    void addAwaitedResource(ResourceId resourceId) {
        log.entering(NeedProcessor.class.getName(), "addAwaitedResource", resourceId);
        boolean added = false;
        lock.writeLock().lock();
        try {
            added = awaitedResources.add(resourceId);
        } finally {
            lock.writeLock().unlock();
        }
        if (added) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "np.aar", new Object [] { resourceId, toString() });
            }
        }
        log.exiting(NeedProcessor.class.getName(), "addAwaitedResource");
    }

    /**
     * This method should be called of an order for an waited resource has been
     * canceled.
     * 
     * <p>If the resource is in the awaited resource set the number of awaited 
     *    resources is decremented.</p>
     * <p> The resource id is put on the list of awaited resource.</p>
     * 
     * @param id id of the awaited resource
     */
    public void cancelAwaitedResource(ResourceId id) {
        log.entering(NeedProcessor.class.getName(), "cancelAwaitedResource", id);
        boolean canceled = false;
        lock.writeLock().lock();
        try {
            if(awaitedResources.remove(id)) {
                canceledAwaitedResources.add(id);
                canceled = true;
            }
        } finally {
            lock.writeLock().unlock();
        }
        if (canceled) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "np.car", new Object[]{id, toString()});
            }
        }
        log.exiting(NeedProcessor.class.getName(), "cancelAwaitedResource");
    }

    /**
     * This method should be called of an order for an awaited resource has been
     * committed. 
     * 
     * <p>If the resource is in the awaited resource set committed quantity is
     *    incremented by one.</p>
     * 
     * @param resourceId the id of resource that is supposed to fill the need
     */
    public void commitAwaitedResource(ResourceId resourceId) {
        log.entering(NeedProcessor.class.getName(), "commitAwaitedResource", resourceId);
        boolean commited = false;
        lock.writeLock().lock();
        try {
            if (awaitedResources.remove(resourceId)) {
                this.commitedQuantity++;
                commited = true;
            }
        } finally {
            lock.writeLock().unlock();
        }
        if (commited) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "np.comar", new Object [] { resourceId, toString() });
            }
        }
        log.exiting(NeedProcessor.class.getName(), "commitAwaitedResource");
    }
    
}
