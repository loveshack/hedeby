/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.FactoryWithException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdException;
import com.sun.grid.grm.resource.ResourcePropertyType;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract base class for <tt>ResourceFactory</tt>
 * @param <RA> the type of the resource adapter
 */
public abstract class AbstractResourceFactory<RA extends ResourceAdapter> implements ResourceFactory<RA> {

    /**
     * Create a new Resource
     * @param env   the execution env (needed to context CS for the next resource id)
     * @param type  the type of the resource
     * @param props the properties of the resource
     * @return the new resource
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the type did not accept the properties
     * @throws ResourceIdException if the resource id could not be created
     */
    public Resource createResource(ExecutionEnv env, ResourceType type, Map<String, Object> props) throws InvalidResourcePropertiesException, ResourceIdException {
        return createResource(env.getResourceIdFactory(), type, props);
    }

    /**
     * Create a resource
     * @param resIdFactory the factory for the resource id
     * @param type    the type of the resource
     * @param props  the resource property map
     * @return the resource
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the type did not accept the properties
     * @throws ResourceIdException if the resource id could not be created
     */
    public final Resource createResource(FactoryWithException<ResourceId,ResourceIdException> resIdFactory, ResourceType type, Map<String, Object> props) throws InvalidResourcePropertiesException, ResourceIdException {
        return new ResourceImpl(type, resIdFactory, props);
    }


}
