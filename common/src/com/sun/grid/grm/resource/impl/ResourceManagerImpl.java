/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.order.MaintenanceOrder;
import com.sun.grid.grm.resource.order.ResourceRequestOrder;
import com.sun.grid.grm.resource.order.Order;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.OrderStore;
import com.sun.grid.grm.resource.RequestQueue;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.resource.management.ResourceProviderEventSupport;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.ServiceStoreException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.order.OrderFactory;
import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceIdAndName;
import com.sun.grid.grm.resource.filter.OrderVariableResolver;
import com.sun.grid.grm.resource.filter.RequestVariableResolver;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.ServiceBase;
import com.sun.grid.grm.service.descriptor.ResourcePurgeDescriptor;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.impl.DefaultService;
import com.sun.grid.grm.ui.resource.ResourceActionResult;
import com.sun.grid.grm.util.I18NManager;

import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.OrFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default implementation of a ResourceManager interface that provides the needed
 * functionality for automatic and manual management of services and their resources.
 *
 * The ResourceManagerImpl needs to have access to a source store, order store and request queue.
 *
 * Service store is used when querying the services for resources.
 * Order store is used when a order for an assignment/removal is needed to be done.
 *
 * @param <T> implementing class type of managed services
 */
public class ResourceManagerImpl<T extends Service> implements ResourceManager {

    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    private static final Logger log = Logger.getLogger(ResourceManagerImpl.class.getName(), BUNDLE);

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    private final ServiceStore<T> services;
    private final OrderStore orders;
    private final DefaultService defaultService;
    private final Map<ResourceId, Set<String>> blacklistedIDs;
    private final ResourceProviderEventSupport rpEventListenerSupport;
    private final String rpName;
    private static final ThreadGroup rmThreadGroup = new ThreadGroup("resource manager");
    private static final String actionProcessorName = "ACTION_PROCESSOR";
    private ExecutorService actionProcessorExecutor;
    private Future<Void> actionProcessorFuture = null;
    private final BlockingQueue<Action> actionQueue;
    private RequestQueue requestQueue;

    /**
     * A constructor.
     *
     * @param resourceProviderName parent resource provider name (for the monitoring purposes)
     * @param services Instance of ServiceStore, can not be null.
     * @param defaultService the default service, can not be null.
     * @param orders Instance of OrderStore, can not be null.
     */
    public ResourceManagerImpl(String resourceProviderName,
            ServiceStore<T> services, DefaultService defaultService, OrderStore orders) {
        if (services == null) {
            throw new IllegalArgumentException(I18NManager.formatMessage(
                    "rm.error.nullservicestore", BUNDLE));
        }

        if (defaultService == null) {
            throw new NullPointerException();
        }

        if (orders == null) {
            throw new IllegalArgumentException(I18NManager.formatMessage(
                    "rm.error.nullorderstore", BUNDLE));
        }

        this.services = services;
        this.orders = orders;
        this.defaultService = defaultService;
        this.rpName = resourceProviderName;
        this.rpEventListenerSupport = new ResourceProviderEventSupport(rpName);
        this.blacklistedIDs = new HashMap<ResourceId, Set<String>>();
        this.actionQueue = new LinkedBlockingQueue<Action>();
    }

    /**
     * Sets the request queue.
     * @param rq RequestQueue for storing resource requests, can not be null.
     * @throws IllegalArgumentException if request queue parameter is null
     */
    public void setRequestQueue(RequestQueue rq) {
        log.entering(ResourceManagerImpl.class.getName(), "setRequestQueue", rq);
        if (rq == null) {
            throw new IllegalArgumentException(I18NManager.formatMessage(
                    "rm.error.nullrequestqueue", BUNDLE));
        }
        lock.writeLock().lock();
        try {
            this.requestQueue = rq;
        } finally {
            lock.writeLock().unlock();
        }
        log.exiting(ResourceManagerImpl.class.getName(), "setRequestQueue");
    }

    /**
     * Get the default service of the resource manager
     *
     * <p>The default service is not a real service, it is the place where unassigned
     *    resource are stored.</p>
     *
     * @return the default service
     */
    public DefaultService getDefaultService() {
        return defaultService;
    }

    /**
     * Add a resource to the resource manager.
     *
     * <p>This method adds a resource to default service of the resource manager (holds all unassigned resources).
     *    With  the next resource processing the resource will be moved to a real service.</p>
     *
     * @param resource  the resource.
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the default service is not active
     * @throws InvalidResourceException if the resource is not accepted by the default service
     */
    public void addResource(Resource resource) throws ServiceNotActiveException, InvalidResourceException {
        log.entering(ResourceManagerImpl.class.getName(), "addResource", resource);
        defaultService.addResource(resource);
        log.exiting(ResourceManagerImpl.class.getName(), "addResource");
    }

    private void addResourceToDefaultService(Resource resource) {
        try {
            defaultService.addResource(resource);
            triggerRequestQueueReprocessing();
        } catch (InvalidResourceException ex) {
            log.log(Level.SEVERE, I18NManager.formatMessage("rm.po.resNotAccepted", BUNDLE, resource), ex);
        } catch (ServiceNotActiveException ex) {
            log.log(Level.SEVERE, I18NManager.formatMessage("rm.po.panic", BUNDLE, resource), ex);
        }
    }
    
    /**
     * Remove a resource from a service.
     *
     * <p>This method call removes a resource from the system. It creates
     *    an maintaince remove order for the resource and calls the <tt>removeResource</tt>
     *    method of the owning service.</p>
     *
     * <p>Package private for testing.</p>
     * 
     * @see ServiceBase#removeResource(ResourceId, ResourceRemovalDescriptor)
     *
     * @param resourceId id of the resource to remove
     * @param descr the descriptor of the resource remove operation
     * 
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not found in the system
     * @throws InvalidResourceException if the <tt>removeResource</tt> call on the owning service throws an InvalidResourceException
     * @throws ServiceNotActiveException if the owning service is not active
     */
     void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr)
            throws UnknownResourceException, InvalidResourceException, ServiceNotActiveException {
        log.entering(ResourceManagerImpl.class.getName(), "removeResource", descr);
        lock.writeLock().lock();
        try {
            Service service = getResourceOwner(resourceId);
            String serviceName;
            try {
                serviceName = service.getName();
            } catch (GrmRemoteException ex) {
                throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
            }
            maintenanceRemoveResource(service, descr, OrderFactory.createMaintenanceRemoveOrder(resourceId, serviceName));
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Remove resources from the system.
     * @param idOrNameList list of resource names or resource ids
     * @param descr the removal descriptor
     * @return list of actions that describe the result for each resource
     */
    public List<ResourceActionResult> removeResources(List<String> idOrNameList, ResourceRemovalDescriptor descr) {
        List<ResourceActionResult> ret = new ArrayList<ResourceActionResult>(idOrNameList.size());
        for(String idOrName: idOrNameList) {
            ResourceActionResult res = null;
            ResourceId resId = null;
            try {
                resId = getResourceByIdOrName(idOrName).getId();
                removeResource(resId, descr);
                final String message;
                if (descr instanceof ResourcePurgeDescriptor) {
                    message = I18NManager.formatMessage("rm.rp.triggered", BUNDLE);
                } else {
                    message = I18NManager.formatMessage("rm.rr.triggered", BUNDLE);
                }
                res = new ResourceActionResult(resId, idOrName, message);
            } catch (GrmException ex) {
                res = new ResourceActionResult(resId, idOrName, ex.getLocalizedMessage(), ex);
            }
            ret.add(res);
        }
        return ret;
    }


    /**
     * Administrative operation that modifies a resource (its properties).
     * If the resoruce is not found an exception is thrown.
     * 
     * @param resourceId a resource to be modified
     * @param operations that will be done on resource properties and attributes
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource
     * was not found in the system
     * @throws com.sun.grid.grm.resource.InvalidResourceException in case that
     * resource with specified id is multiple-assigned
     * @throws com.sun.grid.grm.service.UnknownServiceException if the service that
     * owns the resource is removed from system during the operation
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service that
     * owns the resource is not active
     * service is not longer valid
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if properties contains invalid valued
     */
    private void  modifyResource(ResourceId resourceId, Collection<ResourceChangeOperation> operations)
            throws UnknownResourceException, InvalidResourceException, ServiceNotActiveException, InvalidResourcePropertiesException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "modifyResource",
                    new Object[]{resourceId, operations});
        }
        Service service = getResourceOwner(resourceId);
        try {
            service.modifyResource(resourceId, operations);
        } catch (GrmRemoteException ex) {
            throw new ServiceNotActiveException("Service problem: " + ex.getLocalizedMessage(), ex);
        }
        log.exiting(ResourceManagerImpl.class.getName(), "modifyResource");
    }


    public Resource getResource(ResourceId resId) throws UnknownResourceException {
        try {
            return getResourceOwner(resId).getResource(resId);
        } catch (Exception ex) {
            throw new UnknownResourceException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Modify a resource.
     * @param resId the resource id
     * @param properties the new properties
     * @return the result of the resource action
     */
    public ResourceActionResult modifyResource(ResourceId resId, Map<String,Object> properties) {
        try {
            Resource res = getResource(resId);
            return modifyResource(res, properties);
        } catch (GrmException ex) {
            return new ResourceActionResult(resId, resId.getId(), ex.getLocalizedMessage(), ex);
        }
    }

    private ResourceActionResult modifyResource(Resource res, Map<String,Object> properties) {

        Map<String,Object> tmpProps = new HashMap<String,Object>(properties);

        res.getType().fillResourcePropertiesWithDefaults(tmpProps);
        
        Collection<ResourceChanged> changed = AbstractResourceChanged.getChanges(res.getProperties(), tmpProps);
        try {
            modifyResource(res.getId(), AbstractResourceChanged.createOperation(changed));
            return new ResourceActionResult(res.getId(), res.getName(), I18NManager.formatMessage("rm.mr", BUNDLE));
        } catch (GrmException ex) {
            return new ResourceActionResult(res.getId(), res.getName(), ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Modifies resources.
     *
     * @param resIdOrNameMapWithProperties Map containing the resource id or name as key and the new resource
     *             properties as values
     * @return the list of resource action results (for each key in <tt>resIdOrNameMapWithProperties</tt> one result)
     */
    public List<ResourceActionResult> modifyResources(Map<String, Map<String, Object>> resIdOrNameMapWithProperties) {

        List<ResourceActionResult> ret = new ArrayList<ResourceActionResult>(resIdOrNameMapWithProperties.size());

        for (Map.Entry<String, Map<String, Object>> entry : resIdOrNameMapWithProperties.entrySet()) {
            ResourceId resId = null;
            try {
                Resource res = getResourceByIdOrName(entry.getKey());
                resId = res.getId();
                ret.add(modifyResource(res, entry.getValue()));
            } catch (GrmException ex) {
                ret.add(new ResourceActionResult(resId, entry.getKey(), ex.getLocalizedMessage(), ex));
            }
        }
        return ret;
    }



    /**
     * Get a resource by it's or name.
     *
     * @param idOrName
     * @return the resources
     * @throws com.sun.grid.grm.resource.InvalidResourceException if idOrName does not
     *    address exactly one resource
     * @throws UnknownResourceException If no resource with this id or name has been found
     */
    public Resource getResourceByIdOrName(String idOrName) throws InvalidResourceException, UnknownResourceException {

        Filter<Resource> filter = ResourceVariableResolver.createIdOrNameFilter(idOrName);
        Resource ret = null;

        try {
            for (T service : services.getServices()) {
                try {
                    List<Resource> matchingResources = service.getResources(filter);
                    switch (matchingResources.size()) {
                        case 0:
                            break;
                        case 1:
                            if (ret != null) {
                                throw new InvalidResourceException("rm.gsr.fnu", BUNDLE, filter);
                            } else {
                                ret = matchingResources.get(0);
                            }
                            break;
                        default:
                            throw new InvalidResourceException("rm.gsr.fnu", BUNDLE, filter);
                    }
                } catch(ServiceNotActiveException ex) {
                    // Ignore
                } catch( GrmRemoteException ex) {
                    // Ignore
                }
            }

            try {
                List<Resource> matchingResources = defaultService.getResources(filter);
                switch (matchingResources.size()) {
                    case 0:
                        break;
                    case 1:
                        if (ret != null) {
                            throw new InvalidResourceException("rm.gsr.fnu", BUNDLE, filter);
                        } else {
                            ret = matchingResources.get(0);
                        }
                        break;
                    default:
                        throw new InvalidResourceException("rm.gsr.fnu", BUNDLE, filter);
                }
            } catch (ServiceNotActiveException ex) {
                // Ingore
            }
            
            if (ret == null) {
                throw new UnknownResourceException("rm.gsr.nf", BUNDLE, filter);
            }
            return ret;
        } catch (ServiceStoreException ex) {
            throw new InvalidResourceException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Administrative operation that triggers a action for moving the existing
     * resource from its current service to another service and marks it as a
     * static (if the flag is set to true).
     * 
     * @param targetServiceName target service where the resource should be moved
     * @param resourceId a resource to be moved
     * @param isForced if true, the resource will be removed from its current service
     * by force
     * @param isStatic if true, the resource will be marked as static in target service
     * @return true if there was a successful attempt to move the resource
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource
     * was not found in the system
     * @throws com.sun.grid.grm.service.UnknownServiceException if the target service
     * was not found in the system
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the target
     * service or service which owns the resource is not active
     * @throws com.sun.grid.grm.resource.InvalidResourceException in case that
     * resource with specified id is multiple-assigned
     * @throws com.sun.grid.grm.service.InvalidServiceException if the target
     * service is not longer valid
     */
    void moveResource(String targetServiceName, ResourceId resourceId,
            boolean isForced, boolean isStatic)
            throws UnknownResourceException, UnknownServiceException,
            ServiceNotActiveException, InvalidResourceException, InvalidServiceException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "moveResource",
                    new Object[]{targetServiceName, resourceId, isForced, isStatic});
        }

        lock.writeLock().lock();
        try {
            Service service = getResourceOwner(resourceId);

            final String serviceName;
            try {
                serviceName = service.getName();
            } catch (GrmRemoteException ex) {
                throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
            }

            if (serviceName.equals(targetServiceName)) {
                // move "from" and "to" equal!
                InvalidResourceException e = new InvalidResourceException("rm.mvr.same_service",
                        BUNDLE, new Object[]{resourceId, targetServiceName});
                log.throwing(ResourceManagerImpl.class.getName(), "moveResource", e);
                throw e;
            }

            if (isResourceIDOnBlackList(resourceId, targetServiceName)) {
                InvalidResourceException e = new InvalidResourceException("rm.mvr.res_blacklist",
                        BUNDLE, new Object[]{resourceId, targetServiceName});
                log.throwing(ResourceManagerImpl.class.getName(), "moveResource", e);
                throw e;
            }
            ResourceRemovalDescriptor descr = ResourceReassignmentDescriptor.getInstance(isForced);
            maintenanceRemoveResource(service, descr, OrderFactory.createMaintenanceAssignOrder(resourceId, serviceName, targetServiceName, isStatic));
        } finally {
            lock.writeLock().unlock();
        }

        log.exiting(ResourceManagerImpl.class.getName(), "moveResource");
    }

    /**
     * Move resource to a service
     * @param idOrNameList   list of resource names or resource ids
     * @param service       the target service
     * @param forced       if <tt>true</tt> move the resources forced
     * @param isStatic      if <tt>true</tt> the resource will become static at the target service
     * @return results of the individual resource movements
     */
    public List<ResourceActionResult> moveResources(List<String> idOrNameList, String service, boolean forced, boolean isStatic) {
        List<ResourceActionResult> ret = new ArrayList<ResourceActionResult>(idOrNameList.size());
        for(String idOrName: idOrNameList) {
            ResourceActionResult res;
            ResourceId resId = null;
            try {
                resId = getResourceByIdOrName(idOrName).getId();
                moveResource(service, resId, forced, isStatic);
                res = new ResourceActionResult(resId, idOrName, I18NManager.formatMessage("rm.rm.triggered", BUNDLE));
            } catch (GrmException ex) {
                res = new ResourceActionResult(resId, idOrName, ex.getLocalizedMessage(), ex);
            }
            ret.add(res);
        }
        return ret;
    }

    /**
     * Get the list of unassigned resources that matches a filter
     * @param filter  the filter
     * @return the list of unassigned resources
     */
    public List<Resource> getUnassignedResources(Filter<Resource> filter) {
        try {
            return defaultService.getResources(filter);
        } catch (GrmException ex) {
            return Collections.<Resource>emptyList();
        }
    }


    /**
     * Gets a list of all resources that belong to the given service matching a 
     * resource filter
     * (cached information).
     *
     * @param serviceName the target service's id
     * @param filter the resource filter
     * @return a list of resources
     * @throws com.sun.grid.grm.service.UnknownServiceException if the service
     * was not found in the system
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service
     * is not active
     * @throws com.sun.grid.grm.service.InvalidServiceException if the target
     * service is not longer valid
     */
    public List<Resource> getResources(String serviceName, Filter<Resource> filter) throws UnknownServiceException, ServiceNotActiveException, InvalidServiceException {
        log.entering(ResourceManagerImpl.class.getName(), "getResources", serviceName);

        List<Resource> ret = Collections.<Resource>emptyList();

        Service service = null;
        try {
            service = services.getService(serviceName);
        } catch (ServiceStoreException sse) {
            log.log(Level.FINER, "rm.ar.ssexception", serviceName);
        }

        if (service == null) {
            UnknownServiceException e = new UnknownServiceException("rm.error.unknownservice",
                    BUNDLE, serviceName);

            log.throwing(ResourceManagerImpl.class.getName(), "getResources", e);

            throw e;
        }
        try {
            // ServiceCachingProxy#getResources(...) calls returns the orginal
            // resources. Don't give these resources away, clone it
            List<Resource> tmpList;
            if (filter == null) {
                tmpList = service.getResources();
            } else {
                tmpList = service.getResources(filter);
            }
            ret = new ArrayList<Resource>(tmpList.size());
            for(Resource res: tmpList) {
                ret.add(res.clone());
            }
        } catch (GrmRemoteException gre) {
            InvalidServiceException ise = new InvalidServiceException(I18NManager.formatMessage(BUNDLE, "rm.error.invalid_svc", serviceName), gre);

            log.throwing(ResourceManagerImpl.class.getName(), "getResources", ise);

            throw ise;
        }
        log.exiting(ResourceManagerImpl.class.getName(), "getResources", ret);

        return ret;
    }

    private Service getResourceOwner(ResourceId resourceId) throws UnknownResourceException {
        log.entering(ResourceManagerImpl.class.getName(), "getResourceServices", resourceId);

        Service ret = null;
        try {
            defaultService.getResource(resourceId);
            ret = defaultService;
        } catch (UnknownResourceException ex) {
            // Ignore
        } catch (ServiceNotActiveException ex) {
            // Ignore
        }
        if (ret == null) {
            List<T> serviceList = Collections.<T>emptyList();
            try {
                serviceList = services.getServices();
            } catch (ServiceStoreException ex) {
                log.log(Level.WARNING, "rm.grs.ssexception", ex);
            }

            for (Service service : serviceList) {
                try {
                    service.getResource(resourceId);
                    ret = service;
                    break;

                } catch (UnknownResourceException ex) {
                    // OK service does not know the resource
                } catch (ServiceNotActiveException ex) {
                     if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "rm.grs.snae", new Object[]{service, resourceId});
                    }
                } catch (GrmRemoteException ex) {
                    log.log(Level.FINE, "rm.grs.rme", service);
                }
            }
        }
        if (ret == null) {
            throw new UnknownResourceException("rm.ex.ur", BUNDLE, resourceId);
        }

        log.exiting(ResourceManagerImpl.class.getName(), "getResourceServices", ret);

        return ret;
    }

    /**
     * This method looks for an order for the resource and carries it out.
     * @param resource the subject resource of order
     * @param source the name of the source service that triggered the order processing
     * package private for testing purpose
     */
    void processOrder(String source, Resource resource) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "processOrder",
                    new Object[]{resource, source});
        }

        Filter<Order> srcAndResFilter = OrderVariableResolver.newResourceIdAndSourceFilter(resource.getId(), source);

        // The order must not be removed at this time
        // It will be removed once target service sent the RESOURCE_ADDED event
        Order order = orders.get(srcAndResFilter);
        if (order != null) {
            switch (order.getType()) {
                case REMOVE:
                    log.log(Level.FINE, "rm.po.resourceremove", resource);
                    blacklistedIDs.remove(resource.getId());
                    break;

                case ASSIGN:
                    processAssignOrder(resource, order);
                    break;
            }
        } else {
            log.log(Level.WARNING, "rm.po.noorder", resource);
            addResourceToDefaultService(resource);
        }
        log.exiting(ResourceManagerImpl.class.getName(), "processOrder");
    }

    /** 
     * helper method to process the assign order.
     */
    private void processAssignOrder(Resource resource, Order order) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "processAssignOrder", new Object[]{resource, order});
        }

        boolean assign_triggered = false;
        try {
            // assign resource to target service
            assignExistingResource(order, resource);
            assign_triggered = true;
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "rm.po.resourceassign",
                        new Object[]{resource.getId(), order.getTarget()});
            }
        } catch (InvalidResourceException e) {
            /* Service does not like the resource, put it on its blacklist
             * to prevent assinging the resource to the service 
             * in the future.
             */
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "rm.po.badresource",
                        new Object[]{
                            resource.getId(), order.getTarget(),
                            e.getMessage()
                        });
            }
            putResourceIDOnBlackList(resource.getId(), order.getTarget());
        } catch (UnknownServiceException e) {
            /* The service is no longer in the system, so forget about 
             * its request. 
             */
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "rm.po.unknownservice",
                        new Object[]{order.getTarget(), resource.getId()});
            }
        } catch (ServiceNotActiveException e) {
            /* If the source is down, just forget about it.  The
             * source needed a resource before it went down, but
             * there's no guarantee that it will need one when it
             * comes back up.  If it does, it can ask again. */
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "rm.po.snae", new Object[]{order.getTarget(), resource});
            }
        } catch (Exception e) {
            /* If any other error occured give the administrator 
             * detailed message and try to assign to first found service.
             */
             log.log(Level.WARNING, "rm.po.othererror");
        }
        if (!assign_triggered) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "rm.po.firstfoundfail", new Object [] { resource, order });
            }
            // Cancel the order
            orders.cancelOrders(OrderVariableResolver.newOrderFilter(order));

            // resource will remain unnasigned
            addResourceToDefaultService(resource);
        }

        log.exiting(ResourceManagerImpl.class.getName(), "processAssignOrder");
    }

    /** 
     * helper method to assign a resource to specified service.
     * dealing with temporary storage has to be handled in the methods
     * that are using this method
     */
    private void assignExistingResource(Order order, Resource resource)
            throws UnknownServiceException, ServiceNotActiveException,
            InvalidResourceException, GrmException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(),
                    "assignExistingResource",
                    new Object[]{order, resource});
        }

        if (resource != null) {
            Service service = services.getService(order.getTarget());

            if (service != null) {

                String sloName;
                if (order instanceof MaintenanceOrder) {
                    MaintenanceOrder mo = (MaintenanceOrder) order;
                    resource.setStatic(mo.isStatic());
                    sloName = null;
                } else if (order instanceof ResourceRequestOrder) {
                    resource.setStatic(false);
                    sloName = ((ResourceRequestOrder) order).getSloName();
                } else {
                    throw new GrmException("Order type " + order + " is not supported");
                }
                service.addResource(resource, sloName);
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rm.aer.addresource",
                            new Object[]{order.getTarget(), resource});
                }
            } else {
                UnknownServiceException e = new UnknownServiceException("rm.error.unknownservice",
                        BUNDLE, order.getTarget());
                log.throwing(ResourceManagerImpl.class.getName(), "assignExistingResource", e);
                throw e;
            }

        } else {
            InvalidResourceException e = new InvalidResourceException("rm.error.nullresource",
                    BUNDLE);
            log.throwing(ResourceManagerImpl.class.getName(), "assignExistingResource", e);
            throw e;
        }

        log.exiting(ResourceManagerImpl.class.getName(), "assignExistingResource");
    }

    /**
     * helper method, triggers a remove of existing resource from service
     */
    private void maintenanceRemoveResource(Service service, ResourceRemovalDescriptor descr, Order order)
            throws UnknownResourceException, ServiceNotActiveException, InvalidResourceException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "maintenanceRemoveResource",
                    new Object [] { service, descr, order });
        }

        orders.add(order);
        try {
            service.removeResource(order.getResourceId(), descr);
        } catch (GrmRemoteException gre) {
            ServiceNotActiveException ise = new ServiceNotActiveException(I18NManager.formatMessage(BUNDLE, "rm.error.invalid_svc", order.getSource()), gre);

            log.throwing(ResourceManagerImpl.class.getName(), "maintenanceRemoveResource", ise);
            log.log(Level.WARNING, gre.getLocalizedMessage());
            orders.remove(OrderVariableResolver.newOrderFilter(order));
            throw ise;
        } catch (ServiceNotActiveException ex) {
            log.log(Level.WARNING, ex.getLocalizedMessage());
            orders.remove(OrderVariableResolver.newOrderFilter(order));
            throw ex;
        } catch (UnknownResourceException ex) {
            log.log(Level.WARNING, ex.getLocalizedMessage());
            orders.remove(OrderVariableResolver.newOrderFilter(order));
            throw ex;
        } catch (InvalidResourceException ex) {
            log.log(Level.WARNING, ex.getLocalizedMessage());
            orders.remove(OrderVariableResolver.newOrderFilter(order));
            throw ex;
        }

        if (log.isLoggable(Level.FINER)) {
            log.log(Level.FINER, "rm.rr.removeresource", new Object[]{order.getResourceId().toString(), order.getSource()});
            log.exiting(ResourceManagerImpl.class.getName(), "maintenanceRemoveResource");
        }
    }

    private boolean resetResource(ResourceId resourceId)
            throws UnknownResourceException, InvalidResourceException, ServiceNotActiveException {
        log.entering(ResourceManagerImpl.class.getName(), "resetResource", resourceId);

        boolean res_reset = false;

        Service service = getResourceOwner(resourceId);

        if (service != null) {
            try {
                service.resetResource(resourceId);
            } catch (GrmRemoteException ex) {
                throw new ServiceNotActiveException(ex.getLocalizedMessage(), ex);
            }
        } else {
            throw new UnknownResourceException("rm.ex.ur", BUNDLE, resourceId);
        }

        log.exiting(ResourceManagerImpl.class.getName(), "resetResource", res_reset);
        return res_reset;
    }

    /**
     * Reset a list of resources
     * @param idOrNameList resource names or resource ids
     * @return result of the action for each resource
     */
    public List<ResourceActionResult> resetResources(List<String> idOrNameList) {
        List<ResourceActionResult> ret = new ArrayList<ResourceActionResult>(idOrNameList.size());
        for(String idOrName: idOrNameList) {
            ResourceActionResult res;
            ResourceId resId = null;
            try {
                resId = getResourceByIdOrName(idOrName).getId();
                resetResource(resId);
                res = new ResourceActionResult(resId, idOrName, I18NManager.formatMessage("rm.rs.triggered", BUNDLE));
            } catch (GrmException ex) {
                res = new ResourceActionResult(resId, idOrName, ex.getLocalizedMessage(), ex);
            }
            ret.add(res);
        }
        return ret;
    }

    /**
     * Registers a ResourceProviderEventListener. Main purpose is to be used internally
     * by parent resource provider.
     * @param lis listener to be registered
     */
    public void addResourceProviderEventListener(ResourceProviderEventListener lis) {
        log.entering(ResourceManagerImpl.class.getName(), "addResourceProviderEventListener", lis);
        rpEventListenerSupport.addResourceProviderEventListener(lis);
        log.exiting(ResourceManagerImpl.class.getName(), "addResourceProviderEventListener");
    }

    /**
     * Unregisters a ResourceProviderEventListener.
     * @param lis listener to be unregistered
     */
    public void removeResourceProviderEventListener(ResourceProviderEventListener lis) {
        log.entering(ResourceManagerImpl.class.getName(), "removeResourceProviderEventListener", lis);
        rpEventListenerSupport.removeResourceProviderEventListener(lis);
        log.exiting(ResourceManagerImpl.class.getName(), "removeResourceProviderEventListener");
    }

    /**
     * Delegate method for RequestQueue.triggerRequestQueueReprocessing().
     */
    public void triggerRequestQueueReprocessing() {
        log.entering(ResourceManagerImpl.class.getName(), "triggerRequestQueueReprocessing");
        if (requestQueue != null) {
            requestQueue.triggerReprocessing();
        } else {
            log.log(Level.WARNING, "rm.error.nullrequestqueue");
        }
        log.exiting(ResourceManagerImpl.class.getName(), "triggerRequestQueueReprocessing");
    }

    /**
     * Purges all queued and pending requests for a service.
     * 
     * @param serviceName name of the service
     */
    private void purgeRequests(String serviceName) {
        log.entering(ResourceManagerImpl.class.getName(), "purgeRequests");
        if (requestQueue != null) {
            CompareFilter<Request> serviceFilter = new CompareFilter<Request>(
                    RequestVariableResolver.SERVICE_VAR,
                    new FilterConstant<Request>(serviceName),
                    CompareFilter.Operator.EQ);
            List<Request> removed = requestQueue.removeRequests(serviceFilter);
            log.log(Level.FINER, "rm.prgr.purged", removed);
        } else {
            log.log(Level.WARNING, "rm.error.nullrequestqueue");
        }
        log.exiting(ResourceManagerImpl.class.getName(), "purgeRequests");
    }

    /**
     * Purges all orders where source or target service is specified by service name.
     * 
     * @param serviceName name of the source/target service
     */
    private void purgeOrders(String serviceName) {
        log.entering(ResourceManagerImpl.class.getName(), "purgeOrders");
        if (orders != null) {
            List<Filter<Order>> filters = new ArrayList<Filter<Order>>(2);
            filters.add(OrderVariableResolver.newSourceFilter(serviceName));
            filters.add(OrderVariableResolver.newTargetFilter(serviceName));
            Filter<Order> srcOrTrgFilter = new OrFilter<Order>(filters);
            List<Order> rem = orders.removeAll(srcOrTrgFilter);
            log.log(Level.FINER, "rm.prgo.purged", rem);
            for (Order o : rem) {
                o.cancel();
            }
        } else {
            log.log(Level.WARNING, "rm.error.nullorderstore ");
        }
        log.exiting(ResourceManagerImpl.class.getName(), "purgeOrders");
    }

    /**
     * Handles notification that a service requested a resource. 
     * 
     * @param event a resource request event with details
     */
    public void processResourceRequestEvent(ResourceRequestEvent event) {
        log.entering(ResourceManagerImpl.class.getName(), "processResourceRequestEvent", event);
        submitResourceRequestEvent(event);
        log.exiting(ResourceManagerImpl.class.getName(), "processResourceRequestEvent");
    }

    /**
     * Submit an action which processes an resource request event.
     * 
     * Package private for testing.
     * 
     * @param event  the resource request event
     * @return the action which processes the resource request event
     */
    Action submitResourceRequestEvent(ResourceRequestEvent event) {
        log.entering(ResourceManagerImpl.class.getName(), "submitResourceRequestEvent", event);
        Action ret = new ResourceRequestAction(event);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitResourceRequestEvent", ret);
        return ret;
    }

    /**
     * Handles notification that a resource was added to service.
     * @param serviceName the target source id
     * @param added the target resource
     */
    public void processResourceAddedEvent(String serviceName, Resource added) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "processResourceAddedEvent", new Object[]{serviceName, added});
        }
        submitResourceAddedEvent(serviceName, added);
        log.exiting(ResourceManagerImpl.class.getName(), "processResourceAddedEvent");
    }

    /**
     * Submit an action which processes an resource added event.
     * 
     * Package private for testing.
     * 
     * @param serviceName the target source id
     * @param added the target resource
     * @return the action which processes the resource request event
     */
    Action submitResourceAddedEvent(String serviceName, Resource added) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "submitResourceAddedEvent", new Object[]{serviceName, added});
        }
        Action ret = new ResourceAddedAction(serviceName, added);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitResourceAddedEvent", ret);
        return ret;
    }

    /**
     * Handles notification that a resource was modified by the service.
     * @param serviceName the id of the source service
     * @param modified the target resource
     * @param changes a set of properties and attributes that were changed
     */
    public void processResourceChangedEvent(String serviceName, Resource modified, Collection<ResourceChanged> changes) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "processResourcePropertiesChangedEvent", new Object[]{serviceName, modified});
        }
        submitResourceChangedEvent(serviceName, modified, changes);
        log.exiting(ResourceManagerImpl.class.getName(), "processResourcePropertiesChangedEvent");
    }

    /**
     * Submit an action which processes an resource changed event.
     * 
     * Package private for testing.
     * 
     * @param serviceName the target source id
     * @param added the target resource
     * @return the action which processes the resource changed event
     */
    Action submitResourceChangedEvent(String serviceName, Resource modified, Collection<ResourceChanged> changes) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "submitResourceChangedEvent", new Object[]{serviceName, modified});
        }
        Action ret = new ResourceChangedAction(serviceName, modified, changes);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitResourceChangedEvent", ret);
        return ret;
    }

    /**
     * Handles notification that a resource is in error.
     * 
     * @param serviceName the id of the source service
     * @param bad the target resource
     * @param message a message describing the reason for the error
     */
    public void processResourceErrorEvent(String serviceName, Resource bad, String message) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "processResourceErrorEvent", new Object[]{serviceName, bad, message});
        }
        submitResourceErrorEvent(serviceName, bad, message);
        log.exiting(ResourceManagerImpl.class.getName(), "processResourceErrorEvent");
    }

    /**
     * Submit an action which processes an resource error event.
     * 
     * Package private for testing.
     * 
     * @param serviceName the target source id
     * @param added the target resource
     * @return the action which processes the resource error event
     */
    Action submitResourceErrorEvent(String serviceName, Resource bad, String message) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "submitResourceErrorEvent", new Object[]{serviceName, bad, message});
        }
        Action ret = new ResourceErrorAction(serviceName, bad, message);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitResourceErrorEvent", ret);
        return ret;
    }

    /**
     * Handles notification that a resource was released from a service.
     * @param source the id of the source service
     * @param released the target resource
     */
    public void processResourceRemovedEvent(String source, Resource released) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "processResourceRemovedEvent", new Object[]{source, released});
        }
        submitResourceRemovedEvent(source, released);
        log.exiting(ResourceManagerImpl.class.getName(), "processResourceRemovedEvent");
    }

    /**
     * Submit an action which processes an resource removed event.
     * 
     * Package private for testing.
     * 
     * @param serviceName the target source id
     * @param added the target resource
     * @return the action which processes the resource removed event
     */
    Action submitResourceRemovedEvent(String serviceName, Resource released) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "submitResourceRemovedEvent", new Object[]{serviceName, released});
        }
        Action ret = new ResourceRemovedAction(serviceName, released);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitResourceRemovedEvent", ret);
        return ret;
    }

    /**
     * Handles notification that a service has started to add a resource.
     * @param source id of the source service
     * @param resource the target resource
     */
    public void processAddResourceEvent(String source, Resource resource) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "processAddResourceEvent", new Object[]{source, resource});
        }
        submitAddResourceEvent(source, resource);
        log.exiting(ResourceManagerImpl.class.getName(), "processAddResourceEvent");
    }

    /**
     * Submit an action which processes an add resource event.
     * 
     * Package private for testing.
     * 
     * @param serviceName the target source id
     * @param added the target resource
     * @return the action which processes the add resource event
     */
    Action submitAddResourceEvent(String serviceName, Resource resource) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "submitAddResourceEvent", new Object[]{serviceName, resource});
        }
        Action ret = new AddResourceAction(serviceName, resource);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitAddResourceEvent", ret);
        return ret;
    }

    /**
     * Handles notification that a service  has started to remove a resource.
     * @param source id of the source service
     * @param resource the resource
     */
    public void processRemoveResourceEvent(String source, Resource resource) {
        log.entering(ResourceManagerImpl.class.getName(), "processRemoveResourceEvent", resource);
        submitRemoveResourceEvent(source, resource);
        log.exiting(ResourceManagerImpl.class.getName(), "processRemoveResourceEvent");
    }

    /**
     * Submit an action which processes a remove resource event.
     * 
     * Package private for testing.
     * 
     * @param serviceName the target source id
     * @param added the target resource
     * @return the action which processes the remove resource event
     */
    Action submitRemoveResourceEvent(String serviceName, Resource resource) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "submitRemoveResourceEvent", new Object[]{serviceName, resource});
        }
        Action ret = new RemoveResourceAction(serviceName, resource);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitRemoveResourceEvent", ret);
        return ret;
    }

    /**
     * Handles notification that a resource was rejected from a service during the assignment.
     * 
     * Tries to assign the resource to a service that declared a need for it, 
     * or to a first found service (if there is no order registered).
     * 
     * @param source the id of the source service
     * @param rejected the target resource
     */
    public void processResourceRejectedEvent(String source, Resource rejected) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "processResourceRejectedEvent", new Object[]{source, rejected});
        }
        submitResourceRejectedEvent(source, rejected);
        log.exiting(ResourceManagerImpl.class.getName(), "processResourceRejectedEvent");
    }

    /**
     * Submit an action which processes a resource rejected event.
     * 
     * Package private for testing.
     * 
     * @param serviceName the target source id
     * @param resource the target resource
     * @return the action which processes the resource rejected event
     */
    Action submitResourceRejectedEvent(String serviceName, Resource resource) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "submitResourceRejectedEvent", new Object[]{serviceName, resource});
        }
        Action ret = new ResourceRejectedAction(serviceName, resource);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitResourceRejectedEvent", ret);
        return ret;
    }

    /**
     * Handles notification that a resource was reset (from error state).
     *
     * @param source the id of the source service
     * @param reset the target resource
     */
    public void processResourceResetEvent(String source, Resource reset) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "processResourceResetEvent", new Object[]{source, reset});
        }
        submitResourceResetEvent(source, reset);
        log.exiting(ResourceManagerImpl.class.getName(), "processResourceResetEvent");
    }

    /**
     * Submit an action which processes a resource reset event.
     * 
     * Package private for testing.
     * 
     * @param serviceName the target source id
     * @param resource the target resource
     * @return the action which processes the resource rejected event
     */
    Action submitResourceResetEvent(String serviceName, Resource resource) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "submitResourceResetEvent", new Object[]{serviceName, resource});
        }
        Action ret = new ResourceResetAction(serviceName, resource);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitResourceResetEvent", ret);
        return ret;
    }

    /**
     * Handles notification that a Orders and Requests have to be purged.
     *
     * @param source the id of the source service
     */
    public void processPurgeOrdersAndRequestsEvent(String source) {
        log.entering(ResourceManagerImpl.class.getName(), "processPurgeOrdersAndRequestsEvent", source);
        submitPurgeOrdersAndRequestsEvent(source);
        log.exiting(ResourceManagerImpl.class.getName(), "processPurgeOrdersAndRequestsEvent");
    }

    /**
     * Submit an action which processes a PurgeOrdersAndRequest event.
     * 
     * Package private for testing.
     * 
     * @param serviceName the target source id
     * @return the action which processes the resource rejected event
     */
    Action submitPurgeOrdersAndRequestsEvent(String serviceName) {
        log.entering(ResourceManagerImpl.class.getName(), "submitPurgeOrdersAndRequestsEvent", serviceName);
        Action ret = new PurgeOrdersAndRequestsAction(serviceName);
        actionQueue.add(ret);
        log.exiting(ResourceManagerImpl.class.getName(), "submitPurgeOrdersAndRequestsEvent", ret);
        return ret;
    }

    /* helper class - encapsulates processing of PurgeOrdersAndRequest event */
    private class PurgeOrdersAndRequestsAction extends AbstractAction {

        private final String service;

        PurgeOrdersAndRequestsAction(String service) {
            this.service = service;
        }

        protected boolean doExecute() {
            log.entering(PurgeOrdersAndRequestsAction.class.getName(), "doExecute", service);
            purgeOrders(service);
            purgeRequests(service);
            log.exiting(PurgeOrdersAndRequestsAction.class.getName(), "doExecute", true);
            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("PurgeOrdersAndRequestsAction{");
            sb.append("[service=");
            if (service != null) {
                sb.append(service);
            } else {
                sb.append("no service");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /**
     * Registers Id of a resource on a service's resource blacklist. Resource
     * on service's blacklist is not considered as a candidate to be assigned
     * to the service (during the resource request processing or manual 
     * move of the resource to the service).
     * 
     * @param resourceId the id to be registered
     * @param service name of the service that registers the resource
     */
    private void putResourceIDOnBlackList(ResourceId resourceId, String service) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "putResourceIDOnBlackList", new Object[]{resourceId, service});
        }
        lock.writeLock().lock();
        try {
            Set<String> owners = blacklistedIDs.get(resourceId);
            if (owners == null) {
                owners = new HashSet<String>();
                owners.add(service);
                blacklistedIDs.put(resourceId, owners);
            } else {
                owners.add(service);
            }
        } finally {
            lock.writeLock().unlock();
        }
        log.exiting(ResourceManagerImpl.class.getName(), "putResourceIDOnBlackList");
    }

    /**
     * Registers Id of a resource on a service's resource blacklist. Resource
     * on service's blacklist is not considered as a candidate to be assigned
     * to the service (during the resource request processing or manual
     * move of the resource to the service).
     *
     * @param idOrNameList list of resource ids or resource names
     * @param service name of the service that registers the resource
     *
     * @return list of resource action result that describes what happend for each entry of the
     *          idOrNameList
     */
    public List<ResourceActionResult> putResourcesOnBlackList(List<String> idOrNameList, String service) {

        List<ResourceActionResult> ret = new ArrayList<ResourceActionResult>(idOrNameList.size());
        for(String idOrName: idOrNameList) {
            ResourceActionResult res;
            ResourceId resId = null;
            try {
                resId = getResourceByIdOrName(idOrName).getId();
                putResourceIDOnBlackList(resId, service);
                res = new ResourceActionResult(resId, idOrName, I18NManager.formatMessage("rm.pb", BUNDLE, service));
            } catch (GrmException ex) {
                res = new ResourceActionResult(resId, idOrName, ex.getLocalizedMessage(), ex);
            }
            ret.add(res);
        }
        return ret;
    }

    /**
     * Gets a list of all resource ids that are on a blacklist of specified service. 
     * 
     * @param service name of the service
     * @return the list of blacklisted resource ids
     */
    public List<ResourceIdAndName> getBlackList(String service) {
        log.entering(ResourceManagerImpl.class.getName(), "getBlackList", service);
        List<ResourceIdAndName> ret = new LinkedList<ResourceIdAndName>();
        lock.readLock().lock();
        try {
            for (Map.Entry<ResourceId, Set<String>> entry : blacklistedIDs.entrySet()) {
                if ((entry.getValue() != null) && (entry.getValue().contains(service))) {
                    Resource res;
                    try {
                        res = getResource(entry.getKey());
                        ret.add(new ResourceIdAndName(res.getId(), res.getName()));
                    } catch (GrmException ex) {
                        // We can savely ingore this exception, it can be that the
                        // owning service is not active
                    }

                }
            }
        } finally {
            lock.readLock().unlock();
        }
        log.exiting(ResourceManagerImpl.class.getName(), "getBlackList", ret);
        return ret;
    }

    /**
     * Unregisters ID of a resource from a service's resource blacklist.
     *
     * @param resourceId the id to be unregistered
     * @param service name of the service that unregisters the resource
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not
     * on service's blacklist
     * @return the number of remaining services that has the resource id on its blacklist
     */
    public int removeResourceIDFromBlackList(ResourceId resourceId, String service) throws UnknownResourceException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "removeResourceIDFromBlackList", new Object[]{resourceId, service});
        }
        int ret;
        lock.writeLock().lock();
        try {
            Set<String> owners = blacklistedIDs.get(resourceId);
            if (owners == null) {
                UnknownResourceException ure = new UnknownResourceException("rm.rrfm.ure", BUNDLE, resourceId, service);
                log.throwing(ResourceManagerImpl.class.getName(), "removeResourceIDFromBlackList", ure);
                throw ure;
            } else {
                boolean was_owner = owners.remove(service);
                if (was_owner) {
                    int size = owners.size();
                    if (size == 0) {
                        blacklistedIDs.remove(resourceId);
                        owners = null;
                    }
                    ret = size;
                } else {
                    UnknownResourceException ure = new UnknownResourceException("rm.rrfm.ure", BUNDLE, resourceId, service);
                    log.throwing(ResourceManagerImpl.class.getName(), "removeResourceIDFromBlackList", ure);
                    throw ure;
                }
            }
        } finally {
            lock.writeLock().unlock();
        }
        log.exiting(ResourceManagerImpl.class.getName(), "removeResourceIDFromBlackList", ret);
        return ret;
    }

    /**
     * Remove resources from the blacklist of a service. The resource are addressed
     * by it's name or id.
     * @param idOrNameList list of resource names or ids
     * @param service the name of the service
     * @return list of action resource for each entry if the idOrNameList
     */
    public List<ResourceActionResult> removeResourcesFromBlackList(List<String> idOrNameList, String service) {
        List<ResourceActionResult> ret = new ArrayList<ResourceActionResult>(idOrNameList.size());
        lock.writeLock().lock();
        try {
            for(String idOrName: idOrNameList) {
                ResourceActionResult res;
                ResourceId resId = null;
                try {
                    resId = getResourceByIdOrName(idOrName).getId();
                    removeResourceIDFromBlackList(resId, service);
                    res = new ResourceActionResult(resId, idOrName, I18NManager.formatMessage("rm.rb", BUNDLE, service));
                } catch (GrmException ex) {
                    res = new ResourceActionResult(resId, idOrName, ex.getLocalizedMessage(), ex);
                }
                ret.add(res);
            }
        } finally {
            lock.writeLock().unlock();
        }
        return ret;
    }

    /** 
     * Checks whether the resource with specified Id is on the service's resource
     * blacklist. 
     * 
     * @param resourceId the id to be find out
     * @param service name of the service 
     * @return true is resource with given id is on service's resource blacklist
     */
    public boolean isResourceIDOnBlackList(ResourceId resourceId, String service) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ResourceManagerImpl.class.getName(), "isResourceIDOnBlackList", new Object[]{resourceId, service});
        }
        boolean ret;
        lock.readLock().lock();
        try {
            Set<String> owners = blacklistedIDs.get(resourceId);
            if (owners == null) {
                ret = false;
            } else {
                ret = owners.contains(service);
            }
        } finally {
            lock.readLock().unlock();
        }
        log.exiting(ResourceManagerImpl.class.getName(), "isResourceIDOnBlackList", ret);
        return ret;
    }

    /* helper interface - for command pattern like processing of resource events */
    interface Action {

        public boolean execute();

        public boolean waitUntilExecuted(long timeout) throws InterruptedException;
    }

    /**
     * This helper class tracks the state of an Action.
     */
    private static abstract class AbstractAction implements Action {

        private Lock actionLock = new ReentrantLock();
        private Condition finishedCondition = actionLock.newCondition();
        private boolean finished;

        public final boolean execute() {
            try {
                return doExecute();
            } finally {
                actionLock.lock();
                try {
                    finished = true;
                    finishedCondition.signalAll();
                } finally {
                    actionLock.unlock();
                }
            }
        }

        protected abstract boolean doExecute();

        public final boolean waitUntilExecuted(long timeout) throws InterruptedException {
            long endTime = System.currentTimeMillis() + timeout;
            actionLock.lock();
            try {
                while (true) {
                    if (Thread.interrupted()) {
                        throw new InterruptedException();
                    }
                    if (finished) {
                        return true;
                    }
                    long rest = endTime - System.currentTimeMillis();
                    if (rest <= 0) {
                        return false;
                    }
                    finishedCondition.await(rest, TimeUnit.MILLISECONDS);
                }
            } finally {
                actionLock.unlock();
            }
        }
    }

    /* helper class - uncapsulates processing of ResourceRequestEvent */
    private class ResourceRequestAction extends AbstractAction {

        private final ResourceRequestEvent e;

        ResourceRequestAction(ResourceRequestEvent event) {
            this.e = event;
        }

        protected boolean doExecute() {
            Service service = null;
            try {
                service = services.getService(e.getServiceName());
                if (service != null) {
                    log.log(Level.FINEST, "rm.event.resourcerequestqueued", e);
                    if (requestQueue != null) {
                        requestQueue.addEvent(e);
                    } else {
                        log.log(Level.WARNING, "rm.error.nullrequestqueue_cant_add_event", e);
                    }
                } else {
                    log.log(Level.WARNING, "rm.event.unknownservice", e.getServiceName());
                }
            } catch (ServiceStoreException ex) {
                log.log(Level.WARNING, "rm.event.ssexception", ex);
            } catch (UnknownServiceException ex) {
                log.log(Level.WARNING, "rm.event.unknownservice", e.getServiceName());
            }

            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ResourceRequestAction{");
            sb.append("[event=");
            if (e != null) {
                sb.append(e);
            } else {
                sb.append("no event");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /* helper class - uncapsulates processing of ResourceResetEvent */
    private class ResourceResetAction extends AbstractAction {

        private final String source;
        private final Resource reset;

        ResourceResetAction(String service, Resource resource) {
            this.source = service;
            this.reset = resource;
        }

        protected boolean doExecute() {

            /* system has changed, trigger the reprocessing of the pending requests */
            ResourceManagerImpl.this.triggerRequestQueueReprocessing();

            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ResourceResetAction{");
            sb.append("[resource=");
            if (reset != null) {
                sb.append(reset.getId().getId());
            } else {
                sb.append("no resource");
            }
            sb.append("][service=");
            if (source != null) {
                sb.append(source);
            } else {
                sb.append("no service");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /* helper class - encapsulates processing of ResourceChangedEvent */
    private class ResourceChangedAction extends AbstractAction {

        private final String source;
        private final Resource modified;
        private final Collection<ResourceChanged> changes;

        ResourceChangedAction(String service, Resource resource, Collection<ResourceChanged> changes) {
            this.source = service;
            this.modified = resource;
            this.changes = changes;
        }

        protected boolean doExecute() {
            /* system has changed, trigger the reprocessing of the pending requests */
            ResourceManagerImpl.this.triggerRequestQueueReprocessing();

            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ResourcePropertiesChangedAction{");
            sb.append("[resource=");
            if (modified != null) {
                sb.append(modified.getId().getId());
            } else {
                sb.append("no resource");
            }
            sb.append("][properties=");
            if (changes != null) {
                sb.append(changes);
            } else {
                sb.append("no changed properties");
            }
            sb.append("][service=");
            if (source != null) {
                sb.append(source);
            } else {
                sb.append("no service");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /* helper class - encapsulates processing of RemoveResourceEvent */
    private class RemoveResourceAction extends AbstractAction {

        private final String source;
        private final Resource remove;

        RemoveResourceAction(String service, Resource resource) {
            this.source = service;
            this.remove = resource;
        }

        protected boolean doExecute() {

            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("RemoveResourceAction{");
            sb.append("[resource=");
            if (remove != null) {
                sb.append(remove.getId().getId());
            } else {
                sb.append("no resource");
            }
            sb.append("][service=");
            if (source != null) {
                sb.append(source);
            } else {
                sb.append("no service");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /* helper class - encapsulates processing of ResourceRemovedEvent */
    private class ResourceRemovedAction extends AbstractAction {

        private final String source;
        private final Resource released;

        ResourceRemovedAction(String service, Resource resource) {
            this.source = service;
            this.released = resource;
        }

        protected boolean doExecute() {
            processOrder(source, released);
            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ResourceRemovedAction{");
            sb.append("[resource=");
            if (released != null) {
                sb.append(released.getId().getId());
            } else {
                sb.append("no resource");
            }
            sb.append("][service=");
            if (source != null) {
                sb.append(source);
            } else {
                sb.append("no service");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /* helper class - encapsulates processing of AddResourceEvent */
    private class AddResourceAction extends AbstractAction {

        private final String source;
        private final Resource add;

        AddResourceAction(String service, Resource resource) {
            this.source = service;
            this.add = resource;
        }

        protected boolean doExecute() {

            // If there exists a resource request order for the resource and the service as source
            // we can delete it. This is the sitation that service send event sequence
            // REMOVE_RESOURCE, ADD_RESOURCE, ...
            orders.cancelOrders(OrderVariableResolver.newResourceIdAndSourceFilter(add.getId(), source));

            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("AddResourceAction{");
            sb.append("[resource=");
            if (add != null) {
                sb.append(add);
            } else {
                sb.append("no resource");
            }
            sb.append("][service=");
            if (source != null) {
                sb.append(source);
            } else {
                sb.append("no service");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /* helper class - encapsulates processing of ResourceAddedEvent */
    private class ResourceAddedAction extends AbstractAction {

        private final String source;
        private final Resource added;

        ResourceAddedAction(String service, Resource resource) {
            this.source = service;
            this.added = resource;
        }

        protected boolean doExecute() {
            // If there exists a resource request order for the resource and the service as source
            // we can delete it. This is the sitation that service send event sequence
            // REMOVE_RESOURCE, RESOURCE_ADDED
            orders.cancelOrders(OrderVariableResolver.newResourceIdAndSourceFilter(added.getId(), source));

            // Service has added the resource, we can commit the assign order
            orders.commitOrders(OrderVariableResolver.newResourceIdAndTargetFilter(added.getId(), source));

            /* system has changed, trigger the reprocessing of the pending requests */
            ResourceManagerImpl.this.triggerRequestQueueReprocessing();

            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ResourceAddedAction{");
            sb.append("[resource=");
            if (added != null) {
                sb.append(added);
            } else {
                sb.append("no resource");
            }
            sb.append("][service=");
            if (source != null) {
                sb.append(source);
            } else {
                sb.append("no service");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /* helper class - encapsulates processing of ResourceRejectedEvent */
    private class ResourceRejectedAction extends AbstractAction {

        private final String source;
        private final Resource rejected;

        ResourceRejectedAction(String service, Resource resource) {
            this.source = service;
            this.rejected = resource;
        }

        protected boolean doExecute() {
            /* put resource on a service's blacklist - the information is used only when the service is managed */
            putResourceIDOnBlackList(rejected.getId(), source);

            // Cancel all order which are waiting for this resource
            orders.cancelOrders(OrderVariableResolver.newResourceIdAndTargetFilter(rejected.getId(), source));
            addResourceToDefaultService(rejected);
            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ResourceRejectedAction{");
            sb.append("[resource=");
            if (rejected != null) {
                sb.append(rejected);
            } else {
                sb.append("no resource");
            }
            sb.append("][service=");
            if (source != null) {
                sb.append(source);
            } else {
                sb.append("no service");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /* helper class - encapsulates processing of ResourceErrorEvent */
    private class ResourceErrorAction extends AbstractAction {

        private final String source;
        private final Resource bad;
        private final String message;

        ResourceErrorAction(String service, Resource resource, String message) {
            this.source = service;
            this.bad = resource;
            this.message = message;
        }

        protected boolean doExecute() {
            // We must cancel orders which has the service as source
            orders.cancelOrders(OrderVariableResolver.newResourceIdAndSourceFilter(bad.getId(), source));
            return true;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ResourceErrorAction{");
            sb.append("[resource=");
            if (bad != null) {
                sb.append(bad);
            } else {
                sb.append("no resource");
            }
            sb.append("][service=");
            if (source != null) {
                sb.append(source);
            } else {
                sb.append("no service");
            }
            sb.append("]}");
            return sb.toString();
        }
    }

    /* helper class - processes events from event queue */
    private class ActionProcessor implements Callable<Void> {

        public Void call() throws Exception {

            Action r = null;

            try {
                while (!Thread.currentThread().isInterrupted()) {
                    try {
                        r = actionQueue.take();
                        log.log(Level.FINEST, "rm.ac.before_execute", r);
                        lock.writeLock().lock();
                        try {
                            r.execute();
                        } finally {
                            lock.writeLock().unlock();
                        }
                        log.log(Level.FINEST, "rm.ac.after_execute", r);
                        Thread.yield();
                    } catch (InterruptedException e) {
                        /* If we're interrupted, escape from loop. */
                        log.log(Level.FINE, "rm.ac.interrupted");
                        break;
                    }
                }
            } catch (Exception e) {
                log.log(Level.SEVERE, "rq.unexpected", e);
            }

            log.exiting(ActionProcessor.class.getName(), "call");
            return null;
        }
    }

    /**
     * Starts the resource manager.
     */
    public void start() {
        log.entering(ResourceManagerImpl.class.getName(), "start");

        lock.writeLock().lock();
        try {
            if ((actionProcessorExecutor == null) || actionProcessorExecutor.isTerminated()) {
                actionProcessorExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {

                    public Thread newThread(Runnable r) {
                        return new Thread(rmThreadGroup, r, actionProcessorName);
                    }
                });
            }

            if ((actionProcessorFuture == null) || actionProcessorFuture.isDone()) {
                actionProcessorFuture = actionProcessorExecutor.submit(new ActionProcessor());
            }

            /* clear all blacklists */
            blacklistedIDs.clear();

        } finally {
            lock.writeLock().unlock();
        }

        log.exiting(ResourceManagerImpl.class.getName(), "start");
    }

    /**
     * Stops the resource manager.
     */
    public void stop() {
        log.entering(ResourceManagerImpl.class.getName(), "stop");
        try {
            if (actionProcessorFuture != null) {
                actionProcessorFuture.cancel(true);

                if (actionProcessorFuture.isDone()) {
                    actionProcessorFuture = null;
                }
            }

            if (actionProcessorExecutor != null) {
                actionProcessorExecutor.shutdownNow();
                actionProcessorExecutor.awaitTermination(5, TimeUnit.SECONDS);
                actionProcessorExecutor = null;
            }
        } catch (InterruptedException ex) {
            log.log(Level.SEVERE, "rm.error.stop_intr", ex);
        }
        log.exiting(ResourceManagerImpl.class.getName(), "stop");
    }



}
