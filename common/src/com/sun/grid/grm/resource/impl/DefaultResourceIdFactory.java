/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.FactoryWithException;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdException;
import com.sun.grid.grm.ui.component.GetNextResourceIdCommand;

/**
 * Default implementation of the resource id factory.
 */
public class DefaultResourceIdFactory implements FactoryWithException<ResourceId,ResourceIdException> {

    private final ExecutionEnv env;

    /**
     * Create a new instance of the DefaultResourceIdFactory
     * @param env the execution env
     */
    public DefaultResourceIdFactory(ExecutionEnv env) {
        this.env = env;
    }
    
    /**
     * Create a new resource id.
     *
     * <p>This command executes the <tt>GetNextResourceIdCommand</tt> command
     *    via the command service to get the next resource id.</p>
     * @return the new resource id
     * @throws com.sun.grid.grm.resource.ResourceIdException if the <tt>GetNextResourceIdCommand</tt> command failed
     */
    public ResourceId newInstance() throws ResourceIdException {
        GetNextResourceIdCommand cmd = new GetNextResourceIdCommand();
        try {
            long id = env.getCommandService().execute(cmd).getReturnValue();
            return new ResourceIdImpl(id);
        } catch (GrmException ex) {
            throw new ResourceIdException(ex.getLocalizedMessage(), ex);
        }
    }

}
