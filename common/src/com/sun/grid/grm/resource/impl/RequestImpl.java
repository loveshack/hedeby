/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.order.Order;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.order.OrderFactory;
import com.sun.grid.grm.resource.OrderStore;
import com.sun.grid.grm.resource.OrderStoreException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.filter.OrderVariableResolver;
import com.sun.grid.grm.resource.policy.PolicyManager;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.ServiceStoreException;
import com.sun.grid.grm.service.UnknownServiceException;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.FilterVariable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default implementation of the Request. This implementation can be used exclusively
 * by ResourceProvider. If there is need to use Request for monitoring purposes,
 * use ShowableRequest.
 *
 * @param <T> type of implementing class of managed services
 */
public final class RequestImpl<T extends Service> extends AbstractRequest {

    private static final long serialVersionUID = -2007061201L;
    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.resource.resource";
    private transient static final Logger log = Logger.getLogger(RequestImpl.class.getName(),
            BUNDLE_NAME);
    private transient PolicyManager policy;
    private transient ServiceStore<T> services;
    private transient ResourceManager resourceManager;
    private transient OrderStore orders;
    /**
     * name of the filter variable to get the resource usage
     */
    private final static String USAGE_VAR = "usage";

    /**
     * Creates a new Request object.
     * @param event the resource request event holding the service name and
     *              the list of requested needs
     * @param services        the service store
     * @param resourceManager the resource manager
     * @param orderStore      the order store
     * @param policy          the policy manager
     */
    public RequestImpl(ResourceRequestEvent event, ServiceStore<T> services,
            ResourceManager resourceManager, OrderStore orderStore, PolicyManager policy) {
        super(event);
        this.services = services;
        this.resourceManager = resourceManager;
        this.orders = orderStore;
        this.policy = policy;
    }

    @Override
    public boolean equals(Object obj) {
        boolean ret = false;

        if (obj instanceof RequestImpl) {
            ret = super.equals(obj);
        }

        return ret;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Executes the request.
     * @return true if the request was fulfilled
     */
    public boolean execute() {
        try {
            /* If this was a "cancel" request, we're done. */
            if (getNeedProcessors().size() == 0) {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.info.zeroneed", getServiceName());
                }

                return true;
            }

            // Issue 707: Adapt the NeedProcessor list with the information of
            // the stored orders (and their repsective NeedProcessors) in the
            // order store. Clean up the order store as well (delete 
            // unnecessary orders). See OrderStore#adaptExistingOrders().
            NeedProcessorUpdater<Boolean> up = new NeedProcessorUpdater<Boolean>() {
                public Boolean update(List<NeedProcessor> needProcessors) {
                    Iterator<NeedProcessor> iter = needProcessors.iterator();
                    boolean haveNeeds = false;
                    while (iter.hasNext()) {
                        NeedProcessor np = iter.next();
                        if (np.getRemainingQuantity() == 0) {
                            iter.remove();
                        } else {
                            haveNeeds = true;
                        }
                    }
                    if (haveNeeds) {
                       orders.adaptExistingOrders(needProcessors, getServiceName(), getSLOName());
                    }
                    return haveNeeds;
                }
            };

            if (!updateNeedProcessors(up)) {
                return true;
            }

            Service targetService = null;

            try {
                targetService = services.getService(getServiceName());
            } catch (UnknownServiceException e) {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.info.unknownservice",
                            getServiceName());
                }

                return false;
            }

            // the below situation can occur, if the service was removed
            // in the meantime
            if (targetService == null) {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.info.unknownservice",
                            getServiceName());
                }

                return false;
            }

            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "rq.info.fillneed",
                        new Object[]{getServiceName(), getNeedProcessors().size()});
            }

            fillNeed();
            cleanUpNeed();


            if (getNeedProcessors().size() > 0) {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.info.unsatisfied",
                            new Object[]{getServiceName(), getNeedProcessors().size()});
                }

                return false;
            } else {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.info.satisfied", getServiceName());
                }

                return true;
            }
        } catch (Exception ex) {
            if (log.isLoggable(Level.SEVERE)) {
                log.log(Level.SEVERE, "rq.unexpected", ex);
            }
            return false;
        }
    }

    /**
     * Returns the urgency level that must be exceeded for a new entry to be
     * added to urgency list.
     * @param sortedset the sorted urgency list. If the list is not sorted, the
     *  correct behavior is not guaranteed.
     * @param quantity the number of top entries in the urgency list which will
     * be considered
     * @return the urgency level needed for a new entry to be added to the list
     */
    private final float getMinimumUrgency(SortedSet<NormalizedUrgency> set,
            int quantity) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestImpl.class.getName(), "getMinimumUrgency",
                    new Object[]{set, quantity});
        }

        if (set == null) {
            throw new NullPointerException(I18NManager.formatMessage(
                    "rq.error.nullurgencylist", BUNDLE_NAME));
        } else if ((set.size() == 0) || (quantity < 1)) {
            throw new IllegalArgumentException(I18NManager.formatMessage(
                    "rq.error.badmurgencyparams", BUNDLE_NAME,
                    set.size(), quantity));
        }

        float ret = 0.0F;

        if (set.size() <= quantity) {
            ret = set.first().normalizedUrgency;
        } else {
            Iterator<NormalizedUrgency> iterator = set.iterator();

            for (int i = 0; i < quantity; i++) {
                ret = iterator.next().normalizedUrgency;
            }
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestImpl.class.getName(), "getMinimumUrgency",
                    ret);
        }

        return ret;
    }

    /**
     * This method takes a given need and attempts to satify that need through
     * resources available from other services.
     *
     * @param need a description of the need
     * @param service the requesting service
     * @param querySource if true, the requesting service will also be queried
     * for spare resources
     * @return a sorted set of resource descriptions for matching resources
     */
    private SortedSet<NormalizedUrgency> findSpareMatchesForNeed(NeedProcessor needProcessor,
            boolean querySource) throws OrderStoreException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestImpl.class.getName(),
                    "findSpareMatchesForNeed", new Object[]{needProcessor, querySource});
        }

        SortedSet<NormalizedUrgency> spare = new TreeSet<NormalizedUrgency>();

        List<Resource> free = null;
        NormalizedUrgency nu = null;
        int usage = 0;

        /* Get the policy modifier of the requesting service. */
        float requestingServicePolicyModifier = policy.applyPolicies(needProcessor.getNeed(), getServiceName());

        if (log.isLoggable(Level.FINER)) {
            log.log(Level.FINER, "rq.fsmfn.serviceurgency",
                    new Object[]{getServiceName(), requestingServicePolicyModifier});
        }


        List<? extends Service> tmpList;
        try {
            tmpList = services.getServices();
        } catch (ServiceStoreException ex) {
            if (log.isLoggable(Level.SEVERE)) {
                log.log(Level.SEVERE, "rq.fsmfn.ssexception", ex);
            }
            tmpList = Collections.<T>emptyList();
        }

        List<Service> serviceList = new ArrayList<Service>(tmpList.size()+1);
        // Add first the default service, the order matters
        serviceList.add(resourceManager.getDefaultService());
        // Finally add the real services
        serviceList.addAll(tmpList);
        
        for (Service service: serviceList) {
            String svcName;
            try {
                svcName = service.getName();
            } catch (GrmRemoteException ex) {
                throw new IllegalStateException("Must not happen, service must be local");
            }
            
            /* No need to ask the requesting service. */
            if (!querySource && getServiceName().equals(svcName)) {
                continue;
            }

            /* Get the normalized urgency of the need as if was requested by the selected service. */
            float candidatePolicyModifier = policy.applyPolicies(needProcessor.getNeed(), svcName);

            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "rq.fsmfn.serviceurgency",
                        new Object[]{candidatePolicyModifier, svcName});
            }

            /* TODO - check how the policies are done and then fix this correctly */
            float absRuling = Math.abs(candidatePolicyModifier);
            if (absRuling >= 0 && absRuling <= Float.MIN_VALUE) {
                candidatePolicyModifier = 1.0E-7F;
            }

            /* Get the usage level that interests us. */
            float maximumUsageLevel = 0.0F;

            if (spare.size() <= needProcessor.getOpenQuantity()) {
                maximumUsageLevel = needProcessor.getNeed().getUrgency().getLevel();
            } else {
                /* If we've met our quantity requirement, check what it will take
                 * to make the list. */
                maximumUsageLevel = getMinimumUrgency(spare, needProcessor.getNeed().getQuantity());
            }

            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "rq.fsmfn.minurgency", maximumUsageLevel);
            }

            /* Calculate the normalized urgency for the service's resources. */
            usage = Math.round(needProcessor.getNeed().getUrgency().getLevel() * requestingServicePolicyModifier / candidatePolicyModifier);

            if (log.isLoggable(Level.FINEST)) {
                log.log(Level.FINEST, "rq.fsmfn.normalizedurgency", usage);
            }

            try {

                Filter<Resource> usageFilter = new CompareFilter<Resource>(new FilterVariable<Resource>(USAGE_VAR),
                        new FilterConstant<Resource>(usage), CompareFilter.Operator.LT);

                Filter<Resource> filter = null;
                if (needProcessor.getNeed().getResourceFilter() != null) {
                    AndFilter<Resource> andFilter = new AndFilter<Resource>(2);
                    andFilter.add(usageFilter);
                    andFilter.add(needProcessor.getNeed().getResourceFilter());
                    filter = andFilter;
                } else {
                    filter = usageFilter;
                }

                free = service.getResources(filter);
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.fsmfn.freeresources",
                            new Object[]{svcName, free});
                }

                if ((free != null) && (free.size() > 0)) {
                    for (Resource entry : free) {
                        if (!isResourceCandidate(entry, getServiceName(), needProcessor)) {
                            if (log.isLoggable(Level.FINER)) {
                                log.log(Level.FINER, "rq.fsmfn.notcandidate", entry);
                            }
                            continue;
                        }
                        nu = new NormalizedUrgency(service,
                                entry.getId(),
                                entry.getUsage().getLevel() * candidatePolicyModifier / requestingServicePolicyModifier);
                        spare.add(nu);

                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "rq.fsmfn.spareurgency",
                                    new Object[]{
                                        nu.sourceName, nu.resource, nu.normalizedUrgency
                                    });
                        }
                    }
                }

                /* if free isn't null */
                free = null;
            } catch (ServiceNotActiveException e) {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.fsmfn.snae",
                            svcName);
                }
            } catch (GrmRemoteException e) {
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "rq.fsmfn.rme",
                            svcName);
                }
            }
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestImpl.class.getName(),
                    "findSpareMatchesForNeed", spare);
        }

        return spare;
    }

    private boolean isResourceCandidate(Resource r, String targetService, NeedProcessor needProcessor) throws OrderStoreException {

        // We accept ASSIGNED resources from real service and UNASSIGNED resources from the default service
        if (!(r.getState().equals(Resource.State.ASSIGNED) || r.getState().equals(Resource.State.UNASSIGNED))) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "rq.nrc.state", new Object [] { r, r.getState() });
            }
            return false;
        }
        if (r.isStatic()) {
            log.log(Level.FINE, "rq.nrc.static", r);
            return false;
        }
        if (r.isAmbiguous()) {
            log.fine("resource " + r + " is not a resource candidate (ambiguous)" );
            return false;
        }
        if (resourceManager.isResourceIDOnBlackList(r.getId(), targetService)) {
            log.log(Level.FINE, "rq.nrc.black", r);
            return false;
        }
        if (orders.get(OrderVariableResolver.newResourceIdFilter(r.getId())) != null) {
            log.log(Level.FINE, "rq.nrc.order", r);
            return false;
        }
        if (needProcessor.hadCanceledOrderForResource(r.getId())) {
            log.log(Level.FINE, "rq.nrc.canceledOrder", r);
            return false;
        }
        
        return true;
    }

    /**
     * Release the resource from the service and post assignment and/or
     * reprovision orders.
     * @param found description of the resource to be released
     * @param properties the target resource definition for reprovisioning
     * @return true if the resource was successfully released, false otherwise
     */
    private boolean doRelease(NormalizedUrgency found, NeedProcessor needProcessor,
            Map<String, Object> properties) throws OrderStoreException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestImpl.class.getName(), "doRelease",
                    new Object[]{found, properties});
        }

        boolean ret = false;
        Order assignOrder = null;
        try {
            // The assignOrder is created in response of a Request caused by an SLO
            assignOrder = OrderFactory.createResourceRequestAssignOrder(found.resource, found.source.getName(), getServiceName(), needProcessor, getSLOName());
            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "rq.dr.addorder",
                        new Object[]{found.resource, getServiceName()});
            }
            orders.add(assignOrder);
            needProcessor.addAwaitedResource(found.resource);
            found.source.removeResource(found.resource, ResourceReassignmentDescriptor.INSTANCE);
            ret = true;
        } catch (InvalidResourceException ex) {
            orders.remove(OrderVariableResolver.newOrderFilter(assignOrder));
            // Service can not remove the resource
            // => Cancel the order, this resource will not be considered for this need
            assignOrder.cancel();

            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "rq.dr.invalidresource",
                        new Object[]{getServiceName(), found.resource});
                log.log(Level.WARNING, "rq.dr.removeorder",
                        new Object[]{found.resource, getServiceName()});
            }
        } catch (UnknownResourceException e) {
            orders.remove(OrderVariableResolver.newOrderFilter(assignOrder));
            assignOrder.cancel();

            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "rq.dr.unknownresource2",
                        found.resource);
                log.log(Level.WARNING, "rq.dr.removeorder",
                        new Object[]{found.resource, getServiceName()});
            }
        } catch (ServiceNotActiveException e) {
            orders.remove(OrderVariableResolver.newOrderFilter(assignOrder));
            assignOrder.cancel();

            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "rq.dr.snae",
                        new Object[]{found.sourceName, found.resource});
                log.log(Level.FINER, "rq.dr.removeorder",
                        new Object[]{found.resource, getServiceName()});
            }
        } catch (GrmRemoteException e) {
            orders.remove(OrderVariableResolver.newOrderFilter(assignOrder));
            assignOrder.cancel();

            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "rm.dr.rme",
                        new Object[]{found.sourceName, found.resource});
                log.log(Level.WARNING, "rq.dr.removeorder",
                        new Object[]{found.resource, getServiceName()});
            }
        }
        
        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestImpl.class.getName(), "doRelease", ret);
        }

        return ret;
    }

    /**
     * This method requests that the resource provider release the resources
     * in the list from their respective services.
     * @param spare the set of spare resources to release
     * @param properties the target resource definition for reprovisioning
     * @param quantity the number of resources from the spare list to release
     * @return the number of resources released; may be less than
     * <i>quantity</i>
     */
    private int releaseResources(Set<NormalizedUrgency> spare, NeedProcessor needProcessor,
            Map<String, Object> properties) throws OrderStoreException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestImpl.class.getName(), "releaseResources",
                    new Object[]{spare, properties, needProcessor});
        }

        int released = 0;
        Resource resource = null;

        for (NormalizedUrgency found : spare) {
            if (released >= needProcessor.getOpenQuantity()) {
                if (log.isLoggable(Level.FINEST)) {
                    log.log(Level.FINEST, "rq.rq.enoughreleased",
                            new Object[]{released, needProcessor.getOpenQuantity()});
                }

                break;
            }

            boolean succeeded = false;

            if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "rq.rr.askrelease",
                        new Object[]{
                            found.resource, found.sourceName, getServiceName()
                        });
            }

            succeeded = doRelease(found, needProcessor, properties);

            if (succeeded) {
                released++;

                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.rr.okrelease",
                            new Object[]{
                                found.resource, found.sourceName, getServiceName()
                            });
                }
            } else if (log.isLoggable(Level.FINER)) {
                log.log(Level.FINER, "rq.rr.failrelease",
                        new Object[]{
                            found.resource, found.sourceName, getServiceName()
                        });
            }
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestImpl.class.getName(), "releaseResources",
                    released);
        }

        return released;
    }

    /**
     * Attempts to fill all needs.
     */
    private void fillNeed() throws OrderStoreException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestImpl.class.getName(), "fillNeed");
        }

        int spares = 0;
        Set<NormalizedUrgency> spareSet = null;

        for (NeedProcessor needProcessor : getNeedProcessors()) {
            spareSet = findSpareMatchesForNeed(needProcessor, false);

            if (spareSet.size() > 0) {
                spares = 0;

                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.fn.foundspares",
                            new Object[]{spareSet.size(), getServiceName()});
                }

                spares = releaseResources(spareSet, needProcessor, null);

                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.fn.askedspares",
                            new Object[]{spares, getServiceName()});
                }

//                 issue 632
//                 need.setQuantity(need.getRemainingQuantity() - spares);

                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.fn.remainsneed",
                            needProcessor.getOpenQuantity());
                }
            } else {
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.fn.nospares", getServiceName());
                }

                /* If we are not able to find a spare, break the searching. */
                break;
            }
        }

        /* For each need */
        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestImpl.class.getName(), "fillNeed",
                    getCurrentNeeds());
        }
    }

    /**
     * removes all needs which has 0 or less quantity, does not have to
     * sync'ed as the Request is never accessed from multiple threads.
     */
    private void cleanUpNeed() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestImpl.class.getName(), "cleanUpNeed");
        }

        NeedProcessorUpdater<Void> up = new NeedProcessorUpdater<Void>() {

            public Void update(List<NeedProcessor> needProcessors) {
                List<NeedProcessor> filled = new ArrayList<NeedProcessor>(needProcessors.size());
                Iterator<NeedProcessor> i = needProcessors.iterator();
                while (i.hasNext()) {
                    NeedProcessor n = i.next();

                    if (n.getRemainingQuantity() <= 0) {
                        filled.add(n);
                    }
                }
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.cun.needremoved",
                            new Object[]{getServiceName(), filled});
                }

                needProcessors.removeAll(filled);
                if (log.isLoggable(Level.FINER)) {
                    log.log(Level.FINER, "rq.cun.stillneed",
                            new Object[]{getServiceName(), needProcessors.size()});
                }
                return null;
            }

        };

        updateNeedProcessors(up);
        
        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestImpl.class.getName(), "cleanUpNeed");
        }
    }

    /**
     * The NormalizedUrgency class relates a service's resource to that
     * service's normalized urgency value.
     */
    private static final class NormalizedUrgency implements Comparable<NormalizedUrgency> {

        final Service source;
        final String sourceName;
        final ResourceId resource;
        final float normalizedUrgency;
        /** Cache the hash code for the string */
        private int hash;
        
        /**
         * Creates a new NormalizedUrgency object.
         * @param source the service name
         * @param resource the resource
         * @param normalizedUrgency the service's normalized urgency value
         */
        NormalizedUrgency(Service source, ResourceId resource,
                float normalizedUrgency) {
            if (source == null) {
                throw new NullPointerException();
            }
            if (resource == null) {
                throw new NullPointerException();
            }
            this.source = source;
            this.normalizedUrgency = normalizedUrgency;
            this.resource = resource;
            try {
                this.sourceName = source.getName();
            } catch (GrmRemoteException ex) {
                throw new IllegalStateException("Must not happen, all services must be local services");
            }
        }

        /**
         * Compare the object to another object.
         * @param u the object to which this object should be compared
         * @return less than zero if this object is less than the other object,
         * zero if both objects are equal, and greater than 0 if this object is
         * greater than the other object.
         */
        public int compareTo(NormalizedUrgency u) {
            int ret = Float.compare(this.normalizedUrgency, u.normalizedUrgency);
            if  (ret != 0) {
                return ret;
            }
            ret = this.resource.compareTo(u.resource);
            if (ret != 0) {
                return ret;
            }
            return this.sourceName.compareTo(u.sourceName);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final NormalizedUrgency other = (NormalizedUrgency) obj;
            if (!this.sourceName.equals(other.sourceName)) {
                return false;
            }
            if (this.resource != other.resource && (this.resource == null || !this.resource.equals(other.resource))) {
                return false;
            }
            if (this.normalizedUrgency != other.normalizedUrgency) {
                return false;
            }
            return true;
        }

        @Override
        public int hashCode() {
            int h = hash;
            if (h == 0) {
                h = (97 * h) + this.sourceName.hashCode();
                h = (97 * h) +
                        ((this.resource != null) ? this.resource.hashCode() : 0);
                h = (97 * h) + Float.floatToIntBits(this.normalizedUrgency);
                hash = h;
            }
            return hash;
        }

        /**
         * Return a String representation of this object.
         * @return a String representation of this object.
         */
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(sourceName);
            sb.append("::");
            sb.append(resource);
            sb.append("[");
            sb.append(normalizedUrgency);
            sb.append("]");
            return sb.toString();
        }
    }
}
