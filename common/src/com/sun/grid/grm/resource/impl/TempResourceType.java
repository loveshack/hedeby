/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.AbstractResourceType;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourceIdException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourcePropertyType;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceTypeException;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.util.HashMap;
import java.util.Map;

/**
 * TempRysourceType used in 1.0u3 and earlier by resource_providerClass necessary to read old systems (1.0u3) spooled files
 * @deprecated Class necessary to read old systems (1.0u3) spooled files. Used by upgrade
 */
@Deprecated
public class TempResourceType extends AbstractResourceType {

    protected static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    public static final ResourcePropertyType ID = ResourcePropertyType.newMandatory("id", String.class);
    public static final ResourcePropertyType SERVICE = ResourcePropertyType.newMandatory("service", String.class);
    public static final ResourcePropertyType DELEGATE = ResourcePropertyType.newMandatory("delegate", ResourceId.class);
    public static final ResourcePropertyType DELEGATE_TYPE = ResourcePropertyType.newMandatory("type", String.class, "host");
    public static final ResourcePropertyType HOSTNAME = ResourcePropertyType.newOptional("resourceHostname", Hostname.class);
    /**
     * name of the TempResourceType
     */
    public final static String TEMP_TYPE = "temp";
    private final static TempResourceType instance = new TempResourceType();

    private TempResourceType() {
        addProperty(ID);
        addProperty(SERVICE);
        addProperty(DELEGATE);
        addProperty(DELEGATE_TYPE);
    }

    /**
     * Gets the type instance.
     *
     * @return singleton instance of TempResourceType
     */
    public static TempResourceType getInstance() {
        return instance;
    }

    /**
     * Gets type name.
     *
     * @return type name
     */
    public String getName() {
        return "temp";
    }

    /**
     * Creates an id for a given string if possible.
     *
     * @param idStr string containing information for id creation
     * @return an instance of TempResourceId
     * @throws com.sun.grid.grm.resource.InvalidResourceIdException if given string
     * does not contain necessary information for id creation
     */
    public TempResourceId createId(String idStr) throws InvalidResourceIdException {
        /* try to parse the resource id - in case it is AnyResourceID 
         * with id string that contains temporary resource id
         */
        int pos = idStr.lastIndexOf('@');
        String original_id;
        String original_service;
        if (pos == -1) {
            throw new InvalidResourceIdException(idStr, I18NManager.formatMessage("tmpid.bad.id", idStr));
        } else {
            original_id = idStr.substring(0, pos);
            original_service = idStr.substring(pos + 1);
        }
        return new TempResourceId(new AnyResourceIdImpl(original_id), original_service);
    }

    /**
     * Creates an id for a given properties if possible.
     *
     * @param properties map of properties containing information for id creation
     * @return an instance of TempResourceId
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if given properties
     * do not contain necessary information for id creation
     */
    public TempResourceId createId(Map<String, Object> properties) throws InvalidResourcePropertiesException {
        String service = (String) properties.get(SERVICE.getName());
        ResourceId delegate = (ResourceId) properties.get(DELEGATE.getName());
        if (service == null) {
            throw new InvalidResourcePropertiesException("tmpid.null.s", BUNDLE, properties);
        }
        if (delegate == null) {
            throw new InvalidResourcePropertiesException("tmpid.null.d", BUNDLE, properties);
        }
        return new TempResourceId(delegate, service);
    }

    /**
     * Creates a map of properties for a given string.
     *
     * @param id string containing information for creation of map of properties
     * @return a map of properties
     * @throws com.sun.grid.grm.resource.InvalidResourceIdException if given string
     * does not contain necessary information for creation of map of properties
     */
    public Map<String, Object> createPropertiesForId(String id) throws InvalidResourceIdException {
        Map<String, Object> props = new HashMap<String, Object>(3);
        TempResourceId tid = createId(id);
        props.put(SERVICE.getName(), tid.getService());
        props.put(DELEGATE.getName(), tid.getDelegate());
        props.put(ID.getName(), id);
        props.put(DELEGATE_TYPE.getName(), DELEGATE_TYPE.getDefaultValue());
        return props;
    }

    /**
     * Creates a map of properties for a given string and resource type specified by type
     * parameter.
     *
     * @param id string containing information for creation of map of properties
     * @param type resource type parameter
     * @return a map of properties
     * @throws com.sun.grid.grm.resource.InvalidResourceIdException if given string
     * does not contain necessary information for creation of map of properties
     */
    public Map<String, Object> createPropertiesForId(String id, ResourceType type) throws InvalidResourceIdException {
        Map<String, Object> props = new HashMap<String, Object>(3);
        TempResourceId tid = createId(id);
        props.put(SERVICE.getName(), tid.getService());
        props.put(DELEGATE.getName(), tid.getDelegate());
        props.put(ID.getName(), id);
        props.put(DELEGATE_TYPE.getName(), type.getName());
        return props;
    }

    /**
     * Get the name of the resource from its properties
     *
     * @param properties the resource properties
     * @return if the resourceHostname property is found the content of this
     *         property is returned other the unboud resource name is returned
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException
     */
    public String getResourceName(Map<String, Object> properties) throws InvalidResourcePropertiesException {
        Object value = HOSTNAME.getValue(properties);
        if (value != null) {
            return value.toString();
        } else {
            return getUnboundResourceName(properties);
        }
    }

    /**
     * Get the bound state of the resource by evaluating the properties of the
     * resource.
     *
     * <p>A host resource is considered as bound of the resourceHostname properties
     *    is set.</p>
     *
     * @param properties the resource properties
     * @return <code>true</code> if the resource is bound
     */
    public boolean isResourceBound(Map<String, Object> properties) {
        try {
            return HOSTNAME.getValue(properties) != null;
        } catch (InvalidResourcePropertiesException ex) {
            return false;
        }
    }
}
