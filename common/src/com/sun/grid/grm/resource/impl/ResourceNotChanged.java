/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.util.I18NManager;

/**
 *  Resource changed object that did not change the resource
 */
public class ResourceNotChanged extends AbstractResourceChanged<Object> {

    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    private final static long serialVersionUID = -2009100501L;
    private final static ResourceNotChanged instance = new ResourceNotChanged();

    private ResourceNotChanged() {
        super(null, null);
    }

    /**
     * get the singleton instance of the ResourceNotChanged
     * @return the singleton instance
     */
    public static ResourceNotChanged getInstance() {
        return instance;
    }

    /**
     * Had this change impact on the resource?
     * @return always <tt>false</tt>
     */
    @Override
    public boolean hasChangedResource() {
        return false;
    }

    /**
     * undo this change
     * @param resource the resource
     */
    public void undo(Resource resource) {
        // empty implementation
    }

    /**
     * Craete the resource operation that procudes this change
     * @return the resource operation
     */
    public ResourceChangeOperation createOperation() {
        return ResourceNoChangeOperation.getInstance();
    }

    @Override
    public String toString() {
        return I18NManager.formatMessage("ResourceNotChanged.toString", BUNDLE);
    }
}
