/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.util.I18NManager;
import java.io.Serializable;

/**
 *
 * Information about usage value changed
 */
public class ResourceUsageChanged extends AbstractResourceChanged<Usage> implements Serializable {

    private final static String BUNDLE = "com.sun.grid.grm.resource.resource";

    /**
     * The serial version UID
     */
    private static final long serialVersionUID = 2009100501L;

    private final Usage oldUsage;

    /**
     * Creates the instance of this class
     * @param oldUsage the old usage
     * @param newUsage the new usage
     */
    public ResourceUsageChanged(Usage oldUsage, Usage newUsage) {
        super(Resource.USAGE, newUsage);
        this.oldUsage = oldUsage;
    }

    /**
     * Reconstruct the operation that has produced this change
     * @return the operation
     */
    public ResourceChangeOperation createOperation() {
        return new UpdateResourceUsageOperation(getValue());
    }

    /**
     * Undo this change.
     *
     * @param resource the resource
     */
    public void undo(Resource resource) {
        resource.setUsage(oldUsage);
    }


    @Override
    public String toString() {
        return I18NManager.printfMessage("ResourceUpdated.toString", BUNDLE, Resource.USAGE, oldUsage, getValue());
    }

}
