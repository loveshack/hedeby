/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.*;

/**
 * An AbstractResourceId defines a final implementation of an hashcode of any 
 * ResourceId subclass to make matching a resource using the ResourceId possible 
 * (especially for resource storages that are leveraging a hashtable).
 * 
 * 
 * @deprecated use ResourceIdImpl instead
 */
@Deprecated
public abstract class AbstractResourceId implements ResourceId {
    
    private final static long serialVersionUID = -2008020601L;       
    
    /**
     * Indicates whether any other object is "equal to" this one.
     * 
     * Keep in mind, that whenever this method is overridden, it has to maintain 
     * the general contract for the <tt>hashCode</tt> method, which states
     * that equal objects must have equal hash codes. In this case it reads, 
     * that either <tt>getId</tt> method or <tt>hashCode</tt> method has to be 
     * used for finding out whether two objects are equal.
     *
     * @param   obj   the reference object with which to compare.
     * @return  <code>true</code> if this object is the same as the obj
     *          argument; <code>false</code> otherwise.
     * @see     #hashCode()
     */
    @Override
    public abstract boolean equals(Object obj);
    
    
    /**
    * Returns the string representation of a id.
    * @return the id
    */
    public abstract String getId();
    
    /**
     * Get the hashcode of this object. 
     * @return the hashcode
     */
    @Override
    public final int hashCode() {
        if (getId() == null) {
            throw new IllegalStateException("getId must not return null");
        }
        return getId().hashCode();
    }    
}
