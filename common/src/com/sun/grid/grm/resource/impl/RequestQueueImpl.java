/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.OrderStore;
import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.RequestQueue;
import com.sun.grid.grm.resource.RequestState;
import com.sun.grid.grm.resource.ResourceManager;
import com.sun.grid.grm.resource.filter.RequestVariableResolver;
import com.sun.grid.grm.resource.management.ManagementEventSupport;
import com.sun.grid.grm.resource.policy.PolicyManager;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceStore;
import com.sun.grid.grm.service.event.ResourceRequestEvent;

import com.sun.grid.grm.util.filter.Filter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The RequestQueueImpl is the default implementation of the RequestQueue
 * interface. It is responsible for queueing the resource requests sent
 * by services and also for processing them.
 *
 * The default implementation does not support any type of persistence - all
 * request are stored only in memory.
 *
 * @param <T> implementing class type of managed services
 */
public class RequestQueueImpl<T extends Service> implements RequestQueue {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.resource.resource";
    private static final Logger log = Logger.getLogger(RequestQueueImpl.class.getName(), BUNDLE_NAME);
    private static final ThreadGroup consumerThreadGroup = new ThreadGroup("request consumer");
    private static final String consumerName = "CONSUMER";
    /**
     * Thread pool managing enqueueing of the requests
     */
    private ExecutorService consumerExecutor;
    /**
     * Queue for storing queued requests. Queued request is waiting to be processed
     * by consumer.
     * 
     * It can happen, that if new request from the same service and triggered by
     * the same SLO arrives, that queued request is removed and instead this new
     * request is queued.
     *
     */
    private final BlockingQueue<Request> queued;
    /**
     * Queue for storing pending requests. Pending request is request that has been
     * already processed, but RP was not able to fulfill all its needs, so the
     * request is moved to pending queue. The pending request is then waiting 
     * for a change in the sytem that signals that request can be queued again.
     * 
     * It can happen, that if new request from the same service and triggered by
     * the same SLO arrives, that pending request is removed and instead this new
     * request is queued.
     *
     */
    private final BlockingQueue<Request> pending;
    private PolicyManager policy;
    private final ServiceStore<T> services;
    private final ResourceManager resources;
    private final OrderStore orders;
    private Future<Void> consumerFuture = null;
    private final ManagementEventSupport listener;
    private final ReadWriteLock rqLock = new ReentrantReadWriteLock();
    private boolean active = false;
    /**
     * Creates an instance of ConfigurationService
     * @param mes the management event support for forwarding the management events
     * @param services service store holding the services managed by RP
     * @param resources resource manager managing the resources
     * @param orderStore store of orders (actions that has to be taken upon a resource event)
     * @param policy policy provider as defined by RP
     */
    public RequestQueueImpl(ServiceStore<T> services, ResourceManager resources, OrderStore orderStore, PolicyManager policy, ManagementEventSupport mes) {
        this.queued = new PriorityBlockingQueue<Request>(10, REQUEST_COMPARATOR);
        this.pending = new PriorityBlockingQueue<Request>(10, REQUEST_COMPARATOR);

        this.policy = policy;

        this.services = services;
        this.resources = resources;
        this.orders = orderStore;
        this.active = false;
        this.listener = mes;
    }

    private final static Comparator<Request> REQUEST_COMPARATOR = new Comparator<Request>() {

        public int compare(Request o1, Request o2) {
            int ret = 0;
            int maxUrg1 = 0;
            for(Need n: o1.getCurrentNeeds()) {
                maxUrg1 = Math.max(maxUrg1, n.getUrgency().getLevel());
            }
            int maxUrg2 = 0;
            for(Need n: o2.getCurrentNeeds()) {
                maxUrg2 = Math.max(maxUrg2, n.getUrgency().getLevel());
            }
            if (maxUrg1 < maxUrg2) {
                ret = -1;
            } else if (maxUrg1 > maxUrg2) {
                ret = 1;
            } else {
                ret = compare(o1.getServiceName(),o2.getServiceName());
                if (ret == 0) {
                    ret = compare(o1.getSLOName(), o2.getSLOName());
                }
            }
            /**
             * Sort the requests descending by max urgency
             */
            return -ret;
        }
        
        @SuppressWarnings("unchecked")
        private int compare(Comparable c1, Comparable c2) {

            if (c1 == null) {
                if (c2 == null) {
                    return 0;
                } else {
                    return -1;
                }
            } else if (c2 == null) {
                return 1;
            } else {
                return c1.compareTo(c2);
            }
        }

    };



    /**
     * This method puts an incoming event on the queue to be handled. If
     * there is an event from the same service and triggered by the same SLO
     * waiting in the queue, it is overwritten by the new request.
     * @param event an incoming resource request event
     */
    public void addEvent(ResourceRequestEvent event) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestQueueImpl.class.getName(), "addEvent", event);
        }

        Request r = new RequestImpl<T>(event, services, resources, orders, policy);
        addRequest(r);

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestQueueImpl.class.getName(), "addEvent");
        }
    }

    /**
     * In contrast to the BlockingQueue#contains method this method does not
     * use the compareTo method of the comparator of the queue. It uses
     * the Request#equals to find the request
     */
    private static boolean contains(BlockingQueue<Request> queue, Request request) {
        for(Request r: queue) {
            if (request.equals(r)) {
                return true;
            }
        }
        return false;
    }

    /**
     * In contrast to the BlockingQueue#contains method this method does not
     * use the compareTo method of the comparator of the queue. It uses
     * the Request#equals to find the request
     */
    private static boolean remove(BlockingQueue<Request> queue, Request request) {
        Iterator<Request> iter = queue.iterator();
        while (iter.hasNext()) {
            if (iter.next().equals(request)) {
                iter.remove();
                return true;
            }
        }
        return false;
    }
    private void addRequest(Request request) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestQueueImpl.class.getName(), "addRequest", request);
        }
        rqLock.writeLock().lock();
        try {
            if (!isActive()) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "rq.notactive");
                }
            } else {
                // We must not rely on the queue.remove method, because
                // the Comparator of the queue considers besides the service
                // name and the slo name also the urgency of the request
                // The equals method of Request considers only service name and
                // slo name
                if (remove(queued, request)) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "rq.event.overwrite", request);
                    }
                }
                if (remove(pending, request)) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "rq.event.overwrite_nf", request);
                    }
                }
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "rq.event.add", request);
                }
                queued.add(request);
            }
        } finally {
            rqLock.writeLock().unlock();
        }
        listener.fireRequestQueued(request.getServiceName(), request.getCurrentNeeds());

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestQueueImpl.class.getName(), "addRequest");
        }
    }

    private void addNotFulfilledRequest(Request request) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestQueueImpl.class.getName(),
                    "addNotFulfilledRequest", request);
        }

        rqLock.writeLock().lock();

        try {
            if (!isActive()) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "rq.notactive");
                }
            } else {
                if (contains(queued, request)) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "rq.event.overwrite", request);
                    }
                // there is newer request, do not enqueue the pending
                // request again. the new request will take over
                } else if (remove(pending, request)) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "rq.event.overwrite_nf", request);
                    }
                    pending.add(request);
                // if this happens, there is an implementation bug -
                // log it
                } else {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "rq.event.add", request);
                    }
                    pending.add(request);
                }
            }

        } finally {
            rqLock.writeLock().unlock();
        }

        listener.fireRequestPending(request.getServiceName(), request.getCurrentNeeds());

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestQueueImpl.class.getName(), "addRequest");
        }
    }

    /**
     * This method puts an incoming event on the queue to be handled. If
     * there is an event from the same service and triggered by the same SLO
     * waiting in the queue, it is overwritten by the new request.
     */
    public void triggerReprocessing() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestQueueImpl.class.getName(), "triggerReprocessing");
        }

        rqLock.writeLock().lock();
        Map<String, List<Need>> queuedRequests = new HashMap<String, List<Need>>();
        try {
            if (!isActive()) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "rq.notactive");
                }
            } else {
                Iterator<Request> iterator = pending.iterator();

                while (iterator.hasNext()) {
                    Request r = iterator.next();

                    if (!contains(queued, r)) {
                        // there is no newer request from the same service/SLO
                        // so we assume this one is still valid
                        queued.add(r);
                        queuedRequests.put(r.getServiceName(), r.getCurrentNeeds());
                    } else {
                        // throw away, there is newer request from the same
                        // service/SLO queued
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "rq.pendingremoved", r);
                        }
                    }

                    iterator.remove();
                }
            }
        } finally {
            rqLock.writeLock().unlock();
        }

        listener.fireRequestsQueued(queuedRequests);

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestQueueImpl.class.getName(), "triggerReprocessing");
        }
    }

    private boolean isActive() {
        rqLock.readLock().lock();

        try {
            return active;
        } finally {
            rqLock.readLock().unlock();
        }
    }

    /**
     * Sets the policy manager.
     *
     * @param policyProvider desired policy manager
     */
    public void setPolicyProvider(PolicyManager policyProvider) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestQueueImpl.class.getName(), "setPolicyProvider",
                    policyProvider);
        }

        rqLock.writeLock().lock();

        try {
            this.policy = policyProvider;
        } finally {
            rqLock.writeLock().unlock();
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestQueueImpl.class.getName(), "setPolicyProvider");
        }
    }

    /**
     * Starts the request queue.
     */
    public void start() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestQueueImpl.class.getName(), "start");
        }

        rqLock.writeLock().lock();

        try {
            if ((consumerExecutor == null) || consumerExecutor.isTerminated()) {
                consumerExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {

                    public Thread newThread(Runnable r) {
                        return new Thread(consumerThreadGroup, r, consumerName);
                    }
                });
            }

            if ((consumerFuture == null) || consumerFuture.isDone()) {
                consumerFuture = consumerExecutor.submit(new Consumer());
            }

            active = true;
        } finally {
            rqLock.writeLock().unlock();
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestQueueImpl.class.getName(), "start");
        }
    }

    /**
     * Stops the request queue.
     */
    public void stop() {
        if (log.isLoggable(Level.FINER)) {
            log.entering(RequestQueueImpl.class.getName(), "stop");
        }

        rqLock.writeLock().lock();

        try {
            active = false;
            /* clear both queued and pending events - if service has requests
             * after RP is restarted, it will send them again */
            queued.clear();
            pending.clear();
            if (consumerFuture != null) {
                consumerFuture.cancel(true);
                consumerFuture = null;
            }

            if (consumerExecutor != null) {
                consumerExecutor.shutdownNow();
                consumerExecutor.awaitTermination(5, TimeUnit.SECONDS);
                consumerExecutor = null;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(RequestQueueImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rqLock.writeLock().unlock();
        }

        if (log.isLoggable(Level.FINER)) {
            log.exiting(RequestQueueImpl.class.getName(), "stop");
        }
    }

    /**
     * Returns a list of request matching a request filter
     * @param requestFilter the requestFilter
     * @return list of requests
     */
    public List<Request> getRequests(Filter<Request> requestFilter) {
        RequestVariableResolver resolver = new RequestVariableResolver();
        rqLock.readLock().lock();
        try {
            List<Request> ret = new ArrayList<Request>(queued.size());
            for (Request r : queued) {
                resolver.setRequest(r, RequestState.QUEUED);
                if (requestFilter.matches(resolver)) {
                    ShowableRequest sr = new ShowableRequest(r, policy);
                    sr.setState(RequestState.QUEUED);
                    ret.add(sr);
                }
            }
            for (Request r : pending) {
                resolver.setRequest(r, RequestState.PENDING);
                if (requestFilter.matches(resolver)) {
                    ShowableRequest sr = new ShowableRequest(r, policy);
                    sr.setState(RequestState.PENDING);
                    ret.add(sr);
                }
            }
            return Collections.unmodifiableList(ret);
        } finally {
            rqLock.readLock().unlock();
        }
    }

    /**
     * Removes a list of request matching a request filter
     * 
     * @param requestFilter the requestFilter
     * @return list of requests
     */
    public List<Request> removeRequests(Filter<Request> requestFilter) {
        RequestVariableResolver resolver = new RequestVariableResolver();
        rqLock.readLock().lock();
        try {
            List<Request> ret = new ArrayList<Request>(queued.size());
            Iterator<Request> qi = queued.iterator();
            while (qi.hasNext()) {
                Request r = qi.next();
                resolver.setRequest(r, RequestState.QUEUED);
                if (requestFilter.matches(resolver)) {
                    ShowableRequest sr = new ShowableRequest(r, policy);
                    sr.setState(RequestState.QUEUED);
                    ret.add(sr);
                    qi.remove();
                }
            }
            Iterator<Request> pi = pending.iterator();
            while (pi.hasNext()) {
                Request r = pi.next();
                resolver.setRequest(r, RequestState.PENDING);
                if (requestFilter.matches(resolver)) {
                    ShowableRequest sr = new ShowableRequest(r, policy);
                    sr.setState(RequestState.PENDING);
                    ret.add(sr);
                    pi.remove();
                }
            }
            return Collections.unmodifiableList(ret);
        } finally {
            rqLock.readLock().unlock();
        }
    }

    /**
     * Scheduler submits the request from the incoming queue.
     *
     * May be not needed if we adjust the algorithm and events will end up in
     * the executor queue directly
     */
    private class Consumer implements Callable<Void> {

        public Void call() throws Exception {
            if (log.isLoggable(Level.FINER)) {
                log.entering(Consumer.class.getName(), "call");
            }

            Request r = null;

            try {
                while (!Thread.currentThread().isInterrupted()) {
                    if (log.isLoggable(Level.FINEST)) {
                        log.log(Level.FINEST, "rq.consumer.queued_requests", queued.size());
                    }

                    try {
                        r = queued.take();
                        
                        List<Need> currentNeeds = r.getCurrentNeeds();
                        listener.fireProcessRequest(r.getServiceName(), currentNeeds);

                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "rq.consumer.process_request",
                                    new Object[]{r.getServiceName(), currentNeeds});
                        }

                        r.execute();

                        if (log.isLoggable(Level.FINER)) {
                            log.log(Level.FINER, "rq.consumer.request_processed",
                                    new Object[]{r.getServiceName(), r.getCurrentNeeds()});
                        }

                        if (r.getNeedProcessors().size() > 0) {
                            addNotFulfilledRequest(r);
                        } else {
                            listener.fireRequestProcessed(r.getServiceName(), r.getCurrentNeeds());
                        }

                        Thread.yield();
                    } catch (InterruptedException e) {
                        /* If we're interrupted, just reevaluate the conditional. */
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "rm.rq.interrupted");
                        }
                        continue;
                    }
                }
            } catch (RuntimeException e) {
                if (log.isLoggable(Level.SEVERE)) {
                    log.log(Level.SEVERE, "rq.unexpected", e);
                }
            }

            if (log.isLoggable(Level.FINER)) {
                log.exiting(Consumer.class.getName(), "call");
            }

            return null;
        }
    }
}
