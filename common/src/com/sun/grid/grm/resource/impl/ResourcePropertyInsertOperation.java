/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.impl;


import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.util.I18NManager;
import java.io.Serializable;

/**
 *
 * Operation that adds new property
 */
public class ResourcePropertyInsertOperation extends ResourcePropertyUpdateOperation implements Serializable {

    private final static String BUNDLE = "com.sun.grid.grm.resource.resource";

    /**
     * The serial version UID
     */
    private static final long serialVersionUID = 2008031001;
    
    /**
     * Public constructor
     * @param key to add
     * @param value that will be set
     */
    public ResourcePropertyInsertOperation(String key, Object value) {
        super(key, value);
    }

    @Override
    public String toString() {
        return I18NManager.printfMessage("ResourcePropertyInsertOperation.toString", BUNDLE, key);
    }

    @Override
    public ResourceChanged execute(Resource resource) throws InvalidResourcePropertiesException {
       if (resource.getProperty(key) != null) {
            throw new InvalidResourcePropertiesException(I18NManager.formatMessage("iro.exists", BUNDLE, key));
        }
        return super.execute(resource);
    }



}
