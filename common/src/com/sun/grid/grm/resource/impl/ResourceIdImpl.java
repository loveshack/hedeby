/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.InvalidResourceIdException;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.util.I18NManager;

/**
 *  Default implementation of a ResourceId.
 * 
 */
public class ResourceIdImpl implements ResourceId {
    
    private final static long serialVersionUID = -2009072101L;
    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    
    private final static String PREFIX = "res#";
    private final long id;
    private transient String strId;
    
    /**
     * Create a new instanceof of <code>ResourceIdImpl</code>
     * @param id the sequence number of the resource id
     */
    public ResourceIdImpl(long id) {
        this.id = id;
    }

    /**
     * Create a new resource id out of a string.
     * @param str the string representation of a resource
     * @throws com.sun.grid.grm.resource.InvalidResourceIdException if the string does not contain a valid resource id
     */
    public ResourceIdImpl(String str) throws InvalidResourceIdException {
        if (str.startsWith(PREFIX)) {
            String idStr = str.substring(PREFIX.length());
            if (idStr.length() == 0) {
                throw new InvalidResourceIdException(str, I18NManager.formatMessage("residimpl.noSeq", BUNDLE));
            }
            try {
                long tmpId = Long.parseLong(idStr);
                if (tmpId <= 0) {
                    throw new InvalidResourceIdException(str, I18NManager.formatMessage("residimpl.nonPosSeq", BUNDLE));
                }
                this.id = tmpId;
            } catch(NumberFormatException ex) {
                throw new InvalidResourceIdException(str, I18NManager.formatMessage("residimpl.invalidSeq", BUNDLE));
            }
        } else {
            throw new InvalidResourceIdException(str, I18NManager.formatMessage("residimpl.invalidPrefix", BUNDLE, PREFIX));
        }
    }

    /**
     * Get the string representation of the resource id
     * @return the string representation (res#%lt;seq number&gt;)
     */
    public String getId() {
        if (strId == null) {
            strId = PREFIX + id;
        }
        return strId;
    }

    /**
     * Get the string representation of the resource id
     * @return the string representation (res#%lt;seq number&gt;)
     */
    @Override
    public String toString() {
        return getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourceIdImpl other = (ResourceIdImpl) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }
    
    /**
     * Compare two resource id
     * @param o the other resource id
     * @return the value 0 if this ResourceId is equal to the argument o; 
     *         a value less than 0 if this ResourceId is less than the argument o; 
     *         and a value greater than 0 if this ResourceId is greater than the argument o.
     */
    public int compareTo(ResourceId o) {
        if (o instanceof ResourceIdImpl) {
            ResourceIdImpl that = (ResourceIdImpl)o;
            return (this.id<that.id ? -1 : (this.id==that.id ? 0 : 1));
        } else {
            return getId().compareTo(o.getId());
        }
    }

}
