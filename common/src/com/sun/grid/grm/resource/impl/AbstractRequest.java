/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.RequestState;
import com.sun.grid.grm.resource.policy.PolicyManager;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.util.I18NManager;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * AbstractRequest implements the common methods of the request interface.
 *
 */
public abstract class AbstractRequest implements Request, Serializable {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.resource.resource";
    private final static long serialVersionUID = -2007112001L;
    private final String serviceName;
    private final String sloName;
    private final List<NeedProcessor> needProcessors;
    private RequestState state;
    
   /**
     * Creates a new AbstractRequest object as a copy of another Request.
     * @param request the object to copy
     */
    public AbstractRequest(Request request) {
        this.serviceName = request.getServiceName();
        this.sloName = request.getSLOName();

        List<Need> currentNeeds = request.getCurrentNeeds();
        ArrayList<NeedProcessor> al = new ArrayList<NeedProcessor>(currentNeeds.size());
        for (Need n : currentNeeds) {
            al.add(new NeedProcessor(n));
        }
        this.needProcessors = Collections.unmodifiableList(al);
    }

    

    /**
     * Creates a new AbstractRequest object as a copy of another Request with
     * normalized urgency value of each need.
     * @param request the object to copy
     * @param policyManager policy manager responsible for normalizing the needs 
     */
    public AbstractRequest(Request request, PolicyManager policyManager) {
        this.serviceName = request.getServiceName();
        this.sloName = request.getSLOName();
        List<Need> currentNeeds = request.getCurrentNeeds();
        ArrayList<NeedProcessor> al = new ArrayList<NeedProcessor>(currentNeeds.size());
        for (Need n : currentNeeds) {
            int usage = Math.round(100 * policyManager.applyPolicies(n, serviceName));
            Need u = new Need(n.getResourceFilter(), new Usage(usage), n.getQuantity());
            al.add(new NeedProcessor(u));
        }
        this.needProcessors = Collections.unmodifiableList(al);
    }
    
    /**
     * Creates a new AbstractRequest object using the ResourceRequestEvent as a template.
     * @param event the resource request event holding the service name and
     * the list of requested needs
     */
    public AbstractRequest(ResourceRequestEvent event) {
        this.serviceName = event.getServiceName();
        this.sloName = event.getSLOName();
        this.needProcessors = new ArrayList<NeedProcessor>(event.getNeeds().size());
        for (Need n : event.getNeeds()) {
            needProcessors.add(new NeedProcessor(n));
        }
    }

    /**
     * Get the need processors for the needs
     * @return list containing a copy of the need processor list
     */
    public List<NeedProcessor> getNeedProcessors() {
        synchronized(needProcessors) {
            return new ArrayList<NeedProcessor>(needProcessors);
        }
    }
    /**
     * Get the current needs of the request
     * @return
     */
    public List<Need> getCurrentNeeds() {
        List<NeedProcessor> nps = getNeedProcessors();
        ArrayList<Need> ret = new ArrayList<Need>(nps.size());
        for(NeedProcessor np: nps) {
            ret.add(np.getCurrentNeed());
        }
        return ret;
    }

    /**
     * Interface to allow the safe modification of the NeedProcessors of the
     * Request.
     * @param <T> return type of the update method
     */
    interface NeedProcessorUpdater<T> {
        /**
         * Perform an update on the NeedProcessor list
         * @param np the NeedProcessor list, can be modified!
         * @return an arbitrary result of type T of the update operation
         */
        public T update(List<NeedProcessor> np);
    }

    /**
     * Method to update the need Processor list with an NeedProcessorUpdater.
     * Takes care of synchronizing the access to needProcessors.
     * 
     * @param <T> the type of the result of the update operation
     * @param updater the updater that performs the update operation
     * @return an arbitrary result of type T of the update operation
     */
    <T> T updateNeedProcessors(NeedProcessorUpdater<T> updater) {
        synchronized(needProcessors) {
            return updater.update(needProcessors);
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean ret = false;

        if (obj instanceof AbstractRequest) {
            if (this.getServiceName() != null) {
                ret = this.getServiceName().equals(((AbstractRequest) obj).getServiceName());
            }
            if (this.getSLOName() != null) {
                ret = ret && this.getSLOName().equals(((AbstractRequest) obj).getSLOName());
            }
        }

        return ret;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = (53 * hash) +
                ((this.getServiceName() != null) ? this.getServiceName().hashCode()
                : 0);
        hash = (53 * hash) +
                ((this.getSLOName() != null) ? this.getSLOName().hashCode() : 0);

        return hash;
    }

    /**
     * Returns a string representation of the object.
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getNeedProcessors().toString());
        sb.append(" " +
                I18NManager.formatMessage("rq.requestfrom", BUNDLE_NAME) + " ");
        sb.append(getServiceName());
        sb.append(", " +
                I18NManager.formatMessage("rq.triggeredby", BUNDLE_NAME) + " ");
        sb.append(getSLOName());

        return sb.toString();
    }

    /**
     * Gets a requesting service name.
     * @return service name
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Gets a name of the slo that triggered the request.
     * @return slo name
     */
    public String getSLOName() {
        return sloName;
    }

    /**
     * Get the state of the request
     * @return the state of the request
     */
    public RequestState getState() {
        return state;
    }

    /**
     * Set the state of the request
     * @param state the new state of the request
     */
    public void setState(RequestState state) {
        this.state = state;
    }
}
