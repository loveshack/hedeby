/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
/**
 * Simple resource store which stores the resource in in-memory map only. No
 * persistence is implemented.
 *
 * Simple resource store is used in ServiceCachingProxy (local proxy of a service)
 * by default.
 *
 */
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStore;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.util.I18NManager;

import com.sun.grid.grm.util.filter.Filter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This class represents the store of Resource objects. It is backed up by
 * a ConcurrentHashMap and the resources are not stored in any persistent repository.
 *
 */
public class SimpleResourceStore implements ResourceStore {
    private static final String BUNDLE = "com.sun.grid.grm.resource.resource";
    private static final Logger log = Logger.getLogger(SimpleResourceStore.class.getName(),
            BUNDLE);
    private Map<ResourceId, Resource> storage;
    private final ReentrantLock resourceLock = new ReentrantLock();

    /**
     * Creates a new instance of ResourceStore
     */
    public SimpleResourceStore() {
        this.storage = new ConcurrentHashMap<ResourceId, Resource>();
    }

    /**
     * Load the resources.
     * 
     */
    public void loadResources() {
        // no action neccessary
    }
    
    /**
     * Gets a list of the resources.
     * @return a list of resources
     */
    public List<Resource> getResources() throws ResourceStoreException {
        resourceLock.lock();
        try {
            return new ArrayList<Resource>(storage.values());
        } catch (Exception e) {
            ResourceStoreException rse = new ResourceStoreException(e.getLocalizedMessage());
            rse.initCause(e);
            throw rse;
        } finally {
            resourceLock.unlock();
        }
    }

    /**
     * Gets a list of the resources matching a filter
     * @return a list of resources
     */
    public List<Resource> getResources(Filter<Resource> filter) throws ResourceStoreException {
        if(filter == null) {
            return getResources();
        } else {
            resourceLock.lock();
            try {
                List<Resource> ret = new LinkedList<Resource>();
                ResourceVariableResolver resolver = new ResourceVariableResolver();
                for(Resource res: storage.values()) {
                    resolver.setResource(res);
                    if(filter.matches(resolver)) {
                        ret.add(res);
                    }
                }
                return ret;
            } catch (Exception e) {
                ResourceStoreException rse = new ResourceStoreException(e.getLocalizedMessage());
                rse.initCause(e);
                throw rse;
            } finally {
                resourceLock.unlock();
            }
        }
    }
    
    /**
     * Gets a list of the names of the resources.
     * @return a set of resource names
     */
    public Set<ResourceId> getResourceIds() throws ResourceStoreException {
        resourceLock.lock();
        try {
            return storage.keySet();
        } catch (Exception e) {
            ResourceStoreException rse = new ResourceStoreException(e.getLocalizedMessage());
            rse.initCause(e);
            throw rse;
        } finally {
            resourceLock.unlock();
        }
    }

    /**
     * This method is a convenience method that is equivalent to
     * put(resource.getId(), resource).
     * @param resource the resource whose id will be added
     * @return the resource whose id was added
     */
    public Resource add(Resource resource) throws ResourceStoreException {
        resourceLock.lock();
        try {
            return storage.put(resource.getId(), resource);
        } catch (Exception e) {
            ResourceStoreException rse = new ResourceStoreException(e.getLocalizedMessage());
            rse.initCause(e);
            throw rse;
        } finally {
            resourceLock.unlock();
        }
    }

    /**
     * Removes the key (and its corresponding value) from this
     * table. This method does nothing if the key is not in the table.
     * If the key was successfully removed from the table, the corresponding
     * spool file will also be deleted.
     *
     * @return the value to which the key had been mapped in this table,
     *          or <tt>null</tt> if the key did not have a mapping.
     * @throws NullPointerException  if the key is
     *               <tt>null</tt>.
     */
    public Resource remove(ResourceId resourceId)
        throws ResourceStoreException, UnknownResourceException {
        Resource retValue;
        resourceLock.lock();
        try {
            retValue = storage.remove(resourceId);
        } catch (Exception e) {
            ResourceStoreException rse = new ResourceStoreException(e.getLocalizedMessage());
            rse.initCause(e);
            throw rse;
        } finally{
            resourceLock.unlock();
        }

        if (retValue == null) {
            UnknownResourceException ure = new UnknownResourceException(I18NManager.formatMessage(
                        "srs.error.unknownresource", BUNDLE, resourceId));

            if (log.isLoggable(Level.FINER)) {
                log.throwing(SimpleResourceStore.class.getName(), "remove", ure);
            }

            throw ure;
        }

        return retValue;
    }

    /**
     * Returns the resource with the specified Id or null if such resource is
     * not stored.
     * @param resourceId the Id of a resource
     * @return the found resource or null
     */
    public Resource getResource(ResourceId resourceId)
        throws ResourceStoreException, UnknownResourceException {
        Resource r = null;
        resourceLock.lock();
        try {
            r = storage.get(resourceId);
        } catch (Exception e) {
            ResourceStoreException rse = new ResourceStoreException(e.getLocalizedMessage());
            rse.initCause(e);
            throw rse;
        } finally {
            resourceLock.unlock();
        }

        if (r == null) {
            UnknownResourceException ure = new UnknownResourceException(I18NManager.formatMessage(
                        "srs.error.unknownresource", BUNDLE, resourceId));

            if (log.isLoggable(Level.FINER)) {
                log.throwing(SimpleResourceStore.class.getName(),
                    "getResource", ure);
            }

            throw ure;
        } else {
            return r;
        }
    }

    /**
     * Removes all resources
     * @throws com.sun.grid.grm.resource.ResourceStoreException when the call fails
     */
    public void clear() throws ResourceStoreException {
        resourceLock.lock();
        try {
            storage.clear();
        } catch (Exception e) {
            ResourceStoreException rse = new ResourceStoreException(e.getLocalizedMessage());
            rse.initCause(e);
            throw rse;
        } finally {
            resourceLock.unlock();
        }
    }

}
