/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.util.Hostname;

/**
 *  The id of a host resource.
 * 
 *  The equals, compare and hashcode method used the Hostname class.
 *  
 *  @see com.sun.grid.grm.util.Hostname
 *  @deprecated class required to read old systems spooled files. Used just for
 *              upgrade purposes
 */

@Deprecated
public class HostResourceIdImpl extends AbstractResourceId {
    
    private final static long serialVersionUID = -2008020601L;
    
    private final Hostname hostname;
    
    /**
     * Create a new instance.
     * @param hostname the hostname
     * @throws java.lang.NullPointerException if hostname is null
     */
    public HostResourceIdImpl(String hostname) {
        this(Hostname.getInstance(hostname));
    }
    
    /**
     * Create a new instance.
     * @param hostname the hostname
     * @throws java.lang.NullPointerException if hostname is null
     */
    public HostResourceIdImpl(Hostname hostname) {
        if(hostname == null) {
            throw new NullPointerException("hostname must not be null");
        }
        this.hostname = hostname;
    }

    /**
     * Get the id string
     * @return the id string
     */
    public String getId() {
        return hostname.getHostname();
    }

    /**
     * Compare this resource id with o
     * @param o  the other resource id
     * @return the compare result
     */
    public int compareTo(ResourceId o) {
        int ret = -1;
        if(o instanceof HostResourceIdImpl) {
            HostResourceIdImpl that = (HostResourceIdImpl)o;
            ret = hostname.compareTo(that.hostname);
        } else if (o != null) {
            ret = getId().compareTo(o.getId());
        } else {
            ret = -1;
        }
        return ret;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof HostResourceIdImpl) {
            HostResourceIdImpl that = (HostResourceIdImpl)o;
            return hostname.equals(that.hostname);
        } else if (o instanceof ResourceId) {
            return getId().equals(((ResourceId)o).getId());
        } else {
            return false;
        }
    } 

    @Override
    public String toString() {
        return hostname.getHostname();
    }

    /**
     * Get the hostname 
     * @return the hostname
     */
    public Hostname getHostname() {
        return hostname;
    }
}
