/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.impl;

import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Abstract base class for <tt>ResourceChanged</tt> classes.
 * 
 * @param <T> the type of the value of the change
 */
public abstract class AbstractResourceChanged<T> implements ResourceChanged<T> {

    private final String name;
    private final T value;

    /**
     * The serial version UID
     */
    private static final long serialVersionUID = 2008031001;

    /**
     * Creates new ResourceChanged object
     * @param name of the changed property
     * @param value new value
     */
    public AbstractResourceChanged(String name, T value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Gets the property name
     * @return property name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the new property value
     * @return property value
     */
    public T getValue() {
        return this.value;
    }

    /**
     * Has this change impact on the resource
     * @return <tt>true</tt> if this change has an impact on the resource
     */
    public boolean hasChangedResource() {
        return true;
    }

    /**
     * This method is doing a merge between 2 maps that contain old and new properties
     * and returns collection of ResourceChanged objects
     * @param oldProperties old resource properties
     * @param newProperties current resource properties
     * @return collection of ResourceChanged objects
     */
    public static Collection<ResourceChanged> getChanges(Map<String, Object> oldProperties, Map<String, Object> newProperties) {
        Collection<ResourceChanged> patch = new HashSet<ResourceChanged>(oldProperties.size()+newProperties.size());

        for (Map.Entry<String, Object> entry : oldProperties.entrySet()) {
            if (newProperties.containsKey(entry.getKey())) {
                if (!entry.getValue().equals(newProperties.get(entry.getKey()))) {
                    ResourcePropertyUpdated update = new ResourcePropertyUpdated(entry.getKey(), newProperties.get(entry.getKey()), entry.getValue());
                    patch.add(update);
                }
            } else {
                ResourcePropertyDeleted delete = new ResourcePropertyDeleted(entry.getKey(), entry.getValue());
                patch.add(delete);
            }
        }
        for (Map.Entry<String, Object> entry : newProperties.entrySet()) {
            if (!oldProperties.containsKey(entry.getKey())) {
                ResourcePropertyInserted insert = new ResourcePropertyInserted(entry.getKey(), entry.getValue());
                patch.add(insert);
            }
        }
        return patch;
    }

    /**
     * This method is doing a merging between 2 resources
     * and returns collection of ResourceChanged objects
     * @param old resource
     * @param newOne resource
     * @return collection of ResourceChanged objects
     */
    public static Collection<ResourceChanged> getChanges(Resource old, Resource newOne) {
        Collection<ResourceChanged> patch = new HashSet<ResourceChanged>();
        patch = getChanges(old.getProperties(), newOne.getProperties());
        if (old.isAmbiguous() != newOne.isAmbiguous()) {
            ResourceAmbiguousStateChanged ambiguous = new ResourceAmbiguousStateChanged(newOne.isAmbiguous());
            patch.add(ambiguous);
        }
        if (!old.getUsage().equals(newOne.getUsage())) {
            ResourceUsageChanged usage = new ResourceUsageChanged(old.getUsage(), newOne.getUsage());
            patch.add(usage);
        }
        boolean annotationChanged = false;
        if (old.getAnnotation() == null) {
            annotationChanged = newOne.getAnnotation() != null;
        } else {
            annotationChanged = !old.getAnnotation().equals(newOne.getAnnotation());
        }
        if (annotationChanged) {
            ResourceAnnotationChanged anno = new ResourceAnnotationChanged(old.getAnnotation(), newOne.getAnnotation());
            patch.add(anno);
        }
        return patch;
    }

    /**
     * Create ResourceChangeOperation object out of collection of ResourceChanged objects
     * @param changes that were made on resource properties
     * @return collection of ResourceChangeOperation objects
     */
    public static Collection<ResourceChangeOperation> createOperation(Collection<ResourceChanged> changes) {
        List<ResourceChangeOperation> ret = new ArrayList<ResourceChangeOperation>();
        for(ResourceChanged change: changes) {
            if(change.hasChangedResource()) {
                ret.add(change.createOperation());
            }
        }
        return ret;
    }

    /**
     * Creates a ResourceChanged object that sets the value of the property aName
     * to aValue.
     * <p>
     * A aValue of null removes the property.
     * <p>
     * Returns a ResourceChanged object that represents whether the property was
     * inserted, updated or deleted. If no modification was done, this method
     * returns null.
     *
     * @param props   the resource properties, will not be changed
     * @param aName   name of the property to change
     * @param newValue  new value of the property
     * @return ResourceChanged object or null if the resource property has not been modified
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if <code>value</code> is not
     *                 valid value for this resource property
     */
    public static ResourceChanged getChange(Map<String, Object> props, String aName, Object newValue) throws InvalidResourcePropertiesException {
        Object oldValue = props.get(aName);
        if (newValue == null) {
            // delete property
            if (oldValue == null) {
                return null;
            } else {
                return new ResourcePropertyDeleted(aName, oldValue);
            }
        } else {
            // set property
            if (newValue.equals(oldValue)) {
                return null;
            }

            if (oldValue == null) {
                // no old value -> insert
                return new ResourcePropertyInserted(aName, newValue);
            } else {
                return new ResourcePropertyUpdated(aName, newValue, oldValue);
            }
        }
    }

}
