/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.bootstrap.FactoryWithParam;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;

/**
 * Factory class for ResourceRemovedOPRs
 */
public final class ResourceRemovedOPRFactory implements FactoryWithParam<RAOperationResult, Resource> {

    private final ResourceRemovalDescriptor descr;
    private final String annotation;

    /**
     * Create a new instance of ResourceRemovedOPRFactory
     * @param descr        the resource removal descriptor
     * @param annotation   the annotation for the resource
     */
    public ResourceRemovedOPRFactory(ResourceRemovalDescriptor descr, String annotation) {
        super();
        this.descr = descr;
        this.annotation = annotation;
    }

    /**
     * Create the OPR
     * @param resource the resource to which the OPR belongs
     * @return the OPR
     */
    public RAOperationResult newInstance(Resource resource) {
        resource.setState(Resource.State.UNASSIGNED);
        resource.setAnnotation(annotation);
        return new ResourceRemovedOPR(resource.clone(), resource.getAnnotation(), descr);
    }
}
