/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.impl.*;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import java.io.File;

/**
 *  Default implementation of a file based ResourceAdapterStore. 
 * 
 *  <p>This class stores instances of Resource into the file system via a
 *     FileResourceStore</p>
 * 
 * @see FileResourceStore
 */
public class DefaultResourceAdapterFileStore extends AbstractResourceAdapterFileStore<ResourceId, DefaultResourceAdapter,Resource> {

    /**
     * Create a new DefaultResourceAdapterFileStore
     * @param spoolDir the spooling directory
     */
    public DefaultResourceAdapterFileStore(File spoolDir) {
        super(new FileResourceStore(spoolDir));
    }
    
    /**
     * Creates a DefaultResourceAdpater containing the resource
     * @param elem  the resource
     * @return the DefaultResourceAdpater
     */
    @Override
    protected DefaultResourceAdapter createResourceAdapterForElement(Resource elem) {
        return new DefaultResourceAdapter(elem);
    }

    /**
     * Get the resource from the adapter
     * @param adapter the adapter
     * @return the resource
     */
    @Override
    protected Resource getElementFromResourceAdapter(DefaultResourceAdapter adapter) {
        return adapter.getResource();
    }

    /**
     * Creates a DefaultResourceAdpater containing the resource
     * @param resource  the resource
     * @return the DefaultResourceAdpater
     */
    public DefaultResourceAdapter createResourceAdapter(Resource resource) {
        return new DefaultResourceAdapter(resource);
    }

}
