/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.adapter.*;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.util.I18NManager;
import java.util.LinkedList;
import java.util.List;

/**
 *  Container for RAOperationResults
 */
public class ListOPR implements RAOperationResult {

    private final static String BUNDLE = "com.sun.grid.grm.resource.adapter.impl.resource_adapter_impl";

    private final List<RAOperationResult> results = new LinkedList<RAOperationResult>();
    private boolean hasModifiedResource;
    
    /**
     * Add a new result
     * @param result the result
     */
    public void add(RAOperationResult result) {
        results.add(result);
        hasModifiedResource |= result.hasModifiedResource();
    }

    /**
     * Is there any result stored?
     * @return <code>true</code> if no result is stored
     */
    public boolean isEmpty() {
        return results.isEmpty();
    }
    
    /**
     * Has this result modified the resource.
     * 
     * @return <code>true</code> if any sub result has modified the resource
     */
    public boolean hasModifiedResource() {
        return hasModifiedResource;
    }
    
    /**
     * Call fireEvents of all sub results.
     * @param evtSupport the event support
     */
    public void fireEvents(ServiceEventSupport evtSupport) {
        for(RAOperationResult result: results) {
            if(result.hasModifiedResource()) {
                result.fireEvents(evtSupport);
            }
        }
    }

    /**
     * 
     * @param resultType the type of the RAOperationResult
     * @return <code>true</code> if at least on sub result contains the type
     */
    public boolean contains(Class<? extends RAOperationResult> resultType) {
        for(RAOperationResult result: results) {
            if(resultType.isAssignableFrom(result.getClass())) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Returns a contained OPR of a specific type or null.
     * @param <T> the type of the OPR
     * @param oprType the OPR class to look for
     * @return  the contain OPR or <tt>null</tt>
     */
    @SuppressWarnings("unchecked")
    public <T extends RAOperationResult> T getByType(Class<T> oprType) {
        for(RAOperationResult result: results) {
            if(oprType.isAssignableFrom(result.getClass())) {
                return (T) result;
            }
        }
        return null;
    }
    

    @Override
    public String toString() {
        return I18NManager.printfMessage("ListOPR.toString", BUNDLE, results.toString());
    }

}
