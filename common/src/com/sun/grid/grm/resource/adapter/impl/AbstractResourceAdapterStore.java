/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.resource.adapter.ResourceAdapterStore;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

/**
 * Abstract base class for ResourceAdapterStore implementation.
 * 
 * <h2>Caching</h2>
 * 
 * <p>This class provides caches for all available resource adapter. The get...
 *    methods retrieves the desired information from the caches.</p>
 * 
 * <h2>Mapping between ResourceAdpater and persisted objects</h2>
 * <p>
 *    This class provides a mapping between the ResourceAdapter and the objects
 *    stored in the storage.Whenever a object is loaded from the storage the method 
 *    <code>createResourceAdapterForElement</code> is called to map the object
 *    into a ResourceAdapter. </p>
 * <p>To store a the content of a ResourceAdapter the method
 *    <code>getElementFromResourceAdapter</code> is called to get the
 *    object which will be made persistent. 
 * </p>
 *
 * <p>This class does make implement any persistence mechanism. Subclasses must 
 *    provide the methods <code>loadElements</code>, <code>storeElement</code> 
 *    and <code>removeElement</code> to manage the persistence.</p>
 * 
 * @param <RA> the type of the resource adapter
 * @param <E>  the type of the stored element
 */
public abstract class AbstractResourceAdapterStore<K, RA extends ResourceAdapter<K>,E> implements ResourceAdapterStore<K,RA> {

    private final static String BUNDLE = "com.sun.grid.grm.resource.adapter.impl.resource_adapter_impl";
    private final static  Logger log = Logger.getLogger(AbstractResourceAdapterStore.class.getName(), BUNDLE);
    
    private final Map<ResourceId,RA> resourceAdapterMap = new HashMap<ResourceId,RA>();
    private final Map<K,RA> resourceAdapterKeyMap = new HashMap<K,RA>();
    private final Map<ResourceId,Resource> resourceMap = new HashMap<ResourceId,Resource>();
    private final Lock lock = new ReentrantLock();
    
    protected AbstractResourceAdapterStore() {
    }
    
    /**
     * Load all persistent elements
     * @return set of elements
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the elements could not be loaded
     */
    protected abstract Set<E> loadElements() throws ResourceStoreException;
    
    /**
     * Store an element in the persistent storage.
     * @param elem the element
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the element could not be stored
     */
    protected abstract void storeElement(E elem) throws ResourceStoreException;
    
    /**
     * Remove an element from the persitent storage
     * @param elem the element
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the element could not be removed
     */
    protected abstract void removeElement(E elem) throws ResourceStoreException;
    
    /**
     * Create the ResourceAdapter for an element.
     * @param elem  the element
     * @return the ResourceAdapter
     */
    protected abstract RA createResourceAdapterForElement(E elem);
    
    /**
     * Get the element from an ResourceAdapter
     * @param ra  the ResourceAdapter
     * @return the element
     */
    protected abstract E getElementFromResourceAdapter(RA ra);
    
    
    /**
     * Load all ResourceAdapters from the persistent storage
     * 
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the ResourceAdapters could not be loaded
     */
    public final void load() throws ResourceStoreException {
        lock.lock();
        try {
            resourceAdapterMap.clear();
            for(E elem: loadElements()) {
                RA ra = createResourceAdapterForElement(elem);
                resourceAdapterMap.put(ra.getId(), ra);
                resourceMap.put(ra.getId(), ra.getResource());
                resourceAdapterKeyMap.put(ra.getKey(), ra);
            }
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * Get the ResourceAdapter for a resource
     * @param resourceId the id of the resource
     * @return the ResourceAdapater
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is not stored
     */
    public final RA get(ResourceId resourceId) throws UnknownResourceException {
        RA rc = null;
        lock.lock();
        try {
            rc = resourceAdapterMap.get(resourceId);
        } finally {
            lock.unlock();
        }
        if (rc == null) {
            throw new UnknownResourceException("aras.ex.unknownRes", BUNDLE, resourceId);
        }
        return rc;
    }

    /**
     * Get the ResourceAdapter for a Resource
     * @param key  the key of the resource
     * @return  the ResourceAdapter
     * @throws com.sun.grid.grm.resource.ResourceStoreException  if the ResourceAdapter could not be loaded
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is not stored
     */
    public RA getByKey(K key)  throws ResourceStoreException, UnknownResourceException {
        RA rc = null;
        lock.lock();
        try {
            rc = resourceAdapterKeyMap.get(key);
        } finally {
            lock.unlock();
        }
        if (rc == null) {
            throw new UnknownResourceException("aras.ex.unknownRes", BUNDLE, key);
        }
        return rc;
    }


    /**
     * Get all resource ids known by this ResourceAdapterStore
     * @return set of resource ids
     */
    public final Set<ResourceId> getResourceIds() {
        lock.lock();
        try {
            return new HashSet<ResourceId>(resourceAdapterMap.keySet());
        } finally {
            lock.unlock();
        }
    }

    /**
     * Get all resources known by this ResourceAdapterStore
     * @return set of resources
     */
    public final Set<Resource> getResources() {
        lock.lock();
        try {
            return new HashSet<Resource>(resourceMap.values());
        } finally {
            lock.unlock();
        }
    }

    /**
     * Get all ResourceAdapters known by this ResourceAdapterStore
     * @return set of resources
     */
    public final Set<RA> getResourceAdapters() {
        lock.lock();
        try {
            return new HashSet<RA>(resourceAdapterMap.values());
        } finally {
            lock.unlock();
        }
    }

    /**
     * Store the content of a ResourceAdapter.
     * 
     * <p>Performs the ResourceAdapter to element mapping and calls
     *    <code>storeElement</code>.</p>
     * @param ra  the ResourceAdapter
     * @throws com.sun.grid.grm.resource.ResourceStoreException if element could not be stored
     */
    public final void store(RA ra) throws ResourceStoreException {
        lock.lock();
        try {
            storeElement(getElementFromResourceAdapter(ra));
            resourceAdapterMap.put(ra.getId(), ra);
            resourceAdapterKeyMap.put(ra.getKey(), ra);
            resourceMap.put(ra.getId(), ra.getResource());
        } finally {
            lock.unlock();
        }
    }

    /**
     * Remove a resource from the storage
     * 
     * @param resourceId the id of the resource
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the element could not be removed
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is not 
     *               stored in this ResourceAdapterStore
     */
    public final RA remove(ResourceId resourceId, ResourceRemovalDescriptor descr) throws ResourceStoreException, UnknownResourceException {
        lock.lock();
        try {
            RA ra = get(resourceId);
            removeElement(getElementFromResourceAdapter(ra));
            resourceAdapterMap.remove(resourceId);
            resourceAdapterKeyMap.remove(ra.getKey());
            resourceMap.remove(resourceId);
            return ra;
        } finally {
            lock.unlock();
        }
    }

    /**
     * Get the number of resources stored in the ResourceAdapterStore
     * @return the number of resources
     */
    public final int size() {
        lock.lock();
        try {
            return resourceAdapterMap.size();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Get the resource adapters that can be freed. This is called when service
     * is going down with free resources option.
     * @return set of all non-static resource adapters
     */
    public Set<RA> getResourceAdaptersToFree() {
        lock.lock();
        try {
            Set<RA> ret = new HashSet<RA>();
            for (RA ra: resourceAdapterMap.values()) {
                if (!ra.isStatic()) {
                    ret.add(ra);
                }
            }
            return ret;
        } finally {
            lock.unlock();
        }
    }

}
