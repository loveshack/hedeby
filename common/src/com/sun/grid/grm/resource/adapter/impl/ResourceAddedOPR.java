/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.util.I18NManager;

/**
 * This RAOperationResult signalizes that the resource has been
 * successfully assigned to the service.
 */
public class ResourceAddedOPR extends AbstractSimpleOPR {

    private final static String BUNDLE = "com.sun.grid.grm.resource.adapter.impl.resource_adapter_impl";

    /**
     * Create a new instance of ResourceAddedOPR
     * @param resource the resource
     * @param message the change message
     */
    public ResourceAddedOPR(Resource resource, String message) {
        super(resource, message);
    }

    /**
     * Fires the RESOURCE_ADDED event
     * @param evtSupport the event support
     */
    public void fireEvents(ServiceEventSupport evtSupport) {
        evtSupport.fireResourceAdded(getResource(), getMessage());
    }

    /**
     * The ResourceAddedOPR has modified the resource
     * @return <code>true</code>
     */
    public boolean hasModifiedResource() {
        return true;
    }
    
    @Override
    public String toString() {
        return I18NManager.printfMessage("ResourceAddedOPR.toString", BUNDLE, getResource(), getMessage());
    }
    
}
