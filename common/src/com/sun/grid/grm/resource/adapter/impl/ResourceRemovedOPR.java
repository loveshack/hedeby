/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.event.ServiceEventSupport;
import com.sun.grid.grm.util.I18NManager;

/**
 * This RAOperationResult signalizes that the resource transition has 
 * removed the resource from the service.
 */
public class ResourceRemovedOPR extends AbstractResourceRemoveOPR {

    private final static String BUNDLE = "com.sun.grid.grm.resource.adapter.impl.resource_adapter_impl";

    /**
     * Create a new instance of ResourceRemovedOPR
     * @param resource the resource
     * @param message the change message
     * @param descr   the removal descriptor
     */
    public ResourceRemovedOPR(Resource resource, String message, ResourceRemovalDescriptor descr) {
        super(resource, message, descr);
    }

    /**
     * Fires the RESOURCE_REMOVED event
     * @param evtSupport the event support
     */
    public void fireEvents(ServiceEventSupport evtSupport) {
        evtSupport.fireResourceRemoved(getResource(), getMessage());
    }

    /**
     * such a transition changed the resource.
     * @return <code>true</code>
     */
    public boolean hasModifiedResource() {
        return true;
    }
    
    @Override
    public String toString() {
        return I18NManager.printfMessage("ResourceRemovedOPR.toString", BUNDLE, getResource(), getDescr(), getMessage());
    }
}
