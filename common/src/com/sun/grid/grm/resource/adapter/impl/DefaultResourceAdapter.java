/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.adapter.InvalidResourceStateException;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Usage;

/**
 *  Default implementation of a ResourceAdapter.
 * 
 *  <p>It does not perform any specific action, it sets only the
 *     resource into the correct states if a resource transition is
 *     triggered.</p>
 * 
 * <p>This class is thread save.</p>
 */
public class DefaultResourceAdapter extends AbstractResourceAdapter<ResourceId> {

    /**
     * Create a new instance of DefaultResourceAdapter
     * @param resource the resource represented by this ResourceAdpater
     */
    public DefaultResourceAdapter(Resource resource) {
        super(resource);
    }

    /**
     * get the key of the resource
     *
     * No lock necessary as the id is immutable.
     * 
     * @return the key of the resource
     */
    public ResourceId getKey() {
        return resource.getId();
    }



    /**
     * Prepare the installation.
     * 
     * <p>Sets the resource state to ASSIGNED<p/>
     * @return an AddResourceOPR
     * @throws InvalidResourceStateException if the resource can not be installed
     */
    protected RAOperationResult doPrepareInstall() throws InvalidResourceStateException {
        resourceStateLock.lock();
        try {
            resource.setState(Resource.State.ASSIGNING);
            resource.setAnnotation("");
            // We set the usage to the maximum, the next
            // SLO calculation will override it
            resource.setUsage(Usage.MAX_VALUE);
            
            return new AddResourceOPR(resource.clone(), resource.getAnnotation());
        } finally {
            resourceStateLock.unlock();
        }
    }

    /**
     * Prepare the uninstallation.
     * 
     * <p>Sets the resource into UNASSIGNING</p>
     * @param descr The descriptor of the resource remove operation
     * @return an RemoveResourceOPR
     * @throws InvalidResourceStateException if the resource can not be uninstalled
     */
    protected RAOperationResult doPrepareUninstall(ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
        resourceStateLock.lock();
        try {
            resource.setState(Resource.State.UNASSIGNING);
            resource.setAnnotation("");
            return new RemoveResourceOPR(resource.clone(), resource.getAnnotation(), descr);
        } finally {
            resourceStateLock.unlock();
        }
    }

    /**
     * Prepare the reset.
     * 
     * <p>Sets the resource into ASSIGNING state.</p>
     * @return an AddResourceOPR
     */
    protected RAOperationResult doPrepareReset() {
        resourceStateLock.lock();
        try {
            resource.setState(Resource.State.ASSIGNING);
            resource.setAnnotation("");
            return new AddResourceOPR(resource.clone(), resource.getAnnotation());
        } finally {
            resourceStateLock.unlock();
        }
    }

    /**
     * Install the resource
     * 
     * <p>Set the resources into ASSIGNED state.</p>
     * @return an ResourceAddedOPR
     */
    protected RAOperationResult doInstall() {
        resourceStateLock.lock();
        try {
            resource.setState(Resource.State.ASSIGNED);
            resource.setAnnotation("");
            return new ResourceAddedOPR(resource.clone(), resource.getAnnotation());
        } finally {
            resourceStateLock.unlock();
        }
    }

    /**
     * Uninstall a resource
     * 
     * <p>Sets the resource into UNASSIGNED state.</p>
     * @param descr The descriptor of the resource remove operation
     * @return a ResourceRemovedOPR
     */
    protected RAOperationResult doUninstall(ResourceRemovalDescriptor descr) {
        resourceStateLock.lock();
        try {
            resource.setState(Resource.State.UNASSIGNED);
            resource.setAnnotation("");
            return new ResourceRemovedOPR(resource.clone(), resource.getAnnotation(), descr);
        } finally {
            resourceStateLock.unlock();
        }
    }

    /**
     * Reset the resource.
     * 
     * <p>Sets the resource into ASSIGNED state.</p>
     * @return a ResourceResetOPR
     */
    protected RAOperationResult doReset() {
        resourceStateLock.lock();
        try {
            resource.setState(Resource.State.ASSIGNED);
            resource.setAnnotation("");
            return new ResourceResetOPR(resource.clone(), resource.getAnnotation());
        } finally {
            resourceStateLock.unlock();
        }
    }
}
