/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.adapter;

import com.sun.grid.grm.service.event.ServiceEventSupport;

/**
 *  This class represents the resource of a RAOperation.
 */
public interface RAOperationResult {

    /**
     * Fire all service events which have been triggered by the RAOperation
     * @param evtSupport the ServiceEventSupport
     */
    public void fireEvents(ServiceEventSupport evtSupport);

    /**
     * Determine if the result is or contains a result of a specific type
     * @param resultType the result type
     * @return true if the result is or contains a result of the type
     */
    public boolean contains(Class<? extends RAOperationResult> resultType);
    
    /**
     * Get a result by it's type
     * @param <T> the type of the result
     * @param resultType the resolut class
     * @return  the result or <tt>null</tt>
     */
    public <T extends RAOperationResult> T getByType(Class<T> resultType);
    
    /**
     * Determine if the RAOperation has modified the resource
     * @return true if the resource has been modified
     */
    public boolean hasModifiedResource();
    
}
