/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.impl.*;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.resource.ResourceStoreException;
import java.util.Set;

/**
 * Abstract base class for a file based ResourceAdapterStore.
 * 
 * <p>
 * This class uses a AbstractFileStore to manage persistence.
 * </p>
 * 
 * @param <RA>  The type of the supported ResourceAdapter
 * @param <E>   The tpye of the element which will be stored in the file system.
 */
public abstract class AbstractResourceAdapterFileStore<K,RA extends ResourceAdapter<K>,E> extends AbstractResourceAdapterStore<K,RA,E> {

    private final AbstractFileStore<E,ResourceStoreException> storage;
    
    /**
     * Create a new instance of of AbstractResourceAdapterFileStore
     * @param storage the AbstractFileStore which is used for persistence
     */
    protected AbstractResourceAdapterFileStore(AbstractFileStore<E,ResourceStoreException> storage) {
        this.storage = storage;
    }

    /**
     * Load all elements from the AbstractFileStore
     * @return all elements
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the elements could not be loaded
     */
    @Override
    protected Set<E> loadElements() throws ResourceStoreException {
        return storage.load();
    }

    /**
     * Remove an elements from the AbstractFileStore
     * @param elem the element
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the element could not be removed
     */
    @Override
    protected void removeElement(E elem) throws ResourceStoreException {
        storage.delete(elem);
    }

    /**
     * Store an element in the AbstractFileStore
     * @param elem the element
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the element could not be stored
     */
    @Override
    protected void storeElement(E elem) throws ResourceStoreException {
        storage.write(elem);
    }

}
