/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter;

import com.sun.grid.grm.resource.*;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import java.util.Set;

/**
 * Generic interface for the implementation of a storage  which has the
 * task to make a Resource represented by a ResourceAdapter persistent.
 * 
 * @param <K> The type of the key that identifies a resource from the service view
 *            (e.g. for a host resource consuming resource the type is the Hostname)
 * @param <RA> The type of the ResourceAdapter
 */
public interface ResourceAdapterStore<K, RA extends ResourceAdapter<K>> {

    /**
     * Create a ResourceAdapter for a Resource.
     * 
     * <p>This method is used whenever a new resource is assigned to a Service and
     *    a ResourceAdapter for this resource must be created.</p>
     * 
     * @param resource the resource
     * @return the ResourceAdapter which represents the Resource
     * @throws com.sun.grid.grm.resource.InvalidResourceException if the Resource can not be handled
     *            by this ResourceAdapterStore
     */
    public RA createResourceAdapter(Resource resource) throws InvalidResourceException;
    
    
    /**
     * Load all ResourceAdapter from the persistent storage
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the ResourceAdapter could not be loaded
     */
    public void load() throws ResourceStoreException;
    
    /**
     * Make the content of the Resource represented by <code>ra</code> persistent.
     * @param ra the ResourceAdapter
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the Resource could not be stored
     */
    public void store(RA ra) throws ResourceStoreException;
    
    /**
     * Remove a resource from the storage
     * @param resourceId  the id of the resource
     * @param descr       the removal descriptor
     * @return the adapter of the removed resource
     * @throws com.sun.grid.grm.resource.ResourceStoreException if the Resource could not be removed
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the Resource is not stored
     */
    public RA remove(ResourceId resourceId, ResourceRemovalDescriptor descr) throws ResourceStoreException, UnknownResourceException;

    /**
     * Get the ResourceAdapter for a Resource
     * @param resourceId  the id of the Resource
     * @return  the ResourceAdapter
     * @throws com.sun.grid.grm.resource.ResourceStoreException  if the ResourceAdapter could not be loaded
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is not stored
     */
    public RA get(ResourceId resourceId) throws ResourceStoreException, UnknownResourceException;

    /**
     * Get the ResourceAdapter for a Resource
     * @param key  the key of the resource
     * @return  the ResourceAdapter
     * @throws com.sun.grid.grm.resource.ResourceStoreException  if the ResourceAdapter could not be loaded
     * @throws com.sun.grid.grm.resource.UnknownResourceException if the resource is not stored
     */
    public RA getByKey(K key)  throws ResourceStoreException, UnknownResourceException;
    
    /**
     * Get the ids of all stored Resources.
     * @return set of resource ids
     * @throws com.sun.grid.grm.resource.ResourceStoreException on any storage problem
     */
    public Set<ResourceId> getResourceIds() throws ResourceStoreException;
    
    /**
     * Get the resource adapters that can be freed (when service is going down
     * with free resources option).
     * @return set of resource adapters
     */
    public Set<RA> getResourceAdaptersToFree();    
    
    /**
     * Get all stored Resources
     * @return set of Resources
     * @throws com.sun.grid.grm.resource.ResourceStoreException
     */
    public Set<Resource>  getResources() throws ResourceStoreException;
    
    /**
     * Get all stored ResourceAdapters.
     * @return Set of ResourceAdapters
     * @throws com.sun.grid.grm.resource.ResourceStoreException on any storage exception
     */
    public Set<RA> getResourceAdapters() throws ResourceStoreException;
    
    /**
     * Get the number of stored resources:
     * @return the number of stored resources
     */
    public int size();    
    
}
