/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter;

import java.util.Set;

/**
 * This object represents the result of a merge between the resources
 * stored in a ResourceAdapterStore and a Resource set.
 * 
 * @param <RA> the type of the resource adapter
 */
public class MergeResult<K, RA extends ResourceAdapter<K>> {

    private final Set<RA> added;
    private final Set<RA> removed;
   
    /**
     * Create a new instance of MergeResult
     * @param added    set of ResourceAdapters which has been added to the ResourceAdapterStore
     * @param removed  Set of ResourceAdapters which has been removed from the ResourceAdapterStore
     */
    public MergeResult(Set<RA> added, Set<RA> removed) {
        this.added = added;
        this.removed = removed;
    }

    /**
     * Get the set of ResourceAdapters which has been added to the ResourceAdapterStore
     * @return the set of ResourceAdapters
     */
    public Set<RA> getAdded() {
        return added;
    }

    /**
     * Get the set of ResourceAdapters which has been removed to the ResourceAdapterStore
     * @return the set of ResourceAdapters
     */
    public Set<RA> getRemoved() {
        return removed;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[MergeResult: added=");
        sb.append(added.toString());
        sb.append(",removed=");
        sb.append(removed.toString());
        sb.append("]");
        return sb.toString();
    }
    
}
