/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter;

import com.sun.grid.grm.resource.*;
import com.sun.grid.grm.resource.adapter.impl.AddResourceOPR;
import com.sun.grid.grm.resource.adapter.impl.RemoveResourceOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceAddedOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceChangedOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceErrorOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceRejectedOPR;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import java.util.Collection;

/**
 * A ResourceAdapter gives a ServiceAdapter the possibilities to bring
 * service specific resources into the system.
 * 
 * <h2>Multithreading</h2>
 * 
 * <p>All implementations of ResourceAdpater must be thread save. Normally only one
 * installation/uninstallation is executed at a time, however the reset can
 * always be invoked concurrently.<br>
 * </p>
 * 
 * @param <K> The type of the key that identifies a resource from the service view
 *            (e.g. for a host resource consuming resource the type is the Hostname)
 */
public interface ResourceAdapter<K> {

    /**
     * Get the id of the resource which is represented by the 
     * ResourceAdapter
     * @return the id of the resource
     */
    public ResourceId getId();

    /**
     * Get the key of the resource that identifies the resource from the
     * service view
     * @return the key of the resource
     */
    public K getKey();

    /**
     * Get a clone of Resource which is represented by this ResourceAdapter
     * @return a clone of the Resource
     */
    public Resource getResource();
    
    /**
     * Is the resource behind this ResourceAdapter static
     * @return true if the Resource behind this ResourceAdapter is static
     */
    public boolean isStatic();

    /**
     * Modify a resource. This method will not change the
     * state of the resource.
     *
     * <p>If the returned list of changes is not empty the ServiceAdapter will
     *    spool the resource and fire and ResourceChanged event.</p>
     *
     * @param operation  the resource operation
     * @return the list of changes
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation would create an invalid resource
     */
    public ResourceChanged modifyResource(ResourceChangeOperation operation) throws InvalidResourcePropertiesException;
    
    /**
     * Modify a resource. This method will not change the
     * state of the resource.
     * 
     * <p>If the returned list of changes is not empty the ServiceAdapter will
     *    spool the resource and fire and ResourceChanged event.</p>
     * 
     * @param operation  the resource operation
     * @return the list of changes
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation would create an invalid resource
     */
    public Collection<ResourceChanged> modifyResource(Collection<ResourceChangeOperation> operation) throws InvalidResourcePropertiesException;
    
    /**
     * Prepare the resource for installation.
     * 
     * <p>The result may contain the following RAOperationResults</p>
     * 
     * <table>
     *    <tr><td>AddResourceOPR</td>
     *        <td>If the resource has been prepared for installation. The ServiceAdapter will send
     *            the ADD_RESOURCE event.
     *            The state of the resource has been set to ASSIGNING</td>
     *    </tr>
     *    <tr><td>ResourceChangedOPR</td>
     *        <td>If the properties of the resource has been changed. The ServiceAdapter will send
     *            the RESOURCE_CHANGED events</td>
     *    </tr>
     * </table>
     * 
     * <p><b>Note:</b> The ServiceAdapter calls this method synchronous. This should only do
     *    the minimum effort to ensure that the Resource can be installed.</p>
     * 
     * @see AddResourceOPR
     * @see ResourceChangedOPR
     * @return the transaction which describes the changes on the resource
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource can not be installed
     */
    public ResourceTransition prepareInstall() throws InvalidResourceStateException;
    
    /**
     * Perform the installation procedure for the resource.
     * 
     * <p>The result may contain the following RAOperationResults</p>
     * 
     * <table>
     *    <tr><td>ResourceAddedOPR</td>
     *        <td>The resources has been successfully installed. The ServiceAdapter will send
     *            the RESOURCE_ADDED event. 
     *            The state of the resource has been set to ASSIGNED</td>
     *    </tr>
     *    <tr><td>ResourceChangedOPR</td>
     *        <td>If the properties of the resource has been changed. The ServiceAdapter will send
     *            the RESOURCE_CHANGED events</td>
     *    </tr>
     *    <tr><td>ResourceRejectedOPR</td>
     *        <td>If the resource has not been accepted. The ServiceAdapter will send the
     *            RESOURCE_REJECTED event and remove the resource from the ResourceAdapterStore.
     *            The state of the resource has been set to  RESOURCE_REMOVED.</td>
     *    </tr>
     *    <tr><td>ResourceErrorOPR</td>
     *        <td>The installation of the resource ended in an unrecoverable error. The ServiceAdapter
     *            will send the RESOURCE_ERROR event.
     *            The state of the resource has been set to RESOURCE_ERROR.</td>
     *    </tr>
     * </table>
     * 
     * <p>If the installation of the Resource has been successfully performed, but the resource is
     *    still not available the return value must not contain the ResourceAddedOPR. In this case
     *    the specific ServiceAdapter or ResourceAdapter implementation is responsible for sending
     *    the corresponding events once the Resource is available.</p>
     * 
     * <p><b>Note:</b> The ServiceAdapter will call this method asynchronous. This method must be implemented
     * in a way that it can be aborted with the call of the <code>prepareReset</code> method.</p>
     * 
     * @see ResourceAddedOPR
     * @see ResourceChangedOPR
     * @see ResourceRejectedOPR
     * @see ResourceErrorOPR
     * @param transition  the transition which has prepared the resource for installation
     * @return the transaction which describes the changes on the resource 
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource is not prepared for install
     */
    public ResourceTransition install(ResourceTransition transition) throws InvalidResourceStateException;

    /**
     * Prepare the resource for uninstall.
     * 
     * <p>The result may contain the following RAOperationResults</p>
     * 
     * <table>
     *    <tr><td>RemoveResourceOPR</td>
     *        <td>If the resource has been prepared for uninstallation. The ServiceAdapter will send
     *            the REMOVE_RESOURCE event. 
     *            The state of the resource has been set to UNASSIGNING.</td>
     *    </tr>
     *    <tr><td>ResourceChangedOPR</td>
     *        <td>If the properties of the resource has been changed. The ServiceAdapter will send
     *            the RESOURCE_CHANGED events</td>
     *    </tr>
     *    <tr><td>ResourceErrorOPR</td>
     *        <td>The uninstallation of the resource ended in an unrecoverable error. The ServiceAdapter
     *            will send the RESOURCE_ERROR event.
     *            The state of the resource has been set to RESOURCE_ERROR.</td>
     *    </tr>
     * </table>
     * 
     * <p><b>Note:</b> The ServiceAdapter calls this method synchronous. This should only do
     *    the minimum effort to ensure that the Resource can be uninstalled.</p>
     * 
     * @see RemoveResourceOPR
     * @see ResourceChangedOPR
     * @param descr the description of the resource removal
     * @return the transaction which describes the changes on the resource 
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource is not
     *           in a state that allows uninstallation
     */
    public ResourceTransition prepareUninstall(ResourceRemovalDescriptor descr) throws InvalidResourceStateException;
    
    /**
     * Perform the uninstall procedure on the resource
     * 
     * <p>The result may contain the following RAOperationResults</p>
     * 
     * <table>
     *    <tr><td>ResourceRemovedOPR</td>
     *        <td>If the resource has been uninstallation. The ServiceAdapter will send
     *            the RESOURCE_REMOVED event. 
     *            The state of the resource has been set to UNASSIGNED.</td>
     *    </tr>
     *    <tr><td>ResourceChangedOPR</td>
     *        <td>If the properties of the resource has been changed. The ServiceAdapter will send
     *            the RESOURCE_CHANGED events</td>
     *    </tr>
     * </table>
     * 
     * <p>If the uninstallation of the Resource has been successfully performed, but the resource is
     *    still available the return value must not contain the RemoveResourceOPR. In this case
     *    the specific ServiceAdapter or ResourceAdapter implementation is responsible for sending 
     *    the corresponding events once the Resource has been completely removed.</p>
     * 
     * <p><b>Note:</b> The ServiceAdapter will call this method asynchronous. This method must be implemented
     * in a way that it can be aborted with the call of the <code>prepareReset</code> method.</p>
     * 
     * @param transition  the transition which has prepared the resource for uninstallation
     * @param descr the description of the resource removal
     * @return the transaction which describes the changes on the resource 
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource is not prepared for uninstall
     */
    public ResourceTransition uninstall(ResourceTransition transition, ResourceRemovalDescriptor descr) throws InvalidResourceStateException;

    /**
     * Prepare the resource for a reset
     * 
     * <p>The result may contain the following RAOperationResults</p>
     * 
     * <table>
     *    <tr><td>AddResourceOPR</td>
     *        <td>If the resource is going to be installed. The ServiceAdapter will send
     *            the ADD_RESOURCE event. 
     *            The state of the resource has been set to ASSIGING.</td>
     *    </tr>
     *    <tr><td>ResourceChangedOPR</td>
     *        <td>If the properties of the resource has been changed. The ServiceAdapter will send
     *            the RESOURCE_CHANGED events</td>
     *    </tr>
     * </table>
     * 
     * <p>This method is responsible for aborting ongoing installations/uninstallations off
     *    the resource.</p>
     * 
     * <p><b>Note:</b> The ServiceAdapter calls this method synchronous. This should only do
     *    the minimum effort to ensure that the Resource can be reset.</p>
     * 
     * @return the transaction which describes the changes on the resource 
     */
    public ResourceTransition prepareReset();
    
    /**
     * Perform the necessary steps the reset the resource.
     * 
     * <table>
     *    <tr><td>ResourceResetOPR</td>
     *        <td>The resources has been successfully reset. The ServiceAdapter will send
     *            the RESOURCE_RESET event. 
     *            The state of the resource has been set to ASSIGNED</td>
     *    </tr>
     *    <tr><td>ResourceChangedOPR</td>
     *        <td>If the properties of the resource has been changed. The ServiceAdapter will send
     *            the RESOURCE_CHANGED events</td>
     *    </tr>
     *    <tr><td>ResourceErrorOPR</td>
     *        <td>The installation of the resource ended in an unrecoverable error. The ServiceAdapter
     *            will send the RESOURCE_ERROR event.
     *            The state of the resource has been set to RESOURCE_ERROR.</td>
     *    </tr>
     * </table>
     * 
     * <p>If the installation of the Resource has been successfully performed, but the resource is
     *    still not available the return value must not contain the ResourceResetOPR. In this case
     *    the specific ServiceAdapter or ResourceAdapter implementation is responsible for sending
     *    the corresponding events once the Resource is available.</p>
     * 
     * <p><b>Note:</b> The ServiceAdapter will call this method asynchronous. This method must be implemented
     * in a way that it can be aborted with the call of the <code>prepareReset</code> method.</p>
     * 
     * @param transition the transition which prepared the resource for reset
     * @return the transaction which describes the changes on the resource 
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource is not prepared for reset
     */
    public ResourceTransition reset(ResourceTransition transition) throws InvalidResourceStateException;
    
}
