/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.adapter.RAOperationResult;

/**
 *  Abstract for a simple RAOperationResult
 */
public abstract class AbstractSimpleOPR implements RAOperationResult {

    private final Resource resource;
    private final String message;

    /**
     * Create a new instance of of AbstractSimpleOPR
     * @param resource the resource which has been changed
     * @param message the changed message
     */
    public AbstractSimpleOPR(Resource resource, String message) {
        this.resource = resource;
        this.message = message;
    }

    /**
     * Get the resource
     * @return the resource
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * Get the change message
     * @return the change message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Determine if the result is or contains a result of a specific type
     * @param resultType the result type
     * @return true if the result is or contains a result of the type
     */
    public boolean contains(Class<? extends RAOperationResult> resultType) {
        return resultType.isAssignableFrom(getClass());
    }

    /**
     * Returns a contained OPR of a specific type or null.
     * @param <T> the type of the OPR
     * @param oprType the OPR class to look for
     * @return  the contain OPR or <tt>null</tt>
     */
    @SuppressWarnings("unchecked")
    public <T extends RAOperationResult> T getByType(Class<T> oprType) {
        return (T) (oprType.isAssignableFrom(getClass()) ? this : null);
    }
    
    
}
