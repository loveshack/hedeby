/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.resource.adapter.ResourceAdapterStore;
import com.sun.grid.grm.resource.adapter.ResourceTransition;
import com.sun.grid.grm.service.descriptor.ResourceReassignmentDescriptor;
import com.sun.grid.grm.service.event.ServiceEventSupport;

/**
 * Default implementation of a ResourceTransition
 */
public class DefaultResourceTransition implements ResourceTransition {

    private final long id;
    private final RAOperationResult result;
    
    /**
     * Create a new instance of DefaultResourceTransition
     * @param id      the id of the transition
     * @param result  the result of all operation which has been invoked on the resource 
     *                within the transition
     *         
     */
    public DefaultResourceTransition(long id, RAOperationResult result) {
        this.id = id;
        this.result = result;
    }
    
    /**
     * Get the id of the transition
     * @return
     */
    public final long getId() {
        return id;
    }

    /**
     * Commit the transition.
     * 
     * <p>If the transition contains a ResourceRemovedOPR or a ResourceRejectedOPR
     *    result the resource is removed from the storage and all events
     *    are fired.</p>
     * <p>If the result has changed the resource the resource is stored in the
     *    storage and all events are fired</p>
     * <p>Otherwise no events are fired</p>
     * 
     * @param ra    the ResourceAdapter
     * @param storage  the storage
     * @param eventSupport the event support
     * @return <code>true</code> if the transition has changed the resource
     * @throws com.sun.grid.grm.resource.ResourceStoreException    if the storage has a problem
     */
    @SuppressWarnings("unchecked")
    public final boolean commit(ResourceAdapter ra, ResourceAdapterStore storage, ServiceEventSupport eventSupport) throws ResourceStoreException {
        
        ResourceRemovedOPR rropr = result.<ResourceRemovedOPR>getByType(ResourceRemovedOPR.class);
        if (rropr != null) {
            try {
                storage.remove(ra.getId(), rropr.getDescr());
            } catch (UnknownResourceException ex) {
                // Ignore it, it is fine that a resource that should be removed
                // has not been found in the storage
            }
            result.fireEvents(eventSupport);
            return true;
        } else if(result.contains(ResourceRejectedOPR.class)) {
            try {
                storage.remove(ra.getId(), ResourceReassignmentDescriptor.getInstance(true));
            } catch (UnknownResourceException ex) {
                // Ignore it, it is fine that a resource that should be removed
                // has not been found in the storage
            }
            result.fireEvents(eventSupport);
            return true;
        } else if (result.hasModifiedResource()) {
            storage.store(ra);
            result.fireEvents(eventSupport);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("[Transition[id=%d,res=%s]", id, result);
    }
    
    /**
     * Is a specific resource operation result type contained in the this
     * transition
     * @param resultType  the resource operation result type
     * @return <code>true</code> if such a resource operation result type is included
     */
    public boolean contains(Class<? extends RAOperationResult> resultType) {
        return result.contains(resultType);
    }

    /**
     * Has this resource transition modified the resource
     * @return <tt>true</tt> if the transition has modified the resource
     */
    public boolean hasModifiedResource() {
        return result.hasModifiedResource();
    }


}
