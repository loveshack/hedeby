/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter;

import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.service.event.ServiceEventSupport;

/**
 * A ResourceTransition describes the result of a resource transition.
 */
public interface ResourceTransition {

    /**
     * Unique id of the transition.
     * 
     * @return the id of the transition
     */
    long getId();

    /**
     * Commit the changes which has been made with this resource transition.
     * 
     * <p>This method makes the resource persistent in the storage and fires the
     * service events which has been generated within the transition.</p>
     * 
     * @param ra   the resource adapter
     * @param storage  the storage
     * @param eventSupport the event support
     * @return <code>true</code> if the transition has changed the resource
     * @throws com.sun.grid.grm.resource.ResourceStoreException  if the changes could not be made persistent
     */
    boolean commit(ResourceAdapter ra, ResourceAdapterStore storage, ServiceEventSupport eventSupport) 
            throws ResourceStoreException;
   
    /**
     * Contains this Resource transition a the resource operation result of a specific type
     * @param resultType the type of the resource operation result
     * @return <code>true</code> if a corresponding type is included
     */
    public boolean contains(Class<? extends RAOperationResult> resultType);

    /**
     * Has this resource transition modified the resource
     * @return <tt>true</tt> if the transition has modified the resource
     */
    public boolean hasModifiedResource();
}
