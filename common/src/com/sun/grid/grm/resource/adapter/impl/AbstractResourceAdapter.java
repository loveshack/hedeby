/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.resource.Resource.State;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.adapter.InvalidResourceStateException;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.resource.adapter.ResourceTransition;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.descriptor.ResourcePurgeDescriptor;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Abstract base for a ResourceAdapter.
 * 
 * <p>This class check that the Resource is in the correct state for a resource transition.
 *    It implements this check in a state pattern. </p>
 *
 * @param <K> The type of the key that identifies a resource from the service view
 *            (e.g. for a host resource consuming resource the type is the Hostname)
 */
public abstract class AbstractResourceAdapter<K> implements ResourceAdapter<K> {

    private final static String BUNDLE = "com.sun.grid.grm.resource.adapter.impl.resource_adapter_impl";
    
    protected final Resource resource;
    protected final AtomicLong transitionId = new AtomicLong();

    /**
     *  This lock protects the state transition of the resource
     *  Whenever the resource adapter modifies the resource state it must
     *  hold this lock
     */
    protected final Lock resourceStateLock = new ReentrantLock();
    
    protected AbstractResourceAdapter(Resource resource) {
        this.resource = resource;
    }

    @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!AbstractResourceAdapter.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        return getKey().equals(((AbstractResourceAdapter)obj).getKey());
    }

    @Override
    public final int hashCode() {
        return getKey().hashCode();
    }
    
    
    /**
     * Get the id of the resource represented by this ResourceAdapter
     * @return
     */
    public final ResourceId getId() {
        return resource.getId();
    }

    /**
     * Get a clone of the resource represented by this ResourceAdapter
     * @return the clone of the resource
     */
    public final Resource getResource() {
        return resource.clone();
    }

    /**
     * Is the resource represented by this ResourceAdapter static?
     * @return <code>true</code> if the resource is state
     */
    public final boolean isStatic() {
        return resource.isStatic();
    }

    /**
     * Modify the Resource
     * @param operation the operation
     * @return the change
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation leads into 
     *            an invalid resource (resource has not been changed)
     */
    public ResourceChanged modifyResource(ResourceChangeOperation operation) throws InvalidResourcePropertiesException {
        return resource.modify(operation);
    }

    /**
     * Modify the resource. This method will not change the
     * state of the resource.
     *
     * <p>If the returned list of changes is not empty the ServiceAdapter will
     *    spool the resource and fire and ResourceChanged event.</p>
     *
     * @param operation  the resource operation
     * @return the list of changes
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation would create an invalid resource
     */
    public Collection<ResourceChanged> modifyResource(Collection<ResourceChangeOperation> operation) throws InvalidResourcePropertiesException {
        return resource.modify(operation);
    }

    
    /**
     * Prepare the resource for install.
     * 
     * <p>This method calls finally doPrepareInstall if the resource is in a state
     * that it can be prepared for installation.</p>
     * 
     * @return the resource transition
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException
     */
    public final ResourceTransition prepareInstall() throws InvalidResourceStateException {
        return getStateHandler(resource.getState()).prepareInstall(this);
    }

    /**
     * Install the resource
     * 
     * <p>This method calls finally doInstall if the resource is prepared for
     *    installation.</p>
     * 
     * @param transition the transition which has prepared the installation
     * @return the resulting resource transition
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource is not prepared for installation
     */
    public final ResourceTransition install(ResourceTransition transition) throws InvalidResourceStateException {
        return getStateHandler(resource.getState()).install(this);
    }

    /**
     * Prepare the resource for uninstall.
     * 
     * <p>This method calls finally doPrepareUninstall if the resource is in a state
     * that it can be prepared for uninstallation.</p>
     * 
     * @param descr The descriptor of the resource remove operation
     * @return the resource transition
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource can not be prepared for uninstallation
     */
    public final ResourceTransition prepareUninstall(ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
        return getStateHandler(resource.getState()).prepareUninstall(this, descr);
    }

    /**
     * Uninstall the resource
     * 
     * <p>This method calls finally doUninstall if the resource is prepared for
     *    installation.</p>
     * 
     * @param transition the transition which has prepared the uninstallation
     * @param descr The description of the resource remove operation
     * @return the resulting resource transition
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource is not prepared for uninstallation
     */
    public final ResourceTransition uninstall(ResourceTransition transition, ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
        return getStateHandler(resource.getState()).uninstall(this, descr);
    }

    /**
     * Prepare the resource for reset.
     * 
     * <p>This method calls finally doPrepare</p>
     * 
     * @return the resource transition
     */
    public final ResourceTransition prepareReset() {
        return getStateHandler(resource.getState()).prepareReset(this);
    }

    /**
     * Reset the resource
     * 
     * <p>This method calls finally doReset if the resource is prepared for
     *    installation.</p>
     * 
     * @param transition the transition which has prepared the reset
     * @return the resulting resource transition
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException if the resource is not prepared for reset
     */
    public final ResourceTransition reset(ResourceTransition transition) throws InvalidResourceStateException {
        return getStateHandler(resource.getState()).reset(this);
    }
    
    /**
     * Prepare the resource for install.
     * 
     * @return the result of the resource transition
     * @throws InvalidResourceStateException If the resource is not in the state that allows installation
     */
    protected abstract RAOperationResult doPrepareInstall() throws InvalidResourceStateException;
    
    /**
     * Install the resource.
     * @return the result of the installation
     */
    protected abstract RAOperationResult doInstall();


    /**
     * Checks that the resource is not static and in ASSIGNED state
     * @param res    the resource
     * @param descr  the resource removal descriptor
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException
     */
    public static void canResourceBeRemoved(Resource res, ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
        if(res.isStatic()) {
            throw new InvalidResourceStateException("ara.ex.resStatic", BUNDLE, res.getName());
        }
        if(!res.getState().equals(Resource.State.ASSIGNED)) {
            throw new InvalidResourceStateException("ara.ex.invalidResState", BUNDLE, res.getName(), res.getState());
        }
    }
    
    /**
     * Prepare the resource for uninstallation
     * @param descr The descriptor of the resource remove operation
     * @return the result of the resource transition
     * @throws InvalidResourceStateException If the resource is not in the state that allows uninstallation
     */
    protected abstract RAOperationResult doPrepareUninstall(ResourceRemovalDescriptor descr) throws InvalidResourceStateException;
    
    /**
     * Uninstall the resource.
     * @param descr The descriptor of the resource remove operation
     * @return the result of the uninstallation
     */
    protected abstract RAOperationResult doUninstall(ResourceRemovalDescriptor descr);
    
    /**
     * Prepare the resource for reset.
     * 
     * <p><b>Note:</b> This method must abort ongoing installations/uninstallations</p>
     * 
     * @return the result of the resource transition
     */
    protected abstract RAOperationResult doPrepareReset();
    
    /**
     * Reset the resource.
     * @return the result of the uninstallation
     */
    protected abstract RAOperationResult doReset();
    

    private ResourceTransition createTransitionResult(RAOperationResult opResult) {
        return new DefaultResourceTransition(transitionId.getAndIncrement(), opResult);
    }

    private static StateHandler getStateHandler(Resource.State state) {
        switch(state) {
            case ASSIGNING:   return ASSIGNING_STATEHANDLER;
            case ASSIGNED:    return ASSIGNED_STATEHANDLER;
            case UNASSIGNING: return UNASSIGNING_STATEHANDLER;
            case UNASSIGNED:  return UNASSIGNED_STATEHANDLER;
            case ERROR:       return ERROR_HANDLER;
            default:
                throw new IllegalStateException("Unknown resource state " + state);
        }
    }

    
    private abstract static class StateHandler {

        public abstract Resource.State getState();
        
        public final ResourceTransition prepareReset(AbstractResourceAdapter ra) {
            return ra.createTransitionResult(ra.doPrepareReset());
        }

        public ResourceTransition reset(AbstractResourceAdapter ra) throws InvalidResourceStateException {
            throw new InvalidResourceStateException("ara.ex.reset", BUNDLE, getState());
        }

        public ResourceTransition prepareInstall(AbstractResourceAdapter ra) throws InvalidResourceStateException {
            throw new InvalidResourceStateException("ara.ex.prepareInstall", BUNDLE, getState());
        }

        public ResourceTransition install(AbstractResourceAdapter ra) throws InvalidResourceStateException {
            throw new InvalidResourceStateException("ara.ex.install", BUNDLE, getState());
        }

        public ResourceTransition prepareUninstall(AbstractResourceAdapter ra, ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
            throw new InvalidResourceStateException("ara.ex.prepareUninstall", BUNDLE, getState());
        }

        public ResourceTransition uninstall(AbstractResourceAdapter ra, ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
            throw new InvalidResourceStateException("ara.ex.uninstall", BUNDLE, getState());
        }
        
    }

    private final static StateHandler ASSIGNING_STATEHANDLER = new StateHandler() {

        @Override
        public ResourceTransition install(AbstractResourceAdapter ra) throws InvalidResourceStateException {
            return ra.createTransitionResult(ra.doInstall());
        }

        @Override
        public ResourceTransition reset(AbstractResourceAdapter ra) throws InvalidResourceStateException {
            return ra.createTransitionResult(ra.doReset());
        }
        
        @Override
        public State getState() {
            return Resource.State.ASSIGNING;
        }
        
    };

    private RAOperationResult checkAndRemoveResource(ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
        canResourceBeRemoved(resource, descr);
        return doPrepareUninstall(descr);
    }
    
    private final static StateHandler ASSIGNED_STATEHANDLER = new StateHandler() {

        @Override
        public ResourceTransition prepareUninstall(AbstractResourceAdapter ra, ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
            return ra.createTransitionResult(ra.checkAndRemoveResource(descr));
        }

        @Override
        public State getState() {
            return Resource.State.ASSIGNED;
        }

    };
    
    private final static StateHandler UNASSIGNING_STATEHANDLER = new StateHandler() {
        @Override
        public ResourceTransition uninstall(AbstractResourceAdapter ra, ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
            return ra.createTransitionResult(ra.doUninstall(descr));
        }
        
        @Override
        public State getState() {
            return Resource.State.UNASSIGNING;
        }
    };

    private final static StateHandler UNASSIGNED_STATEHANDLER = new StateHandler() {

        @Override
        public ResourceTransition prepareInstall(AbstractResourceAdapter ra) throws InvalidResourceStateException {
            return ra.createTransitionResult(ra.doPrepareInstall());
        }
        
        @Override
        public State getState() {
            return Resource.State.UNASSIGNED;
        }
    };
    
    private final static StateHandler ERROR_HANDLER = new StateHandler() {
        @Override
        public State getState() {
            return Resource.State.ERROR;
        }
        
        @Override
        public ResourceTransition prepareUninstall(AbstractResourceAdapter ra, ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
            if (descr instanceof ResourcePurgeDescriptor) {
                // allow purging of resources in ERROR state
                return ra.createTransitionResult(ra.doPrepareUninstall(descr));
            }
            throw new InvalidResourceStateException("ara.ex.prepareUninstall", BUNDLE, getState());
        }        
    };

    @Override
    public final String toString() {
        return getId().toString();
    }
    
}
