/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.adapter.impl;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceStoreException;
import java.util.Collections;
import java.util.Set;

/**
 *  This ResourceAdapterStore does not provide any persitence, it can be
 *  only used for testing.
 */
public class SimpleResourceAdapterStore extends AbstractResourceAdapterStore<ResourceId, DefaultResourceAdapter,Resource> {

    /**
     * Creates a DefaultResourceAdapter
     * @param resource the resource
     * @return the DefaultResourceAdapter
     */
    public DefaultResourceAdapter createResourceAdapter(Resource resource) {
        return new DefaultResourceAdapter(resource);
    }

    /**
     * This ResourceAdapterStore has no persistent storage.
     * 
     * @return an empty set
     * @throws com.sun.grid.grm.resource.ResourceStoreException
     */
    @Override
    protected Set<Resource> loadElements() throws ResourceStoreException {
        return Collections.<Resource>emptySet();
    }

    /**
     * Does nothing
     * @param elem
     * @throws com.sun.grid.grm.resource.ResourceStoreException
     */
    @Override
    protected void storeElement(Resource elem) throws ResourceStoreException {
        // No action required
    }

    /**
     * Does nothing
     * @param elem
     * @throws com.sun.grid.grm.resource.ResourceStoreException
     */
    @Override
    protected void removeElement(Resource elem) throws ResourceStoreException {
        // No action required
    }

    /**
     * Creates a DefaultResourceAdapter
     * @param elem the resource
     * @return the DefaultResourceAdapter
     */
    @Override
    protected DefaultResourceAdapter createResourceAdapterForElement(Resource elem) {
        return new DefaultResourceAdapter(elem);
    }
    
    /**
     * get the resource from the DefaultResourceAdapter
     * @param ra the resource adapter
     * @return the resource
     */
    @Override
    protected Resource getElementFromResourceAdapter(DefaultResourceAdapter ra) {
        return ra.getResource();
    }
}
