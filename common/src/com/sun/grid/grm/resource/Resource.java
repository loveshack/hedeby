/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;

import com.sun.grid.grm.service.Usage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * The Resource interface is a representation of a resource.  The resource interface
 * itself is generic.  It derives its type and properties from its
 * ResourceType class.
 *
 * @see ResourceType
 */
public interface Resource {

    /**
     * name for the resource usage
     */
    public final static String USAGE = "usage";
    /**
     * name for the resource annotation
     */
    public final static String ANNOTATION = "annotation";
    /**
     * name for the resource ambiguous state
     */
    public final static String AMBIGUOUS = "ambiguous";

    /**
     * clone this Resource
     * @return the clone
     */
    public Resource clone();

    /**
     * Get a clone of this Resource with a new Id
     * @param newId  the new Id
     * @param type type of cloned resource has to match newId type
     * @return the cloned resource
     * @deprecated
     */
    @Deprecated
    public Resource cloneWithNewId(ResourceId newId, ResourceType type);

    /**
     * Gets the resource's id.
     * @return the resource's id
     */
    public ResourceId getId();

    /**
     * Get the name of the resource.
     * @return the name of the resource
     */
    public String getName();

    /**
     * Get the bound state of the resource.
     *
     * @return <code>true</code> if the resource is bound
     */
    public boolean isBound();

    /**
     * Gets the resource's properties.
     * @return the resource's properties (immuntable)
     */
    public Map<String, Object> getProperties();

    /**
     * get the value of a resource property
     * @param name the name of the resource property
     * @return the value
     */
    public Object getProperty(String name);

    /**
     * Get the type of the resource
     * @return the type of the resource
     */
    public ResourceType getType();

    /**
     * Gets the resource's state.
     * @return the resource's state
     */
    public State getState();

    /**
     * Sets the resource's state.
     * @param state the resource's state
     */
    public void setState(State state);

    /**
     * Sets the resource's annotation.  The annotation is used to provide
     * additional information about the resource's state.
     * @param annotation the resource's annotation
     */
    public void setAnnotation(String annotation);

    /**
     * Gets the resource's annotation.
     * @return the resource's annotation
     */
    public String getAnnotation();

    /**
     * Sets a property's value in this resource.
     * @param name the property's name
     * @param value the property's value
     * @return the object describing the change
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the value of the property can not change
     */
    public ResourceChanged setProperty(String name, Object value) throws InvalidResourcePropertiesException;

    /**
     * Set the properties of the resource
     * @param properties the properties
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the properties are not valid
     */
    public void setProperties(Map<String, Object> properties) throws InvalidResourcePropertiesException;

    /**
     * Removes a resource property.
     * @param name the name of the property to remove
     * @return the value of the removed property
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the property is mandatory
     */
    public Object removeProperty(String name) throws InvalidResourcePropertiesException;

    /**
     * Modify a resource
     * @param operations that will be done on this resource
     * @return list of the changed properties
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if a property 
     *            contains an invalid value. If this exception is thrown the resource
     *            has not been modified
     */
    public Collection<ResourceChanged> modify(Collection<ResourceChangeOperation> operations) throws InvalidResourcePropertiesException;

    /**
     * Modify a resource.
     * @param operation that will be done on this resource
     * @return the change
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if a property
     *            contains an invalid value. If this exception is thrown the resource
     *            has not been modified
     */
    public ResourceChanged modify(ResourceChangeOperation operation)  throws InvalidResourcePropertiesException;

    /**
     * This method writes the resource to the given file.
     * @param file the target file
     * @throws java.io.IOException if any error while writing to file occured
     */
    public void writeToFile(File file) throws IOException;

    /**
     * Convenient way to set the static flag property.
     * @param isStatic true if resource has to be set static
     */
    public void setStatic(boolean isStatic);

    /**
     * Convenient way to set the ambiguous flag property.
     * @param isAmbiguous true if resource has to be set ambiguous
     */
    public void setAmbiguous(boolean isAmbiguous);

    /**
     * Convenient way to set the usage property.
     * @param usage the usage to be set
     */
    public void setUsage(Usage usage);

    /**
     * Convenient way to get the usage property.
     * @return the Usage object constructed using the usage level property or Usage.MIN_VALUE
     * if usage property is not set or invalid
     */
    public Usage getUsage();

    /**
     * Convenient wat to find out if resource is static.
     * @return true if resource is static, false otherwise
     */
    public boolean isStatic();

    /**
     * Convenient wat to find out if resource is ambiguous.
     * @return true if resource is ambiguous, false otherwise
     */
    public boolean isAmbiguous();

    /**
     * State represents the state of a resource.
     */
    public enum State {

        /**
         * The resource has been assigned to a service.
         */
        ASSIGNED,
        /**
         * The resource has a problem.
         */
        ERROR,
        /**
         * The resource is in the process of assignement
         */
        ASSIGNING,
        /**
         * The resource was unassigned form service but is not yet in process
         */
        UNASSIGNED,
        /**
         * The resource is in the process of unassignement
         */
        UNASSIGNING;
    }
}
