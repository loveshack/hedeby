/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;

import com.sun.grid.grm.resource.policy.PolicyManager;
import com.sun.grid.grm.service.event.ResourceRequestEvent;
import com.sun.grid.grm.util.filter.Filter;

import java.util.List;

/**
 * RequestQueue defines interface for queuing the resource requests sent
 * by services.
 *
 * The implementation class has to decide about the persistence - there are
 * no constraints put by the interface.
 *
 */
public interface RequestQueue {

    /**
     * This method puts an incoming event on the queue to be handled.
     * @param event an incoming event
     */
    void addEvent(ResourceRequestEvent event);

    /**
     * This method triggers the re-processing of the requests that were not
     * fulfilled by their initial processing. The method should be called when
     * a significant change in the managed system occurs that can lead to
     * presence of new spare resources.
     */
    void triggerReprocessing();

    /**
     * Starts the request queue.
     */
    public void start();

    /**
     * Stops the request queue.
     */
    public void stop();

    /**
     * Sets the policy manager.
     *
     * @param policyProvider desired policy manager
     */
    public void setPolicyProvider(PolicyManager policyProvider);

    /**
     * Returns a list of request matching a request filter
     * @param requestFilter the requestFilter
     * @return list of requests
     */
    public List<Request> getRequests(Filter<Request> requestFilter);

    /**
     * Removes a list of request matching a request filter.
     * 
     * @param requestFilter the requestFilter
     * @return list of requests
     */
    public List<Request> removeRequests(Filter<Request> requestFilter);
}
