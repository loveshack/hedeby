/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;

import com.sun.grid.grm.GrmException;

/**
 * This exception is thrown if a resource type can not create a resource id.
 */
public class InvalidResourceIdException extends GrmException {

    private final static long serialVersionUID = -2008020801L;
    public final static String BUNDLE = "com.sun.grid.grm.resource.resource";
    
    private final String id;
    
    /**
     * Create a new instance of InvalidResourceIdException
     * @param id      the id string for the resource id
     * @param reason  the reason why the id is invalid
     */
    public InvalidResourceIdException(String id, String reason) {
        super("irid.ex", BUNDLE, id, reason);
        this.id = id;
    }
    
    /**
     * Get the invalid resource id
     * @return the invalid resource id
     */
    public String getId() {
        return id;
    }
}
