/*___INFO__MARK_BEGIN__*/

/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.order;

import com.sun.grid.grm.resource.ResourceId;

/**
 * Common interface for an Order.
 */
public interface Order {

    /**
     * Get the resource id of the resource that is subject of the order.
     * @return the resource id
     */
    ResourceId getResourceId();

    /**
     * Get the name of a service that is the owner of a resource that is subject of the order.
     * 
     * @return name of the service
     */
    String getSource();

    /**
     * Returns the target service for assignment.
     * @return the target service for assignment
     */
    String getTarget();

    /**
     * Returns the order type.
     * @return the order type
     */
    OrderType getType();

    /**
     * Checks whether the order is maintenance (order by administrator)
     * or not.
     * @return true if maintenance order, false otherwise
     */
    boolean isMaintenance();

    /**
     * Gets an order identifier for use in hash tables. As resource is identified
     * by its resource id and an owning service, hash key generation uses resource id
     * and source service identifier of the order - each order targets just the one resource.
     *
     * @return string usable as a key in hash tables
     */
    public String getHashKey();
    
    /**
     * Commit this order.
     * 
     * <p>This method is called from ResourceManager once an order has been successfully executed.<p>
     * <p>For maintenance orders the commit has no effect. For ResourceRequest orders the commit
     *    signalizes that the quantity of the resource request can be decremented.</p>
     */
    public void commit();
    
    /**
     * Cancel the order
     * 
     * <p>This method is called from ResourceManager if it detects the service did not accept 
     *    the resource that is subject of the order.</p>.
     * <p>For maintenance orders the cancel has no effect. For ResourceRequest orders the cancel
     *    signalizes that the order which has been placed to due a need has not be fulfilled, the
     *    need must produce a new order.</p>
     */
    public void cancel();
    
    
}
