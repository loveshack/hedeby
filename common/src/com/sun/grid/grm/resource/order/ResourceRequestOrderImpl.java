/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.order;

import com.sun.grid.grm.resource.impl.*;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.util.I18NManager;

/**
 * Default implementation of a ResourceRequestOrder.
 * 
 */
class ResourceRequestOrderImpl extends AbstractOrderImpl implements ResourceRequestOrder {
    
    private final static String BUNDLE = "com.sun.grid.grm.resource.order.resource-order";
    private final static long serialVersionUID = -2009040701;

    private final transient NeedProcessor needProcessor;
    private final String sloName;
    private final int hash;
    
    /**
     * Create a new ResourceRequestOrderImpl
     * @param id       the id of the resource
     * @param source   the source service
     * @param target   the target service
     * @param np       the need processor
     * @param sloName  the name of the requesting SLO
     * @throws NullPointerException if np is null
     */
    ResourceRequestOrderImpl(ResourceId id, String source, String target, NeedProcessor np, String sloName) 
       throws NullPointerException {
        super(OrderType.ASSIGN, id, source, target);
        if (np == null) {
            throw new NullPointerException("np must not be null");
        }
        this.needProcessor = np;
        this.sloName = sloName;
        
        int h = super.hashCode();
        h = 37 * h + (this.sloName != null ? this.sloName.hashCode() : 0);
        hash = h;
    }
    
    /**
     * Is this order an maintenance order (triggered by the  administrator)
     * @return always false
     */
    public boolean isMaintenance() {
        return false;
    }

    /**
     * Get the name of the SLO which produced the order.
     * @return the name of the SLO which produced the order
     */
    public String getSloName() {
        return sloName;
    }

    /**
     * Indicates whether an object is equal to the order.
     *
     * @param obj object to match
     * @return true if object is equal to the order, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourceRequestOrderImpl other = (ResourceRequestOrderImpl) obj;
        if (this.sloName != other.sloName && (this.sloName == null || !this.sloName.equals(other.sloName))) {
            return false;
        }
        return true;
    }

    /**
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        return hash;
    }

    /**
     * Commit the order.
     * 
     * <p>This method calls the <code>commitAwaitedResource</code> of the 
     *    associated need processor</p>
     * 
     * @see NeedProcessor#commitAwaitedResource(com.sun.grid.grm.resource.ResourceId) 
     */
    public void commit() {
        needProcessor.commitAwaitedResource(getResourceId());
    }

    /**
     * Cancel the order.
     * 
     * <p>This method calls the <code>cancelAwaitedResource</code> of the 
     *    associated need processor</p>
     * 
     * @see NeedProcessor#cancelAwaitedResource(com.sun.grid.grm.resource.ResourceId) 
     * 
     */
    public void cancel() {
        needProcessor.cancelAwaitedResource(getResourceId());
    }

    /**
     * @return the need processor that is linked with this order
     */
    public NeedProcessor getNeedProcessor() {
        return needProcessor;
    }
    /**
     * Get a string representation of this order
     * @return the string representation
     */
    @Override
    public String toString() {
        return I18NManager.formatMessage("ro.str", BUNDLE, getType(), getResourceId(), getSource(), getTarget(), getSloName());
    }
}
