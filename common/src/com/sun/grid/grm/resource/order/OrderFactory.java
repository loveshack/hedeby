/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.order;

import com.sun.grid.grm.resource.*;
import com.sun.grid.grm.resource.impl.NeedProcessor;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Factory class for OrderImpl objects
 */
public final class OrderFactory {

    private final static String BUNDLE = "com.sun.grid.grm.resource.order.resource-order";

    /**
     * Create an maintenance order which removes a resource from the system.
     * 
     * <p>The resource will be first removed from the owning service and if the
     *    unassign process was successful it will be removed from the system.</p>
     *    
     * @param id       the id of the resource
     * @param source   the name of the service
     * @return the order
     */
    public static Order createMaintenanceRemoveOrder(ResourceId id, String source) {
        return new MaintenanceOrderImpl(OrderType.REMOVE, id, source, null, false);
    }
    
    /**
     * Create a maintenance order which moves a resource from <code>source</code> to <code>target</code>.
     * 
     * @param id        the id of the resource
     * @param source    the name of the source service
     * @param target    the name of the target service
     * @param isStatic  if set to <code>true</code> the resource should become static once it is
     *                  assigned to the target service
     * @return the order
     */
    public static Order createMaintenanceAssignOrder(ResourceId id, String source, String target, boolean isStatic) {
        return new MaintenanceOrderImpl(OrderType.ASSIGN, id, source, target, isStatic);
    }
    
    /**
     * Create a resource request order which moves a resource from <code>source</code> to
     * <code>target</code>.
     * 
     * @param id       the id of the resource
     * @param source   the source service
     * @param target   the target service
     * @param needProcessor  the processor of the need
     * @param sloName  the name of the SLO which produced the need (SLO in the target service)
     * @return  the order
     * @throws NullPointerException if neepProcessor is null
     */
    public static Order createResourceRequestAssignOrder(ResourceId id, String source, String target, NeedProcessor needProcessor, String sloName) {
        return new ResourceRequestOrderImpl(id, source, target, needProcessor, sloName);
    }

    /**
     * This method reads an order from a file
     * @param file the target file
     * @return the order read from file
     * @throws java.io.IOException if any error while reading from file occurred
     */
    public static Order readFromFile(File file) throws IOException {
        FileInputStream fin = new FileInputStream(file);
        try {
            ObjectInputStream in = new ObjectInputStream(fin);
            try {
                Object obj = in.readObject();
                if (obj instanceof Order) {
                    return (Order) obj;
                }
                // Here we can do upgrade

                // We do not know this order implementation
                throw new IOException(I18NManager.formatMessage("of.ex.cnf", BUNDLE, file.getAbsolutePath(), obj.getClass().getName()));
            } catch (ClassNotFoundException e) {
                IOException ioe = new IOException(I18NManager.formatMessage("of.ex.cnf", BUNDLE, file.getAbsolutePath(), e.getMessage()));
                ioe.initCause(e);
                throw ioe;
            }
        } finally {
            fin.close();
        }
    }
    
    /**
     * Write an order into a file
     * @param order the order
     * @param file the file
     * @throws java.io.IOException
     */
    public static void writeToFile(Order order, File file) throws IOException {
        FileOutputStream fout = new FileOutputStream(file);
        try {
            ObjectOutputStream oout = new ObjectOutputStream(fout);
            oout.writeObject(order);
        } finally {
            fout.close();
        }        
    }
}
