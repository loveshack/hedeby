/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.order;

import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.util.I18NManager;

/**
 *  Default implementation of an maintenance order.
 */
class MaintenanceOrderImpl extends AbstractOrderImpl implements MaintenanceOrder {
    
    private final static String BUNDLE = "com.sun.grid.grm.resource.order.resource-order";
    private final static long serialVersionUID = -2009040701;
    private final boolean isStatic;
    private final int hash;
    
    /**
     * Create a new new <code>MaintenanceOrderImpl</code>.
     * 
     * @param type   the type of the order
     * @param id     the id of the resource
     * @param source the owner of the resource
     * @param target the targeted service
     * @param isStatic  <code>true</code> if the resource should be added static to the target service
     */
    MaintenanceOrderImpl(OrderType type, ResourceId id, String source, String target, boolean isStatic) {
        super(type, id, source, target);
        this.isStatic = isStatic;
        
        int h = super.hashCode();
        h = 37 * h + (this.isStatic ? 1 : 0);
        hash = h;
    }
    
    /**
     * A MaintenanceOrderImpl is an maintenance order
     * @return always true
     */
    public boolean isMaintenance() {
        return true;
    }

    /**
     * This static field of the <code>MaintenanceOrder</code> is only relevant
     * for an maintenance assign order. It say whether the target service should
     * make the resource static or not. 
     * @return <code>true</code> if the resource should be added static to the target service
     */
    public boolean isStatic() {
        return isStatic;
    }

    /**
     * Commit the order.
     */
    public void commit() {
        // Empty implementation, commit has no mean for a Maintance order
    }

    /**
     * Cancel the order.
     */
    public void cancel() {
        // Empty implementation, cancel has no mean for a Maintance order
    }
    
    /**
     * Indicates whether an object is equal to the order.
     *
     * @param obj object to match
     * @return true if object is equal to the order, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final MaintenanceOrderImpl other = (MaintenanceOrderImpl) obj;
        if (this.isStatic != other.isStatic) {
            return false;
        }
        return true;
    }

    /**
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        return hash;
    }
    
    /**
     * Get a human readable string representation of the order
     * @return the string representation
     */
    @Override
    public String toString() {
        return I18NManager.printfMessage("mo.str", BUNDLE, getType(), getResourceId(), getSource(), getTarget(), isStatic());
    }
    

}
