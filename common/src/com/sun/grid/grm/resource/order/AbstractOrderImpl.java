/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.order;

import com.sun.grid.grm.resource.ResourceId;
import java.io.Serializable;

/**
 * Abstract base class of an order.
 */
abstract class AbstractOrderImpl implements Comparable<Order>, Order, Serializable {

    private final OrderType type;
    private final ResourceId id;
    private final String source;
    private final String target;
    private final String hashKeyString;
    private final int hash;
    
    /**
     * Create a new instance of of AbstractOrderImpl
     * @param type    the type of the order
     * @param id      the id of the resource
     * @param source  the source service (current owner)
     * @param target  the target service
     */
    AbstractOrderImpl(OrderType type, ResourceId id, String source, String target) {
        this.type = type;
        this.id = id;
        this.source = source;
        this.target = target;

        String resIdStr = id.toString();
        StringBuilder sb = new StringBuilder(resIdStr.length() + source.length() + 1);
        sb.append(resIdStr);
        sb.append('@');
        sb.append(this.source);
        this.hashKeyString = sb.toString();
        
        int h = 7;
        h = 37 * h + (this.id != null ? this.id.hashCode() : 0);
        h = 37 * h + (this.type != null ? this.type.hashCode() : 0);
        h = 37 * h + (this.source != null ? this.source.hashCode() : 0);
        h = 37 * h + (this.target != null ? this.target.hashCode() : 0);
        hash = h;
    }

    /**
     * Compare the order with another order.
     * 
     * The compare is down by comparing the hashkey.
     * 
     * @see #getHashKey() 
     * @param o  the other order
     * @return the resource of the compare as defined in the Compare interface
     */
    public final int compareTo(Order o) {
        return hashKeyString.compareTo(o.getHashKey());
    }

    /**
     * Is the order equal to another object
     * @param obj the object
     * @return <code>true</code> of obj is equal to this
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractOrderImpl other = (AbstractOrderImpl) obj;
        
        if (this.hash != other.hash) {
            return false;
        }
        
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if ((this.source == null && other.source != null) || (this.source != null && !this.source.equals(other.source))) {
            return false;
        }
        if ((this.target == null && other.target != null) || (this.target != null && !this.target.equals(other.target))) {
            return false;
        }
        return true;
    }

    /**
     * Get the hash key of the order (resource@service)
     * @return the hash key
     */
    public String getHashKey() {
        return hashKeyString;
    }

    /**
     * Get the id of the resource
     * @return the resource id
     */
    public ResourceId getResourceId() {
        return id;
    }

    /**
     * Get the source service
     * @return the source service
     */
    public String getSource() {
        return source;
    }

    /**
     * Get the target service
     * @return the target service
     */
    public String getTarget() {
        return target;
    }

    /**
     * Get the type of the order
     * @return
     */
    public OrderType getType() {
        return type;
    }

    /**
     * Get the hashcode of the order
     * @return the hashcode
     */
    @Override
    public int hashCode() {
        return hash;
    }
}
