/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource;

import com.sun.grid.grm.resource.impl.ResourceIdImpl;
import com.sun.grid.grm.resource.impl.ResourceNotChanged;
import com.sun.grid.grm.resource.impl.ResourcePropertyDeleted;
import com.sun.grid.grm.resource.impl.ResourcePropertyInserted;
import com.sun.grid.grm.resource.impl.ResourcePropertyUpdated;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.NumberParser;
import java.io.Serializable;
import java.util.Map;

/**
 *   This class defines the a property of a resource type
 * 
 *   @see ResourceType
 */
public class ResourcePropertyType implements Serializable {
    
    private final static long serialVersionUID = -2009072901L;

    private final static String BUNDLE = "com.sun.grid.grm.resource.resource";
    
    private final boolean mandatory;
    private final String  name;
    private final Object defaultValue;
    private final Class<?> type;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourcePropertyType other = (ResourcePropertyType) obj;
        if (this.mandatory != other.mandatory) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.defaultValue != other.defaultValue && (this.defaultValue == null || !this.defaultValue.equals(other.defaultValue))) {
            return false;
        }
        if (this.type != other.type && (this.type == null || !this.type.equals(other.type))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.mandatory ? 1 : 0);
        hash = 23 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 23 * hash + (this.defaultValue != null ? this.defaultValue.hashCode() : 0);
        hash = 23 * hash + (this.type != null ? this.type.hashCode() : 0);
        return hash;
    }
    
    private static Class<?> [] SUPPORTED_TYPES = {
        String.class,
        Integer.class,
        Short.class,
        Long.class,
        Float.class,
        Double.class,
        Boolean.class,
        Usage.class,
        Hostname.class,
        ResourceId.class
    };
    
    private ResourcePropertyType(String name, Class<?> type, boolean mandatory) {
        this(name, type, mandatory, null);
    }
    
    private ResourcePropertyType(String name, Class<?> type, boolean mandatory, Object defaultValue) {
        boolean notsupported = true;
        for(Class<?> t: SUPPORTED_TYPES) {
            if (t.isAssignableFrom(type)) {
                notsupported = false;
                break;
            }
        }
        if(notsupported) {
            throw new IllegalArgumentException(String.format("resource property type %s is not supported", type.getName()));
        }
        this.name = name;
        this.type = type;
        this.mandatory = mandatory;
        this.defaultValue = defaultValue;
    }
    
    /**
     * Create a new mandatory resource property type
     * @param name  the name of the resource property
     * @param type  the type of the resource property
     * @return  the ResourcePropertyType
     */
    public static ResourcePropertyType newMandatory(String name, Class<?> type) {
        return new ResourcePropertyType(name, type, true);
    }
    
    /**
     * Create a new mandatory resource property type with a default value
     * @param name  the name of the resource property
     * @param type  the type of the resource property
     * @param defaultValue the default value
     * @return  the ResourcePropertyType
     */
    public static ResourcePropertyType newMandatory(String name, Class<?> type, Object defaultValue) {
        return new ResourcePropertyType(name, type, true, defaultValue);
    }
    
    /**
     * Create a new optional resource property type
     * @param name  the name of the resource property
     * @param type  the type of the resource property
     * @return  the ResourcePropertyType
     */
    public static ResourcePropertyType newOptional(String name, Class<?> type) {
        return new ResourcePropertyType(name, type, false);
    }

    /**
     * Create a default resource property type
     * @param name  the name of the resource property
     * @return  the ResourcePropertyType
     */
    public static ResourcePropertyType newDefault(String name) {
        return newOptional(name, String.class);
    }

    
    /**
     * Create a new optional resource property type with a default value
     * @param name  the name of the resource property
     * @param type  the type of the resource property
     * @param defaultValue the default value
     * @return  the ResourcePropertyType
     */
    public static ResourcePropertyType newOptional(String name, Class<?> type, Object defaultValue) {
        return new ResourcePropertyType(name, type, false, defaultValue);
    }

    /**
     * Get the name of the resource property
     * @return the name of the resource property
     */
    public String getName() {
        return name;
    }

    /**
     * Get the type of the resource property
     * @return the type of the resoruce property
     */
    public Class<?> getType() {
        return type;
    }
    
    /**
     * Is this property optional
     * @return <code>true</code> if this property is optional
     */
    public boolean isOptional() {
        return !isMandatory();
    }
    
    /**
     * Is this property mandatory
     * @return <code>true</code> if this property is mandatory
     */
    public boolean isMandatory() {
        return mandatory;
    }
    
    /**
     * Get the value of the property from the resource properties
     * @param props  the resource properties
     * @return the value of the property
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if a mandatory property
     *                  is missing or if <code>props</code> does not have a valid value for this property
     *  
     */
    public Object getValue(Map<String,Object> props) throws InvalidResourcePropertiesException {
        Object ret = props.get(name);
        if(ret == null) {
            if(isMandatory()) {
                throw new InvalidResourcePropertiesException("rpt.mpnf", BUNDLE, name);
            } else {
                return null;
            }
        } else if (type.isInstance(ret)) {
            return ret;
        } else if (ret instanceof String) {
            return parse((String)ret);
        } else {
            throw new InvalidResourcePropertiesException("rpt.ivv", BUNDLE, ret, name, type.getSimpleName());
        }
    }
    
    /**
     * Returns a ResourceChanged object that reflects the change on the props
     * (insert, update, delete or no change). Depending on the parameter doModify,
     * the parameter props will be changed or not.
     * <p>
     * A value of null removes the property. If a mandatory property is removed
     * in this way and the mandatory property has a default value then it is
     * reset to this default value. Otherwise an InvalidResourcePropertiesException
     * is thrown.
     * <p>
     * Returns a ResourceChanged object that represents whether the property was
     * inserted, updated or deleted. If no modification was done, this method 
     * returns an instance of ResrouceNotChanged.
     * 
     * @param props   the resource properties, will not be changed
     * @param value   the value
     * @param doModify modify the map in props?
     * @return ResourceChanged object
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if <code>value</code> is not
     *                 valid value for this resource property
     */
    private ResourceChanged diff(Map<String,Object> props, Object value, boolean doModify) throws InvalidResourcePropertiesException {
        Object oldValue = props.get(name);
        if(value == null) {
            // delete property
            if(isMandatory()) {
                if(getDefaultValue() != null) {
                    // reset mandatory property to default value
                    return diff(props, getDefaultValue(), doModify);
                } else {
                    // mandatory properties without default value cannot be deleted
                    throw new InvalidResourcePropertiesException("rpt.cndmp", BUNDLE, name);
                }
            } else if ( oldValue == null ) {
                return ResourceNotChanged.getInstance();
            } else {
                if (doModify) {
                    props.remove(name);
                }
                return new ResourcePropertyDeleted(name, oldValue);
            }
        } else  {
            // set property
            Object newValue = null;
            if (type.isInstance(value)) {
                newValue = value;
            } else if (value instanceof String) {
                newValue = parse((String)value);
            } else {
                throw new InvalidResourcePropertiesException("rpt.ivv", BUNDLE, value, name, type.getSimpleName());
            }
            
            if (newValue.equals(oldValue)) {
                return ResourceNotChanged.getInstance();
            }
            if (doModify) {
                props.put(name, newValue);
            }
            if (oldValue == null) {
                // no old value -> insert
                return new ResourcePropertyInserted(name, newValue);
            } else {
                return new ResourcePropertyUpdated(name, newValue, oldValue);
            }
        }
    }
    
   /**
     * Returns a ResourceChanged object that reflects the change on the props
     * (insert, update, delete or no change). 
     * <p>
     * A value of null removes the property. If a mandatory property is removed
     * in this way and the mandatory property has a default value then it is
     * reset to this default value. Otherwise an InvalidResourcePropertiesException
     * is thrown.
     * <p>
     * Returns a ResourceChanged object that represents whether the property was
     * inserted, updated or deleted. If no modification was done, this method 
     * returns null.
     * 
     * @param props   the resource properties, will not be changed
     * @param value   the value
     * @return ResourceChanged object or null if no change was done
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if <code>value</code> is not
     *                 valid value for this resource property
     */
    public ResourceChanged getChange(Map<String, Object> props, Object value) throws InvalidResourcePropertiesException {
        return diff(props, value, false);
    }
        
    /**
     * Set the value of the property in the resource properties. The properties
     * in the parameter props will be changed accordingly.
     * <p>
     * A value of null removes the property. If a mandatory property is removed
     * in this way and the mandatory property has a default value then it is
     * reset to this default value. Otherwise an InvalidResourcePropertiesException
     * is thrown.
     * <p>
     * Returns a ResourceChanged object that represents whether the property was
     * inserted, updated or deleted. If no modification was done, this method 
     * returns nullt.
     * 
     * @param props   the resource properties, will be changed
     * @param value   the value
     * @return ResourceChanged object or null if no change was done
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if <code>value</code> is not
     *                 valid value for this resource property
     */
    public ResourceChanged setValue(Map<String,Object> props, Object value) throws InvalidResourcePropertiesException {
        return diff(props, value, true);
    }
    
 
    /**
     * Parse the value of for this property
     * @param value  the string value
     * @return the value
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if <code>value</code>
     *             does not a parseable string for this resource property
     */
    public Object parse(String value) throws InvalidResourcePropertiesException {
        
        if(type.equals(String.class)) {
            return value;
        }
        try {
            
            if(type.equals(Integer.TYPE) || type.equals(Integer.class)) {
                return NumberParser.parseInteger(value);
            }

            if(type.equals(Short.TYPE) || type.equals(Short.class)) {
                return NumberParser.parseShort(value);
            }
            
            if(type.equals(Long.TYPE) || type.equals(Long.class)) {
                return NumberParser.parseLong(value);
            }
            
            if(type.equals(Float.TYPE) || type.equals(Float.class)) {
                return NumberParser.parseFloat(value);
            }
            
            if(type.equals(Double.TYPE) || type.equals(Double.class)) {
                return NumberParser.parseDouble(value);
            }
        
            if (type.equals(Boolean.TYPE) || type.equals(Boolean.class)) {
                return Boolean.valueOf(value);
            }

            if (type.equals(Hostname.class)) {
                return Hostname.getInstance(value);
            }
            
            if (type.isAssignableFrom(ResourceId.class)) {
                try {
                    return new ResourceIdImpl(value);
                } catch (InvalidResourceIdException ex) {
                    throw new InvalidResourcePropertiesException(ex.getLocalizedMessage(), ex);
                }
            }
            
            if (type.equals(Usage.class)) {
                return new Usage(Integer.parseInt(value));
            }
            
        } catch(NumberFormatException ex) {
            throw new InvalidResourcePropertiesException("rpt.ivv", BUNDLE, value, name, type.getSimpleName());
        }
        // Can not happen, the constructor checks already that the
        // type is supported
        throw new IllegalStateException(String.format("resource property type %s is not supported", type.getName()));
    }

    /**
     * Get the default value for this resource properties
     * @return the default value
     */
    public Object getDefaultValue() {
        return defaultValue;
    }
}
