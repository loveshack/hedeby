/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;

import java.io.Serializable;

/**
 * This class store the id and the name of a resource
 */
public class ResourceIdAndName implements Serializable {

    private final static long serialVersionUID = -2009072201L;
    
    private final ResourceId id;
    private final String name;

    /**
     * Create a new instanceof of <tt>ResourceIdAndName</tt> out of a resource.
     *
     * @param resource the resource
     */
    public ResourceIdAndName(Resource resource) {
        this(resource.getId(), resource.getName());
    }

    /**
     * Create a new instanceof of <tt>ResourceIdAndName</tt>.
     * @param id the id of the resource
     * @param name  the name of the resource
     */
    public ResourceIdAndName(ResourceId id, String name) {
        this.id = id;
        this.name = name;
    }


    /**
     * get the id of the resource
     * @return the id
     */
    public ResourceId getId() {
        return id;
    }

    /**
     * get the name of the resource
     * @return the name
     */
    public String getName() {
        return name;
    }
}
