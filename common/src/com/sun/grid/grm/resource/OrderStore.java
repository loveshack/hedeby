/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;

import com.sun.grid.grm.resource.impl.NeedProcessor;
import com.sun.grid.grm.resource.order.Order;
import com.sun.grid.grm.util.filter.Filter;
import java.util.List;

/**
 * OrderStore defines methods that has to be supported by implementation
 * in order to use OrderStore for ResourceProvider.
 * 
 */
public interface OrderStore {

    /**
     * Add an order to the end of the queue.
     * @param order the Order to add
     */
    void add(Order order);

    /**
     * Returns the number of stored orders.
     * @return the size of the order queue
     */
    int size();

    /**
     * Returns the first found order for the resource or returns null
     * if there is no order for the resource.
     * 
     * @param orderFilter the filter specifying the template for search
     * @return the first found order matching the filter
     */
    Order get(Filter<Order> orderFilter);

    /**
     * Returns and removes the first found order for the resource or returns null
     * if there is no order for the resource.
     * 
     * @param orderFilter the filter specifying the template for search
     * @return the first found order matching the filter
     */
    Order remove(Filter<Order> orderFilter);

    /**
     * Returns and removes all orders for the resource or an empty list if there
     * is no order for the resource.
     * 
     * @param orderFilter the filter specifying the template for search
     * @return a list of all orders matching the filter
     */
    List<Order> removeAll(Filter<Order> orderFilter);

    /**
     * Removes all orders. 
     * 
     * @return true if all orders were removed
     */
    boolean clear();


    /**
     * Cancel all order that matches a filter.
     * 
     * @param filter the filter
     */
    void cancelOrders(Filter<Order> filter);

    /**
     * Commit all orders that matches a filter
     * @param filter the filter
     */
    public void commitOrders(Filter<Order> filter);

    /**
     * Adapts the NeedProcessors nps for a particular service and SLO. This
     * looks through the existing orders, reuses or changes the NeedProcessors
     * if necessary and deletes obsolete orders for this service and SLO
     * combination.
     * @param nps the list of NeedProcessors of a resource request, will be changed!
     * @param serviceName the service that requested the resources
     * @param sloName the SLO that requested the resources
     */
    public void adaptExistingOrders(List<NeedProcessor> nps, String serviceName, String sloName);
}
