/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract base class for all resource types. 
 * 
 * Defines the basic attributes
 */
public abstract class AbstractResourceType implements ResourceType, Serializable {

    private final static long serialVersionUID = -2009072901L;
    
    /**
     * The static flag
     */
    public static final ResourcePropertyType STATIC = ResourcePropertyType.newMandatory("static", Boolean.class, false);
    
    public static final ResourcePropertyType UNBOUND_NAME = ResourcePropertyType.newMandatory("unbound_name", String.class);
    
    private final Map<String,ResourcePropertyType> propertyTypes = new HashMap<String,ResourcePropertyType>();
    private final Map<String,ResourcePropertyType> unmodifiableProperties = Collections.unmodifiableMap(propertyTypes);

    /** the default resource property type */
    public static final ResourcePropertyType DEFAULT_RESOURCE_PROPERTY_TYPE = ResourcePropertyType.newOptional("default", String.class);
    
    protected AbstractResourceType() {
        addProperty(STATIC);
        addProperty(UNBOUND_NAME);
    }

    /**
     * Get the unbound resource name from the properties of a resource
     * @param props the properties of a resource
     * @return the unbound resource name
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if props does not define the unbound_name properties
     */
    public String getUnboundResourceName(Map<String,Object> props) throws InvalidResourcePropertiesException {
        return UNBOUND_NAME.getValue(props).toString();
    }
    /**
     * Add a new property type
     * @param property the property type
     * @throws java.lang.IllegalArgumentException if a property with this name already exists
     */
    protected void addProperty(ResourcePropertyType property) {
        if(propertyTypes.containsKey(property.getName())) {
            throw new IllegalArgumentException(String.format("property %s already exists", property.getName()));
        }
        propertyTypes.put(property.getName(), property);
    }
    
    /**
     * Get the propertyTypes
     * @return the property types (unmodifiable)
     */
    public Map<String, ResourcePropertyType> getPropertyTypes() {
        return unmodifiableProperties;
    }

    /**
     * Validate a map of resource propertyTypes
     * @param props  the resource propertyTypes
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the propertyTypes are not valid for this ResourceType
     */
    public void validate(Map<String,Object> props) throws InvalidResourcePropertiesException {
        for(ResourcePropertyType p: propertyTypes.values()) {
            p.getValue(props);
        }
        
    }

    /**
     * Set the properties of a host resource that depends on the name of the resource
     * @param name the name of the resource
     * @param props map of resource properties
     */
    protected void setPropertiesForResourceName(String name, Map<String,Object> props) {
        props.put(UNBOUND_NAME.getName(), name);
    }

    /**
     * create the resource properties which can be derived from the resource name
     * @param name the resource name
     * @return the resource properties
     */
    public final Map<String,Object> createResourcePropertiesForName(String name) {
        Map<String,Object> ret = new HashMap<String,Object>();
        setPropertiesForResourceName(name, ret);
        return ret;
    }

    /**
     * Fill the property map with the default values
     * @param props modifiable map
     */
    public void fillResourcePropertiesWithDefaults(Map<String,Object> props) {
        // Fill the default values for missing resource properties
        for(ResourcePropertyType rpt: getPropertyTypes().values()) {
            if (rpt.isMandatory() && rpt.getDefaultValue() != null && !props.containsKey(rpt.getName())) {
                props.put(rpt.getName(), rpt.getDefaultValue());
            }
        }
    }

}
