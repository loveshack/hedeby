/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource;

/**
 * Exception stating the exceptional state during the operation on the 
 * OrderStore.
 *
 */
public class OrderStoreException extends ObjectStoreException {
    private static final long serialVersionUID = -2008121401L;
    /**
     * Creates a new instance of <code>OrderStoreException</code> without detail message.
     */
    public OrderStoreException() {
    }

    /**
     * Constructs an instance of <code>OrderStoreException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public OrderStoreException(String msg) {
        super(msg);
    }

    /**
     * Constructs an instance of <code>OrderStoreException</code> with the specified
     * internationalized detail message.
     * @param msg the detail message.
     * @param bundleName the name of the resource bundle
     * @param params the parameters to the message
     */
    public OrderStoreException(String msg, String bundleName, Object... params) {
        super(msg, bundleName, params);
    }
    /**
     * Constructs an instance of <code>OrderStoreException</code> with the specified
     * internationalized detail message.
     * @param msg the detail message.
     * @param cause the cause of the exception
     * @param bundleName the name of the resource bundle
     * @param params the parameters to the message
     */
    public OrderStoreException(String msg, Throwable cause, String bundleName, Object... params) {
        super(msg, cause, bundleName, params);
    }
}

