/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmSingleton;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.management.Manageable;
import com.sun.grid.grm.resource.management.ManagementEventListener;
import com.sun.grid.grm.resource.management.ResourceProviderEventListener;
import com.sun.grid.grm.service.InvalidServiceException;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.UnknownServiceException;

import com.sun.grid.grm.ui.resource.ResourceActionResult;
import com.sun.grid.grm.util.filter.Filter;
import java.util.List;
import java.util.Map;

/**
 * The ResourceProvider interface is the administrative interface to the resource
 * provider implementation.
 *
 * It is supposed to be exposed as a management bean.
 *
 */
@Manageable
public interface ResourceProvider extends GrmComponent, GrmSingleton {

    /**
     *  Add a ManagementEventListener to the resource provider
     *  @param lis the listener
     *  @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void addManagementEventListener(ManagementEventListener lis) throws GrmRemoteException;

    /**
     *  Remove a ManagementEventListener to the resource provider
     *  @param lis the listener
     *  @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void removeManagementEventListener(ManagementEventListener lis) throws GrmRemoteException;

    /**
     * Add a ResourceProviderEventListener to the resource provider
     * @param lis the listener
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void addResourceProviderEventListener (
            ResourceProviderEventListener lis) throws GrmRemoteException;

    /**
     * Remove a ResourceProviderEventListener to the resource provider
     * @param lis the listener
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void removeResourceProviderEventListener(
            ResourceProviderEventListener lis) throws GrmRemoteException;

    /**
     * Create a new resource and assign it
     * @param type       the type of the resource
     * @param properties the properties of the resource
     * @throws ResourceProviderNotActiveException if the resource provides is not active
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the resource properties are not valid
     * @throws com.sun.grid.grm.GrmRemoteException  communication error
     * @throws com.sun.grid.grm.GrmException if the resource could not be created
     * @return the resource
     */
    Resource addNewResource(ResourceType type, Map<String,Object> properties) throws ResourceProviderNotActiveException, InvalidResourcePropertiesException, GrmRemoteException, GrmException;
    
    /**
     * Remove resources from the system
     * @param idOrNameList list of resource names or resource ids
     * @param descr the removal descriptor
     * @return list of actions that describe the result for each resource
     * @throws ResourceProviderNotActiveException if the resource provider is not active
     * @throws com.sun.grid.grm.GrmRemoteException  communication error
     */
    List<ResourceActionResult> removeResources(List<String> idOrNameList, ResourceRemovalDescriptor descr) throws GrmRemoteException, ResourceProviderNotActiveException;

    /**
     * Reset a list of resources
     * @param idOrNameList resource names or resource ids
     * @return result of the action for each resource
     * @throws ResourceProviderNotActiveException if the reource provider is not active
     *  @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    List<ResourceActionResult>resetResources(List<String> idOrNameList) throws ResourceProviderNotActiveException, GrmRemoteException;
    
    /**
     * Modify a resource.
     * @param resId the resource id
     * @param properties the new properties
     * @return the result of the resource action
     * @throws ResourceProviderNotActiveException if resource provider is not active
     * @throws GrmRemoteException  on any communication error
     */
    ResourceActionResult modifyResource(ResourceId resId, Map<String,Object> properties) throws ResourceProviderNotActiveException, GrmRemoteException;

    /**
     * Modifies resources.
     *
     * @param resIdOrNameMapWithProperties Map containing the resource id or name as key and the new resource
     *             properties as values
     * @return the list of resource action results (for each key in <tt>resIdOrNameMapWithProperties</tt> one result)
     * @throws ResourceProviderNotActiveException if resource provider is not active
     * @throws GrmRemoteException  on any communication error
     */
    List<ResourceActionResult> modifyResources(Map<String, Map<String,Object>> resIdOrNameMapWithProperties)
            throws ResourceProviderNotActiveException, GrmRemoteException;

    /**
     * Get a resource by id or name.
     *
     * @param idOrName
     * @return the resources
     * @throws ServiceNotActiveException if the owning service is not active
     * @throws ResourceProviderNotActiveException if the resource provider is not active
     * @throws com.sun.grid.grm.resource.InvalidResourceException if idOrName does not
     *    address exactly one resource
     * @throws UnknownResourceException If no resource with this id or name has been found
     */
    Resource getResourceByIdOrName(String idOrName) throws ServiceNotActiveException, ResourceProviderNotActiveException, InvalidResourceException, UnknownResourceException;

    /**
     * Gets a list of all resources that belong to the given service and matches
     * a resource filter
     * (cached information).
     *
     * @param service the target service's id
     * @param filter  the resource filter
     * @return a list of resources
     * @throws com.sun.grid.grm.service.UnknownServiceException if the service
     * was not found in the system
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service
     * is not active
     * @throws com.sun.grid.grm.service.InvalidServiceException if the target
     * service is not longer valid
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    List<Resource> getResources(String service, Filter<Resource> filter)
            throws UnknownServiceException, ServiceNotActiveException,
            InvalidServiceException, ResourceProviderNotActiveException, GrmRemoteException;

    /**
     * Get the list of unassigned resources that matches a filter
     * @param filter  the filter
     * @return the list of unassigned resources
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if the resource provider is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    List<Resource> getUnassignedResources(Filter<Resource> filter) throws ResourceProviderNotActiveException, GrmRemoteException;
    
    /**
     * Move resources to a service.
     *
     * @param idOrNameList  list of resource names or resource ids
     * @param service    the target service
     * @param isForced if true, the resource will be removed from its current service
     * by force
     * @param isStatic if true, the resource will be marked as static in target service
     * @return list of resource action that describe the result for each entry of the idOrNameList
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    List<ResourceActionResult> moveResources(List<String> idOrNameList, String service, boolean isForced, boolean isStatic) throws GrmRemoteException, ResourceProviderNotActiveException;

    /**
     * Returns a list of request matching a request filter
     * @param requestFilter the requestFilter
     * @return list of requests
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public List<Request> getRequests(Filter<Request> requestFilter)
            throws ResourceProviderNotActiveException, GrmRemoteException;
    
    /**
     * Registers Id of a resource on a service's resource blacklist. Resource
     * on service's blacklist is not considered as a candidate to be assigned
     * to the service (during the resource request processing or manual
     * move of the resource to the service).
     *
     * @param idOrNameList list of resource ids or resource names
     * @param service name of the service that registers the resource
     *
     * @return list of resource action result that describes what happend for each entry of the
     *          idOrNameList
     * @throws ResourceProviderNotActiveException if the resource provider is not active
     */
    public List<ResourceActionResult> putResourcesOnBlackList(List<String> idOrNameList, String service) throws ResourceProviderNotActiveException;
    
    /**
     * Gets a list of all resource ids that are on a blacklist of specified service. 
     * 
     * @param service name of the service
     * @return the list of blacklisted resource ids
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public List<ResourceIdAndName> getBlackList(String service)
            throws ResourceProviderNotActiveException, GrmRemoteException;
    
    /**
     * Unregisters ID of a resource from a service's resource blacklist.
     *
     * @param resourceId the id to be unregistered
     * @param service name of the service that unregisters the resource
     * @return the number of remaining services that has the resource id on its blacklist
     * @throws com.sun.grid.grm.resource.UnknownResourceException if resource is not
     * on service's blacklist
     * @throws com.sun.grid.grm.resource.ResourceProviderNotActiveException if RP is not active
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
     public int removeResourceIDFromBlackList(ResourceId resourceId, String service) 
            throws UnknownResourceException, ResourceProviderNotActiveException, GrmRemoteException;

    /**
     * Remove resources from the blacklist of a service. The resource are addressed
     * by it's name or id.
     * @param idOrNameList list of resource names or ids
     * @param service the name of the service
     * @return list of action resource for each entry if the idOrNameList
     * @throws ResourceProviderNotActiveException if the resource provider is not active
     */
     public List<ResourceActionResult> removeResourcesFromBlackList(List<String> idOrNameList, String service) throws ResourceProviderNotActiveException;
     
}
