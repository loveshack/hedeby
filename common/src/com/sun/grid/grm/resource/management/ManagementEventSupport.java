/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.management;

import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.util.EventListenerSupport;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class for firing the management events.
 */
public class ManagementEventSupport {

    private final static String BUNDLE = "com.sun.grid.grm.resource.management.messages";
    private static Logger log = Logger.getLogger(ManagementEventSupport.class.getName(), BUNDLE);
    private final EventListenerSupport<ManagementEventListener> listeners;
    private final AtomicLong eventSequenceNumber;
    private final String rpName;

    /**
     * Default constructor. The resource provider name is needed for monitoring
     * purposes - for identifying the source of management events.
     * @param resourceProviderName
     */
    public ManagementEventSupport(String resourceProviderName) {
        listeners = EventListenerSupport.<ManagementEventListener>newAsynchronInstance(ManagementEventListener.class);
        eventSequenceNumber = new AtomicLong();
        rpName = resourceProviderName;
    }

    /**
     * Register a <code>ManagementEventListener</code>.
     *
     * @param eventListener the management listener
     */
    public void addManagementEventListener(ManagementEventListener eventListener) {
        listeners.addListener(eventListener);
    }

    /**
     * Deregister a <code>ManagementEventListener</code>.
     *
     * @param eventListener the management listener
     */
    public void removeManagementEventListener(ManagementEventListener eventListener) {
        listeners.removeListener(eventListener);
    }

    /**
     * Send a process request event
     * @param serviceName name of the requesting service
     * @param needs list of resource needs
     */
    public void fireProcessRequest(String serviceName, List<Need> needs) {

        ProcessRequestEvent evt = new ProcessRequestEvent(eventSequenceNumber.incrementAndGet(), rpName,
                serviceName, needs);

        if (log.isLoggable(Level.FINER)) {
            log.log(Level.FINER, "fireevent", evt);
        }

        listeners.getProxy().processRequest(evt);
    }

    /**
     * Fire a request processed event.
     * @param serviceName name of the requesting service
     * @param needs list of resource needs
     */
    public void fireRequestProcessed(String serviceName, List<Need> needs) {

        RequestProcessedEvent evt = new RequestProcessedEvent(eventSequenceNumber.incrementAndGet(),
                rpName, serviceName, needs);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().requestProcessed(evt);

    }

    /**
     * Fire a request queued event.
     * @param serviceName name of the requesting service
     * @param needs list of needs
     */
    public void fireRequestQueued(String serviceName, List<Need> needs) {

        RequestQueuedEvent evt = new RequestQueuedEvent(eventSequenceNumber.incrementAndGet(), rpName,
                serviceName, needs);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }
        listeners.getProxy().requestQueued(evt);
    }

    /**
     * Fire a request queued event for each service in the map.
     * @param servicesNeeds map of service-list of needs pair
     */
    public void fireRequestsQueued(Map<String, List<Need>> servicesNeeds) {

        for (Map.Entry<String, List<Need>> entry : servicesNeeds.entrySet()) {
            fireRequestQueued(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Fire a request pending event.
     * @param serviceName name of the requesting service
     * @param needs list of needs
     */
    public void fireRequestPending(String serviceName, List<Need> needs) {

        RequestPendingEvent evt = new RequestPendingEvent(eventSequenceNumber.incrementAndGet(), rpName,
                serviceName, needs);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }
        listeners.getProxy().requestPending(evt);
    }
}
