/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.management;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.service.event.*;
import com.sun.grid.grm.util.EventListenerSupport;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class for firing the management events.
 */
public class ResourceProviderEventSupport {

    private final static String BUNDLE = "com.sun.grid.grm.resource.management.messages";
    private static Logger log = Logger.getLogger(ResourceProviderEventSupport.class.getName(), BUNDLE);
    private final EventListenerSupport<ResourceProviderEventListener> listeners;
    private final AtomicLong eventSequenceNumber;
    private final String rpName;

    public ResourceProviderEventSupport(String resourceProviderName) {
        listeners = EventListenerSupport.<ResourceProviderEventListener>newAsynchronInstance(ResourceProviderEventListener.class);
        eventSequenceNumber = new AtomicLong();
        rpName = resourceProviderName;
    }

    /**
     * Register a <code>ManagementEventListener</code>.
     *
     * @param eventListener the management listener
     */
    public void addResourceProviderEventListener(ResourceProviderEventListener eventListener) {
        listeners.addListener(eventListener);
    }

    /**
     * Deregister a <code>ManagementEventListener</code>.
     *
     * @param eventListener the management listener
     */
    public void removeResourceProviderEventListener(ResourceProviderEventListener eventListener) {
        listeners.removeListener(eventListener);
    }

    /**
     * Fire a add resource event.
     * @param resource the resource which will be added
     * @param message message for the for monitoring
     */
    public void fireAddResource(Resource resource, String message) {

        AddResourceEvent evt = new AddResourceEvent(eventSequenceNumber.incrementAndGet(),
                rpName, resource, message);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().addResource(evt);

    }

    /**
     * Fire a resource added event.
     * @param resource the resource which has been added
     * @param message the message for monitoring
     */
    public void fireResourceAdded(Resource resource, String message) {

        ResourceAddedEvent evt = new ResourceAddedEvent(eventSequenceNumber.incrementAndGet(),
                rpName, resource, message);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }
        listeners.getProxy().resourceAdded(evt);
    }

    /**
     * Fire a remove resource event
     * @param resource The resource which will be removed
     * @param message The message for monitoring
     */
    public void fireRemoveResource(Resource resource, String message) {
        RemoveResourceEvent evt = new RemoveResourceEvent(eventSequenceNumber.incrementAndGet(),
                rpName, resource, message);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().removeResource(evt);
    }

    /**
     * Fire a resource removed event.
     * @param resource The resource which has been removed
     * @param message The message for monitoring
     */
    public void fireResourceRemoved(Resource resource, String message) {

        ResourceRemovedEvent evt = new ResourceRemovedEvent(eventSequenceNumber.incrementAndGet(),
                rpName, resource, message);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().resourceRemoved(evt);
    }
    
    /**
     * Fire a resource removed event.
     * @param resource The resource which has been removed
     * @param message The message for monitoring
     */
    public void fireResourceRejected(Resource resource, String message) {

        ResourceRejectedEvent evt = new ResourceRejectedEvent(eventSequenceNumber.incrementAndGet(),
                rpName, resource, message);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().resourceRejected(evt);
    }

    /**
     * Fire a resource error event.
     * @param resource the error resource 
     * @param message the message for monitoring
     */
    public void fireResourceError(Resource resource, String message) {

        ResourceErrorEvent evt = new ResourceErrorEvent(eventSequenceNumber.incrementAndGet(),
                rpName, resource, message);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().resourceError(evt);
    }

    /**
     * Fire a resource reset event
     * @param resource the resource which has been reset
     * @param message The message for monitoring
     */
    public void fireResourceReset(Resource resource, String message) {

        ResourceResetEvent evt = new ResourceResetEvent(eventSequenceNumber.incrementAndGet(),
                rpName, resource, message);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().resourceReset(evt);
    }

    /**
     * Fire a resource properties changed event
     * @param resource the resource
     * @param changed the changed properties and attributes of the resource
     * @param message message for the monitoring
     */
    public void fireResourceChanged(Resource resource, Collection<ResourceChanged> changed, String message) {

        ResourceChangedEvent evt = new ResourceChangedEvent(eventSequenceNumber.incrementAndGet(),
                rpName, resource, changed, message);

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "fireevent", evt);
        }

        listeners.getProxy().resourceChanged(evt);
    }
}
