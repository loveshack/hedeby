/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.management;

import java.io.Serializable;

/**
 * AbstractManagementEvent provides default implementation for operations that
 * are common to all management events.
 */
public abstract class AbstractManagementEvent implements Serializable, Comparable<AbstractManagementEvent> {

    /**
     * The serial version UID
     */
    private static final long serialVersionUID = -2009081401L;
    private final long timestamp;
    private final long sequenceNumber;
    private final String managerName;
    private transient int hash;

    /**
     * Creates a new instance of AbstractManagementEvent
     * @param aSequenceNumber the sequence number for the event
     * @param rpName name of the resource provider
     */
    public AbstractManagementEvent(long aSequenceNumber, String rpName) {
        if (rpName == null) {
            throw new NullPointerException();
        }
        timestamp = System.currentTimeMillis();
        sequenceNumber = aSequenceNumber;
        managerName = rpName;
    }

    /**
     * get the sequence number of the event
     * @return the sequence number
     */
    public long getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * get the name of the manager's class
     * @return the name of the manager's class
     */
    public String getResourceProviderName() {
        return managerName;
    }

    /**
     * Compare with another management event.
     *
     * @param o the other management event
     * @return  a negative integer, zero, or a positive integer as this object is
     *          less than, equal to, or greater than the specified object.
     */
    public int compareTo(AbstractManagementEvent o) {
        int ret = managerName.compareTo(o.getResourceProviderName());
        if (ret == 0) {
            if (sequenceNumber < o.sequenceNumber) {
                ret = -1;
            } else if (sequenceNumber > o.sequenceNumber) {
                ret = 1;
            } else {
                ret = 0;
            }
        }
        return ret;
    }

    /**
     *  test if this is equal to a object.
     *
     *  @param obj the object
     *  @return <code>true</code> if this is equal to <code>object</code>
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AbstractManagementEvent) {
            AbstractManagementEvent e = (AbstractManagementEvent) obj;
            return sequenceNumber == e.sequenceNumber && managerName.equals(e.managerName);
        }
        return false;
    }

    /**
     *  Get the hashcode of this object
     *
     *  @return the hashcode
     */
    @Override
    public int hashCode() {
        int h = hash;
        if (h == 0) {
            h = (int) (sequenceNumber ^ (sequenceNumber >>> 32));
            h = 31 * h + managerName.hashCode();
            hash = h;
        }
        return h;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }
}
