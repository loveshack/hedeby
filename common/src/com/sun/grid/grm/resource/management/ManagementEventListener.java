/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource.management;

import com.sun.grid.grm.management.ManagedEventListener;
import java.util.EventListener;

/**
 * This interface is used for listening for events from the resource
 * provider.
 */
@ManagedEventListener
public interface ManagementEventListener extends EventListener {

    /**
     * This event occurs whenever a service request is received by resource provider
     * and the request is queued.  The event contains the service's id and the list of needs.
     * @param event
     */
    public void requestQueued(RequestQueuedEvent event);
    
    /**
     * This event occurs whenever a service request is procssed but not fulfilled by resource provider
     * and the request is pending (waiting for a change is sytem to be queued again).  
     * The event contains the service's id and the list of needs.
     * @param event
     */
    public void requestPending(RequestPendingEvent event);
    
    /**
     * This event occurs whenever a service request is received by resource provider
     * and the request is queued.  The event contains the service's id and the list of needs
     * that are about to be fulfilled.
     * @param event
     */
    public void processRequest(ProcessRequestEvent event);
    
    /**
     * This event occurs whenever a service request is received by resource provider
     * and the request is queued.  The event contains the service's id and the list of needs
     * that were not fulfilled.
     * @param event
     */
    public void requestProcessed(RequestProcessedEvent event);

}
