/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.resource.management;

import com.sun.grid.grm.management.ManagedEventListener;
import com.sun.grid.grm.service.event.AddResourceEvent;
import com.sun.grid.grm.service.event.RemoveResourceEvent;
import com.sun.grid.grm.service.event.ResourceAddedEvent;
import com.sun.grid.grm.service.event.ResourceErrorEvent;
import com.sun.grid.grm.service.event.ResourceChangedEvent;
import com.sun.grid.grm.service.event.ResourceRejectedEvent;
import com.sun.grid.grm.service.event.ResourceRemovedEvent;
import com.sun.grid.grm.service.event.ResourceResetEvent;
import java.util.EventListener;

@ManagedEventListener
public interface ResourceProviderEventListener extends EventListener {

    /**
     *  The RP informs the listeners that is starting to add a resource.
     *
     *  @param event  the event object
     */
    public void addResource(AddResourceEvent event);

    /**
     *  The RP informs the listeners that a resource has been added.
     *
     *  @param event  the event object
     */
    public void resourceAdded(ResourceAddedEvent event);
    
    /**
     *  The RP informs the listeners that process of adding a resource failed.
     *
     *  @param event  the event object
     */
    public void resourceRejected(ResourceRejectedEvent event);

    /**
     *  The RP informs the listeners that a resource is added to the service
     *
     *  @param event  the event object
     */
    public void removeResource(RemoveResourceEvent event);

    /**
     *  The RP informs the listeners that has been removed from the service.
     *
     *  @param event  the event object
     */
    public void resourceRemoved(ResourceRemovedEvent event);

    /**
     *  The RP informs the listener that an unexpected error has been occurred
     *  on a resource.
     *
     *  <p>This error can not be recovered. Administrator has to look on the resource
     *  and clean it up.</p>
     *
     *  @param event  the event object
     */
    public void resourceError(ResourceErrorEvent event);

    /**
     *  The RP reports that a resoruce has been reset. The RP will
     *  assign the resource to first avaialable service.
     *
     *  @param event  the event object
     */
    public void resourceReset(ResourceResetEvent event);

    /**
     * The RP reports changes in the resource properties and attributes with this method
     *
     *  @param event  the event object
     */
    public void resourceChanged(ResourceChangedEvent event);
}
