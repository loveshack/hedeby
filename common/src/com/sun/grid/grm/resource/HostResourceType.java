/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.resource;

import com.sun.grid.grm.util.Hostname;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  The type of a host resource
 */
public class HostResourceType extends AbstractResourceType implements Serializable {
    
    private final static long serialVersionUID = -2009072901L;

    private final static String BUNDLE = "com.sun.grid.grm.resource.resource";
    
    /**
     * Hostname
     */
    public static final ResourcePropertyType HOSTNAME = ResourcePropertyType.newOptional("resourceHostname", Hostname.class);
    
    /**
     * IP Address
     */
    public static final ResourcePropertyType IPADDRESS = ResourcePropertyType.newOptional("resourceIPAddress", String.class);
    
    /**
     * Architecture type, e.g. sparc64, x86
     */
    public static final ResourcePropertyType HW_CPU_ARCH = ResourcePropertyType.newOptional("hardwareCpuArchitecture", String.class);
    
    /**
     * CPU Count
     */
    public static final ResourcePropertyType HW_CPU_COUNT = ResourcePropertyType.newOptional("hardwareCpuCount", Integer.class);
    
    /**
     * CPU clock speed in MHz
     */
    public static final ResourcePropertyType HW_CPU_FREQUENCY = ResourcePropertyType.newOptional("hardwareCpuFrequency", String.class);
    
    
    /**
     * The total memory of a host
     */
    public static final ResourcePropertyType HW_TOTAL_MEMORY = ResourcePropertyType.newOptional("hardwareTotalMemory", Double.class);
    
    /**
     * The size of swap of a host
     */
    public static final ResourcePropertyType TOTAL_SWAP = ResourcePropertyType.newOptional("totalSwap", Double.class);
    
    /**
     * The size of virtual memory of a host
     */
    public static final ResourcePropertyType TOTAL_VIRTUAL_MEMORY = ResourcePropertyType.newOptional("totalVirtualMemory", Double.class);
    
    
    /**
     * Operating system name, e.g. Solaris, Windows
     */
    public static final ResourcePropertyType OS_NAME = ResourcePropertyType.newOptional("operatingSystemName", String.class);
    
    /**
     * Operating system version number
     */
    public static final ResourcePropertyType OS_RELEASE = ResourcePropertyType.newOptional("operatingSystemRelease", String.class);
    
    /**
     * Operating system patch level
     */
    public static final ResourcePropertyType OS_PATCHLEVEL = ResourcePropertyType.newOptional("operatingSystemPatchlevel", String.class);
    
    /**
     * Operating system vendor, e.g. RedHat, Sun
     */
    public static final ResourcePropertyType OS_VENDOR = ResourcePropertyType.newOptional("operatingSystemVendor", String.class);

    
    /**
     * name of the HostResourceType
     */
    public final static String HOST_TYPE = "host";
    
    private final static HostResourceType instance = new HostResourceType();
    
    private HostResourceType() {
        addProperty(HOSTNAME);
        addProperty(IPADDRESS);
        addProperty(HW_CPU_ARCH);
        addProperty(HW_CPU_COUNT);
        addProperty(HW_CPU_FREQUENCY);
        addProperty(HW_TOTAL_MEMORY);
        addProperty(TOTAL_SWAP);
        addProperty(TOTAL_VIRTUAL_MEMORY);
        addProperty(OS_NAME);
        addProperty(OS_RELEASE);
        addProperty(OS_PATCHLEVEL);
        addProperty(OS_VENDOR);
    }
  
    /**
     * Get the name of the ResourceType
     * @return the name
     */
    public String getName() {
        return HOST_TYPE;
    }

    /**
     * Get the name of the resource from its properties
     * 
     * @param properties the resource properties
     * @return if the resourceHostname property is found the content of this
     *         property is returned other the unboud resource name is returned
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException
     */
    public String getResourceName(Map<String, Object> properties) throws InvalidResourcePropertiesException {
        Object value = HOSTNAME.getValue(properties);
        if (value != null) {
            return value.toString();
        } else {
            return getUnboundResourceName(properties);
        }
    }

    /**
     * Get the bound state of the resource by evaluating the properties of the
     * resource.
     *
     * <p>A host resource is considered as bound of the resourceHostname properties
     *    is set.</p>
     * 
     * @param properties the resource properties
     * @return <code>true</code> if the resource is bound
     */
    public boolean isResourceBound(Map<String,Object> properties) {
        try {
            return HOSTNAME.getValue(properties) != null;
        } catch (InvalidResourcePropertiesException ex) {
            return false;
        }
    }

    
    /**
     * Get the instance of the HostResourceType
     * @return the instance of the HostResourceType
     */
    public static HostResourceType getInstance() {
        return instance;
    }

    /**
     * The the properties of a host resource that depends on the name of the resource
     * @param name the name of the resource
     * @param props map of resource properties
     */
    @Override
    protected void setPropertiesForResourceName(String name, Map<String,Object> props) {
        Hostname hostname = Hostname.getInstance(name);
        if (hostname.isResolved()) {
            props.put(HOSTNAME.getName(), hostname);
        }
        super.setPropertiesForResourceName(name, props);
    }

    /**
     * Fill the property map with the default values
     * @param props modifiable map
     */
    @Override
    public void fillResourcePropertiesWithDefaults(Map<String,Object> props) {
        super.fillResourcePropertiesWithDefaults(props);
        try {
            Hostname hostname = (Hostname) HOSTNAME.getValue(props);
            if (!props.containsKey(UNBOUND_NAME.getName()) && hostname != null && hostname.isResolved()) {
                UNBOUND_NAME.setValue(props, hostname.getHostname());
            }
        } catch (InvalidResourcePropertiesException ex) {
            // We have no hostname
            // => do not set the unbound name
        }
    }

}