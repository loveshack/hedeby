/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.event.ComponentEventSupport;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.GetConfigurationCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.InvalidStateTransistionException;
import com.sun.grid.grm.util.StateMachine;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to ensure the correct state transitions and
 * the component is in the correct state when it is called.<p>
 * The derived component should set the name field, otherwise
 * a default string is used as name.<p>
 * @param <C> The type of configuration
 */
public abstract class GrmComponentBase<C> implements GrmComponent {

    /**
     * Bundle which contains i18n strings
     */
    private static final String BUNDLE = "com.sun.grid.grm.messages";
    private final static Logger log = Logger.getLogger(GrmComponentBase.class.getName(), BUNDLE);
    private final static StateMachine<ComponentState> STATE_MACHINE = new StateMachine<ComponentState>();
    

    static {
        // First parameter is the state, the following parameters defines the
        // allowed state transitions
        STATE_MACHINE.addState(ComponentState.STARTING, ComponentState.STARTED, ComponentState.STOPPING, ComponentState.STOPPED);
        STATE_MACHINE.addState(ComponentState.STARTED, ComponentState.STOPPING, ComponentState.RELOADING, ComponentState.STOPPED);
        STATE_MACHINE.addState(ComponentState.STOPPING, ComponentState.STOPPED);
        STATE_MACHINE.addState(ComponentState.STOPPED, ComponentState.STARTING, ComponentState.STARTED);
        STATE_MACHINE.addState(ComponentState.RELOADING, ComponentState.STARTED, ComponentState.STOPPED, ComponentState.STOPPING);
        STATE_MACHINE.addState(ComponentState.UNKNOWN, ComponentState.STARTING, ComponentState.STARTED);
    }
    private final StateMachine<ComponentState>.Instance state = STATE_MACHINE.newInstance(ComponentState.STOPPED);
    /** The execution env */
    protected final ExecutionEnv env;
    protected final ComponentEventSupport cmpEventSupport;
    /**
     * stores the name of the component. Used for exceptions.
     */
    private String name = null;
    /**
     * host on which the component runs. Used for events.
     */
    private final Hostname host;

    /** 
     * Creates a new instance of GrmComponentBase.
     *
     * @param env   the execution of of the Hedeby system
     * @param name  the name for the component
     */
    public GrmComponentBase(ExecutionEnv env, String name) {
        this.env = env;
        this.name = name;
        this.host = Hostname.getLocalHost();
        this.cmpEventSupport = ComponentEventSupport.newInstance(name, host);
    }

    /**
     * Get the configuration of the component.
     *
     * @throws com.sun.grid.grm.GrmException if the configuration was not found
     * @return the configuration
     */
    protected C getConfig() throws GrmException {
        GetConfigurationCommand<C> cmd = new GetConfigurationCommand<C>(name);
        Result<C> r = env.getCommandService().<C>execute(cmd);
        return r.getReturnValue();
    }

    /**
     * Returns the name of a component.
     *
     * @return component name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the name of host on which runs the component.
     *
     * @return host name
     */
    public Hostname getHostname() throws GrmRemoteException {
        return host;
    }

    /**
     * Return the execution environment for this component.
     *
     * @return the execution environment
     */
    public ExecutionEnv getExecutionEnv() {
        return env;
    }

    /**
     * Returns the current component state.
     * @return Current state.
     */
    public ComponentState getState() {
        return state.getState();
    }

    /**
     * Sets the current component state.
     *
     * @param newState the new state
     * @return the original state
     * @throws com.sun.grid.grm.util.InvalidStateTransistionException if 
     *            the state transition is not allowed
     */
    protected final ComponentState setState(ComponentState newState) throws InvalidStateTransistionException {
        ComponentState oldState = this.state.setState(newState);

        if (!oldState.equals(newState)) {
            cmpEventSupport.fireComponentStateChangedEvent(newState);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "componentBase.stateChanged", new Object[]{getName(), oldState, newState});
            }
        }
        return oldState;
    }

    /**
     * Register a <code>ComponentEventListener</code>.
     *
     * @param listener  the listener
     */
    public void addComponentEventListener(ComponentEventListener listener) {
        cmpEventSupport.addComponentEventListener(listener);
    }

    /**
     * Deregister a <code>ComponentEventListener</code>.
     *
     * @param listener  the listener
     */
    public void removeComponentEventListener(ComponentEventListener listener) {
        cmpEventSupport.removeComponentEventListener(listener);
    }
}
