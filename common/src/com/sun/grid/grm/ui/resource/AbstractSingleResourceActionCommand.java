/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.Collections;
import java.util.List;

/**
 *  Abstract base class for all command which performs an action on list of
 *  components
 */
public abstract class AbstractSingleResourceActionCommand extends AbstractResourceActionCommand {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.resource.messages";
    private static final long serialVersionUID = -2008040301L;

    /**
     * This method performs the action with the resource
     * @param env the exection environment
     * @throws com.sun.grid.grm.GrmException on any error
     * @return the success message
     */
    protected abstract ResourceActionResult doAction(ExecutionEnv env) throws GrmException;

    /**
     * This method searchs first all matching components and call then
     * for each component the <code>changeComponent</code> method
     * @param env   the exection environment
     * @throws com.sun.grid.grm.GrmException on any error
     * @return list a result for each matching component
     */
    public Result<List<ResourceActionResult>> execute(ExecutionEnv env) throws GrmException {

        ResourceActionResult res = doAction(env);
        
        return new CommandResult<List<ResourceActionResult>>(Collections.singletonList(res));
    }
}
