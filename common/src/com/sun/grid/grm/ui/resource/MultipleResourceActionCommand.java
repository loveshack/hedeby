/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.EventListenerSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 *  Abstract base class for all command which performs an action on list of
 *  components
 * @param <T> The type of the action
 */
public class MultipleResourceActionCommand<T extends AbstractSingleResourceActionCommand>  extends AbstractResourceActionCommand {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.resource.messages";
    private final EventListenerSupport<Listener> evtSupport = EventListenerSupport.<Listener>newSynchronInstance(Listener.class);
    private final Collection<T> actions;
    
    /**
     * Creates an instance of MultipleResourceActionCommand
     * @param actions the actions for this command
     * @throws com.sun.grid.grm.GrmException if list of target resources is null
     */
    public MultipleResourceActionCommand(Collection<T> actions) throws GrmException {
        this.actions = new ArrayList<T>(actions);
    }

    /**
     * This method searchs first all matching components and call then
     * for each component the <code>changeComponent</code> method
     * @param env   the exection environment
     * @throws com.sun.grid.grm.GrmException on any error
     * @return list a result for each matching component
     */
    public Result<List<ResourceActionResult>> execute(ExecutionEnv env) throws GrmException {

        try {
            List<ResourceActionResult> affectedResources = new LinkedList<ResourceActionResult>();
            for(T action: actions) {
                ResourceActionResult result = action.doAction(env);
                affectedResources.add(result);
                evtSupport.getProxy().commandExecuted(result);
            }
            return new CommandResult<List<ResourceActionResult>>(affectedResources);
        } finally {
            evtSupport.getProxy().finished();
        }
    }

    /**
     * Get the number of actions
     * @return the number of actions
     */
    public int getActionCount() {
        return actions.size();
    }

    /**
     * This listener can be used to inform the caller that a resourcecommand has been executed
     * and the complete command is finished
     */
    public interface Listener {
        /**
         * A command on a resource has been executed
         * @param result the result of the resource action
         */
        public void commandExecuted(ResourceActionResult result);
        
        /**
         * The component command has been finished
         */
        public void finished();
    }
    
    
    /**
     * Register a listener
     * @param lis the listener
     */
    public void addListener(Listener lis) {
        evtSupport.addListener(lis);
    }
    
    /**
     * Unregister a listener
     * @param lis
     */
    public void removeListener(Listener lis) {
        evtSupport.removeListener(lis);
    }
     
}
