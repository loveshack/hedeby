/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdAndName;
import java.io.Serializable;

/**
 *  Instances of this class contains the result of an action which
 *  has been execution on a resource
 *
 *  @see com.sun.grid.grm.ui.resource.AbstractResourceActionCommand
 */
public class ResourceActionResult implements Serializable {

    private static final long serialVersionUID = -2008040301L;
    private final ResourceIdAndName idAndName;
    private final String message;
    private final Throwable error;

    /**
     * Create a new instance of ResourceActionResult
     * @param r       the affected resource id
     * @param resourceName the name of the resource
     * @param message  the result message
     */
    public ResourceActionResult(ResourceId r, String resourceName, String message) {
        this(r, resourceName, message, null);
    }

    /**
     * Create a new instance of ResourceActionResult
     * @param r       the affected resource id
     * @param resourceName the name of the resource
     * @param message  the result message
     * @param error    the error which has been occured
     */
    public ResourceActionResult(ResourceId r, String resourceName, String message, Throwable error) {
        this(new ResourceIdAndName(r,resourceName), message, error);
    }

    /**
     * Create a new instance of ResourceActionResult
     * @param idAndName the id and name of the resource
     * @param message the message
     */
    public ResourceActionResult(ResourceIdAndName idAndName, String message) {
        this(idAndName, message, null);
    }

    /**
     * Create a new instance of ResourceActionResult
     *
     * @param idAndName the id and name of the resource
     * @param message the message
     * @param error the error
     */
    public ResourceActionResult(ResourceIdAndName idAndName, String message, Throwable error) {
        this.idAndName = idAndName;
        this.message = message;
        this.error = error;
    }

    /**
     * Get the affected resource id
     * @return the id of resource that is the target of an action
     */
    public ResourceId getResourceId() {
        return idAndName.getId();
    }

    /**
     * The message (will be displayed in the ui)
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * get the name of the resource
     * @return the name of the resource
     */
    public String getResourceName() {
        return idAndName.getName();
    }
    
    /**
     * Get the error of the action
     * @return the error of the action
     */
    public Throwable getError() {
        return error;
    }
}
