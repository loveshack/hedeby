/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceProvider;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Commands that modifies an existing resource in a system.
 * 
 */
public class ModifyResourceCommand extends AbstractSingleResourceActionCommand {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.resource.messages";
    private static final long serialVersionUID = -2008040301L;
    private final Map<String, Object> resourceProperties;
    private final String idOrName;
    private final ResourceId resId;

    public ModifyResourceCommand(String idOrName, Map<String, Object> properties) {
        this(null, idOrName, properties);
    }

    public ModifyResourceCommand(ResourceId resId, String name, Map<String, Object> properties) {
        if (properties == null || properties.isEmpty()) {
            throw new NullPointerException("Need resource properties.");
        }
        resourceProperties = properties;
        this.idOrName = name;
        this.resId = resId;
    }


    @Override
    protected ResourceActionResult doAction(ExecutionEnv env) throws GrmException {

        ResourceProvider rp = ComponentService.<ResourceProvider>getComponentByType(env, ResourceProvider.class);
        if (rp == null) {
            throw new GrmException("ModifyResourceCommand.rp.notfound", BUNDLE_NAME);
        }
        if (resId == null) {
            List<ResourceActionResult> result = rp.modifyResources(Collections.singletonMap(idOrName, resourceProperties));
            return result.get(0);
        } else {
            return rp.modifyResource(resId, resourceProperties);
        }
        
    }

}
