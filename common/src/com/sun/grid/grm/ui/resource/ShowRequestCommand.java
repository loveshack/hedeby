/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.Request;
import com.sun.grid.grm.resource.RequestState;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.resource.filter.RequestVariableResolver;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.I18NManager;

import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.FilterException;
import com.sun.grid.grm.util.filter.OrFilter;
import com.sun.grid.grm.util.filter.RegExpFilter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


/**
 * This command is responsible for showing the queued or the pending requests
 * as registered by resource provider.
 */
public class ShowRequestCommand extends AbstractLocalCommand<Map<String, List<Request>>> {
    /**
    * i18n Bundle name
    */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.resource.messages";
    private static final Logger log = Logger.getLogger(ShowRequestCommand.class.getName());
    private static final long serialVersionUID = -2007101201L;
    private boolean queued = false;
    private boolean pending = false;
    private String queuedLabel;
    private String pendingLabel;
    private Filter<Request> serviceFilter;
    private Filter<Request> sloFilter;

    public ShowRequestCommand(boolean queued, boolean pending) {
        this.queued = queued;
        this.pending = pending;

        if (!queued && !pending) {
            this.queued = true;
            this.pending = true;
        }

        this.queuedLabel = I18NManager.formatMessage("ShowRequestCommand.ql",
                BUNDLE_NAME);
        this.pendingLabel = I18NManager.formatMessage("ShowRequestCommand.pl",
                BUNDLE_NAME);
    }

    public Result<Map<String, List<Request>>> execute(ExecutionEnv env)
        throws GrmException {
        Map<String, List<Request>> resMap = new HashMap<String, List<Request>>();
        ResourceProvider ret = ComponentService.<ResourceProvider>getComponentByType(env,
                ResourceProvider.class);

        if (ret == null) {
            throw new GrmException("ShowRequestCommand.rp.notfound", BUNDLE_NAME);
        }
        
        AndFilter<Request> filter = new AndFilter<Request>(4);
        if(serviceFilter != null) {
            filter.add(serviceFilter);
        }
        if(sloFilter != null) {
            filter.add(sloFilter);
        }

        if(queued && pending) {
            OrFilter<Request> orFilter = new OrFilter<Request>();
            orFilter.add(new CompareFilter<Request>(RequestVariableResolver.STATE_VAR, 
                         new FilterConstant<Request>(RequestState.QUEUED.toString()), CompareFilter.Operator.EQ));
            orFilter.add(new CompareFilter<Request>(RequestVariableResolver.STATE_VAR, 
                         new FilterConstant<Request>(RequestState.PENDING.toString()), CompareFilter.Operator.EQ));
            filter.add(orFilter);
        } else if (queued) {
            filter.add(new CompareFilter<Request>(RequestVariableResolver.STATE_VAR, 
                       new FilterConstant<Request>(RequestState.QUEUED.toString()), CompareFilter.Operator.EQ));
        } else if(pending) {
            filter.add(new CompareFilter<Request>(RequestVariableResolver.STATE_VAR, 
                       new FilterConstant<Request>(RequestState.PENDING.toString()), CompareFilter.Operator.EQ));
        }
        
        List<Request> requests = ret.getRequests(filter);
        LinkedList<Request> pendingList = new LinkedList<Request>();
        LinkedList<Request> queuedList = new LinkedList<Request>();
        
        for(Request r: requests) {
            switch(r.getState()) {
                case PENDING:
                    pendingList.add(r);
                    break;
                case QUEUED:
                    queuedList.add(r);
                    break;
                default: // Ignore
            }
        }
        if (!queuedList.isEmpty()) {
            resMap.put(queuedLabel, queuedList);
        }

        if (!pendingList.isEmpty()) {
            resMap.put(pendingLabel, pendingList);
        }

        return new CommandResult<Map<String, List<Request>>>(resMap);
    }
    
    /**
     * Set the service name pattern
     * @param service the service name pattern
     * @throws com.sun.grid.grm.util.filter.FilterException if service is not a valid regular expression
     */
    public void setService(String service) throws FilterException {
        if(service != null) {
            serviceFilter = new RegExpFilter<Request>(RequestVariableResolver.SERVICE_VAR, service);
        } else {
            serviceFilter = null;
        }
    }
    
    /**
     * Set the slo name pattern
     * @param slo the service name pattern
     * @throws com.sun.grid.grm.util.filter.FilterException if service is not a valid regular expression
     */
    public void setSLO(String slo) throws FilterException {
        if(slo != null) {
            sloFilter = new RegExpFilter<Request>(RequestVariableResolver.SLO_VAR, slo);
        } else {
            sloFilter = null;
        }
    }
    
}
