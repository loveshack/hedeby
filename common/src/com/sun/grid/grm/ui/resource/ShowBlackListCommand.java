/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdAndName;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.I18NManager;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


/**
 * This command is responsible for retrieving the list of resource id that are 
 * ignored by a service (eg. they are on service's resource blacklist).
 */
public class ShowBlackListCommand extends AbstractLocalCommand<List<ShowBlackListResult>> {
    /**
    * i18n Bundle name
    */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.resource.messages";
    private static final long serialVersionUID = -2007122101L;
    private String service;
    

    public ShowBlackListCommand(String service) {
        this.service = service;        
    }

    public Result<List<ShowBlackListResult>> execute(ExecutionEnv env)
        throws GrmException {
        List<ShowBlackListResult> result = new LinkedList<ShowBlackListResult>();
        ResourceProvider ret = ComponentService.<ResourceProvider>getComponentByType(env,
                ResourceProvider.class);

        if (ret == null) {
            throw new GrmException("ShowBlackListCommand.rp.notfound", BUNDLE_NAME);
        }

        if (service == null) {
            List<Service> serviceList = ComponentService.getComponentsByType(env, Service.class);
            for (Service serId : serviceList) {
                result.add(getBlackListedResourceIds(ret, serId));
            }            
        } else {
            Service serviceIns = ComponentService.getComponentByNameAndType(env, service, Service.class);
            if (serviceIns == null) {
                throw new GrmException("ShowBlackListCommand.ser.notfound", BUNDLE_NAME, service);
            }
            result.add(getBlackListedResourceIds(ret, serviceIns));
        }        
        

        return new CommandResult<List<ShowBlackListResult>>(result);
    }
        
    private ShowBlackListResult getBlackListedResourceIds(ResourceProvider ret, Service s) {       
        String serviceName = "";
        try {
            serviceName = s.getName();
        } catch (GrmRemoteException gre) {
            serviceName = I18NManager.formatMessage("ShowBlackListCommand.ser.nameproblem", BUNDLE_NAME);
            return new ShowBlackListResult(serviceName, null, gre);
        }
        try {
            List<ResourceIdAndName> resourceids = Collections.<ResourceIdAndName>emptyList();
            resourceids = ret.getBlackList(serviceName);
            return new ShowBlackListResult(serviceName, resourceids);
        } catch (Exception e) {
            return new ShowBlackListResult(serviceName, null, e);
        }       
    }
}
