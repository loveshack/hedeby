/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.resource.Resource;
import java.io.Serializable;

/**
 *  Instances of this class contains the detailed information about
 *  the resource.
 *
 *  @see com.sun.grid.grm.ui.resource.ShowResourceStateCommand
 */
public class ShowResourceResult implements Serializable {

    private static final long serialVersionUID = -2007112401L;
    private final Resource resource;
    private final String serviceName;
    private final Throwable error;

    /**
     * Create a new instance of ShowResourceResult.
     * 
     * @param service name of the service
     * @param resource the affected resource
     */
    public ShowResourceResult(String service, Resource resource) {
        this(service, resource, null);
    }

    /**
     * Create a new instance of ShowResourceResult.
     * 
     * @param service name of the service
     * @param resource the affected resource
     * @param error exception that occured during the action
     */
    public ShowResourceResult(String service, Resource resource, Throwable error) {
        this.serviceName = service;
        this.resource = resource;
        this.error = error;
    }

    /**
     * Get the affected service name
     * @return the name of the service
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Gets the resource (will be displayed in the ui)
     * @return the resource
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * Get the error of the action
     * @return the error of the action
     */
    public Throwable getError() {
        return error;
    }
}
