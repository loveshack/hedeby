/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.service.GetComponentInfosCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterVariable;
import com.sun.grid.grm.util.filter.OrFilter;
import com.sun.grid.grm.util.filter.RegExpFilter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * This command is responsible for showing the current resource states in 
 * Hedeby system.
 */
public class ShowResourceStateCommand extends AbstractLocalCommand<List<ShowResourceResult>> {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.resource.messages";
    private static final long serialVersionUID = -2007101201L;
    private String service = null;
    private String resourceId = null;
    private boolean cached = false;
    private Filter<Resource> advancedFilter;
    private boolean includeServicesWithNoResource = false;

    public ShowResourceStateCommand(String service, String ResourceId, boolean cached) {
        this.service = service;
        this.resourceId = ResourceId;
        this.cached = cached;
    }

    public Result<List<ShowResourceResult>> execute(ExecutionEnv env) throws GrmException {
        List<ShowResourceResult> result = new LinkedList<ShowResourceResult>();
        ResourceProvider ret = ComponentService.<ResourceProvider>getComponentByType(env, ResourceProvider.class);
        if (ret == null) {
            throw new GrmException("ShowResourceStateCommand.rp.notfound", BUNDLE_NAME);
        }

        Filter<Resource> resourceFilter = null;

        if (resourceId == null && advancedFilter == null) {
            resourceFilter = ConstantFilter.<Resource>alwaysMatching();
        } else {
            if (resourceId != null && advancedFilter != null) {
                AndFilter<Resource> andFilter = new AndFilter<Resource>(2);
                andFilter.add(ResourceVariableResolver.createIdOrNameFilter(resourceId));
                andFilter.add(advancedFilter);
                resourceFilter = andFilter;
            } else if (resourceId != null) {
                resourceFilter = ResourceVariableResolver.createIdOrNameFilter(resourceId);
            } else {
                resourceFilter = advancedFilter;
            }
        }

        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>(2);

        OrFilter<ComponentInfo> typeFilter = new OrFilter<ComponentInfo>(2);
        typeFilter.add(GetComponentInfosCommand.newTypeFilter(Service.class));
        typeFilter.add(GetComponentInfosCommand.newTypeFilter(ResourceProvider.class));
        filter.add(typeFilter);

        if (service != null) {
            filter.add(GetComponentInfosCommand.newRegExpNameFilter(service));
        }

        List<ComponentInfo> components = ComponentService.getComponentInfos(env, filter);
        if (components == null || components.isEmpty()) {
            throw new GrmException("ShowResourceStateCommand.ser.notfound", BUNDLE_NAME, service);
        }
        for (ComponentInfo comp : components) {
            if (comp.isAssignableTo(Service.class)) {
                Service svc = ComponentService.<Service>getComponent(env, comp);
                result.addAll(getResources(ret, svc, resourceFilter, cached));
            } else if (comp.isAssignableTo(ResourceProvider.class)) {
                result.addAll(getRPResources(ret, resourceFilter));
            }
        }
        return new CommandResult<List<ShowResourceResult>>(result);
    }

    private List<ShowResourceResult> getResources(ResourceProvider ret, Service s, Filter<Resource> filter, boolean cached) {
        List<ShowResourceResult> srr = new LinkedList<ShowResourceResult>();
        String serviceName = "";
        try {
            serviceName = s.getName();
        } catch (GrmRemoteException gre) {
            serviceName = I18NManager.formatMessage("ShowResourceStateCommand.ser.nameproblem", BUNDLE_NAME);
            srr.add(new ShowResourceResult(serviceName, null, gre));
            return srr;
        }

        try {
            List<Resource> resources = Collections.<Resource>emptyList();
            if (cached) {
                resources = ret.getResources(serviceName, filter);
            } else {
                resources = s.getResources(filter);
            }
            for (Resource r : resources) {
                srr.add(new ShowResourceResult(serviceName, r));
            }
            if (isIncludeServicesWithNoResource()) {
                if (resources.isEmpty() && (resourceId == null || resourceId.length() == 0)) {
                    srr.add(new ShowResourceResult(serviceName, null, null));
                }
            }
        } catch (Exception e) {
            if (isIncludeServicesWithNoResource()) {
                srr.add(new ShowResourceResult(serviceName, null, e));
            }
        }
        return srr;
    }

    private List<ShowResourceResult> getRPResources(ResourceProvider ret, Filter<Resource> filter) {
        List<ShowResourceResult> srr = new LinkedList<ShowResourceResult>();
        String rpName = "";
        try {
            rpName = ret.getName();
        } catch (GrmRemoteException gre) {
            rpName = I18NManager.formatMessage("ShowResourceStateCommand.rp.nameproblem", BUNDLE_NAME);
            srr.add(new ShowResourceResult(rpName, null, gre));
            return srr;
        }

        try {
            List<Resource> resources = ret.getUnassignedResources(filter);
            for (Resource r : resources) {
                srr.add(new ShowResourceResult(rpName, r));
            }
            if (isIncludeServicesWithNoResource()) {
                if (resources.isEmpty() && (resourceId == null || resourceId.length() == 0)) {
                    srr.add(new ShowResourceResult(rpName, null, null));
                }
            }
        } catch (Exception e) {
            if (isIncludeServicesWithNoResource()) {
                srr.add(new ShowResourceResult(rpName, null, e));
            }
        }
        return srr;
    }

    /**
     * Get the advanced filter
     * @return Filter<Resource> advanced filter
     */
    public Filter<Resource> getAdvancedFilter() {
        return advancedFilter;
    }

    /**
     * Set the advanced filter
     * @param advancedFilter
     */
    public void setAdvancedFilter(Filter<Resource> advancedFilter) {
        this.advancedFilter = advancedFilter;
    }

    /**
     * This method returns true if services without resources should be shown.
     * @return boolean value
     */
    public boolean isIncludeServicesWithNoResource() {
        return includeServicesWithNoResource;
    }

    /**
     * Enable or disable the include of services without resources.
     * @param includeServicesWithNoResource
     */
    public void setIncludeServicesWithNoResource(boolean includeServicesWithNoResource) {
        this.includeServicesWithNoResource = includeServicesWithNoResource;
    }
}
