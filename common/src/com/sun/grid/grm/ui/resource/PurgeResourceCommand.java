/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.service.descriptor.ResourcePurgeDescriptor;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.List;

/**
 *
 * This command is used to purge resources from the system.
 */
public class PurgeResourceCommand extends AbstractResourceActionCommand {

    private static final String BUNDLE = "com.sun.grid.grm.ui.resource.messages";

    private final List<String> resIdOrNameList;

    /**
     * Constructor.
     * @param resIdOrNameList a list of resource ids or names to be purged
     * @throws com.sun.grid.grm.GrmException
     */
    public PurgeResourceCommand(List<String> resIdOrNameList) throws GrmException {
        this.resIdOrNameList = resIdOrNameList;
    }


    /**
     * Execute the purge resource command.
     * @param env the execution environment
     * @return the <tt>CommandResult</tt> containing a list of
     *         <tt>ResourceActionResult</tt> objects, one for each purged resource
     * @throws com.sun.grid.grm.GrmException
     */
    public Result<List<ResourceActionResult>> execute(ExecutionEnv env) throws GrmException {
        ResourceProvider rp = ComponentService.getComponentByType(env, ResourceProvider.class);
        if (rp == null) {
            throw new GrmException("ResourceCommand.ex.noRP", BUNDLE);
        }
        return new CommandResult<List<ResourceActionResult>>(rp.removeResources(resIdOrNameList, ResourcePurgeDescriptor.getInstance()));
    }

}
