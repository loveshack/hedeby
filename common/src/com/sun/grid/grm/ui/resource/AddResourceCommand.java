/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.resource;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.util.I18NManager;
import java.util.Map;

/**
 * Commands that adds a new resource to a system.
 * 
 */
public class AddResourceCommand extends AbstractSingleResourceActionCommand {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.resource.messages";
    private static final long serialVersionUID = -2008040301L;
    private final String serviceName;
    private final ResourceType type;
    private final Map<String, Object> resourceProperties;

    public AddResourceCommand(ResourceType type, Map<String, Object> properties) {
        this(null, type, properties);
    }
    
    public AddResourceCommand(String service, ResourceType type, Map<String, Object> properties) {
        if (properties == null || properties.isEmpty()) {
            throw new NullPointerException("Need properties");
        }
        if (type == null)  {
            throw new NullPointerException("Need resource type");
        }
        this.type = type;
        this.serviceName = service;
        this.resourceProperties = properties;
    }

    @Override
    protected ResourceActionResult doAction(ExecutionEnv env) {
        try {
            Resource r = null;

            if (getService() == null) {
                ResourceProvider rp = ComponentService.<ResourceProvider>getComponentByType(env, ResourceProvider.class);
                r = rp.addNewResource(type, resourceProperties);
            } else {
                Service service = ComponentService.<Service>getComponentByNameAndType(env, getService(), Service.class);
                if (service == null) {
                    throw new GrmException("AddResourceCommand.ex.unknownService", BUNDLE_NAME, getService());
                }
                r = service.addNewResource(type, resourceProperties);
            }
            return new ResourceActionResult(r.getId(), r.getName(), I18NManager.formatMessage("AddResourceCommand.resource.added", BUNDLE_NAME));
        } catch(GrmException ex) {
            String resName;
            try {
                resName = type.getResourceName(resourceProperties);
            } catch (InvalidResourcePropertiesException ex1) {
                resName = "unknown";
            }
            return new ResourceActionResult(null, resName, ex.getLocalizedMessage(), ex);
        }
    }

    public String getService() {
        return serviceName;
    }

    public Map<String, Object> getResourceProperties() {
        return resourceProperties;
    }
}
