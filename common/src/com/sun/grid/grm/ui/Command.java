/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;

/**
 * Command interface defines the basic method that can be invoked on a 
 * command. 
 * 
 * Classes implementing the interface encapsulate the request 
 * (operation) that has to be applied on the configuration service.
 * 
 * All bussines logic is contained in the <code>execute(ExecutionEnv)</code> 
 * method, which takes as a parameter an instance of an ExecutionEnv. 
 * The instance of an ExecutionEnv is bound to an invoker of a command - 
 * if the command is invoked using a command queue, it is responsibility of 
 * a command queue to construct an instance of ExecutionEnv and pass it to 
 * the method.
 * 
 * Command itself uses the instance of an ExecutionEnv for getting connection
 * information for Configuration Service etc.
 * 
 * @param T Type of the result value that is returned by a command execution.
 * It depends on a command and can be of any type.
 */
public interface Command<T>  {
    
    /**
     * The method contains whole bussines logic that is specific for 
     * the command. It is not supposed to be called directly - it is used
     * by a command invoker (instance of a CommandInvoker) that is hidden behind 
     * a CommandService. An instance of CommandInvoker knows how to run 
     * <code>Command.execute(env)</code> method and it creates all
     * necessary infrastructure that is needed for command invocation.
     * 
     * The CommandService is a helper class to be used by user to run command 
     * easily.
     *
     * Sample:
     * <code>
     * Command<ExecutorConfig> c = new GetComponentConfigCommand<ExecutorConfig>();
     * Result<ExecutorConfig>  r = CommandService.<ExecutorConfig>execute(env, c);
     * </code>
     *
     * @param env Instance of an ExecutionEnv that is bound to an invoker 
     * of a command
     * @return Instance of a Result with return value of type <code>T</code>.
     * @throws com.sun.grid.grm.GrmException 
     */
    public Result<T> execute(ExecutionEnv env) throws GrmException;

    /**
     * Undo operation is supposed to be called internally by 
     * <code>execute(ExecutionEnv)</code> method, when an exception in command 
     * execution occurs.
     *
     * Command implementation is responsible for storing all necessary 
     * information that will allow to "undo" the execute operation in any time.
     *
     * The method is not supposed to be called directly.
     * @param env Instance of an ExecutionEnv that is bound to an invoker 
     * of a command
     */
    public void undo(ExecutionEnv env);
        
}
