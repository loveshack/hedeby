/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;

/** 
 * CommandDelegate is a interface for delagated comamnds - such commands that need 
 * to be loaded with special classloader which is done with CommandDelegator, the
 * classloader is needed to add external jars to classpath.
 *
 * The logic of the command should be implemented in <code>execute</code> method
 * of this class
 */
 
public interface CommandDelegate<T> {
    
    /**
     * Execution the delegated CLI command
     * @param env         the ExecutionEnv object
     * @param delegator   the delegator which calls the delegate
     * @throws com.sun.grid.grm.GrmException 
     */
    public Result<T> execute(ExecutionEnv env, CommandDelegator<T> delegator) throws GrmException;
    
    /**
     * Undo the changes.
     *
     * @param env       the execution env
     * @param delegator the delegator which calls the delegate
     */
    public void undo(ExecutionEnv env, CommandDelegator<T> delegator);
    
}
