/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.naming.ConfigurationServiceInitialContextFactory;
import java.io.File;
import java.net.MalformedURLException;
import java.util.Hashtable;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Abstract class providing methods for SystemCommand that needs special classloader.
 */
public abstract class AbstractSystemCommandDelegator<T> extends CommandDelegator<T> implements SystemCommand<T> {
    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.messages";
    private static final Logger log = Logger.getLogger(AbstractSystemCommandDelegator.class.getName());
    /**
     * Method that creates an instance of JNDI context needed to contact
     * Configuration Service. ExecutionEnv that contains contact information
     * for communication with Configuration Service is constructed using
     * system preferences.
     *
     * @param env Instance of ExecutionEnv that is bound to command invoker
     * @return Instance of a JNDI Context for Configuration Service
     * @throws com.sun.grid.grm.GrmException when there is ay problem creating
     * an instance of JNDI Context
     */
    public javax.naming.Context createContext(ExecutionEnv env) throws GrmException {
        try {

            Hashtable<String, Object> contextEnv = new Hashtable<String, Object>();
            File csDir = PathUtil.getSpoolDirForComponent(env, BootstrapConstants.CS_ID);

            contextEnv.put(Context.INITIAL_CONTEXT_FACTORY, ConfigurationServiceInitialContextFactory.class.getCanonicalName());
            contextEnv.put(Context.PROVIDER_URL, csDir.toURI().toURL());
            return new InitialContext(contextEnv);
        } catch (MalformedURLException ex) {
            GrmException grm = new GrmException("ui.cs.malformedurl", ex, BUNDLE_NAME, ex.getLocalizedMessage());
            throw grm;
        } catch (NamingException ex) {
            GrmException grm = new GrmException("ui.context.creationfailed", ex, BUNDLE_NAME, ex.getLocalizedMessage());
            throw grm;
        }
    }

}
