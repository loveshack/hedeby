/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventListener;
import com.sun.grid.grm.management.Manageable;

/**
 * Configuration service to use synchronous command invocation.
 */
@Manageable
public interface ConfigurationService extends GrmComponent {
        
    /**
     * Method that is supposed to call when a client wants to wait for a 
     * result of command execution - the communication is synchronous. Result
     * of the command execution is not stored in the internal storage as well as 
     * no "commandExecutionFinished" event is fired.
     * 
     * @param <T> type of return value
     * @param c Command that has to be executed on the queue.
     * @return Result of the command execution.
     * @throws com.sun.grid.grm.GrmException in case of any problems 
     * occured during the execution of the command.
     * @throws com.sun.grid.grm.ui.SchedulingTimeoutException  in case of 
     * unsuccessful scheduling
     * @throws GrmRemoteException when unknown communication error has occured
     * @throws com.sun.grid.grm.management.GrmRemoteException on any remote error
     * @throws java.lang.SecurityException if the caller has not the <code>CommandPermission</code>
     *                  for command <code>c</code>
     *
     * @see CommandPermission
     */
    public <T> Result<T> run(Command<T> c) throws GrmException, SchedulingTimeoutException, GrmRemoteException;
    
    /**
     * Registers a ConfigurationServiceEventListener.
     * @param lis listener to be registered
     */
    public void addConfigurationServiceEventListener(ConfigurationServiceEventListener lis);

    /**
     * Unregisters a ConfigurationServiceEventListener.
     * @param lis listener to be unregistered
     */
    public void removeConfigurationServiceEventListener(ConfigurationServiceEventListener lis);

    /**
     * Get current version of central component (system)
     * @return version of ConfigurationService
     * @throws GrmRemoteException when unknown communication error has occured
     */
    public String getVersion() throws GrmRemoteException;
}
