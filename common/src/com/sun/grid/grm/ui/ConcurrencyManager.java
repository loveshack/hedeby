/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

/**
 * Classes implementing this interface has to create a logic, which will 
 * help them identify whether two command can be scheduled concurrently
 * or not. The rules on which the logic will be based are not striclty defined -
 * it can be JNDI operation type, command classname etc. but it has to 
 * work with ConfigurationService implementation and prevent its deadlocks.
 * 
 */
public interface ConcurrencyManager {

    /**
     * Finds out whether two specific commands can be run concurrently
     *
     * @param a Command that has to be considered
     * @param b Command that has to be considered
     * @return true if commands allow to be run concurrently, false otherwise
     */
    boolean concurrentRunAllowed(Command a, Command b);
}
