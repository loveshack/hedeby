/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.util.GrmClassLoaderFactory;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *  Abstract base class for all commands which depends on classes
 *  that are not available in the System classpath of a Hedeby system.
 *
 *  This class loads a delegate class on a separate class loader.
 */
public abstract class CommandDelegator<T>  {
    

    /**
     * L10N Bundle for this class
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.messages";
    
    private static final Logger log = Logger.getLogger(CommandDelegator.class.getName(), BUNDLE);
    
    private CommandDelegate<T> delegate;
    
    /**
     * Get the classpath for the component delegate.
     * @param env Used ExecutionEnv for operation
     * @return an array with the URLs of all classpath elements
     * @throws com.sun.grid.grm.GrmException if the classpath can not be constructed
     */
    protected abstract URL[] getClasspath(ExecutionEnv env) throws GrmException;
    
    
    /**
     * Get the classname of the Command delegate
     * @param Env Used ExecutionEnv for operation
     * @return the classname of the component delegate
     * @throws com.sun.grid.grm.GrmException if the classname can not be constructed
     */
    protected abstract String getDelegateClassname(ExecutionEnv Env) throws GrmException;
    
    /**
     * This method creates the command delegate.
     * 
     * <p>First out of the classpath a new <code>ClassLoader</code> is created. This
     * <code>ClassLoader</code> is used find the class of the component delegate.</p>
     * @param env Used ExecutionEnv for operation
     * @return the command delegate
     * @see #getDelegateClassname
     * @see #getClasspath
     * @throws com.sun.grid.grm.GrmException On Errros
     */
    @SuppressWarnings("unchecked")
    protected CommandDelegate<T> createDelegate(ExecutionEnv env) throws GrmException {
        
        URL [] classpath = getClasspath(env);
        String classname = getDelegateClassname(env);
        
        ClassLoader cl = GrmClassLoaderFactory.getInstance(classpath, getClass().getClassLoader());
        
        if(log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "commandDelegator.classpath", GrmClassLoaderFactory.toString(cl));
        }
        Class cls;
        
        try {
            cls = Class.forName(classname, true, cl);
        } catch (ClassNotFoundException ex) {
            throw new GrmException("commandDelegator.error.classNotFound", ex, BUNDLE, classname, cl.toString());
        } catch (NoClassDefFoundError ex) {
            throw new GrmException("commandDelegator.error.dependClassNotFound", ex, BUNDLE, classname, ex.getLocalizedMessage(), cl.toString());
        }
        try {
            return (CommandDelegate<T>)cls.newInstance();
        } catch (IllegalAccessException ex) {
            throw new GrmException("commandDelegator.error.noConstructor", ex, BUNDLE, classname);
        } catch (InstantiationException ex) {
            throw new GrmException("commandDelegator.error", ex, BUNDLE, classname, ex.getLocalizedMessage());
        }
    }
    
    /**
     * Execute the command.
     * 
     * Calls inside the execute method of the <code>CommandDelegate</code>.
     * @param env the ExecutionEnv object
     * @return Result<T>
     * @throws com.sun.grid.grm.GrmException on any error
     */
    public final Result<T> execute(ExecutionEnv env) throws GrmException {
        
        delegate = createDelegate(env);
        
        return delegate.execute(env, this);
    }
    
    /**
     *  Undo all changes.
     *
     * @param env the ExecutionEnv object
     */
    public final void undo(ExecutionEnv env) {
        
        if(delegate != null) {
            delegate.undo(env, this);
            delegate = null;
        }
    }
    
}
