/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui;

import com.sun.grid.grm.util.I18NManager;
import java.security.Permission;

/**
 * This <code>Permission</code> is used to check if caller is
 * allowed to execute a command in the <code>ConfigurationService</code>.
 *
 * <p>The name of the <code>CommandPermission</code> is the full qualified
 *    classname of the permitted <code>Command</code>.</p>
 * <p>At the end of the name the wildcard <code>*</code> is allowed. Such a name
 *    defines that all commands with a classname starting with the string 
 *    before <code>*</code> are permitted.</p>
 *
 * <p><b>Examples:</b></p>
 *
 * <ul>
 *     <li><p>Permit the <code>AddComponentCommand</code></p>
 * <pre>
 *    permission com.sun.grid.grm.ui.CommandPermission "com.sun.grid.grm.ui.component.AddComponentCommand";
 * </pre>
 *     </li>
 *     <li><p>Permit all commands from a package</p>
 * <pre>
 *    permission com.sun.grid.grm.ui.CommandPermission "com.sun.grid.grm.ui.component.*";
 * </pre>
 *     </li>
 *     <li><p>Permit all commands</p>
 * <pre>
 *    permission com.sun.grid.grm.ui.CommandPermission "*";
 * </pre>
 *     </li>
 *  </ul>
 *
 * @see ConfigurationService#run
 */
public class CommandPermission extends Permission {
    
    private static final String BUNDLE = "com.sun.grid.grm.ui.messages";
    private static final long serialVersionUID = -2007091100L;
    private final String prefix;
    
    /**
     * Creates a new instance of CommandPermission.
     *
     * @param name the name of the permission
     */
    public CommandPermission(String name) {
        super(name);
        if(name == null) {
            throw new NullPointerException(I18NManager.formatMessage("commandPermission.nameNull", BUNDLE));
        }
        int starIndex = name.indexOf('*');
        if(starIndex < 0) {
            prefix = null;
        } else if (starIndex == name.length() -1) {
            if(starIndex == 0) {
                prefix = "";
            } else {
                prefix = name.substring(0, starIndex);
            }
        } else {
            throw new IllegalArgumentException(I18NManager.formatMessage("commandPermission.invalidName", BUNDLE));
        }
    }
    
    /**
     * Creates a new instance of CommandPermission.
     * 
     * @param cmdClass the command class
     */
    public CommandPermission(Class<? extends Command> cmdClass) {
        this(cmdClass.getName());
    }
    
    
    
    /**
     * Checks if this <code>CommandPermission</code> object "implies" the specified permission.
     * <P>
     * More specifically, this method returns true if:<p>
     * <ul>
     * <li> <code>permission</code> is an instanceof CommandPermission,</li>
     * <li> the <code>name</code> of <code>permission</code> matches to the
     *      name of this object's name</li>
     * @param permission the permission to check against.
     *
     * @return <code>true</code> if the specified permission is implied by this object,
     * <code>false</code> if not.  
     */
    public boolean implies(Permission permission) {
        if(permission instanceof CommandPermission) {
            CommandPermission that = (CommandPermission)permission;
            if(prefix == null) {
                if(that.prefix == null) {
                    return that.getName().equals(getName());
                } else {
                    return false;
                }
            } else {
                if(that.prefix == null) {
                    if(prefix.length() == 0) {
                        return true;
                    } else {
                        return that.getName().startsWith(prefix);
                    }
                } else {
                    if(prefix.length() == 0) {
                        return true;
                    } else {
                        return that.prefix.startsWith(prefix);
                    }
                }
            }
        } else {
            return false;
        }
    }
    
    /**
     * Checks two <code>CommandPermission</code> objects for equality.
     * 
     * <p>Checks  that <code>obj</code> is a <code>CommandPermission</code>, and
     * has the name as this object.</p>
     *
     * @param obj the object we are testing for equality with this object.
     * @return <code>true</code> if obj is a <code>CommandPermission</code>, and
     *         has the same name this object.
     */
    public boolean equals(Object obj) {
        if(obj instanceof CommandPermission) {
            CommandPermission that = (CommandPermission)obj;
            return getName().equals(that.getName());   
        } else {
            return false;
        }
    }
    
    /**
     * Returns the hash code value for this object.
     * 
     * @return a hash code value for this object.
     */
    public int hashCode() {
        return getName().hashCode();
    }
    
    /**
     * Returns the "canonical string representation" of the actions.
     *
     * <p>The <code>CommandPermission</code> has not actions. This method returns
     * always an empty string.</p>
     *
     * @return always an empty string
     */
    public String getActions() {
        return "";
    }
    
}
