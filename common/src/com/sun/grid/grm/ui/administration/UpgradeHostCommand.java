/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.administration;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.bootstrap.UpgradeResult;
import com.sun.grid.grm.bootstrap.UpgradeUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.io.File;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
/**
 * Ensures that one upgrade at a time is executed and is used for locking host so
 * Any other jvm will not be started during upgrade. Calls upgrade if necessary
 *
 */
public class UpgradeHostCommand extends AbstractLocalCommand<UpgradeResult> {

    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.administration.messages";
    private static final Logger log = Logger.getLogger(UpgradeHostCommand.class.getName(), BUNDLE_NAME);
    private File undo = null;
    private static final String PERM_RW = "rw";
    private static final String PERM_R = "r";
    private static final String FILE_PERM = "644";

    public Result<UpgradeResult> execute(ExecutionEnv env) throws GrmException {
        String hostVersion = null;
        String systemVersion = null;
        UpgradeResult result = new UpgradeResult(true);
        boolean master = env.getCSHost().equals(Hostname.getLocalHost());
        File lockFile = PathUtil.getLocalSpoolLockFilePath(env);
        FileChannel channel = null;
        FileLock lock = null;
        //Check if upgrade module is present
        if (!UpgradeUtil.isUpgradeAvailable(env)) {
            try {
                //sdm-upgrade.jar doesnt exist, start jvms
                //each user stating jvm should be able to get read lock
                channel = new RandomAccessFile(lockFile, PERM_R).getChannel();
                //Lock will be released then process triggering this commands ends (sdmadm suj)
                //This ensures that we dont get situatioin that jvm is starting up and someone meanwhile
                //updates binaries and starts upgrade procedure 
                lock = channel.tryLock(0L, Long.MAX_VALUE, true);
                if (lock == null) {
                    //this should never happen for system without sdm-upgrade-impl.jar
                    throw new GrmException("uhc.unexpectederror.cannotlockfile.nonupgradebinaries",BUNDLE_NAME, lockFile.getAbsolutePath());
                }
                return new CommandResult<UpgradeResult>(result);
            } catch (FileNotFoundException ex) {
                //This will happen when start jvm on system 1.0u3 (and older) and 
                //binaries do not contain upgrade
                throw new GrmException("uhc.error.oldsystem.nonupgradebinaries", BUNDLE_NAME, ex, lockFile.getAbsolutePath());
            } catch (IOException ex) {
                throw new GrmException("uhc.error.failedlock.nonupgradebinaries", BUNDLE_NAME, ex, lockFile.getAbsolutePath());
            }
        }

        //we have sdm-upgrade.jar
        
        try {
            /**
             * try to get read lock, if success check if upgrade necessary,
             * if null throw exception upgrade is executed by someone else,
             * if exp FileNotFound, first start after patching the binaries
             */
            channel = new RandomAccessFile(lockFile, PERM_R).getChannel();
            lock = channel.tryLock(0L, Long.MAX_VALUE, true);
            if (lock == null) {
                //cannot proceed someone is upgrading system, exit
                throw new GrmException("uhc.error.upgradeinprogress", BUNDLE_NAME, lockFile.getAbsolutePath());
            }
            /**
             * get host version
             */
            hostVersion = SystemUtil.getHostVersion(env);

        //we have read lock and upgrade is necessary - release read lock and get wrote lock to perform upgrade

        } catch (FileNotFoundException ex) {
            //lock file does not exist - only for old versions of system
            hostVersion = BootstrapConstants.VERSION;
        } catch (IOException ex) {
            throw new GrmException("uhc.error.failedlock.nonupgradebinaries", BUNDLE_NAME, ex, lockFile.getAbsolutePath());
        }
        /**
         * GetSystem version if impossible, exit with exception
         * if we are on master host and CS is already runnig return from command
         */
        if (master) {
            systemVersion = null;
            if (SystemUtil.isCSStarted(env)) {
                //if this is master with already running CS skip upgrade
                return new CommandResult<UpgradeResult>(result);
            }
        } else {
            //if master is oldVersion we will get exception, catch it and set correct message
            try {
                systemVersion = SystemUtil.getSystemVersion(env);
            } catch (GrmException ex) {
                throw new GrmException("uhc.error.masternotreachable.cannotgetsystemversion", BUNDLE_NAME, SystemUtil.getBinaryVersion(env));
            }
        }
        /**
         * Check if upgrade is necessary
         * if hostVersion was set to old version this will return true
         */
        if (!UpgradeUtil.isUpgradeNecessary(env, hostVersion, systemVersion)) {
            /**
             * Skip upgrade
             */
            return new CommandResult<UpgradeResult>(result);
        }
        /**
         * We need to upgrade so we need write lock
         */
        if (lock != null) {
            try {
                lock.release();
            } catch (IOException ex) {
                throw new GrmException("uhc.error.failedreleasereadlock", BUNDLE_NAME, ex, lockFile.getAbsolutePath());
            }
        }

        try {
            /**
             * If lock file does not exist it will be created
             */
            channel = new RandomAccessFile(lockFile, PERM_RW).getChannel();
            lock = channel.tryLock();
            if (lock == null) {
                //cannot proceed someone is upgrading system, or starting jvm
                throw new GrmException("uhc.error.upgradeorstartinprogress", BUNDLE_NAME, lockFile.getAbsolutePath());
            }
            /**
             * Lock acquired, if file was created add it to undo
             * Lock will be release when process finishes
             * The owner and permissions on this file need to be set properly
             */
            if (hostVersion.equals(BootstrapConstants.VERSION)) {
                undo = lockFile;
                Platform platform = Platform.getPlatform();
                try {
                    String admin = platform.getFileOwner(env.getLocalSpoolDir());
                    platform.chown(lockFile, admin);
                    platform.chmod(lockFile, FILE_PERM);
                } catch (Exception ex) {
                    throw new GrmException("uhc.error.failedbootsraplockfile", BUNDLE_NAME, ex, lockFile.getAbsolutePath());
                }
            }
            result = UpgradeUtil.executeHostUpgrade(env, hostVersion, systemVersion);
            if (result.isSuccess()) {
                undo = null;
            }
            return new CommandResult<UpgradeResult>(result);
        } catch (FileNotFoundException ex) {
            //we dont have permissions to create new file lock throw exception upgrade not possible permission denied
            //User cannot create lock in localspool
            throw new GrmException("uhc.error.perm.cannotcreatelockfile", BUNDLE_NAME, ex, lockFile.getAbsolutePath());
        } catch (IOException ex) {
            //Unexpected error
            throw new GrmException("uhc.error.cannotcreatelockfile", BUNDLE_NAME, ex, lockFile.getAbsolutePath());
        } finally {
            this.undoFailure(env);
        }

    }
    /**
     * Undo for this command is not supported - UnsupportedOperationException will be thrown
     * @param env ExecutionEnv
     */
    @Override
    public void undo(ExecutionEnv env) {
        throw new UnsupportedOperationException(I18NManager.formatMessage("uhc.error.undonotsupported",BUNDLE_NAME));
    }
    private void undoFailure(ExecutionEnv env) {
        if (undo != null) {
            if (!undo.delete()) {
                //log
                LogRecord lr = new LogRecord(Level.WARNING, "uhc.error.failedundo");
                lr.setParameters(new Object[]{undo.getAbsolutePath()});
                log.log(lr);
            }
            undo = null;
        }
    }
}
