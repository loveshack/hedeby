/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.administration;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.JVMResultObject;
import com.sun.grid.grm.ui.component.service.GetActiveJVMListCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 *
 * This command sets the different logging level for loggers on jvm
 */
public class SetLoggingCommand extends AbstractLocalCommand<List<JVMResultObject>> {
    
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.administration.messages";
    
    private Hostname hostname;
    private String jvm;
    private Map<String, Level> loggers;
    private Level changeAll;
    
    /**
     * This command sets the loggers logging level on jvm
     * @param hostname name of the host
     * @param jvm name of the jvm
     * @param loggers loggers data
     * @param changeAll logger level
     */
    public SetLoggingCommand(Hostname hostname, String jvm, Map<String, Level> logger, Level changeAll) throws GrmException {  
        setHostname(hostname);
        setJvm(jvm);
        setLoggers(logger);
        setChangeAll(changeAll);
    }
    
    /**
     * This method sets the level for the loggers on jvm
     * @param env enviroment variable
     * @return list of changed loggers
     * @throws com.sun.grid.grm.GrmException
     */
    public Result<List<JVMResultObject>> execute(ExecutionEnv env) throws GrmException {
        List<JVMResultObject> ret = new LinkedList<JVMResultObject>();        
        GetActiveJVMListCommand cmd = new GetActiveJVMListCommand();
            cmd.setJvmName(getJvm());
            if (hostname != null) {
                cmd.setHostname(hostname.getHostname());
            }
            for(ActiveJvm jvmName: env.getCommandService().<List<ActiveJvm>>execute(cmd).getReturnValue()) {
                ComponentInfo ci = ComponentInfo.newInstance(env, Hostname.getInstance(jvmName.getHost()), jvmName.getName(), jvmName.getName(), JVM.class);
                JVM proxy = ComponentService.<JVM>getComponent(env, ci);
                Map<String, Level> logers = proxy.setLoggers(getLoggers(), getChangeAll());
                for (Map.Entry<String, Level> entry: logers.entrySet()) {     
                    ret.add(new JVMResultObject(jvmName.getName(), jvmName.getHost(),
                    entry.getKey(), entry.getValue().getName()));                   
                }
            }       
        return new CommandResult<List<JVMResultObject>>(ret);
    }
    
    /**
     * Gets the hostname
     * @return hostname
     */
    public Hostname getHostname() {
        return hostname;
    }
    
    /**
     * Sets the hostname
     * @param hostname name of the jvm
     */
    public void setHostname(Hostname hostname) {
        this.hostname = hostname;
    }
    
    /**
     * Gets the jvm name
     * @return jvm name
     */
    public String getJvm() {
        return jvm;
    }
    
    /**
     * Sets the jvm
     * @param jvm name
     */
    public void setJvm(String jvm) {
        this.jvm = jvm;
    }
    
    /**
     * Gets loggers data
     * @return the loggers data
     */
    public Map<String, Level> getLoggers() {
        return loggers;
    }
    
    /**
     * Sets the loggers data
     * @param loggers data
     */
    public void setLoggers(Map<String, Level> loggers) {
        this.loggers = loggers;
    }
    
    /**
     * Get the logger level
     * @return the logger level
     */
    public Level getChangeAll() {
        return this.changeAll;
    }
    
    /**
     * Sets the logger level
     * @param changeAll logger level
     */
    public void setChangeAll(Level changeAll) {
        this.changeAll = changeAll;
    }
}
