/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.impl;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectAddedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectChangedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationObjectRemovedEvent;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventListener;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.naming.ConfigurationServiceContext;
import com.sun.grid.grm.config.naming.event.ConfigurationServiceEventSupport;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.CommandPermission;
import com.sun.grid.grm.ui.ConfigurationService;
import com.sun.grid.grm.ui.ConcurrencyManager;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.SchedulingTimeoutException;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.security.AccessControlContext;
import java.security.AccessControlException;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ResourceBundle;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.security.auth.Subject;

/**
 *  The <code>Configuration Service</code> provides the functionality for invoking
 *  commands.
 *
 */
public class ConfigurationServiceImpl implements ConfigurationService, GrmComponent {

    private final ExecutionEnv executionEnv;
    /**
     * Thread pool managing execution of the commands
     */
    private final ExecutorService executorService;
    /**
     * Single thread scheduling commands for execution
     */
    private final ExecutorService schedulerService;
    /**
     * Timeout within the command has to be scheduled (in seconds)
     */
    private static final int SCHEDULING_TIMEOUT = 10;
    /**
     * Queue for storing incoming commands until they are scheduled for execution
     */
    private final BlockingQueue<SynchronousCommandAdapter<?>> incomingQueue;
    /**
     * Single thread scheduling commands for execution
     */
    private final ConcurrentHashMap<Future<Result<?>>, SynchronousCommandAdapter<?>> futureCommands;
    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.impl.messages";
    private static final Logger log = Logger.getLogger(ConfigurationServiceImpl.class.getName(), BUNDLE_NAME);
    private static final ThreadGroup ConfigurationServiceThreadGroup = new ThreadGroup("command queue");
    private final String name;
    private final Callable<Void> scheduler;
    private final ConfigurationServiceEventSupport listeners = new ConfigurationServiceEventSupport();
    private final Hostname hostname;
    private final String version;
            
    /**
     * Creates an instance of ConfigurationService
     * @param env ExecutionEnv of the JVM that host the ComamndQueue
     * @param name
     * @param configName
     * @throws com.sun.grid.grm.GrmException
     */
    public ConfigurationServiceImpl(ExecutionEnv env, String name, String configName) throws GrmException {
        this.name = name;
        this.hostname = Hostname.getLocalHost();
        this.scheduler = new Scheduler();
        this.version = SystemUtil.getHostVersion(env);
        executorService = Executors.newCachedThreadPool(new ThreadFactory() {

            public Thread newThread(Runnable r) {
                return new Thread(ConfigurationServiceThreadGroup, r);
            }
        });
        incomingQueue = new LinkedBlockingQueue<SynchronousCommandAdapter<?>>();
        schedulerService = Executors.newSingleThreadExecutor();
        futureCommands = new ConcurrentHashMap<Future<Result<?>>, SynchronousCommandAdapter<?>>();
        executionEnv = env;
        ConfigurationServiceContext.addConfigurationServiceEventListener(new CSContextListener());
        schedulerService.submit(scheduler);
    }

    public <T> Result<T> run(Command<T> c) throws GrmException, SchedulingTimeoutException, GrmRemoteException {
        log.logrb(Level.FINE, ConfigurationServiceImpl.class.getName(), "run", BUNDLE_NAME, "ui.cq.cmdsubmit", c.getClass());
        // Check that the caller has the permission to execute the command
        // We need to catch Runtime expection and throw GrmException to notify
        // Cli about failure in this commnad
        try {                     
            AccessController.checkPermission(new CommandPermission(c.getClass()));
        } catch (AccessControlException ex) {
            throw new GrmException("ui.cq.permdenied", ex.getCause(), BUNDLE_NAME);
        }
        
        // Get the security context of the caller
        // and store it in the SynchronousCommandAdapter
        // This object is responsible for executing the command
        // under the corret security context (Subject)
        AccessControlContext ctx = AccessController.getContext();
        Subject subject = Subject.getSubject(ctx);
        SynchronousCommandAdapter<T> sca = new SynchronousCommandAdapter<T>(c, subject, ctx);
        incomingQueue.add(sca);
        Result<T> res = null;
        try {
            log.logrb(Level.FINE, ConfigurationServiceImpl.class.getName(), "run", BUNDLE_NAME, "ui.cq.cmdgetstart", c.getClass());
            Future<Result<T>> f = sca.getFuture();
            res = f.get();
            log.logrb(Level.FINE, ConfigurationServiceImpl.class.getName(), "run", BUNDLE_NAME, "ui.cq.cmdgetstop", c.getClass());
        } catch (InterruptedException ex) {
            log.logrb(Level.FINE, ConfigurationServiceImpl.class.getName(), "run", BUNDLE_NAME, "ui.cq.threadint", ex);
            throw new GrmException("ui.cq.threadint", ex.getCause(), BUNDLE_NAME);
        } catch (ExecutionException ex) {
            log.logrb(Level.FINE, ConfigurationServiceImpl.class.getName(), "run", BUNDLE_NAME, "ui.cq.executionexcp", ex.getCause());
            if(ex.getCause() instanceof GrmException) {
                throw (GrmException)ex.getCause();
            } else if (ex.getCause() instanceof SecurityException) {
                throw new GrmException("ui.cq.permdenied", ex.getCause(), BUNDLE_NAME);
            } else {
                LogRecord lr = new LogRecord(Level.WARNING, "ui.cq.executionexcp");
                lr.setParameters(new Object[]{c.getClass().getName()});
                lr.setThrown(ex.getCause());
                lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE_NAME));
                log.log(lr);
                throw new GrmException("ui.cq.executionexcp", ex.getCause(), BUNDLE_NAME, new Object[]{c.getClass().getName()});
            }
        }
        return res;
    }

    public void start() throws GrmException {
        throw new UnsupportedOperationException(I18NManager.formatMessage("ui.cq.uso.cs", BUNDLE_NAME));
    }

    public void stop(boolean isForced) throws GrmException {
        throw new UnsupportedOperationException(I18NManager.formatMessage("ui.cq.uso.cs", BUNDLE_NAME));
    }

    public void reload(boolean isForced) throws GrmException {
        throw new UnsupportedOperationException(I18NManager.formatMessage("ui.cq.uso.cs", BUNDLE_NAME));
    }

  
    public ComponentState getState() {
        return ComponentState.STARTED;
    }

  
    public void addComponentEventListener(ComponentEventListener stateChangedLister) {
//        throw new UnsupportedOperationException(I18NManager.formatMessage("ui.ci.uso.cs", BUNDLE_NAME));
    }

  
    public void removeComponentEventListener(ComponentEventListener stateChangedListener) {
        throw new UnsupportedOperationException(I18NManager.formatMessage("ui.ci.uso.cs", BUNDLE_NAME));
    }

  
    public String getName() {
        return this.name;
    }
    
    public Hostname getHostname() {
        return this.hostname;
    }

    public String getVersion() {
        return this.version;
    }
/**
     * Command adapter to allow to run commands using java.util.concurrent.ExecutorService.
     * @param T Type of the return value of the result
     */
    private class SynchronousCommandAdapter<T> implements Callable<Result<T>>, PrivilegedExceptionAction<Void> {

        private final Command<T> _command;
        private Future<Result<T>> _future;
        private final Subject subject;
        private final AccessControlContext ctx;
        private Result<T> result;
        private SchedulingTimeoutException _error;
        private final Lock lock = new ReentrantLock();
        private final Condition finished = lock.newCondition();
        
        public SynchronousCommandAdapter(Command<T> command, Subject subject, AccessControlContext ctx) {
            this._command = command;
            this._future = null;
            this.subject = subject;
            this.ctx = ctx;
        }

        public Result<T> call() throws Exception {
            try {
                // Execute the command under the subject of the caller
                if(log.isLoggable(Level.FINE)) {
                    log.logrb(Level.FINE, SynchronousCommandAdapter.class.getName(), "call", BUNDLE_NAME, "ui.sca.execCmd",
                              new Object [] { _command, subject } );
                }
                Subject.doAsPrivileged(subject, this, ctx);
                return result;
            } catch(PrivilegedActionException ex) {
                throw ex.getException();
            }            
        }
        
        public Void run() throws Exception {
                result = this._command.execute(executionEnv);
            return null;
        }
        

        public Command<T> getCommand() {
            return this._command;
        }

        public void setFuture(Future<Result<T>> f) {
            if(f == null) {
                // To avoid a dead lock we have to ensure
                // that f is not null
                throw new NullPointerException("f must not be null");
            }
            lock.lock();
            try {
                _future = f;
                finished.signalAll();
            } finally {
                lock.unlock();
            }
        }
        
        
        public void timeout() {
            lock.lock();
            try {
               _error = new SchedulingTimeoutException(_command);
               finished.signalAll();
            } finally {
                lock.unlock();
            }
        }
        
        public Future<Result<T>> getFuture() throws SchedulingTimeoutException, InterruptedException {
            lock.lock();
            try {
                if (_error == null && _future == null) {
                    if (!finished.await(SCHEDULING_TIMEOUT, TimeUnit.SECONDS)) {
                       _error = new SchedulingTimeoutException(_command);
                       finished.signalAll();
                    }
                }
                if(_error != null) {
                    throw _error;
                } else {
                    return _future;
                }
            } finally {
                lock.unlock();
            }
        }

    }

    /**
     * Scheduler takes the role of scheduling the incoming request properly.
     * It checks (using the ConcurrencyManager) whether it is possible to
     * schedule the command, considering the facts:
     * 1. All currently running command allow concurrent execution of the
     *    command that is about to be scheduled for execution
     * 2. All currenlty scheduled (but not yet running) commands allow
     *    concurrent execution of the command that is about to be scheduled.
     */
    private class Scheduler implements Callable<Void> {

        private final ConcurrencyManager _manager;

        /**
         * It'd be nice to make ConcurencyManager instantiation configurable
         */
        private Scheduler() {
            this._manager = new JNDIBasedConcurrencyManager();
        }

        /**
         * Get the command from the running command list block
         * <code>c</code>.
         *
         * @param c Command that is about to be scheduled
         * @return the adapter of the block command or <code>null</code>
         *         if no block command has been found
         */
        private SynchronousCommandAdapter getBlockingCommand(Command<?> c) {
            for (SynchronousCommandAdapter sca : futureCommands.values()) {
                if (!_manager.concurrentRunAllowed(c, sca.getCommand())) {
                    return sca;
                }
            }
            return null;
        }

        /**
         * Removes the executed (either successfully or unsuccessfully) commands.
         */
        private void cleanUp() {
            for (Future f : futureCommands.keySet()) {
                if (f.isDone()) {
                    futureCommands.remove(f);
                }
            }
        }

        /**
         * Tries to schedule commands from incoming queue, if possible.
         *
         * @throws java.lang.Exception if an unpredicted problem occurs
         */
        @SuppressWarnings(value = "unchecked")
        public Void call() throws Exception {
            try {
                SynchronousCommandAdapter sca = null;
                long endTime = 0;
                
                while (!Thread.currentThread().isInterrupted()) {
                    
                    if(sca == null) {
                        sca = incomingQueue.take();
                        endTime = System.currentTimeMillis() + SCHEDULING_TIMEOUT * 1000;
                    }
                    cleanUp();
                    SynchronousCommandAdapter blockingSca = getBlockingCommand(sca.getCommand());
                    long timeout = endTime - System.currentTimeMillis();
                    if(timeout < 0) {
                        // Waiting for the first blocking command was not eough
                        // There are additional blocking commands running
                        sca.timeout();
                        sca = null;
                    } else if (blockingSca == null) {
                        // We have no futher blocking command
                        // Start the command
                        Future f = executorService.submit(sca);
                        // mark command as active
                        futureCommands.put(f, sca);
                        sca.setFuture(f);
                        sca = null;
                    } else {
                        // We have a blocking command
                        if(log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE,"ui.sch.wait", 
                                    new Object [] { sca.getCommand(), blockingSca.getCommand(), timeout });
                        }
                        // The getFuture call will not block because it is already running
                        Future f = blockingSca.getFuture();
                        // Wait for the end of the blocking command with timeout
                        f.get(timeout, TimeUnit.MILLISECONDS);
                        if(!f.isDone()) {
                            // Execution of the blocking command takes longer then
                            // the schedule timeout
                            sca.timeout();
                            sca = null;
                        }
                        // blocking command finished in time
                        // with the next loop cylce we will check if another
                        // block command is running
                    }
                }
            } catch (InterruptedException ex) {
                log.log(Level.FINE, "ui.sch.interrupt");
            } catch (TimeoutException ex) {
                log.log(Level.INFO, "ui.sch.timeout");
            } catch(Exception ex) {
                log.log(Level.WARNING, "unexpected error in scheduler", ex);
            } finally {
                log.log(Level.INFO,"ui.sch.died");
            }
            return null;
        }
    }

    /**
     * Registers a ConfigurationServiceEventListener.
     * @param lis listener to be registered
     */
    public void addConfigurationServiceEventListener(ConfigurationServiceEventListener lis) {
        listeners.addConfigurationServiceEventListener(lis);
    }

    /**
     * Unregisters a ConfigurationServiceEventListener.
     * @param lis listener to be unregistered
     */
    public void removeConfigurationServiceEventListener(ConfigurationServiceEventListener lis) {
        listeners.removeConfigurationServiceEventListener(lis);
    }
    
    private class CSContextListener implements ConfigurationServiceEventListener {

        public void configurationObjectAdded(ConfigurationObjectAddedEvent event) {
            listeners.fireConfigurationObjectAdded(event.getName(),
                    event.getHost(),
                    event.getContextPath(),
                    event.getMessage(),
                    event.getType());
        }

        public void configurationObjectRemoved(ConfigurationObjectRemovedEvent event) {
            listeners.fireConfigurationObjectRemoved(event.getName(),
                    event.getHost(),
                    event.getContextPath(),
                    event.getMessage(),
                    event.getType());
        }

        public void configurationObjectChanged(ConfigurationObjectChangedEvent event) {
            listeners.fireConfigurationObjectChanged(event.getName(),
                    event.getHost(),
                    event.getContextPath(),
                    event.getMessage(),
                    event.getType());
        }
        
    }
}
