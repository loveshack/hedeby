/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.impl;

import com.sun.grid.grm.ui.Result;
import java.io.Serializable;


/**
 * Result with serialization support. Implementation does not deal with
 * serialization support for type <code>T</code>, it is up to end user to make
 * sure that <code>T</code> can be serialized.
 *
 * The class is supposed to be used as a result of execution of a SystemCommand.
 *
 * @param T Type of the return value that is result of a command execution.
 */
public class CommandResult<T> implements Result<T>, Serializable {

    private static final long serialVersionUID = 2007062001;
    
    public static final CommandResult<Void> VOID_RESULT = new CommandResult<Void>();
    
    T _value;

    public CommandResult() {
        _value = null;
    }

    public CommandResult(T value) {
        _value = value;
    }

    public T getReturnValue() {
        return _value;
    }

    public void setReturnValue(T value) {
        _value = value;
    }
}
