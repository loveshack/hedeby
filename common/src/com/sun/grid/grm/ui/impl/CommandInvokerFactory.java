/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.impl;

import com.sun.grid.grm.ui.CommandInvoker;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Factory class for creating dynamic proxies that implement CommandInvoker 
 * interface.
 */
public abstract class CommandInvokerFactory {
    
    /**
     * The CommandInvokerFactory is not supposed to be instantiated. The only use of 
     * the class is to call <code>getCommandInvokerProxy(InvocationHandler handler)</code>
     * to get an instance of CommandInvoker.
     * 
     */
    private CommandInvokerFactory() {
    }
    
    /**
     * Gets a proxy that implemetns Commandinvoker interface
     * 
     * @param handler Instance of InvocationHandler that knows how to deal with
     * CommandInvoker interface
     * @return dynamic proxy for ComamndInvoker
     */
    public static final CommandInvoker getCommandInvokerProxy(InvocationHandler handler){        
            return (CommandInvoker) Proxy.newProxyInstance(CommandInvoker.class.getClassLoader(),
                new Class[] {CommandInvoker.class}, 
                handler);            
    }
    
}
