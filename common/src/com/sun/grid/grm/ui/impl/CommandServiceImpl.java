/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.impl;

import com.sun.grid.grm.bootstrap.*;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.ui.*;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.management.remote.JMXServiceURL;

/**
 *  The <code>CommandServiceImpl</code> provides the convenient way to invoke
 *  a commands.
 */
public class CommandServiceImpl implements CommandService{

    /**
     * i18n Bundle name
     */
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.messages";
    private ExecutionEnv env;

    private static Logger log;
    private static Logger log() {
        if(log == null) {
            log = Logger.getLogger(CommandServiceImpl.class.getName(), BUNDLE_NAME);
        }
        return log;
    }
    
    
    private static final Map<JMXServiceURL, ConfigurationService> proxyMap = new HashMap<JMXServiceURL, ConfigurationService>(3);
    
    
    private static ConfigurationService getProxy(ExecutionEnv env) throws GrmException {
        ConfigurationService ret = null;
        synchronized(proxyMap) {
            ret = proxyMap.get(env.getCSURL());
            if(ret == null) {
                ret = ComponentService.getCS(env);
                proxyMap.put(env.getCSURL(), ret);
            }
        }
        return ret;
    }
    
    /**
     * A convenient way to invoke a command. For each invocation, new proxy
     * for ConfigurationService is created if needed, with creddentials as specified
     * in ExecutionEnv.
     *
     * @param command Command that has to be invoked
     * @return Result with return value as specified by Command type
     * @throws com.sun.grid.grm.GrmException everytime a problem during
     * command invocation occurs.
     */
    @SuppressWarnings(value = "unchecked")
    public <T> Result<T> execute(Command<T> command) throws GrmException {
        Result<T> ret = null;
        boolean fine = log().isLoggable(Level.FINE);
        try {
            if (command instanceof LocalCommand) {
                if(fine) {
                    log().log(Level.FINE, "ui.cs.el", command.getClass().getName());
                }
                ret = command.execute(env);
            } else if (command instanceof SystemCommand) {
                if (executeLocally.get()) {
                    if(fine) {
                        log().log(Level.FINE, "ui.cs.el", command.getClass().getName());
                    }
                    ret = command.execute(env);
                } else if (SystemUtil.isThisCSJvm()) {
                    if(fine) {
                        log().log(Level.FINE, "ui.cs.els", command.getClass().getName());
                    }
                    ConfigurationService cq = null;
                    JVMImpl jvm = JVMImpl.getInstance();
                    if(jvm != null) {
                        cq = jvm.getCs();
                    }
                    if (cq != null) {
                        ret = cq.run(command);
                    } else {
                        throw new GrmException("ui.cs.csmisinitialized", BUNDLE_NAME);
                    }
                } else {
                    if(fine) {
                        log().log(Level.FINE, "ui.cs.er", command.getClass().getName());
                    }
                    try {
                        ret = getProxy(env).run(command);
                    } catch(UndeclaredThrowableException e) {
                        throw new GrmException("ui.cs.cserror", e.getCause(), BUNDLE_NAME);
                    }
                }
            } else {
                throw new GrmException("ui.cs.commandnotsupported", BUNDLE_NAME, new Object[]{command.getClass().getName()});
            }
            if(fine) {
                log().log(Level.FINE,"ui.cs.cf", command.getClass().getName());
            }
            return ret;
        } catch(GrmException ex) {
            LogRecord lr = new LogRecord(Level.FINE, "ui.cs.ce");
            lr.setParameters(new Object [] { command.getClass().getName(), ex.getLocalizedMessage() });
            lr.setThrown(ex);
            lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE_NAME));
            log().log(lr);
            throw ex;
        }
    }
    /**
     *  This thread local flag signalizes that all command inside this thread
     *  will be executed locally.
     */
    private static ThreadLocal<Boolean> executeLocally = new ThreadLocal<Boolean>() {

        @Override
        protected synchronized Boolean initialValue() {
            return false;
        }
    };

    /**
     * Execute a command on the local queue, no matter if it is a local or
     * a system command. This is useful for installation.
     *
     * @param command   the command which should be execution
     * @throws com.sun.grid.grm.GrmException if the execution of the command failed
     * @return the result of the command
     */
    public <T> Result<T> executeLocally(Command<T> command) throws GrmException {
        executeLocally.set(true);
        try {
            return execute(command);
        } finally {
            executeLocally.set(false);
        }
    }

    /**
     * The CommandServiceImpl is not supposed to be instantiated. The only use of
     * the class is to call <code>execute(ExecutionEnv env, Command<T> command)</code>
     * to submit a command.
     * @param env the execution environment
     */
    public CommandServiceImpl(ExecutionEnv env) {
        this.env = env;
    }
}
