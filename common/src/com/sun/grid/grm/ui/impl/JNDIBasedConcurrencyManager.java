/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.impl;

import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.ConcurrencyManager;
import com.sun.grid.grm.ui.SystemCommand;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Simple ConcurrencyManager, that makes decission whether two commands can run 
 * concurrently. Decission is based on JNDI operations that are used in commands.
 * The commands with the same ticket number can be executed concurrently.
 */
public class JNDIBasedConcurrencyManager implements ConcurrencyManager {

    public boolean concurrentRunAllowed(Command a, Command b) {
        
        if ((a instanceof CSModifyCommand) && (b instanceof CSModifyCommand)) {
            List<String> aList = ((CSModifyCommand)a).getListOfModifyNames();
            List<String> bList = ((CSModifyCommand)b).getListOfModifyNames();
            // Assume that is any of commands has empty list the CommandService is locked
            if (!aList.isEmpty() && !bList.isEmpty() && !hasCommonElems(aList, bList)) {
                return true;
            }
            return false;
        } else {
            return true;
        }
    }
    
    
    /**
     * Helper method for checking if two lists with defined String are containing the same elements.
     * It checks if strings are equal and if one contains the other.
     * For example:
     * test.* and test.test1 are also treated as common elements
     */
    private boolean hasCommonElems(List<String> a, List<String> b) {
        List<String> tmp = new LinkedList<String>(a);
        tmp.retainAll(b);
        if (tmp.isEmpty()) {
            for (String s : a) {
                int tmpLast = s.lastIndexOf(".");
                if (tmpLast <= 0) {
                    continue;
                }
                if (s.substring(tmpLast,s.length()).equals(".*")) {
                    
                    String s1 = s.substring(0,tmpLast);
                    for (String s2 : b) {
                        int tmpLast2 = s2.lastIndexOf(".");
                        if (tmpLast2 <= 0) {
                            continue;
                        }
                        if (s2.substring(0,tmpLast2).equals(s1)) {
                            return true;
                        }
                    }
                }
            }
            for (String s : b) {
                int tmpLast = s.lastIndexOf(".");
                if (tmpLast <= 0) {
                    continue;
                }
                if (s.substring(tmpLast,s.length()).equals(".*")) {
                    
                    String s1 = s.substring(0,tmpLast);
                    for (String s2 : a) {
                        int tmpLast2 = s2.lastIndexOf(".");
                        if (tmpLast2 <= 0) {
                            continue;
                        }
                        if (s2.substring(0,tmpLast2).equals(s1)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

}
