/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.JvmConfig;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * Helper class which provides set of methods for matching the expressions
 * used in commands. 
 */
public final class FilterUtil {
    /**
     * i18n Bundle name
     */
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.messages";
    
    private FilterUtil() {
    }
    
    /**
     * Relaxed filter for matching two string values. If source value is 
     * <code>null</code> or empty string, the condition is met (it behaves like
     * a wildcard comparison).
     * 
     * @param source string that has to be compared
     * @param target string acting like a filter
     * @return true if source string is <code>null</code>, is empty string or 
     * source string equals target string, false otherwise
     */
    public static final boolean matchRelaxedString(String source, String target) {
        if ((source == null) || (source.length() == 0) || (source.equals(target))) {
            return true;
        } else {
            return false;
        }
    }

    

    
     /** 
     * Search for the jvm on which the given component should run
     *
     * @param jvms - list of JvmConfig 
     * @param componentName - name of component 
     * @return  name of JVM on which component with specified name should run
     * @throws com.sun.grid.grm.GrmException
     */
   public static String getJvmName(List<JvmConfig> jvms, String componentName) throws GrmException{
       if (jvms == null || componentName == null) {
           throw new GrmException("ui.filter.NullArgumentNotAllowed");
       }
       
       for(JvmConfig jvm : jvms) {
           for (Component c : jvm.getComponent()) {
               if (c.getName().equals(componentName)) {
                   return jvm.getName();
               }
           }
       }
       throw new GrmException("ui.filter.ComponentNotFound", componentName);
   }
    
    /** 
     * Search for the jvmCofig with given jvmName
     *
     * @param jvms - list of JvmConfigs 
     * @param jvmName - name of jvm 
     * @return  JvmConfig owith a given jvmName
     * @throws com.sun.grid.grm.GrmException
     */
   public static JvmConfig getJvmConfig(List<JvmConfig> jvms, String jvmName) throws GrmException{
       if (jvms == null || jvmName == null) {
           throw new GrmException("ui.filter.NullArgumentNotAllowed");
       }
       
       for(JvmConfig jvm : jvms) {
           if (jvm.getName().equals(jvmName)) {
               return jvm;
           }
       }
       throw new GrmException("ui.filter.ComponentNotFound", jvmName);
   }
   
    /**
     * Takes snapshot of Object
     * 
     * @param object object to be stored to stream of bytes
     * @return byte[] object streamed to bytes
     * @throws com.sun.grid.grm.GrmException
     */
    public static  <T> byte[] makeObjectSnapShot(T object) throws GrmException{
        byte [] snapshot = null;
        
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);

            if (oos != null) {
                oos.writeObject(object);
                oos.flush();
                snapshot = bos.toByteArray();
                bos.close();
                oos.close();
            } else {
                throw new GrmException("ui.filter.snapshotfailed", BUNDLE_NAME);
            }
        } catch (IOException ioEx) {
            throw new GrmException("ui.filter.snapshotfailed", ioEx, BUNDLE_NAME, ioEx.getLocalizedMessage());
        }
        
        return snapshot;
    }
    /**
     * Retrieves Object from snapshot
     * 
     * @param snapshot stream of bytes to which object was stored
     * @return &lt;T&gt; object retrieved from stream of bytes
     * @throws com.sun.grid.grm.GrmException
     */
     @SuppressWarnings(value = "unchecked")
    public static <T> T getObjectSnapShot(byte[] snapshot) throws GrmException {
        ObjectInputStream ois;
        ByteArrayInputStream bis;
        T object;
        try {           
            bis = new ByteArrayInputStream(snapshot);
            ois = new ObjectInputStream(bis);
            object = (T) ois.readObject();
            ois.close();
            bis.close();
        } catch (IOException ioEx) {
            throw new GrmException("ui.filter.snapshotfailed", ioEx, BUNDLE_NAME, ioEx.getLocalizedMessage());
        } catch (ClassNotFoundException ex) {
            throw new GrmException("ui.filter.snapshotfailed", ex, BUNDLE_NAME, ex.getLocalizedMessage());
        }
        
        return object;
    }
    
    
    
}
