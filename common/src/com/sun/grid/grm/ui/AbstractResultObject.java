/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import java.io.Serializable;

/**
 * Abstract class for result object returned from UI commands
 * This object will provide comprehensive data that can be used as input data
 * for generating UI output
 */
public abstract class AbstractResultObject implements Serializable{
    
    private static final String ERROR = "ERROR";
    private Throwable error;
    private String message;
    private String result;

    /**
     * Get message string
     * @return String message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set message string
     * @param message string with message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get result string
     * @return result string - should contain result message like "ERROR", "DONE",
     * "OK", "CONNECTED". "ERROR" strint is set automatically when the error for 
     *  this object is set.
     */
    public String getResult() {
        return result;
    }

    /**
     * Set result string
     * @param result string - should contain result message like "ERROR", "DONE",
     * "OK", "CONNECTED". "ERROR" string is set automatically when the error for 
     *  this object is set.
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * Get error
     * @return error - Throwable assigned to this instance of ResultObject.
     */
    public Throwable getError() {
        return error;
    }

    /**
     * Set error
     * @param error - Throwable, this setter method automatically sets, "ERROR"
     * as result in this instance, and localized messages as message in this instance.
     */
    public void setError(Throwable error) {
        this.result = ERROR;
        this.message = error.getLocalizedMessage();
        this.error = error;
    }
    
    
    
}
