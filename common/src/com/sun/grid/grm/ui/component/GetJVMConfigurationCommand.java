/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.naming.ConfigurationObjectNotFoundException;
import com.sun.grid.grm.ui.impl.FilterUtil;
import java.util.List;
import java.util.logging.Logger;

/**
 * Command that retrieves JvmConfig that matches specified JvmName
 */
public class GetJVMConfigurationCommand extends AbstractSystemCommand<JvmConfig> {

    /**
     * i18n Bundle name
     */
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(GetJVMConfigurationCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;

    private String jvmName;

    /**
     * Creates an instance of GetJVMConfigurationCommand.
     */
    public GetJVMConfigurationCommand() {
        this.jvmName = null;
    }  
    
    /**
     * Creates an instance of GetJVMConfigurationCommand.
     * 
     * @param jvm name of the JVM
     */
    public GetJVMConfigurationCommand(String jvm) {
        this.jvmName = jvm;
    } 

    public Result<JvmConfig> execute(ExecutionEnv env) throws GrmException {

        Result<JvmConfig> r = new CommandResult<JvmConfig>();
        
        GetGlobalConfigurationCommand cmd = new GetGlobalConfigurationCommand();
        GlobalConfig global = cmd.execute(env).getReturnValue();
        
        List<JvmConfig> jvms = global.getJvm();
        for (JvmConfig jvmc : jvms) {
            if (FilterUtil.matchRelaxedString(jvmName, jvmc.getName())) {
                r.setReturnValue(jvmc);
                return r;
            }
        }
        if (r == null) {
            throw new ConfigurationObjectNotFoundException("ui.context.objectnotfound", BUNDLE_NAME, new Object[]{"JVMConfig", jvmName});
        }
        return r;        
    }

    /**
     * Sets the name of the JVM
     * @param jvm
     */
    public void setJVMName(String jvm) {
        this.jvmName = jvm;
    }

    /**
     * Gets the name of the JVM
     * @return the JVM name
     */
    public String getJVMName() {
        return this.jvmName;
    }
    
}
