/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.slo.SLOState;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *  This command queries the list of needs of the services
 */
public class GetServiceNeedsCommand extends AbstractQuerySLOsCommand<List<ServiceNeed>> {

    private List<ServiceNeed> result;
    
    /**
     * adds a ServiceNeed for each need of the SLO state to the result.
     * @param serviceName  name of the service
     * @param sloState     the slo state
     */
    @Override
    protected void processSLO(String serviceName, SLOState sloState) {
        if(result == null) {
            result = new LinkedList<ServiceNeed>();
        }
        List<Need> needs = sloState.getNeeds();
        if(needs == null || needs.isEmpty()) {
            result.add(new ServiceNeed(serviceName, sloState.getSloName(), null));
        } else {
            for (Need need : needs) {
                result.add(new ServiceNeed(serviceName, sloState.getSloName(), need));
            }
        }
    }
    
    /**
     * This method is call if the slos for a service can not be processed
     * @param serviceName  name of the service
     * @param error        the error 
     */
    protected void serviceError(String serviceName, GrmException error) {
        if(result == null) {
            result = new LinkedList<ServiceNeed>();
        }
        result.add(new ServiceNeed(serviceName, error));
    }
    

    /**
     * Returns a list of ServiceNeed objects (one entry per need)
     * @return list of ServiceNeed objects
     */
    @Override
    protected List<ServiceNeed> getResult() {
        List<ServiceNeed> ret = result == null ? Collections.<ServiceNeed>emptyList() : result;
        result = null;
        return ret;
    }
    
}
