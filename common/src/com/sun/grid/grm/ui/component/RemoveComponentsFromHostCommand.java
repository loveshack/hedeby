/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.MultiComponent;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import java.util.LinkedList;
import java.util.List;

/**
 * UI Command for removing all compomponents and its configurations that should be run
 * on specific host, if a component is a <code>MultiComponent</code> the entry
 * specifying that the component should this host will be removed.
 * With this command GlobalConfig and the component configurations can be modified.
 */
public class RemoveComponentsFromHostCommand extends AbstractSystemCommand<Void> implements CSModifyCommand{
    private Hostname hostname;
    private final List<RemoveConfigurationCommand> removed = new LinkedList<RemoveConfigurationCommand>();
    private ModifyGlobalConfigurationCommand modGlobal;
    private static final long serialVersionUID = -2007121401L;
    /**
     * Creates a new instance of RemoveComponentsFromHostCommand
     */
    public RemoveComponentsFromHostCommand(Hostname hostname) {
        this.setHostname(hostname);
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        modGlobal = null;
        removed.clear();
        List<String> rmComponents = new LinkedList<String>();
        try {
            GetGlobalConfigurationCommand cmd = new GetGlobalConfigurationCommand();
            GlobalConfig global = cmd.execute(env).getReturnValue();
            
            for (JvmConfig jvm : global.getJvm()) {
                List<Component> compos = new LinkedList<Component>();
                for (Component c : jvm.getComponent()) {                   
                    if (c instanceof Singleton) {
                        if (getHostname().equals(Hostname.getInstance(((Singleton)c).getHost()))) {
                            compos.add(c);
                        }
                    } else if (c instanceof MultiComponent) {
                        List <String> hosts = new LinkedList<String>();
                        for (String host : ((MultiComponent)c).getHosts().getInclude()) {
                            if (getHostname().equals(Hostname.getInstance(host))) {
                                hosts.add(host);
                            }                           
                        }
                        ((MultiComponent)c).getHosts().getInclude().removeAll(hosts);
                    }
                }               
                jvm.getComponent().removeAll(compos);
                for (Component c : compos) {
                    rmComponents.add(c.getName());
                }
            }
            //update global
            modGlobal = new ModifyGlobalConfigurationCommand(global);
            modGlobal.execute(env);
            
            for (String c : rmComponents) {
                //remove not needed configs
                RemoveConfigurationCommand rc = new RemoveConfigurationCommand(c);
                rc.execute(env);            
                //save the removed commands so we can call undo on this one
                removed.add(rc);
            }
        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }
        
        return CommandResult.VOID_RESULT;
    }
    
    @Override
    public void undo(ExecutionEnv env) {
        
        for (RemoveConfigurationCommand rc : removed) {
            rc.undo(env);
        }
        removed.clear();
        if (modGlobal != null) {
            modGlobal.undo(env);
        }
    }

    public Hostname getHostname() {
        return hostname;
    }

    public void setHostname(Hostname hostname) {
        this.hostname = hostname;
    }

    public List<String> getListOfModifyNames() {
        List<String> ret = new LinkedList<String>();
        ret.add(BootstrapConstants.CS_GLOBAL);
        ret.add(BootstrapConstants.CS_COMPONENT+"."+"*");
        return ret;
    }
}
