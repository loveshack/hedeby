/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.config.common.ComponentConfig;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 * Command that retrieves list of all ComponentConfigs that match provided
 * combination of ConfigurationName, JvmName, ComponentName and Hostname
 *
 * @param <C>
 */
public class GetComponentConfigurationCommand<C extends ComponentConfig> extends AbstractSystemCommand<List<C>> {

    /**
     * i18n Bundle name
     */
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(GetComponentConfigurationCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;

    private String jvmName;
    private String componentName;
    private String hostname;
    private String configurationName;

    /**
     * Creates an instance of GetComponentConfigurationCommand.
     */
    public GetComponentConfigurationCommand() {
        this.jvmName = null;
        this.componentName = null;
        this.hostname = null;
    }

    public Result<List<C>> execute(ExecutionEnv env) throws GrmException {
        try {


            GetComponentCommand cmd = new GetComponentCommand();
            cmd.setComponentName(componentName);
            cmd.setHostname(hostname);
            cmd.setJVMName(jvmName);
            Result<List<Component>> compos = cmd.execute(env);
            
            Context ctx = env.getContext();
            
            List<C> configs = new LinkedList<C>();
            for (Component c : compos.getReturnValue()) {
                @SuppressWarnings(value = "unchecked")
                C config = (C) ctx.lookup(BootstrapConstants.CS_COMPONENT + "." + c.getName());
                if (config != null) {
                    configs.add(config);
                }
                
            }
            if (ctx != null) {
                ctx.close();
            }
            Result<List<C>> r = new CommandResult<List<C>>();
            r.setReturnValue(configs);
            return r;
        } catch (NamingException ex) {
            this.undo(env);
            GrmException grm = new GrmException("ui.context.operationfailed", ex, BUNDLE_NAME, ex.getLocalizedMessage());
            throw grm;
        }
    }

    /**
     * Sets the name of the JVM
     * @param jvm
     */
    public void setJVMName(String jvm) {
        this.jvmName = jvm;
    }

    /**
     * Gets the name of the JVM
     * @return the JVM name
     */
    public String getJVMName() {
        return this.jvmName;
    }

    /**
     * Sets the hostname
     * @param host
     */
    public void setHostname(String host) {
        this.hostname = host;
    }

    /**
     * Gets the hostname
     * @return the hostname
     */
    public String getHostname() {
        return this.hostname;
    }

    /**
     * Sets the Component name
     * @param component
     */
    public void setComponentName(String component) {
        this.componentName = component;
    }

    /**
     * Gets the Component name
     * @return the component name
     */
    public String getComponentName() {
        return this.componentName;
    }

    /**
     * Sets the Configuration name
     * @param configName
     */
    public void setConfigurationName(String configName) {
        this.configurationName = configName;
    }

    /**
     * Gets the Configuration name
     * @return the Configuration name
     */
    public String getConfigurationName() {
        return this.configurationName;
    }

}
