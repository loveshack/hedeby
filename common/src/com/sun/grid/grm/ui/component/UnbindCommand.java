/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.ui.impl.FilterUtil;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 * Abstract base class for deleting an item from CS
 * @param T The type of the value
 */
public class UnbindCommand<T> extends AbstractSystemCommand<Void> implements CSModifyCommand{
   
    private final static long serialVersionUID = -2007112601L;
    
    private String name = null;
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(UnbindCommand.class.getName());
    private byte[] snapshot = null;

    /**
     * Create a new instance of AbstractUnbindCommand
     * @param name the name which should be deleted
     */
    public UnbindCommand(String name) {
        this.name = name;
    }
    
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        snapshot = null;

        try {
            Context ctx = env.getContext();
            @SuppressWarnings("unchecked")
            T obj = (T)ctx.lookup(name);
            // save object for undo
            snapshot = FilterUtil.makeObjectSnapShot(obj);
            ctx.unbind(name);
            ctx.close();
            
            return CommandResult.VOID_RESULT;
        } catch (NamingException ex) {
            throw new GrmException("ui.context.operationfailed", ex, BUNDLE_NAME, ex.getLocalizedMessage());
        }
    }
    
    @Override
    public void undo(ExecutionEnv env) {
        
        //if snapshot null, we have nothing to undo
        if (snapshot!= null) {
            try {
                T obj = FilterUtil.<T>getObjectSnapShot(snapshot);
                Context ctx = env.getContext();
                ctx.rebind(name, obj);
                ctx.close();
            } catch (GrmException ex) {
                log.log(Level.WARNING, "ui.command.undofailed", ex);
            } catch (NamingException ex) {
                log.log(Level.WARNING, "ui.command.undofailed", ex);
            }
        } 
        snapshot = null;
    }

    /**
     * Getter for the name of the bound value
     * @return the name of the bound value
     */ 
    public String getName() {
        return name;
    }
    
    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(name);
    }
}
