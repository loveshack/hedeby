/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.bootstrap.ComponentInfo;
import java.io.Serializable;

/**
 *  Instances of this class contains the result of an action which
 *  has been execution on a component
 *
 *  @see com.sun.grid.grm.ui.component.AbstractComponentCommand
 */
public class ComponentChangeResult implements Serializable {
    
    private static final long serialVersionUID = -2007112301L;
    
    private final ComponentInfo componentInfo;
    private final String message;
    private final Throwable error;
    
    /**
     * Create a new instance of ComponentChangeResult
     * @param ci       the component info of the affected component
     * @param message  the result message
     */
    public ComponentChangeResult(ComponentInfo ci, String message) {
        this(ci, message, null);
    }
    
    /**
     * Create a new instance of ComponentChangeResult
     * @param ci       the component info of the affected component
     * @param message  the result message
     * @param error    the error which has been occured
     */
    public ComponentChangeResult(ComponentInfo ci, String message, Throwable error) {
        this.componentInfo = ci;
        this.message = message;
        this.error = error;
    }

    /**
     * Get the component info of the affected component
     * @return the component info of the affected component
     */
    public ComponentInfo getComponentInfo() {
        return componentInfo;
    }

    /**
     * The message (will be displayed in the ui)
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    
    /**
     * Get the error of the action
     * @return the error of the action
     */
    public Throwable getError() {
        return error;
    }
    
}
