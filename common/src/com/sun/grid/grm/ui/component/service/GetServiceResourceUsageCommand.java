/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceIdAndName;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.slo.SLOState;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This command the resource usage of a service according to the SLO definitions
 */
public class GetServiceResourceUsageCommand extends AbstractQuerySLOsCommand<List<ServiceResourceUsage>> {

    private List<ServiceResourceUsage> result;
    
    /**
     * Add for each entry of the usage map of the slo state a ServiceResourceUsage
     * object to the result
     * @param serviceName  name of the service
     * @param sloState     the slo state
     */
    @Override
    protected void processSLO(String serviceName, SLOState sloState) {
        if(result == null) {
            result = new LinkedList<ServiceResourceUsage>();
        }
        Map<Resource, Usage> usageMap = sloState.getUsageMap();
        if (usageMap == null || usageMap.isEmpty()) {
            result.add(new ServiceResourceUsage(serviceName, sloState.getSloName(), null, null));
        } else {
            for (Map.Entry<Resource, Usage> entry : usageMap.entrySet()) {
                result.add(new ServiceResourceUsage(serviceName, sloState.getSloName(), new ResourceIdAndName(entry.getKey()), entry.getValue()));
            }
        }
    }
    
    /**
     * This method is call if the slos for a service can not be processed
     * @param serviceName  name of the service
     * @param error        the error 
     */
    protected void serviceError(String serviceName, GrmException error) {
        if(result == null) {
            result = new LinkedList<ServiceResourceUsage>();
        }
        result.add(new ServiceResourceUsage(serviceName, error));
    }
    

    @Override
    protected List<ServiceResourceUsage> getResult() {
        List<ServiceResourceUsage> ret = result == null ? Collections.<ServiceResourceUsage>emptyList() : result;
        result = null;
        return ret;
    }
}
