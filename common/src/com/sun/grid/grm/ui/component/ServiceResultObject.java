/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.ui.AbstractResultObject;
import java.io.Serializable;

/**
 * Helper Result object for commands that are operating on services
 */
public class ServiceResultObject extends AbstractResultObject implements Serializable{
    private String serviceName = null;
    private String hostName = null;
    private static final long serialVersionUID = -2007112401L;
   /**
     * Create a new instance of ServiceResultObject.
     * 
     * @param serviceName name of service name
     * @param hostname name of host
     * @param error Throwable representing the error
     */
    public ServiceResultObject(String serviceName, String hostname, Throwable error) {
        this.serviceName = serviceName;
        this.hostName = hostname;
        this.setError(error);
    }

    /**
     * Create a new instance of ServiceResultObject.
     * 
     * @param serviceName name of service name
     * @param hostname name of host
     * @param result string
     * @param message string
     */
    public ServiceResultObject(String serviceName, String hostname, String result, String message) {
        this.serviceName = serviceName;
        this.hostName = hostname;
        this.setResult(result);
        this.setMessage(message);
    }
    
    /**
     * Get the service name
     * @return name of service
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Set the service name
     * @param serviceName name of service
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * Get the host name
     * @return name of host
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Set the host name
     * @param hostName name of host
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }
}
