/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.ui.impl.FilterUtil;
import com.sun.grid.grm.util.Hostname;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This command removes component entry from system configuration with component configuration.
 * The component entry with given parameters (name, host) will be removed from 
 * Global configuration of the system, and than the configuration of component 
 * that is stored with componentName is removed
 */

public class RemoveComponentWithConfigurationCommand extends AbstractSystemCommand<Void> implements CSModifyCommand{
    private String componentName = null;
    private String hostname = null;
    private static final long serialVersionUID = -2007080101L;
    private static String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private byte[] globalSnapshot;
    private final List<RemoveConfigurationCommand> removed = new LinkedList<RemoveConfigurationCommand>();
    private static final Logger log = Logger.getLogger(RemoveComponentWithConfigurationCommand.class.getName());
    
    /**
     * Constructor for RemoveComponentWithConfigurationCommand
     * @param componentName - name of component to be removed
     * @param hostname - name of host on which the removed component is supposed to be
     * run
     */
    public RemoveComponentWithConfigurationCommand(String componentName, String hostname) {
        this.componentName = componentName;
        this.hostname = hostname;
        
    }
    
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        globalSnapshot = null;
        removed.clear();
        List<Component> compos = new LinkedList<Component>();

        try {
            GetGlobalConfigurationCommand cmd = new GetGlobalConfigurationCommand();
            GlobalConfig global = cmd.execute(env).getReturnValue();
            //save global state for undo
            globalSnapshot = FilterUtil.<GlobalConfig>makeObjectSnapShot(global);
            
            Hostname hostnameObj = null;
            if(getHostname() != null) {
                hostnameObj = Hostname.getInstance(getHostname());
            }
            List<JvmConfig> jvms = global.getJvm();
            for (JvmConfig jvmc : jvms) {
                Set<Component> compo_set = GetComponentCommand.getComponents(jvmc, getComponentName(), hostnameObj);
                jvmc.getComponent().removeAll(compo_set);
                compos.addAll(compo_set);           
            }
            
            ModifyGlobalConfigurationCommand mod = new ModifyGlobalConfigurationCommand(global);
            mod.execute(env);
            
            GetConfigurationCommand gc = new GetConfigurationCommand();
            
            for (Component c : compos) {          
                    //first save a copy of configuration for undo
                    gc.setConfigName(c.getName());
                    ComponentConfig obj = (ComponentConfig)gc.execute(env).getReturnValue();
                    //the object is saved so we can delete the object
                    RemoveConfigurationCommand rc = new RemoveConfigurationCommand(c.getName());
                    rc.execute(env);
  
                    //save the removed commands so we can call undo on this one
                    removed.add(rc);                           
            }
        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }
        return CommandResult.VOID_RESULT;
    }
    
    @Override
    public void undo(ExecutionEnv env) {
        
        if (globalSnapshot !=null){
            try {
                for (RemoveConfigurationCommand rc : removed) {
                    rc.undo(env);
                }
                removed.clear();
                //revert changes to global config
                GlobalConfig global = FilterUtil.<GlobalConfig>getObjectSnapShot(globalSnapshot);
                ModifyGlobalConfigurationCommand mod = new ModifyGlobalConfigurationCommand(global);
                mod.execute(env);
            } catch (GrmException ex) {
                log.logrb(Level.FINE, RemoveComponentWithConfigurationCommand.class.getName(), "undo", BUNDLE_NAME, "ui.command.undofailed", ex);
            }
        }
    }
    
    /**
     * Getter for component name
     * @return name of component
     */
    public String getComponentName() {
        return componentName;
    }

    /** 
     * Setter for component name
     * @param componentName - name of component
     */
    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }
    
    /**
     * Getter for host
     * @return hostname
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * Setter for host
     * @param hostname - name of host
     */
    public void setHost(String hostname) {
        this.hostname = hostname;
    }
    
    public List<String> getListOfModifyNames() {
        List<String> ret = new LinkedList<String>();
        ret.add(BootstrapConstants.CS_GLOBAL);
        ret.add(BootstrapConstants.CS_COMPONENT+"."+getComponentName());
        return ret;
    }
}
