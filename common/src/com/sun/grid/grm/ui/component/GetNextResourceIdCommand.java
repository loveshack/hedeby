/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ResourceIdSequence;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

/**
 * The command requests new resource ids.
 */
public class GetNextResourceIdCommand extends AbstractSystemCommand<Long> implements Serializable, CSModifyCommand {

    private final static long serialVersionUID = -2009072101L;
    private final static String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private final static Logger log = Logger.getLogger(GetNextResourceIdCommand.class.getName(), BUNDLE_NAME);
    // pacakge private for testing
    final static String RESOURCE_IDS_NAME = "next_resource_id";
    private final static List<String> LIST_OF_MODIFY_NAMES = Collections.singletonList(RESOURCE_IDS_NAME);
    
    private volatile transient Command<Void> bindCommand;
    
    /**
     * Execute the command.
     * 
     * This method increments the value of the next_resource_id value from
     * the naming context and returns the request id range
     *  
     * @param env the execution env
     * @return  the command result with the ResourceIdRange as value
     * @throws com.sun.grid.grm.GrmException if the resource ids could not be requested
     */
    public Result<Long> execute(ExecutionEnv env) throws GrmException {

        Context ctx = env.getContext();
        
        ResourceIdSequence nextResId;
        try {
            nextResId = (ResourceIdSequence) ctx.lookup(RESOURCE_IDS_NAME);
        } catch (NameNotFoundException ex) {
            nextResId = new ResourceIdSequence();
            nextResId.setNextResourceId(1);
        } catch (NamingException ex) {
            throw new GrmException("gnrids.ex.lookup", ex, BUNDLE_NAME, ex.getLocalizedMessage());
        } finally {
            try {
                ctx.close();
            } catch (NamingException ex) {
                log.log(Level.WARNING, "gnrids.close", ex);
            }
        }

        long id = nextResId.getNextResourceId();
        nextResId.setNextResourceId(id + 1);
        Command<Void> bc;
        if (id == 1) {
            bc = new BindCommand<ResourceIdSequence>(RESOURCE_IDS_NAME, nextResId);
        } else {
            bc = new RebindCommand<ResourceIdSequence>(RESOURCE_IDS_NAME, nextResId);
        }
        
        bc.execute(env);
        this.bindCommand = bc;
        return new CommandResult<Long>(id);
    }

    /**
     * Undo the changes in the next_resource_id element.
     * @param env the execution env
     */
    @Override
    public void undo(ExecutionEnv env) {
        if (bindCommand != null) {
            bindCommand.undo(env);
        }
    }

    /**
     * Get the list of names which are modified by this command.
     * 
     * @return returns a singleton list with the string "next_resource_id";
     */
    public List<String> getListOfModifyNames() {
        return LIST_OF_MODIFY_NAMES;
    }
}
