/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.ui.AbstractResultObject;

/**
 * Helper ResultObject for commands that are executing some operations on JVMs,
 * the object contains such fields like jvmName nad hostName to identify the JVM on
 * which operetion was performed.
 */

public class JVMResultObject extends AbstractResultObject {
    
    private final static long serialVersionUID = -2008031701L;
    
    private final String jvmName;
    private final String hostName;
    
   /**
     * Create a new instance of JVMResultObject.
     * 
     * @param jvmName name of jvm
     * @param hostname name of host
     * @param error Throwable representing the error
     */
    public JVMResultObject(String jvmName, String hostname, Throwable error) {
        this.jvmName = jvmName;
        this.hostName = hostname;
        this.setError(error);
    }

    /**
     * Create a new instance of JVMResultObject.
     * 
     * @param jvmName name of jvm
     * @param hostname name of host
     * @param result string
     * @param message string
     */
    public JVMResultObject(String jvmName, String hostname, String result, String message) {
        this.jvmName = jvmName;
        this.hostName = hostname;
        this.setResult(result);
        this.setMessage(message);
    }
    
    /**
     * Get the jvm name
     * @return name of jvm
     */
    public String getJvmName() {
        return jvmName;
    }

    /**
     * Get the host name
     * @return name of host
     */
    public String getHostName() {
        return hostName;
    }

}
