/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.MultiComponent;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.config.common.HostSet;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 *  This command is a <code>MacroCommand</code> which makes it possible to
 *  add component and it's configuration object in one step.
 * 
 *  <H3>Usage</H3>
 *
 *  <pre>
 *      SampleComponentConfig config = new SampleComponentConfig();
 *  
 *      // initialze the config
 *      ...
 *
 *      // Create the command
 *      AddComponentWithConfigurationCommand addCompCommand = 
 *              new AddComponentWithConfigurationCommand()
 *              .classname(SimpleComponentImpl.class.getName())
 *              .componentName("sample")
 *              .configName("sample")
 *              .config(config)
 *              .hostname(".*")
 *              .jvmName("vm0");
 *
 *      // Execute the command
 *      CommandService.execute(env, addCompCommand);
 *  </pre>
 */
public class AddComponentWithConfigurationCommand extends AbstractSystemCommand<Void> implements CSModifyCommand {
    
    private static final long serialVersionUID = -2007101201L;
    
    private final Component comp;
    private final AddComponentCommand addComponentCmd = new AddComponentCommand();
    private final AddConfigurationCommand addConfigCmd = new AddConfigurationCommand();
    private final LinkedList<Command> commands = new LinkedList<Command>();
    private Stack<Command> finished = new Stack<Command>();
    /** 
     * Creates a new instance of AddComponentWithConfigurationCommand.
     *
     * @param aComp the component configuration
     */
    private AddComponentWithConfigurationCommand(Component aComp) {
        comp = aComp;
        commands.add(addComponentCmd);        
        commands.add(addConfigCmd);
        addComponentCmd.setComponent(comp);
    }
    
    /**
     * Create a new instance of a AddComponentWithConfigurationCommand
     * for a MultiComponent.
     *
     * @return thew new instance of AddComponentWithConfigurationCommand
     */
    public static AddComponentWithConfigurationCommand newInstanceForMultiComponent() {
        return new AddComponentWithConfigurationCommand(new MultiComponent());
    }
    
    /**
     * Create a new instance of a AddComponentWithConfigurationCommand
     * for a Singleton.
     *
     * @return thew new instance of AddComponentWithConfigurationCommand
     */
    public static AddComponentWithConfigurationCommand newInstanceForSingleton() {
        return new AddComponentWithConfigurationCommand(new Singleton());
    }
    
   
    /**
     *  Set the name of  jvm where the component should run.
     *
     *  @param jvmName the name of the jvm
     *  @return reference to this <code>AddComponentWithConfigurationCommand</code>
     */
    public AddComponentWithConfigurationCommand jvmName(String jvmName) {
        addComponentCmd.setJVMName(jvmName);
        return this;
    } 
    
    /**
     *  Set the name of the component.
     *
     *  @param componentName the name of the jvm
     *  @return reference to this <code>AddComponentWithConfigurationCommand</code>
     */
    public AddComponentWithConfigurationCommand componentName(String componentName) {
        comp.setName(componentName);
        addConfigCmd.setName(componentName);
        return this;
    } 
    
    /**
     *  Set the name of the component implementation class.
     *
     *  @param classname the name of the component implementation class
     *  @return reference to this <code>AddComponentWithConfigurationCommand</code>
     */
    public AddComponentWithConfigurationCommand classname(String classname) {
        comp.setClassname(classname);
        return this;
    } 

    /**
     *  Set the configuration object of the component.
     *
     *  @param config the configuration object
     *  @return reference to this <code>AddComponentWithConfigurationCommand</code>
     */
    public AddComponentWithConfigurationCommand config(ComponentConfig config) {
        addConfigCmd.setValue(config);
        return this;
    } 

    /**
     *  Set the name of the host where the component should run
     *
     *  @param hostname the name of the host
     *  @return reference to this <code>AddComponentWithConfigurationCommand</code>
     */
    public AddComponentWithConfigurationCommand hostname(String hostname) {
        if(comp instanceof Singleton) {
            ((Singleton)comp).setHost(hostname);
        } else if (comp instanceof MultiComponent) {
            if(!((MultiComponent)comp).isSetHosts()) {
                HostSet hosts = new HostSet();
                ((MultiComponent)comp).setHosts(hosts);
            }
            ((MultiComponent)comp).getHosts().getInclude().add(hostname);
        } else {
            throw new IllegalStateException("Unknown component type " + comp);
        }
        return this;
    } 

    
    @SuppressWarnings(value = "unchecked")
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        finished.clear();

        
        try {
            while(!commands.isEmpty()) {
                Command cmd = commands.poll();
                cmd.execute(env);
                finished.push(cmd);
            }
        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }
        return CommandResult.VOID_RESULT;
    }

    public void undo(ExecutionEnv env) {
        
        while(!finished.empty()) {
            finished.pop().undo(env);
        }
        
    }

    public List<String> getListOfModifyNames() {
        List<String> ret = new LinkedList<String>();
        ret.add(BootstrapConstants.CS_GLOBAL);
        ret.add(BootstrapConstants.CS_COMPONENT+"."+addConfigCmd.getName());
        return ret;
    }
    

}
