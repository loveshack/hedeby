/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.VariableResolver;
import java.io.Serializable;

/**
 *
 * @param T 
 */
public class NameFilterListCommand<T> extends ListCommand<T> {
    
    private static final long serialVersionUID = -2007080101L;
    private Filter<String> filter;

    public NameFilterListCommand(String name) {
        super(name);
    }
    
    public NameFilterListCommand(String name, Filter<String> filter) {
        this(name);
        this.filter = filter;
    }

    @Override
    protected boolean accept(String name, T value) {
        if(filter != null) {
            resolver.setFilterObject(name);
            return filter.matches(resolver);
        }
        return true;
    }
    
    private final NameVariableResolver resolver = new NameVariableResolver();
    
    private static class NameVariableResolver implements VariableResolver<String>, Serializable {
        
        private final static long serialVersionUID = -2007112901L;
        private String name;
        
        public void setFilterObject(String name) {
            this.name = name;
        }

        public Object getValue(String name) {
            if(name.equals("name")) {
                return this.name;
            }
            return null;
        }
    }
    
    public Filter<String> getFilter() {
        return filter;
    }

    public void setFilter(Filter<String> filter) {
        this.filter = filter;
    }
    
}
