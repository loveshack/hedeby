/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingException;

/**
 * Command that adds new active component to CS
 */
public class AddActiveComponentCommand extends AbstractSystemCommand<Void> implements CSModifyCommand{

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.service.messages";
    private static final Logger log = Logger.getLogger(AddActiveComponentCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;

    private ActiveComponent ac;
    private String acName;

    /**
     * Creates an instance of AddActiveComponentCommand.
     */
    public AddActiveComponentCommand() {
        this.ac = null;
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {

        Context ctx = env.getContext();
        try {
            try {
                ctx.bind(BootstrapConstants.CS_ACTIVE_COMPONENT + "." + acName, ac);
            } catch (NameAlreadyBoundException ex) {
                ctx.rebind(BootstrapConstants.CS_ACTIVE_COMPONENT + "." + acName, ac);
            }
        } catch (NamingException ex) {
            this.undo(env);
            GrmException grm = new GrmException("ui.componentservice.bindexception", ex, BUNDLE_NAME, new Object[]{AddActiveComponentCommand.class, acName});
            throw grm;
        } finally {
            if(ctx != null) {
                try {
                    ctx.close();
                } catch(Exception ex) {
                    // Ignore
                }
            }
        }

        return CommandResult.VOID_RESULT;
    }

    /**
     * Sets the active component
     * @param ac the active component
     */
    public void setActiveComponent(ActiveComponent ac) {
        this.ac = ac;
    }

    /**
     * Gets the active component
     * @return the acive component
     */
    public ActiveComponent getActiveComponent() {
        return this.ac;
    }

    /**
     * Sets the active component name
     * @param name active component name
     */
    public void setActiveComponentName(String name) {
        this.acName = name;
    }

    /**
     * Gets the active component name
     * @return the active component name
     */
    public String getActiveComponentName() {
        return this.acName;
    }
    
    
    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(BootstrapConstants.CS_ACTIVE_COMPONENT+"."+getActiveComponentName());
    }
}
