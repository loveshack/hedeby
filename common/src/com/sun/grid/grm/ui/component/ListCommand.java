/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;


/**
 * Base class for search items in CS
 * @param T the type of the search items
 */
public class ListCommand<T> extends AbstractSystemCommand<Map<String,T>> {
    
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final long serialVersionUID = -2007112701L;
    
    private final String name;
    
    /**
     * Create a new instanceof AbstractListCommand
     * @param name base name for the search
     */
    public ListCommand(String name) {
        this.name = name;
    }
    
    /**
     * This method can be overridden if special filtering is necessary
     * Be default all items are accepted
     * @param name   the name of the item
     * @param value  the value of the item
     * @return <code>true</code> if the item should be accepted
     */
    protected boolean accept(String name, T value) {
        return true;
    }
    
    public Result<Map<String,T>> execute(ExecutionEnv env) throws GrmException {
        Context ctx = env.getContext();
        try {
            Map<String,T> ret = new HashMap<String,T>();
            NamingEnumeration<Binding> enumeration = ctx.listBindings(name);
            if (enumeration != null) {
                while (enumeration.hasMore()) {
                    Binding bd = enumeration.next();
                    @SuppressWarnings("unchecked")
                    T value = (T) bd.getObject();
                    if(accept(bd.getName(), value)) {
                        ret.put(bd.getName(), value);
                    }
                }
            }
            return new CommandResult<Map<String,T>>(ret);
        } catch (NameNotFoundException ex) {
            return new CommandResult<Map<String,T>>(Collections.<String,T>emptyMap());
        } catch (NamingException ex) {
            throw new GrmException("AbstractListCommand.ex.lookup", ex, BUNDLE_NAME, name, ex.getLocalizedMessage());
        } finally {
            try {
                ctx.close();
            } catch(NamingException ex) {
                // Ignore
            }
        }
    }
}
