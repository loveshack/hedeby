/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.cli.UsageException;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.service.GetComponentInfosCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.NotFilter;
import java.util.ArrayList;
import java.util.List;

/**
 *  Abstract base class for all command which performs an action on list of
 *  components
 */
public abstract class AbstractStopComponentCommand extends AbstractComponentCommand {
    
    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final long serialVersionUID = -2007112601L;
    
    private boolean all;
    
    /**
     * Creates an instance of AbstractStopComponentCommand
     */
    protected AbstractStopComponentCommand() {
        this.all = false;
    }
    
    /**
     * This method performs the action on a matching component
     * @param comp the component
     * @throws com.sun.grid.grm.GrmException on any error
     * @return the success message
     */
    protected abstract String changeComponent(GrmComponent comp) throws GrmException;
    
    
    /**
     * This method searchs first all matching components and call then
     * for each component the <code>changeComponent</code> method
     * @param env   the exection env
     * @throws com.sun.grid.grm.GrmException on any error
     * @return list a result for each matching component
     */
    @Override
    public Result<List<ComponentChangeResult>> execute(ExecutionEnv env) throws GrmException {
        
        if ((getHostname() == null || getComponentName() ==null) && isAll() == false
                || (getHostname() != null && getComponentName() != null && isAll() == true)) {
            throw new UsageException(I18NManager.formatMessage("ui.stopcomponentcommand.nall", BUNDLE_NAME));
        }
        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>(3);
        
        Filter<ComponentInfo> typeFilter = GetComponentInfosCommand.newTypeFilter(JVM.class);
        NotFilter<ComponentInfo> notFilter = new NotFilter<ComponentInfo>(typeFilter);
        filter.add(notFilter);
        
        if(getHostname() != null) {
            filter.add(GetComponentInfosCommand.newHostFilter(getHostnameObj()));
        }
        if(getComponentName() != null) {
            filter.add(GetComponentInfosCommand.newNameFilter(getComponentName()));
        }
        
        List<ComponentInfo> components = ComponentService.getComponentInfos(env, filter);
        
        if(components.isEmpty()) {
            throw new GrmException("ui.AbstractComponent.ComponentNotFound", BUNDLE_NAME);
        }
        
        List<ComponentChangeResult> affectedComps = new ArrayList<ComponentChangeResult>(components.size());
        
        for (ComponentInfo ci: components) {
            try {
                GrmComponent comp = ComponentService.getComponent(env, ci);
                String msg = changeComponent(comp);
                affectedComps.add(new ComponentChangeResult(ci, msg));
            } catch (GrmException ex) {
                affectedComps.add(new ComponentChangeResult(ci, ex.getLocalizedMessage(), ex));
            } catch (Exception ex) {
                String message = ex.getLocalizedMessage();
                if(message == null || message.equals("null"))  {
                    message = ex.getClass().getName();
                }
                affectedComps.add(new ComponentChangeResult(ci, 
                     I18NManager.formatMessage("ui.AbstractComponent.unknownError", BUNDLE_NAME, message), ex));
            }
        }
        
        return new CommandResult<List<ComponentChangeResult>>(affectedComps);
    }
   
    /*
     * Gets the all value
     * @return all value
     */
    public boolean isAll() {
        return this.all;
    }
    
    /*
     * Sets the all value
     * @param all value
     */
    public void setAll(boolean all) {
        this.all = all;
    }
}
