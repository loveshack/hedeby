/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceIdAndName;
import com.sun.grid.grm.service.Usage;
import java.io.Serializable;

/**
 * Helper class for the result of the GetServiceResourceUsage command
 */
public class ServiceResourceUsage implements Serializable {

    private final static long serialVersionUID = -2007121701L;
    private final String serviceName;
    private final String sloName;
    private final ResourceIdAndName resourceIdAndName;
    private final Usage  usage;
    private final GrmException error;
    
    /**
     * Create a new instance of ServiceResourceUsage
     * @param serviceName  name of the service
     * @param sloName      name of the slo
     * @param resourceIdAndName   name and id the of the used resource
     * @param usage        usage of the resource
     */
    public ServiceResourceUsage(String serviceName, String sloName, ResourceIdAndName resourceIdAndName, Usage usage) {
        this.serviceName = serviceName;
        this.sloName = sloName;
        this.resourceIdAndName = resourceIdAndName;
        this.usage = usage;
        this.error = null;
    }
    
    /**
     * Create a new instance of ServiceResourceUsage
     * @param serviceName  name of the service
     * @param error the error which occured while querying slo states for the service
     */
    public ServiceResourceUsage(String serviceName, GrmException error) {
        this.serviceName = serviceName;
        this.sloName = null;
        this.resourceIdAndName = null;
        this.usage = null;
        this.error = error;
    }
    

    /**
     * get the service name
     * @return the service name
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * get the SLO name
     * @return the SLO name
     */
    public String getSloName() {
        return sloName;
    }

    /**
     * Get the resource id
     * @return the resource id
     */
    public ResourceId getResourceId() {
        return resourceIdAndName == null ? null : resourceIdAndName.getId();
    }

    /**
     * Get the name of the resource
     * @return the name of the resource
     */
    public String getResourceName() {
        return resourceIdAndName == null ? null : resourceIdAndName.getName();
    }

    /**
     * Get the usage
     * @return the usage
     */
    public Usage getUsage() {
        return usage;
    }

    /**
     * Get the error 
     * @return the error
     */
    public GrmException getError() {
        return error;
    }
    
    /**
     * Is the error set
     * @return <code>true</code> if the error is set
     */
    public boolean hasError() {
        return error != null;
    }

}
