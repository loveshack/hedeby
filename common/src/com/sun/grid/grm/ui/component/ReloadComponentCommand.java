/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.I18NManager;

/**
 * Command that makes component on a specified host to reload its configuration
 */
public class ReloadComponentCommand extends AbstractComponentCommand {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final long serialVersionUID = -2007112601L;
    private boolean forced;
    private static String msg;

    /**
     * Creates an instance of ReloadComponentCommand
     */
    public ReloadComponentCommand() {
        this.forced = false;
    }
    
    /**
     * Calls the reload method of the component
     * @param comp   the component
     * @throws com.sun.grid.grm.GrmException thrown by the relead method
     * @return the success message
     */
    protected String changeComponent(GrmComponent comp) throws GrmException {
        comp.reload(forced);
        return getMessage();
    }
    

    /**
     * Sets the forced flag
     * @param forced
     */
    public void setForced(boolean forced) {
        this.forced = forced;
    }

    /**
     * Gets the forced flag
     * @return the forced flag
     */
    public boolean getForced() {
        return this.forced;
    }

    private static String getMessage() {
       if(msg == null) {
           msg = I18NManager.formatMessage("ui.ReloadComponent.reloaded", BUNDLE_NAME);
       }
       return msg;
    }
    
}