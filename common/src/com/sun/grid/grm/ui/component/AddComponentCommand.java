/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * Command that adds new Component to specified JVM
 */
public class AddComponentCommand extends AbstractSystemCommand<Void> implements CSModifyCommand {

    /**
     * i18n Bundle name
     */
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(AddComponentCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;

    private Component component;
    private String jvmName;
    private transient ModifyGlobalConfigurationCommand executedCmd;
    /**
     * Creates an instance of AddComponentCommand.
     */
    public AddComponentCommand() {
        this.component = null;
        this.jvmName = null;
    }
 
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        GetGlobalConfigurationCommand cmd = new GetGlobalConfigurationCommand();
        GlobalConfig global = cmd.execute(env).getReturnValue();
        if (this.jvmName == null) {
            this.undo(env);
            throw new GrmException("ui.command.nullnotallowed", BUNDLE_NAME, new Object[]{AddComponentCommand.class.getName(), "jvmName"});
        } else if (this.component == null) {
            this.undo(env);
            throw new GrmException("ui.command.nullnotallowed", BUNDLE_NAME, new Object[]{AddComponentCommand.class.getName(), "component"});
        } else {
            List<JvmConfig> jvms = global.getJvm();
            JvmConfig jvm = null;
            for (JvmConfig jvmc : jvms) {
                if (jvmc.getName().equals(this.jvmName)) {
                    jvm = jvmc;
                    break;
                }
            }
            if (jvm != null) {
                for (JvmConfig jvmc : jvms) {
                    for (Component c : jvmc.getComponent()) {
                        if (c.getName().equals(this.component.getName())) {
                            this.undo(env);
                            throw new GrmException("ui.context.objectexists", BUNDLE_NAME, new Object[]{"component", component.getName()});
                        }
                    }                    
                }                
                jvm.getComponent().add(this.component);
                ModifyGlobalConfigurationCommand mod = new ModifyGlobalConfigurationCommand(global);
                mod.execute(env);
                executedCmd = mod;
                
            } else {
                this.undo(env);
                GrmException grm = new GrmException("ui.context.objectnotfound", BUNDLE_NAME, new Object[]{"jvm", jvmName});
                throw grm;
            }
        }
        return CommandResult.VOID_RESULT;
    }

    @Override
    public void undo(ExecutionEnv env) {
        if (executedCmd != null) {
            executedCmd.undo(env);
            executedCmd = null;
        }
    }
    
    

    /**
     * Sets the Component
     * @param c
     */
    public void setComponent(Component c) {
        this.component = c;
    }

    /**
     * Gets the Component
     * @return the component name
     */
    public Component getComponent() {
        return this.component;
    }

    /**
     * Sets the name of the JVM
     * @param jvm
     */
    public void setJVMName(String jvm) {
        this.jvmName = jvm;
    }

    /**
     * Gets the name of the JVM
     * @return the JVM name
     */
    public String getJVMName() {
        return this.jvmName;
    }

    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(BootstrapConstants.CS_GLOBAL);
    }

}
