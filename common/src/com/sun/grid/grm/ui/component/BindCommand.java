/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingException;

/**
 *
 * This command adds a single component 
 * configuration (its equivalent of jndi bind("component."+name, componentConfig) method)
 */
public class BindCommand<T> extends AbstractSystemCommand<Void> implements CSModifyCommand {
    /**
     * i18n Bundle name
     */
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(AddConfigurationCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;
    
    private String name;
    private T value;
    private boolean allowRebind;
    private transient RebindCommand rebindCommand;
    private boolean bound;
    
    /** Creates a new instance of AddConfigurationCommand
     * @param name   the name
     * @param value  the value
     */
    public BindCommand(String name, T value) {
        this.name = name;
        this.value = value;
    }
    
    public BindCommand() {
    }
    
    
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        Context ctx = env.getContext();
        if(name == null) {
            throw new GrmException("ui.command.nullnotallowed", BUNDLE_NAME, getClass().getName(), "name");
        }
        if(value == null) {
            throw new GrmException("ui.command.nullnotallowed", BUNDLE_NAME, getClass().getName(), "value");
        }
        try {
            try {
                ctx.bind(name, value);
                bound = true;
            } catch (NameAlreadyBoundException ex) {
                if (isAllowRebind()) {
                    ctx.rebind(name, value);
                } else {
                    throw ex;
                }
            }
        } catch (NameAlreadyBoundException ex) {
            throw new GrmException("ui.context.alreadybound", ex, BUNDLE_NAME, name);
        } catch (NamingException ex) {
            this.undo(env);
            throw new GrmException("ui.context.operationfailed", ex, BUNDLE_NAME, ex.getLocalizedMessage());
        } finally {
            try {
                ctx.close();
            } catch(NamingException ex) {
                // Ignore
            }
        }
        return CommandResult.VOID_RESULT;
    }

    @Override
    public void undo(ExecutionEnv env) {
        if(rebindCommand != null) {
           rebindCommand.undo(env);
           rebindCommand = null;
        } else if (bound) {
           try {
               bound = false;
               Context ctx = env.getContext();
               ctx.unbind(name);
               ctx.close();
           } catch(Exception ex) {
               if (log.isLoggable(Level.WARNING)) {
                   LogRecord lr = new LogRecord(Level.WARNING, "ui.command.undofailed");
                   lr.setThrown(ex);
                   log.log(lr);
               }
           }
        }
    }
    
    

    /**
     * Get the name of the value
     * @return the name of the value
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Get the name which should be bound
     * 
     * @return the value
     */
    public T getValue() {
        return this.value;
    }

    /**
     * Set the name
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set the value
     * @param value the value
     */
    public void setValue(T value) {
        this.value = value;
    }

    public boolean isAllowRebind() {
        return allowRebind;
    }

    public void setAllowRebind(boolean allowRebind) {
        this.allowRebind = allowRebind;
    }
    
    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(name);
    }
    
}
