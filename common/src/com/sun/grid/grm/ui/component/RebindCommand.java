/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.ui.*;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.ui.impl.FilterUtil;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

/**
 * Abstract base class for all bind commands
 * @param T type of the item which should be bound
 */
public class RebindCommand<T> extends AbstractSystemCommand<Void> implements CSModifyCommand {

    private final static long serialVersionUID = -2007112601L;
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private final static Logger log = Logger.getLogger(RebindCommand.class.getName(),BUNDLE_NAME);

    private final String name;
    private byte[] snapshot;
    private T value;
    
    /**
     * Create a new instance of AbstractRebindCommand 
     * @param name   the name if the item
     * @param value  the new value of the item
     */
    protected RebindCommand(String name, T value) {
        this.name = name;
        this.value = value;
    }
    
    @Override
    public void undo(ExecutionEnv env) {
        //if snapshot is null, we have nothing to UNDO
        if (snapshot == null) {
            return;
        }
        try {            
            //Retrieve save component configuration object
            T oldValue = FilterUtil.<T>getObjectSnapShot(snapshot);
            Context ctx = env.getContext();
            //Try to recreate it
            ctx.rebind(name, oldValue);
            snapshot = null;
            if (ctx != null) {
                ctx.close();
            }
        //Log error message if UNDO failed
        } catch (Exception ex) {
            if (log.isLoggable(Level.WARNING)) {
                LogRecord lr = new LogRecord(Level.WARNING, "ui.command.undofailed");
                lr.setThrown(ex);
                log.log(lr);
            }
        } 
    }
    
    
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        Context ctx = env.getContext();      
        try {
            @SuppressWarnings("unchecked")
            T oldObj = (T)ctx.lookup(name);            
            snapshot = FilterUtil.<T>makeObjectSnapShot(oldObj);
        } catch (NamingException ne) {
            if (ne instanceof NameNotFoundException) {
                //ignore
            } else {
                throw new GrmException("ui.context.operationfailed", ne, BUNDLE_NAME, name);
            }
        } 
        
        try {
            ctx.rebind(name, value);
        } catch(NamingException ex) {
            throw new GrmException("ui.context.operationfailed", ex, BUNDLE_NAME, name);
        } finally {
            try {
                ctx.close();
            } catch (NamingException ex) {
                // Ignore
            }
        }
        return CommandResult.VOID_RESULT;
    }
    
    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(name);
    }
}
