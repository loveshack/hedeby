/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.FilterVariable;
import com.sun.grid.grm.util.filter.VariableResolver;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

/**
 * This command gets a list of all active jvms
 */
public class GetActiveJVMListCommand extends AbstractSystemCommand<List<ActiveJvm>> {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.service.messages";
    private static final Logger log = Logger.getLogger(GetActiveJVMListCommand.class.getName());
    private static final long serialVersionUID = -2007110101L;
    
    private String jvmName = null;
    private String hostname = null;

    /**
     * Set the name of the searched jvms
     * @param aJvmName name of the search jvms or <code>null</code>
     */
    public void setJvmName(String aJvmName) {
        jvmName = aJvmName;
    }
    
    
    /**
     * Set the hostname of the searched jvms
     * @param aHostname the hostname or <code>null</code>
     */
    public void setHostname(String aHostname) {
        hostname = aHostname;
    }

    public Result<List<ActiveJvm>> execute(ExecutionEnv env) throws GrmException {
        Context ctx = env.getContext();
        List<ActiveJvm> list = new LinkedList<ActiveJvm>();
        
        AndFilter<ActiveJvm> filter = new AndFilter<ActiveJvm>(2);
        if(hostname != null) {
            filter.add(new CompareFilter<ActiveJvm>(ActiveJvmVariableResolver.HOST_VAR, 
                                                    new FilterConstant<ActiveJvm>(Hostname.getInstance(hostname)), 
                                                    CompareFilter.Operator.EQ)
                                                    );
        }
        if(jvmName != null) {
            filter.add(new CompareFilter<ActiveJvm>(ActiveJvmVariableResolver.NAME_VAR, 
                                                    new FilterConstant<ActiveJvm>(jvmName), 
                                                    CompareFilter.Operator.EQ)
                                                    );
        }
        try {
            NamingEnumeration<Binding> enumeration = ctx.listBindings(BootstrapConstants.CS_ACTIVE_JVM);
            if (ctx != null) {
                ctx.close();
            }
            if (enumeration != null) {
                ActiveJvmVariableResolver resolver = new ActiveJvmVariableResolver();
                
                while (enumeration.hasMore()) {
                    ActiveJvm ac = (ActiveJvm) enumeration.next().getObject();
                    if (jvmName != null || hostname != null) {
                        resolver.setActiveJvm(ac);
                        if (filter.matches(resolver)) {
                            list.add(ac);
                        }
                    } else {
                        list.add(ac);
                    }
                }
            } else {                   
                this.undo(env);
                GrmException grm = new GrmException("ui.componentservice.lookupexception", BUNDLE_NAME, GetActiveJVMListCommand.class, new String[]{jvmName, hostname});
                throw grm;
            }                
        } catch (NamingException ex) {
            this.undo(env);
            GrmException grm = new GrmException("ui.componentservice.lookupexception", ex, BUNDLE_NAME, GetActiveJVMListCommand.class, new String[]{jvmName, hostname});
            throw grm;
        }
        return new CommandResult<List<ActiveJvm>>(list);
    }
    
    private static class ActiveJvmVariableResolver implements VariableResolver<ActiveJvm> {

        public final static FilterVariable<ActiveJvm> HOST_VAR = new FilterVariable<ActiveJvm>("host");
        public final static FilterVariable<ActiveJvm> NAME_VAR = new FilterVariable<ActiveJvm>("name");
        
        private ActiveJvm activeJvm;
        
        public void setActiveJvm(ActiveJvm activeJvm) {
            this.activeJvm = activeJvm;
        }
        
        public Object getValue(String name) {
            if(HOST_VAR.getName().equals(name)) {
                return Hostname.getInstance(activeJvm.getHost());
            } else if (NAME_VAR.getName().equals(name)) {
                return activeJvm.getName();
            } else {
                return null;
            }
        }
    }
}
