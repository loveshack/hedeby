/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ParentStartupService;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.SkippedJVMException;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.ui.impl.FilterUtil;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Command that starts JVM on a localhost
 */
public class StartJVMCommand extends AbstractLocalCommand<List<JVMResultObject>> {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(StartJVMCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;

    private final String STARTED = I18NManager.formatMessage("StartJVMCommand.success", BUNDLE_NAME);
    private final String SKIPPED = I18NManager.formatMessage("StartJVMCommand.skipped", BUNDLE_NAME);
    private String jvmName;
    private int port;
    private boolean forced;
    private PreferencesType prefs;
    
    /**
     * Creates an instance of StartJVMCommand.
     * @param prefs system preferences type
     */
    public StartJVMCommand(PreferencesType prefs) throws GrmException {
        this.jvmName = null;
        this.port = BootstrapConstants.NO_DEBUG_PORT;
        this.forced = false;
        if (prefs == null) {
            throw new GrmException("ui.startjvmcommand.prefsnull", BUNDLE_NAME);
        }
        this.prefs = prefs;
    }

    public Result<List<JVMResultObject>> execute(ExecutionEnv env) throws GrmException {
        List<JVMResultObject> ret = new LinkedList<JVMResultObject>();
        Map<String, Throwable> res;
        try {
            if (FilterUtil.matchRelaxedString(jvmName, "")) {
                if (port == BootstrapConstants.NO_DEBUG_PORT) {
                    res = ParentStartupService.start(env, forced, prefs);
                } else {
                    throw new GrmException("ui.startjvmcommand.alldebug", BUNDLE_NAME);
                }
            } else {
                if (1024 < port && port < 65535 || port == BootstrapConstants.NO_DEBUG_PORT){
                    res = ParentStartupService.start(env, jvmName, port, forced, prefs);
                } else {
                    throw new GrmException("ui.startjvmcommand.wrongport", BUNDLE_NAME);
                }
            }
        } catch (InterruptedException ex) {
            this.undo(env);
            throw new GrmException("ui.startjvmcommand.interrupted", ex, BUNDLE_NAME, new Object[]{ jvmName });
        } catch (GrmException ex) {
            this.undo(env);            
            throw ex;
        }
        String host = Hostname.getLocalHost().getHostname();
        for (String jvm : res.keySet()) {
            Throwable tmp = res.get(jvm);
            if (tmp == null) {
                ret.add(new JVMResultObject(jvm, host, STARTED, null));
            } else {
                if (res.get(jvm) instanceof SkippedJVMException) {
                    ret.add(new JVMResultObject(jvm, host, SKIPPED, tmp.getLocalizedMessage()));
                } else {
                    ret.add(new JVMResultObject(jvm, host, tmp));
                }
            }
        }
        return new CommandResult<List<JVMResultObject>>(ret);
    }

    /**
     * Sets the JVM name
     * @param jvm name of the jvm
     */
    public void setJVMName(String jvm) {
        this.jvmName = jvm;
    }

    /**
     * Gets the JVM name
     * @return JVM name
     */
    public String getJVMName() {
        return this.jvmName;
    }
    
    /**
     * Sets the JVM debug port
     * @param port JVM debug port
     */
    public void setJVMPort(int port) {
        this.port = port;
    }

    /**
     * Gets the JVM debug port
     * @return JVM debug port
     */
    public int getJVMPort() {
        return this.port;
    }
    
    /**
     * Sets if the JVM will run in force mode
     * @param forced if set to true will run the JVM in force mode
     * it means that it will run it despite of pid files in run dir.
     */
    public void setForcedStartup(boolean forced) {
        this.forced = forced;
    }

    /**
     * Gets the info if the JVM will run in forced mode
     * @return true if JVM will run in forced mode
     */
    public boolean getForcedStartup() {
        return this.forced;
    }
}
