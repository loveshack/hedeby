/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.config.common.AbstractServiceConfig;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.ui.impl.FilterUtil;
import com.sun.grid.grm.util.Hostname;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is command for removing the services from system, both services that are down
 * and services that are running, are allowed to be removed. Although to remove running
 * service the special flags should be set:
 * forced - if component is started this flag has to be set otherwise the component can not 
 *          be removed
 * NOTE: service name has to be different from null
 */   
public class RemoveServiceCommand extends AbstractSystemCommand<ServiceResultObject> implements CSModifyCommand{
    private String name = null;

    private boolean forced = false;
    private final Set<Component> started = new HashSet<Component>(); 
    private final Set<Component> added = new HashSet<Component>();
    private final List<RemoveComponentWithConfigurationCommand> removed = new LinkedList<RemoveComponentWithConfigurationCommand>();
    List<JvmConfig> jvms;
     private static final long serialVersionUID = -2007080101L;
     private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(RemoveServiceCommand.class.getName());
    private static final String REMOVED="REMOVED";
    /** Creates a new instance of RemoveServiceCommand */
    public RemoveServiceCommand() {
    }

    public Result<ServiceResultObject> execute(ExecutionEnv env) throws GrmException {
        Service service = null;
        String host = null;
        ServiceState serviceState = null;
        ComponentState componentState = null;
        ServiceResultObject ret = null;
        //we have to clear this one because someone can call execute on the same instance of command
        //if this variables will not be cleared the Undo will not work properly
        jvms = null;
        started.clear();
        added.clear();
        removed.clear();
        
        if (name == null) {
            throw new GrmException("ui.removeservicecommand.error.argumentsnotset", BUNDLE_NAME);
        }
        
        Component tmpComp = null;
        try {
            GetJVMConfigurationsCommand jvmsCmd = new GetJVMConfigurationsCommand();
            jvms = jvmsCmd.execute(env).getReturnValue();
            //filter jvms so only components known as the singletons with matching name and hostname will stay
            JVM jvmComp = null;
            
            for (JvmConfig jvm : jvms) {
                jvmComp = null;
                for(Singleton c: GetComponentCommand.getSingletons(jvm, name, null)) {
                    // try to obtain the service if was started
                    service = ComponentService.<Service>getComponentByNameAndType(env, c.getName(), Service.class);
                    // service was obtained
                    if (service == null) {
                        //not running but can be in comp
                        GetConfigurationCommand<ComponentConfig> confCmd = new GetConfigurationCommand<ComponentConfig>();
                        confCmd.setConfigName(c.getName());
                        ComponentConfig config = confCmd.execute(env).getReturnValue();
                        if (config instanceof AbstractServiceConfig) {
                            // this component is a Service
                            tmpComp = c;
                            break;
                        }
                        continue;
                    }
                    //add component to removal list
                    tmpComp = c;
                    //Check if component state is proper
                    componentState = service.getState();
                    if (componentState.equals(ComponentState.RELOADING) ||
                            componentState.equals(ComponentState.STARTING) ||
                            componentState.equals(ComponentState.STOPPING) ||
                            (componentState.equals(ComponentState.STARTED) && !isForced())) {
                        throw new GrmException("ui.illegal_component_state", BUNDLE_NAME, componentState);
                    }
                    serviceState = service.getServiceState();
                    //service was registered, we need to check the state
                    if (serviceState.equals(ServiceState.STOPPED) ||
                            serviceState.equals(ServiceState.ERROR) ||
                            serviceState.equals(ServiceState.UNKNOWN)) {
                        // with these service states we can  uninstall
                        Hostname hostname = Hostname.getInstance(c.getHost());
                        jvmComp = ComponentService.getComponentByNameTypeAndHost(env, jvm.getName(), JVM.class, hostname);
                        if (jvmComp != null) {
                            //check whether component was started if yes undo will try to start it on any problems
                            if (service.getState().equals(ComponentState.STARTED)) {
                                started.add(c);
                            }
                            jvmComp.removeComponent(c.getName());
                            //save the name of the component that was registered in Jvm
                            added.add(c);
                        } else {//should not occur because service is running so jvm should be also running
                            throw new GrmException("ui.removeservicecommand.couldntgetjvm", BUNDLE_NAME, name, jvm.getName());
                        }
                    } else {
                        // we dont allow to remove service that is in transition state
                        throw new GrmException("ui.illegal_service_state", BUNDLE_NAME, serviceState);
                    }                                              
                }
                if (tmpComp != null) {
                    break;
                }
            }
            /** Here tmpComps should contain those components that are to be removed
             * and they are not running
             */
            if (tmpComp == null) {
               throw new GrmException("ui.removeservicecommand", BUNDLE_NAME, getName()); 
            }
            
            
            RemoveComponentWithConfigurationCommand removeCmd = new RemoveComponentWithConfigurationCommand(tmpComp.getName(), host);
            removeCmd.execute(env);
            //save command for possible undo
            removed.add(removeCmd);
            ret = new ServiceResultObject(tmpComp.getName(), ((Singleton)tmpComp).getHost(), REMOVED, null);

        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }
        
        CommandResult<ServiceResultObject> res = new CommandResult<ServiceResultObject>();
        res.setReturnValue(ret);
        return res;
    }
    @Override
    public void undo(ExecutionEnv env) {
        try {
            
            for (int i=removed.size()-1;i>=0;i--) {
                removed.get(i).undo(env);
            }
            removed.clear();
            //Here the configurations are restored
            
            //Get the current jvmConfigs

            GetJVMConfigurationsCommand jvmsCmd = new GetJVMConfigurationsCommand();
            jvms = jvmsCmd.execute(env).getReturnValue();
            JVM jvmComp = null;
            String jvm = null;
            for (Component registeredComp : added) {                
                jvm = FilterUtil.getJvmName(jvms,registeredComp.getName());               
                Hostname hostname = Hostname.getInstance(((Singleton)registeredComp).getHost());
                jvmComp = ComponentService.getComponentByNameTypeAndHost(env, jvm, JVM.class, hostname);                   
                jvmComp.addComponent(registeredComp);
                if (started.contains(registeredComp) && (!(registeredComp.isAutostart()))) {
                    Service comp = ComponentService.getComponentByNameAndType(env, registeredComp.getName(), Service.class);
                    comp.start();
                }           
            }
        } catch (GrmException ex) {
            log.logrb(Level.WARNING, RemoveServiceCommand.class.getName(), "undo", BUNDLE_NAME, "ui.command.undofailed", ex);
        }             
    }
    /** 
     * Getter for the name of service
     * @return name of service to removed
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for name of service
     * @param name of service to be removed, either name or host has to be different
     * from <code>null</code>
     */
    public void setName(String name) {
        this.name = name;
    }

    /** 
     * Getter for the force flag
     * @return true, if the started component should be removed
     */
    public boolean isForced() {
        return forced;
    }
    /** 
     * Setter for the force flag
     * @param forced - set true, if the started component should be removed, otherwise 
     * started components are not allowed to be removed
     */
    public void setForced(boolean forced) {
        this.forced = forced;
    }

    
    public List<String> getListOfModifyNames() {
        List<String> ret = new LinkedList<String>();
        ret.add(BootstrapConstants.CS_GLOBAL);
        ret.add(BootstrapConstants.CS_COMPONENT+"."+getName());
        return ret;
    }
}
