/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.MultiComponent;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.ui.impl.FilterUtil;
import com.sun.grid.grm.util.Hostname;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Command that starts either component on a specified host/all hosts
 * or all components on a specified host/all hosts.
 * This command is for starting components that are not registered in JVM.
 * Command should be used for New Components like for example just added to system.
 */
public class InstantiateComponentCommand extends AbstractSystemCommand<Void> {
    
    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(InstantiateComponentCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;
    
    private String componentName;
    private String hostName;
    private String jvmName;
 
    private final List<JvmComponent> started = new LinkedList<JvmComponent>();
    
    /**
     * Creates an instance of InstantiateComponentCommand
     */

    public InstantiateComponentCommand() {
        this.setComponentName(null);
        this.setHostName(null);
        this.setJvmName(null);
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        String tmpJvmName = null;
        try {
            started.clear();
            GetComponentCommand componentCmd = new GetComponentCommand();
            componentCmd.setJVMName(jvmName);
            componentCmd.setComponentName(componentName);
            componentCmd.setHostname(hostName);
            List<Component> compos = componentCmd.execute(env).getReturnValue();
            
            GetJVMConfigurationsCommand jvmCmd = new GetJVMConfigurationsCommand();
            List<JvmConfig> jvms = jvmCmd.execute(env).getReturnValue();
            
            for (Component comp : compos) {            
                if(comp instanceof MultiComponent) {
                    for (String host : ((MultiComponent)comp).getHosts().getInclude()) {
                        Hostname hostname = Hostname.getInstance(host);
                        tmpJvmName = FilterUtil.getJvmName(jvms, comp.getName());
                        JVM jvmComp = ComponentService.<JVM>getComponentByNameTypeAndHost(env, tmpJvmName, JVM.class, hostname);
                        jvmComp.addComponent(comp);
                        started.add(new JvmComponent(comp.getName(),jvmComp));
                    }
                } else if (comp instanceof Singleton) {
                    Hostname hostname = Hostname.getInstance(((Singleton)comp).getHost());
                    tmpJvmName = FilterUtil.getJvmName(jvms, comp.getName());
                    JVM jvmComp = ComponentService.<JVM>getComponentByNameTypeAndHost(env, tmpJvmName, JVM.class, hostname);
                    if (jvmComp == null) {
                        throw new GrmException("InstantiateComponentCommand.start.failed", BUNDLE_NAME, tmpJvmName);
                    }
                    jvmComp.addComponent(comp);
                    started.add(new JvmComponent(comp.getName(),jvmComp));
                }
            }
        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }        
        return CommandResult.VOID_RESULT;
    }

    @Override
    public void undo(ExecutionEnv env) {
       for(JvmComponent elem : started) {
           try {
               elem.getJvm().removeComponent(elem.getName());
           } catch (GrmRemoteException ex) {
               LogRecord lr = new LogRecord(Level.WARNING, "InstantiateComponentCommand.undo");
               lr.setThrown(ex);
               lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE_NAME));
               lr.setParameters(new Object[]{elem.name, this.jvmName, ex.getLocalizedMessage()});
               log.log(lr);
           }
       }
    }
    /**
     * Getter for componentName
     * @return name of component
     */
    public String getComponentName() {
        return componentName;
    }

    /**
     * Setter for componentName
     * @param componentName
     */
    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    /**
     * Getter for hostName
     * @return host of component
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Setter for hostName
     * @param hostName
     */
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    /**
     * Getter for jvmName
     * @return name of jvm
     */
    public String getJvmName() {
        return jvmName;
    }

    /**
     * Setter for jvmName
     * @param jvmName
     */
    public void setJvmName(String jvmName) {
        this.jvmName = jvmName;
    }
    
    /**
     * Helper class for storing a pair of JVM and componentName for undo purposes
     */
    private class JvmComponent {
        private String name;
        private JVM jvm;
        public JvmComponent(String name, JVM jvm) {
            this.name = name;
            this.jvm = jvm;
        }       
        public String getName() {
            return name;
        }       
        public JVM getJvm() {
            return jvm;
        }       
    }
}
