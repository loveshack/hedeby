/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JvmInfo;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

/**
 * Command that adds new active jvm to CS
 *
 * TODO implement undo method
 */
public class StoreActiveJVMCommand extends AbstractSystemCommand<Void> implements CSModifyCommand{

    private static final long serialVersionUID = -2007112701L;
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private String name;
    private ActiveJvm value;
    private String JvmName;

    /**
     * Creates an instance of StoreActiveJVMCommand.
     * @param aJvm the active component info
     */
    public StoreActiveJVMCommand(ActiveJvm aJvm) {
        this.name = createName(aJvm);
        this.value = aJvm;
        this.JvmName = aJvm.getName();
    }
    
    
    private static String createName(ActiveJvm aJvm) {
        JvmInfo ji = new JvmInfo(aJvm.getName(), Hostname.getInstance(aJvm.getHost()));
        return String.format("%s.%s", BootstrapConstants.CS_ACTIVE_JVM, ji.getIdentifier());
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        
        
        Context ctx = env.getContext();
        try {
            NamingEnumeration<Binding> enumeration = ctx.listBindings(BootstrapConstants.CS_ACTIVE_COMPONENT);
            List<String> list = new ArrayList<String>(); 
            while(enumeration.hasMore()) {
                Binding binding = enumeration.next();
                ActiveComponent ac = (ActiveComponent)binding.getObject();
                if (ac.getJvm().toLowerCase().equals(JvmName.toLowerCase())
                        && ac.getHost().toLowerCase().equals(value.getHost().toLowerCase())) {
                    list.add(binding.getName());
                }
            }
            Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                ctx.unbind(String.format("%s.%s", BootstrapConstants.CS_ACTIVE_COMPONENT, iterator.next()));
            }
        } catch (NamingException ne) {
            //ignore
        } 
            
        try {
            ctx.rebind(name, value);
        } catch (NamingException ne) {
            if (ne instanceof NameNotFoundException) {
                //ignore
            } else {
                throw new GrmException("ui.context.operationfailed", ne, BUNDLE_NAME, ne.getLocalizedMessage());
            }
        } finally {
            try {
                ctx.close();
            } catch(NamingException ex) {
                // Ignore
            }
        }
        return CommandResult.VOID_RESULT;
    }

    public List<String> getListOfModifyNames() {
        List<String> ret = new LinkedList<String>();
        ret.add(name);
        ret.add(BootstrapConstants.CS_ACTIVE_COMPONENT+".*");
        return ret;
    }
}
