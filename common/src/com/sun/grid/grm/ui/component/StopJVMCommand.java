/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.service.GetActiveJVMListCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.ui.impl.FilterUtil;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Command that stops JVM on a specified host
 */
public class StopJVMCommand extends AbstractLocalCommand<List<JVMResultObject>> {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(StopJVMCommand.class.getName(), BUNDLE_NAME);
    private static final long serialVersionUID = -2007073101L;
    private String jvmName;
    private String hostName;
    private final String STOPPED = I18NManager.formatMessage("StopJVMCommand.success", BUNDLE_NAME);

    /**
     * Creates an instance of StopJVMCommand.
     */
    public StopJVMCommand() {
        this.jvmName = null;
        this.hostName = null;
    }

    public Result<List<JVMResultObject>> execute(ExecutionEnv env) throws GrmException {

        List<JVMStopper> jvmStoppers = new LinkedList<JVMStopper>();
        JVMStopper csJvmStopper = null;
        List<JVMResultObject> ret = new LinkedList<JVMResultObject>();

        if (isSetLocalhost()) {
            // We stop only jvms on the local host
            Iterator<File> iterator = PathUtil.getPidFiles(env).iterator();
            while (iterator.hasNext()) {
                File pidFile = iterator.next();
                String pidJVMName = pidFile.getName().split("@")[0];
                if (FilterUtil.matchRelaxedString(jvmName, pidJVMName)) {
                    try {
                        JVMStopper jvmStopper;
                        jvmStopper = new JVMStopper(env, pidFile);
                        if (jvmStopper.isCs()) {
                            csJvmStopper = jvmStopper;
                        } else {
                            jvmStoppers.add(jvmStopper);
                        }
                    } catch (GrmException ex) {
                        ret.add(new JVMResultObject(jvmName, Hostname.getLocalHost().getHostname(),ex));
                    }
                }
            }
        } else {
            GetActiveJVMListCommand cmd = new GetActiveJVMListCommand();
            cmd.setJvmName(jvmName);
            cmd.setHostname(hostName);
            for(ActiveJvm jvm: env.getCommandService().<List<ActiveJvm>>execute(cmd).getReturnValue()) {
                JVMStopper jvmStopper = new JVMStopper(env, jvm);
                if (jvmStopper.isCs()) {
                    csJvmStopper = jvmStopper;
                } else {
                    jvmStoppers.add(jvmStopper);
                }
            }
        }

        for (JVMStopper jvmStopper : jvmStoppers) {
            try {
                String tmp = jvmStopper.stop();
                ret.add(new JVMResultObject(jvmStopper.getJvmName(), jvmStopper.getHost(), STOPPED, tmp));
            } catch (GrmException ex) {
                ret.add(new JVMResultObject(jvmStopper.getJvmName(), jvmStopper.getHost(), ex));
            }           
        }

        if (csJvmStopper != null) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
                log.logrb(Level.FINEST, StopJVMCommand.class.getName(), "execute", BUNDLE_NAME, "ui.stopjvmcommand.interrupted", new Object[]{});
            }
            try {   
                String tmp = csJvmStopper.stop();
                ret.add(new JVMResultObject(csJvmStopper.getJvmName(), csJvmStopper.getHost(), STOPPED, tmp));
            } catch (GrmException ex) {
                ret.add(new JVMResultObject(csJvmStopper.getJvmName(), csJvmStopper.getHost(), ex));
            }
        }
        
        if (ret.isEmpty()) {
            throw new GrmException("ui.stopjvmconf.jvm.notfound", BUNDLE_NAME);
                 
        } 

        return new CommandResult<List<JVMResultObject>>(ret);
    }

    private static class JVMStopper {

        private final boolean local;
        private final String jvmName;
        private final String host;
        private final int pid;
        private JVM proxy;
        private final boolean isCs;

        /**
         * Create a new instanceof JVMStopper
         * @param env  the execution env
         * @param jvm   the comonent info of the jvm
         */
        public JVMStopper(ExecutionEnv env, ActiveJvm jvm) {
            local = false;
            jvmName = jvm.getName();
            host = jvm.getHost();
            pid = 0;
            ComponentInfo ci = ComponentInfo.newInstance(env, Hostname.getInstance(jvm.getHost()), jvm.getName(), jvm.getName(), JVM.class);
            try {
                proxy = ComponentService.<JVM>getComponent(env, ci);
            } catch (GrmException ex) {
                //if error proxy is null
                proxy = null;
            }
            isCs = SystemUtil.isCSJvmHostAndPort(env, ci.getHostname(), jvm.getPort());
        }

        /**
         * Create an instanceof of JVMStopper.
         *
         * The information is read from the pid file of the jvm
         *
         * @param env the execution env
         * @param pidFile the pid file
         * @throws GrmException 
         */
        public JVMStopper(ExecutionEnv env, File pidFile) throws GrmException {
            FileReader fr = null;
            int port = 0;
            try {
                jvmName = pidFile.getName().split("@")[0];
                host = Hostname.getLocalHost().getHostname();
                fr = new FileReader(pidFile);
                LineNumberReader lnr = new LineNumberReader(fr);
                // throw away pid number, then read host
                pid = Integer.parseInt(lnr.readLine());
                port = Integer.parseInt(lnr.readLine());
                ComponentInfo ci = ComponentInfo.newInstance(env, Hostname.getLocalHost(), getJvmName(), getJvmName(), JVM.class);
                proxy = ComponentService.<JVM>getComponent(env, ci);
                local = true;               
                isCs = SystemUtil.isCSJvmHostAndPort(env, ci.getHostname(), port);
            } catch (NumberFormatException ex) {
                throw new GrmException("ui.stopjvmcommand.pidreaderror", ex, BUNDLE_NAME, new Object[]{pidFile.getAbsoluteFile()});
            } catch (FileNotFoundException ex) {
                throw new GrmException("ui.stopjvmcommand.pidnotfound", ex, BUNDLE_NAME, new Object[]{pidFile.getAbsoluteFile()});
            } catch (IOException ex) {
                throw new GrmException("ui.stopjvmcommand.pidreaderror", ex, BUNDLE_NAME, new Object[]{pidFile.getAbsoluteFile()});
            } finally {
                try {
                    if (fr != null) {
                        fr.close();
                    }
                } catch (IOException ex) {
                    log.log(Level.FINEST, "ui.stopjvmcommand.pidcloseerror", pidFile.getAbsoluteFile());
                }
            }
        }

        /**
         *   Stop the JVM.
         *
         *   This method tries to call the shutdown method of the jvm. If it
         *   fails and the jvm is a local jvm then the process is killed.
         *
         *   @return true of the stop trigger has be send to the jvm
         */
        public String stop() throws GrmException {
            try {
                //if proxy was not successfully initialiazed in constructor, exception will be caught here anyway
                proxy.shutdown();
                return null;
            } catch (Exception ex) {
                if (local) {
                    
                    if(Platform.getPlatform().terminateProcess(pid)) {
                        return I18NManager.formatMessage("StopJVMCommand.JVMStopper.processKilled", BUNDLE_NAME);
                    }
                    throw new GrmException("ui.stopjvmcommand.kill_failed", BUNDLE_NAME, getJvmName());
                } else {
                    
                    //throw GrmException 
                    throw new GrmException("ui.stopjvmcommand.stop_error", BUNDLE_NAME, getJvmName());
                }
            }
        }

        /**
         *  Determine of the JVMStopper stops the CS jvm.
         *
         *  @return <code>true</code> if the JVMStopper stops the CS jvm
         */
        public boolean isCs() {
            return isCs;
        }

        /**
         *  Get the name of the jvm.
         *
         *  @return the name of the jvm
         */
        public String getJvmName() {
            return jvmName;
        }

        public String getHost() {
            return host;
        }
    }

    /**
     * Sets the JVM name
     * @param jvm
     */
    public void setJVMName(String jvm) {
        this.jvmName = jvm;
    }

    /**
     * Gets the JVM name
     * @return the JVM name
     */
    public String getJVMName() {
        return this.jvmName;
    }

    /**
     * Sets the hostname
     * @param host
     */
    public void setHostname(String host) {
        this.hostName = host;
    }

    /**
     * Gets the hostname
     * @return the hostname
     */
    public String getHostname() {
        return this.hostName;
    }

    private boolean isSetLocalhost() {
        // empty hostname is wildcard
        if (hostName == null || FilterUtil.matchRelaxedString(hostName, "")) {
            return false;
        }
        Hostname target = Hostname.getInstance(hostName);
        return Hostname.getLocalHost().equals(target);
    }
}
