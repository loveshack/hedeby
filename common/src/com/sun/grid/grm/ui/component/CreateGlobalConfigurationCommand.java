/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;


/**
 * Command that creates internal directory structure on master host
 */
public class CreateGlobalConfigurationCommand extends AbstractSystemCommand<Void> implements CSModifyCommand{

    /**
     * i18n Bundle name
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(CreateGlobalConfigurationCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = -2007080101L;

    private final String adminUser;
    private final int csPort;
    private final boolean simple;
    
    private transient BindCommand<GlobalConfig> executedCommand;

    /**
     * Creates an instance of CreateGlobalConfigurationCommand.
     * @param adminUser name of the admin user
     * @param csPort port used for starting configuration service
     * @param simple if <code>true</code> create an all-in-one-jvm configuration
     */
    public CreateGlobalConfigurationCommand(String adminUser, int csPort, boolean simple) {
        this.adminUser = adminUser;
        this.csPort = csPort;
        this.simple = simple;
    }
    
    /**
     * Execute the operation
     * @param env Used ExecutionEnv for the operation
     * @return Result<Void>
     * @throws com.sun.grid.grm.GrmException On errors
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setName(env.getSystemName());
        
        JvmConfig cs_vm = new JvmConfig();
        cs_vm.setName(BootstrapConstants.CS_JVM);
        cs_vm.setPort(csPort);
        if (Platform.getPlatform().isSuperUser() && simple) {
                // TODO: will not work on Windows arch
                cs_vm.setUser("root");
            } else {
                cs_vm.setUser(adminUser);
            }
        cs_vm.getJvmArg().add(SystemUtil.getMemoryVariable(BootstrapConstants.CS_JVM));
        // Default connection timeout for a SDM jvm is one minute
        // Users can modify the connection timeout in the global configuration
        String connectionTimeout = "-Dcom.sun.grid.grm.management.connectionTimeout=60";
        
        cs_vm.getJvmArg().add(connectionTimeout);
        globalConfig.getJvm().add(cs_vm);
        
        if (!simple) {
            JvmConfig executor_vm = new JvmConfig();
            if (Platform.getPlatform().isSuperUser()) {
                // TODO: will not work on Windows arch
                executor_vm.setUser("root");
            } else {
                executor_vm.setUser(adminUser);
            }
            executor_vm.setPort(0);
            executor_vm.setName(BootstrapConstants.EXECUTOR_VM);
            executor_vm.getJvmArg().add(SystemUtil.getMemoryVariable(BootstrapConstants.EXECUTOR_VM));
            executor_vm.getJvmArg().add(connectionTimeout);
            globalConfig.getJvm().add(executor_vm);

            JvmConfig rp_vm = new JvmConfig();
            rp_vm.setName(BootstrapConstants.RP_VM);
            rp_vm.setPort(0);
            rp_vm.setUser(adminUser);
            rp_vm.getJvmArg().add(SystemUtil.getMemoryVariable(BootstrapConstants.RP_VM));
            rp_vm.getJvmArg().add(connectionTimeout);
            globalConfig.getJvm().add(rp_vm);
        }
        
        BindCommand<GlobalConfig> cmd = new BindCommand<GlobalConfig>("global", globalConfig);
        cmd.execute(env);
        executedCommand = cmd;
        return CommandResult.VOID_RESULT;
    }

    @Override
    public void undo(ExecutionEnv env) {
        if(executedCommand != null) {
            executedCommand.undo(env);
            executedCommand = null;
        }
    }
    
    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(BootstrapConstants.CS_GLOBAL);
    }    
}
