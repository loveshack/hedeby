/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.naming.ConfigurationObjectNotFoundException;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

/**
 *
 * This command gets a single component configuration
 * (its equivalent of jndi lookup("component."+name) method)
 * 
 * @param <C> type of a configuration object
 */
public class GetConfigurationCommand<C> extends AbstractSystemCommand<C>{
            
    /**
     * i18n Bundle name
     */
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(GetConfigurationCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;
    
    private String configName;
    /** Creates a new instance of GetConfigurationCommand */
    public GetConfigurationCommand() {
        this.configName = null;
    }
    
    /** Creates a new instance of GetConfigurationCommand
     * @param config configuration name
     */
    public GetConfigurationCommand(String config) {
        this.configName = config;
    }

    public Result<C> execute(ExecutionEnv env) throws ConfigurationObjectNotFoundException, GrmException {
        Context ctx = env.getContext();
        Result<C> r = new CommandResult<C>();
        
        try { 
            @SuppressWarnings(value = "unchecked")
            C config = (C) ctx.lookup(BootstrapConstants.CS_COMPONENT + "." + configName);
            if (config != null) {
                r.setReturnValue(config);
                return r;
            } else {
                this.undo(env);
                ConfigurationObjectNotFoundException grm = new ConfigurationObjectNotFoundException("ui.context.objectnotfound", BUNDLE_NAME, new Object[]{"ComponentConfig", configName});
                throw grm;
            }
        } catch (NameNotFoundException ex) {
            this.undo(env);
            ConfigurationObjectNotFoundException grm = new ConfigurationObjectNotFoundException("ui.context.objectnotfound", ex, BUNDLE_NAME, new Object[]{"ComponentConfig", configName});
            throw grm;
        } catch (NamingException ex) {
            this.undo(env);
            GrmException grm = new GrmException("ui.context.operationfailed", ex, BUNDLE_NAME, ex.getLocalizedMessage());
            throw grm;
        }
    }

    public String getConfigName() {
        return this.configName;
    }
    
    public void setConfigName(String configName) {
        this.configName = configName;
    }
    
}
