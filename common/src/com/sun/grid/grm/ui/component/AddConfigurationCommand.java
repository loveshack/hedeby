/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.config.common.ComponentConfig;
import com.sun.grid.grm.ui.CSModifyCommand;
import java.util.Collections;
import java.util.List;

/**
 *
 * This command adds a single component 
 * configuration (its equivalent of jndi bind("component."+name, componentConfig) method)
 */
public class AddConfigurationCommand extends BindCommand<ComponentConfig> implements CSModifyCommand{

    private static final long serialVersionUID = -2007080101L;
    
    /** Creates a new instance of AddConfigurationCommand
     * @param name name of the component
     * @param config the configuration of the component
     * 
     */
    public AddConfigurationCommand(String name, ComponentConfig config) {
        super("component." + name, config);
    }
    
    /**
     * Default constructor
     * caller responsible for setting name and value
     */
    public AddConfigurationCommand() {
    }

    @Override
    public void setName(String name) {
        super.setName("component." + name);
    }

    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(BootstrapConstants.CS_COMPONENT+"."+getName());
    }

    
}
