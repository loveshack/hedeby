/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.config.common.MultiComponent;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.ui.impl.FilterUtil;
import com.sun.grid.grm.util.Hostname;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Command that retrieves all Components that match provided combination
 * of JvmName, ComponentName and Hostname
 */
public class GetComponentCommand extends AbstractSystemCommand<List<Component>> {

    /**
     * i18n Bundle name
     */
    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(GetComponentCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;

    private String jvmName;
    private String componentName;
    private String hostname;

    /**
     * Creates an instance of GetComponentCommand.
     */
    public GetComponentCommand() {
        this.jvmName = null;
        this.componentName = null;
        this.hostname = null;
    }   

    public Result<List<Component>> execute(ExecutionEnv env) throws GrmException {

        Result<List<Component>> r = new CommandResult<List<Component>>();
        
        GetGlobalConfigurationCommand cmd = new GetGlobalConfigurationCommand();
        GlobalConfig global = cmd.execute(env).getReturnValue();

        Hostname hostnameObj = null;
        if(hostname != null) {
            hostnameObj = Hostname.getInstance(hostname);
        }
        
        Set<Component> comp_set = getComponents(global, jvmName, componentName, hostnameObj);
        
        r.setReturnValue(new ArrayList<Component>(comp_set));

        return r;
    }

    /**
     * Get all matching components from a global config object
     * @param conf
     * @param jvmName
     * @param componentName
     * @param hostname
     * @return set of matching component
     */
    public static Set<Component> getComponents(GlobalConfig conf, String jvmName, String componentName, Hostname hostname) {
        Set<Component> ret = new HashSet<Component>();
        for (JvmConfig jvmc : conf.getJvm()) {
            if (FilterUtil.matchRelaxedString(jvmName, jvmc.getName())) {
                ret.addAll(getComponents(jvmc, componentName, hostname));
            }
        }
        return ret;
    }
    /** 
     * Search in given JvmConfig for components with given componentName and hostName
     *
     * @param jvm - jvmConfig 
     * @param componentName - name of component that is looked for
     * @param hostname - name of host on which component should run
     * @return  list with components that are meeting the requirements specified in arguments
     */
    public static Set<Component> getComponents(JvmConfig jvm, String componentName, Hostname hostname) {
        if(jvm == null) {
            return Collections.<Component>emptySet();
        }
        Set<Component> ret = new HashSet<Component>();       
        
        for (Component comp : jvm.getComponent()) {
            if (FilterUtil.matchRelaxedString(componentName, comp.getName())) {
                if(comp instanceof MultiComponent) {
                    for (String host : ((MultiComponent)comp).getHosts().getInclude()) {
                        if(hostname == null || hostname.matches(host)) {
                            ret.add(comp);
                        }
                    }
                } else if (comp instanceof Singleton) {
                    Hostname that = Hostname.getInstance(((Singleton)comp).getHost());
                    if(hostname == null || hostname.equals(that)) {
                        ret.add(comp);
                    }
                }
            }
        }
        return ret;
    }
    
    /**
     * Get a set of singletons
     * @param jvm  the jvm name
     * @param componentName the component name 
     * @param hostname the hostname
     * @return set of singletons
     */
    public static Set<Singleton> getSingletons(JvmConfig jvm, String componentName, Hostname hostname) {
        if(jvm == null) {
            return Collections.<Singleton>emptySet();
        }
        Set<Singleton> ret = new HashSet<Singleton>();       
        
        for (Component comp : jvm.getComponent()) {
            if (FilterUtil.matchRelaxedString(componentName, comp.getName())) {
                if (comp instanceof Singleton) {
                    if(hostname == null || hostname.equals(((Singleton)comp).getHost())) {
                        ret.add((Singleton)comp);
                    }
                }
            }
        }
        return ret;
    }
    

    /**
     * Sets the name of the JVM
     * @param jvm
     */
    public void setJVMName(String jvm) {
        this.jvmName = jvm;
    }

    /**
     * Gets the name of the JVM
     * @return the JVM name
     */
    public String getJVMName() {
        return this.jvmName;
    }

    /**
     * Sets the hostname
     * @param host
     */
    public void setHostname(String host) {
        this.hostname = host;
    }

    /**
     * Gets the hostname
     * @return the hostname
     */
    public String getHostname() {
        return this.hostname;
    }

    /**
     * Sets the Component name
     * @param component
     */
    public void setComponentName(String component) {
        this.componentName = component;
    }

    /**
     * Gets the Component name
     * @return the component name
     */
    public String getComponentName() {
        return this.componentName;
    }
    
}
