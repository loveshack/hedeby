/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;


import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.logging.Logger;
/**
 * This command starts the service itsself by calling startService() method from
 * Service interface. To start the service the component with the service has to be running.
 */
public class StartServiceCommand extends AbstractLocalCommand<ServiceResultObject> {
    
    private final static long serialVersionUID = -2007112801L;
    private final String name;
    
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(StartServiceCommand.class.getName());
    private static final String STARTED = "STARTED";
    /** Creates a new instance of StartServiceCommand
     * @param name the name of the service
     */
    public StartServiceCommand(String name) {
        this.name=name;
    }

    public Result<ServiceResultObject> execute(ExecutionEnv env) throws GrmException {
        
        Service service = null;
        ServiceResultObject ret = null;
        service  = ComponentService.getComponentByNameAndType(env, getName(), Service.class);
         if (service == null) {
            GrmException ex =  new GrmException("ui.component_not_found", BUNDLE_NAME, name);
            ret = new ServiceResultObject(getName(), null, ex);
        }
        if (ret == null) {
            try {           
                service.startService();
            } catch (GrmException ex) {
                ret = new ServiceResultObject(service.getName(), null, ex);
            }
            if (ret == null) {
                ret = new ServiceResultObject(service.getName(), null, STARTED, null);
            }
        }
                
        CommandResult<ServiceResultObject> r = new CommandResult<ServiceResultObject>();
        r.setReturnValue(ret);
        return r;
    }
    /**
     * Getter for service name to be started
     * @return service name
     */
    public String getName() {
        return name;
    }
    
}
