/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.service.Need;
import java.io.Serializable;

/**
 * Helper class for the result of the GetServiceNeedsCommand
 */
public class ServiceNeed implements Serializable {

    private final static long serialVersionUID = -2007121701L;
    private final String serviceName;
    private final String sloName;
    private final Need   need;
    private final GrmException error;
    
    /**
     * Create a new instance of ServiceNeed
     * @param serviceName   name of the service
     * @param sloName       name of the slo
     * @param need          the need produced by the slo
     */
    public ServiceNeed(String serviceName, String sloName, Need need) {
        this.serviceName = serviceName;
        this.sloName = sloName;
        this.need = need;
        error = null;
    }
    
    /**
     * Create a new instance of ServiceNeed
     * @param serviceName name of the service
*/
    public ServiceNeed(String serviceName, GrmException error) {
        this.serviceName = serviceName;
        this.sloName = null;
        this.need = null;
        this.error = error;
    }

    /**
     * Get the service name
     * @return the service name
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Get the slo name
     * @return the slo name
     */
    public String getSloName() {
        return sloName;
    }

    /**
     * Get the need
     * @return the need
     */
    public Need getNeed() {
        return need;
    }

    /**
     * Get the error 
     * @return the error
     */
    public GrmException getError() {
        return error;
    }
    
    /**
     * Is the error set
     * @return <code>true</code> if the error is set
     */
    public boolean hasError() {
        return error != null;
    }

}
