/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterException;
import com.sun.grid.grm.util.filter.FilterVariable;
import com.sun.grid.grm.util.filter.RegExpFilter;
import java.util.List;
import java.util.logging.Logger;

/**
 * Abstract base class for commands which calculates static values out of
 * the states of service SLOs.
 * 
 * @param T The type of the result of the command
 */
public abstract class AbstractQuerySLOsCommand<T> extends AbstractLocalCommand<T> {
    
    private final static String BUNDLE = "com.sun.grid.grm.ui.component.service.messages";
    private final static Logger log = Logger.getLogger(AbstractQuerySLOsCommand.class.getName(), BUNDLE);
    private Filter<ComponentInfo> serviceNameFilter;
    private Filter<Resource> resourceFilter = ConstantFilter.<Resource>alwaysMatching();
    
    /**
     * Gets the services from CS and iterator over the SLOs.
     * 
     * For each SLO of each found service the processSLO method is called.
     * Finally the getResult method is called.
     * 
     * @param env  the execution
     * @return the result of the command
     * @throws com.sun.grid.grm.GrmException CS is not reachable
     */
    public final Result<T> execute(ExecutionEnv env) throws GrmException {
        
        Filter<ComponentInfo> filter = GetComponentInfosCommand.newTypeFilter(Service.class);
        
        if(serviceNameFilter != null) {
            AndFilter<ComponentInfo> andFilter = new AndFilter<ComponentInfo>();
            andFilter.add(filter);
            andFilter.add(serviceNameFilter);
            filter = andFilter;
        }
        
        for(ComponentInfo ci: ComponentService.getComponentInfos(env, filter)) {
            String serviceName = ci.getName();
            try {
                Service service = ComponentService.<Service>getComponent(env, ci);
                List<SLOState> slos = service.getSLOStates(resourceFilter);
                for(SLOState sloState: slos) {
                    processSLO(serviceName, sloState);
                }
            } catch(GrmException ex) {
                serviceError(serviceName, ex);
            }
        }
        
        return new CommandResult<T>(getResult());
    }
    
    /**
     * process the state of a SLO
     * @param serviceName  the name of the service
     * @param sloState     the state of the SLO
     */
    protected abstract void processSLO(String serviceName, SLOState sloState);
    
    /**
     * This method is call if the slos for a service can not be processed
     * @param serviceName  name of the service
     * @param error        the error 
     */
    protected abstract void serviceError(String serviceName, GrmException error);
    
    /**
     * Get the result of this command
     * 
     * @return the result of this command
     */
    protected abstract T getResult();
    
    
    /**
     * Set the name of the service.
     * 
     * @param serviceName the name of the service. if this parameter is null all
     *                    services will be ask for needs
     * @throws com.sun.grid.grm.util.filter.FilterException if serviceName is not a valid regular expression
     */
    public void setServiceName(String serviceName) throws FilterException {
        if(serviceName == null) {
            serviceNameFilter = ConstantFilter.<ComponentInfo>alwaysMatching();
        } else {
            serviceNameFilter = GetComponentInfosCommand.newRegExpNameFilter(serviceName);
        }
    }

    /**
     * Set the filter for the resource name
     * @param resourceRegExp the resource name filter
     * @throws com.sun.grid.grm.util.filter.FilterException if resourceRegExp is not a valid regular expression
     */
    public void setResourceFilter(String resourceRegExp) throws FilterException {
        if(resourceRegExp == null) {
            this.resourceFilter = ConstantFilter.<Resource>alwaysMatching();
        } else {
            this.resourceFilter = new RegExpFilter<Resource>(new FilterVariable<Resource>(ResourceVariableResolver.ID_VAR), resourceRegExp);
        }
    }
    

}
