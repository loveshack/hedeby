/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JVM;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.service.GetComponentInfosCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.NotFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Command that starts either component on a specified host/all hosts
 * or all components on a specified host/all hosts.
 * Command starts components that have been already registered in JVM
 */
public class StartComponentCommand extends AbstractComponentCommand {
    
    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final long serialVersionUID = -2007112601L;
    private static String message;
     
    private static String getMessage() {
        if(message == null) {
            message = I18NManager.formatMessage("ui.StartComponent.started", BUNDLE_NAME);
        }
        return message;
    }
    /**
     * Call the start method of the component
     * @param comp the component
     * @throws com.sun.grid.grm.GrmException throws by the start method
     * @return the success message
     */
    protected String changeComponent(GrmComponent comp) throws GrmException {
        comp.start();
        return getMessage();
    }
    
    /**
     * This method searchs first all matching components and call then
     * for each component the <code>changeComponent</code> method
     * @param env   the exection env
     * @throws com.sun.grid.grm.GrmException on any error
     * @return list a result for each matching component
     */
    @Override
    public Result<List<ComponentChangeResult>> execute(ExecutionEnv env) throws GrmException {
        
        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>(3);
        
        Filter<ComponentInfo> typeFilter = GetComponentInfosCommand.newTypeFilter(JVM.class);
        NotFilter<ComponentInfo> notFilter = new NotFilter<ComponentInfo>(typeFilter);
        filter.add(notFilter);
        
        if(getHostname()!= null) {
            filter.add(GetComponentInfosCommand.newHostFilter(getHostnameObj()));
        }
        if(getComponentName() != null) {
            filter.add(GetComponentInfosCommand.newNameFilter(getComponentName()));
        }
        
        List<ComponentInfo> components = ComponentService.getComponentInfos(env, filter);
               
        List<ComponentChangeResult> affectedComps = new ArrayList<ComponentChangeResult>(components.size());
        
        
        //now check global config for components that dont have component info entries
        
        //get running jvms
        AndFilter<ComponentInfo> jvmFilter = new AndFilter<ComponentInfo>(2);
        jvmFilter.add(GetComponentInfosCommand.newTypeFilter(JVM.class));
        if(getHostname() != null) {
            jvmFilter.add(GetComponentInfosCommand.newHostFilter(getHostnameObj()));
        }
        List<ComponentInfo> jvmInfos = ComponentService.getComponentInfos(env, jvmFilter);
        
        if (!jvmInfos.isEmpty()) {
            //get global config
            GetGlobalConfigurationCommand cmd  = new GetGlobalConfigurationCommand();
            GlobalConfig global = env.getCommandService().<GlobalConfig>execute(cmd).getReturnValue();
            for (JvmConfig jvm : global.getJvm()) {
                for (ComponentInfo ci : jvmInfos) {
                    //check if this jvm is running
                    if (jvm.getName().equals(ci.getName())) {
                        //get components that are matching the name-host pattern
                        Set<Component> comps = GetComponentCommand.getComponents(jvm, getComponentName(), ci.getHostname());
                        for (Component c : comps) {
                            boolean already = false;
                            for (ComponentInfo ch : components) {       
                                //check if component has already componentinfo entry
                                if (ch.getName().equals(c.getName()) && (ch.getHostname().equals(ci.getHostname()))) {
                                    //if yes, the component should not be instantiated
                                    already = true;
                                    break;
                                }
                            }
                            //
                            if (!already) {
                                try {
                                    InstantiateComponentCommand initCmd = new InstantiateComponentCommand();
                                    initCmd.setJvmName(jvm.getName());
                                    initCmd.setComponentName(c.getName());
                                    initCmd.setHostName(ci.getHostname().getHostname());
                                    env.getCommandService().execute(initCmd);
                                    //instantiated so compoonentinfo must appear
                                    ComponentInfo started =  ComponentService.getComponentInfoByNameAndHost(env, c.getName(), ci.getHostname());
                                    //check if component is Autostart type,
                                    if (!c.isAutostart()) {
                                        //not an autostart so add componentinfo 
                                        components.add(started);
                                    } else {
                                        affectedComps.add(new ComponentChangeResult(started, getMessage()));
                                    }
                                } catch (GrmException ex) {
                                    affectedComps.add(new ComponentChangeResult(ComponentInfo.newInstance(env, ci.getHostname(), ci.getName(), c.getName(), GrmComponent.class), ex.getLocalizedMessage(), ex));                                  
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        //now trigger start operation on all registered components
        for (ComponentInfo ci: components) {
            try {
                GrmComponent comp = ComponentService.getComponent(env, ci);
                String msg = changeComponent(comp);
                affectedComps.add(new ComponentChangeResult(ci, msg));
            } catch (GrmException ex) {
                affectedComps.add(new ComponentChangeResult(ci, ex.getLocalizedMessage(), ex));
            } catch (Exception ex) {
                String message = ex.getLocalizedMessage();
                if(message == null || message.equals("null"))  {
                    message = ex.getClass().getName();
                }
                affectedComps.add(new ComponentChangeResult(ci, 
                     I18NManager.formatMessage("ui.AbstractComponent.unknownError", BUNDLE_NAME, message), ex));
            }
        }
        
        if(affectedComps.isEmpty()) {
            throw new GrmException("ui.AbstractComponent.ComponentNotFound", BUNDLE_NAME);
        }
        
        return new CommandResult<List<ComponentChangeResult>>(affectedComps);
    }
}
