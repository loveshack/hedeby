/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.component.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ComponentInfo;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.ActiveComponent;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.AndFilter;
import com.sun.grid.grm.util.filter.CompareFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterConstant;
import com.sun.grid.grm.util.filter.FilterException;
import com.sun.grid.grm.util.filter.FilterVariable;
import com.sun.grid.grm.util.filter.RegExpFilter;
import com.sun.grid.grm.util.filter.VariableResolver;
import java.util.LinkedList;
import java.util.List;
import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

/**
 *  This command gets the component infos of active components from CS.
 */
public class GetComponentInfosCommand extends AbstractSystemCommand<List<ComponentInfo>> {

    private static final long serialVersionUID = -2007110601L;
    private final static String BUNDLE = "com.sun.grid.grm.ui.component.service.messages";
    private final Filter<ComponentInfo> filter;

    /** Creates a new instance of GetComponentInfosCommand
     *  @param filter the filter (can be null)
     */
    public GetComponentInfosCommand(Filter<ComponentInfo> filter) {
        this.filter = filter;

    }

    /**
     *  Create a new instance of GetComponentInfosCommand with a component name
     *  filter
     *  @param name value of the name filter
     *  @return the GetComponentInfosCommand
     */
    public static GetComponentInfosCommand newInstanceWithNameFilter(String name) {
        return new GetComponentInfosCommand(newNameFilter(name));
    }

    /**
     *  Create a new instance of GetComponentInfosCommand with a component type
     *  filter
     *  @param type value of the type filter
     *  @return the GetComponentInfosCommand
     */
    public static GetComponentInfosCommand newInstanceWithTypeFilter(Class<?> type) {
        return new GetComponentInfosCommand(newTypeFilter(type));
    }

    /**
     *  Create a new instance of GetComponentInfosCommand with a component type
     *  host and a name filter
     *  @param name value of the name filter
     *  @param type value of the type filter
     *  @param host value of the host filter
     *  @return the GetComponentInfosCommand
     */
    public static GetComponentInfosCommand newInstanceWithNameTypeAndHostFilter(String name, Class<?> type, Hostname host) {

        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>();
        filter.add(newNameFilter(name));
        filter.add(newTypeFilter(type));
        filter.add(newHostFilter(host));
        return new GetComponentInfosCommand(filter);
    }

    /**
     *  Create a new instance of GetComponentInfosCommand with a component type
     *  and name filter
     *  @param name value of the name filter
     *  @param type value of the type filter
     *  @return the GetComponentInfosCommand
     */
    public static GetComponentInfosCommand newInstanceWithNameAndTypeFilter(String name, Class<?> type) {
        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>(2);
        filter.add(newNameFilter(name));
        filter.add(newTypeFilter(type));
        return new GetComponentInfosCommand(filter);
    }

    /**
     *  Create a new instance of GetComponentInfosCommand with a component name
     *  and host filter
     *  @param name value of the name filter
     *  @param host value of the host filter
     *  @return the GetComponentInfosCommand
     */
    public static GetComponentInfosCommand newInstanceWithNameAndHostFilter(String name, Hostname host) {
        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>(2);
        filter.add(newNameFilter(name));
        filter.add(newHostFilter(host));
        return new GetComponentInfosCommand(filter);
    }

    /**
     *  Create a new instance of GetComponentInfosCommand with a component host
     *  and type filter
     *  @param type value of the type filter
     *  @param host value of the host filter
     *  @return the GetComponentInfosCommand
     */
    public static GetComponentInfosCommand newInstanceWithHostnameAndTypeFilter(Hostname host, Class<?> type) {
        AndFilter<ComponentInfo> filter = new AndFilter<ComponentInfo>(2);
        filter.add(newHostFilter(host));
        filter.add(newTypeFilter(type));
        return new GetComponentInfosCommand(filter);
    }

    /**
     * Execute the command.
     * @param env the execution env
     * @throws com.sun.grid.grm.GrmException if the command failed
     * @return result with a list of found components
     */
    public Result<List<ComponentInfo>> execute(ExecutionEnv env) throws GrmException {
        Context ctx = env.getContext();
        try {
            List<ComponentInfo> ret = new LinkedList<ComponentInfo>();
            NamingEnumeration<Binding> enumeration = ctx.listBindings(BootstrapConstants.CS_ACTIVE_COMPONENT);
            if (enumeration != null) {
                ComponentInfoVariableResolver resolver = null;
                if (filter != null) {
                    resolver = new ComponentInfoVariableResolver();
                }
                while (enumeration.hasMore()) {
                    ActiveComponent ac = (ActiveComponent) enumeration.next().getObject();
                    ComponentInfo ci = ComponentInfo.newInstance(ac);
                    resolver.setComponentInfo(ci);
                    if (filter == null || filter.matches(resolver)) {
                        ret.add(ci);
                    }
                }
            }
            return new CommandResult<List<ComponentInfo>>(ret);
        } catch (NamingException ex) {
            throw new GrmException("ui.componentservice.lookupexception", ex, BUNDLE, GetComponentInfosCommand.class, new String[]{filter.toString()});
        } finally {
            try {
                ctx.close();
            } catch (NamingException ex) {
                // Ignore
            }
        }

    }

    /**
     * Create a new filter for the name of a component
     * @param name the name of the component
     * @return the name filter
     */
    public static Filter<ComponentInfo> newNameFilter(String name) {
        return new CompareFilter<ComponentInfo>(
                new FilterVariable<ComponentInfo>(ComponentInfoVariableResolver.NAME),
                new FilterConstant<ComponentInfo>(name),
                CompareFilter.Operator.EQ);
    }

    /**
     * Create a new regexp filter for the name of a component
     * @param name the pattern for the regular expression
     * @return  the repexp filter
     * @throws com.sun.grid.grm.util.filter.FilterException if name is not a regular expression
     */
    public static Filter<ComponentInfo> newRegExpNameFilter(String name) throws FilterException {
        return new RegExpFilter<ComponentInfo>(
                new FilterVariable<ComponentInfo>(ComponentInfoVariableResolver.NAME),
                name);
    }

    /**
     * Create a new regexp filter for the  jvm name of a component
     * @param jvm the pattern for the regular expression
     * @return  the repexp filter
     * @throws com.sun.grid.grm.util.filter.FilterException if name is not a regular expression
     */
    public static Filter<ComponentInfo> newRegExpJvmFilter(String jvm) throws FilterException {
        return new RegExpFilter<ComponentInfo>(
                new FilterVariable<ComponentInfo>(ComponentInfoVariableResolver.JVM),
                jvm);
    }

    /**
     * Create a new filter for the hostname of a component
     * @param hostname the matching hostname
     * @return the hostname filter
     */
    public static Filter<ComponentInfo> newHostFilter(Hostname hostname) {
        return new CompareFilter<ComponentInfo>(
                new FilterVariable<ComponentInfo>(ComponentInfoVariableResolver.HOST),
                new FilterConstant<ComponentInfo>(hostname),
                CompareFilter.Operator.EQ);
    }

    /**
     * Create a new regexp filter for the hostname name of a compoent
     * @param name pattern for the regular expression
     * @return  the regexp filter
     * @throws com.sun.grid.grm.util.filter.FilterException if name os not a valid regular expression
     */
    public static Filter<ComponentInfo> newRegExpHostFilter(String name) throws FilterException {
        return new RegExpFilter<ComponentInfo>(
                new FilterVariable<ComponentInfo>(ComponentInfoVariableResolver.HOST),
                name);
    }

    /**
     * Create a new filter for the type of a component
     * @param type the type of the component
     * @return the type filter
     */
    public static Filter<ComponentInfo> newTypeFilter(Class<?> type) {
        return new CompareFilter<ComponentInfo>(
                new FilterVariable<ComponentInfo>(ComponentInfoVariableResolver.TYPE),
                new FilterConstant<ComponentInfo>(type),
                CompareFilter.Operator.GE);
    }

    private static class ComponentInfoVariableResolver implements VariableResolver<ComponentInfo> {

        public final static String NAME = "name";
        public final static String TYPE = "type";
        public final static String HOST = "host";
        public final static String JVM = "jvm";
        private ComponentInfo ci;

        public void setComponentInfo(ComponentInfo ci) {
            this.ci = ci;
        }

        public Object getValue(String name) {
            if (NAME.equals(name)) {
                return ci.getName();
            } else if (HOST.equals(name)) {
                return ci.getHostname();
            } else if (JVM.equals(name)) {
                return ci.getJvm().getName();
            } else if (TYPE.equals(name)) {
                try {
                    return Class.forName(ci.getInterface());
                } catch (ClassNotFoundException ex) {
                    return null;
                }
            } else {
                return null;
            }
        }
    }
}
        
