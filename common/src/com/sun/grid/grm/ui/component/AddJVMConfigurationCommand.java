/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.component;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.CSModifyCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

/**
 * Command that adds new JVM
 */
public class AddJVMConfigurationCommand extends AbstractSystemCommand<Void> implements CSModifyCommand {

    /**
     * i18n Bundle name
     */
    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.component.messages";
    private static final Logger log = Logger.getLogger(AddJVMConfigurationCommand.class.getName());
    private static final long serialVersionUID = -2007080101L;

    private JvmConfig jvmConfig;    
    private ModifyGlobalConfigurationCommand executedCmd;
    
    /**
     * Creates an instance of GetJVMConfigurationCommand.
     */
    public AddJVMConfigurationCommand() {
        this.jvmConfig = null;
    }   
    
    /**
     * Creates an instance of GetJVMConfigurationCommand.
     * @param jvm JVM configuration
     */
    public AddJVMConfigurationCommand(JvmConfig jvm) {
        this.jvmConfig = jvm;
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {        
            
        GetGlobalConfigurationCommand cmd = new GetGlobalConfigurationCommand();
        GlobalConfig global = cmd.execute(env).getReturnValue();
        if (this.jvmConfig == null) {
            this.undo(env);
            GrmException grm = new GrmException("ui.command.nullnotallowed", BUNDLE_NAME, AddJVMConfigurationCommand.class.getName(), "jvmConfig");
            throw grm;
        } else {
            List<JvmConfig> jvms = global.getJvm();
            boolean unique = true;
            for (JvmConfig jvmc : jvms) {
                if (jvmc.getName().equals(this.jvmConfig.getName())) {
                    unique = false;
                }
            }
            if (unique) {
                global.getJvm().add(this.jvmConfig);
                ModifyGlobalConfigurationCommand mod = new ModifyGlobalConfigurationCommand(global);
                mod.execute(env);
                executedCmd = mod;
            } else {
                this.undo(env);
                GrmException grm = new GrmException("ui.context.objectexists", BUNDLE_NAME, "jvm", jvmConfig.getName());
                throw grm;
            }
        }

        return CommandResult.VOID_RESULT;
        
    }

    @Override
    public void undo(ExecutionEnv env) {
        if (executedCmd != null) {
            executedCmd.undo(env);
            executedCmd = null;
        }
    }
    
    

    /**
     * Sets the JVM configuration
     * @param jvm
     */
    public void setJVMConfig(JvmConfig jvm) {
        this.jvmConfig = jvm;
    }

    /**
     * Gets the JVM configuration
     * @return the JVM config
     */
    public JvmConfig getJVMConfig() {
        return this.jvmConfig;
    }
    
    /**
     * Get the list of names which will be modified by this command
     * @return list of names
     */
    public List<String> getListOfModifyNames() {
        return Collections.<String>singletonList(BootstrapConstants.CS_GLOBAL);
    }
}
