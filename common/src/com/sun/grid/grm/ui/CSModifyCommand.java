/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import java.util.List;

/**
 * Interface to be used with SystemCommands that do some modifications in ConfiguratrionServiceContext
 */
public interface CSModifyCommand {
    
    /**
     * Gets the list of names of the objects that will be used by System Command.
     * The names can point to specific object like: "global" or "component.spare_pool".
     * But the name can also contain all objects in given context this means that specifying:
     * "active_components.*" will define that command is modifying all objects in "active_components"
     * subcontext (all objects non-context objects). If we have structure like:
     * "foo.bar" and "foo.boo.bar" the name specified in modify list is like:
     * "foo.*" it will mean that only "foo.bar" is modified. The "foo.boo.bar" is untouched.
     * 
     * @return list of names
     */
    public List<String> getListOfModifyNames();
    
}
