/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

/**
 * Local command for removing host information from preferences. 
 * Used for uninstalling the host from system.
 */
public class UninstallHostFromPreferencesCommand extends AbstractLocalCommand<Void> {
    private PreferencesType prefs;
    private Hostname hostname;
    private byte[] snapshot;
    private final static String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(UninstallHostFromPreferencesCommand.class.getName(), BUNDLE);
    /** Creates a new instance of UninstallHostFromPreferencesCommand */
    public UninstallHostFromPreferencesCommand(PreferencesType prefs, Hostname hostname) {
        this.setPreferencesType(prefs);
        this.setHostname(hostname);
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        snapshot = null;
        try {           
            snapshot = PreferencesUtil.exportSystemPreferences(getPreferencesType(), env.getSystemName());
        } catch (Exception ex) {
            throw new GrmException("UninstallHostFromPreferencesCommand.error.preferences_not_exported", BUNDLE, ex.getCause());
        } 
        
        try {            
            PreferencesUtil.uninstallHost(env.getSystemName(), getPreferencesType());
        } catch (PreferencesNotAvailableException ex) {
            throw new GrmException("UninstallHostFromPreferencesCommand.error.uninstall_failed_no_host_preferences", BUNDLE, ex.getCause(), getHostname().getHostname());
        } catch (BackingStoreException ex) {
            throw new GrmException("UninstallHostFromPreferencesCommand.error.uninstall_failed_operation_failed", BUNDLE, ex.getCause(), getHostname().getHostname());
        }       
        return CommandResult.VOID_RESULT;
    }
    @Override
    public void undo(ExecutionEnv env) {
        if (snapshot!=null) {
            try {
                PreferencesUtil.importSystemPreferences(getPreferencesType(), env.getSystemName(), snapshot);
            } catch (Exception ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "UninstallHostFromPreferencesCommand.undo.error_import_preferences");
                    lr.setThrown(ex);
                    log.log(lr);
                }
            } finally {
                
                snapshot = null;
            }         
        }
    }

    public PreferencesType getPreferencesType() {
        return prefs;
    }

    public void setPreferencesType(PreferencesType prefs) {
        this.prefs = prefs;
    }

    public Hostname getHostname() {
        return hostname;
    }

    public void setHostname(Hostname hostname) {
        this.hostname = hostname;
    }
}
