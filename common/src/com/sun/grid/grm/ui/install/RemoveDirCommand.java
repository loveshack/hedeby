/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;


/**
 * Local Command for removing the content of directory, the current
 * content is copied to TMP directory for possible undo.
 * TMP directory location is taken from system property "java.io.tmpdir"
 * The removal of directory is done using platform dependend code
 */

public class RemoveDirCommand extends AbstractLocalCommand<Void> {
    private File tmp = null;

    private static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(RemoveDirCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007121801;
    private File dir = null;
    private boolean undo;
    
    /**
     * Creates a new instance of RemoveDirCommand
     * @param dir - directory that has to be removed, cannot be null
     * @throws com.sun.grid.grm.GrmException - when given dir is null
     */
    public RemoveDirCommand(File dir) throws GrmException {
        this.setDir(dir);
    }
    
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        undo = false;
        tmp = null;
        tmp = Platform.getPlatform().createTmpDir(null, null);

        //get current directory
        File cur = new File(System.getProperty("user.dir"));
        
        //to remove the directory current user directory cannot be directory that is removed, or
        //any of its subdirectories
        if (FileUtil.isSubDirectory(getDir(), cur)) {
            throw new  GrmException("RemoveDirCommand.error.user_dir_conflict",
                    BUNDLE,
                    getDir().getAbsolutePath(), cur.getAbsolutePath());
        }
        //to remove localspool we can check whether we have file permissions
        if (!checkPermissions(getDir())) {
            throw new  GrmException("RemoveDirCommand.error.dir_permissions",
                    BUNDLE,
                    getDir().getAbsolutePath());
        }
       
        //copy current localspool to tmp
        try {
            Platform.getPlatform().copy(getDir(), tmp, true);
        } catch (Exception ex) {
            throw new GrmException("RemoveDirCommand.error.cant_copy_dir_for_undo",
                    ex.getCause(),
                    BUNDLE,                   
                    getDir().getAbsolutePath(),
                    tmp.getAbsolutePath());
        }
        
        
        try {
            //the only operation that should be undo
            undo = true;
            Platform.getPlatform().removeDir(getDir(), true);
        } catch (Exception ex) {
            this.undo(env);
            throw new GrmException("RemoveDirCommand.error.remove_operation_dir_failed",
                    ex.getCause(),
                    BUNDLE,
                    getDir().getAbsolutePath());
        }
        if (getDir().exists()) {
            this.undo(env);
            throw new GrmException("RemoveDirCommand.error.remove_operation_dir_failed_still_there",
                    BUNDLE,
                    getDir().getAbsolutePath());
        }
        return CommandResult.VOID_RESULT;
    }
    
    @Override
    public void undo(ExecutionEnv env) {
        if (undo) {
            //check if localspool exists - remove failed ?? deleted partially, try to fix it
            if (getDir().exists()) {
                try {
                    Platform.getPlatform().removeDir(getDir(), true);
                } catch (Exception ex) {
                    if (log.isLoggable(Level.WARNING)) {
                        LogRecord lr = new LogRecord(Level.WARNING, I18NManager.formatMessage("RemoveDirCommand.undo.removedirfailed", BUNDLE, getDir().getAbsolutePath()));
                        lr.setThrown(ex);
                        log.log(lr);
                    }
                    //log - not removed localspool we cannot do anything
                    return;
                }
            }
            File tmplocation = new File(tmp, getDir().getName());
            try {
                Platform.getPlatform().copy(tmplocation, getDir().getParentFile(), true);
            } catch (Exception ex) {
                //log error - while undo
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, I18NManager.formatMessage("RemoveDirCommand.undo.restoredirfailed", BUNDLE, tmplocation.getAbsolutePath(), getDir().getAbsolutePath()));
                    lr.setThrown(ex);
                    log.log(lr);
                }
                return;
            }
            try {
                Platform.getPlatform().removeDir(tmp, true);
            } catch (Exception ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, I18NManager.formatMessage("RemoveDirCommand.undo.removetmpfailed", BUNDLE, tmp.getAbsolutePath()));
                    lr.setThrown(ex);
                    log.log(lr);
                }
                //we dont care tmp not clear system should care
                return;
            }
            undo = false;
        }
    }
    
    /**
     * Helper method for checking if all files and directories canbe read and written by
     * user
     */
    private boolean checkPermissions(File file) {
        if (!(file.canRead() && file.canWrite())) {
            return false;
        }
        File [] files = file.listFiles();
        if (files == null || files.length == 0) {
            return true;
        } else {
            for (int i=0; i< files.length; i++) {
                if (!checkPermissions(files[i])) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Getter for directory to be removed
     * @return dir - directory that is set to be removed
     */
    public File getDir() {
        return dir;
    }
    /**
     * Setter for directory to be removed
     * @param dir - directory that has to be removed, cannot be null
     * @throws com.sun.grid.grm.GrmException - when given dir is null
     */
    public void setDir(File dir) throws GrmException {
        if (dir == null) {
            throw new GrmException("RemoveDirCommand.error.directory_null", BUNDLE);
        }
        this.dir = dir;
    }
    
}
