/*___INFO__MARK_BEGIN__*/
 /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;

/** 
 * Local command for checking if the SMF is available on current host.
 * To do the check the script $hedeby_dist/bin/sdm_smf.sh support is called
 */

public class CheckSMFSupportCommand extends AbstractLocalCommand<Boolean> {
    
    private static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final long serialVersionUID = 2007121801;
    
    /** Creates a new instance of CheckSMFSupportCommand */
    public CheckSMFSupportCommand() {
    }

    public Result<Boolean> execute(ExecutionEnv env) throws GrmException {
        //If the is no SMF installed on this host, check whether this host supports SMF
        Boolean ret = Boolean.valueOf(true);
        if (Platform.getPlatform().isWindowsOs()) {
            //no Support on this platform
            ret = Boolean.valueOf(false);
        } else {
            //Command for checking the availability of SMF
            File script = new File(env.getDistDir(),"util"+File.separator+"sdmsmf.sh");
            if (!script.isFile()) {
                throw new GrmException("CheckSMFSupportCommand.error.script_not_found", BUNDLE, script);
            }
            String command = script.getAbsolutePath()+" support";
            int exit = -1;
            try {
                exit = Platform.getPlatform().exec(command, null, PathUtil.getTmpDir(), null, null, 0);
            } catch (Exception ex) {
                throw new GrmException("CheckSMFSupportCommand.error.failed_to_execute_command", ex, BUNDLE, command);
            }
            if (exit == 2) {
                //SMF not supported
                ret = Boolean.valueOf(false);
            } else if (exit != 0) {
                throw new GrmException("CheckSMFSupportCommand.error.command_error", BUNDLE, command);
            }
        }
        CommandResult<Boolean> res = new CommandResult<Boolean>();
        res.setReturnValue(ret);
        return res;
    }
    
}
