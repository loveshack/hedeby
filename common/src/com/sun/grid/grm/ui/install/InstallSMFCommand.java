/*___INFO__MARK_BEGIN__*/
 /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.PreferencesAlreadyExistException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

/**
 * This is installation command for providing SMF support in hedeby system.
 * SMF services are created for specific JVMs of the system.
 * There are 2 kinds of JVMs CS_VM that contain configurational component, and 
 * other JVMs, all JVMs that are on master host (so called CS host) have dependency 
 * on CS_VM this means that SMF service for NOT-CS VM will not be started unless 
 * CS_VM is online.
 * To perform installation the script located in:
 * $hedeby_dist/bin/sdm_smf.sh is neccessary.
 * The script provides such commands like: register for installation of service, and
 * unregister for uninstallation of service from machine.
 * For installation the SMF manifest is neccessary that is created from one of the 
 * templates:
 * $hedeby_dist/util/templates/sdm_template.xml
 * $hedeby_dist/util/templates/sdm_template_masterhost.xml
 * During the installation of SMF service in the system the entry in preferences
 * is created. Entry contains pair: jvmName and FMRI name of service. This name of 
 * service can be used later for removing the SMF service.
 * The installed SMF service is enabled by default, that means that after installation is complete
 * It tries to go online.
 */

public class InstallSMFCommand extends AbstractLocalCommand<Void> {
    
    private static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final long serialVersionUID = 2007121801;
    private static final Logger log = Logger.getLogger(InstallSMFCommand.class.getName(), BUNDLE);
    private String jvmName;
    private PreferencesType prefs;
    private boolean master = false;
    private static final String masterScript = "sdm_template_masterhost.xml";
    private static final String script = "sdm_template.xml";
    private byte[] snapshot;
    private boolean undoCommand;
    
    /** 
     * Creates a new instance of InstallSMFCommand
     * @param jvmName - name of jvm for which SMF support should be installed
     * @param prefs - type of preferences for current system
     */
    public InstallSMFCommand(String jvmName, PreferencesType prefs) {
        this.setJvmName(jvmName);
        this.prefs = prefs;
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        snapshot=null;
        undoCommand=false;
        String version;
        CheckSMFSupportCommand cmd = new CheckSMFSupportCommand();
        boolean smf = env.getCommandService().<Boolean>execute(cmd).getReturnValue();
        if (!smf) {
            //SMF not supported
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.SMFnotsupported", BUNDLE);
        }
        //check if master host is this host
        Hostname local = Hostname.getLocalHost();
        version = SystemUtil.getBinaryVersion(env);
        File template;
        
        try {
            if (!getJvmName().equals(BootstrapConstants.CS_JVM) && local.equals(env.getCSHost()) &&
                    PreferencesUtil.hasSMFSupport(env.getSystemName(), prefs, BootstrapConstants.CS_JVM)) {
                template = new File(env.getDistDir(),"util"+File.separator+"templates"+File.separator+masterScript);
            } else {
                template = new File(env.getDistDir(),"util"+File.separator+"templates"+File.separator+script);
            }
        } catch (BackingStoreException ex) {
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.preferences_access_error", ex, BUNDLE);
        }
        if (!template.isFile()) {
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.template_missing", BUNDLE, template.getAbsolutePath());
        }
        //Install into preferences - save prefs for undo
        //before modifying preferences export for undo
        try {           
            snapshot = PreferencesUtil.exportSystemPreferences(prefs, env.getSystemName());
        } catch (Exception ex) {
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.preferences_not_exported", ex, BUNDLE);
        } 
        
        String serviceName = SystemUtil.getSMFFMRI(env.getSystemName(), getJvmName());
        try {
            PreferencesUtil.installSMFKey(env.getSystemName(), prefs, getJvmName(), serviceName);
        } catch (PreferencesAlreadyExistException ex) {
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.smf_support_already_installed", ex, BUNDLE, getJvmName());
        } catch (BackingStoreException ex) {
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.failed_to_install_into_preferences", ex, BUNDLE, getJvmName());
        }
        
        //create tmpdir
        File tmp = Platform.getPlatform().createTmpDir(null, null);
        //file where manifest will be stored before importing
        tmp = new File(tmp, script);
        //pattern for modifying template file hedeby.env
        Map<String, String> patterns = new HashMap<String, String>(1);
        
        patterns.put("@@@SDM_DIST@@@", env.getDistDir().getAbsolutePath());
        patterns.put("@@@JAVA_HOME@@@", System.getProperty("java.home"));
        patterns.put("@@@SYSTEM_NAME@@@", env.getSystemName());
        patterns.put("@@@JVM_NAME@@@", getJvmName());
        patterns.put("@@@CS_VM@@@", BootstrapConstants.CS_JVM);
        patterns.put("@@@SDM_LOCALSPOOL@@@", env.getLocalSpoolDir().getAbsolutePath());
        patterns.put("@@@NAME@@@", SystemUtil.getSMFName(env.getSystemName(), getJvmName()));
        patterns.put("@@@INSTANCE_NAME@@@", SystemUtil.getSMFInstanceName(getJvmName()));
        patterns.put("@@@CS_INSTANCE_NAME@@@", SystemUtil.getSMFInstanceName(BootstrapConstants.CS_JVM));
        patterns.put("@@@CS_FMRI@@@", SystemUtil.getSMFFMRI(env.getSystemName(), BootstrapConstants.CS_JVM));
        patterns.put("@@@VERSION@@@", version);
        
        try {
            FileUtil.replace(template, tmp, patterns);
        } catch (IOException ex) {
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.manifest_creation_failed", ex, BUNDLE, template);
        }
        //we have manifest ready so just register service
        //Command for installing smf
        File installScript = new File(env.getDistDir(),"util"+File.separator+"sdmsmf.sh");
        if (!installScript.isFile()) {
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.script_not_found", BUNDLE, script);
        }
        String command = installScript.getAbsolutePath()+" register "+tmp.getAbsolutePath();
        int exit = -1;
        try {
            exit = Platform.getPlatform().exec(command, null, PathUtil.getTmpDir(), null, null, 0);
        } catch (Exception ex) {
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.failed_to_execute_command", ex, BUNDLE, command);
        }
        if (exit != 0) {
            this.undo(env);
            throw new GrmException("InstallSMFSupportCommand.error.command_error", BUNDLE, command, exit);
        }
        undoCommand=true;
        return CommandResult.VOID_RESULT;
    }  
    @Override
    public void undo(ExecutionEnv env) {        
        if (snapshot!=null) {
            try {
                PreferencesUtil.importSystemPreferences(prefs, env.getSystemName(), snapshot);
            } catch (Exception ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "InstallSMFCommand.undo.error_import_preferences");
                    lr.setThrown(ex);
                    log.log(lr);
                }
            } finally {
                
                snapshot = null;
            }
            
            if (undoCommand) {
                //we have manifest ready so just register service
                //Command for installing smf
                File installScript = new File(env.getDistDir(),"util"+File.separator+"sdmsmf.sh");
                if (!installScript.isFile()) {
                    if (log.isLoggable(Level.WARNING)) {
                        LogRecord lr = new LogRecord(Level.WARNING, "InstallSMFCommand.undo.missing_script");
                        lr.setParameters(new Object[]{installScript.getAbsolutePath()});
                        log.log(lr);
                    }
                }
                String command = installScript.getAbsolutePath()+" unregister "+SystemUtil.getSMFFMRI(env.getSystemName(), getJvmName());
                int exit = -1;
                try {
                    exit = Platform.getPlatform().exec(command, null, PathUtil.getTmpDir(), null, null, 0);
                } catch (Exception ex) {
                    if (log.isLoggable(Level.WARNING)) {
                        LogRecord lr = new LogRecord(Level.WARNING, "InstallSMFCommand.undo.script_execution_exp");
                        lr.setParameters(new Object[]{command});
                        log.log(lr);
                    }
                }
                if (exit != 0) {
                    if (log.isLoggable(Level.WARNING)) {
                        LogRecord lr = new LogRecord(Level.WARNING, "InstallSMFCommand.undo.script_not_success");
                        lr.setParameters(new Object[]{command, exit});
                        log.log(lr);
                    }
                }
            }
        }
        undoCommand=false;
    }
    /**
     * Getter for jvmName
     * @return the jvm name
     */
    public String getJvmName() {
        return jvmName;
    }
    /**
     * Setter for jvmName
     * @param jvmName name of jvm for which SMF is installed
     */
    public void setJvmName(String jvmName) {
        this.jvmName = jvmName;
    }
}
