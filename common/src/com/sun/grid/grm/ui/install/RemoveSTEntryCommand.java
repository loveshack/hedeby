/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;

/**
 *
 * This command remove entry from local Service Tags registry.
 */
public class RemoveSTEntryCommand extends AbstractLocalCommand<Void> {
    
    private static String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    
    private final PreferencesType prefs;
    private boolean undo = false;
    
    private final StringBuilder stPath = new StringBuilder()
            .append(File.separator)
            .append("util")
            .append(File.separator)
            .append("sdmST")
            .append(File.separator)
            .append("sdm_st");
    
    /**
     * Constructor for this command.
     * @param prefs system preferences type
     */
    public RemoveSTEntryCommand(PreferencesType prefs) throws GrmException {
        if (prefs == null) {
            throw new GrmException("RemoveSTEntryCommand.error.prefs_null", BUNDLE);
        }
        this.prefs = prefs;
    }
        
    /**
     * This method executes this command. Its removing the Service Tag entry from 
     * the ST local registry
     * @param env execution env variable
     * @return nothing
     * @throws com.sun.grid.grm.GrmException
     */
    @SuppressWarnings("static-access")
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        //TODO: check future releases of ST. Windows ST support is planned.
        if (!Platform.getPlatform().isWindowsOs()) {
            //creating string representation for path to the script ($SDM_DIST/util/sdmST/sdm_st)
            StringBuilder sb = new StringBuilder();
            sb.append(env.getDistDir());
            sb.append(stPath);
            File script = new File(sb.toString());
            //preparing the command for removing entry to the local ST registry 
            sb = new StringBuilder();
            sb.append(script.getAbsolutePath());
            sb.append(" unregister ");
            sb.append(env.getDistDir().getAbsolutePath());
            sb.append(" ");
            sb.append(prefs.toString());
            sb.append(":");          
            sb.append(env.getSystemName());
            int exit_code = 1;
            try {
                exit_code = Platform.getPlatform().exec(sb.toString(), null, PathUtil.getTmpDir(), null, null, 0);
            } catch (Exception ex) {
                //ignore and assume that ST are not supported
            }
            if (exit_code == 0) {
                undo = true;
            }
        }
        return CommandResult.VOID_RESULT;
    }
    
    /**
     * This undo method revive the Service Tag entry after unsuccessful uninstallation.
     * @param env
     */
    @Override
    @SuppressWarnings("static-access")
    public void undo(ExecutionEnv env) {
        //TODO: check future releases of ST. Windows ST support is planned.
        if (!Platform.getPlatform().isWindowsOs() && undo) {
            String instance_urn = "";
            instance_urn = PreferencesUtil.getSTinstanceUrn(env.getSystemName(), prefs);
            if (!instance_urn.equals("")) {
                //creating string representation for path to the script ($SDM_DIST/util/sdmST/sdm_st)
                StringBuilder sb = new StringBuilder();
                sb.append(env.getDistDir());
                sb.append(stPath);
                File script = new File(sb.toString());
                //preparing the command for adding entry to the local ST registry 
                sb = new StringBuilder();
                sb.append(script.getAbsolutePath());
                sb.append(" register_urn ");
                sb.append(env.getDistDir().getAbsolutePath());
                sb.append(" ");
                sb.append(prefs);
                sb.append(":");          
                sb.append(env.getSystemName());
                sb.append(" ");
                sb.append(instance_urn);
                int exit_code = 1;
                try {
                    exit_code = Platform.getPlatform().exec(sb.toString(), null, PathUtil.getTmpDir(), null, null, 0);
                } catch (Exception ex) {
                    //ignore and assume that ST are not supported
                }
            }
        }
    }
}
