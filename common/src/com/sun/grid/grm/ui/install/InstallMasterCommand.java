/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;


import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.Module;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.config.common.SLOSet;
import com.sun.grid.grm.config.executor.ExecutorConfig;
import com.sun.grid.grm.config.reporter.ReporterConfig;
import com.sun.grid.grm.config.resourceprovider.PriorityPolicyConfig;
import com.sun.grid.grm.config.resourceprovider.PriorityPolicyManagerConfig;
import com.sun.grid.grm.config.resourceprovider.ResourceProviderConfig;
import com.sun.grid.grm.config.sparepool.SparePoolServiceConfig;
import com.sun.grid.grm.executor.impl.ExecutorImpl;
import com.sun.grid.grm.reporting.impl.ReporterImpl;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.impl.ResourceProviderImpl;
import com.sun.grid.grm.security.SecurityModule;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.sparepool.SparePoolServiceImpl;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.LocalCommand;
import com.sun.grid.grm.ui.MacroCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.AddComponentWithConfigurationCommand;
import com.sun.grid.grm.ui.component.CreateGlobalConfigurationCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Command for installing the hedeby master host components.
 * Before starting this command the system bootsrap information
 * have to be set. This can be done with the AddSystemCommand.
 */
public class InstallMasterCommand extends MacroCommand<Void> implements LocalCommand<Void> {
    /**
     * i18n Bundle name
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(InstallMasterCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007062001;
    private final String localhost;
    
    private final String adminUser;
    private final Map<String,Object> initSecParams;
    private final int csPort;
    private final boolean autostart;
    private final boolean smf;
    private final boolean st;
    private final boolean noST;
    private final boolean simple;
    private final PreferencesType preferencesType;
    
    /**
     * This command is used to setup all hedeby master host components.
     * 
     * @param initSecParams parameter for security bootstraping
     * @param adminUser Admin user name used for installation
     * @param csPort port where configuration service is started
     * @param autostart flag for installation of auto start feature
     * @param smf flag for installing auto start feature as SMF service
     * @param st flag that determines if ST are available on host or not.
     * @param noST flag that determines if ST were enabled or disabled during installation.
     * @param pref system preferences type
     * @param simple determines if all components should be put into one jvm
     */
    public InstallMasterCommand(String adminUser, Map<String,Object> initSecParams, int csPort, boolean autostart, boolean smf, boolean st, PreferencesType pref, boolean noST, boolean simple) {
        this.adminUser = adminUser;
        this.initSecParams = initSecParams;
        this.csPort = csPort;
        this.autostart = autostart;
        this.smf = smf;
        this.st = st;
        this.preferencesType = pref;
        this.noST = noST;
        this.simple = simple;
        localhost = Hostname.getLocalHost().getHostname();
        createCommandSeqence();
    }
    /*
     * This enum is helper for getting proper jvm name for proper installation mode for components.
     * Currently we have 2 modes: normal (with cs_vm, rp_vm, executor_vm) and simple (cs_vm)
     */
    private enum Comp {
        
        spare_pool (BootstrapConstants.RP_VM, BootstrapConstants.CS_JVM),
        executor (BootstrapConstants.EXECUTOR_VM, BootstrapConstants.CS_JVM),
        reporter (BootstrapConstants.RP_VM, BootstrapConstants.CS_JVM),
        resource_provider (BootstrapConstants.RP_VM, BootstrapConstants.CS_JVM),
        ca (BootstrapConstants.EXECUTOR_VM, BootstrapConstants.CS_JVM);
        
        private String normal;
        private String simple;
        Comp(String normal, String simple) {
            this.normal = normal;
            this.simple = simple;
        }
        String getJvmName(boolean simple) {
            if (simple) {
                return this.simple;
            } else {
                return normal;
            }
            
        }
    }

    private void createCommandSeqence() {
        // create local spool directory structure
        CreateMasterHostDirsCommand cmhdCmd = new CreateMasterHostDirsCommand(adminUser);
        commands.add(cmhdCmd);
        
        // create initial global configuration (ca, executor)
        CreateGlobalConfigurationCommand cgcCmd = 
                new CreateGlobalConfigurationCommand(adminUser, csPort, simple);
        commands.add(cgcCmd);
        
        // create component config for executor
        ExecutorConfig exConf = new ExecutorConfig();
        
        AddComponentWithConfigurationCommand aeCmd = 
                AddComponentWithConfigurationCommand.newInstanceForMultiComponent()
                   .componentName(Comp.executor.toString())
                   .classname(ExecutorImpl.class.getName())
                   .config(exConf)
                   .hostname(".*")
                   .jvmName(Comp.executor.getJvmName(simple));
                
        commands.add(aeCmd);
        
        // create component config for resource provider
        ResourceProviderConfig rpConfig = new ResourceProviderConfig();
        PriorityPolicyManagerConfig ppmc = new PriorityPolicyManagerConfig();
        ppmc.setDefaultPriority(1);
        PriorityPolicyConfig pmc = new PriorityPolicyConfig();
        pmc.setName("spare_pool_priority");
        pmc.setService(Comp.spare_pool.toString());
        pmc.setValue(1);
        ppmc.getPriority().add(pmc);
        rpConfig.setPolicies(ppmc);
        
        AddComponentWithConfigurationCommand arpCmd = 
                AddComponentWithConfigurationCommand.newInstanceForSingleton()
                    .componentName(Comp.resource_provider.toString())
                    .classname(ResourceProviderImpl.class.getName())
                    .config(rpConfig)
                    .hostname(localhost)
                    .jvmName(Comp.resource_provider.getJvmName(simple));
        
        commands.add(arpCmd);
        
        //create component config for reporter
        ReporterConfig rcConfig = new ReporterConfig();
        
        AddComponentWithConfigurationCommand arcCmd = 
                AddComponentWithConfigurationCommand.newInstanceForSingleton()
                    .componentName(Comp.reporter.toString())
                    .classname(ReporterImpl.class.getName())
                    .config(rcConfig)
                    .hostname(localhost)
                    .jvmName(Comp.reporter.getJvmName(simple));
        
        commands.add(arcCmd);
        
        SparePoolServiceConfig spConfig = new SparePoolServiceConfig();
        // prepare SLO config
        PermanentRequestSLOConfig sloConf = new PermanentRequestSLOConfig();
        sloConf.setName("PermanentRequestSLO");
        sloConf.setUrgency(1);
        // create a condigtion for filtering the affected resources
        sloConf.setRequest(String.format("%s = \"%s\"", ResourceVariableResolver.TYPE_VAR, HostResourceType.HOST_TYPE));
        // assign slo to spare pool config
        SLOSet sloset = new SLOSet();
        sloset.getSlo().add(sloConf);
        spConfig.setSlos(sloset);
        
        AddComponentWithConfigurationCommand aspCmd =
                AddComponentWithConfigurationCommand.newInstanceForSingleton()
                    .componentName(Comp.spare_pool.toString())
                    .classname(SparePoolServiceImpl.class.getName())
                    .config(spConfig)
                    .hostname(localhost)
                    .jvmName(Comp.spare_pool.getJvmName(simple));
        
        commands.add(aspCmd);
                    
        // bootstrap security: init CA
        initSecParams.put("COMP_NAME",Comp.ca.toString());
        initSecParams.put("JVM_NAME", Comp.ca.getJvmName(simple));
        commands.add(new BootstrapSecurityCommandProxy(initSecParams));

        commands.add(new InstallVersionCommand());

        commands.add(new InstallLockFileCommand());
        // get the additional install master commands from the modules
        
        for(Module module: Modules.getModules()) {
            for(Command cmd: module.getMasterInstallExtensions()) {
                commands.add(cmd);
            }
        }
        
        // do final cleanup
        commands.add(new CheckMasterHostInstallationCommand(adminUser));
        if(Platform.getPlatform().isSuperUser() && autostart) {           
            if (smf) {
                commands.add(new InstallSMFCommand(BootstrapConstants.CS_JVM, preferencesType));
                if (!simple) {
                    commands.add(new InstallSMFCommand(BootstrapConstants.EXECUTOR_VM, preferencesType));
                    commands.add(new InstallSMFCommand(BootstrapConstants.RP_VM, preferencesType));
                }
            }
            else{
                commands.add(new InstallRcScriptCommand());
            }           
        }
        
        //Service Tags support
        if (noST) {
            commands.add(new SetNoSTCommand(preferencesType, noST));
        } else {
            if (st) {
                AddSTEntryCommand addSt = null;
                try {
                    addSt = new AddSTEntryCommand(preferencesType.toString());
                } catch (GrmException ex) {
                    log.log(Level.FINE, ex.getLocalizedMessage());
                }
                if (addSt != null) {
                    commands.add(addSt);
                }
            }
        } 
                

    }
    
    /**
     *  This command create the boostrap security command over the <code>SecurityFactory</code>
     *  and executes it.
     */
    public static class BootstrapSecurityCommandProxy extends AbstractLocalCommand<Void> {
        
        private final Map<String,Object> params;
        private Command<Void> executedCommand;
        
        /**
         * Creates a new <code>BootstrapSecurityCommandProxy</code>
         * @param params the parameters for the boostrap security command
         */
        public BootstrapSecurityCommandProxy(Map<String,Object> params) {
            this.params = params;
        }
        
        /**
         *  Creates the bootstrap security command and executes it.
         * 
         * @param env the execution env
         * @throws com.sun.grid.grm.GrmException if the boostrap security command
         *         can not be created of it it failed
         * @return void result
         */
        public Result<Void> execute(ExecutionEnv env) throws GrmException {
            
            SecurityModule secModule = Modules.getSecurityModule();
            Command<Void> cmd = secModule.createBootstrapCommand(env, params);

            Result<Void> ret = cmd.execute(env);

            executedCommand = cmd;
            return ret;
        }

        /**
         *  Calls the undo method of the bootstrap security command  if it was 
         *  executed.
         *
         *  @param env the execution env
         */
        @Override
        public void undo(ExecutionEnv env) {
            if(executedCommand != null) {
                executedCommand.undo(env);
            }
        }
    } 

}
