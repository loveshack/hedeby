/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.ExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.InvalidComponentNameException;
import com.sun.grid.grm.bootstrap.PreferencesAlreadyExistException;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.HostAndPort;
import com.sun.grid.grm.util.Hostname;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

/**
 * UI command to add bootstrap configuration for a system
 */
public class AddSystemCommand extends AbstractLocalCommand<Void> {
    /**
     * i18n Bundle name
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(AddSystemCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007062001;
    private final String sysName;
    private final String localSpoolPath;
    private final String distPath;
    private final String csURL;
    private final PreferencesType prefType;
    private boolean sslDisabled;
    private boolean autostart;
    private boolean master;
    private boolean undo;
    private boolean simple;
    

    /**
     * Adds bootstrap information for a new system.
     * @param sysName Name of the system which should be added
     * @param prefType preference location (user/sysem) where the bootstrap configuration 
     * should be stored
     * @param csURL String with format "<host>:<port>" containing information where to
     * find configuration service
     * @param localSpoolPath Path to the local spool directory of the system for this host
     * @param distPath Path to the distribution directory
     */
    public AddSystemCommand(String sysName, String csURL, String localSpoolPath, String distPath, PreferencesType prefType, boolean master) throws GrmException {
        if (sysName == null) {
           throw new GrmException("AddSystemCommand.need_system_name", BUNDLE);
        }
        if (csURL == null) {
           throw new GrmException("AddSystemCommand.need_cs_url", BUNDLE);
        }
        if (localSpoolPath == null) {
           throw new GrmException("AddSystemCommand.need_local_spool_path", BUNDLE);
        }
        if (distPath == null) {
           throw new GrmException("AddSystemCommand.need_dist_path", BUNDLE);
        }
        
        this.sysName = sysName;
        this.csURL = csURL;
        this.localSpoolPath = localSpoolPath;
        this.distPath = distPath;
        this.prefType = prefType;
        this.master = master;
    }

    /**
     * Execute add system bootstrap configuration
     * @param env execution environment
     * @return void
     * @throws com.sun.grid.grm.GrmException Thrown on any error
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        undo = false;
        try {
            SystemUtil.validateSystemName(sysName);
        } catch (InvalidComponentNameException ex) {
            throw new GrmException("AddSystemCommand.bad_system",
                    ex, BUNDLE, sysName);
        }
        
        // make sure beforehand that the install is valid
        if (PreferencesUtil.isHost(sysName, prefType, Hostname.getLocalHost().getHostname())) {
            if (!master && PreferencesUtil.isMasterHost(sysName, prefType, Hostname.getLocalHost().getHostname())) {
                // give special error message for managed host install on master host
                throw new GrmException("AddSystemCommand.cannot_inst_managed_on_master",
                    BUNDLE, sysName, prefType, Hostname.getLocalHost().getHostname());  
            }
            throw new GrmException("AddSystemCommand.system_already_exists", BUNDLE, sysName);
        }
      
        HostAndPort csHostAndPort = null;
        try {
            csHostAndPort = HostAndPort.newInstance(csURL);
            
            if(csHostAndPort.getHost() == null) {
                throw new GrmException("AddSystemCommand.invalid_cs_host", BUNDLE, csURL);
                
            }
            if(csHostAndPort.getPort() <= 0) {
                throw new GrmException("AddSystemCommand.invalid_cs_port", BUNDLE, csHostAndPort.getPort());
            }
            if(csHostAndPort.getPort() >= 65535 ) {
                throw new GrmException("AddSystemCommand.invalid_cs_port", BUNDLE, csHostAndPort.getPort());
            }
            
        } catch (NumberFormatException ex) {
            throw new GrmException("AddSystemCommand.invalid_cs_url", BUNDLE, csURL);
        }
            

        File dist = new File(distPath);
        File localSpool = new File(localSpoolPath);
        
        if (prefType != PreferencesType.SYSTEM &&
                prefType != PreferencesType.USER)  {
            throw new GrmException("AddSystemCommand.pref_type_not_expected", BUNDLE,
                    new Object[]{  PreferencesType.SYSTEM, PreferencesType.USER});
        }
        try {
        
            Map<String,Object> properties = new HashMap<String, Object>(2);
            properties.put(ExecutionEnv.NO_SSL, isSslDisabled());
            properties.put(ExecutionEnv.AUTO_START, isAutostart());
            if (master) {
                properties.put(ExecutionEnv.SIMPLE, isSimple());
            }
            ExecutionEnv newEnv = ExecutionEnvFactory.newInstance(
                    sysName, localSpool, dist,
                    csHostAndPort.getHost(), csHostAndPort.getPort(), 
                    properties);
            undo = true;
            if (master) {
                PreferencesUtil.addSystem(newEnv, prefType);
            } else {
                PreferencesUtil.installHost(newEnv, prefType);
            }
            
            log.log(Level.INFO, "AddSystemCommand.config_added", sysName);
        } catch (PreferencesAlreadyExistException ex) {
            this.undo(env);
            throw ex;
        } catch (BackingStoreException ex) {
            this.undo(env);
            throw new GrmException("AddSystemCommand.cannot_write_config", ex,
                    BUNDLE, ex.getLocalizedMessage());
        }
        
            
        
        return CommandResult.VOID_RESULT;
    }
    @Override
    public void undo(ExecutionEnv env) {
        if (undo) {
            try {
                if (master) {
                    PreferencesUtil.deleteSystem(sysName, prefType);
                } else {
                    PreferencesUtil.uninstallHost(sysName, prefType);
                }
            } catch (PreferencesNotAvailableException ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "AddSystemCommand.undo.failed");
                    lr.setThrown(ex);
                    log.log(lr);
                }
            } catch (BackingStoreException ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "AddSystemCommand.undo.failed");
                    lr.setThrown(ex);
                    log.log(lr);
                }
            }
            undo = false;
        }
    }

    /**
     *  is ssl disabled for this system
     *  @return <code>true</code> 
     */
    public boolean isSslDisabled() {
        return sslDisabled;
    }

    
    /**
     * enabled/disable ssl for the system
     * @param sslDisabled 
     */
    public void setSslDisabled(boolean sslDisabled) {
        this.sslDisabled = sslDisabled;
    }

    public boolean isAutostart() {
        return autostart;
    }

    public void setAutostart(boolean autostart) {
        this.autostart = autostart;
    }
    
    public boolean isSimple() {
        return simple;
    }

    public void setSimple(boolean simple) {
        this.simple = simple;
    }
}
