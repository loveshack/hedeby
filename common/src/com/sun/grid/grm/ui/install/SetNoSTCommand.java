/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.prefs.BackingStoreException;

/**
 *
 * This command set the property in host preferences with the iformation 
 * if ST were disabled during the installation process. 
 */
public class SetNoSTCommand extends AbstractLocalCommand<Void> {
    
    private final PreferencesType prefs;
    private final boolean noST;
    
    /**
     * Constructor for this command
     * @param prefs system preferences type
     * @param noST information if ST were disabled or enabled during installation process
     */
    public SetNoSTCommand(PreferencesType prefs, boolean noST) {
        this.noST = noST;
        this.prefs = prefs;
    }
    
    /**
     * This method executes this command. It tries to put new property value
     * to the host preferences property file.
     * @param env execution env variable
     * @return nothing
     * @throws com.sun.grid.grm.GrmException
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        try {
            PreferencesUtil.setNoST(env.getSystemName(), prefs, noST);
        } catch (BackingStoreException ex) {
            throw new GrmException(ex.getLocalizedMessage(), ex);
        }
        return CommandResult.VOID_RESULT;
    }
}
