/*___INFO__MARK_BEGIN__*/
 /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Platform;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * This is local command for installing the support for rc scripts on host.
 * Using script $hedeby_dist/bin/supportRc.sh
 * /etc/sdm/sdm.env
 * exist on the system, they are overwritten
 * Rc script support for windows host is not supported
 */
 
public class InstallRcScriptCommand extends AbstractLocalCommand<Void> {
    private static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(InstallRcScriptCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007121801;
    //template should be in $DIST/util/templates
    private static String env_file = "sdm.env";
    //path: /etc/sdm/ for storing file with variables
    private static File distEnv = new File(File.separatorChar+"etc"+File.separatorChar+"sdm");
    private List<File> undoRemove = new LinkedList<File>();
    private Map<File, File> undoCopy = new HashMap<File, File>();
    private List<File> undoRemoveDir = new LinkedList<File>();
    boolean success=false;
    /**
     * Creates a new instance of InstallRcScriptCommand
     */
    public InstallRcScriptCommand() {
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        String previousContent = "";
        
        undoRemove.clear();
        undoCopy.clear();
        undoRemoveDir.clear();
        success=false;
        //Template file for environmental variables used by rc script 
        File tempEnv = new File(env.getDistDir(), "util"+File.separatorChar+"templates"+
                File.separatorChar+"sdm.env.template");
        File tmpUndo = Platform.getPlatform().createTmpDir(null, null);
        
        //We dont support Rc script installation on Windows OS
        if (Platform.isWindowsOs()) {
            throw new UnsupportedOperationException("Windows OS rc script is not supported");
        }
        //Installation of Rc scripts should be done as superuser
         if (!Platform.getPlatform().isSuperUser()) {
            throw new GrmException("InstallRcScriptCommand.error.not_super_user", BUNDLE);
         }
        //check whether template hedeby.env exists
        if (!tempEnv.exists()) {
            new GrmException("InstallRcScriptCommand.error.template_file_doesnt_exist", BUNDLE, tempEnv.getAbsolutePath());
        }
        
        //location of rc script
        File tempSvcgrm = new File(env.getDistDir(), "util"+File.separatorChar+"templates"+File.separatorChar+"sdmsvc");
        //check whether script exists
        if (!tempSvcgrm.exists()) {
            new GrmException("InstallRcScriptCommand.error.template_file_doesnt_exist", BUNDLE, tempSvcgrm.getAbsolutePath());
        }
        
        //destination location where hedeby.env file will be placed
        File tmpDistEnv = new File(distEnv, env_file);
        if (!distEnv.exists()) {
            if (!distEnv.mkdirs()) {
                throw new GrmException("InstallRcScriptCommand.error.directory_for_hedeby_env_not_created", BUNDLE, distEnv);
            }
            undoRemoveDir.add(distEnv);
        }
        if (tmpDistEnv.exists()) {
            try {
                //file will be overwritten
                File t = new File(tmpUndo, String.valueOf(Calendar.getInstance().getTimeInMillis()));
                Platform.getPlatform().copy(tmpDistEnv, t);
                undoCopy.put(t, tmpDistEnv);
                previousContent=readSdmEnvContent(t);
            } catch (Exception ex) {
                throw new GrmException("InstallRcScriptCommand.error.storing_file_for_undo_failed", ex, BUNDLE, tmpDistEnv.getAbsolutePath());
            } 
            
        } 

        //pattern for modifying template file hedeby.env
        Map<String, String> patterns = new HashMap<String, String>(1);
                
        patterns.put("@@@SDM_DIST@@@", env.getDistDir().getAbsolutePath());
        patterns.put("@@@JAVA_HOME@@@", System.getProperty("java.home"));
        patterns.put("@@@PREVIOUS_CONTENT@@@", previousContent);
        
        int res = -1;
        String command = env.getDistDir().getAbsolutePath()+"/util/supportRc.sh install "+env.getDistDir().getAbsolutePath();
        try {                
            FileUtil.replace(tempEnv, tmpDistEnv, patterns);
            undoRemove.add(tmpDistEnv);
            res = Platform.getPlatform().exec(command, null, PathUtil.getTmpDir(), null, null, 0);
            //from this point we try to uninstall
            success=true;
        } catch (Exception ex) {
            this.undo(env);
            throw new GrmException("InstallRcScriptCommand.error.failed", ex, BUNDLE, ex.getLocalizedMessage());
        }
        if (res != 0) {
            this.undo(env);
            throw new GrmException("InstallRcScriptCommand.error.command_execution_error_exit", BUNDLE, command, res);
        }
        
        return CommandResult.VOID_RESULT;
    }
    @Override
    public void undo(ExecutionEnv env) {
        for (File f : undoRemove) {
            f.delete();
        }
        undoRemove.clear();
        for (File src : undoCopy.keySet()) {
            try {
                Platform.getPlatform().copy(src, undoCopy.get(src));
            } catch (Exception ex) {
                //log error - while undo
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "InstallRcScriptCommand.undo.restorefilefailed");
                    lr.setParameters(new Object[] {src.getAbsolutePath(), undoCopy.get(src).getAbsolutePath()});
                    lr.setThrown(ex);
                    log.log(lr);
                }
                return;
            } 
        }
        undoCopy.clear();
        for (File f : undoRemoveDir) {
            try {
                Platform.getPlatform().removeDir(f, true);
            } catch (Exception ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "InstallRcScriptCommandCommand.undo.removeDir");
                    lr.setParameters(new Object[] {f.getAbsolutePath()});
                    lr.setThrown(ex);
                    log.log(lr);
                }
                return;
            } 
        }
        if (success) {
            success=false;
            //was uninstalled we can try installtion using script
            int res = -1;
            String command = env.getDistDir().getAbsolutePath()+"/util/supportRc.sh uninstall "+env.getDistDir().getAbsolutePath();
            try {
                res = Platform.getPlatform().exec(command, null, PathUtil.getTmpDir(), null, null, 0);
                //from here we dont know what can fail - reinstall for undo
                success=true;
            } catch (Exception ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "InstallRcScriptCommand.undo.execution_reinstall_failed");
                    lr.setParameters(new Object[] {command});
                    lr.setThrown(ex);
                    log.log(lr);
                }
                return;
            }
            if (res != 0) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "InstallRcScriptCommand.undo.reinstall_failed");
                    lr.setParameters(new Object[] {command, res});
                    log.log(lr);
                }
                return;
            }
        }
    }

    private String readSdmEnvContent(File file) {
        String START = "#@@@START@@@";
        String END = "#@@@END@@@";
        
        boolean begin = false;
        boolean end = false;
        StringBuilder sb = new StringBuilder();
        try {
            FileReader in = new FileReader(file);
            BufferedReader buf = new BufferedReader(in);
            String resp = buf.readLine();
            while (resp != null) {
                if (!begin) {
                    if (resp.equals(START)) {
                        begin = true;
                    }
                } else if (!resp.equals(END)) {
                    sb.append(resp);
                    sb.append('\n');
                } else {
                    break;
                }
                
                resp = buf.readLine();
            }
        } catch (FileNotFoundException ex) {
            //not possible because of checks already before, but who knows 
            //we continue and treat existing sdm.env as invalid, log message and return content as empty
            if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "InstallRcScriptCommand.existing_sdmfile_disappeared");
                    lr.setParameters(new Object[] {file.getAbsolutePath()});
                    lr.setThrown(ex);
                    log.log(lr);
            }
            return "";
        } catch (IOException ex) {
            //we continue and treat existing sdm.env as invalid, log message and return content as empty
            if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "InstallRcScriptCommand.existing_sdmfile_corrupted");
                    lr.setParameters(new Object[] {file.getAbsolutePath()});
                    lr.setThrown(ex);
                    log.log(lr);
            }
            return "";
        }       
        //if START-END marks not found in file the empty string will be returned
        return sb.toString();
    }
}
