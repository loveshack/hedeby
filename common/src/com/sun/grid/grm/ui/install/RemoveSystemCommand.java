/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.InvalidComponentNameException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

/**
 * UI command to add bootstrap configuration for a system
 */
public class RemoveSystemCommand extends AbstractLocalCommand<Void> {
    /**
     * i18n Bundle name
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(RemoveSystemCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007062001;
    private final PreferencesType prefType;
    private boolean unset;

    
    /**
     * Remove a system from the properties
     * @param prefType system or user property specification
     */
    public RemoveSystemCommand(PreferencesType prefType) {
        this.prefType = prefType;
    }

    /**
     * Execute the command
     * @param env Execution environment where the command should run
     * @throws com.sun.grid.grm.GrmException on errors
     * @return void
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        String sysName = env.getSystemName();
        unset = false;
        if (sysName == null) {
            this.undo(env);
            throw new GrmException("GlobalCommandMessages.need_system", BUNDLE);
        }
        
        try {
            SystemUtil.validateSystemName(sysName);
        } catch (InvalidComponentNameException ex) {
            this.undo(env);
            throw new GrmException(ex.getLocalizedMessage(), ex, BUNDLE);
        }
        
        try {
            boolean inSystemPrefs = PreferencesUtil.existsSystem(sysName, PreferencesType.SYSTEM);
            boolean inUserPrefs = PreferencesUtil.existsSystem(sysName, PreferencesType.USER);
           
            // systemName is defined SYSTEM and USER and -p switch was not used
            //  or
            // systemName is defined in SYSTEM AND USER
            //  or
            // ...
            if (inSystemPrefs && inUserPrefs && prefType == PreferencesType.SYSTEM_PROPERTIES) {
                this.undo(env);
                throw new GrmException("RemoveSystemCommand.ambiguous_system", BUNDLE, sysName);
            } else if (inSystemPrefs || inUserPrefs) {
                String defaultSystem = PreferencesUtil.getDefaultSystemFromPrefs();
                if(defaultSystem != null && defaultSystem.equals(sysName)) {
                    // We have to unset the default system because we want to remove it
                    UnsetDefaultSystemCommand uiCmd = new UnsetDefaultSystemCommand(prefType);
                    uiCmd.execute(env);
                    unset = true;
                }
                if (SystemUtil.isCSStarted(env)) {
                    this.undo(env);
                    throw new GrmException("RemoveSystemCommand.system_running", BUNDLE, sysName);
                }
                if (!PreferencesUtil.deleteSystem(sysName, prefType)) {
                    this.undo(env);
                    throw new GrmException("RemoveSystemCommand.config_dne", BUNDLE, sysName);
                }
                log.log(Level.INFO, "RemoveSystemCommand.config_removed", sysName);
            } else {
                this.undo(env);
                throw new GrmException("RemoveSystemCommand.system_not_found", BUNDLE, sysName);
            }
        } catch (BackingStoreException ex) {
            this.undo(env);
            throw new GrmException("RemoveSystemCommand.cannot_write_config", ex, BUNDLE, ex.getLocalizedMessage());
        }

        return CommandResult.VOID_RESULT;
    }
    @Override
    public void undo(ExecutionEnv env) {       
        //We need to undo unsetting of default system on any error
        try {
            if (unset) {
                SetDefaultSystemCommand uiCmd = new SetDefaultSystemCommand();
                uiCmd.execute(env);
                unset = false;
            }
        } catch (GrmException ex) {
            log.logrb(Level.WARNING, RemoveSystemCommand.class.getName(), "undo", BUNDLE, "ui.command.undofailed", ex);
        }
    }
}
