/*___INFO__MARK_BEGIN__*/
 /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.GrmXMLException;
import com.sun.grid.grm.config.common.VersionString;
import com.sun.grid.grm.config.naming.FileCache;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;

/**
 * Install binaryVersion information on host, creates binaryVersion file in localspool/upgrade dir
 * This is installation command. Version string is taken from binaries (sdm-upgrade-impl.jar).
 * Setter methods allows to override default version string.
 */
public class InstallVersionCommand extends AbstractLocalCommand<Void> {

    public static final String BUNDLE_NAME = "com.sun.grid.grm.ui.install.messages";
    private boolean undo = false;
    File versionfile;
    //Overriding version
    private String version = null;
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
  
        //binaryVersion of binaries
        String binaryVersion;
        if (version == null) {
            binaryVersion = SystemUtil.getBinaryVersion(env);
        } else {
            binaryVersion = version;
        }
        if (!env.getCSHost().equals(Hostname.getLocalHost())) {
            String systemVersion = SystemUtil.getSystemVersion(env);
            //check if managed and if binaryVersion of master is proper then
            if (!binaryVersion.equals(systemVersion)) {
                throw new GrmException("ivc.error.wrongversion", BUNDLE_NAME, binaryVersion, systemVersion);
            }
        }
        versionfile = PathUtil.getHostVersionFilePath(env);
        try {
            VersionString v = new VersionString();
            v.setVersion(binaryVersion);
            FileCache.getInstance(versionfile).write(v);
            undo = true;
            Platform.getPlatform().chown(versionfile, Platform.getPlatform().getFileOwner(env.getLocalSpoolDir()));
        } catch (GrmXMLException ex) {
            throw new GrmException("ivc.error.cannotwriteversion", ex, BUNDLE_NAME, versionfile.getAbsolutePath(), ex.getLocalizedMessage());
        } catch (IOException ex) {
            throw new GrmException("ivc.error.cannotwriteversion", ex, BUNDLE_NAME, versionfile.getAbsolutePath(), ex.getLocalizedMessage());
        } catch (InterruptedException ex) {
            throw new GrmException("ivc.error.cannotwriteversion", ex, BUNDLE_NAME, versionfile.getAbsolutePath(), ex.getLocalizedMessage());
        }
 
        return new CommandResult<Void>();
    }
    @Override
    public void undo(ExecutionEnv env) {
        if (undo) {
            versionfile.delete();
            undo = false;
        }

    }

    /**
     * Setter allowing to override binaryVersion
     * @param version the version to set
     */
    public void setVersion(String version) {
        this.version = version;
    }

}
