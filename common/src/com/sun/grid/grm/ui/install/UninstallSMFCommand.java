/*___INFO__MARK_BEGIN__*/
 /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.PreferencesNotAvailableException;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

/**
 * This is uninstallation command for removing SMF support from hedeby system.
 * SMF services in hedeby are for specific JVMs of the system.
 * To perform uninstallation the script located in:
 * $hedeby_dist/bin/sdm_smf.sh is neccessary.
 * The script provides such commands like: register for installation of service, and
 * unregister for uninstallation of service from machine.
 * During the uninstallation of SMF service the entry in preferences is removed.
 * Entry contains pair: jvmName and FMRI name of service. The name of service is used by
 * script to remove SMF service from system.
 * This command is able to remove both online and maintenance SMF services. Of course the
 * operation of disabling is performed before that.
 */

public class UninstallSMFCommand extends AbstractLocalCommand<Void> {
    
    private static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final long serialVersionUID = 2007121801;
    private static final Logger log = Logger.getLogger(UninstallSMFCommand.class.getName(), BUNDLE);
    private String jvmName;
    private PreferencesType prefs;
    private byte[] snapshot;
    private File undoTmp;
    private boolean undoCommand = false;
    /** Creates a new instance of UninstallSMFCommand */
    public UninstallSMFCommand(String jvmName, PreferencesType prefs) {
        this.setJvmName(jvmName);
        this.prefs = prefs;
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        undoCommand = false;
        snapshot = null;
        try {
            if (!PreferencesUtil.hasSMFSupport(env.getSystemName(), prefs, getJvmName())) {
                this.undo(env);
                throw new GrmException("UninstallSMFSupportCommand.error.smf_info_not_present", BUNDLE, getJvmName());
            }
        } catch (BackingStoreException ex) {
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.cannot_read_prefs", ex, BUNDLE);
        }
        CheckSMFSupportCommand cmd = new CheckSMFSupportCommand();
        boolean smf = env.getCommandService().<Boolean>execute(cmd).getReturnValue();
        if (!smf) {
            //SMF not supported
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.SMFnotsupported", BUNDLE);
        }
        
        //before modifying preferences export for undo
        try {
            snapshot = PreferencesUtil.exportSystemPreferences(prefs, env.getSystemName());
        } catch (Exception ex) {
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.preferences_not_exported", ex, BUNDLE);
        }
        
        String serviceName;
        try {
            //get info about service name - SMF FMRI
            serviceName = PreferencesUtil.getSMFService(env.getSystemName(), prefs, getJvmName());
            //uninstall from prefs
            PreferencesUtil.uninstallSMFKey(env.getSystemName(), prefs, getJvmName());
        } catch (PreferencesNotAvailableException ex) {
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.smf_info_not_present", ex, BUNDLE, getJvmName());
        } catch (BackingStoreException ex) {
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.failed_prefs_operation", ex, BUNDLE);
        }
        int exit = -1;
        
        // before uninstalling smf service we can export manifest
        undoTmp = Platform.getPlatform().createTmpDir(null, null);

        undoTmp = new File(undoTmp, "tmp");
        //get manifest of service
        String service = SystemUtil.getSMFName(env.getSystemName(), getJvmName());
        String tmpCommand = "/usr/sbin/svccfg export "+service+" > "+undoTmp.getAbsolutePath();
        try {
            exit = Platform.getPlatform().exec(tmpCommand, null, PathUtil.getTmpDir(), null, null, 0);
        } catch (Exception ex) {
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.failed_to_execute_command", ex, BUNDLE, tmpCommand);
        }
        if (exit != 0) {
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.command_error", BUNDLE, tmpCommand, exit);
        }
        
        //we are prepared for undo'ing the script call
        File unInstallScript = new File(env.getDistDir(),"util"+File.separator+"sdmsmf.sh");
        if (!unInstallScript.isFile()) {
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.script_not_found", BUNDLE, unInstallScript.getAbsolutePath());
        }
        String command = unInstallScript.getAbsolutePath()+" unregister "+serviceName;
        exit = -1;
        try {
            exit = Platform.getPlatform().exec(command, null, PathUtil.getTmpDir(), null, null, 0);
        } catch (Exception ex) {
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.failed_to_execute_command", ex, BUNDLE, command);
        }
        if (exit != 0) {
            this.undo(env);
            throw new GrmException("UninstallSMFSupportCommand.error.command_error", BUNDLE, command, exit);
        }
        //undo for unregistering service should be done
        undoCommand = true;
        return CommandResult.VOID_RESULT;
    }
    
    @Override
    public void undo(ExecutionEnv env) {        
        if (snapshot!=null) {
            try {
                PreferencesUtil.importSystemPreferences(prefs, env.getSystemName(), snapshot);
            } catch (Exception ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "UninstallSMFCommand.undo.error_import_preferences");
                    lr.setThrown(ex);
                    log.log(lr);
                }
            } finally {
                snapshot = null;
            }
            
            if (undoCommand) {
                //we have manifest ready so just register service
                //Command for installing smf
                File installScript = new File(env.getDistDir(),"util"+File.separator+"sdmsmf.sh");
                if (!installScript.isFile()) {
                    if (log.isLoggable(Level.WARNING)) {
                        LogRecord lr = new LogRecord(Level.WARNING, "UninstallSMFCommand.undo.missing_script");
                        lr.setParameters(new Object[]{installScript.getAbsolutePath()});
                        log.log(lr);
                    }
                }
                String command = installScript.getAbsolutePath()+" register "+undoTmp.getAbsolutePath();
                int exit = -1;
                try {
                    exit = Platform.getPlatform().exec(command, null, PathUtil.getTmpDir(), null, null, 0);
                } catch (Exception ex) {
                    if (log.isLoggable(Level.WARNING)) {
                        LogRecord lr = new LogRecord(Level.WARNING, "UninstallSMFCommand.undo.script_execution_exp");
                        lr.setParameters(new Object[]{command});
                        log.log(lr);
                    }
                }
                if (exit != 0) {
                    if (log.isLoggable(Level.WARNING)) {
                        LogRecord lr = new LogRecord(Level.WARNING, "UninstallSMFCommand.undo.script_not_success");
                        lr.setParameters(new Object[]{command, exit});
                        log.log(lr);
                    }
                }
            }
        }
        undoCommand=false;
    }

    /**
     * Getter for jvmName
     * @return name of jvm for which SMF is installed
     */
    public String getJvmName() {
        return jvmName;
    }

    /**
     * Setter for jvmName
     * @param jvmName name of jvm for which SMF is installed
     */
    public void setJvmName(String jvmName) {
        this.jvmName = jvmName;
    }
}
