/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  Abstract base class for all commands which creates directories.
 *
 *  <p>All directories which has been created with the <code>mkdir</code> method
 *  will be deleted if the <code>undo</code> method is called.</p>
 *
 *  <p>In the constructor of the command the owner of the directories can be
 *  specified.</p>
 */
public abstract class AbstractCreateDirectoriesCommand extends AbstractLocalCommand<Void> {
    
    private final static String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    
    private final LinkedList<File> createdDirs = new LinkedList<File>();
   
    private final static Logger log = Logger.getLogger(AbstractCreateDirectoriesCommand.class.getName(), BUNDLE);
    
    private final String owner;
    
    /** 
     *  Creates a new instance of CreateSecurityDirectories. 
     *
     *  @param owner  the owner of the directories
     */
    public AbstractCreateDirectoriesCommand(String owner) {
        this.owner = owner;
    }
    
    /**
     * Create a directory.
     *
     * @param dir  the directory
     * @throws com.sun.grid.grm.GrmException  if the directory can not be created
     */
    protected void mkdir(File dir) throws GrmException {
        if (dir.isDirectory()) {
           throw new GrmException("AbstractCreateDirectoriesCommand.dirAlreadyExisting", BUNDLE, dir );
        }
        if(!dir.mkdir()) {            
            throw new GrmException("AbstractCreateDirectoriesCommand.error.mkDir", BUNDLE, dir );
        }
        createdDirs.add(dir);
        try {
            Platform.getPlatform().chown(dir, getOwner());
        } catch(Exception ex) {
            throw new GrmException("AbstractCreateDirectoriesCommand.error.chown", ex, BUNDLE, dir, ex.getLocalizedMessage());
        }
        log.log(Level.FINE, "AbstractCreateDirectoriesCommand.dirCreated", dir);
    }

    /**
     * Deletes all previous created directories.
     *
     * @param env the execution env
     */
    @Override
    public void undo(ExecutionEnv env) {
        while(!createdDirs.isEmpty()) {
            File dir = createdDirs.removeLast();
            try {
                Platform.getPlatform().removeDir(dir, true);
            } catch(Exception ex) {
                log.log(Level.WARNING, "AbstractCreateDirectoriesCommand.warn.rmDir", 
                        new Object [] { dir, ex.getLocalizedMessage() });
            }
        }
    }

    /**
     * Get the owner.
     * @return  the owner
     */
    public String getOwner() {
        return owner;
    }
    
}
