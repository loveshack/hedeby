/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;

/**
 *
 * This command is checking if support for the Service Tags exists on the host.
 * 
 */
public class CheckSTSupportCommand extends AbstractLocalCommand<Boolean> {
    
    /**
     * This method checks wheter ST are supported on this host or not.
     * We are ignoring all the errors that comes from that command.
     * @param env execution enviroment
     * @return true if ST are supported, otherwise false
     * @throws com.sun.grid.grm.GrmException
     */
    @SuppressWarnings("static-access")
    public Result<Boolean> execute(ExecutionEnv env) throws GrmException {
        Boolean ret = Boolean.valueOf(true);
        //TODO: check future releases of ST. Windows ST support is planned.
        if (Platform.getPlatform().isWindowsOs()) {
            ret = Boolean.valueOf(false);
        } else {
            //creating string representation for path to the script ($SDM_DIST/util/sdmST/sdm_st)
            StringBuilder sb = new StringBuilder();
            sb.append(env.getDistDir());
            sb.append(File.separator);
            sb.append("util");
            sb.append(File.separator);
            sb.append("sdmST");
            sb.append(File.separator);
            sb.append("sdm_st");
            File script = new File(sb.toString());
            //preparing the command for checking if ST are available on the host
            sb = new StringBuilder();
            sb.append(script.getAbsolutePath());
            sb.append(" supported ");
            sb.append(env.getDistDir().getAbsolutePath());
            sb.append(" ");
            sb.append(env.getSystemName());
            int exit_code = 1;
            try {
                exit_code = Platform.getPlatform().exec(sb.toString(), null, PathUtil.getTmpDir(), null, null, 0);
            } catch (Exception ex) {
                //ignore and assume that ST are not supported
            }
            if (exit_code == 1) {
                ret = Boolean.valueOf(false);
            }
        }
        CommandResult<Boolean> res = new CommandResult<Boolean>();
        res.setReturnValue(ret);
        return res;
    }
        
}
