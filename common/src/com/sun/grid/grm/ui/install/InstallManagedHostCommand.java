/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;


import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.security.SecurityModule;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.LocalCommand;
import com.sun.grid.grm.ui.MacroCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.util.Platform;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Command for installing the hedeby managed host components.
 * Before starting this command the system bootsrap information
 * have to be set. This can be done with the AddSystemCommand.
 */
public class InstallManagedHostCommand extends MacroCommand<Void> implements LocalCommand<Void> {
    /**
     * i18n Bundle name
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(InstallManagedHostCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007062001;
    
    /**
     * Command for installing the hedeby managed host components.
     * Before starting this command the system bootsrap information
     * have to be set. This can be done with the AddSystemCommand.
     * @param adminUser Name of the admin user
     * @param autostart flag for installation of auto start feature
     * @param smf flag for installing auto start feature as SMF service
     * @param simple flag if true installs all components in one jvm
     */
    public InstallManagedHostCommand(String adminUser, boolean autostart, boolean smf, boolean simple, PreferencesType prefs) {
        
        
        
        // create local spool directory structure
        CreateManagedHostDirsCommand cmhdCmd = new CreateManagedHostDirsCommand(adminUser);
        commands.add(cmhdCmd);
      
        // bootstrapping security for this managed host
        commands.add(new RemoteInstSecurityCommandProxy(adminUser, prefs));
        //install version and verify if version of managed host if the same as version of master
        commands.add(new InstallVersionCommand());

        commands.add(new InstallLockFileCommand());
        
        if(Platform.getPlatform().isSuperUser() && autostart) {           
            if (smf) {
                if (!simple) {
                    commands.add(new InstallSMFCommand(BootstrapConstants.EXECUTOR_VM, prefs));
                    commands.add(new InstallSMFCommand(BootstrapConstants.RP_VM, prefs));
                } else {
                    commands.add(new InstallSMFCommand(BootstrapConstants.CS_JVM, prefs));
                }
            }
            else{
                commands.add(new InstallRcScriptCommand());
            }           
        }
    }
    
    private static class RemoteInstSecurityCommandProxy extends AbstractLocalCommand<Void> {
        
        
        private Command<Void> executedCommand;
        private final String adminUser;
        private final PreferencesType prefs;
        
        public RemoteInstSecurityCommandProxy(String adminUser, PreferencesType prefs) {
            this.adminUser = adminUser;
            this.prefs = prefs;
        }
        /**
         * Creates a new <code>BootstrapSecurityCommandProxy</code>
        /**
         *  Creates the remote inst security command and executes it.
         * 
         * @param env the execution env
         * @throws com.sun.grid.grm.GrmException if the remote inst security command
         *         can not be created of it it failed
         * @return void result
         */
        public Result<Void> execute(ExecutionEnv env) throws GrmException {
            
            SecurityModule secModule = Modules.getSecurityModule();
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("ADMIN_USER", adminUser);
            params.put("PREFS_TYPE", prefs);
            Command<Void> cmd = secModule.createRemoteInstCommand(env, params);

            Result<Void> ret = cmd.execute(env);

            executedCommand = cmd;

            return ret;
        }

        /**
         *  Calls the undo method of the bootstrap security command  if it was 
         *  executed.
         *
         *  @param env the execution env
         */
        @Override
        public void undo(ExecutionEnv env) {
            if(executedCommand != null) {
                executedCommand.undo(env);
            }
        }
        
    }
}
