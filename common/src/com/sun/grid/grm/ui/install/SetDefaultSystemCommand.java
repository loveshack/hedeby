/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

/**
 * Set the default system name in the preferences
 */
public class SetDefaultSystemCommand extends AbstractLocalCommand<Void> {
    /**
     * i18n Bundle name
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(SetDefaultSystemCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007062001;
    
    
    /** Creates a new instance of SetSystemPropertyCommand */
    public SetDefaultSystemCommand() {
    }

    /**
     * Execute the command
     * @param env Execution environment where the command should run
     * @return void
     * @throws com.sun.grid.grm.GrmException on errors
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        if (env.getSystemName() == null) {
            this.undo(env);
            throw new GrmException("GlobalCommandMessages.need_system", BUNDLE);
        }
        try {
            PreferencesUtil.setDefaultSystem(env.getSystemName());
            log.log(Level.INFO, "SetDefaultSystemCommand.default_system_set", env.getSystemName());
        } catch (BackingStoreException ex) {
            this.undo(env);
            throw new GrmException("SetDefaultSystemCommand.bad_prefs", ex, BUNDLE, ex.getLocalizedMessage());
        }
        return CommandResult.VOID_RESULT;
    }
}
