/*___INFO__MARK_BEGIN__*/
 /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;

/**
 * Create lock file in localspool, set proper owner and permissions
 */
public class InstallLockFileCommand extends AbstractLocalCommand<Void> {

    private static final String BUNDLE_NAME = "com.sun.grid.grm.ui.install.messages";
    private static final String FILE_PERM = "644";
    private File undo = null;

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        File lock = PathUtil.getLocalSpoolLockFilePath(env);
        try {
        if (lock.createNewFile()) {
            undo = lock;
            Platform.getPlatform().chmod(lock, FILE_PERM);
            Platform.getPlatform().chown(lock, Platform.getPlatform().getFileOwner(env.getLocalSpoolDir()));
        }
        } catch (IOException ex) {
            throw new GrmException("ilfc.error.lockfilecreationfailed", BUNDLE_NAME, ex, lock.getAbsolutePath());
        } catch (InterruptedException ex) {
            throw new GrmException("ilfc.error.lockfilecreationfailed", BUNDLE_NAME, ex, lock.getAbsolutePath());
        }
        return CommandResult.VOID_RESULT;
    }

    @Override
    public void undo(ExecutionEnv env) {
        if (undo != null) {
            undo.delete();
            undo = null;
        }
    }

}
