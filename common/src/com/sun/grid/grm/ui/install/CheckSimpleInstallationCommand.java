/*___INFO__MARK_BEGIN__*/
 /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.Map;
import java.util.logging.Logger;

/**
 * System command to check if installation is simple
 */
public class CheckSimpleInstallationCommand extends AbstractSystemCommand<Boolean> {
    private static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(CheckSimpleInstallationCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2009030301;
    private PreferencesType prefs;
    /**
     * 
     * Creates a new instance of CheckSimpleInstallationCommand
     * @param PreferencesType of the checked system
     */
    public CheckSimpleInstallationCommand(PreferencesType prefs) {
        this.prefs = prefs;
    }
    
    /**
     * Retrieve information about installed SMF service from preferences.
     * @param env execution Env for system
     * @return true is master host was installed with -simple_install option
     * @throws com.sun.grid.grm.GrmException - if any error occurred
     */
    public Result<Boolean> execute(ExecutionEnv env) throws GrmException {
        
        //SMF supported host - retrieve info about installed hedeby smf services
        CommandResult<Boolean> ret = new CommandResult<Boolean>();
        
        ret.setReturnValue(new Boolean(PreferencesUtil.isSimple(env.getSystemName(), prefs)));
        
        
        return ret;
    }
    
}
