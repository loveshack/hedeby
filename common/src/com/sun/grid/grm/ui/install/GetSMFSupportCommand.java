/*___INFO__MARK_BEGIN__*/
 /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

/**
 * Local command for retrieving the information about installed SMF services for
 * current system.
 */
public class GetSMFSupportCommand extends AbstractLocalCommand<Map<String, String>> {
    private static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(GetSMFSupportCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007121801;
    private PreferencesType prefs;
    
    /** 
     * Creates a new instance of GetSMFSupportCommand 
     * @param prefs type of preferences for system
     */
    public GetSMFSupportCommand(PreferencesType prefs) {
        this.prefs = prefs;
    }
    
    /**
     * Retrieve information about installed SMF service from preferences.
     * @param env execution Env for system
     * @return map of jvm names as keys and installed SMF service names as values
     * @throws com.sun.grid.grm.GrmException - if any error occurred
     */
    public Result<Map<String, String>> execute(ExecutionEnv env) throws GrmException {
            
        CheckSMFSupportCommand cmd = new CheckSMFSupportCommand();
        boolean smf = env.getCommandService().<Boolean>execute(cmd).getReturnValue();
        if (!smf) {
            //SMF not supported
            throw new GrmException("GetSMFSUpportCommand.error.SMFnotsupported", BUNDLE);
        }
        //SMF supported host - retrieve info about installed hedeby smf services
        CommandResult<Map<String,String>> ret = new CommandResult<Map<String,String>>();
        try {
            ret.setReturnValue(PreferencesUtil.getAllSMFServices(env.getSystemName(), prefs));
        } catch (BackingStoreException ex) {
            throw new GrmException("GetSMFSUpportCommand.error.failedToGetSMFFromPrefs", ex, BUNDLE);
        }
        
        return ret;
    }
    
}
