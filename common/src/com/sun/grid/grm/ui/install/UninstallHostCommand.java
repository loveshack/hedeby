/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Command;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.component.RemoveComponentsFromHostCommand;
import com.sun.grid.grm.ui.component.service.GetActiveJVMListCommand;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.Platform;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.prefs.BackingStoreException;

/**
 * This is UI command for uninstalling the host from system, it removes the host information 
 * from preferences, if no more host are using current preferences the whole preferences are deleted.
 * The localspool directory is cleaned up. To uninstall master host the master flag should be set.
 * For uninstalling master host system should be shut down. For uninstalling managed host when 
 * system is not running (CS not reachable) forced flag should be used.
 */
public class UninstallHostCommand extends AbstractLocalCommand<Void>{
    private Hostname hostname;
    private boolean master;
    private boolean forced;
    private PreferencesType prefs;
    private static String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private List<Command> undo = new LinkedList<Command>();
    
    /** Creates a new instance of UninstallHostCommand */
    public UninstallHostCommand(Hostname hostname, PreferencesType prefs, boolean master, boolean forced) {
        this.setHostname(hostname);
        this.setMaster(master);
        this.setForced(forced);
        this.setPreferencesType(prefs);
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        boolean isCSHost = env.getCSHost().equals(getHostname());
        boolean csRunning = SystemUtil.isCSStarted(env);
        RemoveDirCommand cmd = new RemoveDirCommand(env.getLocalSpoolDir());
        UninstallHostFromPreferencesCommand pCmd = new UninstallHostFromPreferencesCommand(getPreferencesType(), getHostname());
        undo.clear();
        
        if (getPreferencesType().equals(PreferencesType.SYSTEM) && !Platform.getPlatform().isSuperUser()) {
            throw new GrmException("uninstallhost.error.systempreferences.notsuperuser", BUNDLE);
        }
        
        try {
            if (isMaster() && !isCSHost) {
                throw new GrmException("uninstallhost.error.not_master_host", BUNDLE, getHostname().getHostname());
            } else if (!isMaster() && isCSHost) {
                throw new GrmException("uninstallhost.error.not_managed_host", BUNDLE, getHostname().getHostname());
            } else if (isMaster()) {
                //uninstall master host
                if (csRunning) {
                    throw new GrmException("uninstallhost.error.master_cs_running", BUNDLE);
                }
            } else {
                //uninstall managed host
                if (!csRunning && !isForced()) {
                    throw new GrmException("uninstallhost.not_forced_not_running_cs", BUNDLE, getHostname().getHostname());
                }
                if (csRunning) {
                    //check if all jvms on this host are shutdown
                    GetActiveJVMListCommand scmd = new GetActiveJVMListCommand();
                    scmd.setHostname(getHostname().getHostname());
                    List<ActiveJvm> aj = env.getCommandService().execute(scmd).getReturnValue();
                    if (aj.size() > 0 && !forced) {
                        throw new GrmException("uninstallhost.error.active_jvm_on_managed", BUNDLE, getHostname());
                    }
                    RemoveComponentsFromHostCommand remCmd = new RemoveComponentsFromHostCommand(getHostname());
                    env.getCommandService().execute(remCmd);
                    undo.add(remCmd);
                }
            }
            //remove smf support if present
            Map<String, String> smf = PreferencesUtil.getAllSMFServices(env.getSystemName(), prefs);
            if (!smf.isEmpty()) {
                Object[] smfJvms = smf.keySet().toArray();
                for (int i =0; i< smfJvms.length;i++) {
                    UninstallSMFCommand smfCmd = new UninstallSMFCommand((String)smfJvms[i], prefs);
                    env.getCommandService().execute(smfCmd);
                    undo.add(smfCmd);
                }
            }
            //remove Service Tags support
            if(isMaster()) {
                RemoveSTEntryCommand remove = new RemoveSTEntryCommand(prefs);
                env.getCommandService().execute(remove);
                undo.add(remove);
            }
            
            //remove localspool
            //we allow to uninstall system when localspool doesnt exist
            if (env.getLocalSpoolDir().exists()) {
                env.getCommandService().execute(cmd);
                undo.add(cmd);
            }
            //remove preferences
            env.getCommandService().execute(pCmd);
            undo.add(pCmd);
            
            if (getPreferencesType().equals(PreferencesType.SYSTEM) 
            && !PreferencesUtil.hasAnySystem(PreferencesType.SYSTEM) && PathUtil.getSdmEnvFile().exists()) {
                UninstallRcScriptCommand rcCmd = new UninstallRcScriptCommand();
                env.getCommandService().execute(rcCmd);
                undo.add(rcCmd);
            }
        } catch (BackingStoreException ex) {
            this.undo(env);
            throw new GrmException("UninstallHostCommand.error.failed_read_systems", ex, BUNDLE);
        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }       
        return CommandResult.VOID_RESULT;
    }
    @Override
    public void undo(ExecutionEnv env) {
        for (Command c : undo) {
            c.undo(env);
        }
        undo.clear();
    }

    public Hostname getHostname() {
        return hostname;
    }

    public void setHostname(Hostname hostname) {
        this.hostname = hostname;
    }

    public boolean isMaster() {
        return master;
    }

    public void setMaster(boolean master) {
        this.master = master;
    }

    public boolean isForced() {
        return forced;
    }

    public void setForced(boolean forced) {
        this.forced = forced;
    }

    public PreferencesType getPreferencesType() {
        return prefs;
    }

    public void setPreferencesType(PreferencesType prefs) {
        this.prefs = prefs;
    }

    
}
