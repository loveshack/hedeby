/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Command that creates internal directory structure on master host
 */
public class CheckMasterHostInstallationCommand extends AbstractLocalCommand<Void> {

    /**
     * i18n Bundle name
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(CheckMasterHostInstallationCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007081051;
    private final String owner;

    /**
     * Creates an instance of CheckMasterHostInstallationCommand.
     * @param owner owner of the directories. This is normally the admin user.
     */
    public CheckMasterHostInstallationCommand(String owner) {
        this.owner = owner;
    }

    /**
     * Command that checks the master host installation and
     * doing final cleanup steps.
     * @param env Environment used for performing the action
     * @return Result<void>
     * @throws com.sun.grid.grm.GrmException On errors
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        Platform myPlatform = Platform.getPlatform();
        
        // change file ownership if we running as superuser
        try {
            if (myPlatform.isSuperUser()) {
                File globalConfigPath = PathUtil.getSpoolDirForComponent(env, BootstrapConstants.CS_ID);
                myPlatform.chown(globalConfigPath, owner, true);
            }
        } catch (InterruptedException ex) {
            throw new GrmException("CheckMasterHostInstallationCommand.operationfailed", 
                    ex, BUNDLE, ex.getLocalizedMessage());        
        } catch (IOException ex) {
            throw new GrmException("CheckMasterHostInstallationCommand.operationfailed", 
                    ex, BUNDLE, ex.getLocalizedMessage());        
        }
        return CommandResult.VOID_RESULT;
    }
}
