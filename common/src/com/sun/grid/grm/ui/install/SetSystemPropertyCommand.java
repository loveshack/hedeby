/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PreferencesType;
import com.sun.grid.grm.bootstrap.PreferencesUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;

/**
 * Set a system property. Supported are "auto_start" and "no_ssl"
 */
public class SetSystemPropertyCommand extends AbstractLocalCommand<Void> {
    /**
     * i18n Bundle name
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(SetSystemPropertyCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007062001;
    private final String propertyName;
    private final String propertyValue;
    private final PreferencesType prefType;
    
    /**
     * Creates a new instance of SetSystemPropertyCommand
     * @param propertyName name of the property
     * @param propertyValue value of the property
     * @param prefType used preferences type (user or sytem)
     */
    public SetSystemPropertyCommand(String propertyName, String propertyValue, PreferencesType prefType) {
        this.propertyName = propertyName;
        this.propertyValue = propertyValue;
        this.prefType = prefType;
    }

    /**
     * Execute the command in the specified ExecutionEnv
     * @param env Execution environment where the command should run
     * @return void
     * @throws com.sun.grid.grm.GrmException on all errors
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        if(ExecutionEnv.AUTO_START.equals(propertyName)) {
            boolean value = false;
            if("true".equalsIgnoreCase(propertyValue)) {
                value = true;
            } else if ("false".equalsIgnoreCase(propertyValue)) {
                value = false;
            } else {
                this.undo(env);
                throw new GrmException("SetSystemPropertyCommand.invalue_value_for_system_property",
                        BUNDLE, propertyName, propertyValue);
            }
            try {
                PreferencesUtil.setAutoStart(env.getSystemName(), prefType, value);
            } catch (BackingStoreException ex) {
                this.undo(env);
                throw new GrmException("SetSystemPropertyCommand.cannot_store_properties", ex,
                        BUNDLE,
                        new Object[]{propertyName, value, env.getSystemName(),
                        prefType, ex.getLocalizedMessage()});
            }
            // test value
            boolean testValue = PreferencesUtil.isAutoStart(env.getSystemName(), prefType);
            if (testValue == value) {
                log.log(Level.INFO, "SetSystemPropertyCommand.sysprop_set",
                        new Object[]{ExecutionEnv.AUTO_START, testValue});
            } else {
                this.undo(env);
                throw new GrmException("SetSystemPropertyCommand.cannot_set_prop_to_value",
                        BUNDLE, new Object[]{propertyName, value, env.getSystemName(), prefType});
            }
        } else if (ExecutionEnv.NO_SSL.equals(propertyName)) {
            boolean value = false;
            if("true".equalsIgnoreCase(propertyValue)) {
                value = true;
            } else if ("false".equalsIgnoreCase(propertyValue)) {
                value = false;
            } else {
                this.undo(env);
                throw new GrmException("SetSystemPropertyCommand.invalue_value_for_system_property",
                        BUNDLE, propertyName, propertyValue);
            }
            try {
                PreferencesUtil.setSSLDisabled(env.getSystemName(), prefType, value);
            } catch (BackingStoreException ex) {
                this.undo(env);
                throw new GrmException("SetSystemPropertyCommand.cannot_store_properties", ex,
                        BUNDLE,
                        new Object[]{propertyName, value, env.getSystemName(),
                        prefType, ex.getLocalizedMessage()});
            }
            // test value
            boolean testValue = PreferencesUtil.isSSLDisabled(env.getSystemName(), prefType);
            if (testValue == value) {
                log.log(Level.INFO, "SetSystemPropertyCommand.sysprop_set",
                        new Object[]{ExecutionEnv.NO_SSL, value});
            } else {
                this.undo(env);
                throw new GrmException("SetSystemPropertyCommand.cannot_set_prop_to_value",
                        BUNDLE, new Object[]{propertyName, value, env.getSystemName(), prefType});
            }
        } else {
            this.undo(env);
            throw new GrmException("SetSystemPropertyCommand.unknown_system_property",
                    BUNDLE, propertyName);
        }
        return CommandResult.VOID_RESULT;
    }
    

}
