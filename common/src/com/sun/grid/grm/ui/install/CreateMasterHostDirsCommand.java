/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Command that creates internal directory structure on master host
 */
public class CreateMasterHostDirsCommand extends AbstractCreateDirectoriesCommand {

    /**
     * i18n Bundle name
     */
    public static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(CreateMasterHostDirsCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007062001;

    /**
     * Creates an instance of CreateMasterHostDirsCommand.
     * @param owner owner of the directories. This is normally the admin user.
     */
    public CreateMasterHostDirsCommand(String owner) {
        super(owner);
    }

    /**
     * Command that creates internal directory structure on master host
     * @param env Environment used for performing the action
     * @return Result<void>
     * @throws com.sun.grid.grm.GrmException On errors
     */
    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        
        try {
            File lsd = env.getLocalSpoolDir();
            if (!lsd.getParentFile().exists()) {
                mkdir(lsd.getParentFile());
            } 
            if (!lsd.getParentFile().isDirectory()) {
                throw new GrmException("ui.command.isnotfile", BUNDLE, lsd.getParentFile().getPath());
            }
            mkdir(lsd);
            mkdir(PathUtil.getSpoolPath(env));
            mkdir(PathUtil.getLogPath(env));
            mkdir(PathUtil.getLocalTmpPath(env));
            mkdir(PathUtil.getLocalSecurityPath(env));
            mkdir(PathUtil.getLocalSpoolRunPath(env));
            File csDir = new File(PathUtil.getSpoolPath(env), BootstrapConstants.CS_ID);
            File csComponentDir = new File(csDir, "component");
            File csActiveComponentDir = new File(csDir, BootstrapConstants.CS_ACTIVE_COMPONENT);
            File csActiveJvmDir = new File(csDir, BootstrapConstants.CS_ACTIVE_JVM);
            mkdir(csDir);
            mkdir(csComponentDir);
            mkdir(csActiveJvmDir);
            mkdir(csActiveComponentDir);
            
            // copy logging.properties file
            File destFile = new File(env.getLocalSpoolDir(), "logging.properties");
            File srcFile = new File(PathUtil.getTemplatePath(env), "logging.properties.template");
            try {
                // copy logging.properties file
                Platform.getPlatform().copy(srcFile, destFile);
                if (Platform.getPlatform().isSuperUser()) {
                    // do chown to hedeby admin user
                    Platform.getPlatform().chown(destFile, getOwner());
                }
            } catch (IllegalStateException ex) {
                throw new GrmException(ex.getLocalizedMessage(), ex);
            } catch (InterruptedException ex) {
                throw new GrmException(ex.getLocalizedMessage(), ex);
            } catch (IOException ex) {
                throw new GrmException(ex.getLocalizedMessage(), ex);
            }
        } catch (GrmException ex) {
            this.undo(env);
            throw ex;
        }
        
        return CommandResult.VOID_RESULT;
    }
}
