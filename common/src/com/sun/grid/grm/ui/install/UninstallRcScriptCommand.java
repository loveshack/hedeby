/*___INFO__MARK_BEGIN__*/
 /*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.ui.install;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.ui.AbstractLocalCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * This is local command for uninstalling the support for rc scripts on host.
 * For uninstallation script: $hedeby_dist/bin/supportRc.sh is used
 * File with variables information:
 * /etc/sdm/sdm.env
 * exist on the system, rcsupport is installed, they are removed
 * Rc script support for windows host is not supported
 */
public class UninstallRcScriptCommand extends AbstractLocalCommand<Void> {
    private static final String BUNDLE = "com.sun.grid.grm.ui.install.messages";
    private static final Logger log = Logger.getLogger(InstallRcScriptCommand.class.getName(), BUNDLE);
    private static final long serialVersionUID = 2007121801;
    //template should be in $DIST/util/templates
    private static String env_file = "sdm.env";
    //dir where variables file was stored
    private static File distEnv = new File(File.separatorChar+"etc"+File.separatorChar+"sdm");
    private Map<File, File> undoCopy = new HashMap<File, File>();
    private List<File> undoRemoveDir = new LinkedList<File>();
    private List<File> files = new LinkedList<File>();
    private boolean success=false;
    /**
     * Creates a new instance of InstallRcScriptCommand
     */
    /** Creates a new instance of UninstallRcScriptCommand */
    public UninstallRcScriptCommand() {
   
    }

    public Result<Void> execute(ExecutionEnv env) throws GrmException {
        files.clear();
        undoCopy.clear();
        undoRemoveDir.clear();
        success = false;
        File file = new File(distEnv, env_file);
        if (!file.exists()) {
            if (log.isLoggable(Level.FINE)) {
                LogRecord lr = new LogRecord(Level.FINE, "UninstallRcScriptCommand.not_installed.missing_file");
                lr.setParameters(new Object[] {file.getAbsolutePath()});
                log.log(lr);
            }
            return CommandResult.VOID_RESULT;
        }
        File tmpFile = Platform.getPlatform().createTmpDir(null, null);

        File tmp = new File(tmpFile, env_file);
        try{
            Platform.getPlatform().copy(file, tmp);
            undoCopy.put(tmp, file);
            file.delete();
            if (distEnv.listFiles().length == 0) {            
                Platform.getPlatform().removeDir(distEnv, true);
                undoRemoveDir.add(distEnv);
            }
        } catch (Exception ex) {
            throw new GrmException("UninstallRcScriptCommandCommand.error.failed", ex, BUNDLE, ex.getLocalizedMessage());
        }
        
        int res = -1;
        String command = env.getDistDir().getAbsolutePath()+"/util/supportRc.sh uninstall "+env.getDistDir().getAbsolutePath();
        try {                
            res = Platform.getPlatform().exec(command, null, PathUtil.getTmpDir(), null, null, 0);
            //from here we dont know what can fail - reinstall for undo
            success=true;
        } catch (Exception ex) {
            this.undo(env);
            throw new GrmException("UninstallRcScriptCommand.error.failed", ex, BUNDLE, ex.getLocalizedMessage());
        }
        if (res != 0) {
            this.undo(env);
            throw new GrmException("UninstallRcScriptCommand.error.command_execution_error_exit", BUNDLE, command, res);
        }
        return CommandResult.VOID_RESULT;
    }
    @Override
    public void undo(ExecutionEnv env) {
        for (File f : undoRemoveDir) {
            if (!f.mkdirs()) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "UninstallRcScriptCommand.undo.restoredirfailed");
                    lr.setParameters(new Object[] {f.getAbsolutePath()});
                    log.log(lr);
                }
                return;
            }
        }
        for (File src : undoCopy.keySet()) {
            try {
                Platform.getPlatform().copy(src, undoCopy.get(src));
            } catch (Exception ex) {
                //log error - while undo
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "UninstallRcScriptCommand.undo.restorefilefailed");
                    lr.setParameters(new Object[] {src.getAbsolutePath(), undoCopy.get(src).getAbsolutePath()});
                    lr.setThrown(ex);
                    log.log(lr);
                }
                return;
            } 
        }
        undoCopy.clear();
        if (success) {
            success=false;
            //was uninstalled we can try installtion using script
            int res = -1;
            String command = env.getDistDir().getAbsolutePath()+"/util/supportRc.sh install "+env.getDistDir().getAbsolutePath();
            try {
                res = Platform.getPlatform().exec(command, null, PathUtil.getTmpDir(), null, null, 0);
                //from here we dont know what can fail - reinstall for undo
                success=true;
            } catch (Exception ex) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "UninstallRcScriptCommand.undo.execution_reinstall_failed");
                    lr.setParameters(new Object[] {command});
                    lr.setThrown(ex);
                    log.log(lr);
                }
                return;
            }
            if (res != 0) {
                if (log.isLoggable(Level.WARNING)) {
                    LogRecord lr = new LogRecord(Level.WARNING, "UninstallRcScriptCommand.undo.reinstall_failed");
                    lr.setParameters(new Object[] {command, res});
                    log.log(lr);
                }
                return;
            }
        }
    }    
}
