/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.ui.impl.CommandResult;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * MacroCommand is a marker class that identifies all Commands that are macros.
 * MacroCommand can contain both LocalCommands and SystemCommands, therefore
 * its execution is not atomic. In case that any single command fails (its
 * execution throws an exception) only this last command is undone. MacroCommand
 * <b>is not</b> batch command!
 *
 * In case that an atomic execution of the group of command is needed, batch
 * command should be used. When undo is needed, batch command is be able to
 * call undo operation on all previosuly finished commands. To achieve mentioned
 * goals a batch command has to contain the same type of commands only.
 *
 * There is no special class or interface for batch command -
 * it is up to user to implement the command with batch feature to suite his
 * needs.
 *
 * @param T 
 */
public class MacroCommand<T> implements Command<T>, Serializable {

    private static final long serialVersionUID = -2007072401L;
    protected List<Command> commands;
    private Stack<Command> finishedCommands;

    public MacroCommand() {
        commands = new LinkedList<Command>();
        finishedCommands = new Stack<Command>();
    }
    
    public MacroCommand(int capacity) {
        commands = new ArrayList<Command>(capacity);
        finishedCommands = new Stack<Command>();
    }

    @SuppressWarnings(value = "unchecked")
    public final Result<T> execute(ExecutionEnv env) throws GrmException {
        Result<T> finalResult = new CommandResult<T>();
        for (Command cmd : commands) {
            env.getCommandService().execute(cmd);
            finishedCommands.add(cmd);
        }
        return finalResult;
    }
    
    /**
     *  Add a command to the marco.
     */
    public void addCommand(Command<T> command) {
        commands.add(command);
    }

    /**
     *  Undo the marco.
     *  @param env the execution env
     */
    public final void undo(ExecutionEnv env) {
        while (!finishedCommands.empty()) {
            Command cmd = finishedCommands.pop();
            cmd.undo(env);
        }
    }
}
