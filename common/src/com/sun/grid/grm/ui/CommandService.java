/*___INFO__MARK_BEGIN__*/ 
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.ui;

import com.sun.grid.grm.GrmException;

/**
 * Interface for CommandService implementations, CommandService is responsible for 
 * executing commands
 */
public interface CommandService {
    
    /**
     * A convenient way to invoke a command. 
     *
     * @param command Command that has to be invoked
     * @return Result with return value as specified by Command type
     * @throws com.sun.grid.grm.GrmException everytime a problem during
     * command invocation occurs.
     */
    public <T> Result<T> execute(Command<T> command) throws GrmException;
    
    /**
     * Execute a command locally. No comunication with system components should be done.
     *
     * @param command   the command which should be execution
     * @throws com.sun.grid.grm.GrmException if the execution of the command failed
     * @return the result of the command
     */
    public <T> Result<T> executeLocally(Command<T> command) throws GrmException;
    
}
