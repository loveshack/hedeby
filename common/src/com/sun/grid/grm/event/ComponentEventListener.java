/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * ComponentEventListener.java
 *
 * Created on September 29, 2006, 12:59 PM
 *
 */

package com.sun.grid.grm.event;

import com.sun.grid.grm.management.*;
import java.util.EventListener;

/**
 * The ComponentEventListener is used by the Component to inform about state changes
 * in the Component.
 *
 */
@ManagedEventListener
public interface ComponentEventListener extends EventListener {
    
    /**
     * The connection to the component has been closed
     */
    public void connectionClosed();
    
    /**
     * The connection to the component is failed
     */
    public void connectionFailed();
    
    /**
     * The system has probably lost some events from the component
     */
    public void eventsLost();
    
    /**
     * The component state is unknown.
     * 
     *  @param event the event object
     */
    public void componentUnknown(ComponentStateChangedEvent event);
    
    /**
     * The component is starting. 
     * 
     *  @param event the event object
     */
    public void componentStarting(ComponentStateChangedEvent event);
    
    /**
     * The component has been started and is working. 
     * 
     *  @param event the event object
     */
    public void componentStarted(ComponentStateChangedEvent event);
    
    /**
     * The component is stopping. 
     * 
     *  @param event the event object
     */
    public void componentStopping(ComponentStateChangedEvent event);
    
    /**
     * The component has been stopped. 
     * 
     *  @param event the event object
     */
    public void componentStopped(ComponentStateChangedEvent event);
    
    /**
     * The component is reloading. 
     * 
     *  @param event the event object
     */
    public void componentReloading(ComponentStateChangedEvent event);
}
