/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * ComponentStateChangedEvent.java
 *
 * Created on September 29, 2006, 1:01 PM
 *
 */

package com.sun.grid.grm.event;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.util.Hostname;
import java.io.Serializable;

/**
 * This event signals a state change in a GrmComponent bean.
 */
public class ComponentStateChangedEvent extends AbstractComponentEvent implements Serializable {
    private static final long serialVersionUID = -2008250201L;
    
    private final ComponentState newState;    
    
    /**
     * Creates a new instance of ComponentStateChangedEvent
     * 
     * @param sequenceNumber the sequence number of the event
     * @param componentName the name of the component
     * @param hostname     hostname where the components lives
     * @param newState     the new state
     */
    public ComponentStateChangedEvent(long sequenceNumber, String componentName, Hostname hostname, ComponentState newState) {
        super(sequenceNumber, componentName, hostname);
        this.newState = newState;
    }        
        
    /**
     * Returns the new state of the component.
     * 
     * @return the new state of the component
     */
    public ComponentState getNewState() {
        return newState;
    }        
    
    /**
     *  Get human readable string representation of this event
     *
     *  @return human readable string representation
     */
    @Override
    public String toString() {
        return String.format("[%d, %s, %s state changed: %s]", getSequenceNumber(), getComponentName(), getHostname(), newState);
    }
}
