/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.event;

import com.sun.grid.grm.util.Hostname;
import java.io.Serializable;

/**
 * AbstractComponentEvent carries information about Component related events.
 */
public abstract class AbstractComponentEvent implements Serializable, Comparable<AbstractComponentEvent> {

    /**
     * The serial version UID
     */
    private static final long serialVersionUID = -2008021801;
    private final long sequenceNumber;
    private final String componentName;
    private final Hostname hostname;
    private transient int hash;

    /**
     * Creates a new instance of AbstractComponentEvent.
     * 
     * @param aSequenceNumber the sequence number for the event
     * @param aComponentName the name of the component
     * @param aHostname the name of the host the component runs
     */
    public AbstractComponentEvent(long aSequenceNumber, String aComponentName, Hostname aHostname) {
        if (aComponentName == null) {
            throw new NullPointerException();
        }        
        if (aHostname == null) {
            throw new NullPointerException();
        }
        sequenceNumber = aSequenceNumber;
        componentName = aComponentName;
        hostname = aHostname;
    }

    /**
     * get the sequence number of the event
     * @return the sequence number
     */
    public long getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * get the name of the component
     * @return the name of the component
     */
    public String getComponentName() {
        return componentName;
    }
    
    /**
     * Get the name of the host where the components lives
     * @return the name of the host
     */
    public Hostname getHostname() {
        return hostname;
    }

    /**
     * Compare with another component event.
     *
     * @param o the other component event
     * @return  a negative integer, zero, or a positive integer as this object is
     *          less than, equal to, or greater than the specified object.
     */
    public int compareTo(AbstractComponentEvent o) {
        int ret = componentName.compareTo(o.getComponentName());
        if (ret == 0) {
            if (sequenceNumber < o.sequenceNumber) {
                ret = -1;
            } else if (sequenceNumber > o.sequenceNumber) {
                ret = 1;
            } else {
                ret = hostname.compareTo(o.hostname);
            }
        }
        return ret;
    }

    /**
     *  test if this is equal to a object.
     *
     *  @param obj the object
     *  @return <code>true</code> if this is equal to <code>object</code>
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AbstractComponentEvent) {
            AbstractComponentEvent e = (AbstractComponentEvent) obj;
            return sequenceNumber == e.sequenceNumber && componentName.equals(e.componentName) && hostname.equals(e.hostname);
        }
        return false;
    }

    /**
     *  Get the hashcode of this object
     *
     *  @return the hashcode
     */
    @Override
    public int hashCode() {
        int h = hash;
        if (h == 0) {
            h = (int) (sequenceNumber ^ (sequenceNumber >>> 32));
            h = 31 * h + componentName.hashCode() + hostname.hashCode();
            hash = h;
        }
        return h;
    }
}
