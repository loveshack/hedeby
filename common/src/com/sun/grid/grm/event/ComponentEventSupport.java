/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.event;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.util.EventListenerSupport;
import com.sun.grid.grm.util.Hostname;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class for firing component related events.
 *
 */
public class ComponentEventSupport {

    private final static String BUNDLE = "com.sun.grid.grm.event.messages";
    private static Logger log = Logger.getLogger(ComponentEventSupport.class.getName(), BUNDLE);
    private final EventListenerSupport<ComponentEventListener> listeners;
    private final AtomicLong eventSequenceNumber = new AtomicLong();
    private final String componentName;
    private final Hostname hostName;

    private String logPrefix = "Component";
    
    /**
     * Create a new instance of ComponentEventSupport.
     * 
     * @param aComponentName name of the component
     * @param aHostname name of the host where component is running
     */
    protected ComponentEventSupport(String aComponentName, Hostname aHostname) {
        if (aComponentName == null) {
            throw new IllegalArgumentException();
        }
        if (aHostname == null) {
            throw new IllegalArgumentException();
        }
        this.componentName = aComponentName;
        this.hostName = aHostname;
        this.listeners = EventListenerSupport.<ComponentEventListener>newAsynchronInstance(ComponentEventListener.class);
    }

    
    /**
     * Get the number of pending events in the event queue
     * @return the number of pending events
     */
    public int getNumberOfPendingEvents() {
        return listeners.getNumberOfPendingEvents();
    }
    
    /**
     * Get the sequence number of the next event
     * @return the sequence number of the next event
     */
    public long getSequenceNumberOfNextEvent() {
        return eventSequenceNumber.get();
    }

    /**
     * Create the sequence number for the next event
     * @return
     */
    protected long createSequenceNumberForEvent() {
        return eventSequenceNumber.getAndIncrement();
    }
    /**
     * Get the event processor 
     * @return the event processor
     */
    protected EventListenerSupport.EventProcessor getEventProcessor() {
        return listeners.getEventProcessor();
    }
    
    /**
     * Create a new instance of ComponentEventSupport with synchronous message delivery
     * @param aComponentName name of the component
     * @param aHostname name of the host where component is running
     * @return instance of ComponentEventSupport with synchronous message delivery
     */
    public static ComponentEventSupport newInstance(String aComponentName, Hostname aHostname) {
        return new ComponentEventSupport(aComponentName, aHostname);
    }

    /**
     * Register a <code>ComponentEventListener</code>.
     *
     * @param eventListener the component event listener
     */
    public void addComponentEventListener(ComponentEventListener eventListener) {
        listeners.addListener(eventListener);
    }

    /**
     * Deregister a <code>ComponentEventListener</code>.
     *
     * @param eventListener the component event listener
     */
    public void removeComponentEventListener(ComponentEventListener eventListener) {
        listeners.removeListener(eventListener);
    }

    /**
     * Fire a component state changed event
     * @param newState the new state of the component
     */
    public void fireComponentStateChangedEvent(ComponentState newState) {
        switch (newState) {
            case STARTING:
                fireComponentStarting();
                break;
            case STARTED:
                fireComponentStarted();
                break;
            case STOPPING:
                fireComponentStopping();
                break;
            case STOPPED:
                fireComponentStopped();
                break;
            case RELOADING:
                fireComponentReloading();
                break;
            case UNKNOWN:
                fireComponentUnknown();
                break;
            default:
                throw new IllegalStateException("Unknown component state " + newState);
        }
    }
    
    private final static Lock lock = new ReentrantLock();
    
    protected void lock() {
        lock.lock();
    }
    
    protected void unlock() {
        lock.unlock();
    }
    
    
    /**
     * The component is starting.
     */
    public void fireComponentStarting() {
        lock();
        try {
            ComponentStateChangedEvent evt = new ComponentStateChangedEvent(createSequenceNumberForEvent(), componentName, hostName, ComponentState.STARTING);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { logPrefix, componentName, evt });
            }
            listeners.getProxy().componentStarting(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The component has been started and is working.
     */
    public void fireComponentStarted() {
        lock();
        try {
            ComponentStateChangedEvent evt = new ComponentStateChangedEvent(createSequenceNumberForEvent(), componentName, hostName, ComponentState.STARTED);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { logPrefix, componentName, evt });
            }
            listeners.getProxy().componentStarted(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The component is stopping.
     */
    public void fireComponentStopping() {
        lock();
        try {
            ComponentStateChangedEvent evt = new ComponentStateChangedEvent(createSequenceNumberForEvent(), componentName, hostName, ComponentState.STOPPING);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { logPrefix, componentName, evt });
            }
            listeners.getProxy().componentStopping(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The component has been stopped.
     */
    public void fireComponentStopped() {
        lock();
        try {
            ComponentStateChangedEvent evt = new ComponentStateChangedEvent(createSequenceNumberForEvent(), componentName, hostName, ComponentState.STOPPED);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { logPrefix, componentName, evt });
            }
            listeners.getProxy().componentStopped(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The component state is unknown.
     */
    public void fireComponentUnknown() {
        lock();
        try {
            ComponentStateChangedEvent evt = new ComponentStateChangedEvent(createSequenceNumberForEvent(), componentName, hostName, ComponentState.UNKNOWN);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { logPrefix, componentName, evt });
            }
            listeners.getProxy().componentUnknown(evt);
        } finally {
            unlock();
        }
    }

    /**
     * The component state is reloading.
     */
    public void fireComponentReloading() {
        lock();
        try {
            ComponentStateChangedEvent evt = new ComponentStateChangedEvent(createSequenceNumberForEvent(), componentName, hostName, ComponentState.RELOADING);
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "fireevent", new Object [] { logPrefix, componentName, evt });
            }
            listeners.getProxy().componentReloading(evt);
        } finally {
            unlock();
        }
    }

    /**
     * get the log prefix for this event support
     * @return the log prefix
     */
    public String getLogPrefix() {
        return logPrefix;
    }

    /**
     * set the log prefix for this event support
     * @param logPrefix the log prefix
     */
    public void setLogPrefix(String logPrefix) {
        this.logPrefix = logPrefix;
    }
}
