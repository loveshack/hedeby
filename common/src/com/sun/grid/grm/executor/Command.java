/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * Command.java
 *
 * Created on 5. Mai 2006, 09:38
 *
 */

package com.sun.grid.grm.executor;
import java.io.Serializable;
import java.util.concurrent.Callable;

/**
 * An abstract class defining the command interface to execute commands
 * remotely. By it self it is of littly use. The derived classes will
 * know how to implement the "call" method. The "call" method should ensure
 * the timeout mechanism for execution of the command. The timeout value is available
 * from "getMaxRuntime" method.
 *
 */
public abstract class Command implements Callable<Result>,
                                         Serializable {

    
    
    private final static long serialVersionUID = -2008061201L;

    /**
     * The user name under which the command is executed.
     */
    private String userName = null;

    /**
     * The context object, that knows about the working directory.
     */
    private transient CommandContext commandContext = null;

    /**
     * max runtime in millis
     */
    private long maxRuntime;
    
    /**
     * Creates a new instance of Command.
     */
    public Command() {
        maxRuntime = 0;
    }

    /**
     * Sets the object that knows the working directory.
     * Will be set on server side.
     *
     * @param commandContext env. object, must not be null.
     */
    public final void setCommandContext(final CommandContext commandContext) {
        this.commandContext = commandContext;
    }

    /**
     *  Get the command context of the command.
     * @return  the command context
     */
    public final CommandContext getCommandContext() {
        return commandContext;
    }


    /**
     * sets the user, which will execute the command. Needs to be
     * set on client side
     *
     * @param userName must not be null
     */
    public final void setUser(final String userName) {
        this.userName = userName;
    }

    /**
     * Gets the user name that will execute the command.
     *
     * @return user name
     */
    public final String getUser() {
        return userName;
    }

    /**
     * Get the max runtime of this command in millis
     * @return the max runtime in millis, 0 means no max run time defined
     */
    public long getMaxRuntime() {
        return maxRuntime;
    }

    /**
     * set the max runtime of the command in millis
     * @param maxRuntime the max runtime of the command or 0 if no limit is defined
     */
    public void setMaxRuntime(long maxRuntime) {
        if(maxRuntime < 0) {
            throw new IllegalArgumentException("maxRuntime must not be >= 0");
        }
        this.maxRuntime = maxRuntime;
    }


}
