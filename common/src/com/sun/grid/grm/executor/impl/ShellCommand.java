/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
/*
 * ShellCommand.java
 *
 * Created on 4. Mai 2006, 13:41
 *
 */

package com.sun.grid.grm.executor.impl;

import com.sun.grid.grm.executor.Result;
import com.sun.grid.grm.executor.Command;
import com.sun.grid.grm.executor.ExecutorException;
import com.sun.grid.grm.executor.RemoteFile;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Instances of shell command can be used the execute shell command with
 * the Executor.
 *
 * <ul>
 *     <li>The shell command can be a simple string (set with <code>setCommand</code>) or a script
 *         file (set with <code>setScript</code>).</li>
 *     <li>The script will be stored relative to the directory where the <code>ShellCommand</code>
 *         is executed (see <code>setWorkingDirectory</code>)</li>
 *     <li>Additional input files for the command can be added with the <code>addFile</code>
 *         method.</li>
 *     <li>Environment variable can be added with the <code>addEnv</code> method. </li>
 * </ul>
 *
 * <b>Command example:</b>
 *
 * <pre>
 *   Executor executor = ...;
 *
 *   ShellCommand cmd = new ShellCommand();
 *   cmd.setCommand("ls -l");
 *
 *   Result res = executor.exec(cmd);
 *
 *   System.out.println("ls -l exited with status " + res.getExitValue());
 * </pre>
 *
 * <b>Script example:</b>
 *
 * <pre>
 *   Executor executor = ...;
 *
 *   ShellCommand cmd = new ShellCommand();
 *   File script = new File("/tmp/foo.sh");
 *   cmd.setScript(new RemoteFile(script, "foo.sh"));
 *
 *   File inputFile = new File("/tmp/input.txt");
 *   cmd.addFile(new RemoteFile(inputFile, "input.txt"));
 *
 *   Result res = executor.exec(cmd);
 *   System.out.println("foo.sh exited with status " + res.getExitValue());
 *
 * </pre>
 *
 * @see com.sun.grid.grm.executor.Executor
 * @see com.sun.grid.grm.executor.Result
 */
public class ShellCommand extends Command {

    private final static long serialVersionUID = -2006010101L;

    private static final String BUNDLE = "com.sun.grid.grm.executor.executor";
    private static final Logger LOGGER =
                                  Logger.getLogger(ShellCommand.class.getName(),
                                  BUNDLE);

    private String command;

    private RemoteFile script = null;

    private Map<String, String> env = null;

    private List<RemoteFile> files;

    /**
     * Creates a new instance of ShellCommand.
     */
    public ShellCommand() {
        command = null;
        files = null;
    }

    /**
     * Create a new ShellCommand.
     *
     * @param command the command which should be executed
     * @see #setCommand
     */
    public ShellCommand(final String command) {
        setCommand(command);
    }

    /**
     *  Create a new ShellCommand.
     *
     *  @param script the script which will be executed
     *  @see #setScript
     */
    public ShellCommand(final RemoteFile script) {
        setScript(script);
    }

    /**
     *  Set command that will be executed on the remote host.
     *
     *  @param command the command
     */
    public final void setCommand(final String command) {
        this.command = command;
        script = null;
    }
    
    /**
     * Get the command.
     * 
     * @return the command (can be <code>null</code>)
     */
    public final String getCommand() {
        return command;
    }

    /**
     * Set the script which will be executed.
     *
     * The script will be stored relative to the working directory
     * of the execution environment
     *
     * @param  script the script
     */
    public final void setScript(final RemoteFile script) {
        command = null;
        this.script = script;
    }
    
    /**
     * Get the script for the shell command
     * @return the script for the shell command or <code>null</code>
     *         if no script has been set
     */
    public RemoteFile getScript() {
        return script;
    }

    private void envInit() {
        if (env == null) {
            env = new HashMap<String, String>();
        }
    }

    /**
     * Add an environment variable.
     * @param name    name of the environment variable
     * @param value   value of the environment variable
     */
    public final void addEnv(final String name, final String value) {
        envInit();
        env.put(name, value);
    }


    /**
     * Get a set of  environment variable names.
     * @return set of environment variable names
     */
    public final Set<String>getEnvNames() {
        Set<String> ret = null;
        if (env != null) {
            ret = Collections.unmodifiableSet(env.keySet());
        } else {
            ret = Collections.emptySet();
        }
        return ret;
    }

    /**
     *  Get the value of a environment variable.
     *
     *  @param  name  name of the environment variable
     *  @return the value of the environment variable or <code>null</code>
     *          if the environment variable is not set
     */
    public final String getEnvValue(final String name) {
        String ret = null;
        if (env != null) {
            return env.get(name);
        }
        return ret;
    }


    /**
     * Set the environment of the shell command.
     *
     * @param env map with the names of the environment variables as key
     *            add the values as value
     */
    public final void setEnv(final Map<String, String> env) {
        envInit();
        this.env.clear();
        this.env.putAll(env);
    }

    private String[] getEnviroment() {
        String [] ret = null;
        if (env != null) {
            ret = new String[env.size()];
            int i = 0;
            for (String name : env.keySet()) {
                ret[i++] = name + "=" + env.get(name);
            }
        }
        return ret;
    }

    private void initFileList() {
        if (this.files == null) {
            this.files = new ArrayList<RemoteFile>();
        }
    }

    /**
     *  Add a additional file to the shell command.
     *
     *  This file will be stored relatively to the working directory if
     *  the shell command.
     *
     *  @param remoteFile the file
     */
    public final void addFile(final RemoteFile remoteFile) {
        initFileList();
        files.add(remoteFile);
    }

    /**
     *  Get all additional files for the shell command.
     *
     *  @return the additional files
     */
    public final List<RemoteFile> getFiles() {
        if (files != null) {
            return Collections.unmodifiableList(files);
        } else {
            return Collections.emptyList();
        }
    }

    /**
     *  Execute the shell command. This method ensures that it takes no longer than defined timeout.
     *
     *  @return the result of the command
     *  @throws Exception of the shell command failed
     */
    public final Result call() throws Exception {
        LOGGER.entering(ShellCommand.class.getName(), "call");

        if (getUser() == null) {
            setUser(System.getProperty("user.name"));
        }
        
        Result result = new Result();

        File workingDir = getCommandContext().getWorkingDirectory();

        if (!workingDir.exists()) {
            LOGGER.log(Level.FINE, "shellcommand.createWorkingDirectory", workingDir);

            if (!workingDir.mkdir()) {
                throw new ExecutorException("shellcommand.exception.createDirFailed", BUNDLE, workingDir);
            }
        }

        if (this.script != null) {
            File scriptFile = script.store(workingDir);
            Platform.getPlatform().chmod(scriptFile, "u+x");
            Platform.getPlatform().chown(scriptFile, getUser(), false);
            command = scriptFile.getAbsolutePath();
        }

        if (this.command == null) {
            throw new ExecutorException("shellcommand.no_command_specified", BUNDLE);
        }
        
        for (RemoteFile file : getFiles()) {
            File localFile = file.store(workingDir);
            Platform.getPlatform().chown(localFile, getUser(), false);
        }

        Platform.getPlatform().chown(workingDir, getUser(), false);

        try {
            /*
             * Recalculate miliseconds to seconds
             */
            int timeout = Math.round(this.getMaxRuntime()/1000);
            //command execution with proper timeout
            int ret = Platform.getPlatform().execAs(getUser(), command, getEnviroment(),
                                                    workingDir,
                                                    result.getOutputMessages(),
                                                    result.getErrorMessages(), timeout);
            result.setExitValue(ret);
        } catch(InterruptedException ex) {
            // Ignore, the result will not have an exit code
            // This signal the caller that the command has been interrupted
            // However in this case the result files are included into the
            // result
        } finally {
            if (!workingDir.exists()) {
               throw new ExecutorException("shellcommand.workingDirectoryNotExisting", 
                       BUNDLE, workingDir, command);
            }
            addFilesToResult(workingDir, workingDir.getAbsolutePath(), result);
        }

        LOGGER.exiting(ShellCommand.class.getName(), "call", result);

        return result;
    }

    private void addFilesToResult(final File dir, final String workingDir,
                                  final Result result) throws IOException {

        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                addFilesToResult(file, workingDir, result);
            } else if (script == null && !script.getRemoteFilename().equals(file.getName())) {
                String name = file.getAbsolutePath();
                name = name.substring(workingDir.length() + 1, name.length());

                RemoteFile rf = new RemoteFile(file, name);
                // result files have to be loaded immediately, because
                // the executor will cleanup the working directory
                rf.load();
                result.addFile(rf);
                LOGGER.log(Level.FINE, "shellcommand.fileAddedtoResult", name);
            }
        }
    }
    
    /**
     * Get the shell command as string
     * @return the string representation of the shell command
     */
    public String getCommandAsString() {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        if(script != null) {
            pw.println(I18NManager.formatMessage("sc.script", BUNDLE,  script.getRemoteFilename()));
            try {
                pw.println(script.getContentAsString());
            } catch (UnsupportedEncodingException ex) {
                pw.println(ex.getLocalizedMessage());
            }
        } else if (command != null) {
            pw.println(I18NManager.formatMessage("sc.command", BUNDLE, command));
        } else {
            pw.println(I18NManager.formatMessage("sc.noScriptAndCommand", BUNDLE));
        }
        Set<String> envNames = getEnvNames();
        if (envNames.isEmpty()) {
            pw.println(I18NManager.formatMessage("sc.noEnv", BUNDLE));
        } else {
            boolean first = true;
            for (String name : envNames) {
                if (first) {
                    pw.println(I18NManager.formatMessage("sc.envHeader", BUNDLE, name, getEnvValue(name)));
                    first = false;
                } else {
                    pw.println(I18NManager.formatMessage("sc.envEntry", BUNDLE, name, getEnvValue(name)));
                }
            }
        }
        for(RemoteFile dataFile: getFiles()) {
            pw.println(I18NManager.formatMessage("sc.file", BUNDLE,  dataFile.getRemoteFilename()));
            try {
                pw.println(dataFile.getContentAsString());
            } catch (UnsupportedEncodingException ex) {
                pw.println(ex.getLocalizedMessage());
            }
        }
        pw.close();
        return sw.getBuffer().toString();
    }

    @Override
    public String toString() {
        if (script != null) {
            return I18NManager.printfMessage("sc.script.toStr", BUNDLE, script.getRemoteFilename());
        } else {
            return I18NManager.printfMessage("sc.command.toStr", BUNDLE, command);
        }
    }
}
