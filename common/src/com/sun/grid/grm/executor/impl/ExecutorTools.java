/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.executor.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.executor.Command;
import com.sun.grid.grm.executor.ExecutorException;
import com.sun.grid.grm.executor.Result;
import com.sun.grid.grm.util.Hostname;
import static com.sun.grid.grm.bootstrap.BootstrapConstants.GRM_EXECUTOR_INTERFACE;
import com.sun.grid.grm.executor.Executor;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.logging.Logger;

/**
 * The class collects convinience functions around the executor.
 *
 */
public class ExecutorTools {
    
    /**
     * Logger which will be used by this component.
     */
    private static final Logger log =
            Logger.getLogger(ExecutorTools.class.getName());
    
    
    /** Creates a new instance of ExecutorTools */
    public ExecutorTools() {
    }
    
    /**
     * Contacts the executor on the given host and executs the given command.
     *
     * @param env    The execution env
     * @param host   Host where the command should be executed
     * @param command - The command to execute
     * @throws ExecutorException if the command can not be executed
     */
    public static void execute(ExecutionEnv env, Hostname host, Command command) throws ExecutorException {
        try {
            Executor executor = ComponentService.<Executor>getComponentByTypeAndHost(env, Executor.class, host);
            
            if(executor != null) {
                Result ret = executor.execute(command);
                
                if(ret.getExitValue() != 0) {
                    throw new ExecutorException(ret.getResultAsString("command " + command + "failed (exit code " + ret.getExitValue() + ")"));
                }
                
                log.exiting(ExecutorTools.class.getName(), "execute");
            } else {
                throw new ExecutorNotFoundException(host);
            }
        } catch (GrmException e) {
            throw new ExecutorException(e.getLocalizedMessage(), e);
        } catch (UndeclaredThrowableException e) {
            throw new ExecutorException(e.getLocalizedMessage(), e);
        }
    }
}
