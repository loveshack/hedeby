/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2007 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.executor.impl;

import com.sun.grid.grm.executor.ExecutorException;
import com.sun.grid.grm.util.Hostname;
import java.util.concurrent.Executor;

/**
 *  This exception is thrown by the executor tools if the
 *  executor on the target host has not been found
 */
public class ExecutorNotFoundException extends ExecutorException {

    private final static long serialVersionUID = -2007030201L;
    private static final String BUNDLE = "com.sun.grid.grm.executor.executor";

    /**
     * Create a new <code>ExecutorNotFoundException</code>
     *
     * @param host name of the host where the executor should run
     */
    public ExecutorNotFoundException(Hostname host) {
        super("executor.notFound", BUNDLE, Executor.class.getSimpleName(), host.getHostname());
    }
}
