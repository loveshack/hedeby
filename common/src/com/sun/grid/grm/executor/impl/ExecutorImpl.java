/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.executor.impl;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.impl.AbstractComponent;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.ConfigurationException;
import com.sun.grid.grm.config.executor.ExecutorConfig;
import com.sun.grid.grm.executor.Command;
import com.sun.grid.grm.executor.CommandContext;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.executor.ExecutorException;
import com.sun.grid.grm.executor.Result;
import com.sun.grid.grm.impl.ExecutorServiceFactory;
import com.sun.grid.grm.util.Platform;
import com.sun.grid.grm.util.TimeIntervalHelper;
import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default implementation of the Executor.
 *
 * 
 * <H2>Max runtime of the executor</H2>
 * 
 * This implementation of the executor interrupt the thread running a command
 * of the max runtime of the command is exceeded. 
 * The max runtime of a command is defined as
 * <pre>
 *     min(global max runtine, max runtime of the command)
 * </pre>
 * 
 * @todo improve javadoc
 */
public class ExecutorImpl extends AbstractComponent<ExecutorThreadPool, ExecutorConfig> implements Executor {

    private static final String BUNDLE = "com.sun.grid.grm.executor.executor";

    private static final Logger log =
            Logger.getLogger(ExecutorImpl.class.getName(),
            BUNDLE);
    
    /**
     * The default max runtime of a command is two hours
     */
    private final static TimeIntervalHelper DEFAULT_MAX_RUNTIME = new TimeIntervalHelper(2, TimeIntervalHelper.HOURS);
    
    /**
     * ThreadGroup containing all threads created by the executor
     */
    private final static ThreadGroup EXECUTOR_THREAD_GROUP = new ThreadGroup("executor");
    
    /**
     * guards access to the configuration
     */
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    
    /**
     * List with all active command environments
     */
    private final List<CommandEnv> envs = Collections.synchronizedList(new LinkedList<CommandEnv>());

    /**
     * 
     */
    private static AtomicLong jobCounter = new AtomicLong();
    
    private ExecutorConfig config;
    private TimeIntervalHelper maxRuntime;

    /**
     * Create a new instance of the executor
     * @param env  the execution env
     * @param name name of the executor
     */
    public ExecutorImpl(ExecutionEnv env, final String name) {
        super(env, name, new MyExecutorServiceFactory(name));
        maxRuntime = DEFAULT_MAX_RUNTIME;
    }

    private static ResourceBundle rb() {
        return ResourceBundle.getBundle(BUNDLE);
    }

    private File createTmpDirIfNotExist() throws ConfigurationException {
        log.entering(ExecutorImpl.class.getName(), "createTmpDirIfNotExist");
        File tempDir;

        tempDir = PathUtil.getTmpDirForComponent(getExecutionEnv(), getName());
        if (!tempDir.exists()) {
            tempDir.mkdirs();
            if (!tempDir.exists()) {
                throw new ConfigurationException("executorImpl.exception.no_temp", BUNDLE, tempDir);
            }
        }
        log.exiting(ExecutorImpl.class.getName(), "createTmpDirIfNotExist", tempDir);
        return tempDir;
    }

    /**
     *  Get the temp directory where all working.
     *  files will be stored
     *
     *  @return the temp directory
     *  @throws ConfigurationException - when the temp dir could not be created
     */
    public final File getTmpDir() throws ConfigurationException {
        log.entering(ExecutorImpl.class.getName(), "getTmpDir");
        File ret = createTmpDirIfNotExist();
        log.exiting(ExecutorImpl.class.getName(), "getTmpDir", ret);
        return ret;
    }

    /**
     *  Execute a command.
     *
     *  @param  aCommand the command which will be executed
     *  @return a object which describes the result of the command
     *  @throws com.sun.grid.grm.executor.ExecutorException on an unexpected error
     */
    public final Result execute(final Command aCommand)
            throws ExecutorException {
        log.entering(ExecutorImpl.class.getName(), "execute");

        CommandRunnable cr = new CommandRunnable(aCommand);

        Result ret = cr.execute();

        log.exiting(ExecutorImpl.class.getName(), "execute", ret);

        return ret;
    }

    /**
     * Enables the debug state in the executor. This means that all messages created by the commands are looged
     * and the directories will stay alive.
     *
     * @param keepFiles If true, the debug modus is enabled
     **/
    public final void setKeepFiles(final boolean keepFiles) {
        log.entering(ExecutorImpl.class.getName(), "setKeepFiles", keepFiles);
        
        lock.writeLock().lock();
        try {
            if (isKeepFiles() != keepFiles) {
                config.setKeepFiles(keepFiles);
            }
        } finally {
            lock.writeLock().unlock();
        }
        log.exiting(ExecutorImpl.class.getName(), "setKeepFiles");        
    }

    /**
     *  Determine if the keep files flag is set.
     *
     *  @return <code>true</code> if the keep files flag is set
     *  @see #setKeepFiles
     */
    public final boolean isKeepFiles() {
        lock.readLock().lock();
        try {
            return config.isKeepFiles();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Removes all command directories left behind.
     **/
    public final void cleanUp() {
        log.entering(ExecutorImpl.class.getName(), "cleanUp");
        try {
            File[] files = getTmpDir().listFiles();
            if (files != null) {
                CommandEnv[] activeEnvs = envs.toArray(new CommandEnv[0]);
                for (File dir : files) {
                    boolean deleteIt = true;
                    for (CommandEnv aEnv : activeEnvs) {
                        if (aEnv.getWorkingDirectory().equals(dir)) {
                            log.log(Level.FINE, "executorImpl.directoryInUse", dir);
                            deleteIt = false;
                            break;
                        }
                    }
                    if (deleteIt) {
                        removeFiles(dir);
                    }
                }
            }
        } catch (ConfigurationException e) {
            /* the temp dir does not exist, nothing to do. */
            log.log(Level.WARNING, e.getLocalizedMessage(), e);
        }
        log.exiting(ExecutorImpl.class.getName(), "cleanUp");
    }

    /**
     *  Defines the context of a command.
     */
    private final class CommandEnv implements CommandContext {

        private final String user;
        private File workingDir;
        private String error = null;

        public CommandEnv() throws ConfigurationException {
            this(System.getProperty("user.name"));
        }

        /**
         *  Create a execution environment.
         */
        public CommandEnv(final String aUser) throws ConfigurationException {
            if (aUser == null) {
                throw new NullPointerException(rb().getString("executorImpl.userNotSet"));
            }
            user = aUser;
            long nr = jobCounter.getAndIncrement();
            workingDir = new File(getTmpDir(), user.replace(" ", "_") + "_" + nr);
        }

        /**
         *  Get the working directory of the execution environment.
         *
         *  @return the working directory of the execution environment
         */
        public File getWorkingDirectory() {
            return workingDir;
        }

        /**
         *  Removes all resources the command leaves behind.
         *
         *  Currently it does only delete all files in the working directory
         */
        public void cleanUp() {
            log.entering(CommandEnv.class.getName(), "cleanUp");
            
            if (error != null) {
                log.log(Level.WARNING, "executorImpl.env.nocleanup", error);
                return;
            }
            if (!isKeepFiles()) {
                removeFiles(workingDir);
            } else {
                log.log(Level.FINE, "executorImpl.env.keepActiveFile", workingDir);
            }
            log.entering(CommandEnv.class.getName(), "cleanUp");
        }
    }

    private static boolean removeFiles(final File file) {
        log.entering(ExecutorImpl.class.getName(), "removeFiles", file);
        if (!file.exists()) {
            log.exiting(ExecutorImpl.class.getName(), "removeFiles", true);
            return true;
        }
        if (file.isDirectory()) {
            try {
                Platform.getPlatform().removeDir(file, false);
            } catch (Exception ioe) {
                log.log(Level.WARNING, "executorImpl.rmdirFailed", file);
                log.exiting(ExecutorImpl.class.getName(), "removeFiles", false);
                return false;
            }
            return file.exists();
        }
        if (!file.delete()) {
            log.log(Level.WARNING, "executorImpl.rmfileFailed", file);
            log.exiting(ExecutorImpl.class.getName(), "removeFiles", false);
            return false;
        } else {
            log.log(Level.FINER, "executorImpl.fileRemoved", file);
        }
        log.exiting(ExecutorImpl.class.getName(), "removeFiles", true);
        return true;
    }

    /**
     *  This runnable executes a command in the thread pool.
     */
    private class CommandRunnable implements Callable<Result> {

        private final Command command;

        public CommandRunnable(final Command command) {
            this.command = command;
        }

        public Result execute() throws ExecutorException {
            try {
                TimeIntervalHelper globalMaxRuntime = getMaxRuntime();
                
                long maxRuntime = globalMaxRuntime.getValueInMillis();
                if (command.getMaxRuntime() > 0) {
                    maxRuntime = Math.min(maxRuntime, command.getMaxRuntime());
                }
                //this value is needed for commands as they have their own timeout mechanism
                command.setMaxRuntime(maxRuntime);

                Future<Result> f = null;
                try {
                    f = getExecutorService().submit(this);
                } catch(RejectedExecutionException ex) {
                    throw new ExecutorException("executorImpl.exception.inactive", ex, BUNDLE);
                }
                log.log(Level.FINE, "executorImpl.submitted", command);
                return f.get();
            } catch (ExecutionException ex) {
                if (ex.getCause() instanceof ExecutorException) {
                    throw (ExecutorException) ex.getCause();

                } else {
                    ExecutorException ee =
                            new ExecutorException("executorImpl.exception.commandFailed",
                            BUNDLE, ex);

                    ee.initCause(ex);
                    throw ee;
                }
            } catch (RejectedExecutionException ree) {
                throw new ExecutorException("executorImpl.execption.commandRejected", ree, BUNDLE);
            } catch (InterruptedException ire) {
                throw new ExecutorException("executorImpl.execption.interrupted", ire, BUNDLE);
            }
        }

        public Result call() throws Exception {
            log.entering(CommandRunnable.class.getName(), "call");

            log.log(Level.FINE, "executorImpl.cmdStarted", command);

            CommandEnv env = null;
            if (command.getUser() == null) {
                env = new CommandEnv();
            } else {
                env = new CommandEnv(command.getUser());
            }
            envs.add(env);
            command.setCommandContext(env);
            try {
                Result result = command.call();
                if (result == null) {
                    throw new ExecutorException("executorImpl.exception.no_result", BUNDLE);
                }
                log.exiting(CommandRunnable.class.getName(), "call", result);
                return result;
            } finally {
                env.cleanUp();
                envs.remove(env);
                log.log(Level.FINE, "executorImpl.cmdFinished", command);
            }
        }
    }

    /**
     * The startup preparation for the <code>ExecutorImpl</code> stored only the
     * configuration object. 
     * @param config the new configuration.
     */
    @Override
    protected void prepareComponentStartup(ExecutorConfig config) {
        setConfig(config);
    }

    /**
     * The <code>ExecutorImpl</code> does not perform any special action
     * at startup, the configuration has been set already in <code>prepareComponentStartup</code>.
     * @param config the new configuration.
     * @throws ConfigurationException if the temp directory for the executor could not be created
     */
    protected final void startComponent(ExecutorConfig config) throws ConfigurationException {
        createTmpDirIfNotExist();
    }

    /**
     * The <code>ExecutorImpl</code> does not perform any special action
     * at shutdown.
     * 
     * The running commands will be stopped with the shutdown of the component
     * <code>ExecutorService</code>.
     * @param forced is ignored
     */
    protected void stopComponent(boolean forced) {
        // No action necessary
    }

    /**
     * At a reload on the configuration of the executor is exchanged. Running
     * commands will not be affected.
     * 
     * @param config the new configuration
     * @param forced is ignored
     */
    @Override
    protected void prepareComponentReload(ExecutorConfig config, boolean forced) {
        setConfig(config);
    }
    
    /**
     * Reconfigures the thread pool with the values from the new
     * configuration (maxPoolSize).
     * 
     * @param config the new configuration
     * @param forced is not used
     */
    @Override
    protected void reloadComponent(ExecutorConfig config, boolean forced) throws GrmException {
        lock.writeLock().lock();
        try {
            getExecutorService().configure(config);
        } finally {
            lock.writeLock().unlock();
        }
    }

    
    private void setConfig(ExecutorConfig config) {
        log.entering(ExecutorImpl.class.getName(), "setConfig", config);
        lock.writeLock().lock();
        try {
            this.config = config;
            if (config.getMaxCommandRuntime() == null) {
                maxRuntime = DEFAULT_MAX_RUNTIME;
            } else {
                maxRuntime = new TimeIntervalHelper(config.getMaxCommandRuntime());
            }
        } finally {
            lock.writeLock().unlock();
        }
        log.exiting(ExecutorImpl.class.getName(), "setConfig");
    }
    
    /**
     * Get the global maximum runtime of a command
     * @return the global maximum runtime of a command
     */
    public TimeIntervalHelper getMaxRuntime() {
        lock.readLock().lock();
        try {
            return maxRuntime;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     *   Get the core pool size of the executor.
     *
     *   @return the core pool size
     */
    public final int getCorePoolSize() {
        lock.readLock().lock();
        try {
            return config.getCorePoolSize();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     *   Get the max pool size of the executor.
     *
     *   @return max pool size
     */
    public final int getMaxPoolSize() {
        lock.readLock().lock();
        try {
            return config.getMaxPoolSize();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     *   Get the idle timeout for inactive threads in the pool.
     *
     *   @return the idle timeout in seconds
     */
    public final int getIdleTimeout() {
        lock.readLock().lock();
        try {
            return config.getIdleTimeout();
        } finally {
            lock.readLock().unlock();
        }
    }

    private static class MyExecutorServiceFactory implements ExecutorServiceFactory<ExecutorImpl,ExecutorThreadPool, ExecutorConfig>,
            ThreadFactory {

        private AtomicInteger threadCount = new AtomicInteger();
        private final String componentName;

        public MyExecutorServiceFactory(String componentName) {
            this.componentName = componentName;
        }

        public Class<? extends ExecutorService> getType() {
            return ExecutorThreadPool.class;
        }

        public ExecutorThreadPool createExecutor(ExecutorImpl component) {
            // We do not care about the corePoolSize because the ThreadPoolExecutor
            // does not behave in the expected way. It does not startup additional threads
            // unless the queue is full and if the queue is full and max pool size is reached
            // it rejects new tasks. This is not wanted.
            // Solution: Use a fixed thread pool with an unlimited queue
            // Disadvantage is that the idle threads will not terminate

            // One slot of the thread pool is consumed by the component thread (see JVMImpl)
            // To get the desired number of threads for tasks we increase the max pool size by 1
            int nThreads = component.getMaxPoolSize() + 1;
            return new MyExecutorService(nThreads, nThreads,
                    0L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<Runnable>(),
                    this);
        }

        public Thread newThread(Runnable r) {
            return new Thread(EXECUTOR_THREAD_GROUP, r, "Executor-" + componentName + "-" + threadCount.getAndIncrement());
        }
    }

    private static class MyExecutorService extends ThreadPoolExecutor implements ExecutorThreadPool {

        public MyExecutorService(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
        }
        
        public void configure(ExecutorConfig config) throws GrmException {
            int nThreads = config.getMaxPoolSize() + 1;
            // Update the core pool size before
            // the max pool size otherwise you get an IllegalArgumentException
            try {
                setCorePoolSize(nThreads);
                setMaximumPoolSize(nThreads);
            } catch(Exception ex) {
                throw new GrmException("executorImpl.invalidMaxPoolSize", ex, BUNDLE, config.getMaxPoolSize());
            }
        }
    }
}
