/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

/*
 * Executor.java
 *
 * Created on 5. Mai 2006, 09:31
 */

package com.sun.grid.grm.executor;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.management.Manageable;

/**
 * This interface defines how the Agent looks like.
 * <p>
 * <U>Implementation requirements:</U><p>
 * - The agent requires a home directory to store the different jobs to execute and its
 *   message file<p>
 * - The agent requires a port number to provide its service on. This could be part of
 *   the script file that starts the Agent or be in a configuration file<p>
 * <p>
 * <B>Open issues:</B><p>
 * - Security is not solved yet<p>
 * - The best way to execute a command on windows is still open<p>
 * - The Agent could ease the addition of a new host, by registering it<p>
 *
 * <B>Example code:</B><p>
 *
 * <pre>
 *
 * Executor exec = new ..;
 *
 * ShellCommand command = new ShellCommand();
 *
 * command.setUser(mUser);
 * command.addEnviroment("var", "value");
 *
 * File localFile = new File("foo.sh");
 * command.setScript(new RemoteFile(localFile, "foo.sh"));
 *
 * Result res = exec.execute(command);
 *
 * System.out.println("foo.sh exited (status " + res.getExitValue() +")");
 * </pre>
 *
 */
@Manageable
public interface Executor extends GrmComponent {

    /**
     * Execute a command.
     *
     * The Agent takes any kind of Command that implements the Command interface
     * to execute code on the server side. The user switching and command handling
     * are done insight the Command class and is nothing the Agent worries about.
     *
     * This call will block until the execution has finished.
     *
     * @param run The command that needs to be executed
     * @return the result of the command
     * @throws ExecutorException for any kind of problems (SG: needs to be more specific)
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public Result execute(Command run) throws ExecutorException, GrmRemoteException;


    /**
     * Debuging param:
     * If set, all created spooling directories will be kept alive and
     * not removed after the job is finished.
     *
     * @param isKeepFiles If true, the spooling dirs are kept
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void setKeepFiles(boolean isKeepFiles) throws GrmRemoteException;

    /**
     * Debuging param:
     * returns the current keep files state.
     *
     * @return Debug is enabled if true is returned
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public boolean isKeepFiles() throws GrmRemoteException;

    /**
     * Removes all unneeded command directories left behind.
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void cleanUp() throws GrmRemoteException;
}
