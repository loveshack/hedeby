/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/


package com.sun.grid.grm.executor;



import com.sun.grid.grm.util.I18NManager;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * This class encapsulates the result from a Command.
 * It stores the exit value and the stdout and stderr
 * output. Line by line.
 *
 */
public final class Result implements Serializable {
    
    private final static long serialVersionUID = -2007080101L;
    
    private static final transient String BUNDLE = "com.sun.grid.grm.executor.executor";

    /* if true, the return value is set */
    private boolean isExitValue = false;

    /* the return value from the called script */
    private int exitValue = 0;

    /* a list of all error messages, sorted by time */
    private List<String> errorMessages = null;

    /* a list of all output messages, sorted by time */
    private List<String> outputMessages = null;

    /* a map of return files: file name, file content */
    private List<RemoteFile> files = null;

    /**
     * Creates a new instance of Result.
     */
    public Result() {
        errorMessages = new LinkedList<String>();
        outputMessages = new LinkedList<String>();
    }

    /**
     * Sets the return value of a command.
     * @param value the return value of a command
     */
    public void setExitValue(final int value) {
        isExitValue = true;
        exitValue = value;
    }

    /**
     * Get the return value of a command. 0, if everything went fine. If no return value was set,
     * an exception is thrown.
     * @return the return value of a command
     * @throws NoExitValueException In case the exist value is not set
     */
    public int getExitValue() throws NoExitValueException {
        if (!isExitValue) {
            throw new NoExitValueException("result.returnValue.error",
                                           BUNDLE);
        }
        return exitValue;
    }

    /**
     * Adds an error message.
     * @param line an error message
     */
    public void addErrorMessage(final String line) {
        errorMessages.add(line);

    }

    /**
     * returns all the error messages.
     * @return List of error messages
     */
    public List<String> getErrorMessages() {
        return errorMessages;
    }

    /**
     * Adds a new list entry for of the output.
     * @param line an output line
     */
    public void addOutputMessage(final String line) {
        outputMessages.add(line);
    }

    /**
     * Returns all stdout output in form of a list. Each entry is a line.
     *
     * @return returns the stdout output
     */
    public List<String> getOutputMessages() {
        return outputMessages;
    }

    /**
     * Add a file to the result.
     *
     * @param file the file
     */
    public void addFile(final RemoteFile file) {
        if (files == null) {
            files = new ArrayList<RemoteFile>();
        }
        files.add(file);
    }

    /**
     * Returns a list of created files while the command was executed.
     *
     * @return A list of file names and their content
     */
    public List<RemoteFile> getFiles() {
        List<RemoteFile> ret = null;
        if (files == null) {
            ret = Collections.emptyList();
        } else {
            ret = Collections.unmodifiableList(files);
        }
        return ret;
    }

    /**
     * returns the return value as a string.
     * @return A string representation of this class
     */
    @Override
    public String toString() {
        try {
            return MessageFormat.format(
                    ResourceBundle.getBundle(BUNDLE).getString(
                    "result.toString"),
                    new Object[]{getExitValue()});

        } catch (NoExitValueException e) {
            return ResourceBundle.getBundle(BUNDLE).getString(
                    "result.returnValue.error");
        }
    }

    /**
     * Get the stdout messages as string
     * @return the stdout message as string
     */
    public String getStdoutAsString() {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        boolean first = true;
        for(String msg: getOutputMessages()) {
            if (first) {
                pw.println(I18NManager.formatMessage("result.stdout.first", BUNDLE, msg));
                first  = false;
            } else {
                pw.println(I18NManager.formatMessage("result.stdout", BUNDLE, msg));
            }
        }
        pw.close();
        return sw.getBuffer().toString();
    }

    /**
     * Get the stderr messages as string
     * @return the stderr message as string
     */
    public String getStderrAsString() {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        boolean first = true;
        for(String msg: getErrorMessages()) {
            if (first) {
                pw.println(I18NManager.formatMessage("result.stderr.first", BUNDLE, msg));
                first  = false;
            } else {
                pw.println(I18NManager.formatMessage("result.stderr", BUNDLE, msg));
            }
        }
        pw.close();
        return sw.getBuffer().toString();
    }
    
    /**
     * Convert the result of a command into a string
     * @param message first line of the returned string
     * @return the string
     */
    public String getResultAsString(String message) {
        
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        if (message != null && message.length() > 0) {
            pw.println(message);
        }
        if (isExitValue) {
            pw.println(I18NManager.formatMessage("result.exitValue", BUNDLE, exitValue));
        } else {
            pw.println(I18NManager.formatMessage("result.noExitValue", BUNDLE));
        }
        boolean first = true;
        for(String msg: getOutputMessages()) {
            if (first) {
                pw.println(I18NManager.formatMessage("result.stdout.first", BUNDLE, msg));
                first  = false;
            } else {
                pw.println(I18NManager.formatMessage("result.stdout", BUNDLE, msg));
            }
        }
        first = true;
        for(String msg: getErrorMessages()) {
            if (first) {
                pw.println(I18NManager.formatMessage("result.stderr.first", BUNDLE, msg));
                first  = false;
            } else {
                pw.println(I18NManager.formatMessage("result.stderr", BUNDLE, msg));
            }
        }
        for(RemoteFile file: getFiles()) {
            pw.println(I18NManager.formatMessage("sc.file", BUNDLE,  file.getRemoteFilename()));
            try {
                pw.println(file.getContentAsString());
            } catch (UnsupportedEncodingException ex) {
                pw.println(ex.getLocalizedMessage());
            }
        }
        pw.flush();
        return sw.getBuffer().toString();
    }

}

