/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.executor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * For a <code>RemoteFile</code> object the content of a file is included
 * if the object is serialized.
 *
 * If the object has not been serialized the store method makes a copy of a local
 * file into the target directory.
 *
 */
public class RemoteFile implements Serializable {

    private final static long serialVersionUID = -2007120401L;
    
    /** the default max file size if 64 kbyte. */
    public static final int DEFAULT_MAX_FILE_SIZE = 64 * 1024;

    /** the max size of a file which can be handled by this class.
     *
     *  Can be set by the system property <code>com.sun.grid.grm.executor.RemoteFile.maxFileSize</code>
     *  @see #DEFAULT_MAX_FILE_SIZE
     */
    public static final int MAX_FILE_SIZE;

    static {
       int maxFileSize = DEFAULT_MAX_FILE_SIZE;
       String str = System.getProperty(RemoteFile.class.getName() + ".maxFileSize");
       if (str != null) {
           try {
               maxFileSize = Integer.parseInt(str);
           } catch (NumberFormatException nfe) {
               throw new IllegalArgumentException("system property " + RemoteFile.class.getName() + ".maxFileSize must be an integer");
           }
       }
       MAX_FILE_SIZE = maxFileSize;
    }

    private File localFile;
    private String remoteFilename;
    private byte [] content;
    private String charsetName;

    /** Creates a new instance of RemoteFile.
     *
     *  @param  localFile       name of the locale file, content of this file will be serialized
     *  @param  remoteFilename  the remote jvm which deserializes the file should use this filename
     *                          to store the content
     */
    public RemoteFile(final File localFile, final String remoteFilename) {
        this(remoteFilename);
        this.localFile = localFile;
    }
    
    public RemoteFile(final String remoteFilename) {
        this.setRemoteFilename(remoteFilename);
    }

    private void writeObject(final ObjectOutputStream out) throws IOException {
        load();
        out.defaultWriteObject();
    }
    
    
    /**
     * Create a <code>Writer</code> which writes into the content
     * of the <code>RemoteFile</code>.<code>
     * The content of the <code>RemoteFile</code> will only
     * be set if the <close>method</code> of the returned <code>Writer</code>
     * is called
     * @return the writer
     */
    public Writer createWriter() {
        return new ContentWriter();
    }
    
    /**
     * Create a <code>PrintWriter</code> which prints into the content
     * of the <code>RemoteFile</code>.<code>
     * The content of the <code>RemoteFile</code> will only
     * be set if the <close>method</code> of the returned <code>PrintWriter</code>
     * is called
     * @return the <code>PrintWriter</code>
     */
    public PrintWriter createPrintWriter() {
        return new PrintWriter(createWriter());
    }
    

    /**
     *   Get the content of the remote file.
     *
     *   The content is only initialized if
     *   <ul>
     *      <li>the object has been serialized</li>
     *      <li>if the load method has been invoked</li>
     *   </ul>
     *
     *   @return the content of the remote file or <code>null</code>
     */
    public final byte[] getContent() {
        return content;
    }
    
    /**
     * Get the content of the file as string
     * @throws java.io.UnsupportedEncodingException if the content can not be converted
     * @return the content of the file as string
     */
    public String getContentAsString() throws UnsupportedEncodingException {
        try {
            load();
            return new String(content, charsetName);
        } catch (IOException ex) {
            return ex.getLocalizedMessage();
        }
    }

    /**
     * Load the content of the local file into the object.
     *
     * @throws java.io.IOException if the local could not be read
     */
    public final void load() throws IOException {
        if (content == null) {
            if(localFile != null) {
                charsetName = Charset.defaultCharset().name();
                FileInputStream fin = new FileInputStream(localFile);
                try {
                    FileChannel fc = fin.getChannel();
                    long size = fc.size();
                    if (size > MAX_FILE_SIZE) {
                        throw new IOException("file " + localFile + " too large (max. " + MAX_FILE_SIZE + " bytes)");
                    }
                    content = new byte[(int)size];

                    ByteBuffer buf = ByteBuffer.wrap(content);
                    fc.read(buf);
                } finally {
                    fin.close();
                }
            } else {
                throw new IOException("Can not load content, I have no local file");
            }
        }
    }

    /**
     *  Stores the content of a remote file into a directory.
     *  The name of the file will be &lt;dir&gt;/&lt;remoteFilename&gt;
     *
     *  @param dir directory where the file will be stored
     *  @return the file where content of the remote file has been stored
     *  @throws IOException on any IO Error
     */
    public final File store(final File dir) throws IOException {
        File file = new  File(dir, getRemoteFilename());

        FileOutputStream fout = new FileOutputStream(file);
        try {
            FileChannel fc = fout.getChannel();

            if (content == null) {
                FileInputStream fin =  new FileInputStream(this.localFile);
                try {
                    FileChannel srcFc = fin.getChannel();
                    srcFc.transferTo(0, srcFc.size(), fc);
                } finally {
                    fin.close();
                }
            } else {
                ByteBuffer buf = ByteBuffer.wrap(content);
                fc.write(buf);
            }
        } finally {
            fout.close();
        }
        return file;
    }

    /**
     * get the remote file name.
     *
     * @return the remote file name
     */
    public final String getRemoteFilename() {
        return remoteFilename;
    }

    /**
     * set the remote file name.
     *
     * @param remoteFilename  the remote file name
     */
    public final void setRemoteFilename(final String remoteFilename) {
        this.remoteFilename = remoteFilename;
    }

    /**
     *  Get the name of the charset of the local file.
     *  @return name of the charset of the local file
     */
    public final String getCharsetName() {
        return charsetName;
    }
    
    private class ContentWriter extends Writer {

        private StringBuilder sb = new StringBuilder();
        
        public void write(char[] cbuf, int offset, int len) throws IOException {
            sb.append(cbuf, offset, len);
        }

        public void flush() throws IOException {
        }

        public void close() throws IOException {
            charsetName = Charset.defaultCharset().name();
            content = sb.toString().getBytes(charsetName);
        }
    }
    
}
