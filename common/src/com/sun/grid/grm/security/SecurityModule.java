/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.Module;
import com.sun.grid.grm.ui.Command;
import java.util.Map;
import javax.security.auth.callback.CallbackHandler;

/**
 *  Interface for a Hedeby Security Module.
 */
public interface SecurityModule extends Module {
    
    /**
     * Create a security context for a server (child jvm)
     * @param jvmName name of the jvm
     * @param env execution of the system
     * @return the security context
     * @throws com.sun.grid.grm.security.GrmSecurityException if the server context cound not be created
     */
    public ServerSecurityContext createServerContext(ExecutionEnv env, String jvmName) throws GrmSecurityException;
    
    /**
     * Create a security context for a client (e.g. gconf, gstat)
     * @return return security context
     * @param env the execution env
     * @param username name of the user
     * @param callbackHandler callback handler for providing login information
     *                          (e.g. username and password)
     * @throws com.sun.grid.grm.security.GrmSecurityException if the client context can not be created
     */
    
    public ClientSecurityContext createClientContext(ExecutionEnv env, String username, CallbackHandler callbackHandler) throws GrmSecurityException;
    
    /**
     * Create the command which boostraps the security for this security module.
     *
     * @param env    the execution env
     * @param params parameters for the boostraping
     * @throws com.sun.grid.grm.GrmException if the boostrap security command can not be created
     * @return the boostrap security command
     */
    public Command<Void> createBootstrapCommand(ExecutionEnv env, Map<String,Object> params) throws GrmException;
    
    
    /**
     * Create the command which boostraps the security on a managed host.
     *
     * @param env    the execution env
     * @param params parameters for the boostraping
     * @throws com.sun.grid.grm.GrmException if the command can not be created
     * @return the command
     */
    public Command<Void> createRemoteInstCommand(ExecutionEnv env, Map<String,Object> params) throws GrmException;
    
}
