/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import java.io.IOException;
import javax.management.MBeanServer;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXServiceURL;

/**
 * Common interface for the security context of a server.
 *
 */
public interface ServerSecurityContext extends SecurityContext {
    
    /**
     * Get the remote user of the current thread (optional method)
     * @return the remote user of the current thread or <code>null</code>
     *         if the current thread is not bound to a client communication
     */
    public String getRemoteUser();
    
    /**
     * Create a JMX connector server for a MBeanServer
     * @param mbs  the MBeanServer
     * @param url  url of the MBeanServer
     * @throws java.io.IOException 
     * @throws com.sun.grid.grm.security.GrmSecurityException 
     * @return the <code>JMXConnectorServer</code>
     */
    public JMXConnectorServer createConnector(MBeanServer mbs, JMXServiceURL url) throws IOException, GrmSecurityException;
    
}
