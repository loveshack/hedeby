/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Helper class for reading passwords from the console.
 *
 * Requires native parts to read the password.
 *
 * @todo  do not sun.security.util.Password maybe its not available
 *        on all platforms
 */
public final class Password {

    private Password() {

    }

    private static boolean notInitialized = true;

    private static void initNative() {
        if (notInitialized) {
            System.loadLibrary("platform");
            notInitialized = false;
        }
    }

    /**
     * Read a password from the console.
     * @throws java.io.IOException on any IO error
     * @return the password of <code>null</code>
     */
    public static char[] readPassword() throws IOException {
        initNative();
        setEcho(false);

        BufferedReader rb = new BufferedReader(new InputStreamReader(System.in));
        try {
            String line = rb.readLine();
            return line.toCharArray();
        } finally {
            setEcho(true);
            System.out.println();
        }
    }

    private static native void setEcho(final boolean flag) throws IOException;

    /**
     *  Read a password from a file
     *  @param file the file
     *  @throws java.io.IOException on any IO error
     *  @todo  crypt passwords
     */
    public static char[] readPassword(File file) throws IOException {
        StringBuilder buf = new StringBuilder();
        Reader in = new FileReader(file);

        try {
            char[] buffer = new char[2048];
            int bytesReceived = 0;

            while ((bytesReceived = in.read(buffer)) != -1) {
                buf.append(buffer,0,bytesReceived);
            }

            char[] ret = new char[buf.length()];

            for (int i = 0; i < buf.length(); i++) {
                ret[i] = buf.charAt(i);
            }

            buf.setLength(0);
            buf.trimToSize();
            return ret;
        } finally {
            in.close();
        }
    }
    
    
    
  private static final String algorithm = "DES";
  private static final byte[] key = new byte[] {19, -12, 11, -31, 21, -51, 23, -13};
  
  private static SecretKeySpec skeySpec;
  private static Cipher cipher;
  
  /**
   */
  private static void generateKey() {
    try {
      skeySpec = new SecretKeySpec(key, algorithm);
      cipher = Cipher.getInstance(algorithm);
    } catch (NoSuchAlgorithmException nsaEx) {
      throw new IllegalStateException("Algorithm for encryption not found");
    }catch (Exception nsaEx) {
      throw new IllegalStateException("Unable to instanciate values for en-/decryption");
    }
  }
  
  

  /**
   *
   *  Obfuscate a password
   *
   *  @param password the password 
   *  @return obfuscated password
   */
  public static char[] obfuscate(char [] password) {
    try {
      generateKey();
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
      
      byte [] bytes = new byte [password.length*2];
      ByteBuffer buf = ByteBuffer.wrap(bytes);
      for(char c: password) {
           buf.putChar(c);
      }
      
      bytes = cipher.doFinal(bytes);
      buf = ByteBuffer.wrap(bytes);
      char [] ret = new char[bytes.length/2];
      for(int i = 0; i < ret.length; i++) {
          ret[i] = buf.getChar();
      }
      
      return ret;
    } catch (InvalidKeyException ex) {
        throw new IllegalStateException("Obfuscation error", ex);
    } catch (Exception ex) {
        throw new IllegalStateException("Obfuscation error", ex);
    }
  }
  
  /**
   *  Convert a obfuscated password into plain text
   *
   *  @param obfuscatedPassword  the obfuscated password
   *  @return the password in plain text
   */
  public static char [] deobfuscate(char [] obfuscatedPassword) {
    try {
      generateKey();
      cipher.init(Cipher.DECRYPT_MODE, skeySpec);
      
      byte [] bytes = new byte [obfuscatedPassword.length*2];
      ByteBuffer buf = ByteBuffer.wrap(bytes);
      for(char c: obfuscatedPassword) {
           buf.putChar(c);
      }
      
      byte [] res = cipher.doFinal(bytes);

      buf = ByteBuffer.wrap(res);

      char [] ret = new char[res.length/2];
      for(int i = 0; i < ret.length; i++) {
          ret[i] = buf.getChar();
      }
      return ret;
      
    } catch (IllegalBlockSizeException ex) {
        throw new IllegalStateException("Deobfuscation error", ex);
    } catch (BadPaddingException ex) {
        throw new IllegalStateException("Deobfuscation error", ex);
    } catch (InvalidKeyException ex) {
        throw new IllegalStateException("Deobfuscation error", ex);
    }
  }
      
    
    
}
