/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import com.sun.grid.grm.cli.AbstractCli;
import java.util.ResourceBundle;

/**
 * This class defines the different user privileges.
 */
public class UserPrivilege {
    /**
     * The user executing the code must be administrator
     */
    public final static String ADMINISTRATOR = "ADMINISTRATOR";
    
    /**
     * The user executing  the code must have the file permissions to read the
     * bootstrap configuration
     */
    public final static String READ_BOOTSTRAP_CONFIG  = "READ_BOOTSTRAP_CONFIG";

    /**
     * The user executing the code must have the file permissions to write the
     * bootstrap configuration
     */
    public final static String WRITE_BOOTSTRAP_CONFIG = "WRITE_BOOTSTRAP_CONFIG";
    
    /**
     * User needs the file permissions to write the keystore
     */
    public final static String WRITE_KEYSTORE = "WRITE_KEYSTORE";

    /**
     * User need read access on the local spool directory
     */
    public final static String READ_LOCAL_SPOOL_DIR = "READ_LOCAL_SPOOL_DIR";
    
    /**
     * User need write access on the local spool directory
     */
    public final static String WRITE_LOCAL_SPOOL_DIR = "WRITE_LOCAL_SPOOL_DIR";
    
    /**
     * The command must be executed as super user. The super user has the ability 
     * to execute a command as different user and can change the ownership of
     * files.
     */
    public final static String SUPER_USER = "SUPER_USER";

    /**
     * Contains the list of all defined user privileges
     */
    public final static String[] ALL_PRIVILEGES = {
        ADMINISTRATOR,
        READ_BOOTSTRAP_CONFIG,
        WRITE_BOOTSTRAP_CONFIG,
        WRITE_KEYSTORE,
        WRITE_LOCAL_SPOOL_DIR,
        SUPER_USER
    };
    
    
    private final static String BUNDLE = "com.sun.grid.grm.security.privileges";

    /**
     * Get the localized name of an user privilege
     * @param name the code name of the user privilege
     * @return the localized name
     */
    public static String getName(String name) {
        return AbstractCli.getName(ResourceBundle.getBundle(BUNDLE), name);
    }
    
    /**
     * Get the localized description of an user privilege.
     * 
     * @param name the code name of the user privilege
     * @return the localized description of an user privilege.
     */
    public static String getDescription(String name) {
        return AbstractCli.getDescription(ResourceBundle.getBundle(BUNDLE), name);
    }
    
    /**
     * Get the localized long description of an user privilege.
     * 
     * The long description will be generated into the man page
     * 
     * @param name the code name of the user privilege
     * @return the localized long description of an user privilege
     */
    public static String getLongDescription(String name) {
        return AbstractCli.getLongDescription(ResourceBundle.getBundle(BUNDLE), name);
    }
}
