/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import java.io.File;
import javax.security.auth.callback.Callback;

/**
 *  This <code>Callback</code> can be used to query files.
 */
public class FileCallback implements Callback {
    
    private File file;
    
    private int type;
    
    /** 
     * Creates a new FileCallback.
     */
    public FileCallback() {
    }

    /** 
     * Creates a new FileCallback.
     *
     * @param type the type of the callback
     */
    public FileCallback(int type) {
       this.setType(type);
    }
    
    /**
     *  Get the file.
     *
     *  @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     *  Set the file.
     *
     *  @param file the file
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Get the type.
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * Set the type.
     * @param type the type
     */
    public void setType(int type) {
        this.type = type;
    }

}
