/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;


import com.sun.grid.grm.util.I18NManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.ConfirmationCallback;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.TextInputCallback;
import javax.security.auth.callback.TextOutputCallback;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 * This CallbackHandler reads the required information for Callbacks from
 * System.in.
 *
 * For passwords the <code>Password.readPassword</code> is used. This CallbackHandler
 * supports PasswordCallback, NameCallback, ConfirmationCallback and 
 * TextInputCallback
 *
 * @see Password#readPassword
 */
public class ConsoleCallbackHandler implements CallbackHandler {

    private static final String BUNDLE = "com.sun.grid.grm.security.commonsecurity";

    private void prompt(final String prompt, final String defaultValue) {
        
        System.out.print(prompt);
        
        if (defaultValue != null) {
            System.out.print(" [");
            System.out.print(defaultValue);
            System.out.print("]");
        }
        
        System.out.print(" > ");
        System.out.flush();
    }
    
    
    protected void handle(Callback cb) throws IOException,
            UnsupportedCallbackException {
        if (cb instanceof PasswordCallback) {
            
            PasswordCallback pwCallback = (PasswordCallback)cb;
            prompt(pwCallback.getPrompt(), null);
            
            pwCallback.setPassword(Password.readPassword());
            
        } else if (cb instanceof NameCallback) {
            NameCallback ncb = (NameCallback)cb;
            prompt(ncb.getPrompt(), ncb.getDefaultName());
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(System.in));
            String name = br.readLine();
            
            if (name == null || name.length() == 0) {
                name = ncb.getDefaultName();
            }
            ncb.setName(name);
        } else if (cb instanceof TextInputCallback) {
            TextInputCallback ticb = (TextInputCallback)cb;
            prompt(ticb.getPrompt(), ticb.getDefaultText());
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(System.in));
            String text = br.readLine();
            
            if (text == null || text.length() == 0) {
                text = ticb.getDefaultText();
            }
            
            ticb.setText(text);
        } else if ( cb instanceof TextOutputCallback) {
            TextOutputCallback tcb = (TextOutputCallback)cb;
            switch(tcb.getMessageType()) {
                case TextOutputCallback.ERROR:
                    System.out.print(I18NManager.formatMessage("ConsoleCallbackHandler.error", BUNDLE));
                    break;
                case TextOutputCallback.WARNING:
                    System.out.print(I18NManager.formatMessage("ConsoleCallbackHandler.warning", BUNDLE));
                    break;
                default:
                    // ignore
            }
            System.out.println(tcb.getMessage());
        } else if ( cb instanceof ConfirmationCallback) {
            handleConfirmationCallback((ConfirmationCallback)cb);
        } else {
            throw new UnsupportedCallbackException(cb);
        }
    }
    
    /**
     * Handle callbacks.
     *
     * @param callbacks  the callback
     * @throws java.io.IOException on any IO error
     * @throws javax.security.auth.callback.UnsupportedCallbackException if
     *                the unsupported callback is found
     */
    public final void handle(final Callback[] callbacks) throws IOException,
            UnsupportedCallbackException {
        for (Callback cb : callbacks) {
            handle(cb);
        }
    }
    
    private void handleConfirmationCallback(ConfirmationCallback ccb) throws IOException {
        
        // Build the message
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        
        pw.print(ccb.getPrompt());
        pw.print("[");
        switch(ccb.getOptionType()) {
            case ConfirmationCallback.UNSPECIFIED_OPTION:
                boolean first = true;
                for(String opt: ccb.getOptions()) {
                    if(first) {
                        first = false;
                    } else {
                        pw.print("/");
                    }
                    pw.print(opt);
                }
                break;
            case ConfirmationCallback.YES_NO_CANCEL_OPTION:
                pw.print(I18NManager.formatMessage("ConsoleCallbackHandler.yes_no_cancel", BUNDLE));
                break;
            case ConfirmationCallback.YES_NO_OPTION:
                pw.print(I18NManager.formatMessage("ConsoleCallbackHandler.yes_no", BUNDLE));
                break;
            case ConfirmationCallback.OK_CANCEL_OPTION:
                pw.print(I18NManager.formatMessage("ConsoleCallbackHandler.ok_cancel", BUNDLE));
                break;
            default:
                throw new IllegalStateException("Unknown option type " + ccb.getOptionType());
        }
        
        pw.print("] (");
        switch(ccb.getDefaultOption()) {
            case ConfirmationCallback.YES:
                pw.print(I18NManager.formatMessage("ConsoleCallbackHandler.yes", BUNDLE));
                break;
            case ConfirmationCallback.NO:
                pw.print(I18NManager.formatMessage("ConsoleCallbackHandler.no", BUNDLE));
                break;
            case ConfirmationCallback.CANCEL:
                pw.print(I18NManager.formatMessage("ConsoleCallbackHandler.cancel", BUNDLE));
                break;
            default:
                pw.println(ccb.getOptions()[ccb.getDefaultOption()]);
        }
        
        pw.print(") > ");
        pw.flush();
        
        String message = sw.getBuffer().toString();
        
        
        while(true) {
            
            System.out.print(message);
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(System.in));
            String text = br.readLine();
            
            if (text == null || text.length() == 0) {
                ccb.setSelectedIndex(ccb.getDefaultOption());
                return;
            } else {
                
                switch(ccb.getOptionType()) {
                    
                    case ConfirmationCallback.YES_NO_CANCEL_OPTION:
                        if(I18NManager.formatMessage("ConsoleCallbackHandler.yes", BUNDLE).equalsIgnoreCase(text)) {
                            ccb.setSelectedIndex(ConfirmationCallback.YES);
                            return;
                        } else if (I18NManager.formatMessage("ConsoleCallbackHandler.no", BUNDLE).equalsIgnoreCase(text)) {
                            ccb.setSelectedIndex(ConfirmationCallback.NO);
                            return;
                        } else if (I18NManager.formatMessage("ConsoleCallbackHandler.cancel", BUNDLE).equalsIgnoreCase(text)) {
                            ccb.setSelectedIndex(ConfirmationCallback.CANCEL);
                            return;
                        }
                        break;
                    case ConfirmationCallback.YES_NO_OPTION:
                        if(I18NManager.formatMessage("ConsoleCallbackHandler.yes", BUNDLE).equalsIgnoreCase(text)) {
                            ccb.setSelectedIndex(ConfirmationCallback.YES);
                            return;
                        } else if (I18NManager.formatMessage("ConsoleCallbackHandler.no", BUNDLE).equalsIgnoreCase(text)) {
                            ccb.setSelectedIndex(ConfirmationCallback.NO);
                            return;
                        }
                        break;                        
                    case ConfirmationCallback.OK_CANCEL_OPTION:
                        if(I18NManager.formatMessage("ConsoleCallbackHandler.ok", BUNDLE).equalsIgnoreCase(text)) {
                            ccb.setSelectedIndex(ConfirmationCallback.OK);
                            return;
                        } else if (I18NManager.formatMessage("ConsoleCallbackHandler.cancel", BUNDLE).equalsIgnoreCase(text)) {
                            ccb.setSelectedIndex(ConfirmationCallback.CANCEL);
                            return;
                        }
                        break;                        
                    default:
                        String [] opts = ccb.getOptions();
                        if(opts != null) {
                            for(int i = 0; i < opts.length; i++) {
                                if(opts[i].equals(text)) {
                                    ccb.setSelectedIndex(i);
                                    return;
                                }
                            }
                        }
                }
                System.out.println(I18NManager.formatMessage("ConsoleCallbackHandler.invalidOption", BUNDLE, text));
            }
            
        }
    }
    
}
