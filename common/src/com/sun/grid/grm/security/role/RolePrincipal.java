/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security.role;

import java.io.Serializable;
import java.security.Principal;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Collections;
import java.util.List;

/**
 * The role principal assosiates a role to a subject.
 * Needed this class because generated Role doesn't implement Principal
 * and only java.security.Principal's are able to be added to subject
 *
 */
public class RolePrincipal implements Principal, Serializable {
    private final String name;

    public RolePrincipal(String name) {
        this.name = name;
    }
    
    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * @param obj the object
     * @return <code>true</code> if <code>obj</code> is "equal to" this one.
     */
    
   public final boolean equals(final Object obj) {

      if (!(obj instanceof RolePrincipal)) {
         return false;
      }
      return ((RolePrincipal)obj).getName().equals(getName());
   }

    /**
     * get the hashcode of this one.
     * @return the hashcode
     */
   public final int hashCode() {
      return getName().hashCode();
   }

   /**
    * Returns a string representation of the object.
    *
    * @return the string representation of the object
    */
   public final String toString() {
      return "RolePrinicipal " + getName();
   }
   
   /**
    * @return name of role
    */
   public String getName() {
       return name;
   }
}
