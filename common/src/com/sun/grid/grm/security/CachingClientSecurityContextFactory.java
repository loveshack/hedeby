/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.Modules;
import com.sun.grid.grm.util.HostAndPort;
import java.util.HashMap;
import java.util.Map;
import javax.security.auth.callback.CallbackHandler;

/**
 *  This class caches the information for creating a client security context
 *  of a hedeby system.
 * 
 *  <p>Due to constraints in the JSSE SSL implementatiation it is not possible
 *     to have more then one security context for the same hedeby system inside
 *     of one jvm. Subsequent calls of <code>ExecutionEnvFactory.newClientInstance</code>
 *     fails because each class wants to create a ClientSecurityContext for the
 *     hedeby system.</p>
 * 
 *  <p>This class provides a how to reuse instance of ClientSecurityContext</p>
 * 
 *  <H3>Example</H3>
 * 
 *  <pre>
 *  CachingClientSecurityContextFactory factory = new CachingClientSecurityContextFactory()
 * 
 *  factory.registerContext("system1", "sdm_admin", new MyCallbackHandler());
 *  factory.registerContext("system2", "sdm_admin", new MyCallbackHandler());
 * 
 *  ExecutionEnv env = ExecutionEnvFactory.newClientInstance("system1", PreferencesType.SYSTEM, factory);
 *  ExecutionEnv env1 = ExecutionEnvFactory.newClientInstance("system2", PreferencesType.SYSTEM, factory);
 * 
 *  // The new context should use the keystore of user joe
 *  // The previous created execution env for system1 will also use 
 *  // the keystore of user joe
 *  factory.registerContext("system2", "joe", new MyCallbackHandler());
 * 
 *  ExecutionEnv env2 = ExecutionEnvFactory.newClientInstance("system1", PreferencesType.SYSTEM, factory);
 *  
 *  // After removing the context of system1 no further communication over env and env1 
 *  // is possible
 *  factory.removeContext("system1");
 *  </pre>
 * 
 */
public class CachingClientSecurityContextFactory implements ClientSecurityContextFactory {

    public static final String BUNDLE = "com.sun.grid.grm.security.commonsecurity";

    private final Map<HostAndPort, ContextInfo> registeredContexts = new HashMap<HostAndPort, ContextInfo>();
    
    /**
     * Register the information for creating the security context for a hedeby system
     * @param csInfo     the cs contact information for the hedeby system
     * @param username    name of the user which should be used for this hedeby system
     * @param callbackHandler the callbackHandler
     */
    public void registerContext(HostAndPort csInfo, String username, CallbackHandler callbackHandler) {
        synchronized(registeredContexts) {
            unregisterContext(csInfo);
            registeredContexts.put(csInfo, new ContextInfo(username, callbackHandler));
        } 
    }
    
    /**
     * Unregister the context for a system name
     * @param csInfo     the cs contact information for the hedeby system
     */
    public void unregisterContext(HostAndPort csInfo) {
        synchronized(registeredContexts) {
            ContextInfo ctxInfo = registeredContexts.get(csInfo);
            if(ctxInfo != null && ctxInfo.ctx != null) {
                ctxInfo.ctx.destroy();
            }
        } 
    }
    
    /**
     * Looks in the cache for an existing ClientSecurityContext for the system
     * if is does not exit a new ClientSecurityContext is created and stored
     * in the cache.
     * @param env  the execution envt
     * @return the ClientSecurityContext
     * @throws com.sun.grid.grm.security.GrmSecurityException if the ClientSecurityContext can not be created
     */
    public ClientSecurityContext newInstance(ExecutionEnv env) throws GrmSecurityException {
        synchronized(registeredContexts) {
            ContextInfo ctxInfo = registeredContexts.get(env.getCSInfo());
            if (ctxInfo == null) {
                throw new GrmSecurityException("CachedClientSecurityContextFactory.noctx", BUNDLE, env.getSystemName());
            }
            if(ctxInfo.ctx == null) {
                ctxInfo.ctx = Modules.getSecurityModule().createClientContext(env, ctxInfo.username, ctxInfo.callbackHandler);
            }
            return ctxInfo.ctx;
        }
    }

    private static class ContextInfo {
        private final String username;
        private final CallbackHandler callbackHandler;
        
        private ClientSecurityContext ctx;
        
        public ContextInfo(String username, CallbackHandler callbackHandler) {
            this.username = username;
            this.callbackHandler = callbackHandler;
        }
    }

    
}
