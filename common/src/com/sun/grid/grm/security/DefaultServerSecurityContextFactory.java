/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.security;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.FactoryWithParamAndException;
import com.sun.grid.grm.bootstrap.Modules;

/**
 * Default implemenation of a security context factory for a service (JVM).
 */
public class DefaultServerSecurityContextFactory implements FactoryWithParamAndException<SecurityContext,ExecutionEnv,GrmSecurityException> {

    private final String jvmName;

    /**
     * Create a new instance of <tt>DefaultServerSecurityContextFactory</tt>
     * @param jvmName the name of the jvm
     */
    public DefaultServerSecurityContextFactory(String jvmName) {
        this.jvmName = jvmName;
    }

    /**
     * Create the security context
     * @param env the execution enviroment
     * @return the security context
     * @throws com.sun.grid.grm.security.GrmSecurityException if the security context could not be created
     */
    public SecurityContext newInstance(ExecutionEnv env) throws GrmSecurityException {
        SecurityModule secModule = Modules.getSecurityModule();
        return secModule.createServerContext(env, jvmName);
    }
}
