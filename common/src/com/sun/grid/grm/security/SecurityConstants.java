/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.security;

/**
 * Constants for the security
 *
 */
public final class SecurityConstants {
    
    /**
     * Name of the ca component key 
     */
    public static final String CA_KEY = "ca";

    /** default ssl protocal for Haithabu. */
    public static final String SSL_PROTOCOL = "SSL";

    /** name of the used keystore algorithm */
    public static final String KEYSTORE_ALGORITHM = "JKS";

    /** name of the KeyManagerFactory algorithm */
    public static final String KEYMANAGERFACTORY_ALGORITHM = "NewSunX509";
    
    /** name of the Certificate factory (X.509) */
    public static final String CERTIFICATE_FACTORY = "X.509";
    
    /** name of the default trustmanager algorithm (PKIX) */
    public static final String TRUSTMANAGER_ALGORITHM = "PKIX";
    
    public static final String CACERT_FILE = "cacert.pem";
    public static final String CRL_FILE = "ca-crl.pem";

    /**
     *  This constants is the type of the FileCallbackHandler
     *  which is used to query a keystore file for a user.
     */
    public final static int KEYSTORE_FILE_CALLBACK = 1;
    
    /**
     *  This constants is the type of the FileCallbackHandler
     *  which is used to query a ca cert file.
     */
    public final static int CACERT_FILE_CALLBACK  = 2;
    
}
