/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.validate;

import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmSingleton;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.InvalidComponentNameException;
import com.sun.grid.grm.bootstrap.SystemUtil;
import com.sun.grid.grm.config.common.Component;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.MultiComponent;
import com.sun.grid.grm.config.common.Singleton;
import com.sun.grid.grm.resource.ResourceProvider;
import com.sun.grid.grm.util.I18NManager;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Validator for GlobalConfig object. Check the name uniqueness of jvms and components
 */ 
@Validation(type=GlobalConfig.class)
public class GlobalConfigValidator implements Validator<GlobalConfig> {
    private final String BUNDLE = "com.sun.grid.grm.validate.messages";
    private final Logger log = Logger.getLogger(GlobalConfigValidator.class.getName(), BUNDLE);

    /** Creates a new instance of GlobalConfigValidator */
    public GlobalConfigValidator() {
    }

    public void validate(ExecutionEnv env, GlobalConfig global, GlobalConfig old) throws GrmValidationException {

        List<String> errors = new LinkedList<String>();
        Set<String> names = new HashSet<String>();
        //Check if system name is valid
        try {
            SystemUtil.validateSystemName(global.getName());
        } catch (InvalidComponentNameException ex) {
            errors.add(I18NManager.formatMessage("globalconfigvalidator.invalid_systemname", BUNDLE, global.getName()));
        }
        
        List<String> rpNames = new LinkedList<String>();
        
        for (JvmConfig jvm : global.getJvm()) {
            try {
                SystemUtil.validateComponentName(jvm.getName());
                if (!names.contains(jvm.getName())) {
                    names.add(jvm.getName());
                } else {
                    //not unique name
                    errors.add(I18NManager.formatMessage("globalconfigvalidator.not_unique_jvmname", BUNDLE, jvm.getName()));
                }
            } catch (InvalidComponentNameException ex) {
                errors.add(I18NManager.formatMessage("globalconfigvalidator.invalid_jvmmname", BUNDLE, jvm.getName()));
            }
            //check components for jvm
            for (Component c : jvm.getComponent()) {
                try {
                    //validate name string
                    SystemUtil.validateComponentName(c.getName());
                    //validate uniqueness in system
                    if (!names.contains(c.getName())) {
                        names.add(c.getName());
                    } else {
                        //not unique name
                        errors.add(I18NManager.formatMessage("globalconfigvalidator.not_unique_componentname", BUNDLE, c.getName()));
                    }
                } catch (InvalidComponentNameException ex) {
                    errors.add(I18NManager.formatMessage("globalconfigvalidator.invalid_componentname", BUNDLE, c.getName()));
                }
                
                Class clazz;
                try {
                    //check if class exists
                    clazz = Class.forName(c.getClassname());
                    //each component should implement GrmComponent interface
                    if (!GrmComponent.class.isAssignableFrom(clazz)) {
                        errors.add(I18NManager.formatMessage("globalconfigvalidator.component_notgrmcomponent", BUNDLE, c.getClassname(), c.getName(), GrmComponent.class.getName()));
                    }
                    //check if singleton implements GrmSingleton
                    if((c instanceof Singleton) && !(GrmSingleton.class.isAssignableFrom(clazz))) {
                       errors.add(I18NManager.formatMessage("globalconfigvalidator.singletoncomponent_not_grmsingleton", BUNDLE, c.getClassname(), c.getName(), GrmSingleton.class.getName()));
                    }
                    //Check if MultiComponent implements GrmSingleton
                    if ((c instanceof MultiComponent) && (GrmSingleton.class.isAssignableFrom(clazz))) {
                       errors.add(I18NManager.formatMessage("globalconfigvalidator.multicomponent_is_grmsingleton", BUNDLE, c.getClassname(), c.getName(), GrmSingleton.class.getName()));
                    }
                    if (c instanceof ResourceProvider) {
                        rpNames.add(c.getName());
                    }
                } catch (ClassNotFoundException ex) {
                    errors.add(I18NManager.formatMessage("globalconfigvalidator.componentclassname_not_available", BUNDLE, c.getClassname(), c.getName()));
                }
            }
        }
        
        if(rpNames.size() > 1) {
            errors.add(I18NManager.formatMessage("globalconfigvalidator.multiple_rps", BUNDLE, rpNames.toString()));
        }
        //prevent from changing system name
        if ((!global.getName().equals(old.getName()) && (!global.getName().equals(env.getSystemName()))) ) {
            errors.add(I18NManager.formatMessage("globalconfigvalidator.system_name_changed", BUNDLE, old.getName(), global.getName(), env.getSystemName()));
        }
        //i626 - compare number of jvms
        List<JvmConfig> oldJvms = old.getJvm();
        List<JvmConfig> newJvms = global.getJvm();
        try {
            if (oldJvms.size() == newJvms.size()) {
                compare(oldJvms, newJvms, env.getCSPort());
            } else {
                errors.add(I18NManager.formatMessage("globalconfigvalidator.jvm_number_changed", BUNDLE));
            }
        } catch (GrmException ex) {
            errors.add(ex.getLocalizedMessage());
        }
        
        //if any errros, throw validation exception
        if (!errors.isEmpty()) {
            throw new GrmValidationException(errors);
        }     
    }
    //Makes a comparison of old and new configuration - checks whether names are changed and cs_port
    private void compare(List<JvmConfig> o, List<JvmConfig> n, int csPort) throws GrmException {
        //get cs port - we have strong dependency on cs_vm name and its port
        Set<String> tmp = new HashSet<String>(o.size());
        int tmpPort = -1;
        for (JvmConfig jvm : o) {
            if (jvm.getName().equals(BootstrapConstants.CS_JVM)) {
                tmpPort = jvm.getPort();
            }
            tmp.add(jvm.getName());
        }
        if (tmpPort < 0) {
            //cs_vm  not found in old config, warning
            log.log(Level.WARNING, "globalconfigvalidator.no_cs_vm_old", new Object[] {BootstrapConstants.CS_JVM});

        } else if (tmpPort != csPort) {
            log.log(Level.WARNING, "globalconfigvalidator.wrong_cs_port_old", new Object[] {tmpPort, csPort});
        }
        for (JvmConfig jvm : n) {
            if (jvm.getName().equals(BootstrapConstants.CS_JVM)) {
                if (jvm.getPort() != csPort) {
                    //cs+port changed
                    throw new GrmException(I18NManager.formatMessage("globalconfigvalidator.csport_changed", BUNDLE,BootstrapConstants.CS_JVM, jvm.getPort(), csPort));
                }
                tmpPort = 0;
            }
            if (tmp.isEmpty()) {
                //we have more jvms
                throw new GrmException(I18NManager.formatMessage("globalconfigvalidator.jvmnames_changed", BUNDLE));
            }
            tmp.remove(jvm.getName());
        }
        if (tmp.isEmpty()) {
            return;
        }
        //jvmnames changed
        throw new GrmException(I18NManager.formatMessage("globalconfigvalidator.jvmnames_changed", BUNDLE));
    }
    
}
