/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.validate;

import com.sun.grid.grm.bootstrap.ExecutionEnv;

/**
 * This interface should be implemented by all object Validators in Hedeby system.
 * It defines that class implementing this interface is Validator. Class implementing
 * Validator should also have defined annotion com.sun.grid.grm.validate.Validation
 * @param T the type which is validated by this validator
 */

public interface Validator <T>{

    /**
     * The method contains whole bussines logic that is specific for
     * the validator. It is used by a validation invoker a ValidatorService.
     * ValidatorService knows how to run validators.
     *
     *
     * The ValidationService is a helper class to be used by user to run validators
     * easily.
     *
     * Sample:
     * How to call validation on object:
     *
     * <code>
     * ExecutionEnv env = getExecutionEnv();
     * ExecutorConfig conf = new ExecutorConfig();
     * ValidationService.validate(env, conf, oldConf);
     * </code>
     *
     * Sample:
     * How to implement Validator class:
     * <code>
     * (at)Validation (type=ExecutorConfig.class)
     * public class ObjectValidator implements Validator<ExecutorConfig> {
     *  public void validate(ExecutionEnv env, ExecutorConfig conf, ExecutorConfig conf) throws GrmValidationException {
     *  //Validation logic
     *  }
     * }
     * </code>
     *
     * @param env Instance of an ExecutionEnv that is bound to an invoker
     * of a validator
     * @param obj  the object which should be validated
     * specifies the type of validator.
     * @param old  old object which should be validated against by obj.
     * Can be null.
     * @throws com.sun.grid.grm.validate.GrmValidationException if validation of object failed
     */
    public void validate(ExecutionEnv env, T obj, T old) throws GrmValidationException;
    
}
