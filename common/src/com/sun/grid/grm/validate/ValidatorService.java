/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.validate;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.Module;
import com.sun.grid.grm.bootstrap.Modules;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The <code>ValidatorService</code> provides the convenient way to invoke
 * a validators.
 *
 * This helper class should be used by user to run validators.
 * Sample:
 * How to call validation on object:
 *
 * <code>
 * ExecutionEnv env = getExecutionEnv();
 * ExecutorConfig conf = new ExecutorConfig();
 * ValidationService.validate(env, conf);
 * </code>
 *
 * Sample:
 * How to implement Validator class:
 * <code>
 * (at)Validation (type=ExecutorConfig.class)
 * public class ObjectValidator implements Validator <ExecutorConfig>{
 *  public void validate(ExecutionEnv env, ExecutorConfig conf) throws GrmValidationException {
 *  //Validation logic
 *  }
 * }
 * </code>
 */
public class ValidatorService {

    /**
     * i18n Bundle name
     */
    private static Map<Class, Class> validators = null;
    private static final String BUNDLE_NAME = "com.sun.grid.grm.validate.messages";

    //BUNDLE where validators for common package are defined
    private static final String VALIDATORS_BUNDLE = "com.sun.grid.grm.validate.validators";

    //key entry which value represents Validator classes separated by space sign
    private static final String VALIDATORS = "validators";
    private static final String DEFAULT_VALIDATOR = "default";

    //Logger stuff
    private static Logger log;

    /** Creates a new instance of ValidatorService */
    private ValidatorService() {
    }

    private static Logger log() {
        if (log == null) {
            log = Logger.getLogger(ValidatorService.class.getName(), BUNDLE_NAME);
        }

        return log;
    }

    /**
     * A convenient way to invoke a validator. For each invocation, the Validator classes
     * are gathered. Based on type of validated object the proper Validator is called.
     *
     * @param env ExecutionEnv that is used to create a proxy for ConfigurationService
     * @param obj - object that is going to be validated
     * @throws com.sun.grid.grm.GrmException everytime a problem during
     * Validator invocation occurs.
     * @throws com.sun.grid.grm.validate.GrmValidationException when validation of
     * object fails
     */
    @SuppressWarnings(value = "unchecked")
    public static void validate(ExecutionEnv env, Object obj) throws GrmValidationException, GrmException {
        validate(env, obj, null);
    }
    /**
     * A convenient way to invoke a validator. For each invocation, the Validator classes
     * are gathered. Based on type of validated object the proper Validator is called.
     *
     * @param env ExecutionEnv that is used to create a proxy for ConfigurationService
     * @param obj - object that is going to be validated
     * @param old - old object, that is going to be validated against, can be null
     * @throws com.sun.grid.grm.GrmException everytime a problem during
     * Validator invocation occurs.
     * @throws com.sun.grid.grm.validate.GrmValidationException when validation of
     * object fails
     */
    @SuppressWarnings(value = "unchecked")
    public static void validate(ExecutionEnv env, Object obj, Object old) throws GrmValidationException, GrmException {
        if (validators == null) {
            validators = getValidators();
        }

        //null cannot be validated
        if (obj == null) {
            throw new GrmException("validatorservice.no_object_to_validate");
        }

        Class validated = null;
        Class validator = null;
        validated = obj.getClass();
        //Get the name of the proper Validator Class
        validator = validators.get(validated);

        if (validator == null) {
            //log the message validator is not available for specified Object
            log().log(Level.FINE, "validatorservice.validator_not_available", validated);

            return;
        }

        Constructor con;
        Validator f = null;

        try {
            //Get constractor of Validator class and create instance
            con = validator.getConstructor();
            f = (Validator) con.newInstance();
        } catch (SecurityException ex) {
            throw new GrmException("validatorservice.can_create_validator_instance",
                    BUNDLE_NAME, validator, ex.getLocalizedMessage());
        } catch (IllegalArgumentException ex) {
            throw new GrmException("validatorservice.can_create_validator_instance",
                    BUNDLE_NAME, validator, ex.getLocalizedMessage());
        } catch (InvocationTargetException ex) {
            throw new GrmException("validatorservice.can_create_validator_instance",
                    BUNDLE_NAME, validator, ex.getLocalizedMessage());
        } catch (IllegalAccessException ex) {
            throw new GrmException("validatorservice.can_create_validator_instance",
                    BUNDLE_NAME, validator, ex.getLocalizedMessage());
        } catch (InstantiationException ex) {
            throw new GrmException("validatorservice.can_create_validator_instance",
                    BUNDLE_NAME, validator, ex.getLocalizedMessage());
        } catch (NoSuchMethodException ex) {
            throw new GrmException("validatorservice.can_create_validator_instance",
                    BUNDLE_NAME, validator, ex.getLocalizedMessage());
        }

        //Call validate method on Validator
        f.validate(env, obj, old);
    }

    /*
     * Get all defined Validator classes
     */
    private static Map<Class, Class> getValidators() {
        List<Class<? extends Validator>> validatorClass = new LinkedList<Class<? extends Validator>>();
        Map<Class, Class> ret = new HashMap<Class, Class>();
        validatorClass.addAll(getCommonClasses());
        validatorClass.addAll(getExtensionClasses());

        for (Class<? extends Validator> cl : validatorClass) {
            Validation a = cl.getAnnotation(Validation.class);

            //Check if Validation annotation is present
            if (a != null) {
                // store Validator class in Map
                ret.put(a.type(), cl);
            } else {
                //Log message that defined Validator class does not have required annotation-todo
                log().log(Level.WARNING, "validationservice.annotation_missing",
                        cl.getName());
            }
        }

        return ret;
    }

    /*
     *  Load all extension classes from all Modules
     */
    private static List<Class<? extends Validator>> getExtensionClasses() {
        List<Class<? extends Validator>> ret = new LinkedList<Class<? extends Validator>>();

        for (Module module : Modules.getModules()) {
            ret.addAll(module.getValidatorExtensions());
        }

        return ret;
    }

    /*
     * Load Validator classes from property file
     */
    @SuppressWarnings(value = "unchecked")
    private static List<Class<? extends Validator>> getCommonClasses() {
        Set<Class<? extends Validator>> retTmp = new HashSet<Class<? extends Validator>>();
        List<Class<? extends Validator>> ret = new LinkedList<Class<? extends Validator>>();
        String clazz;

        try {
            //read validator classes defined in properties file
            ResourceBundle rb = ResourceBundle.getBundle(VALIDATORS_BUNDLE);
            clazz = rb.getString(VALIDATORS);
        } catch (MissingResourceException ex) {
            //file does not exist so no Validators defined-todo
            log().log(Level.WARNING,
                    "validatorservice.could_not_load_validator_classes_from_file",
                    new Object[]{VALIDATORS_BUNDLE, ex.getLocalizedMessage()});

            return ret;
        }

        //check if VALIDATORS key was found
        if (clazz == null) {
            log().log(Level.WARNING, "validatorservice.common.null");

            return ret;
        }

        //Validator classes should be seperated by space
        String[] classes = clazz.split(" ");

        for (int i = 0; i < classes.length; i++) {
            Class tmp;

            try {
                //Check if class exists
                tmp = Class.forName(classes[i]);
            } catch (ClassNotFoundException ex) {
                //log error and skip entry
                log().log(Level.WARNING,
                        "validatorservice.validator_class_not_found", classes[i]);

                continue;
            }

            //Check if class implements Validator
            if (Validator.class.isAssignableFrom(tmp)) {
                retTmp.add(tmp);
            } else {
                //log error and skip class-todo
                log().log(Level.WARNING,
                        "validatorservice.not_a_validator_class", classes[i]);

                continue;
            }
        }

        ret.addAll(retTmp);

        return ret;
    }

    /**
     * A convenient way to invoke a validator chain. For each invocation, the Validator classes
     * are gathered. Based on type of validated object the proper Validator is called.
     *
     * The validator will then chain the validation calls in order :
     * 1. validate the object using the validator for the object class
     * 2. validate the object using the validator for the object superclasses
     * 3. validate the object using each of the validator for object interfaces
     *
     * @param env ExecutionEnv that is used to create a proxy for ConfigurationService
     * @param obj - object that is going to be validated
     * @throws com.sun.grid.grm.GrmException everytime a problem during
     * Validator invocation occurs.
     * @throws com.sun.grid.grm.validate.GrmValidationException when validation of
     * object fails
     */
    @SuppressWarnings(value = "unchecked")
    public static void validateChain(ExecutionEnv env, Object obj) throws GrmValidationException, GrmException {
        validateChain(env, obj, null);
    }
    /**
     * A convenient way to invoke a validator chain. For each invocation, the Validator classes
     * are gathered. Based on type of validated object the proper Validator is called.
     * 
     * The validator will then chain the validation calls in order :
     * 1. validate the object using the validator for the object class
     * 2. validate the object using the validator for the object superclasses
     * 3. validate the object using each of the validator for object interfaces
     *
     * @param env ExecutionEnv that is used to create a proxy for ConfigurationService
     * @param obj - object that is going to be validated
     * @throws com.sun.grid.grm.GrmException everytime a problem during
     * Validator invocation occurs.
     * @throws com.sun.grid.grm.validate.GrmValidationException when validation of
     * object fails
     */
    @SuppressWarnings(value = "unchecked")
    public static void validateChain(ExecutionEnv env, Object obj, Object old) throws GrmValidationException, GrmException {
        validate(env, obj, old);
        validateInterfaces(env, obj, obj.getClass(), old);
        validateSuperClass(env, obj, obj.getClass(), old);
    }

    @SuppressWarnings(value = "unchecked")
    private static void validateInterfaces(ExecutionEnv env, Object obj, Class classToValidate, Object old)
            throws GrmValidationException, GrmException {
        if (validators == null) {
            validators = getValidators();
        }

        //null cannot be validated
        if (obj == null) {
            throw new GrmException("validatorservice.no_object_to_validate");
        }

        for (Class intf : classToValidate.getInterfaces()) {
            Class validator = null;
            //Get the name of the proper Validator Class
            validator = validators.get(intf);

            if (validator == null) {
                //log the message validator is not available for specified Object
                log().log(Level.FINE,
                        "validatorservice.validator_not_available", intf);
            } else {
                Constructor con;
                Validator f = null;

                try {
                    //Get constractor of Validator class and create instance
                    con = validator.getConstructor();
                    f = (Validator) con.newInstance();
                } catch (SecurityException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (IllegalArgumentException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (InvocationTargetException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (IllegalAccessException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (InstantiationException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (NoSuchMethodException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                }

                //Call validate method on Validator
                f.validate(env, obj, old);
                // interface can extend another interface, so call validateSuperClass
                validateSuperClass(env, obj, intf, old);
            }
        }
    }

    @SuppressWarnings(value = "unchecked")
    private static void validateSuperClass(ExecutionEnv env, Object obj, Class classToValidate, Object old) throws GrmValidationException, GrmException {
        if (validators == null) {
            validators = getValidators();
        }

        //null cannot be validated
        if (obj == null) {
            throw new GrmException("validatorservice.no_object_to_validate");
        }

        Class validator = null;
        Class superClass = classToValidate.getSuperclass();

        while (superClass != null) {
            //Get the name of the proper Validator Class
            validator = validators.get(superClass);

            if (validator == null) {
                //log the message validator is not available for specified Object
                log().log(Level.FINE, "validatorservice.validator_not_available", superClass);
            } else {
                Constructor con;
                Validator f = null;

                try {
                    //Get constructor of Validator class and create instance
                    con = validator.getConstructor();
                    f = (Validator) con.newInstance();
                } catch (SecurityException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (IllegalArgumentException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (InvocationTargetException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (IllegalAccessException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (InstantiationException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                } catch (NoSuchMethodException ex) {
                    throw new GrmException("validatorservice.can_create_validator_instance",
                            BUNDLE_NAME, validator, ex.getLocalizedMessage());
                }

                //Call validate method on Validator
                f.validate(env, obj, old);
            }
            // validate all interfaces implemented by the superclass
            validateInterfaces(env, obj, superClass, old);
            // move one level deeper in class hierarchy
            superClass = superClass.getSuperclass();
        }
    }
}
