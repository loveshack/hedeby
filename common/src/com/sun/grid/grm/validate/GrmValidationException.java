/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.validate;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.Printer;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

/** 
 * Exception that occured during validation process in Hedeby,
 * Contains list that contains String error messages that occured
 */

public class GrmValidationException extends GrmException{
    
    private final static long serialVersionUID = 2007112901L;
    private final static String BUNDLE_NAME="com.sun.grid.grm.validate.messages";
    private final List<String> errorMessages;
    
    /** Creates a new instance of GrmValidationException */
    public GrmValidationException() {
        super("GrmValidationException.msg", BUNDLE_NAME);
        errorMessages = new LinkedList<String>();
    }
    
    /** 
     * Creates a new instance of GrmValidationException
     *
     * @param errorMessages - list of Strings containing error messages
     */
    public GrmValidationException(List<String> errorMessages) {
        super("GrmValidationException.msg", BUNDLE_NAME);
        this.errorMessages = errorMessages;
    }
    
    /** 
     * Add error string to list of errors contained by this exception
     *
     * @param error - error message
     */
    public void addError(String error) {
        errorMessages.add(error);
    }
    /**
     * Returns list which contains string errors generated during validation
     * @return errorMessages <code>List&lt;String&gt;</code>
     */
    public List<String> getErrorMessages() {
        return errorMessages;
    }

    private transient String msgStr;
    
    @Override
    public String getLocalizedMessage() {
        if(msgStr == null) {
            if(errorMessages.isEmpty()) {
                msgStr = super.getLocalizedMessage();
            } else {
                
                StringWriter sw = new StringWriter();
                Printer p = new Printer(sw);
                try {
                    p.println(super.getLocalizedMessage());
                    String errMsg = ResourceBundle.getBundle(BUNDLE_NAME).getString("GrmValidationException.error");
                    p.setDepth(errMsg.length());
                    for(String error: errorMessages) {
                        StringTokenizer st = new StringTokenizer(error, "\n\r");
                        boolean first = true;
                        while(st.hasMoreTokens()) {
                            if(first) {
                                p.print(errMsg);
                                p.println(st.nextToken());
                                p.indent();
                                first = false;
                            } else {
                                p.println(st.nextToken());
                            }
                        }
                        if (!first) {
                            p.deindent();
                        }
                    }
                } finally {
                    p.close();
                }
                msgStr = sw.getBuffer().toString();
            }            
        }
        return msgStr;
    }
    
    
       
}
