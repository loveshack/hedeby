/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.GrmComponent;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.util.I18NManager;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcaster;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;

/**
 *  This implements an <code>DynamicMBean</code> wrapper for the hedeby
 *  components.
 * 
 *  The uses <code>ComponentDescriptor</code> to construct the <code>MBeanInfo</code>.
 * 
 * @param T the type of the wrapped component
 * @see ComponentDescriptor
 */
public class ComponentWrapper<T> implements DynamicMBean, NotificationBroadcaster {

    private static final String BUNDLE = "com.sun.grid.grm.management.management";
    private final static Logger log = Logger.getLogger(ComponentWrapper.class.getName(), BUNDLE);
    private final ComponentDescriptor componentInfo;
    private final Class intf;
    private final T impl;
    private final ObjectName objectName;
    private final String componentName;
    private final Map<NotificationListener,NotificationListenerElem> listeners = new HashMap<NotificationListener,NotificationListenerElem>();
    private Object eventProxy;
    private final AtomicLong sequenceCounter = new AtomicLong(1L);    
    private EventToNotificationBridge eventToNotificationBridge;

    
    /**
     * Creates a new instance of ComponentWrapper
     * @param impl   the instance of the Hedeby component
     * @param intf   interface of the component
     * @param objectName  object name of the hedeby component
     */
    public ComponentWrapper(T impl, Class<?> intf, ObjectName objectName) {
        if(!intf.isAssignableFrom(impl.getClass())) {
            throw new IllegalArgumentException("impl does not implement the interface");
        }
        this.intf = intf;
        this.impl = impl;
        this.objectName = objectName;
        this.componentInfo = ComponentDescriptor.getComponentInfo(intf);
        componentName = objectName.getKeyProperty("name");
    }
    
    /**
     * Create a ComponentWrapper for a <code>GrmComponent</code>.
     *
     * @param env    the execution environment
     * @param impl   the implementation of the <code>GrmComponent</code>
     * @param intf   the real interface of the <code>GrmComponent</code>
     *
     * @return the component wrapper
     */
    public static ComponentWrapper<GrmComponent> newInstance(ExecutionEnv env, GrmComponent impl, Class intf) {        
        String name;
        try {
            name = impl.getName();
        } catch(GrmRemoteException ex) {
            throw new IllegalStateException("Cannot read name from local component", ex);
        }
        return new ComponentWrapper<GrmComponent>(impl, intf, 
                     com.sun.grid.grm.bootstrap.ComponentInfo.createObjectName(env, name, intf));
    }
    
    

    /**
     * Get the implementation of the component
     * @return the implementation
     */
    public T getImpl() {
        return impl;
    }
    
    /**
     * Get the value of an attribute.
     *
     * @param attribute the attribute name
     * @throws javax.management.AttributeNotFoundException if the attribute is unknown
     * @throws javax.management.MBeanException   if the attribute is not accessable or if
     *                any error in the attribute getter occured
     * @return the value of the attribute
     */
    public Object getAttribute(String attribute) throws AttributeNotFoundException, MBeanException {
        
        ComponentAttributeDescriptor attr = componentInfo.getAttributes().get(attribute);
        if(attr == null) {
            throw new AttributeNotFoundException(attribute + " not found in " + intf.getName());
        }
        if(!attr.isReadable()) {
            throw new MBeanException(null, "attribute " + attribute + " is not readable");
        }
        try {
            return attr.getGetter().invoke(impl);
        } catch (IllegalArgumentException ex) {
            throw new MBeanException(ex, 
                I18NManager.formatMessage("ComponentWrapper.error.get", BUNDLE, attribute, ex.getLocalizedMessage()));
        } catch (IllegalAccessException ex) {
            throw new MBeanException(ex, 
                I18NManager.formatMessage("ComponentWrapper.error.get", BUNDLE, attribute, ex.getLocalizedMessage()));
        } catch (InvocationTargetException ex) {
            throw new MBeanException(ex, 
                I18NManager.formatMessage("ComponentWrapper.error.get", BUNDLE, attribute, ex.getLocalizedMessage()));
        }
    }

    /**
     * Set the value of an attribute
     * @param attribute the attribute
     * @throws javax.management.AttributeNotFoundException 
     * @throws javax.management.MBeanException 
     */
    public void setAttribute(Attribute attribute) throws AttributeNotFoundException, MBeanException {
        ComponentAttributeDescriptor attr = componentInfo.getAttributes().get(attribute.getName());
        if(attr == null) {
            throw new AttributeNotFoundException(attribute.getName() + " not found in " + intf.getName());
        }
        if(!attr.isWritable()) {
            throw new MBeanException(null,
                 I18NManager.formatMessage("ComponentWrapper.error.attrNotWriteable", BUNDLE, attribute.getName()));
        }
        try {
            attr.getSetter().invoke(impl, attribute.getValue());
        } catch (IllegalArgumentException ex) {
            throw new MBeanException(ex,
                 I18NManager.formatMessage("ComponentWrapper.error.set", BUNDLE, attribute.getName(), ex.getLocalizedMessage()));
        } catch (IllegalAccessException ex) {
            throw new MBeanException(ex,
                 I18NManager.formatMessage("ComponentWrapper.error.set", BUNDLE, attribute.getName(), ex.getLocalizedMessage()));
        } catch (InvocationTargetException ex) {
            throw new MBeanException(ex,
                 I18NManager.formatMessage("ComponentWrapper.error.set", BUNDLE, attribute.getName(), ex.getLocalizedMessage()));
        }
    }

    /**
     * Get a list of attributes.
     *
     * @param attributes array with the attribute names
     * @return the list of attributes
     */
    public AttributeList getAttributes(String[] attributes) {
        
        AttributeList ret = new AttributeList(attributes.length);
        
        for(String attribute: attributes) {
            ComponentAttributeDescriptor attrInfo = componentInfo.getAttributes().get(attribute);
            if(attrInfo != null && attrInfo.getGetter() != null) {
                try {
                    ret.add(new Attribute(attribute, attrInfo.getGetter().invoke(impl)));
                } catch (IllegalArgumentException ex) {
                    log.log(Level.WARNING, "ComponentWrapper.error.get", new Object [] { attribute, ex.getLocalizedMessage() });
                } catch (IllegalAccessException ex) {
                    log.log(Level.WARNING, "ComponentWrapper.error.get", new Object [] { attribute, ex.getLocalizedMessage() });
                } catch (InvocationTargetException ex) {
                    log.log(Level.WARNING, "ComponentWrapper.error.get", new Object [] { attribute, ex.getLocalizedMessage() });
                }
            }
        }
        return ret;
    }

    /**
     * Set a list of attributes.
     *
     * @param attributes the list of attributes
     * @return the list of attribute which has been changed
     */
    public AttributeList setAttributes(AttributeList attributes) {
        AttributeList ret = new AttributeList(attributes.size());
        for(Object elem: attributes) {
            Attribute attribute = (Attribute)elem;
            ComponentAttributeDescriptor attrInfo = componentInfo.getAttributes().get(attribute.getName());
            if(attrInfo != null && attrInfo.getSetter() != null) {
                try {
                    attrInfo.getSetter().invoke(impl, attribute.getValue());
                    ret.add(attribute);
                } catch (IllegalArgumentException ex) {
                    log.log(Level.WARNING, "ComponentWrapper.error.set", new Object [] { attribute.getName(), ex.getLocalizedMessage() });
                } catch (IllegalAccessException ex) {
                    log.log(Level.WARNING, "ComponentWrapper.error.set", new Object [] { attribute.getName(), ex.getLocalizedMessage() });
                } catch (InvocationTargetException ex) {
                    log.log(Level.WARNING, "ComponentWrapper.error.set", new Object [] { attribute.getName(), ex.getLocalizedMessage() });
                }
            }
        }
        
        return ret;
    }

    /**
     * Invoke an operation on the MBean
     * @param actionName  name of the operation
     * @param params      params of the operation
     * @param signature   signature of the operation
     * @throws javax.management.MBeanException 
     * @throws javax.management.ReflectionException 
     * @return the result of the operation
     */
    public Object invoke(String actionName, Object[] params, String[] signature) throws MBeanException, ReflectionException {
        
        ComponentOperationDescriptor opInfo = componentInfo.getOperation(actionName, signature);
        if(opInfo == null) {
            StringBuilder sb = new StringBuilder();
            sb.append(actionName);
            sb.append('(');
            boolean first = true;
            for(String sig: signature) {
                if(first) {
                    first = false;
                } else {
                    sb.append(',');
                }
                sb.append(sig);
            }
            sb.append(')');
            throw new ReflectionException(null,
                I18NManager.formatMessage("ComponentWrapper.error.opNotFound", BUNDLE, sb.toString()));
        }
        try {
            return opInfo.getMethod().invoke(impl, params);
        } catch (IllegalArgumentException ex) {
            throw new MBeanException(ex,
                I18NManager.formatMessage("ComponentWrapper.error.op", BUNDLE, actionName, ex.getLocalizedMessage()));
        } catch (IllegalAccessException ex) {
            throw new MBeanException(ex,
                I18NManager.formatMessage("ComponentWrapper.error.op", BUNDLE, actionName, ex.getLocalizedMessage()));
        } catch (InvocationTargetException ex) {
            throw new MBeanException(ex,
                I18NManager.formatMessage("ComponentWrapper.error.op", BUNDLE, actionName, ex.getLocalizedMessage()));
        }
    }

    /**
     * Get the <code>MBeanInfo</code> of this MBean
     * @return the <code>MBeanInfo</code>
     */
    public MBeanInfo getMBeanInfo() {
        return componentInfo.getMBeanInfo();
    }

    
    public void addNotificationListener(NotificationListener listener, NotificationFilter filter, Object handback) throws IllegalArgumentException {
        NotificationListenerElem elem = new NotificationListenerElem(listener, filter, handback);
        synchronized (listeners) {
            if (!listeners.containsKey(listener)) {
                listeners.put(listener, elem);
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "ComponentWrapper.anl", new Object[]{listener, componentName, handback, listeners.size()});
                }
                
                if (listeners.size() == 1) {
                    if (eventProxy == null) {
                        eventToNotificationBridge = new EventToNotificationBridge();
                        eventProxy = Proxy.newProxyInstance(getClass().getClassLoader(), componentInfo.getAllEventListenerInterfaces(), eventToNotificationBridge);
                    }
                    for (Method method : componentInfo.getAddListenerMethods()) {
                        try {
                            method.invoke(impl, eventProxy);
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "ComponentWrapper.enb", new Object[]{method.getName(), impl});
                            }
                        } catch (IllegalArgumentException ex) {
                            throw new IllegalArgumentException(
                                    I18NManager.formatMessage("ComponentWrapper.error.addEvtProxy", BUNDLE,
                                    method.getName(), ex.getLocalizedMessage()), ex);
                        } catch (IllegalAccessException ex) {
                            throw new IllegalArgumentException(
                                    I18NManager.formatMessage("ComponentWrapper.error.addEvtProxy", BUNDLE,
                                    method.getName(), ex.getLocalizedMessage()), ex);
                        } catch (InvocationTargetException ex) {
                            throw new IllegalArgumentException(
                                    I18NManager.formatMessage("ComponentWrapper.error.addEvtProxy", BUNDLE,
                                    method.getName(), ex.getTargetException().getLocalizedMessage()), ex.getTargetException());
                        }
                    }
                }
            } else {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "ComponentWrapper.lae", new Object[]{listener, componentName});
                }
            }
        }
    }
    
    public void removeNotificationListener(NotificationListener listener) throws ListenerNotFoundException {
        synchronized(listeners) {
            NotificationListenerElem elem = listeners.remove(listener);
            if (elem != null) {
                if(log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "ComponentWrapper.rnl", new Object [] { elem, componentName, listeners.size()});
                }
                if(listeners.isEmpty()) {
                    for(Method method: componentInfo.getRemoveListenerMethods()) {
                        try {
                            method.invoke(impl, eventProxy);
                            if(log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "ComponentWrapper.unb", new Object [] { method.getName(), impl});
                            }
                        } catch (IllegalArgumentException ex) {
                            log.log(Level.WARNING, "ComponentWrapper.error.removeEvtProxy", new Object [] { method.getName(), ex.getLocalizedMessage() });
                        } catch (IllegalAccessException ex) {
                            log.log(Level.WARNING, "ComponentWrapper.error.removeEvtProxy", new Object [] { method.getName(), ex.getLocalizedMessage() });
                        } catch (InvocationTargetException ex) {
                            log.log(Level.WARNING, "ComponentWrapper.error.removeEvtProxy", new Object [] { method.getName(), ex.getLocalizedMessage() });
                        }
                    }
                }
            } else {
                if(log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "ComponentWrapper.lnf", new Object [] { listener, componentName});
                }
            }
        }
    }
    
    /**
     * Remove all registered event listeners from the component
     */
    public void resetEventListeners() {
        synchronized(listeners) {
            if(eventProxy != null) {
                for (Method method : componentInfo.getRemoveListenerMethods()) {
                    try {
                        method.invoke(impl, eventProxy);
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "ComponentWrapper.unb", new Object[]{method.getName(), impl});
                        }
                    } catch (IllegalArgumentException ex) {
                        log.log(Level.WARNING, "ComponentWrapper.error.removeEvtProxy", new Object[]{method.getName(), ex.getLocalizedMessage()});
                    } catch (IllegalAccessException ex) {
                        log.log(Level.WARNING, "ComponentWrapper.error.removeEvtProxy", new Object[]{method.getName(), ex.getLocalizedMessage()});
                    } catch (InvocationTargetException ex) {
                        log.log(Level.WARNING, "ComponentWrapper.error.removeEvtProxy", new Object[]{method.getName(), ex.getLocalizedMessage()});
                    }
                }
            }
            listeners.clear();
        }        
    }
    
    class EventToNotificationBridge implements InvocationHandler {
        
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if(method.getDeclaringClass().equals(Object.class)) {
                // hashcode or equals method
                if(method.getName().equals("hashCode")) {
                    return ComponentWrapper.this.hashCode();
                } else if(method.getName().equals("equals")) {
                    return ComponentWrapper.this.equals(args[0]);
                } else if (method.getName().equals("toString")) {
                    return "[EventNotificationBridge for " + getObjectName() + "]";                
                } else {
                    throw new IllegalStateException("unknown method " + method);
                }
            } else {
                ComponentEventDescriptor eventInfo = null;
                for(Map.Entry<String, ComponentEventDescriptor> entry: componentInfo.getEvents().entrySet()) {
                    if(!method.equals( entry.getValue().getListenerMethod())) {
                        continue;
                    }
                    eventInfo = entry.getValue();
                    break;
                }

                if(eventInfo != null) {
                    Notification notification = new Notification(eventInfo.getEventName(), getObjectName(), sequenceCounter.incrementAndGet());
                    notification.setUserData(args);

                    List<NotificationListenerElem> tmpListeners = null;

                    synchronized(listeners) {
                        tmpListeners = new ArrayList<NotificationListenerElem>(listeners.values());
                    }

                    boolean logIt = log.isLoggable(Level.FINE);
                    for(NotificationListenerElem elem: tmpListeners) {
                        try {
                            if (logIt) {
                                log.log(Level.FINE, "ComponentWrapper.sn", 
                                        new Object [] { componentName, notification.getSequenceNumber(), elem.listener, notification.getType() } );
                            }
                            elem.handleNotification(notification);
                        } catch(Exception ex) {
                            LogRecord lr = new LogRecord(Level.WARNING, "ComponentWrapper.ex.hn");
                            lr.setThrown(ex);
                            lr.setParameters(new Object [] { ex.getLocalizedMessage() });
                            lr.setResourceBundle(ResourceBundle.getBundle(BUNDLE));
                            log.log(lr);
                        }
                    }
                } else {
                    log.log(Level.WARNING, "ComponentWrapper.error.unknownEvent", method.getName());
                }
                return null;
            }
        }
    }
    
    private static class NotificationListenerElem  {
        private final NotificationListener listener;
        private final NotificationFilter filter;
        private final Object handback;
        
        public NotificationListenerElem(NotificationListener listener, NotificationFilter filter, Object handback) {
            this.listener = listener;
            this.filter = filter;
            this.handback = handback;
        }

        public NotificationListener getListener() {
            return listener;
        }

        public NotificationFilter getFilter() {
            return filter;
        }

        public Object getHandback() {
            return handback;
        }
        
        public void handleNotification(Notification notification) {
            if(filter == null || filter.isNotificationEnabled(notification) ) {
                listener.handleNotification(notification, handback);
            }
        }
        @Override
        public String toString() {
            return String.format("%s(handback=%s)", listener, handback);
        }
    }
    

    /**
     * Get the MBean notifcation infos
     * @return the MBean notifcation infos
     */
    public MBeanNotificationInfo[] getNotificationInfo() {
        return getMBeanInfo().getNotifications();
    }

    /**
     * Get the object name of this MBean
     * @return the object name
     */
    public ObjectName getObjectName() {
        return objectName;
    }

    
}
