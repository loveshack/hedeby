/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanOperationInfo;

/**
 *  This class provides information about a Hedeby component.
 * 
 *  The <code>ComponentDescriptor</code> describes the
 *  attributes, operations and events of a Hedeby component.
 * 
 *  The method <code>getMBeanInfo</code> returns the JMX <code>MBeaninfo</code>
 *  if the hedeby component.
 *  
 * 
 * @see ComponentAttributeDescriptor
 * @see ComponentOperationDescriptor
 * @see ComponentEventDescriptor
 */
public class ComponentDescriptor {
    
    private final static Map<Class, ComponentDescriptor> cache = new HashMap<Class, ComponentDescriptor>();
    
    private final Map<String, ComponentAttributeDescriptor> attributes;
    private final Map<String, ComponentEventDescriptor> events;
    private final Set<ComponentOperationDescriptor> operations;
    private final MBeanInfo mBeanInfo;
    private final Set<Class> allEventListenerInterfaces;
    private final Set<Method> addListenerMethods;
    private final Set<Method> removeListenerMethods;
    /**
     * Get the component information about a hedeby component.
     *
     * @param clazz class of the Hedeby component
     * @return the component information
     */
    public static  ComponentDescriptor getComponentInfo(Class clazz) {
        ComponentDescriptor ret = null;
        synchronized(cache) {
            ret = cache.get(clazz);
            if(ret == null) {
                ret = new ComponentDescriptor(clazz);
                cache.put(clazz, ret);
            }
        }
        return ret;
    }
    
    private ComponentDescriptor(Class intf) {
        
        Map<String,ComponentAttributeDescriptor> tmpAttributes  = new HashMap<String, ComponentAttributeDescriptor>();
        Map<String,ComponentEventDescriptor> tmpEvents = new HashMap<String,ComponentEventDescriptor>();
        Set<ComponentOperationDescriptor> tmpOperations = new HashSet<ComponentOperationDescriptor>();
        Set<Method> tmpAddListenerMethods = new HashSet<Method>();
        Set<Method> tmpRemoveListenerMethods = new HashSet<Method>();
        
        for(Method method: intf.getMethods()) {
            
            if(!Modifier.isPublic(method.getModifiers())) {
                continue;
            }
            
            // is it a setter
            ComponentAttributeDescriptor tmpAttrInfo = ComponentAttributeDescriptor.newInstanceFromAttributeSetter(method);
            if(tmpAttrInfo != null) {
                ComponentAttributeDescriptor attrInfo = tmpAttributes.get(tmpAttrInfo.getName());
                if(attrInfo == null) {
                    attrInfo = tmpAttrInfo;
                    tmpAttributes.put(attrInfo.getName(), attrInfo);
                }
                attrInfo.setSetter(method);
                continue;
            }
            
            // is it a getter
            tmpAttrInfo = ComponentAttributeDescriptor.newInstanceFromAttributeGetter(method);
            if(tmpAttrInfo != null) {
                ComponentAttributeDescriptor attrInfo = tmpAttributes.get(tmpAttrInfo.getName());
                if(attrInfo == null) {
                    attrInfo = tmpAttrInfo;
                    tmpAttributes.put(attrInfo.getName(), attrInfo);
                }
                attrInfo.setGetter(method);
                continue;
            }
            
            // is it a event listener add method
            Class eventListenerType = ComponentEventDescriptor.getManagedEventListenerFromAdder(method);
            if(eventListenerType != null) {
                tmpAddListenerMethods.add(method);
                for(ComponentEventDescriptor event: ComponentEventDescriptor.getComponentEventInfosFromListener(eventListenerType)) {
                    tmpEvents.put(event.getEventName(), event);
                }
                continue;
            }
            
            // is it a event listener remove method
            eventListenerType = ComponentEventDescriptor.getManagedEventListenerFromRemover(method);
            if(eventListenerType != null) {
                tmpRemoveListenerMethods.add(method);
                for(ComponentEventDescriptor event: ComponentEventDescriptor.getComponentEventInfosFromListener(eventListenerType)) {
                    tmpEvents.put(event.getEventName(), event);
                }
                continue;
            }
            
            // We assume the method is a normal method
            tmpOperations.add(new ComponentOperationDescriptor(method));
        }
        
        // Make the maps unmodifiable. The getter can return references to the maps
        attributes = Collections.unmodifiableMap(tmpAttributes);
        operations = Collections.unmodifiableSet(tmpOperations);
        events = Collections.unmodifiableMap(tmpEvents);
        addListenerMethods = Collections.unmodifiableSet(tmpAddListenerMethods);
        removeListenerMethods = Collections.unmodifiableSet(tmpRemoveListenerMethods);
        
        MBeanAttributeInfo [] attrInfos = new MBeanAttributeInfo[attributes.size()];
        int i = 0;
        for(Map.Entry<String, ComponentAttributeDescriptor> entry: attributes.entrySet()) {
            attrInfos[i++] = entry.getValue().getAttrInfo();
        }
        
        MBeanOperationInfo [] opInfos = new MBeanOperationInfo[operations.size()];
        i = 0;
        for(ComponentOperationDescriptor op: operations) {
            opInfos[i++] = op.getMBeanOperationInfo();
        }
        
        MBeanNotificationInfo [] noInfos = new MBeanNotificationInfo[events.size()];
        i = 0;
        Set<Class> eventInterfaces = new HashSet<Class>();
        for(Map.Entry<String, ComponentEventDescriptor> entry: events.entrySet()) {
            noInfos[i++] = entry.getValue().getNotificationInfo();
            eventInterfaces.add(entry.getValue().getListenerClass());
        }
        
        mBeanInfo = new MBeanInfo(intf.getName(), "managed interface",
            attrInfos, null, opInfos,  noInfos);
        
        allEventListenerInterfaces = Collections.unmodifiableSet(eventInterfaces);
    }

    /**
     *  Get the <code>MBeanInfo</code> of the hedeby component
     *  @return the <code>MBeanInfo</code>
     */
    public MBeanInfo getMBeanInfo() {
        return mBeanInfo;
    }
    
    /**
     * Return the attribute map of this component.
     *
     * @return unmodifiable map (key is the name of the attribute)
     */
    public Map<String, ComponentAttributeDescriptor> getAttributes() {
        return attributes;
    }
    
    /**
     * Get the event map for this component.
     *
     * @return the unmodifiable event map (key is the event name)
     */
    public Map<String, ComponentEventDescriptor> getEvents() {
        return events;
    }
    
    /**
     * Get the operations for this component.
     *
     * @return the unmodifiable operation set
     */
    public Set<ComponentOperationDescriptor> getOperations() {
        return operations;
    }
    
    /**
     * Get an operation.
     *
     * @param name   name of the operation
     * @param signature signature of the operation
     * @return the operation or <code>null</code> if the operation has not been found
     */
    public ComponentOperationDescriptor getOperation(String name, String [] signature) {
        for(ComponentOperationDescriptor opInfo: operations) {
            if(opInfo.matches(name, signature) ) {
                return opInfo;
            }
        }
        return null;
    }

    
    /**
     * Get an array which contains all event listeners class for the events of the componet.
     * @return array with all event listener classes
     */
    public Class [] getAllEventListenerInterfaces() {
        return allEventListenerInterfaces.toArray(new Class[allEventListenerInterfaces.size()]);
    }

    /**
     * Get all add event listener methods of the component.
     *
     * @return unmodifiable set with the add event listener methods
     */
    public Set<Method> getAddListenerMethods() {
        return addListenerMethods;
    }

    /**
     * Get all remove event listener methods.
     *
     * @return unmodifiable set with the add event listener methods
     */
    public Set<Method> getRemoveListenerMethods() {
        return removeListenerMethods;
    }
    
}
