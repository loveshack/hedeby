/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.BootstrapConstants;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JvmInfo;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.ui.component.service.GetActiveJVMListCommand;
import com.sun.grid.grm.util.Hostname;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  This class caches the connections to other jvms.
 */
public class ConnectionPool {

    private final static String BUNDLE = "com.sun.grid.grm.management.management";
    private final static Logger log = Logger.getLogger(ConnectionPool.class.getName(), BUNDLE);
    
    private final static Lock poolLock = new ReentrantLock();
    private final static Map<ExecutionEnv,ConnectionPool> poolMap = new HashMap<ExecutionEnv,ConnectionPool>(3);
    
    private final ExecutionEnv env;
    private final Lock lock = new ReentrantLock();
    private final Map<JvmInfo,ConnectionProxy> openConnections = new HashMap<JvmInfo,ConnectionProxy>();
    private final ActiveJvm csJvmInfo;
    private final Map<JvmInfo,ActiveJvm> jvmInfoMap = new HashMap<JvmInfo,ActiveJvm>();
    
    private boolean closed;
    
    /**
     *  Get the connection pool for a execution env.
     *
     *  @param env the execution env
     *  @return the connection pool
     */
    public static ConnectionPool getInstance(ExecutionEnv env) {
        poolLock.lock();
        try {
            ConnectionPool ret = poolMap.get(env);
            if(ret == null) {
                ret = new ConnectionPool(env);
                poolMap.put(env, ret);
            }
            return ret;
        } finally {
            poolLock.unlock();
        }
    }
    
    private ConnectionPool(ExecutionEnv aEnv) {
        env = aEnv;
        csJvmInfo = new ActiveJvm();
        csJvmInfo.setName(BootstrapConstants.CS_JVM);
        csJvmInfo.setHost(env.getCSHost().getHostname());
        csJvmInfo.setPort(env.getCSPort());
    }
    
    /**
     * Get the connection proxy to a jvm.
     *
     * @param jvm  the jvm info
     * @return  the connection proxy
     */
    public ConnectionProxy getConnection(JvmInfo jvm) {
        log.entering(ConnectionPool.class.getName(), "getConnection", jvm);
        ConnectionProxy ret;
        lock.lock();
        try {
          ret = openConnections.get(jvm);
          if(ret == null) {
              ret = new ConnectionProxy(env, jvm);
              openConnections.put(jvm, ret);
              if(closed) {
                  ret.close();
              }
          }
        } finally {
            lock.unlock();
        }
        log.exiting(ConnectionPool.class.getName(), "getConnection", ret);
        return ret;
    }
    
    /**
     * Close all connections of the connection pool
     */
    public void closeAllConnections() {
        log.entering(ConnectionPool.class.getName(), "closeAllConnections");
        // Close the connector => no new connection can be made
        Connector.getInstance().close();
        List<ConnectionProxy> connections;
        lock.lock();
        try {
            closed = true;
            connections = new ArrayList<ConnectionProxy>(openConnections.values());
            openConnections.clear();
        } finally {
            lock.unlock();
        }
        for(ConnectionProxy p: connections) {
            p.close();
        }
        log.exiting(ConnectionPool.class.getName(), "closeAllConnections");
    }
    
    /**
     *   Clear the active jvm info cache
     * 
     *   <p>package private, used only for testing</p>
     */
    void clearActiveJVMInfo() {
        log.entering(ConnectionPool.class.getName(), "clearActiveJVMInfo");
        lock.lock();
        try {
            jvmInfoMap.clear();
        } finally {
            lock.unlock();
        }
        log.exiting(ConnectionPool.class.getName(), "clearActiveJVMInfo");
    }
    
    /**
     * Get the active jvm info for a jvm.
     *
     * @param jvm  the jvm
     * @throws com.sun.grid.grm.GrmException if the active jvm info can not be retrieved from CS
     * @return the active jvm info
     */
    public ActiveJvm getJvmInfo(JvmInfo jvm) throws GrmException {
        log.entering(ConnectionPool.class.getName(), "getJvmInfo", jvm);
        ActiveJvm ret = null;
        if(jvm.getName().equals(csJvmInfo.getName()) && (jvm.getHostname().equals(Hostname.getInstance(csJvmInfo.getHost())))) {
            ret = csJvmInfo;
        } else {
            lock.lock();
            try {
                // Look into the cache
                ret = jvmInfoMap.get(jvm);
            } finally {
                lock.unlock();
            }
            if(ret == null) {
                ret = lookupJvm(jvm);
            }
        }
        log.exiting(ConnectionPool.class.getName(), "getJvmInfo", ret);
        return ret;
    }
    
    /**
     *  Reset the active jvm info.
     *
     *  @param jvm  the jvm info
     */
    public void resetJvmInfo(JvmInfo jvm) {
        log.entering(ConnectionPool.class.getName(), "resetJvmInfo", jvm);
        lock.lock();
        try {
            jvmInfoMap.remove(jvm);
        } finally {
            lock.unlock();
        }
        log.exiting(ConnectionPool.class.getName(), "resetJvmInfo");
    }
    
    private ActiveJvm lookupJvm(JvmInfo jvm) throws GrmException {
        log.entering(ConnectionPool.class.getName(), "lookupJvm", jvm);
        ActiveJvm ret  = null;
        GetActiveJVMListCommand cmd = new GetActiveJVMListCommand();
        // Calling the potential remote method of CS must not be executed within 
        // the lock otherwise a deadlock is possible (issue 643).
        List<ActiveJvm> jvms = env.getCommandService().execute(cmd).getReturnValue();
        Map<JvmInfo,ActiveJvm> oldJvmInfoMap;
        lock.lock();
        try {
            oldJvmInfoMap = new HashMap<JvmInfo,ActiveJvm>(jvmInfoMap);
            jvmInfoMap.clear();

            for(ActiveJvm newJvm: jvms) {
                JvmInfo key = new JvmInfo(newJvm.getName(), Hostname.getInstance(newJvm.getHost()));
                ActiveJvm oldJvm = oldJvmInfoMap.remove(key);
                if(oldJvm == null) {
                    if(log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "ConnectionPool.newJvm", new Object [] { key, newJvm.getPort() });
                    }

                } else if (oldJvm.getPort() != newJvm.getPort()) {
                    if(log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "ConnectionPool.portChanged", new Object [] { key, newJvm.getPort() });
                    }
                }
                jvmInfoMap.put(key, newJvm);
                if(key.equals(jvm)) {
                    ret = newJvm;
                }
            }
        } finally {
            lock.unlock();
        }        
        if(log.isLoggable(Level.FINE)) {
            for(JvmInfo key: oldJvmInfoMap.keySet()) {
                log.log(Level.FINE, "ConnectionPool.jvmGone", key);
            }
        }
        log.exiting(ConnectionPool.class.getName(), "lookupJvm", ret);
        return ret;
    }
}
