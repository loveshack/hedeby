/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.management;

import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.security.ServerSecurityContext;
import com.sun.grid.grm.util.Hostname;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.ServerSocket;
import java.rmi.NoSuchObjectException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.RMISocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.remote.JMXConnectionNotification;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

/**
 *  The SecureMBeanServer creates the RMIRegistry, the MBeanServer and setups a
 *  secure JMXConnectorServer.
 *
 *  For tests the secure JMXConnectorServer can be turned of by setting the
 *  system property <code>com.sun.grid.grm.management.SecureMBeanServer.nosecurity</code>
 *
 *  <b>Attention:</b> If secure JMXConnectorServer is turned off no login is executed
 *
 * @todo    <p>secure the RMI Registry</p>
 * @todo    <p>localization</p>
 */
public class SecureMBeanServer {
    
    private final static String BUNDLE = "com.sun.grid.grm.management.management";
    private static final Logger log = Logger.getLogger(SecureMBeanServer.class.getName(), BUNDLE);
    
    private final ServerSecurityContext securityContext;
    private final int port;
    private final MBeanServer mbs;
    private final String system;
    private final String jvm;
    
    private JMXServiceURL url;
    private int registryPort;
    private JMXConnectorServer cs;
    private Registry registry;
    
    /**
     *  Creates a new instance of SecureMBeanServer.
     * 
     * @param system name of the system
     * @param jvm name of the jvm
     * @param port port for the rmi registry
     * @param securityContext the security context
     * @throws com.sun.grid.grm.security.GrmSecurityException if the security context is invalid
     * @throws java.io.IOException if the MBean server connection could not be
     *                              established
     */
    public SecureMBeanServer(String system, String jvm,
            final int port, ServerSecurityContext securityContext)
            throws IOException, GrmSecurityException {
        this(ManagementFactory.getPlatformMBeanServer(), system, jvm, port, securityContext);
    }
    
    /**
     * Creates a new instance of SecureMBeanServer.
     * 
     * @param mbs the mbean server
     * @param system the system name
     * @param jvm name of the jvm
     * @param port port of the rmi registry
     * @param securityContext the security context
     * @throws com.sun.grid.grm.security.GrmSecurityException if the security context is invalid
     * @throws java.io.IOException on any IO error
     */
    public SecureMBeanServer(MBeanServer mbs, String system, String jvm, int port, 
                             ServerSecurityContext securityContext) throws IOException, GrmSecurityException {
        
        
        this.securityContext = securityContext;
        this.mbs = mbs;
        this.port = port;
        this.system = system;
        this.jvm = jvm;
        start();
    }
    
    
    /**
     * Get the rmi registry of the secure mbean server.
     * @return the rmi registry
     */
    public final Registry getRegistry() {
        return registry;
    }
    
    
    /**
     * Get the port of the rmi registry.
     * @return the port of the rmi registry
     */
    public final int getRegistryPort() {
        return registryPort;
    }
    
    /**
     * Get the JMXServiceURL.
     *
     * @return the JMXServiceURL
     */
    public final JMXServiceURL getJMXServiceUrl() {
        return url;
    }
    
    /**
     * Get the MBeanServer.
     *
     * @return the MBeanServer
     */
    public synchronized final MBeanServer getMBeanServer() {
        if(cs != null) {
            return cs.getMBeanServer();
        } return null;
    }
    
    /**
     *  This socket factory is used for finding the port of the rmi registry.
     */
    private class RegistryServerSocketFactory implements RMIServerSocketFactory {
        private int lastPort;
        
        public ServerSocket createServerSocket(final int port) throws IOException {
            
            ServerSocket ret = RMISocketFactory.getDefaultSocketFactory().createServerSocket(port);
            lastPort = ret.getLocalPort();
            return ret;
        }
        
        public int getLastPort() {
            return lastPort;
        }
        
    }
    
    
    /**
     * Start the ConnectorServer.
     *
     * @throws java.io.IOException 
     * @throws com.sun.grid.grm.security.GrmSecurityException 
     */
    public synchronized void start() throws IOException, GrmSecurityException {
        
        RegistryServerSocketFactory regServerSocketFactory = new RegistryServerSocketFactory();
        RMIClientSocketFactory csf = RMISocketFactory.getDefaultSocketFactory();
        registry = LocateRegistry.createRegistry(port, csf, regServerSocketFactory);
        
        this.registryPort = regServerSocketFactory.getLastPort();
        
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "SecureMBeanServer.rmiRegStarted", registryPort);
        }
        String hostname = Hostname.getLocalHost().getHostname();
        this.url = new JMXServiceURL("service:jmx:rmi://"
                + hostname
                + "/jndi/rmi://"
                + hostname
                + ":"
                + registryPort
                + "/"
                + system
                );

        if(securityContext != null) {
            cs = securityContext.createConnector(mbs, url);
        } else {           
            log.log(Level.WARNING,"SecureMBeanServer.newSecurityContext");
            cs = JMXConnectorServerFactory.newJMXConnectorServer(url, null, mbs);
        }
        
        cs.addNotificationListener(new MyNotificationListener(), null, null);
       
        cs.start();
        
        if (log.isLoggable(Level.INFO)) {
            log.log(Level.INFO, "SecureMBeanServer.started", url);
        }
    }
    
    class MyNotificationListener implements NotificationListener {

        public void handleNotification(Notification notification, Object handback) {
            if(log.isLoggable(Level.FINE) && notification instanceof JMXConnectionNotification) {
                JMXConnectionNotification jn = (JMXConnectionNotification)notification;
                if (JMXConnectionNotification.CLOSED.equals(jn.getType())) {
                    log.log(Level.FINE,"SecureMBeanServer.cc.closed", jn.getConnectionId());
                } else if (JMXConnectionNotification.FAILED.equals(jn.getType())) {
                    log.log(Level.FINE,"SecureMBeanServer.cc.failed", jn.getConnectionId());
                } else if (JMXConnectionNotification.NOTIFS_LOST.equals(jn.getType())) {
                    log.log(Level.FINE,"SecureMBeanServer.cc.nlost", jn.getConnectionId());
                } else if (JMXConnectionNotification.OPENED.equals(jn.getType())) {
                    log.log(Level.FINE,"SecureMBeanServer.cc.opened", jn.getConnectionId());
                }
            }
        }
        
    }
    
    /**
     * Stop the ConnectorServer.
     */
    public synchronized void stopConnectorServer() {
        if (cs != null) {
            try {
                cs.stop();
            } catch (Exception e) {
                log.log(Level.WARNING, "SecureMBeanServer.ex.stop", e);
            } finally {
                cs = null;
            }
        }
    }
    
    /**
     *  Stop the ConnectorServer and the RMIRegistry
     */
    public synchronized void close() {
        stopConnectorServer();
        try {
            UnicastRemoteObject.unexportObject(registry, true);
        } catch (NoSuchObjectException ex) {
            log.log(Level.WARNING, "SecureMBeanServer.ex.close", ex);
        } finally {
            registry = null;
        }
    }
}
