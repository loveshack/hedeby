/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import java.lang.reflect.Method;
import javax.management.IntrospectionException;
import javax.management.MBeanAttributeInfo;

/**
 * Instances of this class describes an attributes of a Hedeby component
 */
public class ComponentAttributeDescriptor  {

    private final String name;
    private final String description;
    private MBeanAttributeInfo attrInfo;
    private Method getter;
    private Method setter;
    private final Class type;
    private boolean isIs;
    private boolean readable;
    private boolean writable;
    
    private ComponentAttributeDescriptor(String name, String description, Class type) {
        this.name = name;
        this.description = description;
        this.type = type;
    }
    
    /**
     * Get the <code>MBeanAttributeInfo</code> of the attribute
     * @return the <code>MBeanAttributeInfo</code>
     */
    public MBeanAttributeInfo getAttrInfo() {
        if(attrInfo == null) {
            attrInfo = new MBeanAttributeInfo(name, type.getName(), description,
                readable, writable, isIs);
        }
        return attrInfo;
    }

    /**
     * Does the getter of this attribute starts with is.
     *
     * @return <code>true</code> if the getter of the attribute starts with is
     */
    public boolean isIs() {
        return isIs;
    }
    
    /**
     * Is the attribute readable.
     *
     * @return <code>true</code> if the attribute is readable
     */
    public boolean isReadable() {
        return readable;
    }

    /**
     * Is the attribute writeable.
     *
     * @return <code>true</code> if the attribute is writeable
     */
    public boolean isWritable() {
        return writable;
    }
    
    
    /**
     * get the getter of this attribute.
     *
     * @return the getter of this attribute or <code>null</code>
     *         if the attribute is not readable
     */
    public Method getGetter() {
        return getter;
    }

    /**
     * Get the getter of this attribute.
     *
     * @return the getter of this attribute or <code>null</code>
     *         if the attribute is not writable
     */
    public Method getSetter() {
        return setter;
    }

    /**
     * Get the name of the attribute
     * @return the name of the attribute
     */
    public String getName() {
        return name;
    }

    /**
     * Get the description of the attribute.
     *
     * @return the description of the attribute
     */
    public String getDescription() {
        return description;
    }

    /**
     * set the getter of the attribute.
     * @param getter  the getter of the attribute
     */
    public void setGetter(Method getter) {
        this.getter = getter;
        this.readable = getter != null;
        this.isIs = readable && getter.getName().startsWith("is");
    }

    /**
     * Set the setter of the attribute.
     * @param setter the setter of the attribute
     */
    public void setSetter(Method setter) {
        this.setter = setter;
        this.writable = setter != null;
    }

    /**
     * Get the type of the attribute
     * @return the type of the attribute
     */
    public Class getType() {
        return type;
    }
    
    /**
     * Get an instance of <code>ComponentAttributeDescriptor</code> from an attribute getter
     * 
     * The signatures of an attribute getters are
     * <pre>
     *      public &lt;attribute type&gt; get&lt;attribute name&gt;()
     *      public boolean is&lt;attribute name&gt;()
     *      public Boolean is&lt;attribute name&gt;()
     * </pre>
     * 
     * @param method the attribute getter
     * @return the <code>CComponentAttributeDescriptor/code> or null if <code>method</code>
     *         is not an attribute getter
     */
    public static ComponentAttributeDescriptor newInstanceFromAttributeGetter(Method method) {
        
        Class returnType = method.getReturnType();

        String attrName = null;
        
        if(!returnType.equals(Void.TYPE)) {
            Class [] paramTypes = method.getParameterTypes();
            if(paramTypes.length == 0) {
                String methodName = method.getName();
                if( (returnType.equals(Boolean.TYPE) ||
                     returnType.equals(Boolean.class))
                    && methodName.length() > 2
                    && methodName.startsWith("is") ) {
                    StringBuilder sb = new StringBuilder(methodName.length()-2);
                    sb.append(Character.toLowerCase(methodName.charAt(2)));
                    sb.append(methodName.substring(3));
                    attrName= sb.toString();
                } else if(method.getName().length() > 3
                       && method.getName().startsWith("get")) {
                    StringBuilder sb = new StringBuilder(methodName.length()-3);
                    sb.append(Character.toLowerCase(methodName.charAt(3)));
                    sb.append(methodName.substring(4));
                    attrName= sb.toString();
                }
            }
        }

        if(attrName != null) {
            ComponentAttributeDescriptor ret = new ComponentAttributeDescriptor(attrName, "", returnType);
            ret.setGetter(method);
            return ret;
        } else {
            return null;
        }
    }
    
    /**
     * Get a new instanceof of <code>ComponentAttributeDescriptor</code> from an
     * attribute getter.
     * 
     * @param method the attribute getter
     * @return the <code>CoComponentAttributeDescriptorcode> or <code>null</code> if <code>method</code>
     *         is not an attribute getter
     */
    public static ComponentAttributeDescriptor newInstanceFromAttributeSetter(Method method) {
        if(isAttributeSetter(method)) {
            String attrName = method.getName();
            attrName = Character.toLowerCase(attrName.charAt(3)) + attrName.substring(4);
            
            ComponentAttributeDescriptor ret = new ComponentAttributeDescriptor(attrName, "", method.getParameterTypes()[0]);
            ret.setSetter(method);
            return ret;
        } else {
            return null;
        }
    }
    
    
    /**
     * It a method an attribute setter
     *
     * @param method the method
     * @return <code>true</code> if the method is an attribute setter
     */
    public static boolean isAttributeSetter(Method method) {
        
        Class [] paramTypes = method.getParameterTypes();
        Class returnType = method.getReturnType();
        
        // is it a setter
        return returnType.equals(Void.TYPE) &&
               paramTypes.length == 1 &&
               method.getName().length() > 3 &&
               method.getName().startsWith("set");
    }

    
}
