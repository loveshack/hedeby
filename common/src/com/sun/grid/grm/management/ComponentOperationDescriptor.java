/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import java.lang.reflect.Method;
import javax.management.MBeanOperationInfo;

/**
 * Instances code <code>ComponentOperationDescriptor</code> describes
 * operations of Hedeby components.
 */
public class ComponentOperationDescriptor {
    
    private final Method method;
    private final MBeanOperationInfo opInfo;
    
    /**
     * Creates a new instance of ComponentOperationDescriptor
     * 
     * @param method the method
     */
    public ComponentOperationDescriptor(Method method) {
        this.method = method;
        this.opInfo = new MBeanOperationInfo(getName(), method);
    }

    /**
     * Get the name of the operation.
     *
     * @return the name of the operation
     */
    public String getName() {
        return method.getName();
    }
    
    /**
     * Get the method of the operation.
     *
     * @return the method of the operation
     */
    public Method getMethod() {
        return method;
    }
    
    /**
     * Get the <code>MBeanOperationInfo</code> if the operation
     * @return the <code>MBeanOperationInfo</code> 
     */
    public MBeanOperationInfo getMBeanOperationInfo() {
        return opInfo;
    }
    
    /**
     * Determine if a jmx action name an the signature matches
     * to the operation
     * @param actionName  name of the action
     * @param signature   signature of the action
     * @return <code>true</code> it matches
     */
    public boolean matches(String actionName, String [] signature) {
        if(getName().equals(actionName)) {
            
            Class [] paramTypes = method.getParameterTypes();
            if(paramTypes.length == signature.length) {
                int i = 0;
                for(Class paramType: paramTypes) {
                    if(!paramType.getName().equals(signature[i++])) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    /**
     * is this <code>ComponentOperationDescriptor</code> equal
     * to an object
     * 
     * 
     * @param obj  the object
     * @return <code>true</code> if <code>obj</code> is equals to this
     */
    public boolean equals(Object obj) {
        return obj instanceof ComponentOperationDescriptor 
            && opInfo.equals(((ComponentOperationDescriptor) obj).opInfo);
    }

    /**
     * Get the hash code of this object
     * @return the hash code
     */
    public int hashCode() {
        return opInfo.hashCode();
    }
    
}
