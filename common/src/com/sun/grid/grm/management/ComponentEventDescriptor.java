/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.management.MBeanNotificationInfo;

/**
 *  Instances of <code>ComponentEventDescriptor</code> describes events which are
 *  supported by Hedeby components.
 * 
 *  An <code>ComponentEventDescriptor</code> contains the following information
 * 
 *  <table border='1'>
 *    <tr><td valign='top'>eventName</td>
 *        <td>The name of the event has the following syntax:
 * <pre>
 *      &lt;event listener classname&gt;.&lt;listener method name&gt;
 * </pre>
 *        </td>
 *    </tr>
 *    <tr><td valign='top'>notificationInfo</td>
 *        <td>The JMX <code>MBeanNotificationInfo</code> of the event
 *        </td>
 *    </tr>
 *    <tr><td valign='top'>listenerMethod</td>
 *        <td>The listener method from the listener class which handles
 *            the event</td>
 *    </tr>
 *  </table>
 * 
 *  <p>This class caches all instances of <code>ComponentEventDescriptor</code> in a static map. 
 *     The key of this map is the listener class.</p>
 * 
 *  <p>A class is seen as event listener of it has the annotation <code>ManagedEventListener</code></p>
 * 
 *  <H2>Example</H2>
 * 
 *  <pre>
 *      package example;
 * 
 * @see javax.management.MBeanNotificationInfo
 * (at)ManagedEventListener public interface SampleEventListener {
 * 
 *           public void aOccured(String a, String b);
 *           public void bOccured();
 *      }
 * 
 *      public class Sample {
 *         public void addSampleEventListener(SampleEventListener lis) { ... }
 *         public void removeSampleEventListener(SampleEventListener lComponentEventDescriptor}
 * 
 *      LiComponentEventDescriptornfo> events = ComponentEventInfo.getComponentEventInfosFromListener(SampleEventListener.class);
 *  </pre>
 * 
 *  <p><code>events</code> will have two elements:</p>
 * 
 *  <ul>
 *      <li><code>example.SampleEventListener.aOccured</code></li>
 *      <li><code>example.SampleEventListener.bOccured</code></li>
 *  </ul>
 */
public class ComponentEventDescriptor {

    private final String eventName;
    private final MBeanNotificationInfo notificationInfo;
    private final Method listenerMethod;
    
    private final static Map<Class, List<ComponentEventDescriptor>> cache = new HashMap<Class, List<ComponentEventDescriptor>>();
    
    
    /**
     * Creates a new instance of ComponentEventDescriptor.
     * 
     * @param listenerMethod the listener method which handles the event
     */
    private ComponentEventDescriptor(Method listenerMethod) {
        this.listenerMethod = listenerMethod;
        this.eventName = getEventName(listenerMethod);
        this.notificationInfo = new MBeanNotificationInfo(new String [] { eventName }, Object[].class.getName(), null);
    }

    /**
     * Get the JMX event name for a listener method
     * @param listenerMethod the listener method
     * @return the event name
     */
    public static String getEventName(Method listenerMethod) {
        return String.format("%s.%s", listenerMethod.getDeclaringClass().getName(), listenerMethod.getName());
    }
    
    /**
     * Get all <code>ComponentEventDescriptor</code> event infos for a listener class.
     * 
     * <p>If the listener class is not a valid event listener this method returns an
     * empty map.</p>
     * 
     * @param type the listener class
     * @return the list of <code>ComponentEventDescriptor</code> of the events which are supported
     *         by the event lister
     */
    public static List<ComponentEventDescriptor> getComponentEventInfosFromListener(Class type) {
        
        List<ComponentEventDescriptor> ret = null;
        synchronized(cache) {
            ret = cache.get(type);
            if(ret == null) {
                ret = createComponentEventInfoFromListener(type);
                cache.put(type, ret);
            }
        }
        return ret;
    }
    
    private static List<ComponentEventDescriptor> createComponentEventInfoFromListener(Class<?> type) {
        if(type.getAnnotation(ManagedEventListener.class) == null) {
            return Collections.<ComponentEventDescriptor>emptyList();
        }
        List<ComponentEventDescriptor> ret = new LinkedList<ComponentEventDescriptor>();

        for(Method method: type.getMethods()) {
            
            if(!Modifier.isPublic(method.getModifiers())) {
                continue;
            }
            
            if(!method.getReturnType().equals(Void.TYPE)) {
                continue;
            }
            ret.add(new ComponentEventDescriptor(method));
        }
        return Collections.unmodifiableList(ret);
    }
    
    
    /**
     * Get a managed event lister from an event listener add method.
     *
     * @param method the event listener add method
     * @return the managed event listener or <code>null</code> if the method
     *         is not a event listener add method
     */
    public static Class getManagedEventListenerFromAdder(Method method) {
        Class ret = null;
        if(method.getReturnType().equals(Void.TYPE)
           && method.getName().startsWith("add")) {
            Class<?> [] paramTypes = method.getParameterTypes();
            if(paramTypes.length == 1 
               && paramTypes[0].getAnnotation(ManagedEventListener.class) != null) {
                ret = paramTypes[0];
            }
        }
        return ret;
    }

    /**
     * Get a managed event lister from an event listener remove method.
     *
     * @param method the event listener remove method
     * @return the managed event listener or <code>null</code> if the method
     *         is not a event listener add method
     */
    public static Class getManagedEventListenerFromRemover(Method method) {
        Class ret = null;
        if(method.getReturnType().equals(Void.TYPE)
           && method.getName().startsWith("remove")) {
            Class<?> [] paramTypes = method.getParameterTypes();
            if(paramTypes.length == 1 
               && paramTypes[0].getAnnotation(ManagedEventListener.class) != null) {
                ret = paramTypes[0];
            }
        }
        return ret;
    }
    
    /**
     * Get the event name
     * @return the event name
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Get the listener method of the listener class.
     * 
     * The listener method is the method which handle the
     * event notifications
     *
     * @return the listener method
     */
    public Method getListenerMethod() {
        return listenerMethod;
    }
    
    /**
     *  Get the listener class for this event
     *  @return the listener class
     */
    public Class getListenerClass() {
        return listenerMethod.getDeclaringClass();
    }
    
    

    /**
     * Get the MBean notification info for the event
     * @return the MBean notification info
     */
    public MBeanNotificationInfo getNotificationInfo() {
        return notificationInfo;
    }
}
