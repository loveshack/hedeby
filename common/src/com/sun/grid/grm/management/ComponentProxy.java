/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.util.I18NManager;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.Attribute;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;

/**
 *  Proxy class for a remote component.
 *
 *  <p>This proxy class creates a dynamic proxy which has the same interfaces as
 *  the remote component. All the JMX details are hidden.</p>
 *
 *  <p>The dynamic proxy can handle the following method types:</p>
 *
 *  <ul>
 *     <li><b>Attribute getters/setters:</b> The setAttribute/getAttribute method
 *         of the MBean is called</li>
 *     <li><b>Registration/Deregistration of event listeners:</v>
 *         The proxy registers a notification listener at the MBean. This listener
 *         forwards the local event over a JMX notification to the dynamic proxy.</li>
 *     <li><b>JMX action method:</b> Must have a corresponding <code>MBeanOperationInfo</code>
 *         in the <code>MBeanInfo</code> of the MBean.</li>
 *  </ul>
 *
 *  <p>The <code>Connection</code> tries a reconnect to the MBean if an I/O error
 *  occurs. This is usefull if the remote jvm uses a dynamic port.</p> 
 *
 *  <p><b>Warning:</p> The <code>ComponentProxy</code> class should not be used directly. A instance
 *  of the proxy of a <code>ComponentProxy</code> can be obtained by 
 *  <code>ComponentService.getComponent(...)</code>.</p>
 *
 *  <H2>Example:</H2>
 *
 *  <pre>
 *      Execution env = ...
 *      ActiveComponent ac = ...
 *
 *      Executor executor = ComponentProxy.<Executor>newProxy(env, ac);
 *
 *      executor.execute(...);
 *  </pre>
 *
 *  <p> or with the component service <p/>
 *
 *  <pre>
 *      ExecutionEnv env = ...
 *      Executor executor = ComponentService.<Executor>getComponent(env, Executor.class);
 *      executor.execute(...);
 *  </pre>
 * 
 * <h2>Thread safeness</h2>
 * 
 * <p>The class itself can be considered as threadsafe. All member variables are final
 *    The <code>mbeanInfo</code> is encapsulated by an AtomicReference and must hence
 *    not be protected by an lock. It can happen that the lookup of the mbeaninfo is
 *    executed twice, but this is not a problem.</p>
 * 
 * <p>The <code>methodInvocationCache</code> is implemented by an <code>ConcurrentHashMap</code>. It
 *    can happen that <code>MethodInvocationHandler</code> for one method is created more than once,
 *    however this is not a problem.</p>
 *
 * @param <T> The interface of the component
 */
public class ComponentProxy<T> implements InvocationHandler {
    
    private final static String BUNDLE = "com.sun.grid.grm.management.management";
    
    private final static Logger log = Logger.getLogger(ComponentProxy.class.getName(), BUNDLE);
    
    /** the proxy object (has same interface as the remote component */
    private final T proxy;
    
    /** the invocation handler for the MBeanServer */
    private final ConnectionProxy connectionProxy;

    /** environment for the component */
    private final ExecutionEnv env;
        
    /** connect information for the remote component */
    private final com.sun.grid.grm.bootstrap.ComponentInfo compInfo;

    /** MBeanInfo of the remote component */
    private final AtomicReference<MBeanInfo> mbeanInfo = new AtomicReference<MBeanInfo>();
    
    /**
     *  This map contains the cached <code>MethodInvocationHandler</code> objects.
     *  
     * <p>This map must be thread safe, because the <code>getMethodInvocationHandler</code>
     *    is not protected by an lock.</p>
     */
    private final static Map<Method, MethodInvocationHandler> methodInvocationCache = new ConcurrentHashMap<Method, MethodInvocationHandler>();
    
    /**
     * Package private constructor. Should only be used directly for testing purpose.
     * In productive code the static <code>newProxy</code> methods should be used.
     * 
     * @param aEnv        the execution env
     * @param aCompInfo   the component info
     * @param classLoader the class loader. If <tt>null</code> the class loader of the interface from aCompInfo is used
     * @see ComponentProxy#newProxy(com.sun.grid.grm.bootstrap.ExecutionEnv, com.sun.grid.grm.bootstrap.ComponentInfo) 
     * @see ComponentProxy#newProxy(com.sun.grid.grm.bootstrap.ExecutionEnv, com.sun.grid.grm.bootstrap.ComponentInfo, java.lang.ClassLoader) 
     */
    @SuppressWarnings("unchecked")
    ComponentProxy(ExecutionEnv aEnv, com.sun.grid.grm.bootstrap.ComponentInfo aCompInfo, ClassLoader classLoader) {
        env = aEnv;
        compInfo = aCompInfo;
        connectionProxy = ConnectionPool.getInstance(env).getConnection(aCompInfo.getJvm());
        
        try {
            Class type = null;
            if(classLoader == null) {
                type = Class.forName(compInfo.getInterface());
            } else {
                type = Class.forName(compInfo.getInterface(), true, classLoader);
            }
            Class<?> [] types = new Class<?> [] {
                type
            };
            proxy = (T)Proxy.newProxyInstance(getClass().getClassLoader(), types , this);
        } catch(ClassNotFoundException ex) {
            throw new IllegalArgumentException(
                        I18NManager.formatMessage("ComponentProxy.error.unknownInterface", 
                                                   BUNDLE, 
                                                   compInfo.getName(), compInfo.getInterface()),
                        ex);
        }
    }
    
    /**
     * Get a proxy object to a remote component.
     * @param <T> the type of the component
     * @param env       the execution environment
     * @param compInfo  the component Info
     * @return the proxy object
     */
    public static <T> T newProxy(ExecutionEnv env, com.sun.grid.grm.bootstrap.ComponentInfo compInfo) {
        return new ComponentProxy<T>(env, compInfo, null).getProxy();
    }
    
    /**
     * Get a proxy object to a remote component.
     * 
     * @param <T> the type of the component
     * @param env       the execution environment
     * @param compInfo  the component Info
     * @param classLoader the classLoader which has access to the interface class of the component
     * @return the proxy object
     */
    public static <T> T newProxy(ExecutionEnv env, com.sun.grid.grm.bootstrap.ComponentInfo compInfo, ClassLoader classLoader) {
        return new ComponentProxy<T>(env, compInfo, classLoader).getProxy();
    }
    
    /**
     * Get the proxy object
     * 
     * Package private for testing purpose
     * 
     * @return the proxy object
     */
    T getProxy() {
        return proxy;
    }
    
    /**
     * Invokes a method in the MBean.
     *
     * @param proxy    the proxy object
     * @param method   the method which has been called in the dynamic proxy
     * @param args     arguments for the method call
     * @throws java.lang.Throwable
     * @return the result of the method
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ComponentProxy.class.getName(), "invoke", new Object [] { method, Arrays.toString(args) });
        }
        try {
            MethodInvocationHandler handler = getMethodInvocationHandler(method, this);
            Object ret = handler.invoke(this, args);
            log.exiting(ComponentProxy.class.getName(), "invoke", ret);
            return ret;
        } catch (Exception ex) {
            if (ex instanceof InstanceNotFoundException) {
                throw new GrmRemoteException("ComponentProxy.ex.rcnf", ex, BUNDLE, compInfo,  GrmRemoteException.findRealMessage(ex));
            } else if (ex instanceof IOException) {
                throw new GrmRemoteException("ComponentProxy.ex.io", ex, BUNDLE, compInfo, GrmRemoteException.findRealMessage(ex));
            } else {
                Throwable t = ex;
                if (t instanceof MBeanException) {
                    t = ((MBeanException) t).getTargetException();
                }
                if (t instanceof InvocationTargetException) {
                    t = ((InvocationTargetException) t).getTargetException();
                }
                if (t instanceof UndeclaredThrowableException) {
                    t = ((UndeclaredThrowableException) t).getUndeclaredThrowable();
                }
                // It this a declared exception 
                for (Class<?> ext : method.getExceptionTypes()) {
                    if (ext.isAssignableFrom(t.getClass())) {
                        throw t;
                    }
                }
                // Finally we throw a GrmRemoteException (must be declared for each remote method)
                throw new GrmRemoteException("ComponentProxy.ex.uk", t, BUNDLE, compInfo,  GrmRemoteException.findRealMessage(t));
            }
        }
    }
    
    /*
     *  Get the MBeanInfo of the MBean
     *
     *  The first call of this method retrieves the MBeanInfo from the MBeanServer.
     *  @return the MBeanInfo
     */
    private MBeanInfo getMBeanInfo() throws Exception {
        MBeanInfo ret = mbeanInfo.get();
        if (ret == null) {
            log.log(Level.FINER, "ComponentProxy.mbeanInfo", compInfo.getObjectName());
            ret = connectionProxy.getProxy().getMBeanInfo(compInfo.getObjectName());
            mbeanInfo.set(ret);
        }
        return ret;
    }
    
    /**
     * Clear the MBean info of the component
     * 
     * Package private for testing
     */
    void clearMBeanInfo() {
        mbeanInfo.set(null);
    }
     
    /*
     *   Get a <code>MethodInvocationHandler</code> for a method
     *
     *   @param  method the method
     *   @param  mBeanInfo  the mBeanInfo (required for creating the <code>MethodInvocationHandler</code>
     *   @return the <code>MethodInvocationHandler</code>
     */
    private static MethodInvocationHandler getMethodInvocationHandler(Method method, ComponentProxy ci)  throws Exception {
        MethodInvocationHandler ret = methodInvocationCache.get(method);
        if(ret == null) {
            ret = createMethodInvocationHandler(method, ci);
            methodInvocationCache.put(method, ret);
        }
        return ret;
    }
    
    /**
     * Clears the method MethodInvocationHandlerCache 
     * 
     * Package private for testing
     */
    static void clearMethodInvocationHandlerCache() {
        methodInvocationCache.clear();
    }
    
    /*
     *   Create a <code>MethodInvocationHandler</code> for a method
     *
     *   @param method  the method
     *   @param mBeanInfo  the description of the MBean
     *   @return the <code>MethodInvocationHandler</code> for this method
     */
    private static MethodInvocationHandler createMethodInvocationHandler(Method method, ComponentProxy ci) throws Exception {
        
        if(method.getName().equals("toString")) {
            return new ToStringMethodInvocationHandler();
        }
        
        // Removing an event listener from a component must work even if
        // the remote mbean is no longer available (see issue 705)
        // Check that method is a EventListenerFromRemover before getting
        // the MBeanInfo

        // Is the method a event listener remove method
        Class eventListenerClass = ComponentEventDescriptor.getManagedEventListenerFromRemover(method);
        if (eventListenerClass != null) {
            return new EventListenerRemover(eventListenerClass);
        }
        
        Class<?> [] paramTypes = method.getParameterTypes();
        
        MBeanInfo mBeanInfo = ci.getMBeanInfo();
        // Is the method an attribute getter?
        ComponentAttributeDescriptor tmpAttrInfo = ComponentAttributeDescriptor.newInstanceFromAttributeGetter(method);
        if(tmpAttrInfo != null) {
            StringBuilder getterName = new StringBuilder();
            for(MBeanAttributeInfo attrInfo: mBeanInfo.getAttributes()) {
                if(attrInfo.getType().equals(method.getReturnType().getName())) {
                    if(tmpAttrInfo.isIs()) {
                        getterName.ensureCapacity(2 + attrInfo.getName().length());
                        getterName.append("is");
                    } else {
                        getterName.ensureCapacity(3 + attrInfo.getName().length());
                        getterName.append("get");
                    }
                    getterName.append(Character.toUpperCase(attrInfo.getName().charAt(0)));
                    getterName.append(attrInfo.getName().substring(1));
                    
                    if(getterName.toString().equals(method.getName())) {
                        return new AttributeGetter(attrInfo.getName());
                    }
                }
                getterName.setLength(0);
            }
        }
        
        // Is the method an attribute setter
        tmpAttrInfo = ComponentAttributeDescriptor.newInstanceFromAttributeSetter(method);
        if(tmpAttrInfo != null) {
            StringBuilder setterName = new StringBuilder();
            for(MBeanAttributeInfo attrInfo: mBeanInfo.getAttributes()) {
                if(attrInfo.getType().equals(paramTypes[0].getName())) {
                    setterName.ensureCapacity(3 + attrInfo.getName().length());
                    setterName.append("set");
                    setterName.append(Character.toUpperCase(attrInfo.getName().charAt(0)));
                    setterName.append(attrInfo.getName().substring(1));
                    if(setterName.toString().equals(method.getName())) {
                        return new AttributeSetter(attrInfo.getName());
                    }
                }
                setterName.setLength(0);
            }
        }
        
        // Is the method a event listener add method
        eventListenerClass = ComponentEventDescriptor.getManagedEventListenerFromAdder(method);
        if(eventListenerClass != null) {
            return new EventListenerAdder(eventListenerClass);
        }
        
        // Assume a normal method call
        for(MBeanOperationInfo opInfo: mBeanInfo.getOperations()) {
            if(opInfo.getName().equals(method.getName())) {
                return new MethodInvoker(method);
            }
        }
        
        throw new IllegalStateException(
                I18NManager.formatMessage("ComponentProxy.error.invalidMethod", BUNDLE, method));
    }
    
    private static interface MethodInvocationHandler {
        
        public Object invoke(ComponentProxy proxy, Object [] args) throws Throwable;
    }
    
    
    private static class ToStringMethodInvocationHandler implements MethodInvocationHandler {
        
        public Object invoke(ComponentProxy proxy, Object[] args) throws Throwable {
            
            String name = proxy.compInfo.getName();
            String host = proxy.compInfo.getHostname().getHostname();
            
            StringBuilder ret = new StringBuilder(12 + name.length() + host.length());
            ret.append("[Proxy to ");
            ret.append(name);
            ret.append('@');
            ret.append(host);
            ret.append("]");
            return ret.toString();
        }
        
        
    }
    
    private static class AttributeGetter implements MethodInvocationHandler {
        
        private String attribute;
        
        public AttributeGetter(String attribute) {
            this.attribute = attribute;
        }
        
        public Object invoke(ComponentProxy proxy, Object[] args) throws Throwable {
            return proxy.connectionProxy.getProxy().getAttribute(proxy.compInfo.getObjectName(), attribute);
        }
    }
    
    private static class AttributeSetter implements MethodInvocationHandler {
        private String attribute;
        
        public AttributeSetter(String attribute) {
            this.attribute = attribute;
        }
        
        public Object invoke(ComponentProxy proxy, Object[] args) throws Throwable {
            proxy.connectionProxy.getProxy().setAttribute(proxy.compInfo.getObjectName(), new Attribute(attribute, args[0]));
            return null;
        }
    }
    
    private static class EventListenerAdder implements MethodInvocationHandler {
        
        private final Class listenerClass;
        
        public EventListenerAdder(Class listenerClass) {
            this.listenerClass = listenerClass;
        }
        
        public Object invoke(ComponentProxy proxy, Object[] args) throws Throwable {
            for(ComponentEventDescriptor evt: ComponentEventDescriptor.getComponentEventInfosFromListener(listenerClass)) {
                proxy.connectionProxy.addEventListener(evt, proxy.compInfo.getObjectName(), args[0]);
            }
            return null;
        }
    }
    
    private static class EventListenerRemover implements MethodInvocationHandler {
        
        private final Class listenerClass;
        
        public EventListenerRemover(Class listenerClass) {
            this.listenerClass = listenerClass;
        }
        public Object invoke(ComponentProxy proxy, Object[] args) throws Throwable {
            for(ComponentEventDescriptor evt: ComponentEventDescriptor.getComponentEventInfosFromListener(listenerClass)) {
                proxy.connectionProxy.removeEventListener(evt, proxy.compInfo.getObjectName(), args[0]);
            }
            return null;
        }
    }
    
    private static class MethodInvoker implements MethodInvocationHandler {
        
        private final String [] signature;
        private final String methodName;
        private final Method method;
        public MethodInvoker(Method method) {
            this.method = method;
            this.methodName = method.getName();
            Class<?> [] paramTypes = method.getParameterTypes();
            this.signature = new String[paramTypes.length];
            for(int i = 0; i < paramTypes.length; i++) {
                this.signature[i] = paramTypes[i].getName();
            }
            
            
        }
        public Object invoke(ComponentProxy proxy, Object[] args) throws Throwable {
            return proxy.connectionProxy.getProxy().invoke(proxy.compInfo.getObjectName(), methodName, args, signature);
        }
    }
}
