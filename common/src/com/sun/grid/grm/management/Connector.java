/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.security.GrmSecurityException;
import com.sun.grid.grm.util.I18NManager;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

/**
 *  This class implements a JMX connection establishment with timeout mechanism.
 * 
 *  <p>The timeout can be specified via the system property 
 *     <code>com.sun.grid.grm.management.connectionTimeout</code>. It defines
 *     max time in seconds for creating a connection to a remove jvm.</p>
 * 
 *  <p>This class has been introduced with the fix of issue 663. For details
 *     please have a look into the issue description.</p>
 */
class Connector {

    private final static Connector instance = new Connector();
    final static String BUNDLE = "com.sun.grid.grm.management.management";
    private final static Logger log = Logger.getLogger(Connector.class.getName(), BUNDLE);
    private final ExecutorService executor = Executors.newCachedThreadPool();
    
    /**
     * Name of the system property which defines the connection timeout.
     */
    final static String CONNECTION_TIMEOUT_SYSPROP_NAME = "com.sun.grid.grm.management.connectionTimeout";
    /**
     * Default connection timeout in seconds
     */
    final static int DEFAULT_CONNECTION_TIMEOUT = 180;
    
    private final int connectionTimeout;
    
    private Connector() {
        this(getConnectionTimeoutFromSystemProperty());
    }
    
    /**
     * Get the connection timeout from the system property.
     * 
     * <p>If the system property is not defined or if it contains an invalid value
     *    the default connection timeout is returned.</p>
     *     
     * <p>Package private for testing</p>
     * @return the connection timeout from the system property
     */
    static int getConnectionTimeoutFromSystemProperty() {
        int ret;
        String prop = System.getProperty(CONNECTION_TIMEOUT_SYSPROP_NAME);
        if (prop != null) {
            try {
                ret = Integer.parseInt(prop);
                if (ret <= 0) {
                    log.log(Level.WARNING, "connector.negativeTimeout", 
                            new Object [] { CONNECTION_TIMEOUT_SYSPROP_NAME, prop, DEFAULT_CONNECTION_TIMEOUT });
                    ret = DEFAULT_CONNECTION_TIMEOUT;
                }
            } catch (NumberFormatException ex) {
                log.log(Level.WARNING, "connector.invalidTimeout", 
                        new Object [] { CONNECTION_TIMEOUT_SYSPROP_NAME, prop, DEFAULT_CONNECTION_TIMEOUT });
                ret = DEFAULT_CONNECTION_TIMEOUT;
            }
        } else {
            ret = DEFAULT_CONNECTION_TIMEOUT;
        }
        return ret;
    }
    
    /**
     * Package private for testing.
     * 
     * @param aConnectionTimeout
     */
    Connector(int aConnectionTimeout) {
        if (aConnectionTimeout <= 0) {
            throw new IllegalArgumentException("aConnectionTimeout must be greater than zero");
        }
        this.connectionTimeout = aConnectionTimeout;
    }

    /**
     * Get the singleton instance of the Connector
     * @return the single instance
     */
    static Connector getInstance() {
        return instance;
    }

    /**
     * Close the Connector.
     * 
     * <p>After calling this method no further connections can be established.
     *    On going connect attempts will be interrupted.</p>
     */
    void close() {
        log.entering(Connector.class.getName(), "close");
        executor.shutdownNow();
        log.exiting(Connector.class.getName(), "close");
    }

    /**
     * Get the connection timeout in seconds.
     * 
     * Package private for testing
     * @return the connection timeout in seconds
     */
    int getConnectionTimeout() {
        return connectionTimeout;
    }

    /**
     * Establish a connection to a remove jvm
     * @param env  the execution env
     * @param url  the JMXServiceURL of the remote jvm
     * @return the JMXConnector to the remote jvm
     * @throws java.io.IOException if the connection could not be established
     * @throws com.sun.grid.grm.security.GrmSecurityException if the security setup in the
     *                             execution environment is not correct
     */
    JMXConnector connect(ExecutionEnv env, JMXServiceURL url) throws IOException, GrmSecurityException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(Connector.class.getName(), "connect", new Object[]{env, url});
        }
        ConnectRunnable conn = new ConnectRunnable(env, url);
        try {
            executor.submit(conn);
            JMXConnector ret = conn.get(connectionTimeout, TimeUnit.SECONDS);
            log.exiting(Connector.class.getName(), "connect", ret);
            return ret;
        } catch (RejectedExecutionException ex) {
            throw new IOException(I18NManager.formatMessage("connector.ex.goingdown", BUNDLE));
        }
    }

    private static class ConnectRunnable implements Runnable {

        private final JMXServiceURL url;
        private final ExecutionEnv env;
        private final Lock lock = new ReentrantLock();
        private final Condition connectedCondition = lock.newCondition();
        private boolean canceled;
        private JMXConnector connector;
        private Thread executingThread;
        private Exception error;

        private ConnectRunnable(ExecutionEnv env, JMXServiceURL url) {
            this.url = url;
            this.env = env;
        }

        private void checkError() throws GrmSecurityException, IOException {
            if (error != null) {
                if (error instanceof GrmSecurityException) {
                    throw (GrmSecurityException) error;
                } else if (error instanceof IOException) {
                    throw (IOException) error;
                } else {
                    // In java 5 the class IOException has no constructor with a cause parameter
                    IOException ex = new IOException(I18NManager.formatMessage("connector.ex.unknown", BUNDLE, error.getLocalizedMessage()));
                    ex.initCause(error);
                    throw ex;
                }
            }
        }

        public JMXConnector get(long timeout, TimeUnit timeUnit) throws IOException, GrmSecurityException {
            if (log.isLoggable(Level.FINER)) {
                log.entering(ConnectRunnable.class.getName(), "get", new Object[]{timeout, timeUnit});
            }
            JMXConnector ret = null;
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "connector.connecting", new Object[]{url, timeout, timeUnit});
            }
            JMXConnector tmp = null;
            lock.lock();
            try {
                if (connector != null) {
                    ret = connector;
                } else {
                    checkError();
                    connectedCondition.await(timeout, timeUnit);
                    if (connector != null) {
                        ret = connector;
                    } else {
                        checkError();
                        if (executingThread != null) {
                            executingThread.interrupt();
                        }
                        canceled = true;
                        throw new SocketTimeoutException(I18NManager.formatMessage("connector.ex.timeout", BUNDLE));
                    }
                }
            } catch (InterruptedException ex) {
                log.log(Level.FINE, "connector.interrupted", url);
                canceled = true;
                // It can happen that the waiting thread has been interrupted and
                // the connection thread has already the connector
                // In this case the waiting thread must close the connector
                tmp = this.connector;
                this.connector = null;
                InterruptedIOException e = new InterruptedIOException(I18NManager.formatMessage("connector.ex.interrupted", BUNDLE));
                e.initCause(ex);
                throw e;
            } finally {
                lock.unlock();
                try {
                    tmp.close();
                } catch (Exception ex) {
                    // Ignore
                }
            }
            log.log(Level.FINE, "connector.connected", url);
            log.exiting(ConnectRunnable.class.getName(), "get", ret);
            return connector;
        }

        public void run() {
            log.entering(ConnectRunnable.class.getName(), "run");
            lock.lock();
            try {
                executingThread = Thread.currentThread();
            } finally {
                lock.unlock();
            }
            try {
                JMXConnector tmp = null;
                if (env.getSecurityContext() == null) {
                    tmp = JMXConnectorFactory.connect(url);
                } else {
                    tmp = env.getSecurityContext().connect(url);
                }
                lock.lock();
                try {
                    if (!canceled) {
                        this.connector = tmp;
                        tmp = null;
                        connectedCondition.signalAll();
                    }
                    executingThread = null;
                } finally {
                    lock.unlock();
                }
                // If tmp is not null the connection request
                // has been canceled
                // => close the connection otherwise we have a resource leak
                if (tmp != null) {
                    try {
                        tmp.close();
                    } catch (Exception ex) {
                        // Ignore
                    }
                }
            } catch (Exception ex) {
                lock.lock();
                try {
                    this.error = ex;
                    executingThread = null;
                    connectedCondition.signalAll();
                } finally {
                    lock.unlock();
                }
            }
            log.exiting(ConnectRunnable.class.getName(), "run");
        }
    }
}
