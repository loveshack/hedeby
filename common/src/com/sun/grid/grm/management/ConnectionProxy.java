/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.management;

import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.JvmInfo;
import com.sun.grid.grm.config.common.ActiveJvm;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.util.I18NManager;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.UndeclaredThrowableException;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.management.InstanceNotFoundException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationFilterSupport;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectionNotification;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;

/**
 *  The ConnectionProxy handles the JMX connections to a jvm.
 */
public class ConnectionProxy implements InvocationHandler {
    
    /**  default number of min. open connections */
    public final static int DEFAULT_MIN_CONNECTION_COUNT = 1;
    
    /** default connection idle time in millis (5 minutes) */
    public final static long DEFAULT_MAX_IDLE_TIME = 5 * 60;
    
    private final static String BUNDLE = "com.sun.grid.grm.management.management";
    private final static Logger log = Logger.getLogger(ConnectionProxy.class.getName(), BUNDLE);
    
    private final static EventMapping SPECIAL_EVENT_MAPPING = new EventMapping();
    
    /** Dynamic proxy to the mbean server connection */
    private final MBeanServerConnection proxy;
    
    private final Lock lock = new ReentrantLock();
    
    private final ExecutionEnv env;
    private final JvmInfo jvmInfo;
    
    private final LinkedList<ConnectionHandler> connections = new LinkedList<ConnectionHandler>();

    private int minConnectionCount  = DEFAULT_MIN_CONNECTION_COUNT;
    private long maxIdleTime = DEFAULT_MAX_IDLE_TIME;
    private int  connectionSeqNr = 0;
    private boolean closed;
    
    /**
     * Create a new instance of ConnectionProxy.
     * @param aEnv     the execution env
     * @param aJvmInfo the jvm info
     */
    public ConnectionProxy(ExecutionEnv aEnv, JvmInfo aJvmInfo) {
        env = aEnv;
        jvmInfo = aJvmInfo;
        proxy = (MBeanServerConnection)Proxy.newProxyInstance(getClass().getClassLoader(), new Class<?> [] { MBeanServerConnection.class }, this);
        for(int i = 0; i < minConnectionCount; i++) {
            connections.add(new ConnectionHandler(connectionSeqNr++));
        }
    }
    
    /**
     * Close all connections used by this connection proxy
     */
    public void close() {
        lock.lock();
        try {
            closed = true;
            // Close all open connections
            for(ConnectionHandler ch: connections) {
               ch.clearNotificationForwarders();
               ch.close();
            }
        } finally {
            lock.unlock();
        }
    }
    
    private ConnectionHandler getConnection() throws GrmException {
        ConnectionHandler ret = null;
        lock.lock();
        try {
            if(closed) {
                throw new GrmException("ConnectionProxy.ex.closed", BUNDLE);
            }
            ListIterator<ConnectionHandler> iter = connections.listIterator();
            int count = connections.size();
            while (iter.hasNext()) {
                ConnectionHandler handler = iter.next();
                if(handler.isIdle()) {
                    if(ret == null) {
                        ret = handler;
                    } else if (handler.getIdleTime() > maxIdleTime && count > minConnectionCount) {
                        // Remove an idle connection
                        if(log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "ConnectionProxy.closeIdle", 
                                    new Object [] { handler.getName(), handler.getIdleTime() / 1000 });
                        }
                        handler.close();
                        iter.remove();
                        count--;
                    }
                }
            }
            if (ret == null) {
                // No free connection available, create a new one
                ret = new ConnectionHandler(connectionSeqNr++);
                connections.addLast(ret);
                if(log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE,"ConnectionProxy.connAdd", ret.getName());
                }
            }
            ret.busy();
        } finally {
            lock.unlock();
        }
        if(log.isLoggable(Level.FINER)) {
            log.log(Level.FINER,"ConnectionProxy.connUsed", new Object [] { ret.getName(), Thread.currentThread().getName() });
        }
        return ret;
    }
    
    private void releaseConnection(ConnectionHandler handler) {
        try {
            lock.lock();
            handler.idle();
         } finally {
            lock.unlock();
        }
        if(log.isLoggable(Level.FINER)) {
            log.log(Level.FINER,"ConnectionProxy.connReleased", new Object [] { handler.getName(), Thread.currentThread().getName() });
        }
    }
    
    /**
     *   Get the dynamic proxy to the MBeanServerConnection.
     *
     *   @return the dynamic proxy
     */
    public MBeanServerConnection getProxy() {
        return proxy;
    }
    
    /**
     * Innvocation method to the MBeanServer.
     *
     * @param proxy   the proxy object
     * @param method  the called method
     * @param args    the arguments for the method calls
     * @throws java.lang.Throwable the execption
     * @return  the result of the method call
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        
        ConnectionHandler handler = getConnection();
        try {
             return method.invoke(handler.getConnection(), args);
        } catch(Exception ex) {
            Throwable t = getRealError(ex);
            //reconnect in case of a remote exception in any other case throw t 
            // this avoid a retry in case of a thread interruption (Issue 644)
            if ( (t instanceof GrmRemoteException || t instanceof IOException) && !isCausedByConnectNotPossibleIOException(t)) {
                // Reconnect
                if(log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "ConnectionProxy.failure", t);
                }
                ConnectionPool.getInstance(env).resetJvmInfo(jvmInfo);
                 try {
                    return method.invoke(handler.reconnect(), args);
                } catch(Exception ex1) {
                    throw getRealError(ex1);
                }
            } else {
                throw t;
            }
        } finally {
            releaseConnection(handler);
        }
    }
        
    
    private boolean isCausedByConnectNotPossibleIOException(Throwable ex){
        // SocketTimeoutException extends InterruptedIOException
        // => no additional check neccessary in the case of a connection timeout
        if(ex instanceof InterruptedIOException){
            return true;
        }else if(ex.getCause()!=null){
            return isCausedByConnectNotPossibleIOException(ex.getCause());
        }
        return false;
    }
    
    private Throwable getRealError(Exception ex) {
        if(ex instanceof InvocationTargetException) {
            return ((InvocationTargetException)ex).getTargetException();
        } else if (ex instanceof UndeclaredThrowableException) {
            return ((UndeclaredThrowableException)ex).getUndeclaredThrowable();
        }
        return ex;
    }
    
    /*
     *  Register a new listener for an event
     *
     *  @param env the description of the event
     *  @param listener the event listener
     */
    public void addEventListener(ComponentEventDescriptor evt, ObjectName objName, Object listener) throws IOException, InstanceNotFoundException, GrmException {
        
        lock.lock();
        try {
            if(closed) {
                throw new GrmException("ConnectionProxy.ex.closed", BUNDLE);
            }
            // We have always one open connection
            // This connection is always used from notification
            connections.getFirst().addEventListener(evt, objName, listener);
        } finally {
            lock.unlock();
        }
    }
    
    /*
     *   Deregister a event listener for a event type
     *
     *   @param  evtInfo   description of the event
     *   @param  listener  the event listener
     */
    public void removeEventListener(ComponentEventDescriptor evtInfo, ObjectName objName, Object listener) throws IOException, GrmException, InstanceNotFoundException, ListenerNotFoundException {
        lock.lock();
        try {
            if(closed) {
                throw new GrmException("ConnectionProxy.ex.closed", BUNDLE);
            }
            // We have always one open connection
            // This connection is always used from notification
            connections.getFirst().removeEventListener(evtInfo, objName, listener);
        } finally {
            lock.unlock();
        }
    }
    
    private static String getKey(ComponentEventDescriptor evtInfo, ObjectName objectName) {
        String evtName = evtInfo.getEventName();
        String objName = objectName.toString();
        StringBuilder ret = new StringBuilder(evtName.length() + objName.length() + 1);
        ret.append(evtName);
        ret.append('@');
        ret.append(objName);
        return ret.toString();
    }

    /**
     * Get the minimum number of open connections.
     * @return the minimum number of open connections
     */
    public int getMinConnectionCount() {
        return minConnectionCount;
    }

    /**
     * Set the minimum number of open connections.
     * 
     * @param minConnectionCount the minimum number of open connections (must be
     *                            greater the 0
     */
    public void setMinConnectionCount(int minConnectionCount) {
        if (minConnectionCount <= 0) {
            throw new IllegalArgumentException("minConnectionCount must greater then 0");
        }
        this.minConnectionCount = minConnectionCount;
    }

    /**
     * Get the max idle time of a connection
     * @return the mix idle time of a connection in millis
     */
    public long getMaxIdleTime() {
        return maxIdleTime;
    }

    /**
     * Set the max idle time of a connection
     * @param maxIdleTime max idle time of a connection (must be greater the 0)
     */
    public void setMaxIdleTime(long maxIdleTime) {
        if(maxIdleTime < 0) {
            throw new IllegalArgumentException("minConnectionCount must be greater then 0");
        }
        this.maxIdleTime = maxIdleTime;
    }
    
    /**
     *   The conneciton handler is responsible to connect to a jvm and to
     *   monitor the connection
     */
    private class ConnectionHandler  {
        
        
        private Lock connectionLock = new ReentrantLock();
        private MBeanServerConnection connection;
        private JMXConnector connector;
        private ConnectionNotificationListener connectionListener;
        private boolean idle;
        private long idleTs;
        private final String name;
        /** This map contains for each supported event an notification listener which forwards
         *  the JMX notification to the local listeners */
        private final Map<String, NotificationForwarder> notificationForwarderMap = new HashMap<String, NotificationForwarder>();

        @Override
        public String toString() {
            return name;
        }
        
        public ConnectionHandler(int seqNr) {
            name = String.format("%s[%d]", jvmInfo.getIdentifier() , seqNr);
            idle = true;
            idleTs = 0;
        }
        
        /**
         *  Get the name of the connection
         *  @return the name of the connection
         */
        public String getName() {
            return name;
        }
        
        /**
         * Removes all notification forwarders from this connection handler
         */
        public void clearNotificationForwarders() {
            connectionLock.lock();
            try {
                // Clean up all event listeners
                for(NotificationForwarder fw: this.notificationForwarderMap.values()) {
                    try {
                        fw.clear();
                    } catch(Exception ex) {
                        if(log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "ConnectionProxy.dnff", ex);
                        }
                    }
                }
                notificationForwarderMap.clear();
            } finally {
                connectionLock.unlock();
            }
        }
        
        /**
         *  Close the connection.
         */
        public void close() {
            
//            JMXConnector oldConnector = null;
            connectionLock.lock();
            try {
                connection = null;
//                oldConnector = connector;
                connector = null;
                connectionListener = null;
            } finally {
                connectionLock.unlock();
            }
//            if(oldConnector != null) {
//                try {
//                    oldConnector.close();
//                } catch(IOException ex) {
//                    // Ignore
//                }
//            }
        }
        
        /**
         * Get the real connection to the remove jvm.
         * If the connection is not established the method tries to connect
         * @throws com.sun.grid.grm.GrmException 
         * @return the real connection to the remote jvm
         */
        public MBeanServerConnection getConnection() throws GrmException {
            connectionLock.lock();
            try {
                if(connection == null) {
                    ActiveJvm activeJvmInfo = ConnectionPool.getInstance(env).getJvmInfo(jvmInfo);
                    if(activeJvmInfo == null) {
                        throw new GrmRemoteException("ConnectionPool.ex.jvmNotActive", BUNDLE, jvmInfo);
                    }
                    String urlPattern ="service:jmx:rmi:///jndi/rmi://%s:%d/%s";
                    String urlStr = String.format(urlPattern, activeJvmInfo.getHost(), activeJvmInfo.getPort(), env.getSystemName());
                    JMXServiceURL url;
                    try {
                        url = new JMXServiceURL(urlStr);
                    } catch(MalformedURLException ex) {
                        throw new GrmRemoteException("ConnectionPool.ex.iurl", ex, BUNDLE, jvmInfo.getIdentifier(), urlStr);
                    }
                    
                    try {
                        // Issue 663: connection must be established via the Connector
                        // It provides a mechaism to control the connection timeout
                        connector = Connector.getInstance().connect(env, url);
                        connectionListener = new ConnectionNotificationListener();
                        connector.addConnectionNotificationListener(connectionListener, null, null);
                        connection = connector.getMBeanServerConnection();
                        if(log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "ConnectionProxy.connected", new Object [] { name, url });
                        }
                        for(Map.Entry<String,NotificationForwarder> nf: notificationForwarderMap.entrySet()) {
                            enableNotificationForwarder(nf.getValue());
                        }
                        
                    } catch(IOException ex) {
                        throw new GrmRemoteException("ConnectionPool.ex.io", ex, BUNDLE, jvmInfo.getIdentifier(), GrmRemoteException.findRealMessage(ex));
                    }
                }
                return connection;
            } finally {
                connectionLock.unlock();
            }
        }
        
        /**
         *   Close the connection and create a new one.
         *
         * @throws com.sun.grid.grm.GrmException if the new connection can not be establisched
         * @return the new connection
         */
        public MBeanServerConnection reconnect() throws GrmException {
            close();
            return getConnection();
        }
        
        /**
         *  Instances of this class listens on a JMX connection and report
         *  possible problems.
         */
        private class ConnectionNotificationListener implements NotificationListener {
            
            public void handleNotification(Notification notification, Object handback) {
                if(log.isLoggable(Level.FINER)) {
                    log.entering(ConnectionNotificationListener.class.getName(), "handleNotification", notification);
                }
                if(JMXConnectionNotification.CLOSED.equals(notification.getType()) ||
                    JMXConnectionNotification.FAILED.equals(notification.getType())) {
                    connectionLock.lock();
                    try {
                        // Be sure that the event comes from the current connection
                        if(this == connectionListener && connection != null) {
                            log.log(Level.WARNING, "ConnectionProxy.closedRemotely", name);
                            // Do not call the close method it can cause a deadlock
                            connection = null;
                            connector = null;
                            connectionListener = null;
                        }
                    } finally {
                        connectionLock.unlock();
                    }
                } else if (JMXConnectionNotification.NOTIFS_LOST.equals(notification.getType())) {
                    log.log(Level.WARNING,"ConnectionProxy.notificationsLost", name);
                } else if (JMXConnectionNotification.OPENED.equals(notification.getType())) {
                    log.log(Level.FINE,"ConnectionProxy.connOpened", name);
                    // JMX server has reopened the jmx connection
                    // This means that all notification listeners are lost
                    connectionLock.lock();
                    try {
                        for(Map.Entry<String,NotificationForwarder> nf: notificationForwarderMap.entrySet()) {
                            try {
                                enableNotificationForwarder(nf.getValue());
                            } catch(Exception ex) {
                                LogRecord lr = new LogRecord(Level.WARNING, "ConnectionProxy.ex.reReg");
                                lr.setParameters(new Object [] {name, ex.getLocalizedMessage()});
                                lr.setThrown(ex);
                                log.log(lr);
                            }
                        }
                    } finally {
                        connectionLock.unlock();
                    }
                } 
                
                // Is the event a special event
                String forwardedEventName = SPECIAL_EVENT_MAPPING.getForwardedEventName(notification.getType());
                if (forwardedEventName != null) {
                    
                    // We have a special event => forward it to the interrested listeners
                    List<NotificationForwarder> forwarders = new LinkedList<NotificationForwarder>();
                    connectionLock.lock();
                    try {
                        for(NotificationForwarder nf: notificationForwarderMap.values()) {
                            if (nf.getEventInfo().getEventName().equals(forwardedEventName)) {
                                forwarders.add(nf);
                            }
                        }
                    } finally {
                        connectionLock.unlock();
                    }
                    if (forwarders.size() > 0) {
                        Notification notify = new Notification(forwardedEventName, notification.getSource(), 
                                notification.getSequenceNumber(), notification.getTimeStamp());
                        for(NotificationForwarder fw: forwarders) {
                            fw.handleNotification(notify, null);
                        }
                    }
                }
                
                if(log.isLoggable(Level.FINER)) {
                    log.exiting(ConnectionNotificationListener.class.getName(), "handleNotification");
                }
            }
        }
        
        /*
         *  Register a new listener for an event
         *
         *  @param env the description of the event
         *  @param listener the event listener
         */
        public void addEventListener(ComponentEventDescriptor evt, ObjectName objName, Object listener) throws IOException, InstanceNotFoundException, GrmException {
            String key = getKey(evt, objName);
            int size;
            NotificationForwarder notificationForwarder;
            connectionLock.lock();
            try {
                notificationForwarder = notificationForwarderMap.get(key);
                if (notificationForwarder == null) {
                    notificationForwarder = new NotificationForwarder(evt, objName);
                    notificationForwarderMap.put(key, notificationForwarder);
                }
                if (connection == null) {
                    // Issue 705
                    // We can only add a listener if the connection to the component
                    // is established.
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "ConnectionProxy.adLis.connect", new Object [] { this, listener,  notificationForwarder});
                    }
                    getConnection();
                }
                size = notificationForwarder.addListener(listener);
                if (size == 1) {
                    // It was the first registered listener -> enable the notification forwarder
                    enableNotificationForwarder(notificationForwarder);
                }

            } finally {
                connectionLock.unlock();
            }
            if (log.isLoggable(Level.FINE)) {
                switch(size) {
                    case 1:
                        log.log(Level.FINE, "ConnectionProxy.adLis.first", new Object [] { this, notificationForwarder, listener });
                        break;
                    case -1:
                        log.log(Level.FINE, "ConnectionProxy.adLis.exist", new Object [] { this, notificationForwarder, listener });
                        break;
                    default:
                        log.log(Level.FINE, "ConnectionProxy.adLis.added", new Object [] { this, notificationForwarder, listener, size });
                }
            }
        }

        /*
         *   Deregister a event listener for a event type
         *
         *   @param  evtInfo   description of the event
         *   @param  listener  the event listener
         */
        public void removeEventListener(ComponentEventDescriptor evtInfo, ObjectName objName, Object listener) throws IOException, GrmException, InstanceNotFoundException, ListenerNotFoundException {
            String key = getKey(evtInfo, objName);
            int size = 0;
            NotificationForwarder notificationForwarder;
            connectionLock.lock();
            try {
                notificationForwarder = notificationForwarderMap.get(key);
                if (notificationForwarder == null) {
                    log.log(Level.WARNING, "ConnectionProxy.rmLis.noFW", new Object [] {this, listener, key});
                    return;
                } 
                size = notificationForwarder.removeListener(listener);
                if (size == 0) {
                    // It was the last registered listener -> disable the notification forwarder
                    disableNotificationForwarder(notificationForwarder);
                }
            } finally {
                connectionLock.unlock();
            }
            if (log.isLoggable(Level.FINE)) {
                switch(size) {
                    case 0:
                        log.log(Level.FINE, "ConnectionProxy.rmLis.last", new Object [] { this, notificationForwarder, listener });
                        break;
                    case -1:
                        log.log(Level.FINE, "ConnectionProxy.rmLis.notexist", new Object [] { this, notificationForwarder, listener });
                        break;
                    default:
                        log.log(Level.FINE, "ConnectionProxy.rmLis.removed", new Object [] { this, notificationForwarder, listener, size });
                }
            }
        }
        
        
        private void enableNotificationForwarder(NotificationForwarder forwarder) throws IOException {
            connectionLock.lock();
            try {
                if(connection != null && forwarder.hasListeners()) {
                    NotificationFilterSupport filter = new NotificationFilterSupport();
                    filter.enableType(forwarder.getEventInfo().getEventName());
                    try {
                        connection.removeNotificationListener(forwarder.getObjName(), forwarder);
                    } catch(Exception ex) {
                        // Ignore
                    }
                    try {
                        connection.addNotificationListener(forwarder.getObjName(), forwarder, filter, null);
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "ConnectionProxy.lis.enabled", new Object [] { this, forwarder });
                        }
                    } catch (InstanceNotFoundException ex) {
                        log.log(Level.WARNING,"ConnectionProxy.ex.noObjForList", forwarder.getObjName());
                    }
                }
            } finally {
                connectionLock.unlock();
            }
        }
        
        private void disableNotificationForwarder(NotificationForwarder forwarder)
                throws IOException, GrmException {
            try {
                connectionLock.lock();
                try {
                    if (connection != null) {
                        connection.removeNotificationListener(forwarder.getObjName(), forwarder);
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "ConnectionProxy.lis.disabled", new Object [] { this, forwarder });
                        }
                    }
                } finally {
                    connectionLock.unlock();
                }
            } catch(ListenerNotFoundException ex) {
                // The listener has already been removed
                log.log(Level.FINE, "ConnectionProxy.rml", ex);
            } catch(InstanceNotFoundException ex) {
                // If the instance does no longer exists we do not report
                // an error
                log.log(Level.FINE, "ConnectionProxy.rml", ex);
            }
        }
        
        
        /**
         * is the connection idle (not in use)
         * @return <code>true</code> if the connection is idle
         */
        public boolean isIdle() {
            return idle;
        }
 
        /**
         *  Mark the connection as busy
         */
        public void busy() {
            idle = false;
            idleTs = 0;
        }
        
        /**
         *  Mark the connection as idle
         */
        public void idle() {
            idle = true;
            idleTs = System.currentTimeMillis();
        }
        
        /**
         *  Get the idle time of the connection
         *  @return the idle time in millis
         */
        public long getIdleTime() {
            if(idleTs > 0) {
                return System.currentTimeMillis() - idleTs;
            } else {
                return 0;
            }
        }

    }
    
    private class NotificationForwarder implements NotificationListener {
        
        private final Lock lock = new ReentrantLock();
        private final ComponentEventDescriptor evtInfo;
        private final ObjectName objName;
        private final String toString;
        private Set<Object> listeners = Collections.<Object>emptySet();
        
        /**
         * Create a new <code>NotificationForwarder</code>.
         *
         * @param evtInfo       description of the event
         * @param aObjName      name of the mbeam
         */
        public NotificationForwarder(ComponentEventDescriptor aEvtInfo, ObjectName aObjName) {
            this.evtInfo = aEvtInfo;
            objName = aObjName;

            toString = String.format("[NotificationForwarder:evt=%s.%s,obj=%s]",
                    aEvtInfo.getListenerClass().getSimpleName(), aEvtInfo.getListenerMethod().getName(),
                    objName.getKeyProperty("name"));
        }
        
        /**
         * Remove all listeners from the NotificationForwarder
         */
        public void clear() {
            lock.lock();
            try {
                listeners.clear();
            } finally {
                lock.unlock();
            }
        }
        
        public int getListenerCount() {
            lock.lock();
            try {
                return listeners.size();
            } finally {
                lock.unlock();
            }
        }
        
        /**
         *  Add a new listener for the event
         *
         *  @param   listener  the listener
         *  @return   the new listener count or -1 if the listener is already registered
         */
        @SuppressWarnings("unchecked")
        public int addListener(Object listener) throws IOException, GrmException, InstanceNotFoundException {
            if(listener == null) {
                throw new NullPointerException("listener must not be null");
            }
            
            if(!evtInfo.getListenerClass().isAssignableFrom(listener.getClass())) {
                throw new IllegalArgumentException(
                    I18NManager.formatMessage("ConnectionPool.ex.invalidListener", BUNDLE,
                    listener.getClass().getName(), evtInfo.getListenerClass().getName()));
            }
            int ret = 0;
            lock.lock();
            try {
                if(!listeners.contains(listener)) {
                    Set tmpListeners = new HashSet<Object>(listeners.size()+1);
                    tmpListeners.addAll(listeners);
                    tmpListeners.add(listener);
                    listeners = tmpListeners;
                    ret = listeners.size();
                } else {
                    ret = -1;
                }
            } finally {
                lock.unlock();
            }
            return ret;
        }
        
        /**
         *  Remove a listener.
         *
         *  If this <code>NotificationForwarder</code> has no more listner the
         *  <code>NotificationListener</code> for this code is removed from the
         *  connection
         *
         *  @param listener the listener
         *  @return the new number of listeners or -1 if the listener was not registered
         */
        public int removeListener(Object listener) throws IOException, GrmException, InstanceNotFoundException, ListenerNotFoundException {
            int ret = 0;
            lock.lock();
            try {
                if(listeners.contains(listener)) {
                    Set<Object> tmpListeners = new HashSet<Object>(listeners);
                    tmpListeners.remove(listener);
                    listeners = tmpListeners;
                    ret = listeners.size();
                } else {
                    ret = -1;
                }
            } finally {
                lock.unlock();
            }
            return ret;
        }
        
        public boolean hasListeners() {
            lock.lock();
            try {
                return listeners.size() > 0;
            } finally {
                lock.unlock();
            }
        }
        
        /**
         *  Handler method for incoming notification from the MBean.
         *
         *  This method dispatches the notification are forwards the event to
         *  the registered listeners.
         *
         *  @param notification  the JMX notification
         *  @param handback      the notification object (currently not used)
         */
        public void handleNotification(Notification notification, Object handback) {
            Set<Object> tmpListeners = null;
            lock.lock();
            try {
                tmpListeners = listeners;
            } finally {
                lock.unlock();
            }
            Method listenerMethod = evtInfo.getListenerMethod();
            boolean logIt = log.isLoggable(Level.FINER);
            
            for(Object listener: tmpListeners) {
                try {
                    if(logIt) {
                        log.log(Level.FINER,"ConnectionProxy.notify", listener);
                    }
                    Object [] args = (Object[])notification.getUserData();
                    listenerMethod.invoke(listener, args);
                } catch (IllegalAccessException ex) {
                    throw new IllegalStateException(
                        I18NManager.formatMessage("ConnectionProxy.ex.listenerNotAccessible", BUNDLE), ex);
                } catch (InvocationTargetException ex) {
                    throw new IllegalStateException(I18NManager.formatMessage("ConnectionProxy.ex.listenerError", BUNDLE),
                        ex.getTargetException());
                }
            }
        }
        
        /**
         *  Get the descriptor of the event which is handled by this NotificationForwarder
         *  @return the descriptor of the event
         */
        public ComponentEventDescriptor getEventInfo() {
            return evtInfo;
        }

        /**
         *  Get the name of the mbean which sends the notifications.
         *
         *  @return the name of the mbean
         */
        public ObjectName getObjName() {
            return objName;
        }

        /**
         *  Returns a string in the format<br/>
         *  [NotificationForwarder:evt=&lt;Listener class name&gt;.&lt;Listener method name&gt;,obj=&lt;component name&gt;]
         * 
         * @return the print representation
         */
        @Override
        public String toString() {
            return toString;
        }
    }
    
    private static class EventMapping {

        private Map<String, String> mappedEvents = new HashMap<String, String>(3);

        public EventMapping() {
            addEvent(JMXConnectionNotification.CLOSED, ComponentEventListener.class, "connectionClosed");
            addEvent(JMXConnectionNotification.FAILED, ComponentEventListener.class, "connectionFailed");
            addEvent(JMXConnectionNotification.NOTIFS_LOST, ComponentEventListener.class, "eventsLost");
        }

        private void addEvent(String orgEventName, Class<?> eventListenerClass, String methodName) {
            try {
                Method m = eventListenerClass.getMethod(methodName);
                mappedEvents.put(orgEventName, ComponentEventDescriptor.getEventName(m));
            } catch(NoSuchMethodException nsme) {
                throw new IllegalStateException(String.format("event listener method %s not found in class %s", methodName, eventListenerClass.getName()), nsme);
            }
        }

        public String getForwardedEventName(String orgEventName) {
            return mappedEvents.get(orgEventName);
        }
    }
    
    
}
