/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.config.gef.AbstractStepConfig;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.TimeIntervalHelper;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Map;

/**
 * Interface for a step of a GEF action that has a configuration
 * @param <C> the type of the configuration
 */
interface ConfigurableStep<C extends AbstractStepConfig> extends Step {

    /**
     * get the action
     * @return the action
     */
    GefAction getAction();

    /**
     * get the configuration of the step
     * @return the configuration of the step
     */
    C getConfig();

    /**
     * execute the step
     * @param ctx   the action context
     * @param inputParams the input parameters
     * @return the step result
     */
    StepResult execute(DefaultGefActionContext ctx, Map<String,Object> inputParams);

    /**
     * get the resource filter of the step
     * @return the resource filter
     */
    Filter<Resource> getFilter();

    /**
     * Undo the step
     * @param ctx     the action context
     * @param params  the input parameters
     * @return the output parameters of the undo
     */
    Map<String,Object> undo(DefaultGefActionContext ctx, Map<String,Object> inputParams);

    /**
     * Is the last try of the step reached
     * @param ctx  the action context
     * @return <tt>true</tt> if the last try is reached
     */
    boolean isLastTry(GefActionContext ctx);

    /**
     * Get the max. number of tries of the step
     * @return the max. number of tries
     */
    int getMaxTries();

    /**
     * get the delay between two tries
     * @return the delay
     */
    TimeIntervalHelper getDelay();
}
