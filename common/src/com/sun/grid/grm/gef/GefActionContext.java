/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.Resource;

/**
 * The action context stores the attributes of the invocation of an GE action
 */
public interface GefActionContext {

    /**
     * The SDM execution env
     * @return the SDM execution env
     */
    ExecutionEnv getExecutionEnv();

    /**
     * Get the resource that is processed
     * @return the resource
     */
    Resource getResource();

    /**
     * Get the number of invocations of a step
     * @param stepName the name of the step
     * @return the number of invocations of the step
     */
    int getInvocationCount(String stepName);

    /**
     * Has the undo started
     * @return <tt>true</tt> if the undo has started
     */
    boolean isInUndoPhase();
    
}
