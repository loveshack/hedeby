/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.gef.AbstractStepConfig;
import com.sun.grid.grm.config.gef.JavaStepConfig;
import com.sun.grid.grm.config.gef.ScriptingStepConfig;

/**
 * Factory class for GEF acion steps
 */
class StepFactory {

    private final static String BUNDLE = "com.sun.grid.grm.gef.gef";
    
    /**
     * Create new step for a GEF action
     * @param env      the execution env
     * @param action   the GEF action
     * @param config   the configuration of the step
     * @return  the step
     * @throws com.sun.grid.grm.gef.GefException if the configuration is invalid
     */
    static ConfigurableStep<? extends AbstractStepConfig> newStep(ExecutionEnv env, DefaultGefAction action, AbstractStepConfig config) throws GefException {
        if (config instanceof ScriptingStepConfig) {
            return new ScriptingStep(env, action, (ScriptingStepConfig)config);
        } else if (config instanceof JavaStepConfig) {
            return new ConfigurableJavaStep(env, action, (JavaStepConfig)config);
        } else if (config == null) {
            throw new IllegalArgumentException("config must not be null");
        } else {
            throw new GefException("StepFactory.unsupported", BUNDLE, config.getClass().getName());
        }
    }
}
