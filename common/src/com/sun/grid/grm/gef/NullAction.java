/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.gef.ActionConfig;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Handler;

/**
 * Dummy GEF action
 */
final class NullAction implements GefAction {

    private final static NullAction instance = new NullAction();

    /**
     * get the singleton instance of the null action
     * @return the singleton instance
     */
    static GefAction getInstance() {
        return instance;
    }
    
    /**
     * Create a new executor for the GEF action
     * @param env              the excution env
     * @param resource         the resource which will be modified by the action
     * @param protocolHandler  the handler for protocol writing
     * @param globalParams     global parameters for the action
     * @return the action executor
     */
    public final ActionExecutor newActionExecutor(ExecutionEnv env, Resource resource, Handler protocolHandler, Map<String, Object> globalParams) {
        return NULL_ACTION_EXECUTOR;
    }

    /**
     * get the name of the action
     * @return always the string Null
     */
    public final String getName() {
        return "Null";
    }

    public final String getExecAsUser() {
        return "nobody";
    }

    /**
     * get the configuration of the NullAction
     * @return always null
     */
    public final ActionConfig getConfig() {
        return null;
    }

    private final static ActionExecutor NULL_ACTION_EXECUTOR = new ActionExecutor() {

        public boolean hasNextStep() {
            return false;
        }

        public int getTotalStepCount() {
            return 0;
        }

        public Step getCurrentStep() {
            return null;
        }

        public Collection<ResourceChanged> executeNextStep() {
            return Collections.<ResourceChanged>emptyList();
        }

        public void close() {
            // empty implementation
        }
    };

}
