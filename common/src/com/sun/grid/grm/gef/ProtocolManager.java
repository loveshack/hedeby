/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.util.GrmFormatter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.ErrorManager;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Manager class for protocol files
 *
 * An instance of this class manages all protocol files for a protocol directory.
 * It synchronizes the file creation and ensures the uniqueness of protocol files.
 * It further deletes outdated protocol files.
 */
public final class ProtocolManager {

    private final static String BUNDLE = "com.sun.grid.grm.gef.gef";
    private final static Logger log = Logger.getLogger(ProtocolManager.class.getName(), BUNDLE);

    /**
     * Maximum number of protocol files per action
     */
    final static int MAX_PROTOCOL_FILES = 10;
    private final static DateFormat DF = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    private final static Formatter DEFAULT_FORMATTER = new GrmFormatter("gef", false,
            new GrmFormatter.Column[]{GrmFormatter.Column.TIME, GrmFormatter.Column.LEVEL, GrmFormatter.Column.MESSAGE});

    private static final Map<File, ProtocolManager> instances = new HashMap<File, ProtocolManager>();
    private final static Lock instancesLock = new ReentrantLock();

    /**
     * The extension for the protocol file name
     */
    private final static String FILENAME_EXTENSION = "log";
    

    private final File protocolDir;
    private final Map<File, ActionProtocolHandlerImpl> activeHandlers = new HashMap<File, ActionProtocolHandlerImpl>();
    private final Lock lock = new ReentrantLock();

    private ProtocolManager(File protocolDir) {
        this.protocolDir = protocolDir;
    }

    /**
     * Get the protocol manager for a protocol directory.
     *
     *
     * Please note that the protocol directory will not be created by the
     * ProtocolManager. The caller is responsible for creating this directory
     * 
     * @param protocolDir the protocol directory
     * @return the protocol manager
     */
    public static ProtocolManager getInstance(File protocolDir) {
        log.entering(ProtocolManager.class.getName(), "getInstance", protocolDir);
        ProtocolManager ret;
        instancesLock.lock();
        try {
            ret = instances.get(protocolDir);
            if (ret == null) {
                ret = new ProtocolManager(protocolDir);
                instances.put(protocolDir, ret);
            }
        } finally {
            instancesLock.unlock();
        }
        log.exiting(ProtocolManager.class.getName(), "getInstance", ret);
        return ret;
    }

    /**
     * Create a protocol handler for a GEF action.
     *
     * <b>Attention:</b> The caller must call the clode method for
     * handler if the handler is no longer used
     * @param action the action
     * @param resId the id of the resource which is modified by the GEF action (is used
     *            in the protocol file name)
     * @return the protocoal handler
     */
    public final Handler createHandler(GefAction action, ResourceId resId) {
        log.entering(ProtocolManager.class.getName(), "createHandler", action);
        Handler ret = new ActionProtocolHandlerImpl(action, resId);
        log.exiting(ProtocolManager.class.getName(), "createHandler", ret);
        return ret;
    }

    /**
     * get the protocol directory of this protocol manager
     * @return
     */
    public final File getProtocolDir() {
        return protocolDir;
    }

    /**
     * Clears the protocol handler
     *
     * This method closes all active log handers and deletes finally all protocol
     * files from the protocol directory
     *
     * This method is package private. It is only used for testing.
     */
    void clear() {
        log.entering(ProtocolManager.class.getName(), "clear");
        lock.lock();
        try {
            for (Handler handler : activeHandlers.values()) {
                handler.close();
            }
            activeHandlers.clear();
            File[] files = protocolDir.listFiles();
            if (files != null && files.length > 0) {
                for (File f : files) {
                    f.delete();
                }
            }
        } finally {
            lock.unlock();
        }
        log.exiting(ProtocolManager.class.getName(), "clear");
    }

    /**
     * this method must be called with the handlerLock held
     */
    private void openFile(ActionProtocolHandlerImpl handler) throws IOException {
        log.entering(ProtocolManager.class.getName(), "openFile", handler);
        lock.lock();
        try {
            String date = DF.format(new Date());
            String filenamePrefix = getFilenamePrefixForAction(handler.action);
            String filenamePrefixWithDate = String.format("%s-%s-%s", filenamePrefix, date, handler.resId);
            String filename = String.format("%s.%s", filenamePrefixWithDate, FILENAME_EXTENSION);
            File logFile = new File(protocolDir, filename);
            int i = 1;
            while (logFile.exists()) {
                filename = String.format("%s-%d.%s", filenamePrefixWithDate, i++, FILENAME_EXTENSION);
                logFile = new File(protocolDir, filename);
            }
            // We can modify the member variables directly, because the openFile method
            // is only called when the handlerLock is locked
            handler.fw = new FileWriter(logFile);
            handler.file = (logFile);
            
            activeHandlers.put(logFile, handler);
            deleteOutdatedProtocols(filenamePrefix);
        } finally {
            lock.unlock();
        }
        log.exiting(ProtocolManager.class.getName(), "openFile");
    }

    private void removeHandler(File file) {
        log.entering(ProtocolManager.class.getName(), "removeHandler", file);
        lock.lock();
        try {
            activeHandlers.remove(file);
        } finally {
            lock.unlock();
        }
        log.exiting(ProtocolManager.class.getName(), "removeHandler");
    }

    /**
     * !!Call this method only while holding the lock!!
     */
    private void deleteOutdatedProtocols(String filenamePrefix) {
        log.entering(ProtocolManager.class.getName(), "deleteOutdatedProtocols", filenamePrefix);
        // Keep only the last MAX_PROTOCOL_FILES files
        SortedSet<File> files = getAllProtocols(filenamePrefix);
        int fileCount = files.size();
        if (fileCount > MAX_PROTOCOL_FILES) {
            Iterator<File> iter = files.iterator();
            while (iter.hasNext() && fileCount > MAX_PROTOCOL_FILES) {
                File file = iter.next();
                if (!activeHandlers.containsKey(file)) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "ProtocolManager.delete", file.getName());
                    }
                    if (!file.delete()) {
                        log.log(Level.WARNING, "ProtocolManager.warn.delete", file.getName());
                    } else {
                        iter.remove();
                        fileCount--;
                    }
                }
            }
        }
        log.exiting(ProtocolManager.class.getName(), "deleteOutdatedProtocols");
    }


    private static String getFilenamePrefixForAction(GefAction action) {
        return action.getName().replace(' ', '_');
    }
    
    /**
     * Get a sorted set with all protocol files for one action.
     * The returned set is sorted by the timestamp of the protocol files in
     * ascending order, oldest file first
     * @param action  the action
     * @return the protocol files of the action
     */
    SortedSet<File> getAllProtocols(GefAction action) {
        return getAllProtocols(getFilenamePrefixForAction(action));
    }

    private SortedSet<File> getAllProtocols(String actionName) {
        log.entering(ProtocolManager.class.getName(), "getAllProtocols", actionName);
        File files[] = protocolDir.listFiles(new ActionProtocolFileFilter(actionName));

        TreeSet<File> ret = new TreeSet<File>(PROTOCOL_FILE_COMPARATOR);
        if (files == null) {
            log.log(Level.WARNING, "ProtocolManager.dirNotExist", protocolDir);
        } else {
            for (File file : files) {
                ret.add(file);
            }
        }
        log.exiting(ProtocolManager.class.getName(), "getAllProtocols", ret);
        return ret;
    }

    private final static Comparator<File> PROTOCOL_FILE_COMPARATOR = new Comparator<File>() {
        public int compare(File o1, File o2) {
            long t1 = o1.lastModified();
            long t2 = o2.lastModified();
            if (t1 == t2) {
                return o1.compareTo(o2);
            } else if (t1 < t2) {
                return -1;
            } else {
                return 1;
            }
        }
    };

    private static class ActionProtocolFileFilter implements FileFilter {

        private final String actionName;

        public ActionProtocolFileFilter(String actionName) {
            this.actionName = actionName;
        }

        public boolean accept(File pathname) {
            return pathname.isFile() &&
                    pathname.getName().startsWith(actionName) &&
                    pathname.getName().endsWith(ProtocolManager.FILENAME_EXTENSION);
        }
    }

    final class ActionProtocolHandlerImpl extends Handler {

        private final GefAction action;
        private final ResourceId resId;
        private final Lock handlerLock = new ReentrantLock();
        private File file;
        private FileWriter fw;

        public ActionProtocolHandlerImpl(GefAction action, ResourceId resId) {
            this.action = action;
            setFormatter(DEFAULT_FORMATTER);
            this.resId = resId;
        }

        public final File getFile() {
            handlerLock.lock();
            try {
                return file;
            } finally {
                handlerLock.unlock();
            }
        }

        @Override
        public final void publish(LogRecord record) {
            int errorCode = ErrorManager.OPEN_FAILURE;
            try {
                handlerLock.lock();
                try {
                    if (isLoggable(record)) {
                        if (fw == null) {
                            errorCode = ErrorManager.OPEN_FAILURE;
                            openFile(this);
                        }
                        errorCode = ErrorManager.WRITE_FAILURE;
                        fw.write(getFormatter().format(record));
                    }
                } finally {
                    handlerLock.unlock();
                }
            } catch (IOException ex) {
                getErrorManager().error(ex.getLocalizedMessage(), ex, errorCode);
            }
        }

        @Override
        public final void flush() {
            try {
                handlerLock.lock();
                try {
                    if (fw != null) {
                        fw.flush();
                    }
                } finally {
                    handlerLock.unlock();
                }
            } catch (IOException ex) {
                getErrorManager().error(ex.getLocalizedMessage(), ex, ErrorManager.FLUSH_FAILURE);
            }
        }

        @Override
        public final void close() {
            try {
                handlerLock.lock();
                try {
                    try {
                        if (fw != null) {
                            fw.close();
                        }
                    } finally {
                        fw = null;
                        if (file != null) {
                            removeHandler(file);
                        }
                    }
                } finally {
                    handlerLock.unlock();
                }
            } catch (IOException ex) {
                getErrorManager().error(ex.getLocalizedMessage(), ex, ErrorManager.CLOSE_FAILURE);
            }
        }
    }
}
