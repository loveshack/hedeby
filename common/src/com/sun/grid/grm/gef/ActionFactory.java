/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.gef.AbstractGefConfig;
import com.sun.grid.grm.config.gef.ActionConfig;

/**
 * Factory class for GEF actions
 */
public class ActionFactory {


    /**
     * Create  a new GEF action
     * @param env            the execution env of the SDM system
     * @param name           the name of the GEF action
     * @param config         the configuration of the GEF action
     * @param gefConfig      the global GEF configuration (defines the global parameters and the default execAs user)
     * @return the GefAction
     * @throws com.sun.grid.grm.gef.GefException if the configuraton is not valid
     */
    public static GefAction newAction(ExecutionEnv env, String name,  ActionConfig config, AbstractGefConfig gefConfig) throws GefException {
        return new DefaultGefAction(env, name, config, gefConfig);
    }

}
