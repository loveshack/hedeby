/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.gef.LogLevel;
import com.sun.grid.grm.config.gef.ProtocolOption;
import com.sun.grid.grm.config.gef.Script;
import com.sun.grid.grm.config.gef.ScriptingStepConfig;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.executor.NoExitValueException;
import com.sun.grid.grm.executor.RemoteFile;
import com.sun.grid.grm.executor.Result;
import com.sun.grid.grm.executor.impl.ShellCommand;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.TimeIntervalHelper;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default implementation for a scripting step.
 *
 * This class creates out of the configuration of a scripting step a ShellCommand
 * and executes it via an executor of the SDM system.
 */
final class ScriptingStep extends AbstractConfigurableStep<ScriptingStepConfig> {

    private final static String BUNDLE = "com.sun.grid.grm.gef.gef";
    private static final String INCLUDE_START = "@@@include:";
    private static final String INCLUDE_END = "@@@";
    private final static Logger log = Logger.getLogger(ScriptingStep.class.getName(), BUNDLE);
    private final static TimeIntervalHelper DEFAULT_TIMEOUT = new TimeIntervalHelper(60, TimeIntervalHelper.SECONDS);
    private final TimeIntervalHelper timeout;

    private final String execAsUser;
    
    /**
     * Create a new instanceof of a ScriptingStep
     * @param env     the execution env
     * @param action  the action
     * @param config  the configuration of the step
     * @throws com.sun.grid.grm.gef.GefException if the configuration is not valid
     */
    ScriptingStep(ExecutionEnv env, DefaultGefAction action, ScriptingStepConfig config) throws GefException {
        super(action, config);

        if (config.getScript().isSetTimeout()) {
            timeout = new TimeIntervalHelper(config.getScript().getTimeout());
        } else {
            timeout = DEFAULT_TIMEOUT;
        }

        checkFile(env, config.getScript(), "ScriptingStep.ex.scriptNotExists");
        if (config.isSetUndo()) {
            checkFile(env, config.getUndo(), "ScriptingStep.ex.undoNotExists");
        }

        if (config.isSetExecuteAs()) {
            this.execAsUser = config.getExecuteAs();
        } else {
            this.execAsUser = action.getExecAsUser();
        }
    }

    private void checkFile(ExecutionEnv env, Script s, String errorMsg) throws GefException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ScriptingStep.class.getName(), "checkFile", new Object[]{env, s, errorMsg});
        }
        File f = new File(s.getFile());
        if (!f.isAbsolute()) {
            f = new File(env.getDistDir(), s.getFile());
        }
        if (!f.exists()) {
            throw new GefException(errorMsg, BUNDLE, f);
        }
        log.exiting(ScriptingStep.class.getName(), "checkFile");
    }

    private ProtocolOption getProtocolOption(DefaultGefActionContext ctx, StepExitCode rc) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ScriptingStep.class.getName(), "getProtocolConfig", new Object[]{ctx, rc});
        }
        ProtocolOption ret;
        StepExitCode realRc = rc;
        // Waiting steps are treated as SUCCESS unless we are in undo phase or
        // the last try is reached
        if (rc == StepExitCode.REPEAT 
            && !(isLastTry(ctx) || ctx.isInUndoPhase())) {
                realRc = StepExitCode.SUCCESS;
        }

        ret = getAction().getProtocolOption(realRc);
        log.exiting(ScriptingStep.class.getName(), "getProtocolConfig", ret);
        return ret;
    }

    private Hostname getHostnameFromResource(Resource resource) throws InvalidResourceException {
        if (!resource.getType().getClass().equals(HostResourceType.class)) {
            throw new InvalidResourceException("ScriptingStep.ex.invalidResType", BUNDLE, resource, resource.getType().getName());
        }

        Hostname hostname = (Hostname) resource.getProperty(HostResourceType.HOSTNAME.getName());
        if (hostname == null) {
            throw new InvalidResourceException("ScriptingStep.ex.missingResProp", BUNDLE, resource, HostResourceType.HOSTNAME.getName());
        }
        return hostname;
    }

    private Hostname getExecuteOnHost(Resource resource) throws InvalidResourceException {
        switch (getConfig().getExecuteOn()) {
            case SERVICE_HOST:
                return Hostname.getLocalHost();
            case RESOURCE_HOST:
                return getHostnameFromResource(resource);
            default:
                throw new IllegalStateException("Unknown executeOn " + getConfig().getExecuteOn());
        }
    }

    /**
     * Execute the scripting step
     * @param ctx             the action context
     * @param scriptConfig    the script configuration
     * @param params          the input parameters
     * @param protocol        the protocol logger
     * @return the step result
     */
    private StepResult executeScript(DefaultGefActionContext ctx, Script scriptConfig, Map<String, Object> params, Logger protocol) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ScriptingStep.class.getName(), "executeScript", new Object[]{scriptConfig.getFile(), params});
        }

        Hostname hostname;
        try {
            hostname = getExecuteOnHost(ctx.getResource());
        } catch (InvalidResourceException ex) {
            protocol.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            StepResult ret = new StepResult(StepExitCode.FAILED_NO_UNDO);
            log.exiting(ScriptingStep.class.getName(), "executeScript", ret);
            return ret;
        }

        Executor exe = null;
        try {
            exe = ComponentService.getComponentByTypeAndHost(ctx.getExecutionEnv(), Executor.class, hostname);
        } catch (GrmException ex) {
            protocol.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            StepResult ret = new StepResult(StepExitCode.FAILED_NO_UNDO);
            log.exiting(ScriptingStep.class.getName(), "executeScript", ret);
            return ret;
        }

        if (exe == null) {
            protocol.log(Level.SEVERE, I18NManager.formatMessage("ScriptingStep.ex.noExecutor", BUNDLE, hostname));
            StepResult ret = new StepResult(StepExitCode.FAILED_NO_UNDO);
            log.exiting(ScriptingStep.class.getName(), "executeScript", ret);
            return ret;
        }

        ShellCommand cmd;
        try {
            cmd = createShellCommand(ctx, scriptConfig, protocol, params);
        } catch (IOException ex) {
            protocol.log(Level.SEVERE, I18NManager.formatMessage("ScriptingStep.ex.createScript", BUNDLE, ex.getLocalizedMessage()), ex);
            StepResult ret = new StepResult(StepExitCode.PERMANENT_ERROR);
            log.exiting(ScriptingStep.class.getName(), "executeScript", ret);
            return ret;
        }

        cmd.setUser(execAsUser);
        cmd.setMaxRuntime(timeout.getValueInMillis());
        
        Result res;
        // The isolation level is locked as late as possible
        lock(ctx);
        try {
            res = exe.execute(cmd);
        } catch (GrmException ex) {
            protocol.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
            StepResult ret = new StepResult(StepExitCode.PERMANENT_ERROR);
            log.exiting(ScriptingStep.class.getName(), "executeScript", ret);
            return ret;
        } finally {
            unlock(ctx);
        }

        try {
            int exitValue = res.getExitValue();

            StepExitCode rc;
            switch (exitValue) {
                case 0:
                    rc = StepExitCode.SUCCESS;
                    break;
                case 1:
                    rc = StepExitCode.FAILED_NEED_UNDO;
                    break;
                case 2:
                    rc = StepExitCode.FAILED_NO_UNDO;
                    break;
                case 3:
                    rc = StepExitCode.PERMANENT_ERROR;
                    break;
                case 4:
                    rc = StepExitCode.REPEAT;
                    break;
                default:
                    protocol.log(Level.WARNING, "ScriptingStep.unexpectedExitCode", exitValue);
                    rc = StepExitCode.PERMANENT_ERROR;
                    break;
            }

            ProtocolOption protOpt = getProtocolOption(ctx, rc);

            LogLevel logLevel = protOpt.getScriptLogLevel();
            if (!logLevel.equals(LogLevel.OFF)) {
                Level level = ProtocolLoggerCache.getLevel(logLevel);
                if (level != Level.OFF && protocol.isLoggable(level)) {
                    protocol.log(level, cmd.getCommandAsString());
                }
            }

            logLevel = protOpt.getStdoutLogLevel();
            if (!logLevel.equals(LogLevel.OFF)) {
                Level level = ProtocolLoggerCache.getLevel(logLevel);
                if (level != Level.OFF && protocol.isLoggable(level)) {
                    protocol.log(level, res.getStdoutAsString());
                }
            }

            logLevel = protOpt.getStderrLogLevel();
            if (!logLevel.equals(LogLevel.OFF)) {
                Level level = ProtocolLoggerCache.getLevel(logLevel);
                if (level != Level.OFF && protocol.isLoggable(level)) {
                    protocol.log(level, res.getStderrAsString());
                }
            }
            Map<String, Object> outputParams = parseOutputParams(protocol, res);
            StepResult ret = new StepResult(rc, outputParams);
            log.exiting(ScriptingStep.class.getName(), "executeScript", ret);
            return ret;

        } catch (NoExitValueException ex) {
            protocol.log(Level.SEVERE, I18NManager.formatMessage("ScriptingStep.ex.timeout", BUNDLE), ex);
            StepResult ret = new StepResult(StepExitCode.FAILED_NEED_UNDO);
            log.exiting(ScriptingStep.class.getName(), "executeScript", ret);
            return ret;
        }
    }

    /**
     * Execute the scripting step
     * @param ctx          the action context
     * @param inputParams  the input parameters
     * @param protocol     the protocol logger
     * @return  the step result
     */
    protected final StepResult execute(DefaultGefActionContext ctx, Map<String, Object> inputParams, Logger protocol) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ScriptingStep.class.getName(), "execute", new Object[]{ctx, inputParams, protocol});
        }
        StepResult ret = executeScript(ctx, getConfig().getScript(), inputParams, protocol);
        log.exiting(ScriptingStep.class.getName(), "execute", ret);
        return ret;
    }

    /**
     * Perform the undo of the scripting step
     * @param ctx          the action context
     * @param inputParams  the input parameters
     * @param protocol     the protocol logger
     * @return  the output parameters of the undo
     */
    protected final Map<String, Object> undo(DefaultGefActionContext ctx, Map<String, Object> inputParams, Logger protocol) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ScriptingStep.class.getName(), "undo", new Object[]{ctx, inputParams, protocol});
        }
        Map<String, Object> ret;
        if (!getConfig().isSetUndo()) {
            ret = Collections.<String, Object>emptyMap();
        } else {
            ret = executeScript(ctx, getConfig().getUndo(), inputParams, protocol).getOutputParams();
        }
        log.exiting(ScriptingStep.class.getName(), "undo", ret);
        return ret;
    }

    private Map<String, Object> parseOutputParams(Logger protocol, Result res) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ScriptingStep.class.getName(), "parseOutputParams", new Object[]{protocol, res});
        }

        Map<String, Object> ret = new HashMap<String, Object>(res.getOutputMessages().size());

        boolean outputHeaderFound = false;
        for (String msg : res.getOutputMessages()) {
            if (outputHeaderFound) {
                String tokens[] = msg.split("=", 2);
                if (tokens != null && tokens.length == 2) {
                    String key = tokens[0].trim();
                    if (key.length() > 0) {
                        // Spaces are allowed, do not trim the value
                        if (tokens[1].length() == 0) {
                            ret.put(key, null);
                        } else {
                            ret.put(key, tokens[1]);
                        }
                    }
                } else {
                    protocol.log(Level.SEVERE, I18NManager.formatMessage("ScriptingStep.ex.output", BUNDLE, msg));
                }
            } else if (msg.contains("OUTPUT_PARAMETERS")) {
                outputHeaderFound = true;
            }
        }
        log.exiting(ScriptingStep.class.getName(), "parseOutputParams", ret);
        return ret;
    }

    private ShellCommand createShellCommand(DefaultGefActionContext ctx, Script scriptConfig, Logger protocol, Map<String, Object> inputParams) throws IOException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ScriptingStep.class.getName(), "createShellCommand", new Object[]{ctx, scriptConfig, protocol, inputParams});
        }
        File scriptFile = new File(scriptConfig.getFile());

        // Relative pathnames are relative to SDM dist directory
        if (!scriptFile.isAbsolute()) {
            scriptFile = new File(ctx.getExecutionEnv().getDistDir(), scriptConfig.getFile());
        }

        ShellCommand ret = createShellCommandFromFile(scriptFile, protocol, inputParams);

        ret.setUser(execAsUser);
        ret.setMaxRuntime(timeout.getValueInMillis());

        log.exiting(ScriptingStep.class.getName(), "createShellCommand", ret);
        return ret;
    }

    static ShellCommand createShellCommandFromFile(File scriptFile, Logger protocol, Map<String, Object> inputParams) throws IOException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ScriptingStep.class.getName(), "createShellCommandFromFile", new Object[]{scriptFile, protocol, inputParams});
        }


        Map<String, String> patterns = new HashMap<String, String>(inputParams.size());
        for (Map.Entry<String, Object> entry : inputParams.entrySet()) {
            patterns.put(String.format("@@@%s@@@", entry.getKey()), entry.getValue() == null ? "" : entry.getValue().toString());
        }
        StringWriter sw = new StringWriter();
        FileUtil.readAndReplace(scriptFile, sw, patterns);
        sw.close();
        String scriptContent = sw.getBuffer().toString();

        RemoteFile script = new RemoteFile(scriptFile.getName());

        ShellCommand ret = new ShellCommand(script);

        BufferedReader br = new BufferedReader(new StringReader(scriptContent));
        String line = null;
        PrintWriter pw = script.createPrintWriter();
        int lineNr = 0;
        while ((line = br.readLine()) != null) {
            lineNr++;
            int index = line.indexOf(INCLUDE_START);
            int endIndex = -1;
            if (index >= 0) {
                index += INCLUDE_START.length();
                endIndex = line.indexOf(INCLUDE_END, index);
                if (endIndex < 0) {
                    protocol.log(Level.WARNING, I18NManager.formatMessage("ScriptingStep.ex.endOfIncl", BUNDLE, scriptFile, lineNr));
                } else {
                    String filename = line.substring(index, endIndex);
                    File file = new File(scriptFile.getParentFile(), filename);
                    try {
                        RemoteFile rf = new RemoteFile(file, file.getName());
                        rf.load();
                        ret.addFile(rf);
                    } catch (IOException ex) {
                        protocol.log(Level.WARNING, I18NManager.formatMessage("ScriptingStep.ex.incl", BUNDLE, scriptFile, lineNr, file, ex.getLocalizedMessage()), ex);
                    }
                }
            }
            pw.println(line);
        }
        pw.close();

        log.exiting(ScriptingStep.class.getName(), "createShellCommandFromFile", ret);
        return ret;
    }
}
