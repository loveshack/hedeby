/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.resource.ResourceChanged;
import java.util.Collection;

/**
 *  Executor interface for an GEF action
 */
public interface ActionExecutor {

    /**
     * Has the executor an remaining step
     * @return <tt>true</tt> if the executor has remaining step
     */
    boolean hasNextStep();

    /**
     * Get total number of steps
     * @return the total number of steps
     */
    int getTotalStepCount();

    /**
     * Get the current step
     * @return the current step or <tt>null</tt> if all steps have been executed
     */
    Step getCurrentStep();

    /**
     * Execute the next step
     * @return the changes on the resource
     * @throws com.sun.grid.grm.gef.GefStepException of the step failed
     */
    Collection<ResourceChanged> executeNextStep() throws GefStepException;

    /**
     * close the executor. After this call the executor can no longer be used.
     */
    void close();
}
