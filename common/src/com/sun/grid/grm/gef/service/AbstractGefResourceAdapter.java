/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef.service;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.FactoryWithParam;
import com.sun.grid.grm.config.common.AbstractServiceConfig;
import com.sun.grid.grm.gef.ActionExecutor;
import com.sun.grid.grm.gef.GefAction;
import com.sun.grid.grm.gef.GefStepException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.adapter.RAOperation;
import com.sun.grid.grm.resource.adapter.RAOperationException;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.resource.adapter.ResourceTransition;
import com.sun.grid.grm.resource.adapter.impl.AbstractResourceAdapter;
import com.sun.grid.grm.resource.adapter.impl.DefaultResourceTransition;
import com.sun.grid.grm.resource.adapter.impl.ListOPR;
import com.sun.grid.grm.resource.adapter.impl.NullOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceChangedOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceErrorOPR;
import com.sun.grid.grm.resource.impl.ResourceAnnotationUpdateOperation;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.util.I18NManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract base class for GEF based resource adapter.
 *
 * This class provides the functionality to execute GEF actions on a resource
 * 
 * @param <K> the type of the resource key
 */
public abstract class AbstractGefResourceAdapter<K> extends AbstractResourceAdapter<K> {

    private final static String BUNDLE = "com.sun.grid.grm.gef.service.gef_service";
    private final static Logger log = Logger.getLogger(AbstractGefResourceAdapter.class.getName(), BUNDLE);

    private final AbstractGefServiceAdapterImpl<? extends ExecutorService, ? extends AbstractServiceConfig, K, ? extends ResourceAdapter<K>> gefService;

    /**
     * Create a new instance of AbstractGefResourceAdapter
     * @param gefService the GEF based service
     * @param resource the resource managed by this adapter
     */
    public AbstractGefResourceAdapter(AbstractGefServiceAdapterImpl<? extends ExecutorService, ? extends AbstractServiceConfig, K, ? extends ResourceAdapter<K>> gefService, Resource resource) {
        super(resource);
        this.gefService = gefService;
    }

    /**
     * Submit a GEF action into the resource action executor of the GEF based
     * service that updates an existing resource
     * @param action             the GEF action
     * @param finalOPRFactory    the factory for the final OPR. The OPR created
     *                           by this factory is commited by the service once the action succeeded
     * @return a ResourceChangedOPR that contains a ResourceAnnotationChanged object
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not active
     */
    protected final RAOperationResult submitGefUpdateAction(GefAction action, FactoryWithParam<RAOperationResult, Resource> finalOPRFactory) throws ServiceNotActiveException {
        if (log.isLoggable(Level.FINE)) {
            log.entering(AbstractGefResourceAdapter.class.getName(), "submitGefUpdateAction",
                    new Object [] { action, finalOPRFactory });
        }
        GefActionIterable ops = new GefActionIterable(action, finalOPRFactory);

        gefService.submitGefUpdateOperations(getKey(), action.getName(), ops);

        // set start annotation for action processing
        ResourceAnnotationUpdateOperation op = new ResourceAnnotationUpdateOperation(action.getName());
        
        ResourceChanged change;
        try {
            change = resource.modify(op);
        } catch (InvalidResourcePropertiesException ex) {
            throw new IllegalStateException("Changing the annotation must be always possible", ex);
        }
        RAOperationResult ret = new ResourceChangedOPR(resource.clone(), Collections.singleton(change), action.getName());

        log.exiting(AbstractGefServiceAdapterImpl.class.getName(), "submitGefUpdateAction", ret);
        return ret;
    }

    private class GefActionIterable implements Iterable<RAOperation> {

        private final GefAction action;
        private final FactoryWithParam<RAOperationResult, Resource> finalOPRFactory;

        public GefActionIterable(GefAction action, FactoryWithParam<RAOperationResult, Resource> finalOPRFactory) {
            this.action = action;
            this.finalOPRFactory = finalOPRFactory;
        }
        public Iterator<RAOperation> iterator() {
            ActionExecutor exe;
            try {
                exe = gefService.createActionExecutor(action, resource);
            } catch (GrmException ex) {
                return Collections.<RAOperation>singleton(new ErrorRA(ex)).iterator();
            }
            return new GefRAOperation(exe, action, finalOPRFactory);
        }
    }

    private class ErrorRA implements RAOperation {

        private final GrmException ex;

        private ErrorRA(GrmException ex) {
            this.ex = ex;
        }

        public ResourceTransition execute(ResourceAdapter resourceAdapter) throws RAOperationException {
            resourceStateLock.lock();
            try {
                resource.setState(Resource.State.ERROR);
                resource.setAnnotation(ex.getLocalizedMessage());
                ResourceErrorOPR opr = new ResourceErrorOPR(resource, resource.getAnnotation());
                return new DefaultResourceTransition(transitionId.getAndIncrement(), opr);
            } finally {
                resourceStateLock.unlock();
            }
        }

        public String getOperationName() {
            return I18NManager.formatMessage("agra.errorOp", BUNDLE);
        }
    }

    private class AnnotationChangeRA implements RAOperation {

        private final String annotation;
        
        public AnnotationChangeRA(String annotation) {
            this.annotation = annotation;
        }
        
        public ResourceTransition execute(ResourceAdapter resourceAdapter) {
            ResourceAnnotationUpdateOperation op = new ResourceAnnotationUpdateOperation(annotation);
            try {
                ResourceChanged change = resourceAdapter.modifyResource(op);
                ResourceChangedOPR opr = new ResourceChangedOPR(resourceAdapter.getResource(), Collections.<ResourceChanged>singletonList(change), annotation);
                return new DefaultResourceTransition(transitionId.getAndIncrement(), opr);
            } catch (InvalidResourcePropertiesException ex) {
                throw new IllegalStateException("Change the annotation must be possible", ex);
            }
        }

        public String getOperationName() {
            return I18NManager.formatMessage("agra.annoChangeOp", BUNDLE);
        }
        
    }

    private class GefRAOperation implements RAOperation, Iterator<RAOperation> {

        private final ActionExecutor executor;
        private final GefAction action;
        private final FactoryWithParam<RAOperationResult, Resource> finalOPRFactory;

        /**
         * used for inserting AnnotationChanged operations before each regular step
         */
        private AnnotationChangeRA nextOp;


        private GefRAOperation(ActionExecutor executor, GefAction action,
                FactoryWithParam<RAOperationResult, Resource> finalOPRFactory) {
            this.executor = executor;
            this.action = action;
            this.finalOPRFactory = finalOPRFactory;
            if (executor.hasNextStep()) {
                nextOp = new AnnotationChangeRA(executor.getCurrentStep().getName());
            }
        }

        public boolean hasNext() {
            return executor.hasNextStep();
        }

        public RAOperation next() {
            return this;
        }

        public void remove() {
            throw new UnsupportedOperationException("GefActionIterator does not support the remove method.");
        }


        /**
         * Returns the name of the current step or "unknown"
         * if there is no current step
         * @return the name of the operation
         */
        public String getOperationName() {
            if (executor.getCurrentStep() == null) {
                return I18NManager.formatMessage("agra.unknownAction", BUNDLE);
            } else {
                return executor.getCurrentStep().getName();
            }
        }

        /**
         * Executes the next step of the GEF action executor on the resource.
         * Decides whether this is an AnnotationChangeRA or a regular step.
         *
         * @param resourceAdapter the resource adapter
         * @return the resource transition to the next step
         */
        public final ResourceTransition execute(ResourceAdapter resourceAdapter) {
            ResourceTransition ret;
            if (nextOp != null) {
                ret = nextOp.execute(resourceAdapter);
                nextOp = null;
            } else {
                ret = executeGefStep(resourceAdapter);
                if (executor.hasNextStep()) {
                    nextOp = new AnnotationChangeRA(executor.getCurrentStep().getName());
                }
            }
            return ret;
        }

        /**
         * Executes the next step of the GEF action executor on the resource
         * @param resourceAdapter the resource adapter
         * @return the resource transition to the next step
         */
        private ResourceTransition executeGefStep(ResourceAdapter resourceAdapter) {
            log.entering(GefRAOperation.class.getName(), "execute", resourceAdapter);
            ResourceTransition ret;
            RAOperationResult res;
            if (executor.hasNextStep()) {
                try {
                    Collection<ResourceChanged> changes = executor.executeNextStep();
                    res = new ResourceChangedOPR(resource, changes, resource.getAnnotation());
                    if (!executor.hasNextStep()) {
                        resourceStateLock.lock();
                        try {
                            ListOPR opr = new ListOPR();
                            opr.add(res);
                            opr.add(finalOPRFactory.newInstance(resource));
                            res = opr;
                            executor.close();
                        } finally {
                            resourceStateLock.unlock();
                        }
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "agra.end", new Object[]{action.getName(), res});
                        }
                    }
                } catch (GefStepException ex) {
                    resourceStateLock.lock();
                    try {
                        // The GefActionExecutor has already set the correct error message
                        // containing the name of the protocol in the annotation of the resource
                        // => reuse it and set resource into ERROR state
                        resource.setState(Resource.State.ERROR);
                        Resource resourceClone = resource.clone();
                        res = new ResourceErrorOPR(resourceClone, resource.getAnnotation());
                        // GEF step can change the resource even it failed
                        // => add a ResourceChangedOPR
                        ResourceChangedOPR changedOPR = new ResourceChangedOPR(resourceClone, ex.getChanges(), resource.getAnnotation());
                        if (changedOPR.hasModifiedResource()) {
                            ListOPR listOPR = new ListOPR();
                            listOPR.add(changedOPR);
                            listOPR.add(res);
                            res = listOPR;
                        }
                    } finally {
                        resourceStateLock.unlock();
                    }
                    executor.close();
                }
            } else {
                res = NullOPR.getInstance();
            }
            ret = new DefaultResourceTransition(transitionId.getAndIncrement(), res);
            log.entering(GefRAOperation.class.getName(), "execute", ret);
            return ret;
        }

    }
}
