/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef.service;

import com.sun.grid.grm.service.impl.*;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.AbstractServiceConfig;
import com.sun.grid.grm.gef.GefAction;
import com.sun.grid.grm.gef.ActionExecutor;
import com.sun.grid.grm.gef.ProtocolManager;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.adapter.RAOperation;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.service.ServiceAdapter;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.logging.ErrorManager;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract base class for GEF base service adapters.
 * 
 * @param <E> the type of the executor
 * @param <C> the type of the configuration
 * @param <K> the type of the resource key
 * @param <RA> the type of the resource adapter
 *
 */
public abstract class AbstractGefServiceAdapterImpl<E extends ExecutorService, C extends AbstractServiceConfig, K, RA extends ResourceAdapter<K>>
        extends AbstractServiceAdapter<E, C, K, RA> {

    private final static String BUNDLE = "com.sun.grid.grm.gef.service.gef_service";
    private final static Logger log = Logger.getLogger(AbstractGefServiceAdapterImpl.class.getName(), BUNDLE);
    private final File gefTmpDir;
    private final File gefSpoolDir;
    private final ProtocolManager protocolManager;

    /**
     * Creates a new instance of AbstractServiceAdapter.
     *
     * @param component the owning component
     */
    protected AbstractGefServiceAdapterImpl(AbstractServiceContainer<? extends ServiceAdapter<E, C>, E, C> component) {
        super(component);
        gefTmpDir = new File(PathUtil.getTmpDirForComponent(getExecutionEnv(), getName()), "gef");

        File spoolDir = PathUtil.getSpoolDirForComponent(getExecutionEnv(), getName());
        gefSpoolDir = new File(spoolDir, "gef");
        protocolManager = ProtocolManager.getInstance(PathUtil.getLogPathForComponent(getExecutionEnv(), getName()));
    }

    /**
     * Reload the service
     * @param config  the configuration
     * @param forced  forced reload
     * @return always null
     * @throws com.sun.grid.grm.GrmException
     */
    @Override
    protected final Map<K, Object> doReloadService(C config, boolean forced) throws GrmException {
        log.entering(AbstractGefServiceAdapterImpl.class.getName(), "doReloadService");
        reconfigure(config);
        log.exiting(AbstractGefServiceAdapterImpl.class.getName(), "doReloadService");
        return null;
    }

    /**
     * Start the service
     * @param config the configuration
     * @return always null
     * @throws com.sun.grid.grm.GrmException
     */
    @Override
    protected final Map<K, Object> doStartService(C config) throws GrmException {
        log.entering(AbstractGefServiceAdapterImpl.class.getName(), "doStartService", config);
        reconfigure(config);
        createDirectories();
        log.exiting(AbstractGefServiceAdapterImpl.class.getName(), "doStartService");
        return null; //implies that the service does not assume anything about its resources
    }

    /**
     * Reconfigure the service
     * @param config the configuration
     * @throws com.sun.grid.grm.GrmException if the configuration is not valid
     */
    protected abstract void reconfigure(C config) throws GrmException;

    private void createDirectories() throws GrmException {

        if (!gefTmpDir.exists()) {
            if (!gefTmpDir.mkdirs()) {
                throw new GrmException("ags.ex.mkdirTmp", BUNDLE, gefTmpDir);
            }
        }

        if (!gefSpoolDir.exists()) {
            if (!gefSpoolDir.mkdirs()) {
                throw new GrmException("ags.ex.mkdirSpool", BUNDLE, gefSpoolDir);
            }
        }

        if (!protocolManager.getProtocolDir().exists()) {
            if (!protocolManager.getProtocolDir().mkdirs()) {
                throw new GrmException("ags.ex.mkdirProt", BUNDLE, protocolManager.getProtocolDir());
            }
        }
    }

    /**
     * "Friend" method for the AbstractGefResourceAdapter. Submits the resource operations
     * for a GEF action into the resource action executor.
     *
     * @param resourceKey  the resource key
     * @param name         the name of the action
     * @param ops          the resource operations
     * @throws com.sun.grid.grm.service.ServiceNotActiveException
     */
    void submitGefUpdateOperations(K resourceKey, String name, Iterable<RAOperation> operations) throws ServiceNotActiveException {
        if (log.isLoggable(Level.FINE)) {
            log.entering(AbstractGefServiceAdapterImpl.class.getName(), "submitGefAction",
                    new Object [] { resourceKey, name, operations });
        }
        submitUpdateResourceOperations(resourceKey, name, operations);
        log.exiting(AbstractGefServiceAdapterImpl.class.getName(), "submitGefUpdateOperations");
    }

    ActionExecutor createActionExecutor(GefAction action, Resource resource) throws GrmException {
        return action.newActionExecutor(getExecutionEnv(),
                resource, createProtocolHandler(action, resource), createGlobalParams());
    }

    private Handler createProtocolHandler(GefAction action, Resource resource) throws GrmException {
        Handler ret = protocolManager.createHandler(action, resource.getId());
        ret.setErrorManager(myErrorManager);
        return ret;
    }

    private Map<String, Object> createGlobalParams() {
        Map<String, Object> ret = new HashMap<String, Object>(5);
        ret.put("system_name", getExecutionEnv().getSystemName());
        ret.put("system_spool_dir", getExecutionEnv().getLocalSpoolDir());
        ret.put("cs_url", getExecutionEnv().getCSInfo());
        ret.put("spool_dir", gefSpoolDir);
        ret.put("tmp_dir", gefTmpDir);
        return ret;
    }

    /**
     * This error manager is used to pipe the errors of the GEF protocol into the
     * logging system of the JVM.
     */
    private final ErrorManager myErrorManager = new ErrorManager() {
        @Override
        public void error(String msg, Exception ex, int code) {
            log.log(Level.SEVERE, I18NManager.formatMessage("ags.protError", BUNDLE, msg), ex);
        }
    };
}
