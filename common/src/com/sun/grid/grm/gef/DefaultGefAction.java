/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.gef.AbstractGefConfig;
import com.sun.grid.grm.config.gef.AbstractStepConfig;
import com.sun.grid.grm.config.gef.ActionConfig;
import com.sun.grid.grm.config.gef.Parameter;
import com.sun.grid.grm.config.gef.ProtocolConfig;
import com.sun.grid.grm.config.gef.ProtocolOption;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.resource.impl.ResourceAnnotationUpdateOperation;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default implemenation of a GEF action
 */
final class DefaultGefAction implements GefAction {

    private final static String BUNDLE = "com.sun.grid.grm.gef.gef";
    private final static Logger log = Logger.getLogger(DefaultGefAction.class.getName(), BUNDLE);
    
    private final ActionConfig config;
    private final Map<String, Lock> locks;
    private final Map<String, Object> globalConfigParams;
    private final String execAsUser;
    private final List<ConfigurableStep<? extends AbstractStepConfig>> steps;
    private final String name;

    private final ProtocolConfig protConfig;
    
    /**
     * create an instance of a default action
     * @param env      the execution env
     * @param name     the name of the action
     * @param gefConfig    the global GEF configuration (defines the global parameters and the default execAs user)
     * @param globalConfigParams  the global parameters from the configuration
     * @throws com.sun.grid.grm.gef.GefException if the configuration is invalid
     */
    DefaultGefAction(ExecutionEnv env, String name, ActionConfig config, AbstractGefConfig gefConfig) throws GefException {
        this.name = name;
        this.config = config;

        if(config.isSetExecuteAs()) {
            this.execAsUser = config.getExecuteAs();
        } else {
            this.execAsUser = gefConfig.getExecuteAs();
        }

        Map<String,Object> tmpGlobalParams = new HashMap<String,Object>(gefConfig.getParam().size());

        for(Parameter param: gefConfig.getParam()) {
            tmpGlobalParams.put(param.getName(), param.getValue());
        }
        this.globalConfigParams = Collections.unmodifiableMap(tmpGlobalParams);

        if (config.isSetProtocol()) {
            protConfig = config.getProtocol();
        } else {
            protConfig = gefConfig.getProtocol();
        }

        steps = new ArrayList<ConfigurableStep<? extends AbstractStepConfig>>(config.getStep().size());
        Map<String, Lock> tmpLocks = new HashMap<String, Lock>(config.getStep().size());

        for (AbstractStepConfig stepConfig : config.getStep()) {
            steps.add(StepFactory.newStep(env, this, stepConfig));
            if (stepConfig.isSetIsolationLevel()) {
                tmpLocks.put(stepConfig.getIsolationLevel(), new ReentrantLock());
            }
        }

        // The ActionExecutors will have a reference to the lock map. Make
        // the map unmodifiable, it prohibits the modification of the locks
        this.locks = Collections.unmodifiableMap(tmpLocks);
    }

    /**
     * Get the name of the user that executes scripts
     * @return the name of the user that executes scripts
     */
    public String getExecAsUser() {
        return execAsUser;
    }

    /**
     * Get the protocol options for a step exit code
     * @param rc the step exit code
     * @return the protocol options
     */
    public ProtocolOption getProtocolOption(StepExitCode rc) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(DefaultGefAction.class.getName(), "getProtocolOption", rc);
        }
        ProtocolOption ret;
        switch (rc) {
            case SUCCESS: ret = protConfig.getOnSuccess(); break;
            default: ret = protConfig.getOnError();
        }
        log.exiting(DefaultGefAction.class.getName(), "getProtocolOption", ret);
        return ret;
    }


    /**
     * get the name of the action
     * @return the name of the action
     */
    public final String getName() {
        return name;
    }

    /**
     * get the configuration of the action
     * @return the configuration of the action
     */
    public ActionConfig getConfig() {
        return config;
    }

    /**
     * Create a new ActionExecutor
     * @param env        the execution env
     * @param resource   the resource that should be processed
     * @param protocolHandler  the protocol handler
     * @param globalParamsForExecutor the global parameters for the action executor
     * @return the action executor
     */
    public ActionExecutor newActionExecutor(ExecutionEnv env, Resource resource, Handler protocolHandler, Map<String, Object> globalParamsForExecutor) {
        HashMap<String, Object> params = new HashMap<String, Object>(Math.max(this.globalConfigParams.size(), globalParamsForExecutor.size()));
        params.putAll(globalConfigParams);
        params.putAll(globalParamsForExecutor);
        return new InnerActionExecutor(new DefaultGefActionContext(env, resource, protocolHandler, locks, params));
    }

    private class InnerActionExecutor implements ActionExecutor {

        private final Stack<ConfigurableStep<? extends AbstractStepConfig>> executedStepStack = new Stack<ConfigurableStep<? extends AbstractStepConfig>>();
        private final Queue<ConfigurableStep<? extends AbstractStepConfig>> remaningSteps;
        private final DefaultGefActionContext ctx;

        private InnerActionExecutor(DefaultGefActionContext ctx) {
            this.ctx = ctx;
            this.remaningSteps = new LinkedList<ConfigurableStep<? extends AbstractStepConfig>>(DefaultGefAction.this.steps);

        }

        public boolean hasNextStep() {
            return remaningSteps.size() > 0;
        }

        public ConfigurableStep<? extends AbstractStepConfig> getCurrentStep() {
            return remaningSteps.peek();
        }

        public void close() {
            ctx.getProtocolHandler().close();
        }

        public int getTotalStepCount() {
            return DefaultGefAction.this.steps.size();
        }

        public final Collection<ResourceChanged> executeNextStep() throws GefStepException {
            log.entering(InnerActionExecutor.class.getName(), "executeNextStep");

            Collection<ResourceChanged> ret = Collections.<ResourceChanged>emptyList();
            ConfigurableStep<? extends AbstractStepConfig> step = remaningSteps.poll();
            if (step == null) {
                log.exiting(InnerActionExecutor.class.getName(), "executeNextStep", ret);
                return ret;
            }

            Logger protocol = ProtocolLoggerCache.getInstance().get(ctx, config.getLogLevel());
            try {
                ResourceVariableResolver resolver = new ResourceVariableResolver();
                resolver.setResource(ctx.getResource());

                if (!step.getFilter().matches(resolver)) {
                    protocol.log(Level.FINE, I18NManager.formatMessage("DefaultAction.filter.skip", BUNDLE, step.getName(), ctx.getResource()));
                    log.exiting(InnerActionExecutor.class.getName(), "executeNextStep", ret);
                    return ret;
                }
                ret = executeAndChangeResource(step, protocol);
                log.exiting(InnerActionExecutor.class.getName(), "executeNextStep", ret);
                return ret;
            } catch (GefStepException ex) {
                Collection<ResourceChangeOperation> ops = undo(protocol);
                File protocolFile = ctx.getProtocolFile();
                if (protocolFile == null) {
                    ops.add(new ResourceAnnotationUpdateOperation(I18NManager.formatMessage("DefaultAction.stepFailed", BUNDLE, step.getName())));
                } else {
                    ops.add(new ResourceAnnotationUpdateOperation(I18NManager.formatMessage("DefaultAction.stepFailed.prot", BUNDLE, step.getName(), protocolFile.getName())));
                }
                try {
                    Collection<ResourceChanged> undoChanges = changeResource(ctx.getResource(), ops, protocol);
                    ex.getChanges().addAll(undoChanges);
                } catch (InvalidResourcePropertiesException ex1) {
                    protocol.log(Level.SEVERE, I18NManager.formatMessage("DefaultAction.undoResFailed", BUNDLE, ctx.getResource(), ex1.getLocalizedMessage()), ex1);
                }
                throw ex;
            } finally {
                ProtocolLoggerCache.getInstance().release(protocol);
            }
        }

        private Collection<ResourceChanged> executeAndChangeResource(ConfigurableStep<? extends AbstractStepConfig> step, Logger protocol) throws GefStepException {
            if (log.isLoggable(Level.FINER)) {
                log.entering(InnerActionExecutor.class.getName(), "executeAndChangeResource", new Object [] { step, protocol });
            }
            
            try {
                Collection<ResourceChangeOperation> ops = new LinkedList<ResourceChangeOperation>();
                StepResult result = runNextStep(step, protocol, ops);
                Collection<ResourceChanged> ret = changeResource(ctx.getResource(), ops, protocol);
                switch (result.getExitCode()) {
                    case SUCCESS:
                        executedStepStack.push(step);
                        log.exiting(InnerActionExecutor.class.getName(), "executeAndChangeResource", ops);
                        return ret;
                    case FAILED_NO_UNDO:
                        throw new GefStepException(step, result.getExitCode(), ret);
                    default:
                        executedStepStack.push(step);
                        throw new GefStepException(step, result.getExitCode(), ret);
                }
            } catch (InvalidResourcePropertiesException ex) {
                protocol.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
                throw new GefStepException(step, StepExitCode.FAILED_NEED_UNDO);
            }
        }

        private Collection<ResourceChanged> changeResource(Resource resource, Collection<ResourceChangeOperation> ops, Logger protocol) throws InvalidResourcePropertiesException {
                Collection<ResourceChanged> ret = resource.modify(ops);
                if (protocol.isLoggable(Level.FINE)) {
                    protocol.log(Level.FINE, I18NManager.formatMessage("DefaultAction.resChanges", BUNDLE, ret));
                }
                return ret;
        }

        private StepResult runNextStep(ConfigurableStep<? extends AbstractStepConfig> step, Logger protocol, Collection<ResourceChangeOperation> ops) throws GefStepException {
            if (log.isLoggable(Level.FINER)) {
                log.entering(InnerActionExecutor.class.getName(), "runNextStep", new Object [] { step, protocol });
            }

            int invocationCount;
            StepResult res;
            do {
                invocationCount = ctx.incrInvocationCount(step.getName());
                if (step.getMaxTries() > 1) {
                    protocol.log(Level.INFO, I18NManager.formatMessage("DefaultAction.exeStepWithTries", BUNDLE, step.getName(), invocationCount , step.getMaxTries()));
                } else {
                    protocol.log(Level.INFO, I18NManager.formatMessage("DefaultAction.exeStep", BUNDLE, step.getName()));
                }

                Map<String, Object> inputParams = createStepParams(step, ctx);
                res = step.execute(ctx, inputParams);
                Collection<ResourceChangeOperation> op = ctx.postProcessing(res.getOutputParams());
                ops.addAll(op);

                if (res.getExitCode() == StepExitCode.REPEAT) {
                    // Waiting step
                    if (invocationCount < step.getMaxTries()) {
                        protocol.log(Level.INFO, I18NManager.formatMessage("DefaultAction.repeatStep", BUNDLE, step.getName(), step.getDelay()));
                        try {
                            Thread.sleep(step.getDelay().getValueInMillis());
                        } catch (InterruptedException ex1) {
                            protocol.log(Level.SEVERE, I18NManager.formatMessage("DefaultAction.stepInterrupted", BUNDLE, step.getName()), ex1);
                            res = new StepResult(StepExitCode.FAILED_NO_UNDO, res.getOutputParams());
                        }
                    } else {
                        // Last try
                        break;
                    }
                }
            } while(res.getExitCode() == StepExitCode.REPEAT);
            log.exiting(InnerActionExecutor.class.getName(), "runNextStep", res);
            return res;
        }

        /**
         * This method calls the undo method of all steps stored in the executedStep stack.
         *
         * @param protocol the protocol logger
         * @return the resource change operations produced by the undo method of the steps
         */
        private List<ResourceChangeOperation> undo(Logger protocol) {
            ctx.undoPhaseStarted();
            List<ResourceChangeOperation> ret = new LinkedList<ResourceChangeOperation>();
            while (!executedStepStack.empty()) {
                ConfigurableStep<? extends AbstractStepConfig> undoStep = executedStepStack.pop();
                protocol.log(Level.INFO, I18NManager.formatMessage("DefaultAction.undoStep", BUNDLE, undoStep.getName()));
                Map<String, Object> inputParams = createStepParams(undoStep, ctx);
                Map<String, Object> lastOutputParams = undoStep.undo(ctx, inputParams);
                Collection<ResourceChangeOperation> undoOps = ctx.postProcessing(lastOutputParams);
                ret.addAll(undoOps);
                ctx.getProtocolHandler().flush();
            }
            // Cleanup the remaining steps
            remaningSteps.clear();
            return ret;
        }
    }

    private Map<String, Object> createStepParams(ConfigurableStep step, DefaultGefActionContext ctx) {

        Map<String, Object> ret = new HashMap<String, Object>();

        ret.putAll(ctx.getGlobalParams());

        for (Parameter p : this.config.getParam()) {
            ret.put(p.getName(), p.getValue());
        }

        for (Parameter p : step.getConfig().getParam()) {
            ret.put(p.getName(), p.getValue());
        }

        for (Map.Entry<String, Object> entry : ctx.getResource().getProperties().entrySet()) {
            ret.put("RESOURCE:" + entry.getKey(), entry.getValue());
        }

        ret.putAll(ctx.getRuntimeParams());

        return Collections.unmodifiableMap(ret);
    }
}
