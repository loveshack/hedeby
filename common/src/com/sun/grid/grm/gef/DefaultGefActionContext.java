/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.impl.ResourcePropertyDeleteOperation;
import com.sun.grid.grm.resource.impl.ResourcePropertyUpdateOperation;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.logging.Handler;

/**
 * Default implementation of a GefActionContext
 */
final class DefaultGefActionContext implements GefActionContext {

    private final ExecutionEnv env;
    private final Map<String,Lock> locks;
    private final Resource resource;
    private final Handler protocolHandler;
    private final Map<String,Object> globalParams;
    private final Map<String,Object> runtimeParams;
    private final Map<String,Integer> stepInvocationCountMap = new HashMap<String,Integer>();
    private boolean isInUndoPhase;

    /**
     * Create a new DefaultGefActionContext
     * @param env  the execution env
     * @param resource  the resource
     * @param protocolHandler the protocol handler
     * @param locks   the locks for the isolation levels
     * @param globalParams the global parameters
     */
    DefaultGefActionContext(ExecutionEnv env, Resource resource, Handler protocolHandler, Map<String,Lock> locks, Map<String,Object> globalParams) {
        this.env = env;
        this.locks = locks;
        this.resource = resource;
        this.protocolHandler = protocolHandler;
        this.globalParams = Collections.unmodifiableMap(globalParams);
        this.runtimeParams = new HashMap<String,Object>();
        this.isInUndoPhase = false;
    }

    /**
     * get the protocol handler
     * @return the protocol handler
     */
    public Handler getProtocolHandler() {
        return protocolHandler;
    }

    /**
     * get the execution env
     * @return the execution env
     */
    public ExecutionEnv getExecutionEnv() {
        return env;
    }

    /**
     * lock an isolation level
     * @param isolationLevel the isolation level
     */
    public void lock(String isolationLevel) {
        locks.get(isolationLevel).lock();
    }

    /**
     * unlock an isolation level
     * @param isolationLevel the isolation level
     */
    public void unlock(String isolationLevel) {
        locks.get(isolationLevel).unlock();
    }

    /**
     * get the resource
     * @return the resource
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * Get the global parameters
     * @return the globalParams
     */
    public Map<String, Object> getGlobalParams() {
        return globalParams;
    }

    /**
     * Post process an step
     * @param lastOutputParams  the output parameters of the step
     * @return the resource change operations
     */
    List<ResourceChangeOperation> postProcessing(Map<String,Object> lastOutputParams) {

        List<ResourceChangeOperation> ret = new ArrayList<ResourceChangeOperation>(lastOutputParams.size());

        for(Map.Entry<String,Object> entry: lastOutputParams.entrySet()) {
            if(entry.getKey().startsWith("RESOURCE:")) {
                String resPropName = entry.getKey().substring("RESOURCE:".length());
                if(entry.getValue() != null) {
                    ret.add(new ResourcePropertyUpdateOperation(resPropName, entry.getValue()));
                } else {
                    ret.add(new ResourcePropertyDeleteOperation(resPropName));
                }
                continue;
            }
            if(entry.getValue() == null) {
                runtimeParams.remove(entry.getKey());
                continue;
            }
            runtimeParams.put(entry.getKey(), entry.getValue());
        }
        return ret;
    }

    /**
     * get the runtime parameters
     * @return the runtime parameters
     */
    Map<String,Object> getRuntimeParams() {
        return runtimeParams;
    }

    /**
     * get the invocation count of a step
     * @param stepName the name of the step
     * @return the invocation count
     */
    public int getInvocationCount(String stepName) {
        Integer ret = stepInvocationCountMap.get(stepName);
        return ret == null ? 0 : ret;
    }

    /**
     * increment the invocation count of an step
     * @param stepName the name of the step
     * @return the new value of the invocation counter
     */
    int incrInvocationCount(String stepName) {
        Integer count = stepInvocationCountMap.get(stepName);
        if (count == null) {
            count = 1;
        } else {
            count++;
        }
        stepInvocationCountMap.put(stepName, count);
        return count;
    }

    /**
     * If the executor is in the undo phase
     * @return <tt>true</tt> if the executor is in the undo phase
     */
    public boolean isInUndoPhase() {
        return isInUndoPhase;
    }

    /**
     *  Signalize that the executor is in undo phase
     */
    void undoPhaseStarted() {
        this.isInUndoPhase = true;
    }

    /**
     * Get the protocol file for this action.
     * @return the protocol file  or <tt>null</tt> if no protocol has been written
     */
    File getProtocolFile() {
        // If the protocol handler has been created via the ProtocolManager the
        // handler is an instanceof of ProtocolManager.ActionProtocolHandlerImpl. This
        // kind of handler knows the protocol file.
        // The GefCliCommand does not use a ProtocolManager.ActionProtocolHandlerImpl, it writes
        // the protocol into a ConsoleHandler. For this case we do not know the protocol file.
        if (protocolHandler instanceof ProtocolManager.ActionProtocolHandlerImpl) {
            return ((ProtocolManager.ActionProtocolHandlerImpl) protocolHandler).getFile();
        } else {
            return null;
        }
    }
}
