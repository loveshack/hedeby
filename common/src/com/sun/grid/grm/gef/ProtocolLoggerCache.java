/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.config.gef.LogLevel;
import java.util.LinkedList;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class caches the anonymous logger for the protocols.
 */
class ProtocolLoggerCache {

    private final LinkedList<Logger> pool = new LinkedList<Logger>();
    private final static ProtocolLoggerCache instance = new ProtocolLoggerCache();

    /**
     * get the singleton instance of this class
     * @return the singleton instance
     */
    static ProtocolLoggerCache getInstance() {
        return instance;
    }

    /**
     * Map a protocol log level to java.util.logging log level
     * @param pl the protocol log level
     * @return java.util.logging log level
     */
    static Level getLevel(LogLevel pl) {
        switch(pl) {
            case DEBUG:   return Level.FINE;
            case INFO:    return Level.INFO;
            case WARNING: return Level.WARNING;
            case ERROR:   return Level.SEVERE;
            case OFF:     return Level.OFF;
            default:
                throw new IllegalArgumentException("unknown protocol log level " + pl);
        }
    }

    /**
     * Get a protocol logger.
     *
     * <p><b>Attention:</b> The caller is responsible for calling <tt>release</t>
     * when the logger is no longer needed.</p>
     * @param ctx     the action context
     * @param level   the protocol log leve
     * @return the protocol logger
     */
    Logger get(DefaultGefActionContext ctx, LogLevel level) {
        Logger ret;
        synchronized(pool) {
            if(pool.isEmpty()) {
                ret = Logger.getAnonymousLogger();
            } else {
                ret = pool.removeFirst();
            }
        }
        ret.setUseParentHandlers(false);
        ret.addHandler(ctx.getProtocolHandler());
        ret.setLevel(getLevel(level));
        ret.setFilter(null);
        return ret;
    }

    /**
     * Release a protocol logger
     * @param logger the protocol logger
     */
    void release(Logger logger) {
        logger.setLevel(Level.OFF);
        Handler [] handlers = logger.getHandlers();
        if (handlers != null) {
            for(Handler handler: handlers) {
                logger.removeHandler(handler);
            }
        }
        synchronized(pool) {
            pool.add(logger);
        }
    }

}
