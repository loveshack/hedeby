/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.Path;
import com.sun.grid.grm.config.gef.JavaStepConfig;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Instances of this class represents a JavaStep with it's configuration
 */
class ConfigurableJavaStep extends AbstractConfigurableStep<JavaStepConfig> {

    private final static String BUNDLE = "com.sun.grid.grm.gef.gef";
    private final static Logger log = Logger.getLogger(ConfigurableJavaStep.class.getName(), BUNDLE);
    private final JavaStep step;

    /**
     * Create a new ConfigurableJavaStep
     * @param  env    the execution env
     * @param action  the action to which the step belongs to
     * @param config  the congiuration of the step
     * @throws com.sun.grid.grm.gef.GefException  of the coniguration is invalid
     */
    ConfigurableJavaStep(ExecutionEnv env, DefaultGefAction action, JavaStepConfig config) throws GefException {
        super(action, config);
        if (log.isLoggable(Level.FINER)) {
            log.entering(ConfigurableJavaStep.class.getName(), "<init>", new Object[]{action, config});
        }

        ClassLoader cl = createClassLoader(env);

        Class<?> clazz;
        try {
            clazz = cl.loadClass(config.getClassname());
        } catch (ClassNotFoundException ex) {
            throw new GefException("ConfigurableJavaStep.ex.classNotFound", ex, BUNDLE, config.getClassname());
        }

        Constructor constr;
        try {
            constr = clazz.getConstructor(String.class);
        } catch (Exception ex) {
            throw new GefException("ConfigurableJavaStep.ex.create", ex, BUNDLE, config.getClassname(), ex.getLocalizedMessage());
        }

        try {
            step = (JavaStep) constr.newInstance(config.getName());
        } catch (InvocationTargetException ex) {
            throw new GefException("ConfigurableJavaStep.ex.create", ex, BUNDLE, config.getClassname(), ex.getTargetException().getLocalizedMessage(), ex.getTargetException());
        } catch (ClassCastException ex) {
            throw new GefException("ConfigurableJavaStep.ex.invalidClass", ex, BUNDLE, config.getClassname(), JavaStep.class.getName());
        } catch (Exception ex) {
            throw new GefException("ConfigurableJavaStep.ex.create", ex, BUNDLE, config.getClassname(), ex.getLocalizedMessage());
        }
        log.exiting(ConfigurableJavaStep.class.getName(), "<init>");
    }

    /**
     * Execute the java step
     * @param ctx          the action context
     * @param inputParams  the input parameters
     * @param protocol     the protocol logger
     * @return the step result
     */
    protected final StepResult execute(DefaultGefActionContext ctx, Map<String, Object> inputParams, Logger protocol) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ConfigurableJavaStep.class.getName(), "execute", new Object[]{ctx, inputParams, protocol});
        }
        StepResult ret;
        try {
            lock(ctx);
            try {
                ret = step.execute(ctx, inputParams, protocol);
            } finally {
                unlock(ctx);
            }
        } catch (Throwable ex) {
            protocol.log(Level.SEVERE, I18NManager.formatMessage("ConfigurableJavaStep.unexpectedError", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
            ret = new StepResult(StepExitCode.PERMANENT_ERROR);
        }
        log.exiting(ConfigurableJavaStep.class.getName(), "execute", ret);
        return ret;
    }

    /**
     * Perform the undo of this step
     * @param ctx          the action context
     * @param inputParams  the input parameters
     * @param protocol     the protocol logger
     * @return the output parameters
     */
    protected final Map<String, Object> undo(DefaultGefActionContext ctx, Map<String, Object> inputParams, Logger protocol) {
        if (log.isLoggable(Level.FINER)) {
            log.entering(ConfigurableJavaStep.class.getName(), "undo", new Object[]{ctx, inputParams, protocol});
        }
        Map<String, Object> ret;
        try {
            lock(ctx);
            try {
                ret = step.undo(ctx, inputParams, protocol);
            } finally {
                unlock(ctx);
            }
        } catch (Throwable ex) {
            protocol.log(Level.SEVERE, I18NManager.formatMessage("ConfigurableJavaStep.unexpectedUndoError", BUNDLE, getName(), ex.getLocalizedMessage()), ex);
            ret = Collections.<String, Object>emptyMap();
        }
        log.exiting(ConfigurableJavaStep.class.getName(), "undo", ret);
        return ret;
    }

    
    private ClassLoader createClassLoader(ExecutionEnv env) throws GefException {
        log.entering(ConfigurableJavaStep.class.getName(), "createClassLoader");
        ClassLoader ret;
        if (!getConfig().isSetClasspath()) {
            ret = getClass().getClassLoader();
        } else {
            Path classpath = getConfig().getClasspath();

            URL[] urls = new URL[classpath.getPathelement().size()];

            int i = 0;
            for (String c : classpath.getPathelement()) {
                URL url;
                try {
                    url = new URL(c);
                } catch (MalformedURLException ex) {
                    File f = new File(c);
                    if(!f.isAbsolute()) {
                        f = new File(env.getDistDir(), c);
                    }
                    if (!f.exists()) {
                        throw new GefException("ConfigurableJavaStep.classpath.notFound", ex, BUNDLE, c);
                    }
                    try {
                        url = f.toURI().toURL();
                    } catch (MalformedURLException ex1) {
                        // Should not happen
                        throw new GefException("ConfigurableJavaStep.classpath.invalid", ex1, BUNDLE, c, ex1.getLocalizedMessage());
                    }
                }
                urls[i++] = url;
            }
            ret = new URLClassLoader(urls);
        }
        log.exiting(ConfigurableJavaStep.class.getName(), "createClassLoader", ret);
        return ret;
    }
}
