/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import java.util.Collections;
import java.util.Map;

/**
 * This class represents the result of a GEF step
 */
public class StepResult {

    private final StepExitCode exitCode;
    private final Map<String,Object> outputParams;

    /**
     * create a new instance of StepResult
     * @param exitCode the exit code of the GEF step
     */
    public StepResult(StepExitCode exitCode) {
        this(exitCode, Collections.<String,Object>emptyMap());
    }
    
    /**
     * create a new instance of StepResult
     * @param exitCode the exit code of the GEF step
     * @param outputParams  the output parameters of the GEF step
     */
    public StepResult(StepExitCode exitCode, Map<String,Object> outputParams) {
        this.exitCode = exitCode;
        this.outputParams = outputParams;
    }

    /**
     * @return the exitCode
     */
    public StepExitCode getExitCode() {
        return exitCode;
    }

    /**
     * @return the outputParams
     */
    public Map<String, Object> getOutputParams() {
        return outputParams;
    }

    @Override
    public String toString() {
        return String.format("[StepResult:%s,%s]", exitCode, outputParams);
    }



}
