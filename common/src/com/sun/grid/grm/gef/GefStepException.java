/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import java.util.Collection;
import java.util.Collections;

/**
 * Instances of this exception are thrown of a GEF step failed
 */
public class GefStepException extends GefException {

    private final static String BUNDLE = "com.sun.grid.grm.gef.gef";
    private final static long serialVersionUID = 2009092301L;
    
    private final StepExitCode errorCode;
    private final String stepName;
    private final Collection<ResourceChanged> changes;

    /**
     * Create a new instanceof of a GefStepException
     * @param step the step
     * @param errorCode  the error code
     */
    public GefStepException(Step step, StepExitCode errorCode) {
        this(step, errorCode, (Throwable)null);
    }

    /**
     * Create a new instanceof of a GefStepException
     * @param step the step
     * @param errorCode  the error code
     * @param ex the cause of the exception
     */
    public GefStepException(Step step, StepExitCode errorCode, Throwable ex) {
        this(step, errorCode, Collections.<ResourceChanged>emptySet(), ex);
    }

    /**
     * Create a new instanceof of a GefStepException
     * @param step the step
     * @param errorCode  the error code
     * @param changes  resource changes of the step
     */
    public GefStepException(Step step, StepExitCode errorCode, Collection<ResourceChanged> changes) {
        this(step, errorCode, changes, null);
    }


    /**
     * Create a new instanceof of a GefStepException
     * @param step the step
     * @param errorCode  the error code
     * @param changes  resource changes of the step
     * @param ex the cause of the exception
     */
    public GefStepException(Step step, StepExitCode errorCode, Collection<ResourceChanged> changes, Throwable ex) {
        super("GefStepException.msg", ex, BUNDLE, step.getName(), errorCode);
        this.stepName = step.getName();
        this.errorCode = errorCode;
        this.changes = changes;
    }

    /**
     * get the name of the failed step
     * @return the name of the failed step
     */
    public String getStepName() {
        return stepName;
    }

    /**
     * get the error code of the failed step
     * @return the errorCode
     */
    public StepExitCode getErrorCode() {
        return errorCode;
    }

    /**
     * Get the resource changes produced by the step
     * @return the changes the resource changes
     */
    public Collection<ResourceChanged> getChanges() {
        return changes;
    }
}
