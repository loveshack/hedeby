/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.gef;

import com.sun.grid.grm.config.gef.AbstractStepConfig;
import com.sun.grid.grm.config.gef.LogLevel;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.TimeIntervalHelper;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterException;
import com.sun.grid.grm.util.filter.FilterHelper;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract base class for a step of a GEF action that has a configuration.
 * <C> the type of the configuration
 */
abstract class AbstractConfigurableStep<C extends AbstractStepConfig> implements ConfigurableStep<C> {

    private final static String BUNDLE = "com.sun.grid.grm.gef.gef";
    
    private final DefaultGefAction action;
    private final C config;
    private final Filter<Resource> filter;
    private final TimeIntervalHelper delay;

    private final static TimeIntervalHelper DEFAULT_DELAY = new TimeIntervalHelper(10, TimeIntervalHelper.SECONDS);

    /**
     * Create a new instance of AbstractConfigurableStep
     * @param action  the action
     * @param config  the configuration of the step
     * @throws com.sun.grid.grm.gef.GefException if the configuration is not valid
     */
    AbstractConfigurableStep(DefaultGefAction action, C config) throws GefException  {
        this.action = action;
        this.config = config;

        if (config.isSetFilter()) {
            try {
                this.filter = FilterHelper.<Resource>parse(config.getFilter());
            } catch (FilterException ex) {
                throw new GefException(ex.getLocalizedMessage(), ex);
            }
        } else {
            this.filter = ConstantFilter.<Resource>alwaysMatching();
        }

        if (config.isSetDelay()) {
            delay = new TimeIntervalHelper(config.getDelay());
        } else {
            delay = DEFAULT_DELAY;
        }
    }

    /**
     * get the aciton
     * @return
     */
    public final DefaultGefAction getAction() {
        return action;
    }

    /**
     * Get the protocol logger for the step.
     * 
     * This method sets the effective log level for the step in the logger.
     * 
     * The effective log level is taken from the step if it
     * is smaller than the action log level (DEBUG &lt; INFO &lt; WARNING ...).
     * 
     * @param ctx the action context
     * @return the protocol logger
     */
    private Logger getProtocol(DefaultGefActionContext ctx) {
        LogLevel ret = action.getConfig().getLogLevel();
        if (config.isSetLogLevel()) {
            LogLevel stepLogLevel = config.getLogLevel();
            if (stepLogLevel.compareTo(ret) < 0) {
                ret = stepLogLevel;
            }
        }
        return ProtocolLoggerCache.getInstance().get(ctx, ret);
    }

    private void realeaseProtocol(Logger protocol) {
        ProtocolLoggerCache.getInstance().release(protocol);
    }

    /**
     * get the delay for repeating the step
     * @return the delay
     */
    public TimeIntervalHelper getDelay() {
        return delay;
    }

    /**
     * get the max. number of tries for the step
     * @return the max. number of tries
     */
    public int getMaxTries() {
        return config.isSetMaxTries() ? config.getMaxTries() : 1;
    }

    /**
     * Is this try the last try
     * @param ctx the action context
     * @return <tt>true</tt> if this try is the last try
     */
    public boolean isLastTry(GefActionContext ctx) {
        return ctx.getInvocationCount(getName()) >= getMaxTries();
    }

    /**
     * get the resourced filter
     * @return
     */
    public Filter<Resource> getFilter() {
        return filter;
    }

    /**
     * get the configuration of the setp
     * @return the configuration of the step
     */
    public C getConfig() {
        return config;
    }

    public String getName() {
        return config.getName();
    }

    /**
     * lock the isolation level of the step
     * @param ctx the aciton context
     */
    protected void lock(DefaultGefActionContext ctx) {
        if (config.isSetIsolationLevel()) {
            ctx.lock(config.getIsolationLevel());
        }
    }

    /**
     * unlock the isolation level of the step
     * @param ctx the aciton context
     */
    protected void unlock(DefaultGefActionContext ctx) {
        if (config.isSetIsolationLevel()) {
            ctx.unlock(config.getIsolationLevel());
        }
    }

    /**
     * Execute this step
     * @param ctx          the action context
     * @param inputParams  the input parametes
     * @return the result of the step
     */
    public final StepResult execute(DefaultGefActionContext ctx, Map<String, Object> inputParams) {
        
        Logger protocol = getProtocol(ctx);
        if (protocol.isLoggable(Level.FINE)) {
            protocol.log(Level.FINE, I18NManager.formatMessage("AbstractConfigurableStep.input", BUNDLE, inputParams ));
        }

        try {
            StepResult ret = execute(ctx, inputParams, protocol);
            if (protocol.isLoggable(Level.FINE)) {
                protocol.log(Level.FINE, I18NManager.formatMessage("AbstractConfigurableStep.output", BUNDLE, inputParams ));
            }
            Level level;
            switch (ret.getExitCode()) {
                case SUCCESS:
                    level = Level.INFO;
                    break;
                case REPEAT:
                    level = isLastTry(ctx) ? Level.SEVERE : Level.INFO;
                    break;
                default:
                    level = Level.SEVERE;
            }
            if (protocol.isLoggable(level)) {
                protocol.log(level, I18NManager.formatMessage("AbstractConfigurableStep.exit", BUNDLE, getName(), ret.getExitCode() ));
            }
            return ret;
        } finally {
            realeaseProtocol(protocol);
        }
    }

    /**
     * Execute this step
     * @param ctx the action context
     * @param inputParams the input parameters
     * @param protocol the protocol logger
     * @return the step result
     */
    protected abstract StepResult execute(DefaultGefActionContext ctx, Map<String, Object> inputParams, Logger protocol);

    /**
     * Undo this step.
     * @param ctx      the action context
     * @param params   the input parameters
     * @return the output parameters of the undo
     */
    public Map<String,Object> undo(DefaultGefActionContext ctx, Map<String, Object> inputParams) {

        Logger protocol = getProtocol(ctx);
        if (protocol.isLoggable(Level.FINE)) {
            protocol.log(Level.FINE, I18NManager.formatMessage("AbstractConfigurableStep.input", BUNDLE, inputParams));
        }
        try {
            Map<String, Object> outputParams = undo(ctx, inputParams, protocol);
            if (protocol.isLoggable(Level.FINE)) {
            protocol.log(Level.FINE, I18NManager.formatMessage("AbstractConfigurableStep.output", BUNDLE, outputParams));
            }
            return outputParams;
        } finally {
            realeaseProtocol(protocol);
        }
    }

    /**
     * Undo this step.
     * @param ctx      the action context
     * @param params   the input parameters
     * @param protocol the protocol logger
     * @return the output parameters of the undo
     */
    protected abstract Map<String,Object> undo(DefaultGefActionContext ctx, Map<String, Object> params, Logger protocol);
}