/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2006 by Sun Microsystems, Inc
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm;

import com.sun.grid.grm.management.Manageable;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.util.Hostname;

/** 
 * This interface describes all actions which have to be implemented by
 * a Grm component or subcomponent.
 *
 */
@Manageable
public interface GrmComponent {    
    
    /**
     * Trigger a state transition from STOPPED to STARTED.
     * @throws com.sun.grid.grm.GrmException 
     */
    public void start() throws GrmException;

    /**
     * Triggers a state transition from STARTED to STOPPING.
     * 
     * @param isForced Force the state transition.
     * @throws com.sun.grid.grm.GrmException 
     */
    public void stop(boolean isForced) throws GrmException;
    
    /**
     * Triggers a state transition from STARTED to RELOADING.
     * 
     * @param isForced Force the state transition.
     * @throws com.sun.grid.grm.GrmException when an error happend. It can
     * also be a ReloadNotSupportedException, when the Service does not support the
     * reload.
     */
    public void reload(boolean isForced) throws GrmException;

    /**
     * Returns the current component state.
     *
     * @return Current state.
     * @throws GrmRemoteException on any remote error
     */
    public ComponentState getState() throws GrmRemoteException;

    /**
     *  Add a component event listener to this component.
     *
     *  @param componentEventListener the listener
     *  @throws GrmRemoteException on any remote error
     */
    public void addComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException;
    
    /**
     *  Remove a component evente listener from this component.
     *
     *  @param componentEventListener the listener
     *  @throws GrmRemoteException on any remote error
     */
    public void removeComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException;
    
    /**
     *  Get the name of the component.
     *
     *  @return name of the component
     *  @throws GrmRemoteException on any remote error
     */
    public String getName() throws GrmRemoteException;
    
    /**
     *  Get the name of the host on which runs the component.
     *
     *  @return name of the host
     *  @throws GrmRemoteException on any remote error
     */
    public Hostname getHostname() throws GrmRemoteException;
    
}
