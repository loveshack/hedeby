<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<!--
/*___INFO__MARK_BEGIN__*/
/*************************************************************************
*
*  The Contents of this file are made available subject to the terms of
*  the Sun Industry Standards Source License Version 1.2
*
*  Sun Microsystems Inc., March, 2001
*
*
*  Sun Industry Standards Source License Version 1.2
*  =================================================
*  The contents of this file are subject to the Sun Industry Standards
*  Source License Version 1.2 (the "License"); You may not use this file
*  except in compliance with the License. You may obtain a copy of the
*  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
*
*  Software provided under this License is provided on an "AS IS" basis,
*  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
*  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
*  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
*  See the License for the specific provisions governing your rights and
*  obligations concerning the Software.
*
*   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
*
*   Copyright: 2006 by Sun Microsystems, Inc
*
*   All Rights Reserved.
*
************************************************************************/
/*___INFO__MARK_END__*/
-->
<%@page import="java.util.*"%>
<%@page import="com.sun.grid.grm.cli.*"%>
<%@page import="java.lang.reflect.*"%>
<%@page import="com.sun.grid.grm.security.UserPrivilege"%>

<%
   
  String classname = (String)params.get("classname");
  
  Class aClass = null;
  try {
      aClass = Class.forName(classname);
  } catch(ClassNotFoundException cnf) {
      throw new IllegalStateException("class " + classname + " not found");
  }
  

  AbstractCli cli = null;
  try {
      cli = (AbstractCli)aClass.newInstance();
  } catch(Exception e) {
      throw new IllegalStateException("Can not create new instance of " + aClass.getName());
  }
  
  cli.loadAllExtensions();
  
  String name = cli.getName();
  
  class ManFormatter {
      
      public String format(String msg) {
          
          StringBuilder ret = new StringBuilder();
          
          ret.append("<para>");
          
          for(int i = 0; i < msg.length(); i++) {
              char c = msg.charAt(i);
              switch(c) {
                  case '\r': continue;
                  case '\n': ret.append("</para><para>"); break;
                  case '<': ret.append("&lt;"); break;
                  case '>': ret.append("&gt;"); break;
                  case '&': ret.append("&amp;"); break;
                  default:
                      ret.append(c);
                  
              }
          }
          ret.append("</para>");
          return ret.toString().trim();
      }
      
      public String formatSL(String msg) {
          StringBuilder ret = new StringBuilder();
          for(int i = 0; i < msg.length(); i++) {
              char c = msg.charAt(i);
              switch(c) {
                  case '<': ret.append("&lt;"); break;
                  case '>': ret.append("&gt;"); break;
                  default:
                      ret.append(c);
                  
              }
          }
          return ret.toString();
      }
      
  }
  
  ManFormatter formatter = new ManFormatter();
  
%>
<article lang="en-US">
    
    <artheader>
        <title><application><%=name%></application> Manual</title>
    </artheader>    
    
    <sect1 id="NAME"><title>NAME</title>
        
        <para>
            <%=name%> - <%
            try { 
            %> <%=formatter.format(AbstractCli.getDescription(name.toLowerCase()))%> 
            <%
            } catch(MissingResourceException re) {
            %> TODO \- <%=name%>.description is not defined 
            <%           
            }
            %>
        </para>
    </sect1>
    <sect1 id="SYNOPSIS"><title>SYNOPSIS</title>
        
        <synopsis>
    <%=name%> [ global_options ] command [ command_options ]
        </synopsis>
        
    </sect1>
    <sect1 id="DESCRIPTION"><title>DESCRIPTION</title>
        <para>
            <%
            try { 
            %>
            <%=formatter.format(AbstractCli.getLongDescription(name.toLowerCase()))%>
            <%
            } catch (MissingResourceException re) { %>
            TODO \- <%=name%>.long_description is not defined     
            <%   }
            %>
        </para>
    </sect1>
    
    <sect1 id="COMMANDS"><title>COMMANDS</title>
<%
   Map<CliCategory,Set<Class<? extends CliCommand>>> categoryMap = cli.getSortedCategoryCommandMap();
   for(CliCategory category: categoryMap.keySet()) {
%>
   <sect2 id="<%=category.name()%>_COMMANDS"><title><%=AbstractCli.getLocalizedCategoryName(category)%></title>
    <variablelist>
<%     
       for(Class<? extends CliCommand> cmd: categoryMap.get(category)) { 
          CliCommandDescriptor descr = cmd.getAnnotation(CliCommandDescriptor.class);
        
        // ---------------------------------------------------
        // Print the synopsis of the command
        // ---------------------------------------------------
        %>
        <varlistentry>
            <term>      
                <synopsis>
<%                %> <%=AbstractCli.getName(descr, cmd.getClassLoader())%><%

                    if(descr.hasShortcut()) {
                         %>|<%=AbstractCli.getShortcut(descr,  cmd.getClassLoader())%><%
                    } 
                    %> <%
                    for(Field field: cli.getAnnotatedFields(cmd)) {
                        CliOptionDescriptor fieldDescr = field.getAnnotation(CliOptionDescriptor.class);
                        if(!fieldDescr.required()) {
                        %> [<%
                        }                    
                        %><%=AbstractCli.getName(field)%><%
                        
                                               
                        if(fieldDescr.numberOfOperands() != 0) {
                            %> <%=formatter.formatSL(AbstractCli.getArgument(field))%><%
                        }                    
                        if(!fieldDescr.required()) {
                        %>]<%
                        } %> <%
                    }
            %> </synopsis>
            </term><!-- end of cmdsynopsis <%=AbstractCli.getName(descr, cmd.getClassLoader())%> -->
            <listitem>
                <para><%=formatter.format(AbstractCli.getDescription(descr, cmd.getClassLoader()))%></para>
                <para>
                    <% try { %>
                    <%= formatter.format(AbstractCli.getLongDescription(descr, cmd.getClassLoader()))%>
                    <% } catch(MissingResourceException ex) { %>
                    TODO - <%=descr.name()%>.long_description is not defined
                    <% } %>
                </para>
                
                <%
                Collection<Field> fields = cli.getAnnotatedFields(cmd);
                boolean hasFields = false;
                for(Field field: fields) {
                    CliOptionDescriptor fieldDescr = field.getAnnotation(CliOptionDescriptor.class); 
                    hasFields = true;
                    break;                      
                }                
                if(hasFields) {
                %>
                <informaltable frame="all" cellpadding="5">
                <tgroup cols="3">
                <thead>
                  <row>
                    <entry><para>Option</para></entry>
                    <entry><para>Shortcut</para></entry>
                    <entry><para>Description</para></entry>
                  </row>
                </thead>    
                <tbody>
                    <%     
                    for(Field field: fields) {
                    CliOptionDescriptor fieldDescr = field.getAnnotation(CliOptionDescriptor.class); 
                    %>
                    <row>
                        <entry valign="top"><option><%=AbstractCli.getName(field)%></option></entry>
                        <entry valign="top">
                            <%
                            if(fieldDescr.hasShortcut() ) {
                            %> (<%=AbstractCli.getShortcut(field)%>) <%
                            } else { %>
                            n/a
                            <% } %>
                        </entry>
                        <entry valign="top">
                            <%=formatter.format(AbstractCli.getDescription(field))%> 
                        </entry>
                    </row>
                    <%                    
                    } // end of for if(hasFields) 
                    %>
                </tbody>
                </tgroup>
                </informaltable>
<% } // end of if (hasFields) %>
                <para>Security:</para>
                <informaltable frame="all" cellpadding="5">
                <tgroup cols="2">
                <tbody>
                <row>
                  <entry valign="top"><para>Required Privileges:</para></entry>
                  <entry valign="top"> <%
      {
         boolean firstPriv = true;
         String [] reqPriv = descr.requiredPrivileges();
         if(reqPriv == null || reqPriv.length == 0) {
             %>No privileges defined<%
         }
         for(String privilege: descr.requiredPrivileges()) {
             if(firstPriv) {
                 firstPriv = false;
             } else {
                 %>,<%
             }
        %> <%=UserPrivilege.getName(privilege)%><%
         }
      } %>       </entry>
              </row>
<%                    
      String [] optPriv = descr.optionalPrivileges();
      if (optPriv != null && optPriv.length > 0 ) { %>
              <row>
              <entry valign="top"><para>Optional Privileges:</para></entry>
              <entry valign="top"><%
         boolean firstPriv = true;
         for(String privilege: optPriv) {
             if(firstPriv) {
                 firstPriv = false;
             } else {
                 %>,<%
             }
             %> <%=UserPrivilege.getName(privilege)%><%
         } %>
              </entry>
          </row> 
<%    }
%>    
      </tbody>
  </tgroup>
      </informaltable>
   
<% try { 
      String longDescr = formatter.format(AbstractCli.getPrivilegesDescription(descr, cmd.getClassLoader()));%>
      <para>
      <%=longDescr%>
      </para>
<% } catch(MissingResourceException ex) {
     // Ignore, the privileges description is option
   } %> 
     <para>A detailed description of the privileges can be found in <xref linkend="SECURITY_PRIVILEGES"/>.</para>
            </listitem>
        </varlistentry>
                   <%
                    } // end of cmd loop  
                    %>   
        </variablelist>
        
   </sect2>
<% 
   }  
%>   
   </sect1>
   <sect1 id="GLOBAL_OPTIONS"><title>GLOBAL OPTIONS</title>
        
    <informaltable frame="all">
    <tgroup cols="3">
    <thead>
      <row>
        <entry><para>Option</para></entry>
        <entry><para>Shortcut</para></entry>
        <entry><para>Description</para></entry>
      </row>
    </thead>    
    <tbody>    
        <%
        /*
        Iterator of all commands
        */
        for(Field option: cli.getAnnotatedFields(cli.getClass())) {
        
        CliOptionDescriptor descr = option.getAnnotation(CliOptionDescriptor.class);
        
        // ---------------------------------------------------
        // Print the synopsis of the global option
        // ---------------------------------------------------
        %>
        <row>
            <entry valign="top"><option><%=AbstractCli.getName(option)%></option></entry>
            <entry valign="top">
                <option>
                    <% if(descr.hasShortcut()) {
                    %> <%=AbstractCli.getShortcut(option)%> <%
                    } else {
                    %> n/a <%
                    }
                    %>
                </option>
            </entry>
            <entry valign="top">
                <para>
                    <% 
                    // ---------------------------------------------------
                    // Print the description of the options
                    // ---------------------------------------------------
                    %>
                    <%= formatter.format(AbstractCli.getDescription(option))%> 
                    <% try { %>
                    <%= formatter.format(AbstractCli.getLongDescription(option))%>
                    <% } catch(MissingResourceException ex) { %>
                    TODO - <%=descr.name()%>.long_description is not defined
                    <% }
                    %>
                </para>
            </entry>
        </row>
        <%
        } // end of for options
        %>
    </tbody>
    </tgroup>
    </informaltable>                
        
    </sect1>
    
    <sect1 id="SECURITY_PRIVILEGES"><title>SECURITY PRIVILEGES</title>
    <para>
    The security of the <%=params.get("product_short")%> system checks according a
    defined set privileges where a user has the permissions to access or modify
    the system. Each sdmadm subcommand defines what privileges for the successfully
    execution are necessary. The following list gives an overview of the available
    privileges.
    </para>
    <informaltable frame="all">
    <tgroup cols="2">
    <thead>
      <row>
        <entry><para>Privileges</para></entry>
        <entry><para>Description</para></entry>
      </row>
    </thead>    
    <tbody>    
<% for(String privilege: UserPrivilege.ALL_PRIVILEGES) {
%>
       <row><entry><%=UserPrivilege.getName(privilege)%></entry>
            <entry><para><%= UserPrivilege.getDescription(privilege)%></para>
<% try { %>
<%= formatter.format(UserPrivilege.getLongDescription(privilege))%>
<% } catch(MissingResourceException ex) { %>
TODO \- <%=privilege%>.long_description is not defined
<%   } %>
            </entry>
       </row>
<%   
  } // end of for
%>
   </tbody>
</tgroup>
</informaltable>
    </sect1>
    <sect1 id="ENVIRONMENTAL_VARIABLES"><title>ENVIRONMENTAL VARIABLES</title>
        
        <variablelist>
            <varlistentry><term><varname>SDM_SYSTEM</varname></term>
                <listitem>
                    <para>If set, specifies the name of the system on which the action will be executed.
                    </para>
                </listitem>
            </varlistentry>
        </variablelist>
    </sect1>
    <sect1 id="FILES"><title>FILES</title>
        <para>
            <%=cli.getName()%> uses the java preferences to store configurations. On unix
            systems the preferences normally stored in files (e.g <filename>/etc/.java/.systemPrefs</filename> or
            <filename>$HOME/.java/.userPrefs</filename>). For more details please consult the documentation 
            of the used java implementation.
        </para>
    </sect1>
    <sect1 id="COPYRIGHT"><title>COPYRIGHT</title>
        <para>
            The Contents of this document are made available subject to the terms of
            the Sun Industry Standards Source License Version 1.2 (see 
            <ulink url="http://hedeby.sunsource.net/license.html">http://hedeby.sunsource.net/license.html</ulink>).
        </para>
    </sect1>
</article>
