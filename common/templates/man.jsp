\"<%@page import="java.util.*"%>
\"<%@page import="com.sun.grid.grm.cli.*"%>
\"<%@page import="com.sun.grid.grm.security.UserPrivilege"%>
\"<%@page import="java.lang.reflect.*"%>
\"<%
  
  String classname = (String)params.get("classname");
  
  Class aClass = null;
  try {
      aClass = Class.forName(classname);
  } catch(ClassNotFoundException cnf) {
      throw new IllegalStateException("class " + classname + " not found");
  }
  

  AbstractCli cli = null;
  try {
      cli = (AbstractCli)aClass.newInstance();
  } catch(Exception e) {
      throw new IllegalStateException("Can not create new instance of " + aClass.getName(), e);
  }
  
  cli.loadAllExtensions();
  
  String name = cli.getName();
  
  class ManFormatter {
      
      public String format(String msg) {
          
          StringBuilder ret = new StringBuilder();
          
          for(int i = 0; i < msg.length(); i++) {
              char c = msg.charAt(i);
              switch(c) {
                  case '\r': continue;
                  case '\n': ret.append("\n.br\n"); break;
                  default:
                      ret.append(c);
                  
              }
          }
          return ret.toString();
      }
      
  }
  
  ManFormatter formatter = new ManFormatter();
  
  int year = java.util.Calendar.getInstance().get(java.util.Calendar.YEAR);
%>'\" t
.\"___INFO__MARK_BEGIN__
.\"
.\" Copyright: <%=year%> by Sun Microsystems, Inc.
.\"
.\"___INFO__MARK_END__
.\"
.\" $RCSfile: man.jsp,v $
.\"
.\"
.\" Some handy macro definitions [from Tom Christensen's man(1) manual page].
.\"
.de SB		\" small and bold
.if !"\\$1"" \\s-2\\fB\&\\$1\\s0\\fR\\$2 \\$3 \\$4 \\$5
..
.\"
.de T		\" switch to typewriter font
.ft CW		\" probably want CW if you don't have TA font
..
.\"
.de TY		\" put $1 in typewriter font
.if t .T
.if n ``\c
\\$1\c
.if t .ft P
.if n \&''\c
\\$2
..
.\"
.de M		\" man page reference
\\fI\\$1\\fR\\|(\\$2)\\$3
..
.TH <%=name%> 1 "2008/07/22 12:19:28" "<%=params.get("product_short")%> <%=params.get("version")%>" ""<%=params.get("product_long")%> User Commands"
.SH NAME

<%=name%> \- <%
    try { 
%> <%=AbstractCli.getDescription(name.toLowerCase()) %> 
<%
    } catch(MissingResourceException re) {
%> TODO \- description of <%=name%> is not defined 
<%           
    }
%>
  
.SH SYNOPSIS
.B <%=name%>
[
.B global_options
]
[
.B command 
[
.B command_options
]
]

.SH DESCRIPTION
<%
   try { 
%>
<%= AbstractCli.getLongDescription(name.toLowerCase())%>
<%
} catch (MissingResourceException re) { %>
   TODO \- <%=name%>.long_description is not defined     
<%   }
%>

.SH COMMANDS

<%
   Map<CliCategory,Set<Class<? extends CliCommand>>> categoryMap = cli.getSortedCategoryCommandMap();
   for(CliCategory category: categoryMap.keySet()) {
%>
.SH <%=AbstractCli.getLocalizedCategoryName(category)%>

<%
   for(Class<? extends CliCommand> cmd: categoryMap.get(category)) {  
      CliCommandDescriptor descr = cmd.getAnnotation(CliCommandDescriptor.class);
         
     // ---------------------------------------------------
     // Print the synopsis of the command
     // ---------------------------------------------------
%>
.IP "<%=AbstractCli.getName(descr, cmd.getClassLoader())%> <%
        if(descr.hasShortcut()) {
%> (<%=AbstractCli.getShortcut(descr,  cmd.getClassLoader())%>) <%
        }
%>"
<%
            for(Field field: cli.getAnnotatedFields(cmd)) {
                CliOptionDescriptor fieldDescr = field.getAnnotation(CliOptionDescriptor.class);
                if(!fieldDescr.required()) {
%>[<%
                }     
%> <%=AbstractCli.getName(field)%> <%
                if(fieldDescr.numberOfOperands() != 0) {
%><%=AbstractCli.getArgument(field)%><%
                }
                if(!fieldDescr.required()) {
%>] <%
                }
%><%
            }

     // ---------------------------------------------------
     // Print the description of the command
     // ---------------------------------------------------
         %>
.br
<%= AbstractCli.getDescription(descr, cmd.getClassLoader())%> 
<% try { %>
<%= formatter.format(AbstractCli.getLongDescription(descr, cmd.getClassLoader()))%>
<% } catch(MissingResourceException ex) { %>
TODO \- <%=descr.name()%>.long_description is not defined
<% } %>      
<%
      for(Field field: cli.getAnnotatedFields(cmd)) {
          CliOptionDescriptor fieldDescr = field.getAnnotation(CliOptionDescriptor.class); 
%><%=AbstractCli.getName(field)%> <%
          if(fieldDescr.hasShortcut() ) {
%> (<%=AbstractCli.getShortcut(field)%>) <%
          }
%> <%=AbstractCli.getDescription(field)%> 
.br
<%                    
      }
%>
Security:

Required Privileges:<%
      {
         boolean firstPriv = true;
         String [] reqPriv = descr.requiredPrivileges();
         if(reqPriv == null || reqPriv.length == 0) {
             %>No privileges defined<%
         }
         for(String privilege: descr.requiredPrivileges()) {
             if(firstPriv) {
                 firstPriv = false;
             } else {
                 %>,<%
             }
        %> <%=UserPrivilege.getName(privilege)%><%
         }
      }
      
      {
        String [] optPriv = descr.optionalPrivileges();
        if (optPriv != null && optPriv.length > 0 ) {%>

Optional Privileges:<%            
         boolean firstPriv = true;
         for(String privilege: optPriv) {
             if(firstPriv) {
                 firstPriv = false;
             } else {
                 %>,<%
             }
        %> <%=UserPrivilege.getName(privilege)%><%
         }
            
        }
      }
%>

<% try { %>
<%= formatter.format(AbstractCli.getPrivilegesDescription(descr, cmd.getClassLoader()))%>
<% } catch(MissingResourceException ex) {
     // Ignore, the privileges description is option
   } %> 
A detailed description of the privileges can be found in section SECURITY PRIVILEGES.
<%
     } // end of for
   } // end of for
%>

.SH GLOBAL OPTIONS
.br
<%
   /*
       Iterator of all commands
    */
   for(Field option: cli.getAnnotatedFields(cli.getClass())) {
       
       CliOptionDescriptor descr = option.getAnnotation(CliOptionDescriptor.class);
       
     // ---------------------------------------------------
     // Print the synopsis of the global option
     // ---------------------------------------------------
%>

.IP "<%=AbstractCli.getName(option)%> <%
        if(descr.hasShortcut()) {
%> (<%=AbstractCli.getShortcut(option)%>) <%
        }
%>"
<% 
     // ---------------------------------------------------
     // Print the description of the options
     // ---------------------------------------------------
         %>
.br
<%= AbstractCli.getDescription(option)%> 
<% try { %>
<%= formatter.format(AbstractCli.getLongDescription(option))%>
<% } catch(MissingResourceException ex) { %>
TODO \- <%=descr.name()%>.long_description is not defined
<% }
   } // end of for options
%>

.SH "SECURITY PRIVILEGES"

The security of the <%=params.get("product_short")%> system checks according a
defined set privileges where a user has the permissions to access or modify
the system. Each sdmadm subcommand defines what privileges for the successfully
execution are necessary. The following list gives an overview of the available
privileges.

<% for(String privilege: UserPrivilege.ALL_PRIVILEGES) {
%>
.IP "<%=UserPrivilege.getName(privilege)%>"
<%= UserPrivilege.getDescription(privilege)%>
<% try { %>
<%= formatter.format(UserPrivilege.getLongDescription(privilege))%>
<% } catch(MissingResourceException ex) { %>
TODO \- <%=privilege%>.long_description is not defined
<% }
}%>

.SH "ENVIRONMENTAL VARIABLES"
.IP "\fBSDM_SYSTEM\fP" 1.5i
If set, specifies the name of the system on which the action will be executed.
.sp 1
.RS
.RS

.SH FILES
<%=cli.getName()%> find the SDM systems in the boostrap configuration. The
bootstrap configuration is stored in /etc/sdm/boostrap or $HOME/.sdm/boostrap.


.SH "COPYRIGHT"

Copyright: <%=year%> by Sun Microsystems, Inc.

