<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<!--
/*___INFO__MARK_BEGIN__*/
/*************************************************************************
*
*  The Contents of this file are made available subject to the terms of
*  the Sun Industry Standards Source License Version 1.2
*
*  Sun Microsystems Inc., March, 2001
*
*
*  Sun Industry Standards Source License Version 1.2
*  =================================================
*  The contents of this file are subject to the Sun Industry Standards
*  Source License Version 1.2 (the "License"); You may not use this file
*  except in compliance with the License. You may obtain a copy of the
*  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
*
*  Software provided under this License is provided on an "AS IS" basis,
*  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
*  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
*  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
*  See the License for the specific provisions governing your rights and
*  obligations concerning the Software.
*
*   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
*
*   Copyright: 2006 by Sun Microsystems, Inc
*
*   All Rights Reserved.
*
************************************************************************/
/*___INFO__MARK_END__*/
-->
<article lang="en-US">
    <artheader>
        <title>Setting up the build environment for Hedeby</title>
        <legalnotice>
            <para>
                The Contents of this document are made available subject to the terms of
                the Sun Industry Standards Source License Version 1.2 (see 
                <ulink url="http://hedeby.sunsource.net/license.html">http://hedeby.sunsource.net/license.html</ulink>).
            </para>
        </legalnotice>
    </artheader>    
    <sect1><title>Requirements</title>
    <orderedlist>
        <listitem>
            <para>Java 1.5 (<ulink url="http://java.sun.com/">http://java.sun.com</ulink>)</para>
        </listitem>
        <listitem>
            <para>Apache Ant 1.6.5 or higher (<ulink url="http://ant.apache.org/">http://ant.apache.org</ulink>)</para>
        </listitem>
        <listitem>
            <para>Gridengine installation (you need current version from CVS containingjuti and jgdi, <ulink url="http://gridengine.sunsource.net/">http://gridengine.sunsource.net</ulink>)</para>
        </listitem>
        <listitem>
            <para>javahelp 2.0.x (<ulink url="http://javahelp.dev.java.net/">http://javahelp.dev.java.net</ulink>)</para>
        </listitem>
        <listitem>
            <para>Xalan 2.7 (<ulink url="http://xalan.apache.org/">http://xalan.apache.org/</ulink>, required
                  for XSLT processing)</para>
        </listitem>
        <listitem>
            <para>Apache XML Resolver 1.2 (<ulink url="http://xml.apache.org/commons/">http://xml.apache.org/commons/</ulink>, required
                  for XSLT processing)</para>
        </listitem>
        <listitem>
            <para>Docbook 4.2 (<ulink url="http://www.oasis-open.org/docbook/xml/4.2">http://www.oasis-open.org/docbook/xml/4.2</ulink>, required
                  generating javahelp system)</para>
        </listitem>
        <listitem>
            <para>Docbook XSL  1.71.1 (<ulink url="http://docbook-xsl.sourceforge.net/">http://docbook-xsl.sourceforge.net/</ulink>, required
                  generating javahelp system)</para>
        </listitem>               
        <listitem>
            <para>Optional: javacc 4.0 (<ulink url="http://javacc.dev.java.net/">http://javacc.dev.java.net</ulink>).</para>
        </listitem>
        <listitem>
            <para>Optional: Xerces 2.9 (<ulink url="http://xerces.apache.org">http://xerces.apache.org</ulink>, required for building the Hedeby book).</para>
        </listitem>
        <listitem>
            <para>Optional: FOP 0.93 (<ulink url="http://xmlgraphics.apache.org/fop">http://xmlgraphics.apache.org/fop</ulink>, 
                  required for building the Hedeby book in pdf format).</para>
        </listitem>
    </orderedlist>
    
    <para>
        Please download the required software packages and unpack them under your preferred location. 
        All path adjustments are done in the <filename>build*.properties</filename> files.
    </para>
    
</sect1>
<sect1><title>Checkout from CVS</title>
    <para/>
    <para><prompt>% </prompt><userinput>setenv CVSROOT :pserver:&lt;username&gt;@cvs.sunsource.net:/cvs</userinput></para>
    <para><prompt>% </prompt><userinput>cvs login</userinput></para>
    <para><prompt>% </prompt><userinput>cvs co hedeby</userinput></para>
    <para><prompt>% </prompt><userinput>cd hedeby</userinput></para>
    <para><prompt>% </prompt><userinput>cvs co -d n1sm-adapter hedeby-n1sm-adapter</userinput></para>
    <para><prompt>% </prompt><userinput>cvs co -d ge-adapter hedeby-ge-adapter</userinput></para>
    <para><prompt>% </prompt><userinput>cvs co -d security hedeby-security</userinput></para>
    <para><prompt>% </prompt><userinput>cvs co -d config-center hedeby-config-center</userinput></para>
    <para>For details please see <ulink url="http://hedeby.sunsource.net/servlets/ProjectSource#cmdlinecvs">http://hedeby.sunsource.net/servlets/ProjectSource#cmdlinecvs</ulink>.</para>
<para/></sect1><sect1><title>Adjust your settings in <filename>build*.properties</filename></title>
    <para>The ant build system does not allow overwritting properties. A property that was set 
        by an earlier sourced properties file will not be modified if it is set in a later 
        sourced properties file. The hedeby ant build script sources build files 
        in the following order:
    </para>
    <orderedlist>
        <listitem>
            <filename>build_private.properties</filename>
        </listitem>
        <listitem>
            <filename>build_private_<parameter>&lt;arch&gt;</parameter>.properties</filename>
        </listitem>
        <listitem>
            <filename>build_<parameter>&lt;arch&gt;.properties</parameter></filename>
        </listitem>
        <listitem>
            <filename>build.properties</filename>
        </listitem>
    </orderedlist>
    <para/>
    <para><parameter>&lt;arch&gt;</parameter> must be replaced by the gridengine 
        architecture of the build host (e.g. <constant>win32-x86</constant>, <constant>lx-x86</constant>, ...).
    </para>
    <sect2><title>Available properties</title>
        <informaltable frame="all">
            <tgroup cols="2">
                <thead>
                    <row>
                        <entry>
                            <para>Property</para>
                        </entry>
                        <entry>
                            <para>Description</para>
                        </entry>
                    </row>
                </thead>
                <tbody>
                    <row>
                        <entry>
                            <varname>sge.root</varname>
                        </entry>
                        <entry>
                            <para>Defines the path to the gridengine installation. The build process needs the 
                                <filename>jgdi.jar</filename> and <filename>juti.jar</filename> in 
                                <filename><varname>${sge.root}</varname>/lib</filename>. The <varname>sge.root</varname>
                                property depends in the user environment. It should be set in 
                                <filename>build_private.properties</filename> or in <filename>build_private_win32-x86.properties</filename> if 
                                you try to build hedeby on windows. 
                            </para>
                            <para>Example:
                                <programlisting>sge.root=/opt/sge</programlisting>
                                Or on Windows
                                <programlisting>sge.root=J:\\Program Files\\sge</programlisting>
                            </para>
                            <warning>This property is also used for some junit tests. If it is not set correct, these tests will fail.</warning>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>libs.junit.classpath</varname>
                        </entry>
                        <entry>
                            <para>This property is need to build the junit tests. If you build the hedeby project with Netbeans this property is already set by the IDE. You only need it for build it from the command line.</para>
                            <para>Example: </para>
                            <programlisting>junit.lib.classpath=&lt;path to junit&gt;/junit.jar</programlisting>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>distinst.dir</varname>
                        </entry>
                        <entry>
                            <para>Specfies where the distinst target unpackes the hedeby distribution.</para>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>javacc.home</varname>
                        </entry>
                        <entry>
                            <para>Optional</para>
                            <para>Some parts of hedeby uses the java compiler compiler version 4.0 (<ulink url="https://javacc.dev.java.net/">https://javacc.dev.java.net</ulink>). The javacc.home property specifies where javacc is installed.</para>
                            <para>Example:</para>
                            <programlisting>javacc.home=/tools/javacc-4.0</programlisting>
                        </entry>
                    </row>
                    
                    <row>
                        <entry>
                            <varname>jaxb.home</varname>
                        </entry>
                        <entry>
                            <para>Required</para>
                            <para>
                                Hedeby uses JAXB 2.1.x for xml databinding (https://jaxb.dev.java.net).
                                Due to Bug 358 (see ) the JAXB 2.1.3 does not work we are using
                                our private version which contains the patch.
                            </para>
                            <para>Example:</para>
                            <programlisting>jaxb.home=/tools/jaxb-ri-20070413_with_fix_for_bug_358</programlisting>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>javahelp.home</varname>
                        </entry>
                        <entry>
                            <para>Path the the installation of javahelp. The buildscript looks adds ${javahelp.home}/lib/jh.jar to the classpath of some tasks.</para>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>javacc.debug</varname>
                        </entry>
                        <entry>
                            <para>If set to <constant>true</constant>, all java classes are compiled with the debug option.</para>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>javac.deprecated</varname>
                        </entry>
                        <entry>
                            <para>If set to <constant>true</constant>, all java classes are compiled with the deprecated flag</para>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>javac.optimize</varname>
                        </entry>
                        <entry>
                            <para>If set to <constant>true</constant> all java classes area compiled with the optimize flag</para>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>xalan.dir</varname>
                        </entry>
                        <entry>
                            <para>Defines the path to your Xalan XSLT procoessor installation. From the specified
                                directory the jar files <filename>serializer.jar</filename>, <filename>xalan.jar</filename>, 
                                <filename>fixercesImpl.jar</filename> and <filename>xml-apis.jar</filename> are used.
                            </para>
                            <para>Example:
                                <programlisting>xalan.dir=/opt/xalan_2.7.1</programlisting>
                            </para>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>xml-resolver.jar</varname>
                        </entry>
                        <entry>
                            <para>Defines the path to the Apache XML Resolver jar file</para>
                            <para>Example: </para>
                            <programlisting>xml-resolver.jar=/opt/xml-commons-resolver-1.2/resolver.jar</programlisting>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>xerces.home</varname>
                        </entry>
                        <entry>
                            <para>Defines the path to the xerces installation</para>
                            <para>Example: </para>
                            <programlisting>xerces.home=/opt/xerces-2.9.0</programlisting>
                        </entry>
                    </row>
                    <row>
                        <entry>
                            <varname>fop.home</varname>
                        </entry>
                        <entry>
                            <para>Defines the path to the FOP installation</para>
                            <para>Example: </para>
                            <programlisting>fop.home=/opt/fop-0.93</programlisting>
                        </entry>
                    </row>
                </tbody>
            </tgroup>
        </informaltable>
    </sect2>
</sect1>
<sect1><title>Major build target</title>
    <para>The hedeby build script is quite complex the following table gives an overview of the major build targets.</para>
    <informaltable frame="all">
        <tgroup cols="2">
            <thead>
                <row>
                    <entry>
                        <para>Target</para>
                    </entry>
                    <entry>
                        <para>Description</para>
                    </entry>
                </row>
            </thead><tbody>
                <row>
                    <entry>
                        <varname>jar</varname>
                    </entry>
                    <entry>
                        <para>Builds all jar files of the hedeby (including the jar files of the submodules)</para>
                    </entry>
                </row>
                <row>
                    <entry>
                        <varname>zip</varname>
                    </entry>
                    <entry>
                        <para>Builds the <filename>dist/hedeby.zip</filename> file containing a binary distribution.</para>
                    </entry>
                </row>
                <row>
                    <entry>
                        <varname>tar</varname>
                    </entry>
                    <entry>
                        <para>Builds the <filename>dist/hedeby.tar.gz</filename> file containing a binary distribution.</para>
                        <para>This target does not work on windows.</para>
                    </entry>
                </row>
                <row>
                    <entry>
                        <varname>javadoc</varname>
                    </entry>
                    <entry>
                        <para>Generates the javadocs of hedeby (including the javadoc of the submodules). 
                        Each submodule creates it's own set of javadocs, crosslinks between the javadocs are generated.</para>
                    </entry>
                </row>
                <row>
                    <entry>
                        <varname>javadoc.publish</varname>
                    </entry>
                    <entry>
                        <para>Copies the javadocs into the <filename>www/javadoc/&lt;version&gt;</filename> directory. 
                        The version is defined in the build script of each submodule (property <varname>module.version</varname>).</para>
                    </entry>
                </row>
                <row>
                    <entry>
                        <varname>test</varname>
                    </entry>
                    <entry>
                        <para>Run all junit tests of the hedeby project (including the submodules).</para>
                    </entry>
                </row>
                <row>
                    <entry>
                        <varname>config-center.run</varname>
                    </entry>
                    <entry>
                        <para>Starts the hedeby-config-center.</para>
                    </entry>
                </row>
                <row>
                    <entry>
                        <varname>config-center.debug</varname>
                    </entry>
                    <entry>
                        <para>Starts the hedeby-config-center in the debugger of Netbeans.</para>
                    </entry>
                </row>
                <row>
                    <entry>
                        <varname>spec</varname>
                    </entry>
                    <entry>
                        <para>Builds the Hedeby book in html format (result is stored at <filename>build/spec/index.html</filename>).</para>
                    </entry>
                </row>
                <row>
                    <entry>
                        <varname>spec.pdf</varname>
                    </entry>
                    <entry>
                        <para>Builds the Hedeby book in pdf format (result is stored at <filename>build/spec/HedebyBook.pdf</filename>).</para>
                    </entry>
                </row>
        </tbody></tgroup>
    </informaltable>
</sect1>
</article>
